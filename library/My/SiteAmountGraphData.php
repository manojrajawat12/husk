<?php
class My_SiteAmountGraphData
{
    private $sites;
    
    public function __construct($ids=NULL)
    {
        $sitesMapper = new Application_Model_SitesMapper();
        if($ids!=NULL)
        {
            $sites = array();
            foreach($ids as $id)
            {
                $sites[] = $sitesMapper->getSiteById($id);
            }
        }
        else 
        {
            $sites = $sitesMapper->getAllSites();
            $c = count($sites);
            $filtered = array();
            if($c<5)
            {
                $r = $c+1;
            }
            else
            {
                $r = 6;
            }
            for($i=$c-1;$i>$c-$r;$i--)
            {
                $filtered[] = $sites[$i];
            }
            $sites = $filtered;
        }
        $this->sites = $sites;
    }
    
    public function getData($from_date,$to_date)
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $return = "";
        
        foreach($this->sites as $site)
        {
            $allConsumers = $consumersMapper->searchConsumerBySite(NULL, $site->__get("site_id"));
            if(!$allConsumers)
            {
                $consumerCount = 0;
            }
            else
            {
                $consumerCount = count($allConsumers);
            }
            
            $amountExpected = $siteOutstandingAmount = $moneyCollected = 0;
            
            foreach($allConsumers as $consumer){
                
                $outstandingAmount = $consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
                $siteOutstandingAmount = $siteOutstandingAmount + $outstandingAmount;
                $amount = $consumersMapper->cashPaidByConsumer($consumer->__get("consumer_id"),$from_date,$to_date);
             
                $moneyCollected = $moneyCollected + $amount;
     
            }
            $amountExpected = $moneyCollected;
            
            $return .= "{ site: '".$site->__get("site_name").": ".$consumerCount."', outstandingamount: ".$siteOutstandingAmount.",amountexpected:".$amountExpected." },";
        }
        return $return;
    }
    
   
    
}
?>
