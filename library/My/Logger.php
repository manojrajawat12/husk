<?php 
class My_Logger{
	public function getLogger()
	{
		$logs = new Application_Model_DbTable_Logs();
		$db = $logs->getAdapter();
		$auth =new My_Auth("user");
		if($auth->hasIdentity())
		{
			$user_id = $auth->getIdentity()->user_id;
			$username = $auth->getIdentity()->username;
		}
		else
		{
			$user_id = "0";
			$username = "Guest";
		}
		$columnArray = array(
				"priority_level" => "priority",
				"priority_name" => "priorityName",
				"message" => "message",
// 				"timestamp"=> "timestamp",
				"user_id" => "user_id",
				"username" => "username"
				
		);
		$writer = new Zend_Log_Writer_Db($db, 'logs', $columnArray);
		$logger = new Zend_Log($writer);
		$logger->setEventItem('user_id', $user_id);
		$logger->setEventItem('username', $username);
		
		
		
		return $logger;
	}
}
?>