<?php
class My_DefaultersGraphData
{
    private $sites;
    
    public function __construct($ids=NULL)
    {
        $sitesMapper = new Application_Model_SitesMapper();
        if($ids!=NULL)
        {
            $sites = array();
            foreach($ids as $id)
            {
                $sites[] = $sitesMapper->getSiteById($id);
            }
        }
        else 
        {
            $sites = $sitesMapper->getAllSites();
            $c = count($sites);
            $filtered = array();
            if($c<5)
            {
                $r = $c+1;
            }
            else
            {
                $r = 6;
            }
            for($i=$c-1;$i>$c-$r;$i--)
            {
                $filtered[] = $sites[$i];
            }
            $sites = $filtered;
        }
        $this->sites = $sites;
    }
    
    public function getData($activation_amount_check="show")
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $return = "";
        foreach($this->sites as $site)
        {
            $allConsumers = $consumersMapper->searchConsumerBySite(NULL, $site->__get("site_id"));
            if(!$allConsumers)
            {
                $consumerCount = 0;
            }
            else
            {
                $consumerCount = count($allConsumers);
            }
            $defaulters = 0;
            foreach($allConsumers as $consumer){
                $outstandingAmount = $consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
                if($activation_amount_check=="show")
                {
                    $remainingActAmount = $consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
                }
                
                $totalOutstandingAmount = $outstandingAmount + $remainingActAmount;
                
                if($totalOutstandingAmount > 0){
                    $defaulters = $defaulters + 1;
                }
   
            }
            $goodConsumers= intval($consumerCount)-intval($defaulters);
            $return .= "{ site: '".$site->__get("site_name").": ".$consumerCount."', consumers: ".$goodConsumers.", outstandingconsumers:".$defaulters." },";
        }
        return $return;
    }
    
   
    
}
?>
