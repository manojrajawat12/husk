<?php
class My_ActiveConsumers
{
    private $stats;
    private $sites;
    public function __construct($ids=NULL){
       
        $consumersMapper = new Application_Model_ConsumersMapper();
        $sitesMapper = new Application_Model_SitesMapper();
        if($ids!=NULL)
        {
            $sites = array();
            foreach($ids as $id)
            {
                $sites[] = $sitesMapper->getSiteById($id);
            }
        }
        else 
        {
            $sites = $sitesMapper->getAllSites();
        }       
        $this->sites  = $sites;
        
        $stats = array();
        foreach($sites as $site){
            $siteName = str_replace(" ","",$site->__get("site_name"));
            $siteName = str_replace("-","",$siteName);
            $siteName = strtolower($siteName);
            $stats[$siteName] = $consumersMapper->getTotalConsumersBySite($site->__get("site_id"));
        }
        
        $this->stats = $stats;
    }

    public function getData(){
        $date = new Zend_Date();
        $year = $date->toString("yyyy");
        $counter = intval($date->toString("M"))+1;
        $i = 0;
        $return = "";
        for($i=0;$i<$counter;$i++)
        {
            $month = $i+1;
            $month = str_pad($month, 2, "0", STR_PAD_LEFT);
            $return .= "{m: '".$year."-".$month."', ";
            foreach ($this->stats as $index => $value){
                if(!isset($value[$i]))
                {
                    $v = 0;
                }
                else {
                    $v = $value[$i];
                }
                $return .= $index.": ".$v.",";

            }
            $return .= "},";
        }
        return $return;
        
    }
    
    public function getYKeys(){        
        $return = "";
        foreach($this->sites as $site){
            $siteName = str_replace(" ","",$site->__get("site_name"));
            $siteName = str_replace("-","",$siteName);
            $siteName = strtolower($siteName);
            $return .= "'".$siteName."',"; 
        }
        
        return $return;
    }
            
    public function getLabels(){
        $return = "";
        foreach($this->sites as $site){
            $siteName = $site->__get("site_name");
            $return .= "'".$siteName."',"; 
        }
        
        return $return;
    }
    
}
?>