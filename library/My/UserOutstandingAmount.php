<?php
class My_UserOutstandingAmount
{
    public function displayAmount($auth){
        $user_id = $auth->getIdentity()->user_id;
        
        $usersMapper = new Application_Model_UsersMapper();
        $outstandingAmount = $usersMapper->getOutstandingCash($user_id);
        
        return $outstandingAmount;
    }
    
}
?>
