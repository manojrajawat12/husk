<?php
class My_GraphData
{
    private $stats;
    private $sites;
    public function __construct($ids=NULL){
        
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        $sitesMapper = new Application_Model_SitesMapper();
        if($ids!=NULL)
        {
            $sites = array();
            foreach($ids as $id)
            {
                $sites[] = $sitesMapper->getSiteById($id);
                
            }
        }
        else 
        {
            $sites = $sitesMapper->getAllSites();
        }       
        $this->sites  = $sites;
        $stats = array();
        foreach($sites as $site){
          
            $enteries = $cashRegisterMapper->entryBySite($site->__get("site_id"));
            $siteName = str_replace(" ","",$site->__get("site_name"));
            $siteName = str_replace("-","",$siteName);
            $siteName = strtolower($siteName);
            if($enteries)
            {
                foreach($enteries as $entry){
                    $timestamp = $entry->__get("timestamp");
                    $date = new Zend_Date($timestamp,"yyyy-MM-dd HH:mm:ss");
                    $month = $date->toString("M");
                    $index2 = $month - 1;
                    if($entry->__get("cr_entry_type")==="CREDIT" or $entry->__get("cr_entry_type")==="ACTIVATION"){
                    $stats[$siteName][$index2] = $stats[$siteName][$index2] + $entry->__get("cr_amount"); 

                    }
                    elseif($entry->__get("cr_entry_type")==="REFUND"){                    
                        $stats[$siteName][$index2] = $stats[$siteName][$index2] - $entry->__get("cr_amount"); 
                    }

               }
            }
        }
        
        $this->stats = $stats;
    }

    public function getData(){
        $date = new Zend_Date();
        $year = $date->toString("yyyy");
        $counter = intval($date->toString("M"))+1;
        $i = 0;
        $return = "";
        for($i=0;$i<$counter;$i++)
        {
            $month = $i+1;
            $month = str_pad($month, 2, "0", STR_PAD_LEFT);
            $return .= "{m: '".$year."-".$month."', ";
            foreach ($this->stats as $index => $value){
                if(!isset($value[$i]))
                {
                    $v = 0;
                }
                else {
                    $v = $value[$i];
                }
                $return .= $index.": ".$v.",";

            }
            $return .= "},";
        }
        return $return;
        
    }
    
    public function getYKeys(){        
        $return = "";
        foreach($this->sites as $site){
            $siteName = str_replace(" ","",$site->__get("site_name"));
            $siteName = str_replace("-","",$siteName);
            $siteName = strtolower($siteName);
            $return .= "'".$siteName."',"; 
        }
        
        return $return;
    }
            
    public function getLabels(){
        $return = "";
        foreach($this->sites as $site){
            $siteName = $site->__get("site_name");
            $return .= "'".$siteName."',"; 
        }
        
        return $return;
    }
    
}
?>
