<?php
class My_ConsumersGraphData
{
    private $packages;
    private $sites;
    public function __construct($ids=NULL)
    {
        $sitesMapper = new Application_Model_SitesMapper();
        if($ids!=NULL)
        {
            $sites = array();
            foreach($ids as $id)
            {
                $sites[] = $sitesMapper->getSiteById($id);
            }
        }
        else 
        {
            $sites = $sitesMapper->getAllSites();
            $c = count($sites);
            $filtered = array();
            if($c<5)
            {
                $r = $c+1;
            }
            else
            {
                $r = 6;
            }
            for($i=$c-1;$i>$c-$r;$i--)
            {
                $filtered[] = $sites[$i];
            }
            $sites = $filtered;
        }
        $packagesMapper = new Application_Model_PackagesMapper();
        $packages = $packagesMapper->getAllPackages();
        $this->packages = $packages;
        $this->sites = $sites;
    }
    
    public function getData()
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $return = "";
        foreach($this->sites as $site)
        {
            $allConsumers = $consumersMapper->searchConsumerBySite(NULL, $site->__get("site_id"));
            if(!$allConsumers)
            {
                $consumerCount = 0;
            }
            else
            {
                $consumerCount = count($allConsumers);
            }
            $return .= "{ site: '".$site->__get("site_name").": ".$consumerCount."',"; 
            foreach($this->packages as $package)
            {
                $consumers = $consumersMapper->getConsumersByColumn($site->__get("site_id"),$package->__get("package_id"));
                if(!$consumers)
                {
                    $count = 0;
                }
                else
                {
                    $count = count($consumers);
                }
                $packageName = str_replace(" ","",$package->__get("package_name"));
                $packageName = str_replace("-","",$packageName);
                $packageName = strtolower($packageName);
                $return .= $packageName.": ".$count.", ";
            }
            $return .= "},";
        }
        return $return;
    }
    public function getYKeys(){        
        
        $return = "";
        foreach($this->packages as $package){
            $packageName = str_replace(" ","",$package->__get("package_name"));
            $packageName = str_replace("-","",$packageName);
            $packageName = strtolower($packageName);
            $return .= "'".$packageName."',"; 
        }
        
        return $return;
    }
            
    public function getLabels(){
         $return = "";
        foreach($this->packages as $package){
            $packageName = $package->__get("package_name");
            $return .= "'".$packageName."',"; 
        }
        
        return $return;
    }
    
}
?>
