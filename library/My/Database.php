<?php
class My_Database extends Zend_Controller_Plugin_Abstract
{
    public static function getDetails()
    {
        $front     = Zend_Controller_Front::getInstance();
        $bootstrap = $front->getParam('bootstrap');
        $options = $bootstrap->getOptions();
        $db = $options["resources"]["db"]["params"];
        return $db;
    }
    
}

