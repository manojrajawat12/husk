<?php

class AuthController extends Zend_Controller_Action
{
    public function init()
    { 
       $this->_helper->layout()->setLayout('user_login');
      
        
    }
    
    
    public function indexAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
          $login_type=$request->getParam("login_type");
//            print_r($request->getParams()); die;
            if($request->getParam("request_type")==="login"){
               if($login_type=='agent')
				 {
				   $password=$request->getParam("password");
					$agent_email=$request->getParam("username");
					$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
				 	$collectionAgent=$collectionAgentsMapper->loginAgent(sha1(strtoupper($password)),$agent_email);
					$input=array('username'=>$agent_email,
						'password'=>$password);
				
					if($collectionAgent["collection_agent_id"] !=null || $collectionAgent["collection_agent_id"] !="")
					{
						
						$permissions=explode(',', $collectionAgent["permission"]) ;
						$permission=array('enroll'=>in_array("1", $permissions)?1:0,
										'package'=>in_array("2", $permissions)?1:0,
										'reading'=>in_array("3", $permissions)?1:0,
										'transaction'=>in_array("4", $permissions)?1:0,
										'otp'=>in_array("5", $permissions)?1:0,
										'info'=>in_array("6", $permissions)?1:0, 
										'master_meters'=>in_array("7", $permissions)?1:0,
										'mppts'=>in_array("8", $permissions)?1:0,
										'plantstat'=>in_array("9", $permissions)?1:0,);
		            	$data=array(
						 "agent_id" => $collectionAgent["collection_agent_id"],
						 "agent_mobile" => $collectionAgent["agent_mobile"],
						 "permission"=>$permission
						 );
						$commandResult = array(
						"success"=>1,
						"message" =>"Login Successfully",
						"data"=>$data,
	               		 );
					} else {
						$data=array();
						$commandResult = array(
						"success"=>0,
						"message" =>"Incorrect Username or Password",
						"data"=>$data,
	              		);
					}

					$arr = array(
						"input" => $input,
						"commandResult" => $commandResult,
					);
					$json = json_encode($arr, JSON_PRETTY_PRINT);
					echo $json;
					exit;
				} else{
					if ($this->_process($request->getParams())) 
					{
						// We're authenticated! Redirect to the home page
						$auth = new My_Auth("user");
						$role=$auth->getIdentity()->user_role;
						$this->roleData($role);
						  
						$this->_helper->redirector('index', 'index');
					}
					else 
					{
						$this->view->hasMessage = TRUE;
						$this->view->messageType = "danger";
						$this->view->message = "Username/Password Incorrect";
					}
                }
           }
           else if($request->getParam("request_type")==="forgot_password"){
            
               $reset_code = mt_rand() . mt_rand() . mt_rand() . mt_rand() . mt_rand();
               $email = $request->getParam("email");
               $usersMapper = new Application_Model_UsersMapper();
               $user = $usersMapper->getUserByEmail($email);
               if($user){
                    $user->__set("reset_code",$reset_code);
                    if($usersMapper->updateUser($user)){
                        $subject = "Reset Password";
                        $message = "You requested a password reset link on ".$this->view->baseUrl()."<br/>
                                     Click on the link below to reset your password.<br/><br/>
                                     <a href='".$this->view->baseUrl()."/auth/reset-password/code/".$reset_code."'>".$this->view->baseUrl()."/auth/reset-password/code/".$reset_code."</a>";
                        $result = $this->_newForgotPasswordNotification($email, $subject, $message);
                        if($result){
                            $this->view->hasMessage = TRUE;
                            $this->view->messageType = "success";
                            $this->view->message = "Reset link has been sent to your email";
                        }
                    }
                    else{
                        echo 'defcdsf'; die;
                        $this->view->hasMessage = TRUE;
                        $this->view->messageType = "danger";
                        $this->view->message = "Please try again.";
                    }
               }
               else 
               {
                     
                    $this->view->hasMessage = TRUE;
                    $this->view->messageType = "danger";
                    $this->view->message = "Email doesn't exist in our database";
               }
           
           }
        }
    }
    
    
    protected function _newForgotPasswordNotification($notification_email,$subject,$message)
    {
        $return = array();
        try{
            $mail = new Zend_Mail();
            $mail->setFrom("no-reply@tara.in","Tara.in");
            $mail->addTo($notification_email, 'recipient');
            $mail->setSubject($subject);
            $mail->setBodyHtml($message);
            $return[0] = $mail->send();
            return $return;
        }
        catch (Exception $e){
            $return[0] = false;
            $return[1] = $e->getMessage();
            return $return;
        }
    }
    
    public function resetPasswordAction()
    {
        $request = $this->getRequest();
        if($request->getParam("code"))
        {
            $code = $request->getParam("code");
            $usersMapper = new Application_Model_UsersMapper();
            $user = $usersMapper->getUserByResetCode($code);
            if($user)
            {
                $this->view->flag = TRUE;
            }
            else
            {
                $this->view->flag = FALSE;
                
                $this->view->hasMessage = true;
                $this->view->messageType = "danger";
                $this->view->message = "Invalid reset code";
            }
            if($request->isPost())
            {
                $password = $request->getParam("password");
                $cpassword = $request->getParam("cpassword");
                if($password!=$cpassword)
                {
                    $this->view->hasMessage = true;
                    $this->view->messageType = "danger";
                    $this->view->message = "Passwords doesn't match, Try again";
                }
                else
                {
                    $hashed_password = sha1($password);
                    $user->__set("hashed_password",$hashed_password);
                    $user->__set("reset_code","");
                    if($usersMapper->updateUser($user)){
                        $this->view->hasMessage = true;
                        $this->view->messageType = "success";
                        $this->view->message = "Password reset successful";
                        $this->_redirect('/auth');
                    }
                }
            }
        }
        
          
     }
    
    
    public function logoutAction()
    {
		$adminAuth = new My_Auth("user");
		
		$adminAuth->clearIdentity();
        $this->_helper->redirector('index', 'auth');
    	
    }
    
    protected function _process($values)
    {
		 
    	// Get our authentication adapter and check credentials
    
    	$adapter = $this->_getAuthAdapter();
    
    	$adapter->setIdentity($values['username']);
    
    	$adapter->setCredential($values['password']);
    
        $userAuth = new My_Auth("user");
		 
    	$auth = $userAuth;
		 
    	$result = $auth->authenticate($adapter); 
		// $userMapper=new Application_Model_UsersMapper();
    	//$user_val=$userMapper->userAgent($values['username'], $values['password']);
    	
    	if ($result->isValid()) {
    		//if ($user_val) {
    		$user = $adapter->getResultRowObject();
    		$auth->getStorage()->write($user);
			return true;
    
    	}
    
    	return false;
    
    }
    
    protected function _getAuthAdapter() {
    
    	$users = new Application_Model_DbTable_Users();
    
    	$authAdapter = new Zend_Auth_Adapter_DbTable($users->getAdapter());
    
    	$authAdapter->setTableName('users')
    
    	->setIdentityColumn('username')
    
    	->setCredentialColumn('hashed_password')
    
    	->setCredentialTreatment('SHA1(?)');
    
    	return $authAdapter;
    
    }
    
    public function authDataAction()
    {
    	try{
    		$this->_helper->layout()->disableLayout();
    		$this->_helper->viewRenderer->setNoRender(true);
    		header('Content-Type: application/json');
    		$auth=new My_Auth('user');
    		$role=$auth->getIdentity()->user_role;
    		$state_id=$auth->getIdentity()->state_id;
    		
			 $date = new Zend_Date();
			 $date->setTimezone("Asia/Calcutta");
			 $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			 $ip_address = $_SERVER['REMOTE_ADDR'];
			 $status="Login";
			 $usersMapper= new Application_Model_UsersMapper();
			 $details=$usersMapper->addLogindetails($auth->getIdentity()->user_id,$timestamp,$status,$ip_address,'Website');
			 
		
    		$siteMapper=new Application_Model_SitesMapper();
    		$clusterMapper=new Application_Model_ClustersMapper();
    		$roleSession = new Zend_Session_Namespace('roles');
    		$sites_arr=$roleSession->site_id;
    		$clusters_arr=$roleSession->cluster_id;
    		$states_arr=$roleSession->state_id;
    		
    		$menu_arr=$roleSession->menu_id;
    		$subMenu_arr=$roleSession->subMenu_id;
    		$menu_name=$roleSession->menu_name;
    		$subMenu_name=$roleSession->subMenu_name;
    		
    		//$role_sites=$roleMapper->getAllRoleSiteById($role);
    	
    		if(count($sites_arr)>0){
    			$count_site=count($sites_arr);
    		for($i=0;$i<$count_site;$i++){
    			$site_name=$siteMapper->getSiteById($sites_arr[$i]);
    			$cluster=$clusterMapper->getClusterById($site_name->__get("cluster_id"));
    					$sites=array(
    					"site_id"=>$sites_arr[$i],
    					"site_name"=>$site_name->__get("site_name"),
    					"cluster_id"=>$site_name->__get("cluster_id"),
    					"cluster_name"=>$cluster->__get("cluster_name"),
    					'num_of_poles' => $site_name->__get("num_of_poles"),
    					'site_code' => $site_name->__get("site_code"),
    					'state_id' => $site_name->__get("state_id"),
    					'site_status' => $site_name->__get("site_status"),
    					'site_order' => $site_name->__get("site_order"),
    					'x' => $site_name->__get("x"),
    					'y' => $site_name->__get("y"),
    					'size_of_plant' => $site_name->__get("size_of_plant"),
    					'Solar_panel_size' => $site_name->__get("Solar_panel_size"),
    					'Solar_panel_no' => intval($site_name->__get("Solar_panel_no")),
    					'Charges_controller_size' => $site_name->__get("Charges_controller_size"),
    					'Charges_controller_no' => intval($site_name->__get("Charges_controller_no")),
    					'Inverter_size' => $site_name->__get("Inverter_size"),
    					'Inverter_no' => intval($site_name->__get("Inverter_no")),
    					'Battery_size' => $site_name->__get("Battery_size"),
    					'Battery_no' => intval($site_name->__get("Battery_no")),
    					'otp_act_id' => $site_name->__get("otp_act_id"),
    					'otp_mtr_id' => $site_name->__get("otp_mtr_id"),
    			);
    			$site_arr[]=$sites;
    		}
    		}
    		
    		$stateMaper=new Application_Model_StatesMapper();
    		$state_checks=array_unique($states_arr);
    		$state_check=array_values($state_checks);

    		if(count($state_check)>0){
    			$count_state=count($state_check);
    			for($i=0;$i<$count_state;$i++){
    				$site_name=$stateMaper->getStateById($state_check[$i]);
    				$statesarr=array(
    						"state_id"=>$state_check[$i],
    						"state_name"=>$site_name->__get("state_name"),
    						
    				);
    				$state_val_arr[]=$statesarr;
    			}
    		}
    		$data=array(
    				"userRole" => $role,
    				"state_id" => $state_val_arr,
    				"sites" => $site_arr,

    				
    				"menu_id" => $menu_name,
    				"subMenu_id" => $subMenu_name,
    				
    		);
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data,
    		);
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	 
    }
    public function roleData($role)
    {
    	
    	$roleSession = new Zend_Session_Namespace('roles');
    	//$roleMapper=new Application_Model_RoleSiteMapper();
    	//$role_sites=$roleMapper->getAllRoleSiteById($role);
    	//$permissionMapper=new Application_Model_PermissionMapper();
    	//$permission=$permissionMapper->getAllMenuIdByRoleId($role);
		$sites_arr=array();
		$cluster_arr=array();
		$state_arr=array();
		/*if(count($role_sites)>0){
			foreach ($role_sites as $role_site){
				$sites_arr[]=$role_site->__get("site_id");
				$cluster_arr[]=$role_site->__get("cluster_id");
				$state_arr[]=$role_site->__get("state_id");
			}
    	}*/
		$menu_arr=array();
		$subMenu_arr=array();
		$menu_name=array();
		$subMenu_name=array();
		/*if(count($permission)>0){
			foreach ($permission as $permissions){
				$menu_arr[]=$permissions->__get("parent_menu");
				$subMenu_arr[]=$permissions->__get("menu_id");
				$menu_name[]=$permissions->__get("menu_desc");
				$subMenu_name[]=$permissions->__get("subMenu_name");
			}
			 
		}*/
    	$roleSession->site_id=$sites_arr;
    	$roleSession->cluster_id=$cluster_arr;
    	$roleSession->state_id=$state_arr;
    	$roleSession->menu_id=$menu_arr;
    	$roleSession->subMenu_id=$subMenu_arr;
    	$roleSession->menu_name=$menu_name;
    	$roleSession->subMenu_name=$subMenu_name;
    }

 public function changeDataAction()
    {
    	 $auth = new My_Auth("user");
         $role=$auth->getIdentity()->user_role;
         $this->roleData($role);
    }
}

