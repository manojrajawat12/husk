<?php

class ConsumersController extends Zend_Controller_Action {

    protected $_auth;
    public function init() {
        $this->_auth = new My_Auth("user");
        
        if(!$this->_auth->hasIdentity()){
            $this->_helper->redirector('index', 'auth');
        }
        $this->_helper->layout()->setLayout('user');
        
    }

    public function indexAction() 
    {
        
    }
 
    public function viewAction() {

        $request = $this->getRequest();
		
		$con_id = $request->getParam("con_id");
		$consumer_id = $request->getParam("id");
		
        $consumerMapper = new Application_Model_ConsumersMapper();
		
		if($con_id!=null && $con_id!='undefined' && $con_id!=""){ 
			$roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
			$consumer = $consumerMapper->getConsumerDetailBySearch($con_id,$role_sites_id);
			if($consumer){
				$consumer_id =$consumer->__get("consumer_id");
			}else{
				echo "<center><h1>No Details Found</h1></center>";exit; 
			}
			
		}else{
			$consumer = $consumerMapper->getConsumerById($consumer_id);
		}
	 
		//echo $consumer->__get("consumer_connection_id");exit;
		$WalletTransactionsMapper = new Application_Model_WalletTransactionsMapper();
		$walletTransactions = false;//$WalletTransactionsMapper->getAllWalletTransactionsByConsumerId($consumer_id);
		$this->view->walletTransactions = $walletTransactions;
		
		$consumers_log=$consumerMapper->getConsumerLogsbyConnectionId($consumer->__get("consumer_connection_id"));
			
		$this->view->consumerMapper = $consumerMapper;

        $this->view->consumer = $consumer;

		$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
		$packagesHistory=$packageHistoryMapper->getpackageHistoryByConId($consumer_id);
		//print_r($packagesHistory);
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
		$this->view->cashRegisterMapper = $cashRegisterMapper;
        $cashRegister = $cashRegisterMapper->getAllEntryByConsumerId($consumer_id);
        $typesArray = array("CREDIT", "DEBIT", "DISCOUNT", "ACTIVATION", "PENALTY");
        $cashRegister = $cashRegisterMapper->applyTypeFilter($cashRegister, $typesArray);
		$cashRegister_data=array();
		
		if($cashRegister){
			foreach($cashRegister as $cash_register){
				if($cash_register->__get("cr_status")!='INACTIVE' && $cash_register->__get("entry_status")!='SD'){
					if($cash_register->__get("cr_entry_type") == "DEBIT"){
						$debit  = $debit + $cash_register->__get("cr_amount");
					}else{
						$credit = $credit + $cash_register->__get("cr_amount");
					}
					$balance = ($debit - $credit);
				
					$cr_data = array(
							'cr_id' => $cash_register->__get("cr_id"), 
							'consumer_id' => $cash_register->__get("consumer_id"),
							'user_id' => $cash_register->__get("user_id"),
							'receipt_number' => $cash_register->__get("receipt_number"),
							'transaction_id' => $cash_register->__get("transaction_id"),
							'cr_entry_type' =>$cash_register->__get("cr_entry_type"),
							'entry_status' => $cash_register->__get("entry_status"),
							'cr_amount' => $cash_register->__get("cr_amount"),
							'timestamp' => $cash_register->__get("timestamp"),
							'cr_status' => $cash_register->__get("cr_status"),
							'delete_type' => $cash_register->__get("delete_type"),
							'mtr_entry' => $cash_register->__get("mtr_entry"),
							"status" =>"NoPackage",
							'user_val' => $cash_register->__get("user_val"), 
							'transaction_type'=> $cash_register->__get("transaction_type"),
							'package_id'=> $cash_register->__get("package_id"),
							'approved_date'=> $cash_register->__get("approved_date"),
							'approved_by'=> $cash_register->__get("approved_by"), 
							'balance'=>$balance
					);
					$cashRegister_data[]=$cr_data;
				}
        	}
        }
		  
		if($packagesHistory){	
			foreach ($packagesHistory as $packageHistory){
				$timestamp = $packageHistory->__get("timestamp");
				$date = new Zend_Date($timestamp,"yyyy-MM-dd HH:mm:ss");
				$timestamp= $date->toString("dd-MM-yyyy HH.mm");
				
				if($packageHistory->__get("last_package")=="" || $packageHistory->__get("last_package")==NULL){
					$package=new Application_Model_PackagesMapper();
					    $added_packages=$package->getPackageById($packageHistory->__get("package_id"));
						if($added_packages){
							$package_name=$added_packages->__get("package_name");
							if($packageHistory->__get("status")=='Remove'){
								$message="Remove Package ".$package_name." from customer";
							}else{
								$message="New Package ".$package_name." added for customer";
							}
							
						}else{
							$package_name="NULL";
							$message="";
						}
				}else{
				//elseif($packageHistory->__get("status")=="Package Updated"){
					$package=new Application_Model_PackagesMapper();
					if($packageHistory->__get("old_package_name")!="" || $packageHistory->__get("old_package_name")!=NULL){
						if($packageHistory->__get("last_package")!="" && $packageHistory->__get("last_package")!=NULL){
							 $old_packages=$package->getPackageById($packageHistory->__get("last_package"));
						}else{
							 $old_packages=$package->getPackageById($packageHistory->__get("old_package_name"));
						}
					  
						if($old_packages){
								 $package_name=$old_packages->__get("package_name");
						}else{
							 $package_name="--deleted--";
						}
						if($packageHistory->__get("package_name")!=""||$packageHistory->__get("package_name")!=NULL){
							$newpackage=$packageHistory->__get("package_name");
						}else{
							$newpackage='--deleted--';
						}
						$message="Package updated from ".$package_name." to ".$newpackage.".";
					}else{
					   $package_name="NULL";
					   $message="";
					}
				}
				$package_history_arr = array(
						"timestamp"=>$packageHistory->__get("package_change_date"),
						"message"=>$message,
						"status"=>"package",
				);
		  
				if($package_name!="NULL"){
				array_push($cashRegister_data,$package_history_arr);
				}
			}
		}
		
		$user=new Application_Model_UsersMapper();
		if($consumers_log){
		 	foreach($consumers_log as $consumerlog)
			{
				$users=$user->getUserById($consumerlog["user_id"]);
				
				$message_arr=explode("Consumer ID: ".$consumer->__get("consumer_connection_id"),$consumerlog["message"] ) ;
				$mes=$message_arr[0]." By ".$users->__get("user_fname");
				$data = array(
					"timestamp" => $consumerlog["timestamp"],
					"message" =>   $mes,
					"status"=>"consumer",
				);
		 	
				array_push($cashRegister_data,$data);
			}
		}

		 
        $sort_arr = array();
        foreach ($cashRegister_data as $key => $row)
        {
        	$sort_arr[$key] = $row['timestamp'];
        }
        array_multisort($sort_arr, SORT_ASC ,$cashRegister_data);
		$i=1;$cashRegister_datas=array();
		if(count($cashRegister_data)>0){
			foreach($cashRegister_data as $cash_register){
				if($cash_register["status"]=='NoPackage'){
					$cr_data = array(
							'cr_id' => $cash_register["cr_id"],
							'consumer_id' => $cash_register["consumer_id"],
							'user_id' =>$cash_register["user_id"],
							'receipt_number' => $cash_register["receipt_number"],
							'transaction_id' => $cash_register["transaction_id"],
							'cr_entry_type' =>$cash_register["cr_entry_type"],
							'entry_status' => $cash_register["entry_status"],
							'cr_amount' => $cash_register["cr_amount"],
							'timestamp' =>$cash_register["timestamp"],
							'cr_status' => $cash_register["cr_status"],
							'delete_type' => $cash_register["delete_type"],
							'mtr_entry' => $cash_register["mtr_entry"],
							"status" =>$cash_register["status"],
							'user_val' => $cash_register["user_val"],
							'transaction_type'=> $cash_register["transaction_type"],
							'package_id'=> $cash_register["package_id"],
							'approved_date'=> $cash_register["approved_date"],
							'approved_by'=> $cash_register["approved_by"],
							'balance'=>$cash_register["balance"],
							"no"=>intval($i++)
					);
					 
					$cashRegister_datas[]=$cr_data;
				}elseif($cash_register["status"]=='package'){
					$package_history_arr = array(
						"timestamp"=>$cash_register["timestamp"],
						"message"=>$cash_register["message"],
						"status"=>$cash_register["status"],
						"no"=>$i++
					);
					$cashRegister_datas[]=$package_history_arr;
				}else{
					$con_arr = array(
						"timestamp"=>$cash_register["timestamp"],
						"message"=>$cash_register["message"],
						"status"=>$cash_register["status"],
						"no"=>$i++
					);
					$cashRegister_datas[]=$con_arr;
				}
			
			}
		}
		 
        $this->view->cashRegister = $cashRegister_datas;
		
        //$this->view->cashRegister = $cashRegister;

        $sitesMapper = new Application_Model_SitesMapper();
        $this->view->sitesMapper = $sitesMapper;

        $packagesMapper = new Application_Model_PackagesMapper();
        $this->view->packagesMapper = $packagesMapper;

        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $this->view->collectionAgentsMapper = $collectionAgentsMapper;

        $dummyPackage = new Application_Model_Packages();
        $this->dummyPackage = $dummyPackage;

        $dummySite = new Application_Model_Sites();
        $this->view->dummySite = $dummySite;
                
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $meterReadings = $meterReadingsMapper->getMTRByConsumerId($consumer_id,true);
        $this->view->meterReadings = $meterReadings; 
    	
        $consumerschemesMapper = new Application_Model_ConsumerSchemeMapper();
        $scheme =$consumerschemesMapper->getConsumerschemeByConsumerId($consumer_id);
        if($scheme)
        	$this->view->scheme =$scheme;
        else 
        	$this->view->scheme =NULL;
        
        $schemeMapper=new Application_Model_SchemesMapper();
        $this->view->schemeMapper=$schemeMapper;
		
	$enterpriseMapper=new Application_Model_EnterprisePriceMapper();
        $this->view->priceMapper=$enterpriseMapper;

	$userMapper=new Application_Model_UsersMapper();
        $this->view->userMapper=$userMapper;
        
		/*$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
        $packageHistory=$packageHistoryMapper->getpackageHistoryByConId($consumer_id);
        print_r($packageHistory);
        $this->view->packageHistoryMapper=$packageHistory;
		
		$combine_cr_ph = (object)array_merge((array)$cashRegister,(array)$packageHistory);
		$this->view->combine_CrPh=$combine_cr_ph;
		print_r($combine_cr_ph);*/
		
    }

    protected function _smsNotification($number, $sms) {
        $number = substr($number, 0, 10);
        $sms = urlencode($sms);
        $url = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=TARA&pwd=921118&to=91" . $number . "&sid=TARAMU&msg=" . $sms . "&fl=0&gwid=2";
        $text = file_get_contents($url);
        return $text;
    }

}
