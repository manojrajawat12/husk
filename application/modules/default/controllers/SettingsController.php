<?php

class SettingsController extends Zend_Controller_Action
{
    protected $_auth;
    public function init()
    {
        $this->_auth = new My_Auth("user");
//        if(!$this->_auth->hasIdentity())
//        {
//                $this->_helper->redirector('index', 'auth');
//        }
        $this->_helper->layout()->setLayout('user');
        if($this->_auth->getIdentity()->user_role!="admin")
        {
            echo "Unauthorised Access";exit;
        }
    }

    public function indexAction()
    {
      //  $role = $this->_auth->getIdentity()->user_role;
       // $this->view->role = $role;
        $request = $this->getRequest();
        $userMapper = new Application_Model_UsersMapper();
        $settingsMapper = new Application_Model_SettingsMapper();
        if($request->isPost())
        {
            $req_type = $request->getParam("request_type",false);
            if($req_type)
            {
                try
                { 
                    if($req_type=="change_password"){
                        $id = $this->_auth->getIdentity()->user_id;
                        $user = $userMapper->getUserById($id);
                        $pass = $request->getParam("pass");
                        $cpass = $request->getParam("cpass");
                        if(strlen($pass)<3)
                        {
                            $this->view->hasMessage = true;
                            $this->view->messageType = "danger";
                            $this->view->message = "Length too short";
                        }
                        elseif($pass!=$cpass)
                        {
                            $this->view->hasMessage = true;
                            $this->view->messageType = "danger";
                            $this->view->message = "Password don't match";
                        }
                        else
                        { 
                            $old_password = $request->getParam("old_password");
                            $hashed_password = sha1($old_password);
                            if ($user->__get("hashed_password")==$hashed_password)
                            {
                                $hashed_password = sha1($pass);
                                $user->__set("hashed_password",$hashed_password);
                                if($userMapper->updateUser($user))
                                {
                                    $this->view->hasMessage = true;
                                    $this->view->messageType = "success";
                                    $this->view->message = "Passwords changed successfully";
                                }
                                else
                                {
                                    $this->view->hasMessage = true;
                                    $this->view->messageType = "danger";
                                    $this->view->message = "Error updating password. Try again";
                                }
                            }    
                            else
                            {
                                $this->view->hasMessage = true;
                                $this->view->messageType = "danger";
                                $this->view->message = "Old password incorrect. Please try again.";
                            }
                        }
                   }
                   elseif($req_type==="update"){
                     //  print_r($_POST);exit;
                        $params = $request->getParams();
                     
                        $array = array("module","controller","action","request_type");
                        foreach ($params as $key=>$value){
                            if(!in_array($key, $array))
                            {
                                $setting = $settingsMapper->getSettingByName($key);
                                $setting->__set("setting_value",$value);
                              
                                if($settingsMapper->updateSetting($setting))
                                {
                                    $this->view->hasMessage = true;
                                    $this->view->messageType = "success";
                                    $this->view->message = "Setting changed successfully";
                                }
                                else
                                {
                                    $this->view->hasMessage = true;
                                    $this->view->messageType = "danger";
                                    $this->view->message = "Error updating settings. Try again";
                                }
                            }
                        } 
                   }
                   elseif($req_type==="import")
                   {
                       $importCheckbox = $request->getParam("import-backup",false);
                       if($importCheckbox)
                       {
                           defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
                            $file_name = $this->_backupUpload();
                            $file_name = PUBLIC_PATH."/public_html/imports/".$file_name;
                            $db = My_Database::getDetails(); 
                            $mysql_host = $db["host"];
                             $mysql_username = $db["username"];
                             $mysql_password = $db["password"];
                             $mysql_database = $db["dbname"];
                             $flag = TRUE;
                             if(!mysql_connect($mysql_host, $mysql_username, $mysql_password))
                             {
                                 $flag = FALSE;
                             }
                             if(!mysql_select_db($mysql_database))
                             {
                                 $flag = FALSE;
                             }
                             $templine = '';
                             $lines = file($file_name);
                             foreach ($lines as $line)
                             {
                                 if (substr($line, 0, 2) == '--' || $line == '')
                                     continue;
                                 $templine .= $line;
                                 if (substr(trim($line), -1, 1) == ';')
                                 {
                                     if(!mysql_query($templine))
                                     {
                                         $flag = FALSE;
                                     }
                                     
                                     $templine = '';
                                 }
                             }
                             if($flag)
                             {
                                 $this->view->hasMessage = TRUE;
                                 $this->view->messageType = "success";
                                 $this->view->message = "Imported database successfully";
                             }
                             else
                             {
                                 $this->view->hasMessage = TRUE;
                                 $this->view->messageType = "danger";
                                 $this->view->message = "Not able to import properly. Please try again.";
                             }
                       }
                       else
                       {
                           $this->view->hasMessage = TRUE;
                            $this->view->messageType = "danger";
                            $this->view->message = "Checkbox not checked. Please try again.";
                       }
                   }
                   elseif($req_type=="backup-emails")
                   {
                       $emails = $request->getParam("backup_email");
                       $setting_ids = $request->getParam("setting_id");
                       
                       $i=0;
                       $flag = FALSE;
                       foreach($setting_ids as $setting_id)
                       {
                           if($setting_id!="none")
                           {
                               $setting = $settingsMapper->getSettingById($setting_id);
                               $setting->__set("setting_value",$emails[$i]);
                               $f = $settingsMapper->updateSetting($setting);
                           }
                           else
                           {
                               $setting = new Application_Model_Settings();
                               $setting->__set("setting_name","backup_email");
                               $setting->__set("setting_value",$emails[$i]);
                               $f = $settingsMapper->addNewSetting($setting);
                           }
                           
                           if($f)
                           {
                               $flag = TRUE;
                           }
                           else
                           {
                               $flag = FALSE;
                           }
                           $i++;
                       }
                       if($flag)
                       {
                           $this->view->hasMessage = TRUE;
                            $this->view->messageType = "success";
                            $this->view->message = "All emails updated successfully.";
                       }
                        else 
                        {
                            $this->view->hasMessage = TRUE;
                            $this->view->messageType = "danger";
                            $this->view->message = "Error updating one or more emails.";
                        }
                   }
                    else
                    {
                        $this->view->hasMessage = TRUE;
                        $this->view->messageType = "danger";
                        $this->view->message = "Invalid request. Please try again.";
                    }
                }
                catch(Exception $e)
                {
                    $this->view->hasMessage = TRUE;
                    $this->view->messageType = "danger";
                    $this->view->message = $e->getMessage();
                }
            }
            else 
            {
                $this->view->hasMessage = TRUE;
                $this->view->messageType = "danger";
                $this->view->message = "Invalid request. Please try again.";
            }
        }
        $penalty = $settingsMapper->getSettingByName("penalty_amount");
        $this->view->penaltyAmount = $penalty->__get("setting_value");
        
        $discount = $settingsMapper->getSettingByName("discount_amount");
        $this->view->discountAmount = $discount->__get("setting_value");
        
        $sysEmail = $settingsMapper->getSettingByName("system_email");
        $this->view->sysEmail = $sysEmail->__get("setting_value");
        
        $backupEmails = $settingsMapper->getBackupEmails();
        $this->view->backupEmails = $backupEmails;
    }
    protected function _backupUpload()
    {
        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, 'sql');
        defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        $files = $adapter->getFileInfo();
        $uniqId = time();
        foreach ($files as $file => $info) {
            if($file=="backup_file" and strlen($info["name"])>0)
            {
                $adapter->setDestination(PUBLIC_PATH . "/public_html/imports/");
                $originalFilename = pathinfo($adapter->getFileName($file));
                $newFilename = 'backup-' . $uniqId . '.' . $originalFilename['extension'];
                $adapter->addFilter('Rename', $newFilename, $file); 
                $adapter->receive($file);
                return $newFilename;
            }
        }
    }

}

