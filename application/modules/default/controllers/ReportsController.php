<?php

class ReportsController extends Zend_Controller_Action
{
    public function init()
    {
        $auth = new My_Auth("user");
        if(!$auth->hasIdentity())
        {
                $this->_helper->redirector('index', 'auth');
        }
        $this->_helper->layout()->setLayout('user');
    }
    
    public function indexAction()
    {
        
    }
    /*public function deleteEnteriesAction()
    {
        try
        {
            $request = $this->getRequest();
            if($request->isPost())
            {
                $file = $this->_csvUpload("csv");
                defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
                $filename = PUBLIC_PATH . "/html/csv/".$file;
                $rows = $this->import($filename);
                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $invalid = array();
                foreach($rows as $row)
                {
                    $transaction_id = $row["transaction_id"];
                    $r = $cashRegisterMapper->deleteCashRegisterByTransactionID($transaction_id);
                    if(!$r)
                    {
                        $invalid[] = $transaction_id;
                    }
                }
                $string = "Uploaded ".count($rows)." enteries. Deleted ".count($rows)-count($invalid)." enteries";
                $this->view->string = $string;
                $this->view->ids = $invalid;
            }
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }*/
public function deleteEnteriesAction()
    {
        try
        {
            $request = $this->getRequest();
            if($request->isPost())
            {
                $file = $this->_csvUpload("csv");
                defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
                $filename = PUBLIC_PATH . "/html/csv/".$file;
               
                $file = fopen($filename,"r");
                $data=array();$check=array();
                 while(! feof($file))
                {
                	$data= fgetcsv($file);
                }
             
               
				$invalid=array();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				 
                for($i=1;$i<count($data);$i++){
                	 
                	$r = $cashRegisterMapper->deleteCashRegisterByTransactionID($data[$i]);
                	if(!$r)
                	{
                		$invalid[] = $data[$i];
                	}
                }
                
                fclose($file);
                $total=(count($data)-1);
                $deleted=count($invalid);
                $total_deleted=$total-$deleted;
                $string = "Uploaded ".$total." enteries. Deleted ".$total_deleted." enteries";
                $this->view->string = $string;
                $this->view->ids = $invalid;
            }
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }    

    
    public function managerReportAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $this->view->site_id = $site_id;
            $sitesMapper = new Application_Model_SitesMapper();
            $site = $sitesMapper->getSiteById($site_id);
            $this->view->site = $site;
            $zendDate = new Zend_Date();
            $monthName = $zendDate->toString("MMM");
            $year = $zendDate->toString("yyyy");
            $this->view->year = $year;
            $this->view->monthName = $monthName;
        }
        catch(Exception $e)
        {
           
        }
    }
    public function managerReportTimeWiseAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $this->view->site_id = $site_id;
            $sitesMapper = new Application_Model_SitesMapper();
            $site = $sitesMapper->getSiteById($site_id);
            $this->view->site = $site;
            $zendDate = new Zend_Date();
            $monthName = $zendDate->toString("MMM");
            $year = $zendDate->toString("yyyy");
            $this->view->year = $year;
            $this->view->monthName = $monthName;
        }
        catch(Exception $e)
        {
           
        }
    }
    public function managerReportTimeWiseAAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $this->view->site_id = $site_id;
            $sitesMapper = new Application_Model_SitesMapper();
            $site = $sitesMapper->getSiteById($site_id);
            $this->view->site = $site;
            $zendDate = new Zend_Date();
            $zendDate->setTimezone("Asia/Calcutta");
            $zendDate->addMonth(1);
            $monthName = $zendDate->toString("MMM");
            $year = $zendDate->toString("yyyy");
            $this->view->year = $year;
            $this->view->monthName = $monthName;
        }
        catch(Exception $e)
        {
           
        }
    }
    public function managerReportPlainAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $this->view->site_id = $site_id;
            $sitesMapper = new Application_Model_SitesMapper();
            $site = $sitesMapper->getSiteById($site_id);
            $this->view->site = $site;
            $zendDate = new Zend_Date();
            $monthName = $zendDate->toString("MMM");
            $year = $zendDate->toString("yyyy");
            $this->view->year = $year;
            $this->view->monthName = $monthName;
        }
        catch(Exception $e)
        {
           
        }
    }
    public function managerReportOldAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $this->view->site_id = $site_id;
            $sitesMapper = new Application_Model_SitesMapper();
            $site = $sitesMapper->getSiteById($site_id);
            $this->view->site = $site;
            $zendDate = new Zend_Date();
            $monthNum = intval($zendDate->toString("MM"));
            $months = array();
            for($i=1;$i<=$monthNum;$i++)
            {
                $dateObj   = DateTime::createFromFormat('!m', $i);
                $monthName = $dateObj->format('F'); // March
                $months[] = $monthName;
            }
            $this->view->months = $months;
        }
        catch(Exception $e)
        {
           
        }
    }
    public function import($filename)
   {
       $file_handle = NULL;       
       $data = NULL;
       $keys = NULL;
       $record = NULL;

       if(file_exists($filename))
       {
           $file_handle = fopen($filename, "r");
       } 
       else
       {
           throw new Exception("File not found: " . $filename, null); 
       }

       while (!feof($file_handle) )
       {       
           $row = fgetcsv($file_handle, 1024);   
           if (is_array($row)) 
           {                    
               //set record array keys from csv header
               if ($keys == NULL) 
               {   
                   $keys = array_flip($row);
               } 
               //set record array values from csv row
               elseif ($keys != NULL)
               {
                   foreach ($keys as $key => $value) 
                   {
                       $record[$key] = $row[$value];
                   }
                   $data[] = $record; 
               }   
           }
       }   

       if($file_handle) fclose($file_handle); 

       return $data; 
   }
   protected function _csvUpload($input_name,$file_prefix="csv")
    {
        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, 'csv');

        defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        $files = $adapter->getFileInfo();
        $uniqId = time();
        foreach ($files as $file => $info) {
            if($file==$input_name and strlen($info["name"])>0)
            {
                $adapter->setDestination(PUBLIC_PATH . "/html/csv/");
                $originalFilename = pathinfo($adapter->getFileName($file));
                
                $newFilename = $file_prefix.'-' . $uniqId . '.' . $originalFilename['extension'];
                $adapter->addFilter('Rename', $newFilename, $file); 
                $adapter->receive($file);
     
            }
        }
        return $newFilename;
    }
    
    public function duplicateSmsReportsAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }
	
	 public function cmDefaulterReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$day=$zendDate->toString("dd");
    		$monthName = $zendDate->toString("MMM");
    		$year =intval( $zendDate->toString("yyyy"));
    		$month =intval( $zendDate->toString("MM"));
    		
     		if($day >=25)
    		{
    			$year=($month==12)?$year+1:$year ;
    			$month=($month==12)?1: $month+1;

     		}
     		
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    		$this->view->month = $month;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }
public function cmNextMonthReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }

public function revenueTargetAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year; 
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }
 public function packageSummaryReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }
public function consumerMarketReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }
 
 public function monthlyRevenueReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }
 public function monthlyConsumerReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }


public function reactiveConsumerReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }
    public function newConsumerStaticsAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    		 
    	}
    }   
	  public function collectionSheetAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $this->view->site_id = $site_id;
            $sitesMapper = new Application_Model_SitesMapper();
            $site = $sitesMapper->getSiteById($site_id);
            $this->view->site = $site;
            $zendDate = new Zend_Date();
            $monthName = $zendDate->toString("MMM");
            $year = $zendDate->toString("yyyy");
            $this->view->year = $year;
            $this->view->monthName = $monthName;
        }
        catch(Exception $e)
        {

        }
    }
public function consumerStatusReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
	
public function generationConsumptionReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
    
    public function packageReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
    
    public function consumerProfileReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
    public function revenueReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
	
	 public function meterReadingReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
     
    	}
    }
	public function siteWiseAvgRateReportAction()
    { 
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
	
	public function monthlyOtpAndCollectionReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
	
	public function spiMonthlyReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
	public function productiveLoadReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
	public function monthlySalesDataAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
	 public function reconciliationReportAction()
    {
    	try 
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    }
	public function billingComparisionAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    } 
	public function dieselReportAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    } 
	public function schemeReportAction()
    { 
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    } 
	public function outstandingReportAction()
    { 
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    } 
	public function taraurjaProductiveReportAction()
    { 
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$this->view->site_id = $site_id;
    		$sitesMapper = new Application_Model_SitesMapper();
    		$site = $sitesMapper->getSiteById($site_id);
    		$this->view->site = $site;
    		$zendDate = new Zend_Date();
    		$monthName = $zendDate->toString("MMM");
    		$year = $zendDate->toString("yyyy");
    		$this->view->year = $year;
    		$this->view->monthName = $monthName;
    	}
    	catch(Exception $e)
    	{
    
    	}
    } 
}

