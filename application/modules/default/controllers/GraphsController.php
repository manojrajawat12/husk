<?php

class Api_GraphsController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }
    public function indexAction(){
        
    }
    public function packageConsumersCountAction()
    {
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $packagemapper=new Application_Model_PackagesMapper();
            $consumerMapper=new Application_Model_ConsumersMapper();
            $packages=$packagemapper->getAllPackages();
            if($packages)
            {
                $cols = array();
                
                $col = array();
                $col["id"] = "package";
                $col["label"] = "Package";
                $col["type"] = "string";
                $cols[] = $col;
                
                $col = array();
                $col["id"] = "consumers_count";
                $col["label"] = "Consumers";
                $col["type"] = "number";
                $cols[] = $col;
                
                $rows = array();
                foreach ($packages as $package) 
                {
                    $cs = array();
                    $package_name = $package->__get("package_name");
                    $consumers=$consumerMapper->getAllConsumerByPackageId($package->__get("package_id"),$site_id);
//                    $col["id"] = strtolower(str_replace("@","-",str_replace(" ", "-", $package_name)))."-id";
//                    $col["label"] = $package_name;
//                    $col["type"] = "number";
//                    $col["p"] = array();
//                    $cols[] = $col;
                    
                    $c = array();
                    $c["v"] = $package_name;
                    $cs[] = $c;
                    
                    $c = array();
                    $c["v"] = count($consumers);
                    $cs[] = $c;
                    
                    $row["c"] = $cs;
                    $rows[] = $row;
//                    $consumers=$consumerMapper->getAllConsumerByPackageId($package->__get("package_id"));
//                    $data=array( 
//                        "consumer_count" => count($consumers),   
//                        "package_id" => $package->__get("package_id"),
//                        "package_cost" => $package->__get("package_cost"),
//                        "package_name" => $package_name,
//                        "package_details" =>$package->__get("package_details"),
//                        "is_postpaid" =>$package->__get("is_postpaid")
//                    );
//                    $package_arr[]=$data;
                }
                $data = array(
                    "cols" => $cols,
                    "rows" => $rows
                );
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data,
                );
            } 
            else 
            {
                $meta = array(
                    "code" => 404,
                    "message" => "Not found"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function monthlyBillingRevenueAction()
    {
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $cols = array();
                
            $col = array();
            $col["id"] = "month";
            $col["label"] = "Month";
            $col["type"] = "string";
            $cols[] = $col;

            $col = array();
            $col["id"] = "revenue";
            $col["label"] = "Revenue (CREDIT)";
            $col["type"] = "number";
            $cols[] = $col;
            
            $col = array();
            $col["id"] = "billing";
            $col["label"] = "Billing (DEBIT)";
            $col["type"] = "number";
            $cols[] = $col;
            
            $rows = array();
            for($i=5;$i>=0;$i--){
                $cs = array();
                $zendDate = new Zend_Date();
                $zendDate->setTimezone("Asia/Calcutta");
                $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_NAME);
                $year = $zendDate->toString("yyyy");
                $debit = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"DEBIT",$year));
                $credit = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"CREDIT",$year));
                $c = array();
                $c["v"] = $month;
                $cs[] = $c;

                $c = array();
                $c["v"] = $credit;
                $cs[] = $c;
                
                $c = array();
                $c["v"] = $debit;
                $cs[] = $c;
                

                $row["c"] = $cs;
                $rows[] = $row;
            }
            
            $data = array(
                "cols" => $cols,
                "rows" => $rows
            );
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $arr = array(
                "meta" => $meta,
                "data" => $data,
            );
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function monthlyConsumersCountAction()
    {
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $cols = array();
                
            $col = array();
            $col["id"] = "month";
            $col["label"] = "Month";
            $col["type"] = "string";
            $cols[] = $col;

            
            $col = array();
            $col["id"] = "consumer_count";
            $col["label"] = "Consumer Count";
            $col["type"] = "number";
            $cols[] = $col;
            
            $rows = array();
            for($i=5;$i>=0;$i--){
                $cs = array();
                $zendDate = new Zend_Date();
                $zendDate->setTimezone("Asia/Calcutta");
                $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_SHORT);
                $month_name = $zendDate->toString(Zend_Date::MONTH_NAME);
                $year = $zendDate->toString("yyyy");
                $date_string = "01/".$month."/".$year;
                $count = $consumersMapper->getTotalConsumersCountByMonth($date_string,$site_id);
                $c = array();
                $c["v"] = $month_name;
                $cs[] = $c;

                $c = array();
                $c["v"] = $count;
                $cs[] = $c;

                $row["c"] = $cs;
                $rows[] = $row;
            }
            
            $data = array(
                "cols" => $cols,
                "rows" => $rows
            );
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $arr = array(
                "meta" => $meta,
                "data" => $data,
            );
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function connectionChargeStatsAction()
    {
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $charged_act_amount  = $consumersMapper->getTotalCollectedAmount($site_id);
             
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $received_act_amount = $cashRegisterMapper->getTotalCollectedActivation($site_id);
            $pending_act_amount = $charged_act_amount-$received_act_amount;
            
            $cols = array();
                
            $col = array();
            $col["id"] = "month";
            $col["label"] = "Month";
            $col["type"] = "string";
            $cols[] = $col;

            $col = array();
            $col["id"] = "received";
            $col["label"] = "Received OTP";
            $col["type"] = "number";
            $cols[] = $col;
            
            $col = array();
            $col["id"] = "pending";
            $col["label"] = "Pending OTP";
            $col["type"] = "number";
            $cols[] = $col;
            
            $rows = array();
            $cs = array();
            
            $c = array();
            $c["v"] = "";
            $cs[] = $c;

            $c = array();
            $c["v"] = $pending_act_amount;
            $cs[] = $c;

            $c = array();
            $c["v"] = $received_act_amount;
            $cs[] = $c;


            $row["c"] = $cs;
            $rows[] = $row;
            
            $data = array(
                "cols" => $cols,
                "rows" => $rows
            );
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $arr = array(
                "meta" => $meta,
                "data" => $data,
            );
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
}
?>