<?php

class IndexController extends Zend_Controller_Action
{

    protected $_auth;
    public function init()
    {
        $this->_auth = new My_Auth("user");
        if(!$this->_auth->hasIdentity()){
           $this->_helper->redirector('index', 'auth');
        }
        $this->_helper->layout()->setLayout('user');
        if($this->_auth->getIdentity()->user_role=="4")
        {
             $this->_helper->redirector('manager-report', 'reports'); 
        }
        if($this->_auth->getIdentity()->user_role=="2")
        {
        	$this->_helper->redirector('duplicate-sms-reports', 'reports');
        }
    
    }

   public function indexAction()
   {
   
       
   }

    
}

