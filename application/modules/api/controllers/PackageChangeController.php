<?php

class Api_PackageChangeController extends Zend_Controller_Action { 

    public function init() { 
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userId=$auth->getIdentity()->user_id;
        $this->_userName=$auth->getIdentity()->user_fname;
        
    }
     
    
    public function addPackageChangeAction(){
    	 
    	try {
    		$request=$this->getRequest();
    		$consumer_id=$request->getParam("consumer_id");
    		$site_id=$request->getParam("site_id");
    		$curr_package_id=$request->getParam("current_package_id");
    		$new_package_id=$request->getParam("new_package_id");
    		$assign_package=$request->getParam("assign_package");
    		$activity=$request->getParam("activity");
    		$instant=$request->getParam("instant");
    		$date=$request->getParam("date");
    		$opening_reading=$request->getParam("opening_reading");
    		$closing_reading=$request->getParam("closing_reading");
    		 
    		$date_val = new Zend_Date();
    		$date_val->setTimezone("Asia/Calcutta");
    		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
    		
    		if($date!=NULL && $date!="" && $date!='undefined'){
    			$zendDate = new Zend_Date($date,"dd-MM-yyyy");
    			$effective_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    		}else{
    			$effective_date =$date_val->toString("yyyy-MM-dd");
    		}
    		
    		
    		$changePackageMapper=new Application_Model_PackageChangeMapper();
    		$changePackage=new Application_Model_PackageChange();
    		
    		$changePackage->__set("consumer_id", $consumer_id);
    		$changePackage->__set("site_id", $site_id);
    		$changePackage->__set("curr_package_id", $curr_package_id);
    		$changePackage->__set("new_package_id", $new_package_id);
    		$changePackage->__set("assign_package", $assign_package);
    		$changePackage->__set("activity", $activity);
    		$changePackage->__set("instant", $instant);
    		$changePackage->__set("effective_date", $effective_date);
    		$changePackage->__set("user_id", $this->_userId);
    		$changePackage->__set("user_type", "(W)");
    		$changePackage->__set("timestamp", $timestamp);
    		$changePackage->__set("opening_reading", $opening_reading);
    		$changePackage->__set("closing_reading", $closing_reading);
    		
    		$changePackages=$changePackageMapper->addPackageChange($changePackage);
    		if($changePackages){
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	public function deletePackageChangeIdAction() {
    	try {
    		$request = $this->getRequest();
    		$cp_id = $request->getParam("id");
    		$CPMapper = new Application_Model_PackageChangeMapper();
    		if ($CPs = $CPMapper->deletePackageChangeById($cp_id)) {
    			$this->_logger->info("Package change Id ".$cp_id." has been deleted from change package table by ". $this->_userName.".");
    
    			if (!$CPs) {
    				throw new Exception("No agents found");
    			}
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function getAllPackageChangeAction() {
    	try {
    		$request=$this->getRequest();
    		$status=$request->getParam("status");
    		if($status==NULL || $status=="" || $status=='undefined' ){
    			$status=NULL;
    		} 
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$packageChangeMapper = new Application_Model_PackageChangeMapper();
    		$packageChange = $packageChangeMapper->getAllPackageChange($status,$role_sites_id);
    		if (!$packageChange) {
    			throw new Exception("No Package change found");
    		}
    		$cp_arr=array();
    		 
    		foreach ($packageChange as $packageChanges) {
    			 
    			$usermapper = new Application_Model_UsersMapper();
				
				if($packageChanges->__get("user_type")=='(W)'){
					$users = $usermapper->getUserById($packageChanges->__get("user_id"));
					$userName=$users->__get("user_fname") . " " . $users->__get("user_lname");
				}else{
					$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
					$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($packageChanges->__get("user_id"));
    				$userName=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname") . " " . $cashReg["transaction_type"];
    				
				}
    			
    			
    			$consumerMapper=new Application_Model_ConsumersMapper();
    			$consumers=$consumerMapper->getConsumerById($packageChanges->__get("consumer_id"));
    			
    			$siteMapper=new Application_Model_SitesMapper();
    			$sites=$siteMapper->getSiteById($packageChanges->__get("site_id"));
    			
    			$assign_package_id="";$assign_package_name="";$assign_package_cost="";
    			$currPackage_id="";$currPackage_name="";$currPackage_cost="";
    			$newPackage_id="";$newPackage_name="";$newPackage_cost="";
    			
    			$packageMapper=new Application_Model_PackagesMapper();
    			if($packageChanges->__get("activity")=="new"){
    				$activity_type="Assign Package";
    				$assign_package=$packageMapper->getPackageById($packageChanges->__get("assign_package"));
    				if($assign_package){
    					$assign_package_id=$packageChanges->__get("assign_package");
    					$assign_package_name=$assign_package->__get("package_name");
    					$assign_package_cost=$assign_package->__get("package_cost");
    				}
    			}elseif($packageChanges->__get("activity")=="change"){
    				$activity_type="Change Package";
    				$currPackage=$packageMapper->getPackageById($packageChanges->__get("curr_package_id"));
    				if($currPackage){
    					$currPackage_id=$packageChanges->__get("curr_package_id");
    					$currPackage_name=$currPackage->__get("package_name");
    					$currPackage_cost=$currPackage->__get("package_cost");
    				}
    				$newPackage=$packageMapper->getPackageById($packageChanges->__get("new_package_id"));
    				if($newPackage){
    					$newPackage_id=$packageChanges->__get("new_package_id");
    					$newPackage_name=$newPackage->__get("package_name");
    					$newPackage_cost=$newPackage->__get("package_cost");
    				}
    			}else{
					$activity_type="Remove Package";
    				$assign_package=$packageMapper->getPackageById($packageChanges->__get("assign_package"));
    				if($assign_package){
    					$assign_package_id=$packageChanges->__get("assign_package");
    					$assign_package_name=$assign_package->__get("package_name");
    					$assign_package_cost=$assign_package->__get("package_cost");
    				}
				}
    			 
    			
    			
    			$zendDate = new Zend_Date($packageChanges->__get("effective_date"),"yyyy-MM-dd");
    			$timestamp_edit = $zendDate->toString("dd-MMM-yyyy");
				
				$zendDates = new Zend_Date($packageChanges->__get("timestamp"),"yyyy-MM-dd HH:mm:ss");
    			$timestamp = $zendDates->toString("dd-MMM-yyyy HH:mm:ss");
    			
    			$data = array(
    					"id"=>$packageChanges->__get("id"),
    					"consumer_id" => $packageChanges->__get("consumer_id"),
    					"consumer_name" => $consumers->__get("consumer_name")." (".$consumers->__get("consumer_connection_id").")",
    					"site_id" => $consumers->__get("site_id"),
    					"site_name" =>$sites->__get("site_name"),
    					"curr_package_id" => $currPackage_id,
    					"curr_package_name" => $currPackage_name." (".$currPackage_cost.")",
    					"currPackage_cost" => $currPackage_cost,
    					"new_package_id" => $newPackage_id,
    					"new_package_name" => $newPackage_name." (".$newPackage_cost.")",
    					"newPackage_cost"=>$newPackage_cost,
    					"assign_package_id" => $assign_package_id,
    					"assign_package_name" => $assign_package_name." (".$assign_package_cost.")",
    					"assign_package_cost"=>$assign_package_cost,
    					"effective_date" => $timestamp_edit,
    					"user_name" => $userName,
    					"activity"=>$packageChanges->__get("activity"),
    					"instant"=>$packageChanges->__get("instant"),
    					"activity_type"=>$activity_type,
						"timestamp"=>$timestamp
    					 
    			 );
    			$cp_arr[] = $data;
    		}
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$arr = array(
    				"meta" => $meta,
    				"data" => $cp_arr,
    		);
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function updateAgentByIdAction() {
    	try {
    		$request = $this->getRequest();
    		$agent_id = $request->getParam("id");
    		$agent_mobile = $request->getParam("mobile");
    		$agent_fname = $request->getParam("fname");
    		$agent_lname = $request->getParam("lname");
    		$username = $request->getParam("username");
    		$agent_status = $request->getParam("status");
    		$agent_balance = $request->getParam("balance");
    		$agent_email = $request->getParam("email");
    		$site_id = $request->getParam("site_id");
    		$password = $request->getParam("password");
    		$permission_menu = $request->getParam("permission_menu");
    		$permission_submenu = $request->getParam("permission_submenu");
    		$permission=$permission_menu. ',' .$permission_submenu;
    			
    		$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    		$agents = new Application_Model_CollectionAgents();
    		$lastAgents = $collectionAgentsMapper->getCollectionAgentById($agent_id);
    		$agents->__set("collection_agent_id", $agent_id);
    		$agents->__set("agent_mobile", $agent_mobile);
    		$agents->__set("agent_fname", $agent_fname);
    		$agents->__set("agent_lname", $agent_lname);
    		$agents->__set("username", $username);
    		$agents->__set("agent_status", $agent_status);
    		$agents->__set("agent_balance", $agent_balance);
    		$agents->__set("agent_email", $agent_email);
    		$agents->__set("site_id", $site_id);
    		if($password!='undefined'){
    			$agents->__set("password", sha1(strtoupper($password)));
    		}else{
    			$agents->__set("password", NULL);
    		}
    		$agents->__set("permission", $permission);
    			
    		if ($collectionAgents = $collectionAgentsMapper->updateCollectionAgent($agents)) {
    			 
    			$lastAgent_data = array(
    					"collection_agent_id" => $lastAgents->__get("collection_agent_id"),
    					"agent_fname" => $lastAgents->__get("agent_fname"),
    					"agent_lname" => $lastAgents->__get("agent_lname"),
    					"agent_mobile" => $lastAgents->__get("agent_mobile"),
    					"agent_status" => $lastAgents->__get("agent_status"),
    					"agent_balance" => $lastAgents->__get("agent_balance"),
    					"agent_email" => $lastAgents->__get("agent_email"),
    					"site_id" => $lastAgents->__get("site_id"),
    					"permission" => $lastAgents->__get("permission"),
    			);
    			$newAgent_data = array(
    					"collection_agent_id" => $agents->__get("collection_agent_id"),
    					"agent_fname" => $agents->__get("agent_fname"),
    					"agent_lname" => $agents->__get("agent_lname"),
    					"agent_mobile" => $agents->__get("agent_mobile"),
    					"agent_status" => $agents->__get("agent_status"),
    					"agent_balance" => $agents->__get("agent_balance"),
    					"agent_email" => $agents->__get("agent_email"),
    					"site_id" => $agents->__get("site_id"),
    					"permission" => $agents->__get("permission"),
    			);
    			$lastAgent_diff=array_diff($lastAgent_data,$newAgent_data);
    			$newAgent_diff=array_diff($newAgent_data,$lastAgent_data);
    
    			$change_data="";
    			foreach ($lastAgent_diff as $key => $value)
    			{
    				$change_data.=$key." ".$lastAgent_diff[$key]." change to ".$newAgent_diff[$key]." ";
    			}
    
    			$this->_logger->info("Collection Agents Id ".$agent_id." has been updated where ".$change_data." by ". $this->_userName.".");
    
    			if (!$collectionAgents) {
    				throw new Exception("No agents found");
    			}
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }    
   
   public function updatePackageStatusByIdAction(){
    	 
    	try {
    		$request=$this->getRequest();
    		$cp_id=$request->getParam("id");
    		$status=$request->getParam("status");
    		$remark=$request->getParam("remark");
    
    		$zendDate = new Zend_Date();
    		$zendDate->setTimezone("Asia/Calcutta");
    		$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    
    		$packageChangemapper=new Application_Model_PackageChangeMapper();
    		$packageMapper=new Application_Model_PackagesMapper();
    		$packageHistorymapper = new Application_Model_PackageHistoryMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    		$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
    		$adddeviceMapper=new Application_Model_AddDeviceMapper();
    		$electricityMapper=new Application_Model_ElectricityInfoMapper();
    		  
    		if($status=='disable'){
    			$package=$packageChangemapper->updatePackageChangeById($cp_id,$status,$remark,$timestamp,$this->_userId);
    		}else{
    			$All_Id=explode(",", $cp_id);
    			for($i=0;$i<count($All_Id);$i++){
    				$packageChange=$packageChangemapper->getPackageChangeById($All_Id[$i]);
    				if($packageChange){
    					$package=$packageChangemapper->updatePackageChangeById($All_Id[$i],$status,$remark,$timestamp,$this->_userId);
    					
    					if (($packageChange->__get("activity")=='new' || $packageChange->__get("activity")=='assign' )&& $packageChange->__get("instant")=='Yes'){
    						  
    						$package_id=$packageChange->__get("assign_package");
    						$consumer_id=$packageChange->__get("consumer_id");
    						$packages=$packageMapper->getPackageById($package_id);
    						
    						$consumerPackage= new Application_Model_ConsumersPackage();
    						$consumerPackage->__set("consumer_id", $consumer_id);
    						$consumerPackage->__set("package_id", $package_id);
    						$CP=$consumerPackageMapper->addNewConsumersPackage($consumerPackage); 
    						if($CP){ 
    							$sendMsg=$this->SendMsgToDevice($consumer_id);
    							
    							if($packages->__get("is_postpaid")!=0){
    								$reading=$packageChange->__get("opening_reading");
	    							$meterReading = new Application_Model_MeterReadings();
	    							 
	    							$meterReading->__set("consumer_id",$consumer_id);
	    							$meterReading->__set("meter_reading",$reading);
	    							$meterReading->__set("timestamp",$timestamp);
	    							$meterReading->__set("package_id",$package_id);
	    							$meterReading->__set("start_reading",1);
									$meterReading->__set("entry_by",$this->_userId); 
									$meterReading->__set("entry_type",'W');  
	    							$meterReadingMapper->addNewMeterReading($meterReading);
    							}
    							$package_cost = $packages->__get("package_cost");
    							$user_id=$this->_userId;
    							$status="Request Received";
    							$packageHis= new Application_Model_PackageHistory();
    							$packageHis->__set("package_id",$package_id);
    							$packageHis->__set("consumer_id",$consumer_id);
    							$packageHis->__set("package_cost",$package_cost);
    							$packageHis->__set("change_by",$user_id);
    							$packageHis->__set("status",$status);
    							$packageHis->__set("last_package",NULL);
    							$packageHis->__set("change_description","New Package added");
    							
    							$packageHistory= $packageHistorymapper->addNewPackageHistory($packageHis);
    						}
    					}elseif ($packageChange->__get("activity")=='change' && $packageChange->__get("instant")=='Yes'){
    						 
    						$current_package=$packageChange->__get("curr_package_id");
    						$new_package=$packageChange->__get("new_package_id");
    						$consumer_id=$packageChange->__get("consumer_id");
    						
    						$consumerPackage=$consumerPackageMapper->updatePackageId($consumer_id, $new_package,$current_package);
    						if($consumerPackage){
    							$sendMsg=$this->SendMsgToDevice($consumer_id);
    							
    							$curr_packages=$packageMapper->getPackageById($current_package);
    							$new_packages=$packageMapper->getPackageById($new_package);
    							
    							// Change package code
    								$new_package_cost=$new_packages->__get("package_cost");
    								$is_postpaid=$new_packages->__get("is_postpaid");
    								
    								$current_date = date_parse_from_format("Y-m-d", $timestamp);
    								$day = $current_date["day"];
    								$month = sprintf("%02d", $current_date["month"]);
    								$year = $current_date["year"];
    								$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
    								 
    								$consumers=$consumerMapper->getConsumerById($consumer_id);
    								$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
    								$Act_day = $Act_date["day"];
    								$Act_month = sprintf("%02d", $Act_date["month"]);
    								$Act_year = $Act_date["year"];
    								 
    								$lastDebit=$cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$current_package);
    									
    								
    								if(intval($lastDebit)>0){
										if($month==$Act_month && $year==$Act_year){
											$lastAmount=($lastDebit / $total_days_in_month)*($day-$Act_day);
										}else{
											$lastAmount=($lastDebit / $total_days_in_month) * $day;
										}
										$discount_amount=$lastDebit-$lastAmount;
										
										$timestamps = $zendDate->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
										$cr_amount = str_pad(intval($discount_amount), 4, "0", STR_PAD_LEFT);
										$transaction_id = $timestamps . "-" . $cr_amount;
										 
										$cashRegister = new Application_Model_CashRegister();
										$cashRegister->__set("user_id", $this->_userId); 
										$cashRegister->__set("consumer_id", $consumer_id);
										$cashRegister->__set("cr_entry_type", "DISCOUNT");
										$cashRegister->__set("cr_amount", intval($discount_amount));
										$cashRegister->__set("receipt_number", 0);
										$cashRegister->__set("transaction_id", $transaction_id);
										$cashRegister->__set("transaction_type", "(W)");
										$cashRegister->__set("timestamp", $timestamp);
										$cashRegister->__set("entry_status", "CHG");
										$cashRegister->__set("remark", "Discount for package change");
										$cashRegister->__set("package_id", $new_package);
										$cashRegisterMapper->addNewCashRegister($cashRegister);
									}
    								if($is_postpaid==0){
    									$package_val=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day); 
    									//$package_val=$newAmount-($lastDebit-$lastAmount);
    								}  
									$unit=0;$meter_no=0;$reading=0;
										
    								if($curr_packages->__get("is_postpaid")!=0){
										$reading=$packageChange->__get("closing_reading");
    									
										$lastunit=$meterReadingMapper->checkMonthMeterReadingsByConsumerId($consumer_id);
    									if($lastunit){
    										$reading_last=$lastunit["reading"];
											$meter_no=$lastunit["meter_no"];
											$unit=$reading-$reading_last;
    									}
										
    									$meterReading = new Application_Model_MeterReadings();
    										
    									$meterReading->__set("consumer_id",$consumer_id);
    									$meterReading->__set("meter_reading",$reading);
										$meterReading->__set("unit",$unit);
										$meterReading->__set("meter_no",$meter_no);
    									$meterReading->__set("timestamp",$timestamp);
    									$meterReading->__set("package_id",$current_package);
    									$meterReading->__set("start_reading",0);
										$meterReading->__set("entry_by",$this->_userId); 
										$meterReading->__set("entry_type",'W');  
    									$meterReadingMapper->addNewMeterReading($meterReading);
    								
    								}
    								
    								if($new_packages->__get("is_postpaid")!=0){
    										
    									$reading=$packageChange->__get("opening_reading");
										
    									$meterReading = new Application_Model_MeterReadings();
    										
    									$meterReading->__set("consumer_id",$consumer_id);
    									$meterReading->__set("meter_reading",$reading);
    									$meterReading->__set("timestamp",$timestamp);
    									$meterReading->__set("package_id",$new_package);
    									$meterReading->__set("start_reading",1);
										$meterReading->__set("entry_by",$this->_userId); 
										$meterReading->__set("entry_type",'W');  
    									$meterReadingMapper->addNewMeterReading($meterReading);
    								}
    								if($new_packages->__get("is_postpaid")==0 && intval($package_val)>0){
										$timestamps = $zendDate->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
										$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
										$transaction_id = $timestamps . "-" . $cr_amount;
										 
										$cashRegister = new Application_Model_CashRegister();
										$cashRegister->__set("user_id", $this->_userId);
										$cashRegister->__set("consumer_id", $consumer_id);
										$cashRegister->__set("cr_entry_type", "DEBIT");
										$cashRegister->__set("cr_amount", intval($package_val));
										$cashRegister->__set("receipt_number", 0);
										$cashRegister->__set("transaction_id", $transaction_id);
										$cashRegister->__set("transaction_type", "(W)");
										$cashRegister->__set("timestamp", $timestamp);
										$cashRegister->__set("entry_status", "CHG");
										$cashRegister->__set("remark", "Package upgraded from ".$curr_packages->__get("package_name")."(Rs.".$curr_packages->__get("package_cost").") to " .$new_packages->__get("package_name")."(Rs.".$new_packages->__get("package_cost").")");
										$cashRegister->__set("package_id", $new_package);
										$cashRegisterMapper->addNewCashRegister($cashRegister);
									}
    								$status="Package Updated";
    								
    							
    							 
	    							$packageMapper= new Application_Model_PackagesMapper();
	    							$packages=$packageMapper->getPackageById($new_package);
	    							$package_cost = $packages->__get("package_cost");
	    							$user_id=$this->_userId;
	    							
	    							$package= new Application_Model_PackageHistory();
	    							$package->__set("package_id",$new_package);
	    							$package->__set("consumer_id",$consumer_id);
	    							$package->__set("package_cost",$package_cost);
	    							$package->__set("change_by",$user_id);
	    							$package->__set("status",$status);
	    							$package->__set("last_package",$current_package);
	    							 
	    							$packageHistorymapper = new Application_Model_PackageHistoryMapper();
	    							$packageHistory= $packageHistorymapper->addNewPackageHistory($package);
    							 
    							
    							// End of change package code
    							
    						}
    					}elseif($packageChange->__get("activity")=='remove' && $packageChange->__get("instant")=='Yes'){
							$package_id=$packageChange->__get("assign_package");
    						$consumer_id=$packageChange->__get("consumer_id");
    						$packages=$packageMapper->getPackageById($package_id);
    						 $package_cost=$packages->__get("package_cost");
    						$CP=$consumerPackageMapper->deletePackageByConsumerId($consumer_id,$package_id); 
    						if($CP){
    							$sendMsg=$this->SendMsgToDevice($consumer_id);
    							$unit=0;$meter_no=0;$reading=0;
									if($packages->__get("is_postpaid")!=0){
										$reading=$packageChange->__get("opening_reading");
										$lastunit=$meterReadingMapper->checkMonthMeterReadingsByConsumerId($consumer_id);
											if($lastunit){
												$reading_last=$lastunit["reading"];
												$meter_no=$lastunit["meter_no"];
												$unit=$reading-$reading_last;
											}
										$meterReading = new Application_Model_MeterReadings();
										 
										$meterReading->__set("consumer_id",$consumer_id);
										$meterReading->__set("meter_reading",$reading);
										$meterReading->__set("unit",$unit);
										$meterReading->__set("meter_no",$meter_no);
										$meterReading->__set("timestamp",$timestamp);
										$meterReading->__set("package_id",$package_id);
										$meterReading->__set("start_reading",0);
										$meterReading->__set("entry_by",$this->_userId); 
										$meterReading->__set("entry_type",'W');  
										$meterReadingMapper->addNewMeterReading($meterReading);
									}
									
									$date = new Zend_Date();
									$date->setTimezone("Asia/Kolkata");
									$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
									$current_date = date_parse_from_format("Y-m-d", $curr_date);
									
									$day = $current_date["day"];
									$month = sprintf("%02d", $current_date["month"]);
									$year = $current_date["year"];
									$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
									$consumers=$consumerMapper->getConsumerById($consumer_id);
									$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
									$Act_day = $Act_date["day"];
									$Act_month = sprintf("%02d", $Act_date["month"]);
									$Act_year = $Act_date["year"];
									 
									$package = $packageMapper->getPackageById($package_id);
									if($consumers->__get("site_id")==31 && $consumers->__get("site_id")==62){
										$unit=$meterReadingMapper->getTotalUnitByOnlyConsumerId($consumer_id,$month,$year);
									}
									$lastDebit=intval($cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$package_id));
    								
									if($package->__get("is_postpaid")==0){
										if($month==$Act_month && $year==$Act_year){
											$lastAmount=($lastDebit/$total_days_in_month)*($day-$Act_day);
										}else{
											$lastAmount=($lastDebit/$total_days_in_month)*$day;
										}
										$amount=intval(round($lastAmount)-$lastDebit);
									
									}elseif($package->__get("is_postpaid")==1){
										$amount=round($unit*$consumers->__get("micro_entrprise_price"));
									}else{
										$extra_charges=$package->__get("extra_charges");
										$package_cost=$package->__get("package_cost");
										
										$lastDebit=$package_cost;
										
										if($month==$Act_month && $year==$Act_year){
											$lastAmount=($lastDebit/$total_days_in_month)*($day-$Act_day);
											$min_unit=intval(($package->__get("unit")/$total_days_in_month)*($day-$Act_day));
										}else{
											$lastAmount=($lastDebit/$total_days_in_month)*$day;
											$min_unit=intval(($package->__get("unit")/$total_days_in_month)*$day);
										}
										
										if($unit<=$min_unit){
											$amount=intval($lastAmount);
										}else{
											$calUnitAmount=($unit-$min_unit)*$extra_charges;
											$amount=intval($lastAmount+$calUnitAmount);
										}
									}
									 
									$discount_val=intval($amount); 
									
									$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
									$cr_amount = str_pad(abs($discount_val), 4, "0", STR_PAD_LEFT);
									$transaction_id = $timestamp . "-" . $cr_amount;
									
									if(intval($lastDebit)>0){
										$cashRegisterMapper = new Application_Model_CashRegisterMapper();
										$cashRegister = new Application_Model_CashRegister();
										$cashRegister->__set("user_id",$this->_userId);
										$cashRegister->__set("consumer_id", $consumer_id);
										$cashRegister->__set("cr_entry_type", "DEBIT");
										$cashRegister->__set("cr_amount", intval($discount_val)); 
										$cashRegister->__set("receipt_number", 0); 
										$cashRegister->__set("transaction_id", $transaction_id);
										$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
										$cashRegister->__set("entry_status", "CHG");
										$cashRegister->__set("remark", "delete package");
										$cashRegister->__set("approved_by", $this->_userId); 
										$cashRegister->__set("approved_date", $date->toString("yyyy-MM-dd HH:mm:ss"));
										$cashRegister->__set("package_id",$package_id);
										$cashRegisterMapper->addNewCashRegister($cashRegister);
									}
							 
							}
						}
					 
					}
    			}
    		}
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		 
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
   
	
    public function SendMsgToDevice($consumer_id){
    	
    	$adddeviceMapper=new Application_Model_AddDeviceMapper();
    	$electricityMapper=new Application_Model_ElectricityInfoMapper();
    	$consumerPackagesMapper=new Application_Model_ConsumersPackageMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$iermsMapper=new Application_Model_DeviceMapper();
    	$electricitys=$adddeviceMapper->getVolByConid($consumer_id);
    	$wattage_total=0;
    	if($electricitys){
    		
    	
    		$devices=$adddeviceMapper->getDataByDeviceid($electricitys["device_name"]);
    		if($devices){
    			
    			$cp_data=$consumerPackagesMapper->getPackageByConsumerId($consumer_id);
    			if($cp_data){
    				foreach ($cp_data as $cp){
    					$watt_id=$cp["wattage"];
    					$watts=$wattageMapper->getWattageBywattId($watt_id);
    					if($watts){
    						$wattage_total=$wattage_total+$watts->__get("wattage");
    					}
    				}
    			}
    			
    			$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer_id);
    			 
    			$zendDate = new Zend_Date();
    			$zendDate->setTimezone("Asia/Calcutta");
    			$date_val = $zendDate->toString("yyyy-MM-dd");
    			$trans_date=date_parse_from_format("Y-m-d", $date_val);
    			$month_data= $trans_date["month"];
    			$year_data= $trans_date["year"];
    			 
    			$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_data, $year_data);
    			if($consumer_scheme){
    				foreach($consumer_scheme as $consumer_schemes){
    					$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
    					$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
    						
    					$discount_month=$consumer_schemes["discount_month"];
    					$feeder_hours=$consumer_schemes["feeder_hours"];
    					$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month"));
    					 
    					if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
    						$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]));
    						$wattage_total=$wattage_total + $dailyUnit;
    					}
    				}
    			}
    			
    			$sim_no=$devices[0]["device_sim"];
    			$device_name=$devices[0]["name"];
    			$time=$devices[0]["time"];
    			$times=sprintf("%04d",$time);
    			$count_device=count($devices);
    			$wattage=sprintf("%04d",$wattage_total);
    			$nodeArray=array();
    			 
    			for ($i=0;$i<$count_device;$i++){
    				if($devices[$i]["node"]==1){
    					if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODEA".$wattage;
    					}else{
    						$nodeArray[]= "NODEA".$devices[$i]["wattage"];
    					}
    					 
    				}else if($devices[$i]["node"]==2){
    				if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODEB".$wattage;
    					}else{
    						$nodeArray[]= "NODEB".$devices[$i]["wattage"];
    					}
    				}else if($devices[$i]["node"]==3){
    				if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODEC".$wattage;
    					}else{
    						$nodeArray[]= "NODEC".$devices[$i]["wattage"];
    					}
    				}else if($devices[$i]["node"]==4){
    				if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODED".$wattage;
    					}else{
    						$nodeArray[]= "NODED".$devices[$i]["wattage"];
    					}
    				}else if($devices[$i]["node"]==5){
    				if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODEE".$wattage;
    					}else{
    						$nodeArray[]= "NODEE".$devices[$i]["wattage"];
    					}
    				}
    			}
    	
    			 $msg="616629;LCDID".$device_name.";".$nodeArray[0].";".$nodeArray[1].";".$nodeArray[2].";".$nodeArray[3].";".$nodeArray[4].";"."TIME".$times.";";
				 $texts= $this->_smsNotification($sim_no, $msg);   
				 $zend_date = new Zend_Date();
    			 $zend_date->setTimezone("Asia/Calcutta");
    			 $timestamp_val = $zend_date->toString("yyyy-MM-dd HH:mm:ss");
				 
				 if (strpos($texts, 'success') !== false){
					$iermsMapper->updateSmsDeviceStatsus('Success',$timestamp_val,$electricitys["device_name"]);
				 }else{
					$iermsMapper->updateSmsDeviceStatsus('Failure',$timestamp_val,$electricitys["device_name"]);  
				 }   
    		}
    	}
    }
     
    protected function _smsNotification($number, $sms) {
    
    	$number = substr($number, 0, 10);
    	//$number = '8285859194'; 
    	$sms = urlencode($sms);
    	$smsMapper =new Application_Model_SmsGatewayMapper();
    	$smsgateway = $smsMapper->getAllSmsGateway();
    	$user_name=$smsgateway[0]->__get("user_name");
    	$password=$smsgateway[0]->__get("password");
    
    	$url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=".$user_name."&auth_scheme=plain&password=".$password."&v=1.1&format=text";
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    	
    	$text = file_get_contents($url);
    	
    	$file = 'people.txt';
    	$current = file_get_contents($file);
    	$current .= "IN(".$number."):".$sms." ---- Out:".$text . " ---- Time - ".$timestamp."\r\n";
    	file_put_contents($file, $current);
    	
    	return $text;
    }
}