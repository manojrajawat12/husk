<?php

class Api_MeterInventoryController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userId=$auth->getIdentity()->user_id;
        $this->_userName=$auth->getIdentity()->user_fname;
        
    }
    
    public function addMeterInventoryAction(){

        try {
            $request = $this->getRequest();
			$meterInventoryMapper = new Application_Model_MeterInventoryMapper();
				 
            $meter_number = $request->getParam("meter_number");
            $reading = $request->getParam("reading");
            $status = $request->getParam("status");
            $brand = $request->getParam("brand");
			$checkNo = $meterInventoryMapper->getDetailByMeterNo($meter_number);
			if(!$checkNo){
				$meterInventory = new Application_Model_MeterInventory();
				
				$meterInventory->__set("meter_number",$meter_number);
				$meterInventory->__set("reading",$reading);
				$meterInventory->__set("status",$status);
				$meterInventory->__set("brand",$brand);

				$meterInventories = $meterInventoryMapper->addNewMeterInventory($meterInventory);
             
				if($meterInventories){  
				 
					$data=array();
					 
					$meta = array(
							"code" => 200,
							"message" => "SUCCESS"
						);
						$arr = array(
							"meta" => $meta,
							 "data" => $data
						);
				} else {
					$meta = array(
						"code" => 401,
						"message" => "Error while adding"
					);
					$arr = array(
						"meta" => $meta
					);
				}
			} else {
					$meta = array(
						"code" => 301,
						"message" => "Meter Number is already exist in inventory." 
					);
					$arr = array(
						"meta" => $meta
					);
			}
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllMeterInventoryAction(){
    
        try {
            $meterInventoryMapper = new Application_Model_MeterInventoryMapper();
            $meterInventories = $meterInventoryMapper->getAllMeterInventories();
           
            if(count($meterInventories) >0){
                foreach ($meterInventories as $meterInventory) {
					if($meterInventory->meter_status==0){
						$status_new='Enable (Pending)';
					}elseif($meterInventory->meter_status==1){
						$status_new='Enable (In Use)';
					}elseif($meterInventory->meter_status==2){
						$status_new='Enable (Free)';
					}else{
						$status_new='Disable';
					}
                    $data = array(
                        'id' => $meterInventory->id,
                        'meter_number' => $meterInventory->meter_no,
                        'reading' => floatval($meterInventory->reading),
                        'status' => $meterInventory->meter_status,
                        'brand' => $meterInventory->brand,
						'status_new' => ucwords(strtolower($status_new)),
						
                    );
                    $query_arr[] = $data;
                }
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $query_arr,
                );
    
            }
            else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
    
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function updateMeterInventoryByIdAction(){
    
        try {
            $request = $this->getRequest();
            $id = $request->getParam("id");
            $meter_number = $request->getParam("meter_number");
            $reading = $request->getParam("reading");
            $status = $request->getParam("status");
            $brand = $request->getParam("brand");

            $meterInventoryMapper = new Application_Model_MeterInventoryMapper();
            $meterInventory = new Application_Model_MeterInventory();
            
            $meterInventory->__set("id",$id);
            $meterInventory->__set("meter_number",$meter_number);
            $meterInventory->__set("reading",$reading);
            $meterInventory->__set("status",$status);
            $meterInventory->__set("brand",$brand);
    
            if($meterInventoryMapper->updateMeterInventory($meterInventory)){
              
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
    
                );
            } else {
                $meta = array(
                        "code" => 401,
                        "message" => "Error while updating"
                );
                $arr = array(
                        "meta" => $meta
                );
            }
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function deleteMeterInventoryByIdAction(){
    
        try {
            $request = $this->getRequest();
            $id = $request->getParam("id");
           
            $meterInventoryMapper = new Application_Model_MeterInventoryMapper();
            
            if($state = $meterInventoryMapper->deleteMeterInventoryById($id)){
               
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
    
                );
            } else {
                $meta = array(
                        "code" => 401,
                        "message" => "Error while deleting"
                );
                $arr = array(
                        "meta" => $meta
                );
            }
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    } 

	public function getMeterReadingByRangeAction(){
    
        try {
			date_default_timezone_set('Asia/Kolkata');
            $request = $this->getRequest();
			$curr_date = $request->getParam("curr_date");
            $last_date = $request->getParam("last_date");
            
            $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
            $curr_date = $zendDate->toString("yyyy-MM-dd");
            
            $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
            $last_date=$zendDate->toString("yyyy-MM-dd");
			
			$consumer_id=$request->getParam("consumer_id");
			$package_id=$request->getParam("package_id");
			
			$meterReadingMapper = new Application_Model_MeterReadingsMapper();
           
			$cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
            $days = round($cal); 
            $m=0;$y=0;
			
            $counter=round($days/30);
            $next='+1 month';
			$secondNext=$last_date;
			$reading_arr=array();
			for($i=0;$i<$counter;$i++){
				$trans_date=date_parse_from_format("Y-m-d", $secondNext);
				$month= $trans_date["month"];
				$year= $trans_date["year"];
				
				$meterReadings = $meterReadingMapper->getMeterDetailsForSparkConsumerId($consumer_id,$package_id,$month,$year);
				
				$dateObj   = DateTime::createFromFormat('!m', $month);
				$monthName = $dateObj->format('F');
				$secondNext = date('Y-m-d', strtotime($secondNext.$next));
	                
				$data=array(
					"reading"=>floatval($meterReadings["curr_reading"]),
					"units"=>floatval($meterReadings["units"]),
					"timestamp"=>$monthName.", ".$year,
					"month"=>$month,
					"year"=>$year,
				);
				$reading_arr[]=$data;
			}	
            
			if(count($reading_arr) >0){
               
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $reading_arr,
                );
    
            }else{
                $meta = array(
                        "code" => 200,
                        "message" => "No Detail found"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
		}catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	public function updateMeterReadingByConsumerIdAction(){
    
        try {
			date_default_timezone_set('Asia/Kolkata');
            $request = $this->getRequest();
			$month = $request->getParam("month");
            $year = $request->getParam("year");
			$reading = $request->getParam("reading");
			$reading_old = $request->getParam("reading_old");
            $consumer_id=$request->getParam("consumer_id");
			$package_id=$request->getParam("package_id");
			
			$meterReadingMapper = new Application_Model_MeterReadingsMapper();
            
			$meterReadings = $meterReadingMapper->getMeterReadingsByConsumerIdTimestamp($consumer_id,$package_id,$month,$year);
			$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
			if($meterReadings){
				$timestamp_val=date('Y-m-d', strtotime($meterReadings["timestamp"]."-1 day"));
				$meterReading_val = $meterReadingMapper->getReadingByConPackID($consumer_id,$timestamp_val,$package_id);
				if($meterReading_val){
					$readings_old=$meterReading_val["meter_reading"];
				}else{
					$readings_old=0;
				}
				$unit=floatval($reading-$readings_old);
				$reading_date = $meterReadingMapper->updateReading($meterReadings["meter_reading_id"],$reading,$unit); 
            }else{
				$timestamp=$year."-".$month."-".$total_days_in_month." 00:00:00";
				$reading_date = $meterReadingMapper->getReadingByConPackID($consumer_id,$timestamp,$package_id);
				if($reading_date){
					$meter_no=$reading_date["meter_no"];
				}else{
					$meter_no=NULL;
				}
                $MeterReadings= new Application_Model_MeterReadings();
				$MeterReadings->__set("consumer_id",$consumer_id);
				$MeterReadings->__set("meter_reading",$reading);
				$MeterReadings->__set("timestamp",$timestamp);
				$MeterReadings->__set("package_id",$package_id);
				$MeterReadings->__set("start_reading",0);
				$MeterReadings->__set("meter_no",$meter_no);
				$MeterReadings->__set("unit",floatval($reading-$reading_old));
				$MeterReadings->__set("serial",$meter_no);
				$readings=$meterReadingMapper->addNewMeterReading($MeterReadings);
            }
			
			$meta = array(
				"code" => 200,
				"message" => "SUCCESS"
			);
			$arr = array(
				"meta" => $meta,
				"data" => $reading_arr,
			);
		}catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	public function updateTransactionByConsumerIdAction(){
    
        try {
			date_default_timezone_set('Asia/Kolkata');
            $request = $this->getRequest();
			$month = $request->getParam("month");
            $year = $request->getParam("year");
			$reading = $request->getParam("reading");
			$reading_old = $request->getParam("reading_old");
            $consumer_id=$request->getParam("consumer_id");
			$package_id=$request->getParam("package_id");
			
			$packagesMapper = new Application_Model_PackagesMapper();
		   	$cashRegisterMapper = new Application_Model_CashRegisterMapper();
		    $consumersMapper = new Application_Model_ConsumersMapper();
		   	$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
		    $meterMapper=new Application_Model_MeterReadingsMapper();
            $packageChangeMapper=new Application_Model_PackageChangeMapper();
			$consumerActivityMapper=new Application_Model_ConsumerActivityMapper(); 	
			
			$consumer=$consumersMapper->getConsumerByConsumerId($consumer_id,$package_id);
			
			$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($consumer){
					$package_id = $consumer["packages_val"];
	   				$is_postPaid =  $consumer["is_postpaid"];
	   				$package_cost =  $consumer["package_cost"];
					if($consumer["is_postpaid"]==2){
						$extra_charges =  $consumer["extra_charges"];
						$fixed_unit =  $consumer["unit"];
					}else{
						$extra_charges =  $consumer["micro_entrprise_price"];
						$fixed_unit =  0;
					}
					$ActualAmount=0;
	   				$unit = $consumer["unit"];
	   			  	
	   				$Act_date = date_parse_from_format("Y-m-d", $consumer["consumer_act_date"]);
	   				$Act_day = $Act_date["day"];
	   				$Act_month = sprintf("%02d", $Act_date["month"]);
	   				$Act_year = $Act_date["year"];
	   				
					$consumerActivitys=$consumerActivityMapper->getActivityConsumer('reactive',$month,$year,$consumer["consumer_id"]);
	   				$ckeckActivitys=$packageChangeMapper->getAllPackageActivityOfConsumerInMonth($consumer["consumer_id"],$month,$year);
					
					if($consumerActivitys){
								$curr_packages = $packagesMapper->getPackageById($package_id);
								if($curr_packages->__get("is_postpaid")==2){
									$extra_charges = $curr_packages->__get("extra_charges");
									$fixed_unit =  $curr_packages->__get("unit");
									
									$change_date = date_parse_from_format("Y-m-d", $consumerActivitys["timestamp"]);
									$end_day = ($total_days_in_month-$change_date["day"]);
									$fixed_unit=($fixed_unit/$total_days_in_month)*$end_day;
									
									$amount_val=($curr_packages->__get("package_cost")/$total_days_in_month)*$end_day;
									$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$curr_packages->__get("package_id"),$month,$year);
									if($units){
										if($units>$fixed_unit){
											$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
										}else{
											$ActualAmount=$amount_val;
										}
									}else{
										$ActualAmount=$amount_val;
									}
								} 
								 
					}elseif($ckeckActivitys){
						foreach($ckeckActivitys as $ckeckActivity){
							if($ckeckActivity["activity"]=='change'){
								$curr_packages = $packagesMapper->getPackageById($ckeckActivity['curr_package_id']);
								if($curr_packages->__get("is_postpaid")==2){
									$extra_charges = $curr_packages->__get("extra_charges");
									$fixed_unit =  $curr_packages->__get("unit");
									
									$change_date = date_parse_from_format("Y-m-d", $ckeckActivity["timestamp"]);
									$end_day = $change_date["day"];
									$fixed_unit=($fixed_unit/$total_days_in_month)*$end_day;
									
									$amount_val=($curr_packages->__get("package_cost")/$total_days_in_month)*$end_day;
									$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$curr_packages->__get("package_id"),$month,$year);
									if($units){
										if($units>$fixed_unit){
											$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
										}else{
											$ActualAmount=$amount_val;
										}
									}else{
										$ActualAmount=$amount_val;
									}
								}
								$new_packages = $packagesMapper->getPackageById($ckeckActivity['new_package_id']);
								if($new_packages->__get("is_postpaid")==2){
									$extra_charges = $new_packages->__get("extra_charges");
									$fixed_unit =  $new_packages->__get("unit");
									
									$change_date = date_parse_from_format("Y-m-d", $ckeckActivity["timestamp"]);
									$end_day = $change_date["day"];
									$fixed_unit=($fixed_unit/$total_days_in_month)*($total_days_in_month-$end_day);;
									
									$amount_val=($new_packages->__get("package_cost")/$total_days_in_month)*($total_days_in_month-$end_day);
									$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$new_packages->__get("package_id"),$month,$year);
									if($units){
										if($units>$fixed_unit){
											$ActualAmount=$ActualAmount+$amount_val+(($units-$fixed_unit)*$extra_charges);
										}else{
											$ActualAmount=$ActualAmount+$amount_val;
										}
									}else{
										$ActualAmount=$ActualAmount+$amount_val;
									}
								}
							}elseif($ckeckActivity["activity"]=='new'){
								
								$assign_packages = $packagesMapper->getPackageById($ckeckActivity['assign_package']);
								if($assign_packages->__get("is_postpaid")==2){
									$extra_charges = $assign_packages->__get("extra_charges");
									$fixed_unit =  $assign_packages->__get("unit");
									
									$change_date = date_parse_from_format("Y-m-d", $ckeckActivity["timestamp"]);
									$end_day = $change_date["day"];
									$fixed_unit=($fixed_unit/$total_days_in_month)*($total_days_in_month-$end_day);
									
									$amount_val=($assign_packages->__get("package_cost")/$total_days_in_month)*($total_days_in_month-$end_day);
									$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$assign_packages->__get("package_id"),$month,$year);
									if($units){
										if($units>$fixed_unit){
											$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
										}else{
											$ActualAmount=$amount_val;
										}
									}else{
										$ActualAmount=$amount_val;
									}
								}
							}
						}												 												
		   			}else{
						if($month==$Act_month && $year==$Act_year){
		   					$amount_val=($package_cost/$total_days_in_month)*($day-$Act_day);
							$fixed_unit=($fixed_unit/$total_days_in_month)*($day-$Act_day); 
		   				}else{
		   					$amount_val=($package_cost/$total_days_in_month)*($day);
		   				}
		   				$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$consumer["package_id"],$month,$year);
		   				
						if($is_postPaid==1){
							$ActualAmount=$units*$extra_charges;
						}elseif($units && $is_postPaid==2){
		   					if($units>$fixed_unit){
		   						$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
		   					}else{
		   						$ActualAmount=$amount_val;
		   					}
		   				}else{ 
		   					$ActualAmount=$amount_val;
		   				}
		   			}
					
		   			$cash=$cashRegisterMapper->checktransaction($month,$year,$consumer["consumer_id"]);		
					if($cash){
						$dateCurr = new Zend_Date($cash["timestamp"],'yyyy-MM-dd');
						$timestamp = $dateCurr->toString("ddMMYYss".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));
						
						$cr_amounts = str_pad(round($ActualAmount), 4, "0", STR_PAD_LEFT);
						$transaction_id = $timestamp . "-" . $cr_amounts;
						$cashRegisterMapper->updateTransaction($cash["cr_id"],$transaction_id,$ActualAmount);
						
					}else{
						$timestamp_val=$year."-".$month."-".$total_days_in_month." 00:00:00";
						$dateCurr = new Zend_Date($timestamp_val,'yyyy-MM-dd');
						$dateCurr->setTimezone("Asia/Kolkata");
						$timestamp = $dateCurr->toString("ddMMYYss".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));
						
						$cr_amounts = str_pad(round($ActualAmount), 4, "0", STR_PAD_LEFT);
						$transaction_id = $timestamp . "-" . $cr_amounts; 
						
						$cashRegister = new Application_Model_CashRegister();
						$cashRegister->__set("user_id", 0);
						$cashRegister->__set("consumer_id", $consumer["consumer_id"]);
						$cashRegister->__set("cr_entry_type", "DEBIT");
						$cashRegister->__set("entry_status", "CHG");
						$cashRegister->__set("cr_amount", round($ActualAmount));
						$cashRegister->__set("receipt_number", 0);
						$cashRegister->__set("transaction_id", $transaction_id);
						$cashRegister->__set("transaction_type", '(W)');
						$cashRegister->__set("timestamp", $timestamp_val); 
						$cashRegister->__set("package_id", $consumer["package_id"]); 
						
						$cashRegisterMapper->addNewCashRegister($cashRegister); 
					}
				} 
			
			$meta = array(
				"code" => 200,
				"message" => "SUCCESS"
			);
			$arr = array(
				"meta" => $meta,
				"data" => $reading_arr,
			);
		}catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	public function updateMeterNumberConsumerIdAction(){
    
        try {
			date_default_timezone_set('Asia/Kolkata');
            $request = $this->getRequest();
			$consumer_id = $request->getParam("consumer_id");
            $meter_no = $request->getParam("meter_no");
			$new_meter_no = $request->getParam("new_meter_no");
			$package_id=$request->getParam("package_id");
			
			$meterReadingMapper = new Application_Model_MeterReadingsMapper();
            $meterInventoryMapper = new Application_Model_MeterInventoryMapper();
			 
			
			$meter=$meterReadingMapper->updateMeterNumberByLastNumber($consumer_id,$new_meter_no,$meter_no);
			if($meter){
				if($meter_no!='undefined' && $meter_no!="" && $meter_no!='NULL' && $meter_no!=NULL){
					$inventory_last=$meterInventoryMapper->updateInventory($meter_no,2,$consumer_id);
				}
				$inventory_new=$meterInventoryMapper->updateInventory($new_meter_no,1,$consumer_id); 
				
				$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
				);
				$arr = array(
					"meta" => $meta,
				);
			}else{
				$meta = array(
					"code" => 300,
					"message" => "Failure"
				);
				$arr = array(
					"meta" => $meta,
				);
			}
			
			
		}catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
}    