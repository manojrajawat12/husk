<?php

class Api_ConsumersController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
		$this->_userId=$auth->getIdentity()->user_id;
    }
    public function testAction()
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $consumers = $consumersMapper->getAllPaidConsumers(4);
        echo "<pre>";
        print_r($consumers);
        echo "</pre>";
                
    }
    
    public function addConsumerAction(){
		
         try {
         	$packagemapper=new Application_Model_PackagesMapper();         	
            $request=$this->getRequest();
            $package_id=$request->getParam("id"); 
            $site_id=$request->getParam("site_id");
            $consumer_code=$request->getParam("code");
            $consumer_name=$request->getParam("consumer_name");
            $consumer_father_name=$request->getParam("father_name");
            $consumer_status=$request->getParam("status");
            $consumer_act_date=$request->getParam("act_date");
            $consumer_act_charge=$request->getParam("act_charge");
			$consumer_mtr_charge=$request->getParam("mtr_charge");
            $consumer_connection_id=$request->getParam("consumer_connection_id");
            $micro_entrprise_price=$request->getParam("micro_price");
            $suspension_date=$request->getParam("suspension_date");
            $start_reading = $request->getParam("meter_start_reading");
            $type_of_me=$request->getParam("type_of_me");
			$consumer_categorie=$request->getParam("consumer_categorie");
            $notes=$request->getParam("notes");
			$sms_opt_in=$request->getParam("sms_opt_in");
			$site_meter_id=$request->getParam("site_meter_id");
			$wattage=$request->getParam("watt_id");
			$credit_limit=$request->getParam("credit_limit");
			$price= $request->getParam("package_cost");
			$MM_unit= $request->getParam("MM_unit");
			$MM_charges= $request->getParam("MM_charges");
			$package_type= $request->getParam("package_type");
			$wattage_filter= $request->getParam("wattage_filter");
			$consumer_email=$request->getParam("consumer_email");
		    $consumer_number=$request->getparam("consumer_number");
		    $secure_deposite=$request->getparam("secure_deposite");
			$gender=$request->getparam("gender");
			$dob=$request->getparam("dob");
			$total_package=json_decode($request->getparam("total_package"));
			   
			$sitemapper = new Application_Model_SitesMapper();
			$sites=$sitemapper->getSiteById($site_id);
			$site_code = $sites->__get("site_code");
			$state_id = $sites->__get("state_id");
			 
			$is_micro_enterprise = 0;
			$type = 0;
			if (count($total_package)>0){
				foreach ($total_package as $total_packages){
					if($total_packages->package_type!=0){
						$is_micro_enterprise = 1;
						$type = 1;
					}
				}
			}
			
             $consumermapper=new Application_Model_ConsumersMapper();
             $consumer= new Application_Model_Consumers();
             $consumer->__set("package_id",NULL);
             $consumer->__set("site_id",$site_id);
             $consumer->__set("consumer_code",$consumer_code);
             $consumer->__set("consumer_name",$consumer_name);
             $consumer->__set("consumer_father_name",$consumer_father_name);
             $consumer->__set("consumer_status",$consumer_status);
             $zendDate = new Zend_Date($consumer_act_date,"dd-MM-yyyy");
             $consumer_act_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
             $consumer->__set("consumer_act_date",$consumer_act_date);
             $consumer->__set("consumer_act_charge",$consumer_act_charge);
             $consumer->__set("consumer_connection_id",$consumer_connection_id);
             $consumer->__set("is_micro_enterprise",$is_micro_enterprise);
             $consumer->__set("micro_entrprise_price",$micro_entrprise_price);
             $consumer->__set("meter_start_reading",$start_reading);
             $consumer->__set("wattage",$wattage);
             $date = NULL;
             if($suspension_date!="undefined")
             {
                $zendDate = new Zend_Date($suspension_date,"dd-MM-yyyy");
                $date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
             }
            $consumer->__set("suspension_date",$date);
            $consumer->__set("type_of_me",$type_of_me);
			$consumer->__set("consumer_categorie",$consumer_categorie);
            $consumer->__set("notes",$notes);
			$consumer->__set("sms_opt_in",$sms_opt_in);
			$consumer->__set("site_meter_id",$site_meter_id);
			$consumer->__set("credit_limit",$credit_limit);
			$consumer->__set("wattage_filter",$wattage_filter);
			$consumer->__set("create_type","(W)");
			$consumer->__set("created_by",$this->_userId); 
			$consumer->__set("mobile_no",$consumer_number);
		    $consumer->__set("email_id",$consumer_email);
		    $consumer->__set("gender",$gender);
			$zendDate = new Zend_Date($dob,"dd-MM-yyyy");
            $dob = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
			$consumer->__set("dob",$dob); 
			$consumer->__set("type",$type); 
			
            $consumer_id=$consumermapper->addNewConsumer($consumer);
			if($consumer_id){
				if(intval($consumer_mtr_charge)>0){
					$cashRegisterMapper=new Application_Model_CashRegisterMapper();
					$date = new Zend_Date();
					$date->setTimezone("Asia/Kolkata");
					$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
					
					$cr_amount = str_pad($consumer_mtr_charge, 4, "0", STR_PAD_LEFT);
					$transaction_id = $timestamp . "-" . $cr_amount;
					
					$cashRegister = new Application_Model_CashRegister();
					$cashRegister->__set("user_id", $this->_userId);
					$cashRegister->__set("consumer_id", $consumer_id);
					$cashRegister->__set("cr_entry_type", "DEBIT");
					$cashRegister->__set("cr_amount", $consumer_mtr_charge);
					$cashRegister->__set("receipt_number", "CA-".$this->_userId);
					$cashRegister->__set("transaction_id", $transaction_id);
					$cashRegister->__set("transaction_type", '(W)');
					$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd"));
					$cashRegister->__set("entry_status", "METER");
					$cashRegister->__set("remark", "METER Bill");
					$cashRegister->__set("package_id", NULL);
					 
					$cashRegisterMapper->addNewCashRegister($cashRegister);
				}
				
				if($secure_deposite>0){
					$cashRegisterMapper=new Application_Model_CashRegisterMapper();
					$date = new Zend_Date();
					$date->setTimezone("Asia/Kolkata");
					$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
					
					$cr_amount = str_pad($secure_deposite, 4, "0", STR_PAD_LEFT);
					$transaction_id = $timestamp . "-" . $cr_amount;
					
					 
					$cashRegister = new Application_Model_CashRegister();
					$cashRegister->__set("user_id", $this->_userId);
					$cashRegister->__set("consumer_id", $consumer_id);
					$cashRegister->__set("cr_entry_type", "CREDIT"); 
					$cashRegister->__set("cr_amount", $secure_deposite);
					$cashRegister->__set("receipt_number", "CA-".$this->_userId);
					$cashRegister->__set("transaction_id", $transaction_id);
					$cashRegister->__set("transaction_type", "(W)");
					$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd"));
					$cashRegister->__set("entry_status", "SD");
					$cashRegister->__set("remark", "Security deposit");
					$cashRegister->__set("package_id", NULL);
					 
					$cashRegisterMapper->addNewCashRegister($cashRegister);
				}
			
				$cpMapper=new Application_Model_ConsumersPackageMapper();
				$cp=new Application_Model_ConsumersPackage();
         	
				if (count($total_package)>0){
		    		foreach ($total_package as $total_packages){
						$cp->__set("package_id",$total_packages->package_id);
						$cp->__set("consumer_id",$consumer_id); 
						$cpMapper->addNewConsumersPackage($cp);
							
						if($total_packages->package_type!=0){
							$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
							$meterReading = new Application_Model_MeterReadings();
							$date = new Zend_Date();
							$date->setTimezone("Asia/Calcutta");
							$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
							$meterReading->__set("consumer_id",$consumer_id);
							$meterReading->__set("meter_reading",$total_packages->meter_start_reading);
							$meterReading->__set("unit",0);
							$meterReading->__set("timestamp",$timestamp);
							$meterReading->__set("start_reading",1);
							$meterReading->__set("meter_no",$total_packages->meter_no);
							$meterReading->__set("package_id",$total_packages->package_id);
							$meterReading->__set("entry_by",$this->_userId); 
							$meterReading->__set("entry_type",'W'); 
							$meterReadingsMapper->addNewMeterReading($meterReading);
							
							$MeterInventoryMapper = new Application_Model_MeterInventoryMapper();
							$updateInventory=$MeterInventoryMapper->updateMeterStatus($total_packages->meter_no,1);
						}
					}
				}
				
				$this->_logger->info("New Consumer ID ".$consumer_id." has been created in Consumers by ". $this->_userName.".");
             
           
            $siteMapper=new Application_Model_SitesMapper();
            $packagemapper=new Application_Model_PackagesMapper();
            $site_name=$siteMapper->getSiteById($site_id);
           
            $zendDate = new Zend_Date($consumer_act_date,"dd-MM-yyyy");
            $consumer_act = $zendDate->toString("MMM dd, yyyy");
            
            $data = array(
			   // 'package_name' => $package_name->__get("package_name"), 
				'site_name' => $site_name->__get("site_name"),  
				'package_id' => NULL,
				'site_id' => $site_id,
				'consumer_id' => $consumer_id,    
				'consumer_code' => $consumer_code,
				'consumer_name' => $consumer_name,
				'consumer_father_name' => $consumer_father_name,
				'consumer_status' => $consumer_status,
				'consumer_act_date' => $consumer_act,
				'consumer_act_charge' => $consumer_act_charge,
				'consumer_connection_id' => $consumer_connection_id,
				'is_micro_enterprise' => $is_micro_enterprise,
				'micro_entrprise_price' => $micro_entrprise_price,
				'suspension_date' => $suspension_date,
				'type_of_me' => $type_of_me,
				'notes' => $notes,
				'smsOptIn'=>$sms_opt_in,
				'meter_start_reading'=>$start_reading,
				'site_meter_id'=>$site_meter_id,
				'credit_limit'=>$credit_limit,
				'wallet_bal'=>floatval(0.00),
            		
			);
            
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }

    public function getConsumerByIdAction(){
      
         try {
             $request=$this->getRequest();
             $consumer_id=$request->getParam("id");
             $consumermapper=new Application_Model_ConsumersMapper();
			 $consumerPackMapper=new Application_Model_ConsumersPackageMapper();
			if($consumer=$consumermapper->getConsumerById($consumer_id)){
         	
         	$conPacks=$consumerPackMapper->getPackageByConsumerId($consumer_id);
         	$pack_id=array();
         	if($conPacks){
         		foreach ($conPacks as $conPack){
         			$pack_id[]=$conPack["package_id"];
         		}
         	}
			if($consumer->__get("dob")!="" && $consumer->__get("dob")!=NULL){
				$zendDate = new Zend_Date($consumer->__get("dob"),"yyyy-MM-dd HH:mm:ss");
				$dob = $zendDate->toString("dd-MM-yyyy");
			}else{
				$dob=NULL;
			}
            $data = array(
				'package_id' => implode(",", $pack_id),
				'site_id' => $consumer->__get("site_id"),
				'consumer_code' => $consumer->__get("consumer_code"),
				'consumer_name' => ucwords(strtolower($consumer->__get("consumer_name"))),
				'consumer_father_name' => $consumer->__get("consumer_father_name"),
				'consumer_status' => $consumer->__get("consumer_status"),
				'consumer_act_date' => $consumer->__get("consumer_act_date"),
				'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
				'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
				'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
				'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
				'suspension_date' => $consumer->__get("suspension_date"),
				'enterprise_type' => $consumer->__get("enterprise_type"),
				'type_of_me' => $consumer->__get("type_of_me"),
				'type_of_me' => $consumer->__get("type_of_me"),
				'notes' => $consumer->__get("notes"),
				'sms_opt_in' => $consumer->__get("sms_opt_in"),
				"meter_start_reading"=>intval($consumer->__get("meter_start_reading")),
				"site_meter_id"=>$consumer->__get("site_meter_id"),
				"credit_limit"=>$consumer->__get("credit_limit"),
				"wallet_bal"=>$consumer->__get("wallet_bal"),
				'mobile_no'=>$consumer->__get("mobile_no"),
				'email_id'=>$consumer->__get("email_id"), 
				'gender'=>$consumer->__get("gender"), 
				'dob'=>$dob,
				'type'=>$consumer->__get("type"), 
				'sub_type_of_me' => $consumer->__get("consumer_categorie"),
			 );
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
    public function deleteConsumerByIdAction(){
      
         try {
             $request=$this->getRequest();
             $consumer_id=$request->getParam("id");
        $celamedMapper=new Application_Model_CelamedMapper();
             $consumermapper=new Application_Model_ConsumersMapper();
         if($consumer=$consumermapper->deleteConsumerById($consumer_id)){
            $celamedMapper->deleteCelamedByConsumerId($consumer_id);
            $this->_logger->info("Consumer Id ".$consumer_id." has been deleted from Consumers by ". $this->_userName.".");
            
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
   public function getAllConsumersAction()
    {
        try 
        {
                $request=$this->getRequest();
                $pagination = $request->getParam('pagination');
                if($pagination==NULL){
                	$pagination='true';
                }
                $page = $request->getParam("page",1);
                $total_pages=0;
                $current_page=0;
                $total_consumers=0;
                $new_consumer=0;
                $invalid_consumer=0;
	            $auth=new My_Auth('user');
	            $user=$auth->getIdentity()->user_role;
	            $state_id=$auth->getIdentity()->state_id;
	            
	            $roleSession = new Zend_Session_Namespace('roles');
	            $role_sites_id=$roleSession->site_id;
	            
                $consumermapper=new Application_Model_ConsumersMapper();
                if($consumers=$consumermapper->getAllConsumers(false,NULL,$state_id,$role_sites_id)){
                 if($pagination=='true'){
                    $total_consumers = count($consumers);
					
                    foreach ($consumers as $consumer) {
                    	if($consumer->__get("consumer_status")=='new'){
                    		$new_consumer=$new_consumer+1;
                    	}
                    	if($consumer->__get("consumer_status")=='banned'){
                    		$invalid_consumer=$invalid_consumer+1;
                    	}
                    }
                    $paginator=Zend_Paginator::factory($consumers);
                    $paginator->setItemCountPerPage(50);
                    $paginator->setCurrentPageNumber($page);
                    $consumers=$paginator;
                    $total_pages=$paginator->count();
                    if($page>$total_pages)
                    {
                        throw new Exception("No more pages",555);
                    }
                    $current_page=$paginator->getCurrentPageNumber(); 
                	}
					$totalOutSites=0;
                    foreach ($consumers as $consumer) {
                        $siteMapper=new Application_Model_SitesMapper();
                        $packageMapper=new Application_Model_PackagesMapper();
                        $site_name=$siteMapper->getSiteById($consumer->__get("site_id"));
                        //$package_name=$packageMapper->getPackageById($consumer->__get("package_id"));
                        $consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
                        $consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
                        
                        
                        $packageName=array();$is_postpaid= null;$package_id= array();
                        if($consumerpackage){
                       
                        	foreach($consumerpackage as $package){
                        		//$package_name = $packageMapper->getPackageById($package["package_id"]);
                        		$packageName[]=$package["package_name"];
                        		$is_postpaid=$package["is_postpaid"];
                        		$package_id[]=$package["package_id"];
                        	}
                        }
                        $Consumermapper=new Application_Model_ConsumerSchemeMapper();
    					$consumerscheme=$Consumermapper->getConsumerschemeByConsumerId($consumer->__get("consumer_id"));
    					$consumer_scheme=null;
    					$consumer_scheme_id=null;
						$sms_opt_in;
    					if($consumer->__get("sms_opt_in")=="1"){
							$sms_opt_in=true;
						}else{
							$sms_opt_in=false;
						}
    					if($consumerscheme)
    					{
    						$consumer_scheme=$consumerscheme->__get("consumer_scheme");
    						$consumer_scheme_id= $consumerscheme->__get("consumer_scheme_id");
    					}
    					
                       /*  if(!$package_name){
                            $package_name=new Application_Model_Packages();
                        } */
                        $date = new Zend_Date($consumer->__get("consumer_act_date"),"yyyy-MM-dd HH:mm:ss");
                        //$date->setTimezone("Asia/Calcutta");
                        $timestamp = $date->toString("MMM dd, yyyy");
                        
                        if($is_postpaid!=0){
                        	/*$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
                        	$meter_val=$meterReadingsMapper->getLatestReadingByConsumerId($consumer->__get("consumer_id"));
                        	if($meter_val){
                        		$meter_reading=abs($meter_val->__get("meter_reading"));
                        	}else{*/
                        		$meter_reading=0;
                        	//}
                        }else{
                        		$meter_reading=intval($consumer->__get("meter_start_reading"));
                        	}
                        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                        $siteOutstaning=$cashRegisterMapper->getMonthlyDueBySiteId($consumer->__get("site_id"),null,null,null,$consumer->__get("consumer_id"));
						$totalOutSite=intval($siteOutstaning["debit_amount"])-intval($siteOutstaning["credit_amount"]);
						$totalOutSites=$totalOutSites+$totalOutSite;
                    	$data = array(
                        'package_name' =>implode(",", $packageName),
                        'site_name' => $site_name->__get("site_name"),  
                        'consumer_id' => $consumer->__get("consumer_id"),    
                        'package_id' => implode(",", $package_id),
                        'site_id' => $consumer->__get("site_id"),
                        'consumer_code' => $consumer->__get("consumer_code"),
                        'consumer_name' => $consumer->__get("consumer_name"),
                        'consumer_father_name' => $consumer->__get("consumer_father_name"),
                        'consumer_status' => $consumer->__get("consumer_status"),
                        'consumer_act_date' => $timestamp,
                        'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
                        'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
                        'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
                        'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
                        'suspension_date' => $consumer->__get("suspension_date"),
                    	'enterprise_type' => $consumer->__get("enterprise_type"),
                    	'type_of_me' => $consumer->__get("type_of_me"),
                    	'notes' => $consumer->__get("notes"),
						'smsOptIn' => $sms_opt_in,
                    	'consumer_scheme' =>$consumer_scheme,
                    	'consumer_scheme_id' => $consumer_scheme_id,
                    	"meter_start_reading"=>intval($meter_reading),
                    	"site_meter_id"=>$consumer->__get("site_meter_id"),
                    	"credit_limit"=>$consumer->__get("credit_limit"),
                    	"wallet_bal"=>$consumer->__get("wallet_bal"),
                    	"package_type"=>	$is_postpaid,	
                    	"wattage"=> $consumer->__get("wattage"),
						"totalOutSite"=>$totalOutSite
                    	 
                    );
                    $consumer_arr[]=$data;
                }
				$total_consumers= $total_consumers-($new_consumer +$invalid_consumer);
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                    "total_page" => $total_pages,
                    "current_page" =>$current_page,
                    "total_consumers" => $total_consumers,
                	"new_consumer" => $new_consumer,
                	"invalid_consumer" => $invalid_consumer,
					"total_out"=>$totalOutSites
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    
    public function getConsumerStatsAction()
    {
        try 
        {
            $request=$this->getRequest();
            $consumer_count_act = 0;
            $consumer_suspension_count = 0;
            $consumer_banned_count = 0;
            $consumermapper=new Application_Model_ConsumersMapper();
            $date=new Zend_Date();
            $date->setTimezone("Asia/Calcutta");
            $mounth=$date->get("MM"); 
            $consumer_act_date=$consumermapper->getConsumersCountWithActDate($mounth);
            $consumer_suspension=$consumermapper->getConsumersCountWithSuspensionDate($mounth);
            if($consumer_act_date)
            {
                $consumer_count_act=  count($consumer_act_date);
            }
            if($consumer_suspension)
            {
                $consumer_suspension_count = count($consumer_suspension);
            }
            $data=array(
                "consumer_count_act" => $consumer_count_act,
                "consumer_count_suspension" => $consumer_suspension_count,
            );
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                   
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data,
                );
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function getConsumersBySiteIdAction()
    {
        try 
        {
                $request=$this->getRequest();
                $page = $request->getParam("page",1);
                $site_id = $request->getParam("site_id");
                $order = $request->getParam("order");
				$type_val = $request->getParam("type",1);
                $total_pages=0;
                $current_page=0;
                //$consumer_id=$request->getParam("id");
                $consumermapper=new Application_Model_ConsumersMapper();
				$roleSession = new Zend_Session_Namespace('roles');
	            $role_sites_id=$roleSession->site_id;
	            $meterReadingsMapper=new Application_Model_MeterReadingsMapper();
                if($site_id=="All")
                {
                    $consumers = $consumermapper->getAllConsumers(false,null,null,$role_sites_id,null); 
                }
                else
                {
                	if($order==1){
						$consumers=$consumermapper->getConsumersByColumnValue("site_id", $site_id,true,null,null,null,$order);
                	}else{
						$consumers=$consumermapper->getConsumersByColumnValue("site_id", $site_id,true,null,null,null,$order);
                		 
                	}
                }
                if($consumers)
                {
                    $total_consumers = count($consumers);
					$new_consumer=0;
                    $invalid_consumer=0; 
                    foreach ($consumers as $consumer) {
                    	if($consumer->__get("consumer_status")=='new'){
                    		$new_consumer=$new_consumer+1;
                    	}
                    	if($consumer->__get("consumer_status")=='banned'){
                    		$invalid_consumer=$invalid_consumer+1;
                    	}
                    }
					if($order!=1){
						$paginator=Zend_Paginator::factory($consumers);
						$paginator->setItemCountPerPage(100);
						$paginator->setCurrentPageNumber($page);
						$consumers=$paginator;
						$total_pages=$paginator->count();
						if($page>$total_pages)
						{
							throw new Exception("No more pages",555);
						}
						$current_page=$paginator->getCurrentPageNumber(); 
					}
					
					
					$totalOutSites=0;
                    foreach ($consumers as $consumer) {
                        $date = new Zend_Date($consumer->__get("consumer_act_date"),"yyyy-MM-dd HH:mm:ss");
                        //$date->setTimezone("Asia/Calcutta");
                        $timestamp = $date->toString("MMM dd, yyyy");
                        $siteMapper=new Application_Model_SitesMapper();
                        $packageMapper=new Application_Model_PackagesMapper();
                        $site_name=$siteMapper->getSiteById($consumer->__get("site_id"));
                        $consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
                        $consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
                        
                        $packageName=array();$is_postpaid=NULL;
						if($consumer->__get("sms_opt_in")=="1"){
							$sms_opt_in=true;
						}else{
							$sms_opt_in=false;
						}
                    	$package_id=array();
						$totalCost=0;
						 $wattagesMapper=new Application_Model_WattageMapper();
                        $totalWatt=0;$type=0;
						$meter_no=NULL;$Pack_id=NULL;
						$package_arr=array();
						if($consumerpackage){
                        	foreach($consumerpackage as $package){
								$package_ar=array(
									"package_id"=>$package["package_id"],
									"package_name"=>$package["package_name"],
									"is_postpaid"=>$package["is_postpaid"]
								);
								if($package["is_postpaid"]!=0){
									$meter_val=$meterReadingsMapper->getLatestReadingByPackageConsumerId($consumer->__get("consumer_id"),$package["package_id"]);
									if($meter_val){
										$meter_no=$meter_val->__get("meter_no");
										$Pack_id=$package["package_id"];
									}
								}
								$package_arr[]=$package_ar;
                        		$packageName[]=$package["package_name"];
                        		$is_postpaid=$package["is_postpaid"];
                        		$package_id[]=$package["package_id"];
								$totalCost=$totalCost+$package["package_cost"];
								$wattage=$wattagesMapper->getWattageBywattId($package["wattage"]);
                        		$totalWatt=$totalWatt+$wattage->__get("wattage");
								if($package["is_postpaid"]!=0){
									$type=2;
								}
                        	}
						}
                      
                        
                        $ConsumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
                       
                        $consumer_scheme=null; 
                        $consumer_scheme_id=null;
                        
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
                        $siteOutstaning=$cashRegisterMapper->getMonthlyDueBySiteId($consumer->__get("site_id"),null,null,null,$consumer->__get("consumer_id"));
						$totalOutSite=intval($siteOutstaning["debit_amount"])-intval($siteOutstaning["credit_amount"]);
						$totalOutSites=$totalOutSites+$totalOutSite;
						                      
                    $data = array(
                        'package_name' => implode(",", $packageName),
                        'site_name' => $site_name->__get("site_name"),  
                        'consumer_id' => $consumer->__get("consumer_id"),    
                        'package_id' => implode(",", $package_id),
                        'site_id' => $consumer->__get("site_id"),
                        'consumer_code' => $consumer->__get("consumer_code"),
                        'consumer_name' => ucwords(strtolower($consumer->__get("consumer_name"))),
                        'consumer_father_name' => $consumer->__get("consumer_father_name"),
                        'consumer_status' => $consumer->__get("consumer_status"),
                        'consumer_act_date' => $timestamp,
                        'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
                        'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
                         'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
                        'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
                        'suspension_date' => $consumer->__get("suspension_date"),
                    	'enterprise_type' => $consumer->__get("enterprise_type"),
                    	'type_of_me' => $consumer->__get("type_of_me"),
                    	'notes' => $consumer->__get("notes"),
                    	'consumer_scheme' =>$consumer_scheme,
                    	'consumer_scheme_id' => $consumer_scheme_id,
						'smsOptIn' => $sms_opt_in,
						"totalOutSite"=>$totalOutSite,
						"type"=>$type,
						"package_arr"=>$package_arr,
						"meter_no"=>$meter_no,
						"pack_id"=>$Pack_id
                   
                    );  
					if($type_val==0 && $type!=0){
						$consumer_arr[]=$data;
					}
					if($type_val!=0){
						$consumer_arr[]=$data;
					}
                }
				$total_consumers= $total_consumers-($new_consumer +$invalid_consumer);
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                    "total_page" => $total_pages,
                    "current_page" =>$current_page,
                    "total_consumers" => $total_consumers,
                	"new_consumer" => $new_consumer,
                	"invalid_consumer" => $invalid_consumer,
					"totalOut"=>$totalOutSites
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "No consumers found"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function getSparkConsumersBySiteIdAction()
    {
        try 
        {
                $request=$this->getRequest();
                $page = $request->getParam("page",1);
                $site_id = $request->getParam("site_id");
                $order = $request->getParam("order");
				//$consumer_search = $request->getParam("consumer_search");
                $total_pages=0;
                $current_page=0;
                //$consumer_id=$request->getParam("id");
                $consumermapper=new Application_Model_ConsumersMapper();
                
                $consumers=$consumermapper->getConsumersByColumnValue("site_id", $site_id,true,null,null,null,null,null); 
                
                if($consumers)
                {
                    $total_consumers = count($consumers);
					$new_consumer=0;
                    $invalid_consumer=0;
                    foreach ($consumers as $consumer) {
                    	if($consumer->__get("consumer_status")=='new'){
                    		$new_consumer=$new_consumer+1;
                    	}
                    	if($consumer->__get("consumer_status")=='banned'){
                    		$invalid_consumer=$invalid_consumer+1;
                    	}
                    }
					//if($consumer_search=="" && $consumer_search==NULL && $consumer_search=='undefined'){
						$paginator=Zend_Paginator::factory($consumers);
						if($page==1){
							$paginator->setItemCountPerPage(20);   
						}else{
							$paginator->setItemCountPerPage(10); 
						}
						$paginator->setCurrentPageNumber($page);
						$consumers=$paginator;
						$total_pages=$paginator->count();
						if($page>$total_pages)
						{
							throw new Exception("No more pages",555);
						}
						$current_page=$paginator->getCurrentPageNumber(); 
					//}
					
					 
                    foreach ($consumers as $consumer) {
						
                       $date = new Zend_Date($consumer->__get("consumer_act_date"),"yyyy-MM-dd HH:mm:ss");
                        //$date->setTimezone("Asia/Calcutta");
                        $timestamp = $date->toString("MMM dd, yyyy");
                        $siteMapper=new Application_Model_SitesMapper();
                        $packageMapper=new Application_Model_PackagesMapper();
                        $site_name=$siteMapper->getSiteById($consumer->__get("site_id"));
                        $consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
                        $consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
                        
                        $packageName=array();$is_postpaid=NULL;
                    
                    	$package_id=array();
						$totalCost=0;
						 $wattagesMapper=new Application_Model_WattageMapper();
                        $totalWatt=0;$ispostpaid=0;
						$package_id_value=0;
						if($consumerpackage){
                        	foreach($consumerpackage as $package){
                        		//$package_name = $packageMapper->getPackageById($package["package_id"]);
                        		$packageName[]=$package["package_name"];
                        		$is_postpaid=$package["is_postpaid"];
                        		$package_id[]=$package["package_id"];
								$totalCost=$totalCost+$package["package_cost"];
								$wattage=$wattagesMapper->getWattageBywattId($package["wattage"]);
                        		$totalWatt=$totalWatt+$wattage->__get("wattage");
								if($package["package_id"]!=0){
									$ispostpaid=1;
								} 
								if($package["is_postpaid"]!=0){
									$package_id_value=$package["package_id"];
								}
                        	}
						}
                      
                        $ConsumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
                        $consumerscheme=$ConsumerSchemeMapper->getConsumerschemeByConsumerId($consumer->__get("consumer_id"));
                        $consumer_scheme=null; 
                        $consumer_scheme_id=null;
                        
                        if($consumerscheme)
                        {
                        	$consumer_scheme=$consumerscheme->__get("consumer_scheme");
                        	$consumer_scheme_id= $consumerscheme->__get("consumer_scheme_id");
                        }
                        $avgUnit=0;$remaing_bal=0;
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$siteOutstaning=$cashRegisterMapper->getMonthlyDueBySiteId($consumer->__get("site_id"),null,null,null,$consumer->__get("consumer_id"));
						$mainBalance=intval($siteOutstaning["debit_amount"])-intval($siteOutstaning["credit_amount"]);
						 
						$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
                        $spark = $request->getParam("spark");
                        if($spark!=NULL){
                        	$year = $request->getParam("year");
                        	$month_val = $request->getParam("month");
                        	$month = date("m", strtotime($month_val));
                            
                        	$meter_val=$meterReadingsMapper->getTotalUnitSiteWise($consumer->__get("consumer_id"),$month,$year);
                        	$meters=$meterReadingsMapper->getMeterReadingsByConsumer($consumer->__get("consumer_id"));
                        	if($meters){
								$units=$meterReadingsMapper->getTotalUnitByConsumerId($consumer->__get("consumer_id"),$package_id_value,$month,$year); 
	                        	$operating_mode=$meters[0]->__get("operating_mode"); 
	                        	$is_running_plan=$meters[0]->__get("is_running_plan");
	                        	$package_name=$meters[0]->__get("package_name");
	                        	$serial=$meters[0]->__get("serial");
	                        	$unit=$units;//$meters[0]->__get("unit");
	                        	$timestamp_reading=$meters[0]->__get("timestamp");
								$zendDates = new Zend_Date($timestamp_reading,"yyyy-MM-dd HH:mm:ss"); 
								$timestamp_reading=$zendDates->toString("dd-MMM-yyyy HH:mm"); 
								if($meter_val){
	                        		$curr_reading=sprintf("%.2f",$meter_val["curr_reading"]);
	                        		$last_reading=sprintf("%.2f",$meter_val["last_reading"]);
	                        	}else{
	                        		$curr_reading=0; $last_reading=0;
	                       		}
								$avgUnit=0;
								
								$packages = $packageMapper->getPackageById($meters[0]->__get("package_id"));
								if($packages){
									if($packages->__get("is_postpaid")==0){
										$avgUnit=0;
										$remaing_bal=$consumer->__get("credit_limit")-$avgUnit-$mainBalance; 
									}elseif($packages->__get("is_postpaid")==1){
										$avgUnit=$unit*intval($consumer->__get("micro_entrprise_price"));
										$remaing_bal=$consumer->__get("credit_limit")-$avgUnit-$mainBalance; 
									}else{
										 
										if($unit<=$packages->__get("unit")){
											$avgUnit=($packages->__get("package_cost")/$packages->__get("unit"))*$unit;
											//$avgUnit=($unit/$total_days_in_month;
										}else{
											$avgUnit=$packages->__get("package_cost")+(($unit-$packages->__get("unit"))*$packages->__get("extra_charges"));
										}
										
									}
								} 
								$remaing_bal=$consumer->__get("credit_limit")-$mainBalance-$avgUnit; 
                       		}else{
                       			$curr_reading=0; $last_reading=0;
                       			$operating_mode=NULL; $is_running_plan=NULL; $package_name=NULL;
	                        	$serial=NULL; $unit=NULL; $timestamp_reading=NULL;
                       		}
                        }else{
                        	$curr_reading=0; $last_reading=0;
                        	$operating_mode=NULL; $is_running_plan=NULL; $package_name=NULL;
                        	$serial=NULL; $unit=NULL; $timestamp_reading=NULL;
                        }
						if($serial!=NULL){
							if($is_running_plan==1){
								$is_running_plan="ON";
							}else{
								$is_running_plan="OFF";
							}
							if($operating_mode==0){
								$operating_mode='OFF';
							}elseif($operating_mode==1){
								$operating_mode='ON';
							}else{
								$operating_mode='Auto';
							}
						}else{
							$operating_mode='OFF';
							$is_running_plan="OFF";
						}
						
						
						
	                    $cashRegMapper=new Application_Model_CashRegisterMapper();
	                    $cashRegs=$cashRegMapper->getOtherAmtByConsumerId($consumer->__get("consumer_id"),'SD');
						$MMmapper=new Application_Model_SiteMasterMeterMapper();
					 	$feedarDetail=$MMmapper->getMMById($consumer->__get("site_meter_id"));
						if($feedarDetail){
							$feeder= $feedarDetail->__get("feeder_type");
						}else{
							$feeder="";
						}		 
						
					                   
                    $data = array(
                        'package_name' => implode(",", $packageName),
                        'site_name' => $site_name->__get("site_name"),  
                        'consumer_id' => $consumer->__get("consumer_id"),    
                        'package_id' => implode(",", $package_id),
                        'site_id' => $consumer->__get("site_id"),
                        'consumer_code' => $consumer->__get("consumer_code"),
                        'consumer_name' => ucwords(strtolower($consumer->__get("consumer_name"))),
                        'consumer_father_name' => $consumer->__get("consumer_father_name"),
                        'consumer_status' => $consumer->__get("consumer_status"),
                        'consumer_act_date' => $timestamp,
                        'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
                        'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
                         'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
                        'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
                        'suspension_date' => $consumer->__get("suspension_date"),
                    	'enterprise_type' => $consumer->__get("enterprise_type"),
                    	'type_of_me' => $consumer->__get("type_of_me"),
                    	'notes' => $consumer->__get("notes"),
                    	'consumer_scheme' =>$consumer_scheme,
                    	'consumer_scheme_id' => $consumer_scheme_id,
						'smsOptIn' => $sms_opt_in,
                    	"meter_start_reading"=>intval($meter_reading),
                    	"site_meter_id"=>$consumer->__get("site_meter_id"),
                    	"credit_limit"=>$consumer->__get("credit_limit"),
                    	"wallet_bal"=>$consumer->__get("wallet_bal"),
                    	"package_type"=>	$is_postpaid,	
                    	"wattage"=> $consumer->__get("wattage"),
						"curr_reading"=>sprintf("%.2f",$curr_reading),
                    	"last_reading"=>sprintf("%.2f",$last_reading),
                    	"operating_mode"=>$operating_mode,
                    	"unit"=>$unit,
                    	"is_running_plan"=>$is_running_plan,
                    	"package_name_spark"=>$package_name,
                    	"timestamp_reading"=>$timestamp_reading,
                    	"SD"=>abs($cashRegs),
						"package_cost"=>$totalCost." / (".$totalWatt.")"." / (".$feeder.")", 
						"curr_bal"=>sprintf("%.2f",$mainBalance+$avgUnit),
						"remaing_bal"=>sprintf("%.2f",$remaing_bal),
						"main_bal"=>$mainBalance,
						"serial"=>$serial
                    );
					 
                    $consumer_arr[]=$data;
                }
				$total_consumers= $total_consumers-($new_consumer +$invalid_consumer);
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                    "total_page" => $total_pages,
                    "current_page" =>$current_page,
                    "total_consumers" => $total_consumers,
                	"new_consumer" => $new_consumer,
                	"invalid_consumer" => $invalid_consumer
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "No consumers found"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
		 
	public function getSparkConsumerDetailBySiteIdAction()
    {
        try 
        {
                $request=$this->getRequest();
                $page = $request->getParam("page",0);
                $site_id = $request->getParam("site_id");
                $consumer_search = $request->getParam("consumer_search");
				$filter = $request->getParam("filter");
				if($filter=='All'){
					$filter=0;
				}else{
					$filter=1;
				}
                $consumermapper=new Application_Model_ConsumersMapper();
                $siteMapper=new Application_Model_SitesMapper();
				$packageMapper=new Application_Model_PackagesMapper();
				$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
				$wattagesMapper=new Application_Model_WattageMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
                        
						
                $consumers=$consumermapper->getConsumerBySiteValue($site_id,$page,$consumer_search,$filter); 
				$consumers_count=$consumermapper->getConsumerCountBySiteValue($site_id); 
                $new_consumer = intval($consumers_count["new"]);
				$invalid_consumer = intval($consumers_count["banned"]);
				$total_active = intval($consumers_count["active"]);
				$total_new = $new_consumer+$invalid_consumer+$total_active;
				$MMmapper=new Application_Model_SiteMasterMeterMapper();  	 
                if(isset($consumers))
                {
					foreach ($consumers as $key => $consumer) {
						 
                        $siteMapper=new Application_Model_SitesMapper();
                        $packageMapper=new Application_Model_PackagesMapper();
                        $site_name=$siteMapper->getSiteById($consumer["site_id"]);
                        $consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
                        $consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer["consumer_id"]);
                        
                        $packageName=array();$is_postpaid=NULL;
                    
                    	$package_id=array();
						$totalCost=0;
						 $wattagesMapper=new Application_Model_WattageMapper();
                        $totalWatt=0;$ispostpaid=0;
						$package_id_value=0;
						if(isset($consumerpackage)){
                        	foreach($consumerpackage as $key => $package){
                        		//$package_name = $packageMapper->getPackageById($package["package_id"]);
                        		$packageName[]=$package["package_name"];
                        		$is_postpaid=$package["is_postpaid"];
                        		$package_id[]=$package["package_id"];
								$totalCost=$totalCost+$package["package_cost"];
								$wattage=$wattagesMapper->getWattageBywattId($package["wattage"]);
                        		$totalWatt=$totalWatt+$wattage->__get("wattage");
								if($package["package_id"]!=0){
									$ispostpaid=1;
								} 
								if($package["is_postpaid"]!=0){
									$package_id_value=$package["package_id"];
								}
                        	}
						}
                      
                        $avgUnit=0;$remaing_bal=0;
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$siteOutstaning=$cashRegisterMapper->getMonthlyDueBySiteId($consumer["site_id"],null,null,null,$consumer["consumer_id"]);
						$mainBalance=intval($siteOutstaning["debit_amount"])-intval($siteOutstaning["credit_amount"]);
						 
						$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
                        $spark = $request->getParam("spark");
                        if($spark!=NULL){
                        	$year = $request->getParam("year");
                        	$month_val = $request->getParam("month");
                        	$month = date("m", strtotime($month_val));
                            
                        	$meter_val=$meterReadingsMapper->getTotalUnitSiteWise($consumer["consumer_id"],$month,$year);
                        	$meters=$meterReadingsMapper->getMeterReadingsByConsumer($consumer["consumer_id"]);
                        	if($meters){
								$units=$meterReadingsMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$package_id_value,$month,$year); 
	                        	$operating_mode=$meters[0]->__get("operating_mode"); 
	                        	$is_running_plan=$meters[0]->__get("is_running_plan");
	                        	$package_name=$meters[0]->__get("package_name");
	                        	$serial=$meters[0]->__get("serial");
	                        	$unit=$units;//$meters[0]->__get("unit");
	                        	$timestamp_reading=$meters[0]->__get("timestamp");
								$zendDates = new Zend_Date($timestamp_reading,"yyyy-MM-dd HH:mm:ss"); 
								$timestamp_reading=$zendDates->toString("dd-MMM-yyyy HH:mm"); 
								if($meter_val){
	                        		$curr_reading=sprintf("%.2f",$meter_val["curr_reading"]);
	                        		$last_reading=sprintf("%.2f",$meter_val["last_reading"]);
	                        	}else{
	                        		$curr_reading=0; $last_reading=0;
	                       		}
								$avgUnit=0;
								
								$packages = $packageMapper->getPackageById($meters[0]->__get("package_id"));
								if($packages){
									if($packages->__get("is_postpaid")==0){
										$avgUnit=0;
										$remaing_bal=$consumer["credit_limit"]-$avgUnit-$mainBalance; 
									}elseif($packages->__get("is_postpaid")==1){
										$avgUnit=$unit*intval($consumer["micro_entrprise_price"]);
										$remaing_bal=$consumer["credit_limit"]-$avgUnit-$mainBalance; 
									}else{
										 
										if($unit<=$packages->__get("unit")){
											$avgUnit=($packages->__get("package_cost")/$packages->__get("unit"))*$unit;
											//$avgUnit=($unit/$total_days_in_month;
										}else{
											$avgUnit=$packages->__get("package_cost")+(($unit-$packages->__get("unit"))*$packages->__get("extra_charges"));
										}
										
									}
								} 
								$remaing_bal=$consumer["credit_limit"]-$mainBalance-$avgUnit; 
                       		}else{
                       			$curr_reading=0; $last_reading=0;
                       			$operating_mode=NULL; $is_running_plan=NULL; $package_name=NULL;
	                        	$serial=NULL; $unit=NULL; $timestamp_reading=NULL;
                       		}
                        }else{
                        	$curr_reading=0; $last_reading=0;
                        	$operating_mode=NULL; $is_running_plan=NULL; $package_name=NULL;
                        	$serial=NULL; $unit=NULL; $timestamp_reading=NULL;
                        }
						if($serial!=NULL){
							if($is_running_plan==1){
								$is_running_plan="ON";
							}else{
								$is_running_plan="OFF";
							}
							if($operating_mode==0){
								$operating_mode='OFF';
							}elseif($operating_mode==1){
								$operating_mode='ON';
							}else{
								$operating_mode='Auto';
							}
						}else{
							$operating_mode='OFF';
							$is_running_plan="OFF";
						}
						
						
						
	                    $cashRegMapper=new Application_Model_CashRegisterMapper();
	                    $cashRegs=$cashRegMapper->getOtherAmtByConsumerId($consumer["consumer_id"],'SD');
						$MMmapper=new Application_Model_SiteMasterMeterMapper();
					 	$feedarDetail=$MMmapper->getMMById($consumer["site_meter_id"]);
						if($feedarDetail){
							$feeder= $feedarDetail->__get("feeder_type");
						}else{
							$feeder="";
						}		 
						
					                   
						$data = array(
							'package_name' => implode(",", $packageName),
							'site_name' => $site_name->__get("site_name"),  
							'consumer_id' => $consumer["consumer_id"],    
							'package_id' => implode(",", $package_id),
							'site_id' => $consumer["site_id"],
							'consumer_code' => $consumer["consumer_code"],
							'consumer_name' => ucwords(strtolower($consumer["consumer_name"])),
							'consumer_father_name' => $consumer["consumer_father_name"],
							'consumer_status' => $consumer["consumer_status"],
							'consumer_connection_id' => $consumer["consumer_connection_id"],
							
							"meter_start_reading"=>intval($meter_reading),
							"credit_limit"=>$consumer["credit_limit"],
							"wallet_bal"=>$consumer["wallet_bal"],
							"package_type"=>	$is_postpaid,	
							
							"curr_reading"=>sprintf("%.2f",$curr_reading),
							"last_reading"=>sprintf("%.2f",$last_reading),
							"operating_mode"=>$operating_mode,
							"unit"=>$unit,
							"is_running_plan"=>$is_running_plan,
							"package_name_spark"=>$package_name,
							"timestamp_reading"=>$timestamp_reading,
							"SD"=>abs($cashRegs),
							"package_cost"=>$totalCost." / (".$totalWatt.")"." / (".$feeder.")", 
							"curr_bal"=>sprintf("%.2f",$mainBalance+$avgUnit),
							"remaing_bal"=>sprintf("%.2f",$remaing_bal),
							"main_bal"=>$mainBalance,
							"serial"=>$serial
						);
					 if($filter==0){
						$consumer_arr[]=$data;
					 }elseif($filter==1){
						$notional_bal=round($mainBalance+$avgUnit);
						$credit_limit=round($consumer["credit_limit"]);
						if($notional_bal>$credit_limit){
							$consumer_arr[]=$data; 
						}
					 } 
                    
                }
				if($filter==1){
					$total_active=count($consumer_arr); 
					$new_consumer=0;$invalid_consumer=0;
				}
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                    "total_page" => $total_pages,
                    "current_page" =>$current_page,
                    "total_consumers" => $total_active,
                	"new_consumer" => $new_consumer,
                	"invalid_consumer" => $invalid_consumer
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "No consumers found"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	 
	public function getSparkDetailBySiteIdAction()
    {
        try 
        {
                $request=$this->getRequest();
                $site_id = $request->getParam("site_id");
				$filter = $request->getParam("filter");
                $consumer_search = $request->getParam("consumer_search");
				 
                $consumermapper=new Application_Model_ConsumersMapper();
                $siteMapper=new Application_Model_SitesMapper();
				$packageMapper=new Application_Model_PackagesMapper();
				$wattagesMapper=new Application_Model_WattageMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
                $consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
				$MMmapper=new Application_Model_SiteMasterMeterMapper();  	
				        
				$packageChangeMapper=new Application_Model_PackageChangeMapper();
				$consumerActivityMapper=new Application_Model_ConsumerActivityMapper(); 	
				$packagesMapper=new Application_Model_PackagesMapper();
				$consumers=$consumermapper->getSparkConBySiteValue($site_id,$filter); 
				$consumers_count=$consumermapper->getConsumerCountBySiteValue($site_id); 
                $new_consumer = intval($consumers_count["new"]);
				$invalid_consumer = intval($consumers_count["banned"]);
				$total_active = intval($consumers_count["active"]);
				$total_new = $new_consumer+$invalid_consumer+$total_active;
				$totalCountVal=count($consumers)+1;
                if(isset($consumers))
                {
					foreach ($consumers as $key => $consumer) {
					 	//if($consumer["consumer_id"]==3868){
							$totalCountVal=$totalCountVal-1;
							$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer["consumer_id"]);
							$packageName=array();$totalCost=0;$totalWatt=0;$package_id_value=0;
							if(isset($consumerpackage)){
								$package_id_value=$consumerpackage[0]["package_id"];
								foreach($consumerpackage as $key => $package){
									$packageName[]=$package["package_name"];
									$totalCost=$totalCost+$package["package_cost"];
									$wattage=$wattagesMapper->getWattageBywattId($package["wattage"]);
									$totalWatt=$totalWatt+$wattage->__get("wattage");
									if($package["is_postpaid"]!=0){
										$package_id_value=$package["package_id"];
									} 
								}
							}
                       
							$avgUnit=0;$remaing_bal=0;$amount_val=0; $ActualAmount=0; 
							$mainBalance=intval($consumer["debit"])-intval($consumer["credit"]);
							  
							$date = new Zend_Date(); 
							$date->setTimezone("Asia/Calcutta");
							$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			
							$trans_date=date_parse_from_format("Y-m-d", $timestamp);
							$day=$trans_date["day"];
							$month= $trans_date["month"];
							$year= $trans_date["year"];
							
                            $total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
                        	$meters=$meterReadingsMapper->getMeterDetailsForSparkConsumerId($consumer["consumer_id"],$package_id_value,$month,$year); 
							 
						    
							if($meters){
								$operating_mode=$meters["operating_mode"]; 
	                        	$is_running_plan=$meters["is_running_plan"];
	                        	$package_name=$meters["package_name"];
	                        	$serial=$meters["meter_no"]; 
	                        	$unit=$meters["units"];
	                        	$timestamp_reading=$meters["timestamp"];
								$zendDates = new Zend_Date($timestamp_reading,"yyyy-MM-dd HH:mm:ss"); 
								$timestamp_reading=$zendDates->toString("dd-MMM-yyyy HH:mm"); 
								 
	                        	$curr_reading=sprintf("%.2f",$meters["curr_reading"]);
	                        	$last_reading=sprintf("%.2f",$meters["last_reading"]);
	                        	 
								$amount_val=0;
								$ActualAmount=0;
								/*$packages = $packageMapper->getPackageById($meters["package_id"]);
								if($packages){
									 
									if($packages->__get("is_postpaid")==0){
										$amount_val=0;
										$remaing_bal=$consumer["credit_limit"]-$amount_val-$mainBalance; 
									}else{
										if($packages->__get("is_postpaid")==2){
											$extra_charges =  $packages->__get("extra_charges");
											$fixed_unit =  $packages->__get("unit");
											$package_cost=  $packages->__get("package_cost");
										}else{
											$extra_charges =  $consumer["micro_entrprise_price"];
											$fixed_unit =  0;
											$package_cost=0; 
										}
										 
										$unit = $consumer["unit"];
											  
										$meterMapper=new Application_Model_MeterReadingsMapper();
										$Act_date = date_parse_from_format("Y-m-d", $consumer["consumer_act_date"]);
										$Act_day = $Act_date["day"];
										$Act_month = sprintf("%02d", $Act_date["month"]);
										$Act_year = $Act_date["year"];
										
										$consumerActivitys=$consumerActivityMapper->getActivityConsumer('reactive',$month,$year,$consumer["consumer_id"]);
										$ckeckActivitys=$packageChangeMapper->getAllPackageActivityOfConsumerInMonth($consumer["consumer_id"],$month,$year);
										
										if($consumerActivitys){
													$curr_packages = $packagesMapper->getPackageById($package_id_value);
													if($curr_packages->__get("is_postpaid")==2){
														$extra_charges = $curr_packages->__get("extra_charges");
														$fixed_unit =  $curr_packages->__get("unit");
														
														$change_date = date_parse_from_format("Y-m-d", $consumerActivitys["timestamp"]);
														$end_day = ($total_days_in_month-$change_date["day"]);
														$fixed_unit=($fixed_unit/$total_days_in_month)*$end_day;
														
														$amount_val=($curr_packages->__get("package_cost")/$total_days_in_month)*$end_day;
														$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$curr_packages->__get("package_id"),$month,$year);
														if($units){
															if($units>$fixed_unit){
																$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
															}else{
																$ActualAmount=$amount_val;
															}
														}else{
															$ActualAmount=$amount_val;
														}
													} 
													 
										}elseif($ckeckActivitys){
											foreach($ckeckActivitys as $ckeckActivity){
												if($ckeckActivity["activity"]=='change'){
													$curr_packages = $packagesMapper->getPackageById($ckeckActivity['curr_package_id']);
													if($curr_packages->__get("is_postpaid")==2){
														$extra_charges = $curr_packages->__get("extra_charges");
														$fixed_unit =  $curr_packages->__get("unit");
														
														$change_date = date_parse_from_format("Y-m-d", $ckeckActivity["timestamp"]);
														$end_day = $change_date["day"];
														$fixed_unit=($fixed_unit/$total_days_in_month)*$end_day;
														
														$amount_val=($curr_packages->__get("package_cost")/$total_days_in_month)*$end_day;
														$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$curr_packages->__get("package_id"),$month,$year);
														if($units){
															if($units>$fixed_unit){
																$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
															}else{
																$ActualAmount=$amount_val;
															}
														}else{
															$ActualAmount=$amount_val;
														}
													}
													//echo $ActualAmount;exit;
													$new_packages = $packagesMapper->getPackageById($ckeckActivity['new_package_id']);
													if($new_packages->__get("is_postpaid")==2){
														$extra_charges = $new_packages->__get("extra_charges");
														$fixed_unit =  $new_packages->__get("unit");
														
														$change_date = date_parse_from_format("Y-m-d", $ckeckActivity["timestamp"]);
														$end_day = $change_date["day"];
														$fixed_unit=($fixed_unit/$total_days_in_month)*($total_days_in_month-$end_day);;
														
														$amount_val=($new_packages->__get("package_cost")/$total_days_in_month)*($total_days_in_month-$end_day);
														$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$new_packages->__get("package_id"),$month,$year);
														if($units){
															if($units>$fixed_unit){
																$ActualAmount=$ActualAmount+$amount_val+(($units-$fixed_unit)*$extra_charges);
															}else{
																$ActualAmount=$ActualAmount+$amount_val;
															}
														}else{
															$ActualAmount=$ActualAmount+$amount_val;
														}
													}
												}elseif($ckeckActivity["activity"]=='new'){
													
													$assign_packages = $packagesMapper->getPackageById($ckeckActivity['assign_package']);
													if($assign_packages->__get("is_postpaid")==2){
														$extra_charges = $assign_packages->__get("extra_charges");
														$fixed_unit =  $assign_packages->__get("unit");
														
														$change_date = date_parse_from_format("Y-m-d", $ckeckActivity["timestamp"]);
														$end_day = $change_date["day"];
														$fixed_unit=($fixed_unit/$total_days_in_month)*($total_days_in_month-$end_day);
														
														$amount_val=($assign_packages->__get("package_cost")/$total_days_in_month)*($total_days_in_month-$end_day);
														$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$assign_packages->__get("package_id"),$month,$year);
														if($units){
															if($units>$fixed_unit){
																$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
															}else{
																$ActualAmount=$amount_val;
															}
														}else{
															$ActualAmount=$amount_val;
														}
													}
												}
											}												 												
										}else{
											
											if($month==$Act_month && $year==$Act_year){
												$amount_val=($package_cost/$total_days_in_month)*($day-$Act_day);
												$fixed_unit=($fixed_unit/$total_days_in_month)*($day-$Act_day); 
											}else{
												$amount_val=($package_cost/$total_days_in_month)*($day);
											}
											$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$package_id_value,$month,$year);
											
											
											if(floatval($units)>floatval($fixed_unit)){
												$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
											}else{
												$ActualAmount=$amount_val; 
											}
										}
										if(floatval($ActualAmount)<=0){
											if($month==$Act_month && $year==$Act_year){
												$amount_val=($package_cost/$total_days_in_month)*($day-$Act_day);
												$fixed_unit=($fixed_unit/$total_days_in_month)*($day-$Act_day); 
											}else{
												$amount_val=($package_cost/$total_days_in_month)*($day);
											}
											$units=$meterMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$package_id_value,$month,$year);
											
											if($units){
												if($units>$fixed_unit){
													$ActualAmount=$amount_val+(($units-$fixed_unit)*$extra_charges);
												}else{
													$ActualAmount=$amount_val;
												}
											}else{ 
												$ActualAmount=$amount_val;
											}
										}
										$remaing_bal=$consumer["credit_limit"]-$mainBalance-$ActualAmount; 
									}
								}*/
								$ActualAmount=floatval($consumer["notional_bal"]); 
								$remaing_bal=$consumer["credit_limit"]-$mainBalance-$ActualAmount; 
								
                       		}else{
                       			$curr_reading=0; $last_reading=0; $operating_mode=NULL;$package_name=NULL; $serial=NULL; $unit=NULL; $timestamp_reading=NULL;
                       		}
							
							if($serial!=NULL){
								if($operating_mode==0){ 
									$operating_mode='OFF';
								}elseif($operating_mode==1){
									$operating_mode='ON';
								}else{
									$operating_mode='Auto';
								}
							}else{
								$operating_mode='OFF';
							}
							
							$feedarDetail=$MMmapper->getMMById($consumer["site_meter_id"]);
							if($feedarDetail){
								$feeder= $feedarDetail->__get("feeder_type");
							}else{
								$feeder="";
							}		 
						                
							$data = array(
								'no'=>intval($totalCountVal),
								'package_name' => implode(",", $packageName),
								'consumer_id' => $consumer["consumer_id"],    
								'consumer_name' => ucwords(strtolower($consumer["consumer_name"])),
								'consumer_status' => $consumer["consumer_status"],
								'consumer_connection_id' => $consumer["consumer_connection_id"],
								"credit_limit"=>intval($consumer["credit_limit"]),
								"curr_reading"=>floatval(sprintf("%.2f",$curr_reading)),
								"last_reading"=>floatval(sprintf("%.2f",$last_reading)),
								"operating_mode"=>$operating_mode,
								"unit"=>floatval($unit),
								"package_name_spark"=>$package_name,
								"timestamp_reading"=>$timestamp_reading,
								"SD"=>abs($consumer["sd"]),
								"package_cost"=>$totalCost." / (".$totalWatt.")"." / (".$feeder.")", 
								"curr_bal"=>floatval(sprintf("%.2f",$mainBalance+$ActualAmount)), 
								"remaing_bal"=>floatval(sprintf("%.2f",$remaing_bal)),
								"main_bal"=>floatval($mainBalance),
								"serial"=>$serial,
								"avgUnit"=>$ActualAmount  
							); 
							$consumer_arr[]=$data;
					//}
						
                }
				
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                    "total_page" => $total_pages,
                    "current_page" =>$current_page,
                    "total_consumers" => $total_active,
                	"new_consumer" => $new_consumer,
                	"invalid_consumer" => $invalid_consumer
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "No consumers found"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
     public function updateConsumerByIdAction(){
      
         try {
			$packagemapper=new Application_Model_PackagesMapper(); 
			$celamedMapper=new Application_Model_CelamedMapper();
			 
            $request=$this->getRequest();
            $consumer_id=$request->getParam("id"); 
            $package_id=$request->getParam("package_id"); 
            $site_id=$request->getParam("site_id");
            $consumer_code=$request->getParam("code");
            $consumer_name=$request->getParam("consumer_name");
            $consumer_father_name=$request->getParam("father_name");
            $consumer_status=$request->getParam("status");
            $act_date=$request->getParam("act_date");
            $consumer_act_charge=$request->getParam("act_charge");
            $consumer_connection_id=$request->getParam("connection_id");
            $is_micro_enterprise=$request->getParam("is_micro");
            $micro_entrprise_price=$request->getParam("micro_price");
            $suspension_date=$request->getParam("suspension_date");
            $enterprise_type=$request->getParam("enterprise_type");
            $type_of_me=$request->getParam("type_of_me");
            $notes= $request->getParam("notes");
            $consumer_scheme=$request->getParam("consumer_scheme");
            $consumer_scheme_id= $request->getParam("consumer_scheme_id");
		    $smsOptIn= $request->getParam("sms_opt_in");
		    $meter_start_reading= $request->getParam("meter_start_reading");
			$site_meter_id =$request->getParam("site_meter_id");
			$credit_limit =$request->getParam("credit_limit");
		    if($smsOptIn!="true"){
				$smsOptIn=false;
			}else{
				$smsOptIn=true;
			}
            $package=$packagemapper->getPackageById($package_id);
            if( $package->__get("is_postpaid")==1||$package->__get("is_postpaid")==2)
            {
            	$is_micro_enterprise = 1;
            }
            else
            {
            	$meter_start_reading=0;
                $is_micro_enterprise = 0;
            }
            $zendDate = new Zend_Date($act_date,"MMM dd,yyyy");
            $consumer_act_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
            //$packagemapper=new Application_Model_PackagesMapper();
            $packages_name=$packagemapper->getPackageById($package_id);
            $package_name=$packages_name->__get("package_name");
            $package_cost=$packages_name->__get("package_cost");
            $consumermapper=new Application_Model_ConsumersMapper();
            $consumer=new Application_Model_Consumers();
            $packagehistoryMapper=new Application_Model_PackageHistoryMapper();
            $cashRegisterMapper=new Application_Model_CashRegisterMapper();
            $meterMapper=new Application_Model_MeterReadingsMapper();
            $lastConsumer= $consumermapper->getConsumerById($consumer_id);
		 	$lastSites=$lastConsumer->__get("site_id");
            $old_status = $lastConsumer->__get("consumer_status");
            if($old_status!=$consumer_status and $consumer_status=="banned")
            {
				$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
            	$meterReading = new Application_Model_MeterReadings();
		        $date = new Zend_Date();
		        $date->setTimezone("Asia/Calcutta");
		        $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		        $meterReading->__set("consumer_id",$consumer_id);
		        $meterReading->__set("meter_reading",$meter_start_reading);
		        $meterReading->__set("timestamp",$timestamp);
				$meterReading->__set("package_id",$package_id);
				$meterReading->__set("start_reading",0);
				$meterReading->__set("entry_by",$this->_userId); 
				$meterReading->__set("entry_type",'W');
		        $meterReadingsMapper->addNewMeterReading($meterReading);
            }
          
            if($old_status!=$consumer_status)
            {
            	$consumer->__set("consumer_first_status","new");
             	$this->_logger->info("Consumer status changed to ".$consumer_status." from ".$old_status." Consumer ID: ".$consumer_connection_id." By ". $this->_userName);
			}
             $consumer->__set("consumer_id",$consumer_id);
             $consumer->__set("package_id",$package_id);
             $consumer->__set("site_id",$site_id);
             $consumer->__set("consumer_code",$consumer_code);
             $consumer->__set("consumer_name",$consumer_name);
             $consumer->__set("consumer_father_name",$consumer_father_name);
             $consumer->__set("consumer_status",$consumer_status);
             $consumer->__set("consumer_act_date",$consumer_act_date);
             $consumer->__set("consumer_act_charge",$consumer_act_charge);
             $consumer->__set("consumer_connection_id",$consumer_connection_id);
             $consumer->__set("is_micro_enterprise",$is_micro_enterprise);
             $consumer->__set("micro_entrprise_price",$micro_entrprise_price);
             $consumer->__set("suspension_date",$suspension_date);
             $consumer->__set("enterprise_type",$enterprise_type);
             $consumer->__set("type_of_me",$type_of_me);
             $consumer->__set("notes",$notes);
			 $consumer->__set("sms_opt_in",$smsOptIn);
             $consumer->__set("meter_start_reading",$meter_start_reading);
			 $consumer->__set("site_meter_id","$site_meter_id");
			 $consumer->__set("credit_limit",$credit_limit);
           
           //  if($consumermapper->updateConsumer($consumer)){
             $consumermapper->updateConsumer($consumer);
             if($lastSites!=$site_id){
             	$celamedMapper->updateSiteIdCelamed($site_id,$consumer_id);
             }
             	$lastConsumer_arr = array(
             			
             			'package_id' => $lastConsumer->__get("package_id"),
             			'site_id' => $lastConsumer->__get("site_id"),
             			'consumer_code' => $lastConsumer->__get("consumer_code"),
             			'consumer_name' => $lastConsumer->__get("consumer_name"),
             			'consumer_father_name' => $lastConsumer->__get("consumer_father_name"),
             			'consumer_status' => $lastConsumer->__get("consumer_status"),
             			'consumer_act_date' => $lastConsumer->__get("consumer_act_date"),
             			'consumer_act_charge' => $lastConsumer->__get("consumer_act_charge"),
             			'consumer_connection_id' => $lastConsumer->__get("consumer_connection_id"),
             			'suspension_date' => $lastConsumer->__get("suspension_date"),
             			'type_of_me' =>$lastConsumer->__get("type_of_me"),
             			'notes' =>$lastConsumer->__get("notes"),
						'sms_opt_in' =>$lastConsumer->__get("sms_opt_in"),
						'site_meter_id' =>$lastConsumer->__get("site_meter_id"),
						'credit_limit' =>$lastConsumer->__get("credit_limit"),
             	);
             	$newConsumer_arr = array(
                      
             			'package_id' => $consumer->__get("package_id"),
             			'site_id' => $consumer->__get("site_id"),
             			'consumer_code' => $consumer->__get("consumer_code"),
             			'consumer_name' => $consumer->__get("consumer_name"),
             			'consumer_father_name' => $consumer->__get("consumer_father_name"),
             			'consumer_status' => $consumer->__get("consumer_status"),
             			'consumer_act_date' => $consumer->__get("consumer_act_date"),
             			'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
             			'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
             			'suspension_date' => $consumer->__get("suspension_date"),
             			'type_of_me' =>$consumer->__get("type_of_me"),
             			'notes' =>$consumer->__get("notes"),
						'sms_opt_in' =>$consumer->__get("sms_opt_in"),
						'site_meter_id' =>$lastConsumer->__get("site_meter_id"),
						'credit_limit' =>$lastConsumer->__get("credit_limit"),
                    );
             	$lastConsumer_diff=array_diff($lastConsumer_arr,$newConsumer_arr);
             	$newConsumer_diff=array_diff($newConsumer_arr,$lastConsumer_arr);
             	 
             	$change_data="";
             	foreach ($lastConsumer_diff as $key => $value)
             	{
             		$change_data.=$key." ".$lastConsumer_diff[$key]." change to ".$newConsumer_diff[$key]." ";
             	}
             	$this->_logger->info("Consumer Id ".$consumer_id." has been updated where ".$change_data." by ". $this->_userName.".");
             	
             	
              	if($packagehistory=$packagehistoryMapper->getpackageHistoryByConsumerId($consumer_id)){
              	
              	 $packages_history=$packagehistory[0];
              	 
              	 $packageId=$packages_history->__get('package_id');
              	 $consumerId=$packages_history->__get('consumer_id');
              	 $changeBy=$packages_history->__get('change_by');
              	 $packageCost=$packages_history->__get('package_cost');
              	 $packageChangeDate=$packages_history->__get('package_change_date');
              	 $changeDescription=$packages_history->__get('change_description');
              	 $packagename=$packagemapper->getPackageById($packageId);
              	 $packageName=$packagename->__get("package_name");

              	    /*if($packageId!=$package_id)
          			{
          				$is_post_paid=$packages_name->__get("is_postpaid");
          				if($is_post_paid!=0){
          				$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
          				$meter_val=$meterReadingsMapper->getLatestReadingByConsumerId($consumer_id,$packageId);
          				if($meter_val){
          					$meter_reading=abs($meter_val->__get("meter_reading"));
						}else{
							$meter_reading=0;
						}
          				
          				if($meter_reading<$meter_start_reading){
	          				
		          				$meterReading = new Application_Model_MeterReadings();
		          				$date = new Zend_Date();
		          				$date->setTimezone("Asia/Calcutta");
		          				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		          				$meterReading->__set("consumer_id",$consumer_id);
		          				$meterReading->__set("meter_reading",$meter_start_reading);
		          				$meterReading->__set("timestamp",$timestamp);
		          				$meterReadingsMapper->addNewMeterReading($meterReading);
	          				}
          				}
          				$auth = new My_Auth("user");
          				$change_by=$auth->getIdentity()->user_id;
          				
          				$packages_history->__set("package_id",$package_id);
          				$packages_history->__set("consumer_id",$consumer_id);
          				$packages_history->__set("change_by",$change_by);
          				$packages_history->__set("package_cost",$package_cost);
          				if($packageCost<$package_cost)
          				{
          				    $packages_history->__set("change_description","Package upgraded from ".$packageName."(Rs.".$packageCost.") to " .$package_name."(Rs.".$package_cost.")");
          				}else{
          					$packages_history->__set("change_description","Package downgraded from ".$packageName."(Rs.".$packageCost.") to " .$package_name."(Rs.".$package_cost.")");
          					 
          				}
          				$packages_history->__set("status","Request Received");
          				$packagehistoryMapper->addNewPackageHistory($packages_history);
          				 		
          			 	
          			}else{
          				$is_post_paid=$packages_name->__get("is_postpaid");
          				if($is_post_paid!=0){
          				$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
          				$meter_val=$meterReadingsMapper->getLatestReadingByConsumerId($consumer_id,$package_id);
          				if($meter_val){
          					$meter_reading=abs($meter_val->__get("meter_reading"));
						}else{
							$meter_reading=0;
						}
          				
          				if($meter_reading<$meter_start_reading){
          					    $meterReading = new Application_Model_MeterReadings();
          						$date = new Zend_Date();
          						$date->setTimezone("Asia/Calcutta");
          						$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
          						$meterReading->__set("consumer_id",$consumer_id);
          						$meterReading->__set("meter_reading",$meter_start_reading);
          						$meterReading->__set("timestamp",$timestamp);
          						$meterReadingsMapper->addNewMeterReading($meterReading);
          					}
          				}
          			}*/
              	 }
              	 
              	 $consumer_schememapper = new Application_Model_ConsumerSchemeMapper();
              	
              	 if($is_micro_enterprise =="1")
              	 {
              	 	
              	 	if(($consumer_scheme_id=="null") ||($consumer_scheme_id=="undefined"))
              	 	{
              	 		//echo $consumer_scheme_id."ZBxh";
              	 		$sitemapper = new Application_Model_SitesMapper();
              	 		$site = $sitemapper->getSiteById($site_id);
              	 		$consumer_schemes = new Application_Model_ConsumerScheme();
              	 		$consumer_schemes->__set("consumer_scheme", $consumer_scheme);
              	 		$consumer_schemes->__set("consumer_id", $consumer_id);
              	 		$consumer_schemes->__set("cluster_id", $site->__get("cluster_id"));
              	 		$consumer_schemes->__set("site_id", $site_id);
              	 		 
              	 		$consumer_schemes = $consumer_schememapper->addNewConsumerScheme($consumer_schemes);
              	 		$this->_logger->info("New consumerScheme ID ".$consumer_schemes." has been created in ConsumerScheme by ". $this->_userName.".");
              	 	}
              	 	else{
              	 		$consumers_scheme = $consumer_schememapper->updateConsumerSchemeByScheme($consumer_scheme, $consumer_scheme_id);
              	 		$this->_logger->info("ConsumerScheme ".$consumer_scheme_id." has been updated by ". $this->_userName.".");
              	 	}
              	 }
              	 else if(($consumer_scheme_id !=null) && ($consumer_scheme_id!="undefined"))
              	 {
              	 	//echo $consumer_scheme_id;
              	 	$consumer_schemes = $consumer_schememapper->deleteConsumerSchemeById($consumer_scheme_id);
              	 	$this->_logger->info("ConsumerScheme Id ".$consumer_scheme_id." has been deleted from ConsumerScheme by ". $this->_userName.".");
              	 }
              	
              	 
              	
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            /* } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while updating"
                );
                $arr = array(
                    "meta" => $meta
                );
            } */
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
   }

		 
     public function getConsumerByColumnNameAction(){
        
        try{
            $request=$this->getRequest();
            $package_id=5;
            $site_id = $request->getParam("site_id");
            $consumermapper=new Application_Model_ConsumersMapper();
            $consumers=$consumermapper->getConsumersByColumn($site_id,$package_id);
            if(count($consumers)>0){
            foreach ($consumers as $consumer) {
                $siteMapper=new Application_Model_SitesMapper();
                $packageMapper=new Application_Model_PackagesMapper();
                $site_name=$siteMapper->getSiteById($consumer->__get("site_id"));
                $package_name=$packageMapper->getPackageById($consumer->__get("package_id"));
                $date = new Zend_Date($consumer->__get("consumer_act_date"),"yyyy-MM-dd HH:mm:ss");
                //$date->setTimezone("Asia/Calcutta");
                $timestamp = $date->toString("MMM dd, yyyy");
                $data = array(
                        'package_name' => $package_name->__get("package_name"),
                        'site_name' => $site_name->__get("site_name"),
                        'consumer_id' => $consumer->__get("consumer_id"),
                        'package_id' => $consumer->__get("package_id"),
                        'site_id' => $consumer->__get("site_id"),
                        'consumer_code' => $consumer->__get("consumer_code"),
                        'consumer_name' => $consumer->__get("consumer_name"),
                        'consumer_father_name' => $consumer->__get("consumer_father_name"),
                        'consumer_status' => $consumer->__get("consumer_status"),
                        'consumer_act_date' => $timestamp,
                        'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
                        'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
                        'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
                        'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
                        'suspension_date' => $consumer->__get("suspension_date"),
                        'enterprise_type' => $consumer->__get("enterprise_type"),
                        'type_of_me' => $consumer->__get("type_of_me"),
                        'notes' => $consumer->__get("notes"),
                         'site_meter_id' =>$consumer->__get("site_meter_id"),
                         'credit_limit' => $consumer->__get("credit_limit"),
						 'wallet_bal' => $consumer->__get("wallet_bal"),
                );
                $consumer_arr[]=$data;
            }
                
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS",
                        
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $consumer_arr,
                );
                } else {
                    $meta = array(
                            "code" => 404,
                            "message" => "No consumers found"
                    );
                    $arr = array(
                            "meta" => $meta
                    );
                }
                } catch (Exception $e)
                {
                    $meta = array(
                            "code" => 501,
                            "message" => $e->getMessage()
                    );
                
                    $arr = array(
                            "meta" => $meta
                    );
                }
                $json = json_encode($arr, JSON_PRETTY_PRINT);
                echo $json;
            
        }
        public function getConsumerByConnectionIdAction(){
        
            try{
                $request=$this->getRequest();
                $consumer_connection_id = $request->getParam("connection_id");
                $consumermapper=new Application_Model_ConsumersMapper();
                $consumers=$consumermapper->getConsumerByConnectionID($consumer_connection_id);
                $data=true;
                if($consumers->__get("consumer_name")==""){
                    $data=false;
                } 
                
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS",
                
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $data,
                );
            } catch (Exception $e)
            {
                $meta = array(
                        "code" => 501,
                        "message" => $e->getMessage()
                );
        
                $arr = array(
                        "meta" => $meta
                );
            }
            $json = json_encode($arr, JSON_PRETTY_PRINT);
            echo $json;
             
        }    

 public function getConsumerMeterConnectionsAction(){
        
            try{
                $request=$this->getRequest();
                $site_id = $request->getParam("site_id");
                $state_id = $request->getParam("state_id");
                $type=$request->getParam("type");
                if($site_id=="ALL")
                {
                    $site_id = NULL;
                }
                if($state_id=="ALL")
                {
                    $state_id = NULL;
                }

                $auth=new My_Auth('user');
                $user=$auth->getIdentity()->user_role;
                //$state_id=$auth->getIdentity()->state_id;
                $roleSession = new Zend_Session_Namespace('roles');
                $role_sites_id=$roleSession->site_id;
                
                $consumermapper=new Application_Model_ConsumersMapper();
                $consumers=$consumermapper->getConsumerMeterConnections($site_id,$state_id,$role_sites_id,false,$type);
                $consumer_arr=array();
                if($consumers)
                {
                    foreach ($consumers as $consumer) {
                        $date = new Zend_Date($consumer["timestamp"],"yyyy-MM-dd HH:mm:ss");
                        $timestamp = $date->toString("MMM dd, yy");
                        $cur_date = new Zend_Date();
                        $cur_date->setTimezone("Asia/Calcutta");
                        $difference = $cur_date->sub($date);

                        $measure = new Zend_Measure_Time($difference->toValue(), Zend_Measure_Time::SECOND);
                        $measure->convertTo(Zend_Measure_Time::MONTH);
                        $diffMonth= intval($measure->getValue());
						
                        if($diffMonth ==0)
                            $status='label-success';
                        else if($diffMonth ==1)
                            $status='label-info';
                        if($diffMonth >1)
                            $status='label-warning';
                        if($diffMonth >2)
                            $status='label-danger';
							
                        $data = array(
                        'consumer_id' => $consumer["consumer_id"],    
                        'consumer_name' => $consumer["consumer_name"],
                        'consumer_connection_id' => $consumer["consumer_connection_id"],
                        'meter_reading'=>$consumer["meter_reading"],
                        'meter_reading_id'=>$consumer["meter_reading_id"],
                        'timestamp'=>$timestamp,
                        'status'=>$status,
                        );
                        $consumer_arr[]=$data;
                    }
                }
                
                
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS",
                
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $consumer_arr,
                );
            } catch (Exception $e)
            {
                $meta = array(
                        "code" => 501,
                        "message" => $e->getMessage()
                );
        
                $arr = array(
                        "meta" => $meta
                );
            }
            $json = json_encode($arr, JSON_PRETTY_PRINT);
            echo $json;
             
        } 

public function getConsumerOutstandingAction(){
        
            try{
                $request=$this->getRequest();
                $site_id = $request->getParam("site_id");
                $state_id = $request->getParam("state_id");

				if($site_id=="ALL") {
					$site_id = NULL;
				}
				if($state_id=="ALL") {
					$state_id = NULL;
				}

                $auth=new My_Auth('user');
                $user=$auth->getIdentity()->user_role;
                //$state_id=$auth->getIdentity()->state_id;
                $roleSession = new Zend_Session_Namespace('roles');
                $role_sites_id=$roleSession->site_id;
                $outstandingMapper = new Application_Model_OutstandingMapper();
                $consumermapper=new Application_Model_ConsumersMapper();
                $consumers=$outstandingMapper->getTopDataByConsumerId($site_id,$state_id,$role_sites_id);
				 
                $consumer_arr=array();
                if($consumers)
                {
                    foreach ($consumers as $consumer) {
                        $data = array(
							'consumer_id' => $consumer["consumer_id"],    
							'consumer_name' => $consumer["consumer_name"],
							'consumer_connection_id' => $consumer["consumer_connection_id"],
							'site_id' => intval($consumer["site_id"]),
							'outstanding'=>intval($consumer["outstanding"]),
                        );
                        $consumer_arr[]=$data;
                    }
                }
                
                
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS",
                
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $consumer_arr,
                );
            } catch (Exception $e)
            {
                $meta = array(
                        "code" => 501,
                        "message" => $e->getMessage()
                );
        
                $arr = array(
                        "meta" => $meta
                );
            }
            $json = json_encode($arr, JSON_PRETTY_PRINT);
            echo $json;
             
        }     

        public function getConsumerCountByTypeofConsumersAction(){
     	
     		try{
     			$request=$this->getRequest();
     			$site_id = $request->getParam("site_id");
     			$state_id = $request->getParam("state_id");
     			
     			$curr_date = $request->getParam("curr_date");
	            $last_date = $request->getParam("last_date");
	            
	            if($state_id=="ALL"){$state_id=NULL;}
	            if($site_id=="ALL"){$site_id=NULL;}
	            
	            $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
	            $curr_date = $zendDate->toString("yyyy-MM-dd");
	            
	            $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
	            $last_date=$zendDate->toString("yyyy-MM-dd");
	             
	            
     	
     			$auth=new My_Auth('user');
     			$user=$auth->getIdentity()->user_role;
     			$total_consumers_details=array();
     			$total_crs_details=array();
     			$roleSession = new Zend_Session_Namespace('roles');
     			$role_sites_id=$roleSession->site_id;
     			$consumermapper=new Application_Model_ConsumersMapper();
     			
     			$consumerTypeMapper= new Application_Model_ConsumersMapper();
     			$Consumerstype=$consumerTypeMapper->getConsumerType();
     			$typeofconsumers= Array();
     			foreach ($Consumerstype as $consumer_type) {
     				$typeofconsumers[]=$consumer_type["consumer_type_name"];
     				 
	     			$consumers=$consumermapper->getConsumerCountByTypeofConsumers($site_id,$state_id,$role_sites_id,$consumer_type["consumer_type_id"],$curr_date,$last_date);
	     			if($consumers!=0){
	     			$consumer_arr=array(
	     					"name"=>$consumer_type["consumer_type_name"],
	     					"value"=>intval($consumers)
	     				);
     			  		array_push($total_consumers_details,$consumer_arr);
	     			}
     			  	$crs=$consumermapper->getTotalDebitByTypeofConsumers($site_id,$state_id,$role_sites_id,$consumer_type["consumer_type_id"],$curr_date,$last_date);
					 if($crs!=0){
     			  	$crs_arr=array(
     			  			"name"=>$consumer_type["consumer_type_name"],
     			  			"value"=>intval($crs)
     			  	);
     			  	array_push($total_crs_details,$crs_arr);
					}
     			}
     		if(count($total_crs_details)==0){
     			foreach ($Consumerstype as $consumer_type){
     			$crs_arr=array(
     					"name"=>$consumer_type["consumer_type_name"],
     					"value"=>0
     			);
     			array_push($total_crs_details,$crs_arr);
     			}
     		}
     		if(count($total_consumers_details)==0){
     			foreach ($Consumerstype as $consumer_type){
     				$crs_arr=array(
     						"name"=>$consumer_type["consumer_type_name"],
     						"value"=>0
     				);
     				array_push($total_consumers_details,$crs_arr);
     			}
     		}
     			$meta = array(
     					"code" => 200,
     					"message" => "SUCCESS",
     	
     			);
     			$arr = array(
     					"meta" => $meta,
     					"data" => $total_consumers_details,
     					"Debit"=>$total_crs_details,
     					"TypeOfConsumer"=>$typeofconsumers
     			);
     		} catch (Exception $e)
     		{
     			$meta = array(
     					"code" => 501,
     					"message" => $e->getMessage()
     			);
     	
     			$arr = array(
     					"meta" => $meta
     			);
     		}
     		$json = json_encode($arr, JSON_PRETTY_PRINT);
     		echo $json;
     	
     	}
        
         public function checkMeterReadingByIdAction(){
     	
     		try{
     			$request=$this->getRequest();
     			$consumer_id = $request->getParam("consumer_id");
     			$package_id = $request->getParam("package_id");
     			$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
     			 
     			$meter_val=$meterReadingsMapper->getLatestReadingByPackageConsumerId($consumer_id,$package_id);
     			 
     			if($meter_val){
     				$meter_reading=abs($meter_val->__get("meter_reading"));
					$meter_no=$meter_val->__get("meter_no");
     				$meta = array(
     						"code" => 200,
     						"message" => "SUCCESS",
     				
     				);
     				$arr = array(
     						"meta" => $meta,
     						"data" => $meter_reading,
							"meter_no"=>$meter_no,
     							
     				);
     			}else{
     				
     				$meter_reading=0;
     			$meta = array(
     					"code" => 300,
     					"message" => "Failed",
     	
     			);
     			$arr = array(
     					"meta" => $meta,
     					"data" =>  $meter_reading,
     					 
     			);
     			}
     		} catch (Exception $e)
     		{
     			$meta = array(
     					"code" => 501,
     					"message" => $e->getMessage()
     			);
     	
     			$arr = array(
     					"meta" => $meta
     			);
     		}
     		$json = json_encode($arr, JSON_PRETTY_PRINT);
     		echo $json;
     	
     	}
		
    protected function _smsNotification($number, $sms) {
            $number = substr($number, 0, 10);
            $sms = urlencode($sms);
            $smsMapper =new Application_Model_SmsGatewayMapper();
            $smsgateway = $smsMapper->getAllSmsGateway();
            $user_name=$smsgateway[0]->__get("user_name");
            $password=$smsgateway[0]->__get("password");
        
            $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=".$user_name."&auth_scheme=plain&password=".$password."&v=1.1&format=text";
        
            $text = file_get_contents($url);
            return $text;
    }
    
    
    public function updateConsumerByConsumeridAction(){

        try {
            $packagemapper=new Application_Model_PackagesMapper();
            $celamedMapper=new Application_Model_CelamedMapper();

            $request=$this->getRequest();
            $consumer_id=$request->getParam("id");
            $site_id=$request->getParam("site_id");
            $consumer_code=$request->getParam("code");
            $consumer_name=$request->getParam("consumer_name");
            $consumer_father_name=$request->getParam("father_name");
            $type_of_me=$request->getParam("type_of_me");
            //$package_id=$request->getParam("package_id");
            $micro_price=$request->getParam("micro_price");
            $meter_start_reading=$request->getParam("meter_start_reading");
            $consumer_act_charge=$request->getParam("consumer_act_charge");
            $consumer_status=$request->getParam("status");
			$credit_limit=$request->getParam("credit_limit");
            $packages=$packagemapper->getPackageById($package_id);
            if($packages){
                if($packages->__get("is_postpaid")!=0){
                    $is_micro_enterprise=1; 
                }else{
                    $is_micro_enterprise=0;
                }
            }
            $consumermapper=new Application_Model_ConsumersMapper();
            $consumer=new Application_Model_Consumers();

			$lastConsumer= $consumermapper->getConsumerById($consumer_id);
            $old_status = $lastConsumer->__get("consumer_status");
			$suspension_date = $lastConsumer->__get("suspension_date");
			$consumer_connection_id = $lastConsumer->__get("consumer_connection_id");
            if($old_status!=$consumer_status)
            {
            	$this->_logger->info("Consumer status changed to ".$consumer_status." from ".$old_status." Consumer ID: ".$consumer_connection_id." By ". $this->_userName);
				 if($consumer_status=='active'){
					$consumer->__set("suspension_date",NULL);
				}else{
					$date = new Zend_Date();
					$date->setTimezone("Asia/Calcutta");
					$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
					$consumer->__set("suspension_date",$timestamp);
				}
            }else{
				$consumer->__set("suspension_date",$suspension_date);
			}
			
            $consumer->__set("consumer_id",$consumer_id);
            $consumer->__set("site_id",$site_id);
            $consumer->__set("consumer_code",$consumer_code);
            $consumer->__set("consumer_name",$consumer_name);
            $consumer->__set("consumer_father_name",$consumer_father_name);
            $consumer->__set("type_of_me",$type_of_me);
          //  $consumer->__set("package_id",$package_id);
            $consumer->__set("is_micro_enterprise",$is_micro_enterprise);
            $consumer->__set("micro_price",$micro_price);
            $consumer->__set("meter_start_reading",$meter_start_reading);
            $consumer->__set("consumer_act_charge",$consumer_act_charge);
            $consumer->__set("consumer_status",$consumer_status);
			$consumer->__set("credit_limit",$credit_limit);
           $consumers=$consumermapper->updateConsumerByConsumerId($consumer);
                if($consumers){
                    $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                    );
                    $arr = array(
                        "meta" => $meta,

                    );
                }else{
                    $meta = array(
                        "code" => 300,
                        "message" => "Failed"
                    );
                    $arr = array(
                        "meta" => $meta,

                    );
                }



        }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function addMeterReadingAction(){
    
    	try {
    		$request=$this->getRequest();
    		$consumer_id=$request->getParam("consumer_id");
    		$meter_reading=$request->getParam("meter_reading");
    		$last_reading=$request->getParam("last_reading");
    		$unit=$request->getParam("unit");
			$closing_reading=$request->getParam("closing_reading"); 
			$package_id=$request->getParam("package_id"); 
			$date=$request->getParam("reading_date"); 
			$meter_no=$request->getParam("meter_no");
			$last_meter_no=$request->getParam("last_meter_no");
			$resetMeter=$request->getParam("resetMeter"); 
			$released=$request->getParam("released");
			$usable=$request->getParam("usable");
			$bill=$request->getParam("bill");
			
			if($date!='undefined' && $date!="" && $date!=NULL && $date!='null'){
				$zendDate = new Zend_Date($date,"dd-MM-yyyy HH:mm:ss"); 
				$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
			}else{
				$zendDate = new Zend_Date(); 
				$zendDate->setTimezone("Asia/Calcutta");
				$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
			}
			
    		
    		$consumerPackMapper=new Application_Model_ConsumersPackageMapper();
			$consumerMapper=new Application_Model_ConsumersMapper();
    		$MeterReadingsMapper=new Application_Model_MeterReadingsMapper();
    		$MeterReadings= new Application_Model_MeterReadings();
    		$MeterInventoryMapper = new Application_Model_MeterInventoryMapper(); 
			$PackagesMapper = new Application_Model_PackagesMapper(); 
			$cashRegisterMapper = new Application_Model_CashRegisterMapper(); 
			
    		if(floatval($closing_reading)>0){
				$timestamps = date('Y-m-d h:i:s', strtotime($timestamp ." -1 min"));
				$MeterReading= new Application_Model_MeterReadings();
				$MeterReading->__set("consumer_id",$consumer_id);
				$MeterReading->__set("meter_reading",floatval($closing_reading));
				$MeterReading->__set("timestamp",$timestamps);
				$MeterReading->__set("package_id",$package_id);
				$MeterReading->__set("start_reading",0);
				$MeterReading->__set("meter_no",$last_meter_no);
				$MeterReading->__set("unit",$unit);
				$MeterReading->__set("serial",$last_meter_no);
				$MeterReading->__set("entry_by",$this->_userId); 
				$MeterReading->__set("entry_type",'W');
				$reading_val=$MeterReadingsMapper->addNewMeterReading($MeterReading);
				$this->_logger->info("New meter reading ID ".$reading_val." has been created in meter reading by ". $this->_userName.".");
				if($resetMeter==1 && $bill!='true' && $bill!=true){
					$check=$updateInventory=$MeterInventoryMapper->getDetailByMeterNo($last_meter_no);
						if($check){
							if($last_meter_no!=NULL && $last_meter_no!='NULL' && $last_meter_no!="" && $last_meter_no!='undefined'){
								if($released==0){
									$updateInventory=$MeterInventoryMapper->updateMeterStatus($last_meter_no,$released,NULL,$consumer_id);
									$this->_logger->info("New meter number ".$last_meter_no." has been closed but not Released by ". $this->_userName.".");
								}else{
									$updateInventory=$MeterInventoryMapper->updateMeterStatus($last_meter_no,$usable,NULL,$consumer_id);
									$this->_logger->info("New meter number ".$last_meter_no." has been Released but ".$usable." by ". $this->_userName.".");
								}
							}
						}
				}
			}
			
			if($resetMeter==0 || $resetMeter==1){
				if($resetMeter==0){
					$MeterReading= new Application_Model_MeterReadings();
					$MeterReading->__set("consumer_id",$consumer_id);
					$MeterReading->__set("meter_reading",$meter_reading);
					$MeterReading->__set("timestamp",$timestamp);
					$MeterReading->__set("package_id",$package_id); 
					$MeterReading->__set("start_reading",$resetMeter);
					$MeterReading->__set("meter_no",$last_meter_no);
					$MeterReading->__set("unit",$unit);
					$MeterReading->__set("serial",$last_meter_no); 
					$MeterReading->__set("entry_by",$this->_userId); 
					$MeterReading->__set("entry_type",'W'); 					
				}else{
					$MeterReading= new Application_Model_MeterReadings();
					$MeterReading->__set("consumer_id",$consumer_id);
					$MeterReading->__set("meter_reading",$meter_reading);
					$MeterReading->__set("timestamp",$timestamp);
					$MeterReading->__set("package_id",$package_id); 
					$MeterReading->__set("start_reading",$resetMeter);
					$MeterReading->__set("meter_no",$meter_no);
					$MeterReading->__set("unit",0);
					$MeterReading->__set("serial",$meter_no);  
					$MeterReading->__set("entry_by",$this->_userId); 
					$MeterReading->__set("entry_type",'W'); 
					if($meter_no!=NULL && $meter_no!='NULL' && $meter_no!="" && $meter_no!='undefined'){
						$check=$MeterInventoryMapper->getDetailByMeterNo($meter_no);
						if($check && $bill!='true' && $bill!=true){ 
							$updateInventory=$MeterInventoryMapper->updateMeterStatus($meter_no,1,NULL,$consumer_id,$timestamp);
						}
					} 
				}
			
				
				if($reading=$MeterReadingsMapper->addNewMeterReading($MeterReading)){
					$this->_logger->info("New meter reading ID ".$reading." has been created in meter reading by ". $this->_userName.".");
					
					$packages=$PackagesMapper->getPackageById($package_id);
					
					if($packages->__get("is_postpaid")==1 && $bill!='true' && $bill!=true){
						
						$consumer=$consumerMapper->getConsumerById($consumer_id);
						$unit_price = $consumer->__get("micro_entrprise_price");
						$amount=$unit*$unit_price; 
						
						$date = new Zend_Date();
						$date->setTimezone("Asia/Kolkata");
						$timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));
						
						$tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
						$transaction_id = $timestamp . "-" . $tcr_amount;
					
						$cashRegister = new Application_Model_CashRegister();
						$cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
						$cashRegister->__set("cr_entry_type", "DEBIT"); 
						$cashRegister->__set("entry_status", "CHG");
						$cashRegister->__set("cr_amount", $amount);
						$cashRegister->__set("transaction_id", $transaction_id);
						$cashRegister->__set("user_id", $this->_userId); 
						$cashRegister->__set("receipt_number", 0);
						$cashRegister->__set("mtr_entry", '1');
						$cashRegister->__set("transaction_type", '(W)' );
						$cashRegister->__set("package_id",$package_id); 
						
						$cashRegisterMapper->addNewCashRegister($cashRegister);
					}
				}else {
					$meta = array(
							"code" => 401,
							"message" => "Error while adding"
					);
					$arr = array(
							"meta" => $meta
					);
				}		 
				
				$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		}else{
				$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
			}
			
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
	public function getAllconsumerFlagdetailAction(){
    
    	try {
    		$request=$this->getRequest();
    	 $consumermapper=new Application_Model_ConsumersMapper();
    	 $consumer_arr=array();
    		if($consumers=$consumermapper->getConsumersByFlag()){
    			foreach ($consumers as $consumer){
    				
    				$flag=($consumer["sms_flag"]==3 || $consumer["sms_flag"]==4)?"YES":"NO";
    				if(($consumer["sms_flag"]==3 && $consumer["consumer_status"]=='banned')||($consumer["sms_flag"]<=2 && $consumer["consumer_status"]=='active')){
    					$title="Disconnection Low Balance";
    				}else {
    					$title="Connection High Balance";
    				}
	    			$data = array(
	    					'consumer_id' => $consumer["consumer_id"],
	    					'consumer_name' => $consumer["consumer_name"],
	    					'consumer_connection_id' => $consumer["consumer_connection_id"],
	    					"credit_limit"=>$consumer["credit_limit"],
	    					"wallet_bal"=>$consumer["wallet_bal"],
	    					"sms_flag"=>$consumer["sms_flag"],
	    					"flag_date"=>$consumer["flag_date"],
	    					"flag"=>$flag,
	    					"title"=>$title
	    			);
	    			$consumer_arr[]=$data;
    			}
    			 
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $consumer_arr
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function pendingConsumersAction() {
    	try
    	{
    		$request=$this->getRequest();
    		$page = $request->getParam("page",1);
    		$status = $request->getParam("status");
    		$total_pages=0;
    		$current_page=0;
    		
    		$consumermapper=new Application_Model_ConsumersMapper();
    		 $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
    		$consumers=$consumermapper->getPendingConsumers($status,$role_sites_id);
    		 
    		if($consumers)
    		{
    			$total_consumers = count($consumers);
    			$new_consumer=0;
    			$invalid_consumer=0;
    			 
    			$paginator=Zend_Paginator::factory($consumers);
    			$paginator->setItemCountPerPage(100);
    			$paginator->setCurrentPageNumber($page);
    			$consumers=$paginator;
    			$total_pages=$paginator->count();
    			if($page>$total_pages)
    			{
    				throw new Exception("No more pages",555);
    			}
    			$current_page=$paginator->getCurrentPageNumber();
    			 
    			foreach ($consumers as $consumer) {
    				$date = new Zend_Date($consumer->__get("consumer_act_date"),"yyyy-MM-dd HH:mm:ss");
    				//$date->setTimezone("Asia/Calcutta");
    				$timestamp = $date->toString("MMM dd, yyyy");
    				$siteMapper=new Application_Model_SitesMapper();
    				$packageMapper=new Application_Model_PackagesMapper();
    				$site_name=$siteMapper->getSiteById($consumer->__get("site_id"));
    				//$package_name=$packageMapper->getPackageById($consumer->__get("package_id"));
    				//$package_name = $packageMapper->getPackageById($consumer->__get("package_id"));
    						 
    				//$is_postpaid=$package_name->__get("is_postpaid");
    				$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
                        $consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
                        
                        
                        $packageName="";$is_postpaid= null;$package_id= array();
                        if($consumerpackage){
                       
                        	foreach($consumerpackage as $package){
                        		//$package_name = $packageMapper->getPackageById($package["package_id"]);
                        		$packageName.=$package["package_name"]." , ";
                        		$is_postpaid=$package["is_postpaid"];
                        		$package_id[]=$package["package_id"];
                        	}
                        }	 
    
    				$Consumermapper=new Application_Model_ConsumerSchemeMapper();
    				$consumerscheme=$Consumermapper->getConsumerschemeByConsumerId($consumer->__get("consumer_id"));
    				$consumer_scheme=null;
    				$consumer_scheme_id=null;
    				 
    				if($consumerscheme)
    				{
    					$consumer_scheme=$consumerscheme->__get("consumer_scheme");
    					$consumer_scheme_id= $consumerscheme->__get("consumer_scheme_id");
    				}
    				if(!$package_name){
    					$package_name=new Application_Model_Packages();
    				}
    				if($is_postpaid!=0){
    					/*$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    					$meter_val=$meterReadingsMapper->getLatestReadingByConsumerId($consumer->__get("consumer_id"));
    					if($meter_val){
    						$meter_reading=abs($meter_val->__get("meter_reading"));
    					}else{*/
    						$meter_reading=0;
    					//}
    				}else{
    					$meter_reading=intval($consumer->__get("meter_start_reading"));
    				}
    				$data = array(
    						'package_name' =>chop($packageName,", "),
    						'site_name' => $site_name->__get("site_name"),
    						'consumer_id' => $consumer->__get("consumer_id"),
    						'package_id' => implode(",", $package_id),
    						'site_id' => $consumer->__get("site_id"),
    						'consumer_code' => $consumer->__get("consumer_code"),
    						'consumer_name' =>strtoupper($consumer->__get("consumer_name")),
    						'consumer_father_name' => $consumer->__get("consumer_father_name"),
    						'consumer_status' => $consumer->__get("consumer_status"),
    						'consumer_act_date' => $timestamp,
    						'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
    						'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
    						'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
    						'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
    						'suspension_date' => $consumer->__get("suspension_date"),
    						'enterprise_type' => $consumer->__get("enterprise_type"),
    						'type_of_me' => $consumer->__get("type_of_me"),
    						'notes' => $consumer->__get("notes"),
    						'consumer_scheme' =>$consumer_scheme,
    						'consumer_scheme_id' => $consumer_scheme_id,
    						'smsOptIn' => $consumer->__get("sms_flag"),
    						"meter_start_reading"=>intval($meter_reading),
    						"site_meter_id"=>$consumer->__get("site_meter_id"),
    						"credit_limit"=>$consumer->__get("credit_limit"),
    						"wallet_bal"=>$consumer->__get("wallet_bal"),
    						"remark"=>$consumer->__get("remark"),
							"is_postpaid"=>	$is_postpaid,	
							"wattage"=> $consumer->__get("wattage"),
							"reason_remark"=>$consumer->__get("reason_remark"),
    				);
    				$consumer_arr[]=$data;
    			}
    			 
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS",
    					"total_page" => $total_pages,
    					"current_page" =>$current_page,
    				 
    					 
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $consumer_arr,
    			);
    		} else {
    			$meta = array(
    					"code" => 404,
    					"message" => "No consumers found"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}
    	catch (Exception $e)
    	{
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
        
    public function deletePendingConsumerByIdAction(){
    
    	try {
    		$request=$this->getRequest();
    		$consumer_id=$request->getParam("consumer_id");
    		$remark=$request->getParam("remark");
    		 
    		$consumermapper=new Application_Model_ConsumersMapper();
			 
	    	$consumer=$consumermapper->deleteConsumerById($consumer_id,$remark);
	    	 
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    
    			);
    		 
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
     
    public function insertConsumerByIdAction(){
    	try {
    		$request=$this->getRequest();
    		$consumer_id=$request->getParam("consumer_id");
    		$remark=$request->getParam("remark");
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$consumer=new Application_Model_Consumers();
    		//$consumer_id=explode(",", $consumer_id);
    		if($consumers=$consumerMapper->getAllPendingConsumerById("$consumer_id")){
    			 
				 $date = new Zend_Date();
				 $date->setTimezone("Asia/Calcutta");
				 $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				 
    			foreach ($consumers as $con){
    				$con->__set("remark", $remark);
    				$con->__set("consumer_first_status", 'use');
					$con->__set("approve_by", $this->_userId);
    				$con->__set("approve_date", $timestamp);
    				$cashReg=$consumerMapper->updateConsumer($con);
    			}
    			
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while deleting"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }  

	public function getConnectionIdAction(){
    
    	try{
    		$request=$this->getRequest();
    		$site_id=$request->getParam("site_id");
			$mobile_no=$request->getParam("mobile_no");
			
    		$pole_no=sprintf("%03d",$request->getParam("pole_no"));
    		
    		$consumermapper=new Application_Model_ConsumersMapper();
    		$sitemapper = new Application_Model_SitesMapper();
    		$sites=$sitemapper->getSiteById($site_id);
    		if($sites->__get("site_name")!="---Deleted---"){
	    		
	    		$consumer_connection=$consumermapper->getConnectionId($sites->__get("site_code"), $pole_no);
	    		
	    		if($consumer_connection){
	    			$connection_no = $consumer_connection +1;
	    		}else{
	    			$connection_no='01';
	    		}
	    		$consumer_connection_id=$sites->__get("site_code").$pole_no.sprintf("%02d",$connection_no);
				
	    		$checkMobile=$consumermapper->getMobileNumber($mobile_no);
				if($checkMobile){
					$mobile_no=0;
				}else{
					$mobile_no=1;
				}
				
	    		$meta = array(
	    				"code" => 200,
	    				"message" => "SUCCESS",
	    	
	    		);
	    		$arr = array(
	    				"meta" => $meta,
	    				"data" => $consumer_connection_id,
						"mobile" => $mobile_no,
	    		);
    		}else{
    			$meta = array(
    					"code" => 301,
    					"message" => "Site Not Found",
    			
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		}
    		
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
 
    		);
    	
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
 
    	
    	}
    
    public function getConsumersByPackageIdAction(){
    	try {
    		$request=$this->getRequest();
    		$package_ids=$request->getParam("package_id");
    		$consumer_type=$request->getParam("consumer_type");
    		$consumer_types=($consumer_type=='undefined' || $consumer_type==NULL)?NULL:$consumer_type;
			$package_id=($package_ids=='undefined' || $package_ids==NULL)?NULL:explode(",", $package_ids);
    		//$package_id=explode(",", $package_ids);
    		$consumermapper=new Application_Model_ConsumersMapper();
    		$consumer_arr=array();
    		if($consumers=$consumermapper->getConsumerByPackageId($package_id,$consumer_types)){
    		foreach ($consumers as $consumer){
    			$data = array(
    					'consumer_id' => $consumer["consumer_id"],
    					'package_id' => $consumer["package_id"],
    					'site_id' => $consumer["site_id"],
    					'consumer_code' => $consumer["consumer_code"],
    					'consumer_name' => $consumer["consumer_name"],
    					'consumer_father_name' => $consumer["consumer_father_name"],
    					'consumer_status' => $consumer["consumer_status"],
    					'consumer_act_date' => $consumer["consumer_act_date"],
    					'consumer_act_charge' => $consumer["consumer_act_charge"],
    					'consumer_connection_id' => $consumer["consumer_connection_id"],
    					'is_micro_enterprise' => $consumer["is_micro_enterprise"],
    					'micro_entrprise_price' => $consumer["micro_entrprise_price"],
    					'suspension_date' => $consumer["suspension_date"],
    					'enterprise_type' => $consumer["enterprise_type"],
    					'type_of_me' => $consumer["type_of_me"],
    					'notes' => $consumer["notes"],
    					'sms_opt_in' => $consumer["sms_opt_in"],
    					"meter_start_reading"=>intval($consumer["meter_start_reading"]),
    					"site_meter_id"=>$consumer["site_meter_id"],
    					"credit_limit"=>$consumer["credit_limit"],
    					"wallet_bal"=>$consumer["wallet_bal"]
    			);
    			
    			$consumer_arr[]=$data;
    		}
	    		$package_cost=0;
	    		$packageMapper=new Application_Model_PackagesMapper();
	    		for ($i=0;$i<count($package_id);$i++){
	    			$packages=$packageMapper->getPackageById($package_id[$i]);
	    			$package_cost=$package_cost+$packages->__get("package_cost");
	    	 	}
	    	 	
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $consumer_arr,
    					"package_cost"=>$package_cost
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while checking"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
 
    		);
    	
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
 
    	
    	}
    
    public function checkPackageId($data){

		    	$site_id=$data['site_id'];
		    	$state_id=$data['state_id'];
		    	$site_code=$data['site_code'];
		    	$site_meter_id=$data['site_meter_id'];
		    	$watt_no=$data['watt_no'];
		    	$connection_type=$data['connection_type'];
		    	$consumer_act_date=$data['consumer_act_date'];
		    	$price=$data['price'];
		    	$MM_unit=$data['MM_unit'];
		    	$MM_charges=$data['MM_charges'];
		    	$type_of_me=$data['type_of_me'];
		    	$wattage=$data['wattage'];
		    	$package_type=$data['package_type'];
		    	
    		 	$siteMasterMeterMapper = new Application_Model_SiteMasterMeterMapper();
    			$packagemapper = new Application_Model_PackagesMapper();
    			$feeders=explode(",", $site_meter_id);
    			$package_arr=array();
    			for($i=0;$i<count($feeders);$i++){
    				$feeder_data=$siteMasterMeterMapper->getMMById($feeders[$i]);
    			 
	    			$feeder_type= $feeder_data->__get("feeder_type");
	    			$start_time=$feeder_data->__get("start_time");
	    			$end_time=$feeder_data->__get("end_time");
    			 
	    			if($start_time > $end_time){
						$feeder_hour= intval(24+($end_time - $start_time));
					}
					else {
						$feeder_hour= intval($end_time - $start_time); 
					}
					 if($feeder_hour<10){
						$zero=0;
					 }else{
						$zero="";
					 }
	    			if($package_type==0){
    			 		$pacakge_name=$site_code.$zero.$feeder_hour.$feeder_type.$watt_no.$connection_type.$price;
	    			}elseif ($package_type==1){
	    				$pacakge_name=$site_code.$zero.$feeder_hour.$feeder_type.$watt_no."ME00".$price;
	    			}elseif ($package_type==2){
	    				$pacakge_name=$site_code.$zero.$feeder_hour.$feeder_type.$watt_no."MM".$price.$MM_unit.$MM_charges;
	    			}
					 
    			 	$packages=$packagemapper->getPackagesByPackageName($pacakge_name);
	    			if($packages){
					 
	    				$package_id=$packages[0]->__get("package_id");
	    				$package_arr[]=$package_id;
	    			}else{
					 
	    				$package=new Application_Model_Packages();
	    				$package->__set("package_cost",$price);
	    				$package->__set("package_details","Created by consumer");
	    				$package->__set("is_postpaid",$package_type);
	    				$package->__set("wattage",$wattage);
	    				$package->__set("connection_type", $type_of_me);
	    				$package->__set("state_id", $state_id); 
	    				$package->__set("site_id", $site_id);
	    				$package->__set("feeder_id", $feeders[$i]);
	    				$package->__set("feeder_type", $feeder_type);
	    				$package->__set("unit",$MM_unit);
	    				$package->__set("extra_charges",$MM_charges);
	    			 
    				    $packages=$packagemapper->addNewPackage($package,$this->_userId);
    				    if($packages){
    				    	$package_arr[]=$packages;
    				    }
    			  }
    			}	
			 
    			 return $package_arr;
    	}
    	
    	
    public function interChangePackageIdAction(){
    	
    		try{
    		$request=$this->getRequest();
    		$current_package=$request->getParam("current_package");
    	    $new_package= $request->getParam("new_package");
    		$consumer_id=$request->getParam("consumer_id");
    		$reading=$request->getParam("reading");
			$lastreading=$request->getParam("lastreading");
    		//$is_postpaid=$request->getParam("is_postpaid");
    		
    		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    		$consumerPackageMapper = new Application_Model_ConsumersPackageMapper();
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$consumerPackage=$consumerPackageMapper->updatePackageId($consumer_id, $new_package,$current_package);
			//$consumerPackage=true;
			$packageMapper=new Application_Model_PackagesMapper();
    		$packages=$packageMapper->getPackageById($current_package);
    		$is_postpaid=$packages->__get("is_postpaid");
			 
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
    		if($is_postpaid!=0){
    			$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    			$meterReading = new Application_Model_MeterReadings();
    			
    			$meterReading->__set("consumer_id",$consumer_id);
    			$meterReading->__set("meter_reading",$reading);
    			$meterReading->__set("timestamp",$timestamp);
    			$meterReading->__set("package_id",$current_package);
				$meterReading->__set("start_reading",0);
				$meterReading->__set("entry_by",$this->_userId); 
				$meterReading->__set("entry_type",'W'); 
    			$meterReadingsMapper->addNewMeterReading($meterReading);
    		}
			$packages_val=$packageMapper->getPackageById($new_package);
    		$new_is_postpaid=$packages_val->__get("is_postpaid");
			if($new_is_postpaid!=0){
    			$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    			$meterReading = new Application_Model_MeterReadings();
    			 
    			$meterReading->__set("consumer_id",$consumer_id);
    			$meterReading->__set("meter_reading",$reading);
    			$meterReading->__set("timestamp",$timestamp);
    			$meterReading->__set("package_id",$new_package);
				$meterReading->__set("start_reading",0);
				$meterReading->__set("entry_by",$this->_userId); 
				$meterReading->__set("entry_type",'W'); 
    			$meterReadingsMapper->addNewMeterReading($meterReading);
    		}
			
    		if ($consumerPackage){ 
    			$packageMapper=new Application_Model_PackagesMapper();
    			$new_packages=$packageMapper->getPackageById($request->getParam("new_package"));
    			$curr_packages=$packageMapper->getPackageById($request->getParam("current_package"));
    			
    			$new_package_cost=$new_packages->__get("package_cost");
    			$curr_package_cost=$curr_packages->__get("package_cost");
    			$new_postpaid_value=$new_packages->__get("is_postpaid");
    			$cur_postpaid_value=$curr_packages->__get("is_postpaid");
    			
    			if($cur_postpaid_value==0 && $new_postpaid_value!=0){
    				$packageStatus=true;
    			}else if($cur_postpaid_value==0 && $new_postpaid_value==0){
    				if($new_package_cost>$curr_package_cost){
    					$packageStatus=true;
    				}else{
    					$packageStatus=false;
    				}
    			}else if($cur_postpaid_value!=0 && $new_postpaid_value==0){
    				$packageStatus=false;
    			}else if($cur_postpaid_value!=0 && $new_postpaid_value!=0){
    				if($new_package_cost>$curr_package_cost){
    					$packageStatus=true;
    				}else{
    					$packageStatus=false;
    				}
    			}else{
    				$packageStatus=false;
    			}
    			 
	    			if($packageStatus)
	    			{
	    				$date = new Zend_Date();
	    				$date->setTimezone("Asia/Kolkata");
	    				$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
	    				$current_date = date_parse_from_format("Y-m-d", $curr_date);
	    				$day = $current_date["day"];
	    				$month = sprintf("%02d", $current_date["month"]);
	    				$year = $current_date["year"];
	    				$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
	    				
	    				$consumers=$consumerMapper->getConsumerById($consumer_id);
	    				$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
	    				$Act_day = $Act_date["day"];
	    				$Act_month = sprintf("%02d", $Act_date["month"]);
	    				$Act_year = $Act_date["year"];
	    				
	    				$lastDebit=$cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$current_package);
						 
	    				if($month==$Act_month && $year==$Act_year){
	    					$lastAmount=($lastDebit / $total_days_in_month)*($day-$Act_day);
	    				}else{
	    					$lastAmount=($lastDebit / $total_days_in_month) * $day;
	    				}
						 
	    				if($is_postpaid==0){
							  $newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
							  $package_val=$newAmount-($lastDebit-$lastAmount);
	    				}else {
	    					$unit=$curr_packages->__get("unit");
	    					$extra_charges=$curr_packages->__get("extra_charges");
	    					
	    					$usedUnit=($unit/$total_days_in_month)*($total_days_in_month-$day);
							$readings=$reading-$lastreading;
	    					if($readings>$usedUnit){
	    						$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
	    						$unit_amt=$newAmount+(($readings-$usedUnit)*$extra_charges);
	    						$package_val=($unit_amt+$lastAmount)-$lastDebit;
	    					}else{
	    						$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
	    						$package_val=($newAmount+$lastAmount)-$lastDebit;
	    					}
							 
	    				}
	    				 
	    			    $timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
	    				$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
	    				$transaction_id = $timestamp . "-" . $cr_amount;
	    			 
	    				$cashRegister = new Application_Model_CashRegister();
	    				$cashRegister->__set("user_id", $this->_userId);
	    				$cashRegister->__set("consumer_id", $consumer_id);
	    				$cashRegister->__set("cr_entry_type", "DEBIT");
	    				$cashRegister->__set("cr_amount", intval($package_val));
	    				$cashRegister->__set("receipt_number", 0);
	    				$cashRegister->__set("transaction_id", $transaction_id);
						$cashRegister->__set("transaction_type", "(W)");
	    				$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
	    				$cashRegister->__set("entry_status", "CHG");
	    				$cashRegister->__set("remark", "Package upgraded from ".$curr_packages->__get("package_name")."(Rs.".$curr_packages->__get("package_cost").") to " .$new_packages->__get("package_name")."(Rs.".$new_packages->__get("package_cost").")");
	    				$cashRegister->__set("package_id", $new_package);
	    				$cashRegisterMapper->addNewCashRegister($cashRegister);
	    				$status="Package Updated";
	    				//$packages_history->__set("change_description","Package upgraded from ".$packageName."(Rs.".$packageCost.") to " .$package_Names."(Rs.".$package_Costs.")");
	    				//$packages_history->__set("status","Package Updated");
	    			}else{
	    				$status="Package Updated";
	    				//$packages_history->__set("change_description","Package downgraded from ".$packageName."(Rs.".$packageCost.") to " .$package_Names."(Rs.".$package_Costs.")");
	    				//$packages_history->__set("status","Request Received");
	    			}
    			 
    			
    			$packageMapper= new Application_Model_PackagesMapper();
    			$packages=$packageMapper->getPackageById($new_package);
    			$package_cost = $packages->__get("package_cost");
    			$user_id=$this->_userId;
    			 
    			$package= new Application_Model_PackageHistory();
    			$package->__set("package_id",$new_package);
    			$package->__set("consumer_id",$consumer_id);
    			$package->__set("package_cost",$package_cost);
    			$package->__set("change_by",$user_id);
    			$package->__set("status",$status);
    			$package->__set("last_package",$current_package);
    			
    			$packageHistorymapper = new Application_Model_PackageHistoryMapper();
    			$packageHistory= $packageHistorymapper->addNewPackageHistory($package);
    			
	    		$meta = array(
	    				"code" => 200,
	    				"message" => "SUCCESS",
	    	
	    		);
	    		$arr = array(
	    				"meta" => $meta,
	    				"data" => $new_package,
	    		);
    		}else{
    			$meta = array(
    					"code" => 301,
    					"message" => "Error in Updating",
    			
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		}
    		 
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
 
    		);
    	
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	
    	}
    	
    	
    public function addNewPackageAction(){
    		 
    		try{
    			$request=$this->getRequest();
    			$package_id=$request->getParam("package_id");
    			$consumer_id=$request->getParam("consumer_id");
    			$consumerPackageMapper = new Application_Model_ConsumersPackageMapper();
    			$package= new Application_Model_ConsumersPackage();
    			$package->__set("package_id",$package_id);
    			$package->__set("consumer_id",$consumer_id);
    			$checkConsumer=$consumerPackageMapper->getPackageByConsumerId($consumer_id,$package_id);
    			if(!$checkConsumer)
    			{
    				$consumerPackage=$consumerPackageMapper->addNewConsumersPackage($package);
    				if($consumerPackage)
    				{
    					$packageMapper= new Application_Model_PackagesMapper();
    					$packages=$packageMapper->getPackageById($package_id);
    					$package_cost = $packages->__get("package_cost");
    					$user_id=$this->_userId;
    					$status="Request Received";
    					$package= new Application_Model_PackageHistory();
    					$package->__set("package_id",$package_id);
    					$package->__set("consumer_id",$consumer_id);
    					$package->__set("package_cost",$package_cost);
    					$package->__set("change_by",$user_id);
    					$package->__set("status",$status);
						$package->__set("last_package",NULL);
    					 
    					$packageHistorymapper = new Application_Model_PackageHistoryMapper();
    					$packageHistory= $packageHistorymapper->addNewPackageHistory($package);
    					
    				
							$meta = array(
									"code" => 200,
									"message" => "SUCCESS",
				
							);
							$arr = array(
									"meta" => $meta,
									
							);
					}else{
						$meta = array(
								"code" => 301,
								"message" => "Error in assigning package",
								 
						);
						$arr = array(
								"meta" => $meta,
						);
					}
    			}else{
    				$meta = array(
    						"code" => 301,
    						"message" => "This package is already assign to consumer",
    							
    				);
    				$arr = array(
    						"meta" => $meta,
    				);
    			}
    	
    		} catch (Exception $e) {
    			$meta = array(
    					"code" => 501,
    					"messgae" => $e->getMessage()
    	
    			);
    			 
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    		$json = json_encode($arr, JSON_PRETTY_PRINT);
    		echo $json;
    		 
    	}
    	
    public function deletePackageByConsumerIdAction(){
    		 
    		try{
    		 
    		$request=$this->getRequest();
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    		$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    		$packageMapper=new Application_Model_PackagesMapper();
    		
    		$current_package=$request->getParam("current_package");
    		$consumer_id=$request->getParam("consumer_id");
    		$reading=$request->getParam("reading");
    		$lastreading=$request->getParam("lastreading");
    		$remark=$request->getParam("remark");
    		$status=$request->getParam("status");
    		
    		$packages=$packageMapper->getPackageById($current_package);
    		$is_postpaid=$packages->__get("is_postpaid");
    		$package_cost=$packages->__get("package_cost");
    		
    		if($is_postpaid!=0){
    			$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    			$meterReading = new Application_Model_MeterReadings();
    			$date = new Zend_Date();
    			$date->setTimezone("Asia/Calcutta");
    			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    			$meterReading->__set("consumer_id",$consumer_id);
    			$meterReading->__set("meter_reading",$reading);
    			$meterReading->__set("timestamp",$timestamp);
    			$meterReading->__set("package_id",$current_package);
    			$meterReading->__set("start_reading",0);
				$meterReading->__set("entry_by",$this->_userId); 
				$meterReading->__set("entry_type",'W'); 
    			$meterReadingsMapper->addNewMeterReading($meterReading);
    		}
    		
	    		$date = new Zend_Date();
	    		$date->setTimezone("Asia/Kolkata");
	    		$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
	    		$current_date = date_parse_from_format("Y-m-d", $curr_date);
	    		$day = $current_date["day"];
	    		$month = sprintf("%02d", $current_date["month"]);
	    		$year = $current_date["year"];
	    		$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
	    		 
	    		$consumers=$consumerMapper->getConsumerById($consumer_id);
				$micro_entrprise_price=$consumers->__get("micro_entrprise_price");
				
	    		$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
	    		$Act_day = $Act_date["day"];
	    		$Act_month = sprintf("%02d", $Act_date["month"]);
	    		$Act_year = $Act_date["year"];
	    		 
	    		$lastDebit=$cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$current_package);
	    		if($month==$Act_month && $year==$Act_year){
	    			$lastAmount=($lastDebit/$total_days_in_month)*($day-$Act_day);
	    		}else{
	    			$lastAmount=($lastDebit/$total_days_in_month)*$day;
	    		}
	    		if($is_postpaid==0){
	    			if($status=='banned'){
	    				$package_val=$lastAmount;
	    			}else{
	    				$package_val=0;
	    			}
	    		}else {
	    			$unit=$packages->__get("unit");
	    			$extra_charges=$packages->__get("extra_charges");
	    			 $readings=floatval($reading) - floatval($lastreading);
	    			if($status=='banned'){
						if($is_postpaid==2){
							$usedUnit=($unit/$total_days_in_month)* $day ;
							if($lastDebit<=0){
								$lastAmount=($package_cost/$total_days_in_month)* $day ;
							}
							if($readings>$usedUnit){
								$newAmount=($package_cost/$total_days_in_month)* $day;
								$unit_amt=$newAmount+(($readings-$usedUnit)*$extra_charges);
								$package_val=$unit_amt;
							}else{
								$package_val=$lastAmount;
							}
	    				}else{
							$package_val=$readings*$micro_entrprise_price;
						}
	    			}else{
						if($is_postpaid==2){
							$usedUnit=$unit;
							if($lastDebit<=0){
								$lastAmount=($package_cost/$total_days_in_month)* $day ;
							} 
							if($readings>$usedUnit){
								$newAmount=($package_cost/$total_days_in_month)*($total_days_in_month-$day);
								$unit_amt=$newAmount+(($readings-$usedUnit)*$extra_charges);
								$package_val=($unit_amt+$lastAmount)-$lastDebit;
							}else{
								$package_val=0;
							}
						}else{
							$package_val=$readings*$micro_entrprise_price; 
						}
	    			}
	    		 }
	    		
	    		 if($status!='banned' && $package_val!=0){
			    		$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
			    		$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
			    		$transaction_id = $timestamp . "-" . $cr_amount;
			    		
			    		$cashRegister = new Application_Model_CashRegister();
			    		$cashRegister->__set("user_id", $this->_userId);
			    		$cashRegister->__set("consumer_id", $consumer_id);
			    		$cashRegister->__set("cr_entry_type", "DEBIT");
			    		$cashRegister->__set("cr_amount", intval($package_val));
			    		$cashRegister->__set("receipt_number", 0);
			    		$cashRegister->__set("transaction_id", $transaction_id);
						$cashRegister->__set("transaction_type", "(W)");
			    		$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
			    		$cashRegister->__set("entry_status", "CHG");
			    		$cashRegister->__set("remark", "");
			    		$cashRegister->__set("package_id", $current_package);
			    		$cashRegisterMapper->addNewCashRegister($cashRegister);
	    		 }elseif($status='banned'){
	    		 	$cashRegisters=$cashRegisterMapper->getDebitEntryByConsumerId($consumer_id,$date->toString("yyyy"),$date->toString("M"));
	    		 	if($cashRegisters){
	    		 		$cashRegister=$cashRegisters[0];
	    		 		$cr_id=$cashRegister->__get('cr_id');
	    		 		$res=$cashRegisterMapper->updateCashRegisterAmount($package_val,$cr_id);
	    		 	}else{
						$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
			    		$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
			    		$transaction_id = $timestamp . "-" . $cr_amount;
			    		
			    		$cashRegister = new Application_Model_CashRegister();
			    		$cashRegister->__set("user_id", $this->_userId);
			    		$cashRegister->__set("consumer_id", $consumer_id);
			    		$cashRegister->__set("cr_entry_type", "DEBIT");
			    		$cashRegister->__set("cr_amount", intval($package_val));
			    		$cashRegister->__set("receipt_number", 0);
			    		$cashRegister->__set("transaction_id", $transaction_id);
						$cashRegister->__set("transaction_type", "(W)");
			    		$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
			    		$cashRegister->__set("entry_status", "CHG");
			    		$cashRegister->__set("remark", "");
			    		$cashRegister->__set("package_id", $current_package);
			    		$cashRegisterMapper->addNewCashRegister($cashRegister);
					}
	    		 }
	    		 
    		
    			$consumerPackageMapper = new Application_Model_ConsumersPackageMapper();
    			$checkPackage=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
    			if($checkPackage){
    				if(count($checkPackage)>1){
    					$consumerPackage=$consumerPackageMapper->deletePackageByConsumerId($consumer_id,$current_package);
    				}elseif (count($checkPackage)==1){
    					$this->_logger->info("Consumer status changed to banned from active Consumer ID: ".$consumers->__get("consumer_connection_id")." By ". $this->_userName);
    				    
    					$date = new Zend_Date();
    					$date->setTimezone("Asia/Calcutta");
    					$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    					  
    					$consumers=$consumerMapper->updateStatus($consumers->__get("consumer_connection_id"),'banned',$timestamp,$remark,$this->_userId,$timestamp);
    				}
    					
    				$meta = array(
    						"code" => 200,
    						"message" => "SUCCESS",
    						 
    				);
    				$arr = array(
    						"meta" => $meta,
    	
    				);
    			}else{
    				$meta = array(
    						"code" => 301,
    						"message" => "You cannt Delete this Package",
    							
    				);
    				$arr = array(
    						"meta" => $meta,
    				);
    			}
    			 
    		} catch (Exception $e) {
    			$meta = array(
    					"code" => 501,
    					"messgae" => $e->getMessage()
    					 
    			);
    	
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    		$json = json_encode($arr, JSON_PRETTY_PRINT);
    		echo $json;
    		 
    	}
		
	public function getAllConsumerTypesAction(){
         
         	try{
         		$request=$this->getRequest();
         		
         		$consumersMapper = new Application_Model_ConsumersMapper();
         		 
         		$consumer_val=$consumersMapper->getConsumerType();
         		$sub_consumer_type_arr=array();$consumer_arr=array();
         		if($consumer_val){
         			
         			foreach ($consumer_val as $consumer_type) {
         			
         				$data = array(
         						'consumer_type_id' => $consumer_type["consumer_type_id"],
         						'consumer_type_name' => $consumer_type["consumer_type_name"],
         						'consumer_type_code' => $consumer_type["consumer_type_code"],
         						
         				);
         				$consumer_arr[]=$data;
						
						$sub_consumer_types=$consumersMapper->getConsumerType($consumer_type["consumer_type_id"]);
						if($sub_consumer_types){
							foreach ($sub_consumer_types as $consumer_types){
								$data_val=array(
										"consumer_type_id"=>$consumer_types["consumer_type_id"],
										"consumer_type_name"=>$consumer_types["consumer_type_name"],
										"consumer_type_code"=>$consumer_types["consumer_type_code"],
										"parent_id"=>$consumer_types["parent_id"],
								);
								$sub_consumer_type_arr[]=$data_val;
							}
						}
         			}
         			}
         			 
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS",
         
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $consumer_arr,
							"consumer_categories_arr" => $sub_consumer_type_arr,
         
         			);
         	} catch (Exception $e)
         	{
         		$meta = array(
         				"code" => 501,
         				"message" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         
         }
		 
	public function getOutstandingByConsumerIdAction(){
    	
    		try{
    			$request=$this->getRequest();
    			$consumer_id = $request->getParam("consumer_id");
    		 	
    			$consumermapper=new Application_Model_ConsumersMapper();
    			
    			$outstanding=$consumermapper->getConsumerOutstanding($consumer_id);
    			$outstanding_act=$consumermapper->getRemainingActivation($consumer_id);
    			
    			$total=intval($outstanding)+intval($outstanding_act);
    	
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS",
    	
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $total,
    			);
    		} catch (Exception $e)
    		{
    			$meta = array(
    					"code" => 501,
    					"message" => $e->getMessage()
    			);
    	
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    		$json = json_encode($arr, JSON_PRETTY_PRINT);
    		echo $json;
    	
    	}
    
    public function updateOutstandingByConsumerIdAction(){
    
    	try {
    		$consumermapper=new Application_Model_ConsumersMapper();
    		$consumer_package=new Application_Model_ConsumersPackageMapper();
    		
    		$request=$this->getRequest();
    		$consumer_id=$request->getParam("consumer_id");
    		$outstanding=$request->getParam("outstanding");
    		$pay_outstanding=$request->getParam("pay_outstanding");
    		$remark=$request->getParam("remark");
    		$consumers=$consumermapper->getConsumerById($consumer_id);
    		
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    		
    		$consumers_val=$consumermapper->updateStatus($consumers->__get("consumer_connection_id"),'active',$timestamp,$remark,$this->_userId,'(W)');
    		if($consumers_val){
				$this->_logger->info("Consumer status changed to active from banned Consumer ID: ".$consumers->__get("consumer_connection_id")." By ". $this->_userName);
    				   
    			if(intval($pay_outstanding)>0){
    				$consumerPack=$consumer_package->getPackageByConsumerId($consumer_id);
    				
    				$date = new Zend_Date();
    				$date->setTimezone("Asia/Kolkata");
    				$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
    				
    				$cr_amount = str_pad($pay_outstanding, 4, "0", STR_PAD_LEFT);
    				$transaction_id = $timestamp . "-" . $cr_amount;
    				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    				$cashRegister = new Application_Model_CashRegister();
    				$cashRegister->__set("user_id", $this->_userId);
    				$cashRegister->__set("user_val", $this->_userId);
    				$cashRegister->__set("consumer_id", $consumer_id);
    				$cashRegister->__set("cr_entry_type", "CREDIT");
    				$cashRegister->__set("cr_amount", $pay_outstanding);
    				$cashRegister->__set("receipt_number", 0);
    				$cashRegister->__set("transaction_id", $transaction_id);
    				$cashRegister->__set("transaction_type", "(W)");
    				$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
    				$cashRegister->__set("entry_status", "CHG");
    				$cashRegister->__set("remark", $remark);
    				$cashRegister->__set("package_id", $consumerPack[0]["package_id"]);
    				$cashRegisterMapper->addNewCashRegister($cashRegister);
    			}
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    
    			);
    		}else{
    			$meta = array(
    					"code" => 300,
    					"message" => "Failed"
    			);
    			$arr = array(
    					"meta" => $meta,
    
    			);
    		}
    
    
    
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	 
	public function updateCreditLimitByIdAction(){
    	try {
    		$consumermapper=new Application_Model_ConsumersMapper();
    		$consumer_package=new Application_Model_ConsumersPackageMapper();
			
    		$request=$this->getRequest();
    		$consumer_id=$request->getParam("consumer_id");
    		$credit_limit=$request->getParam("credit_limit");
			$serial=$request->getParam("serial"); 
			if($credit_limit!='undefined' && $credit_limit!='null' && $credit_limit!=NULL && $credit_limit!=""){   
    		$consumers=$consumermapper->getConsumerById($consumer_id);
    	 
    		$consumers_update=$consumermapper->updateCreditLimit($consumer_id,$credit_limit);
    		if($consumers_update){
    			$this->_logger->info("Consumer limit changed to  ".$credit_limit." from ".$consumers->__get("credit_limit")." Consumer ID: ".$consumers->__get("consumer_connection_id")." By ". $this->_userName);
				
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$siteOutstaning=$cashRegisterMapper->getMonthlyDueBySiteId($consumers->__get("site_id"),null,null,null,$consumers->__get("consumer_id"));
				$outstanding=intval($siteOutstaning["debit_amount"])-intval($siteOutstaning["credit_amount"]);
				if($consumers->__get("site_id")==62){
					$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
				}else{
					$url="https://sparkcloud-tara-dumarsan.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
				}  
				//$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
    		    if(intval($credit_limit)<=intval($outstanding)){
						$activitys='off'; 
						$param="state=".$activitys."";
						if($consumers->__get("site_id")==62){ 
							$destination=array(
								'Authentication-Token: .eJwFwcsRwCAIBcBeOIcZlSdqLZkc-Ej_JWT3Jal9bVfjdk0ZFZMNGQy3Y64xZlx6aOZCbgkZhewCO7JQ6jCVqN7o-wEZRhSK.DcwoVA.GIS1jju7M0zPSMjcDrGOBOW7Rp8'
								);
						}else{
							$destination=array(
								'Authentication-Token: .eJwNx7kNwDAIAMBdqGOJ13FmiVIAhv1HiK-7F3bKY-w4fEoPRd4j8NRs3tKVUbngAuvgJC9qjS7KjakktoqVvLnh-wH_yhSf.DgLTZg.K0MSUEm8_CSi_aQQde7UGW8Ikx4'
							);
						}
						$result=$this->CallCURL($url,'POST',$param,$destination);
							
						$data=(json_decode($result,true));
				}else{
						$activitys='on'; 
						$param="state=".$activitys."";
						if($consumers->__get("site_id")==62){ 
							$destination=array(
								'Authentication-Token: .eJwFwcsRwCAIBcBeOIcZlSdqLZkc-Ej_JWT3Jal9bVfjdk0ZFZMNGQy3Y64xZlx6aOZCbgkZhewCO7JQ6jCVqN7o-wEZRhSK.DcwoVA.GIS1jju7M0zPSMjcDrGOBOW7Rp8'
								);
						}else{
							$destination=array(
								'Authentication-Token: .eJwNx7kNwDAIAMBdqGOJ13FmiVIAhv1HiK-7F3bKY-w4fEoPRd4j8NRs3tKVUbngAuvgJC9qjS7KjakktoqVvLnh-wH_yhSf.DgLTZg.K0MSUEm8_CSi_aQQde7UGW8Ikx4'
							);
						} 
						$result=$this->CallCURL($url,'POST',$param,$destination);
							
						$data=(json_decode($result,true));
				}
				 
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
						"data" => $credit_limit,
    	
    			);
    		}else{
    			$meta = array(
    					"code" => 300,
    					"message" => "Something went wrong. Please try again" 
    			);
    			$arr = array(
    					"meta" => $meta,
    	
    			);
    		}
    	}else{
    			$meta = array(
    					"code" => 300,
    					"message" => "No change found"
    			);
    			$arr = array(
    					"meta" => $meta,
    	
    			);
    		}
    	
    	
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    	
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
	public function updateConsumerStatusOnSparkMeterAction(){
    	try {
    		
    		$request=$this->getRequest();
    		$activity=$request->getParam("activity");
    		$serial=$request->getParam("serial");
    		$consumer_id=$request->getParam("consumer_id");
    		 if($activity=='ON'){
    		 	$activitys='off'; 
				$activity_no=0;
    		 }else{
    		 	$activitys='on';
				$activity_no=1;
    		 }
			
			$consumermapper=new Application_Model_ConsumersMapper();
    		$consumers=$consumermapper->getConsumerById($consumer_id);
     
    		//$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
    		if($consumers->__get("site_id")==62){
				$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
			}else{
				$url="https://sparkcloud-tara-dumarsan.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
			} 	
    		$param="state=".$activitys."";
    		if($consumers->__get("site_id")==62){ 
				$destination=array(
					'Authentication-Token: .eJwFwcsRwCAIBcBeOIcZlSdqLZkc-Ej_JWT3Jal9bVfjdk0ZFZMNGQy3Y64xZlx6aOZCbgkZhewCO7JQ6jCVqN7o-wEZRhSK.DcwoVA.GIS1jju7M0zPSMjcDrGOBOW7Rp8'
				);
			}else{
				$destination=array(
					'Authentication-Token: .eJwNx7kNwDAIAMBdqGOJ13FmiVIAhv1HiK-7F3bKY-w4fEoPRd4j8NRs3tKVUbngAuvgJC9qjS7KjakktoqVvLnh-wH_yhSf.DgLTZg.K0MSUEm8_CSi_aQQde7UGW8Ikx4'
				);
			}  
    		$result=$this->CallCURL($url,'POST',$param,$destination);
    			
    		$data=(json_decode($result,true));
			
			if($data["status"]=='success'){
    			
    			$this->_logger->info("Consumer status on spark meter has been changed to  ".$activitys." Consumer ID: ".$consumers->__get("consumer_connection_id")." By ". $this->_userName);
    			 
				 $meterReadingMapper=new Application_Model_MeterReadingsMapper();
				 
				    	$meters=$meterReadingMapper->getMeterReadingsByConsumer($consumers->__get("consumer_id"));
                        	if($meters){
	                        	$meter_reading_id=$meters[0]->__get("meter_reading_id");
								
								$meterReading = $meterReadingMapper->getMeterReadingById($meter_reading_id);
								$meterReading->__set("operating_mode",$activity_no); 
								$meterReading->__set("meter_reading_id",$meter_reading_id);
								
								$meterReadingMapper->updateMeterReading($meterReading);   
						}
    			 
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		}else{
    			$meta = array(
    					"code" => 300,
    					"message" => "Failed"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
      
    function CallCURL($url, $method = 'GET', $params = null, $destination = [])
    {
		//echo $params;exit;
    	if (count($destination) > 0) {
    		foreach ($destination as $item) {
    			$headers[] = $item;
    		}
    	}
    
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	if ($method == 'GET') {
    		if (count($params) > 0) {
    			$url .= '?' . http_build_query($params);
    		}
    	} else {
    		curl_setopt($ch, CURLOPT_POST, true);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    	}
    	curl_setopt($ch, CURLOPT_URL, $url);
    
    	$result = curl_exec($ch);
    	curl_close($ch);
    	return $result;
    }
	
	 public function updateConsumerAction(){

        try {
            $packagemapper=new Application_Model_PackagesMapper(); 
            $consumermapper=new Application_Model_ConsumersMapper(); 
			$siteMapper=new Application_Model_SitesMapper();
			
            $request=$this->getRequest();
            $consumer_id=$request->getParam("id"); 
            $site_id=$request->getParam("site_id");
            $consumer_name=$request->getParam("consumer_name");
            $consumer_father_name=$request->getParam("father_name");
			$consumer_code=$request->getParam("code");
			$mobile_no=$request->getParam("mobile_no");
			$email=$request->getParam("email");
			$gender=$request->getParam("gender");
			$dob=$request->getParam("dob");
			 
			
            $pole_no=$request->getParam("connection_id");
            $is_micro_enterprise=$request->getParam("is_micro_enterprise");
            $type_of_me=$request->getParam("type_of_me");
			$sub_type_of_me=$request->getParam("sub_type_of_me");
            $site_meter_id =$request->getParam("site_meter_id");
			$temp=0;
			
			$consumer=$consumermapper->getConsumerById($consumer_id);
			$consumer->__set("consumer_id",$consumer_id);
			
			if($site_id!=NULL && $site_id!="" && $site_id!='undefined' && $site_id!='null'){
				$consumer->__set("site_id",$site_id);
				$site_id_value=$site_id;
			}else{
				$consumer_val=$consumermapper->getConsumerById($consumer_id);
				$site_id_value=$consumer_val->__get("site_id");
			}
			
			if($pole_no!=NULL && $pole_no!="" && $pole_no!='undefined' && $pole_no!='null'){
				
				$site = $siteMapper->getSiteById($site_id_value);
				$site_code=$site->__get("site_code");
					
				$consumer_connection=$consumermapper->getConnectionId($site_code,sprintf("%03d",$request->getParam("connection_id")));
				if($consumer_connection){
					$connection_no = $consumer_connection +1;
				}else{
					$connection_no='01';
				}
				$consumer_connection_id=$site_code.sprintf("%03d",$pole_no).sprintf("%02d",$connection_no); 
				$consumer->__set("consumer_connection_id",$consumer_connection_id);
			}
			
            if($consumer_name!=NULL && $consumer_name!="" && $consumer_name!='undefined' && $consumer_name!='null'){
				$consumer->__set("consumer_name",$consumer_name);
			}
			if($consumer_father_name!=NULL && $consumer_father_name!="" && $consumer_father_name!='undefined' && $consumer_father_name!='null'){
				$consumer->__set("consumer_father_name",$consumer_father_name);
			}
			if($consumer_code!=NULL && $consumer_code!="" && $consumer_code!='undefined'){
				$consumer->__set("consumer_code",$consumer_code);
				$checkMob=$consumermapper->getMobileNumber($consumer_code);
				if($checkMob){
					$temp=1;
				}
			}
			if($mobile_no!=NULL && $mobile_no!="" && $mobile_no!='undefined' && $mobile_no!='null'){
				$consumer->__set("mobile_no",$mobile_no);
			}
			if($email!=NULL && $email!="" && $email!='undefined' && $email!='null'){
				$consumer->__set("email_id",$email);
			}
			if($gender!=NULL && $gender!="" && $gender!='undefined' && $gender!='null'){
				$consumer->__set("gender",$gender);
			}
			if($dob!=NULL && $dob!="" && $dob!='undefined' && $dob!='null'){
				$zendDate = new Zend_Date($dob,"dd-MM-yyyy");
				$dob = $zendDate->toString("yyyy-MM-dd HH:mm:ss"); 
				$consumer->__set("dob",$dob);
			}
			if($is_micro_enterprise!=NULL && $is_micro_enterprise!="" && $is_micro_enterprise!='undefined' && $is_micro_enterprise!='null'){ 
				$consumer->__set("is_micro_enterprise",$is_micro_enterprise);
			}
			if($type_of_me!=NULL && $type_of_me!="" && $type_of_me!='undefined' && $type_of_me!='null'){
				 $consumer->__set("type_of_me",$type_of_me);
			}
			if($sub_type_of_me!=NULL && $sub_type_of_me!="" && $sub_type_of_me!='undefined' && $sub_type_of_me!='null'){
				$consumer->__set("sub_type_of_me",$sub_type_of_me);
			}
			if($site_meter_id!=NULL && $site_meter_id!="" && $site_meter_id!='undefined' && $site_meter_id!='null'){
				$consumer->__set("site_meter_id","$site_meter_id");
			}
			 
			if($temp==0){
				if($consumermapper->updateConsumer($consumer)) {
					$this->_logger->info("Consumer ID: ".$consumer->__get("consumer_connection_id")." has been updated. By ". $this->_userName); 
					$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
					);
					$arr = array(
						"meta" => $meta,

					);
				} else {
					$meta = array(
						"code" => 300,
						"message" => "Error while updating"
					);
					$arr = array(
					 "meta" => $meta
					);
				} 
			}else{
					$meta = array(
						"code" => 300,
						"message" => "Error. Duplicate mobile number."
					);
					$arr = array(
					 "meta" => $meta
					);
			}
        }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	 public function updateConsumerDetailsAction(){

        try {             
            $request = $this->getRequest();
            $consumer_id = $request->getParam("consumer_id"); 
			$site_id = $request->getParam("site_id");
            $package_id = $request->getParam("package_id");
			$package_type = $request->getParam("package_type");
            $consumer_name = $request->getParam("consumer_name");
            $consumer_father_name = $request->getParam("father_name");
            $site_meter_id = $request->getParam("site_meter_id");
            $gender = $request->getParam("gender");
            $email = $request->getParam("email");
			$consumer_connection_id = $request->getParam("consumer_connection_id");
            $consumer_type = $request->getParam("consumer_type");
            $consumer_categorie = $request->getParam("consumer_categorie");
            $consumer_mtr = $request->getParam("consumer_mtr");
            $consumer_act = $request->getParam("consumer_act");
			
			$consumer=new Application_Model_Consumers();
            
            $consumer->__set("consumer_id",$consumer_id);
            $consumer->__set("package_id",$package_id);
            $consumer->__set("site_id",$site_id);
            $consumer->__set("consumer_name",$consumer_name);
            $consumer->__set("consumer_father_name",$consumer_father_name);
            $consumer->__set("consumer_connection_id",$consumer_connection_id);
            $consumer->__set("type_of_me",$consumer_type);
			$consumer->__set("sub_type_of_me",$consumer_categorie);
            $consumer->__set("site_meter_id","$site_meter_id");
            $consumer->__set("gender",$gender);
			$consumer->__set("email",$email);
				
            $consumermapper = new Application_Model_ConsumersMapper();
			
            $consumerUp=$consumermapper->updateConsumer($consumer, $consumer_id);
					if($consumerUp){
						$cpMapper=new Application_Model_ConsumersPackageMapper();
						$cp=new Application_Model_ConsumersPackage();
						$cp->__set("package_id",$package_id);
						$cp->__set("consumer_id",$consumer_id);
						$cpMapper->addNewConsumersPackage($cp);
						
						if($consumer_mtr>0){
							$cashRegisterMapper=new Application_Model_CashRegisterMapper();
							$date = new Zend_Date();
							$date->setTimezone("Asia/Kolkata");
							$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
							
							$cr_amount = str_pad($consumer_mtr, 4, "0", STR_PAD_LEFT);
							$transaction_id = $timestamp . "-" . $cr_amount;
							
							 
							$cashRegister = new Application_Model_CashRegister();
							$cashRegister->__set("user_id", $this->_userId);
							$cashRegister->__set("consumer_id", $consumer_id);
							$cashRegister->__set("cr_entry_type", "DEBIT"); 
							$cashRegister->__set("cr_amount", $consumer_mtr);
							$cashRegister->__set("receipt_number", "CA-".$this->_userId);
							$cashRegister->__set("transaction_id", $transaction_id);
							$cashRegister->__set("transaction_type", "(W)");
							$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd"));
							$cashRegister->__set("entry_status", "METER");
							$cashRegister->__set("remark", "METER ON NEW CONSUMER");
							$cashRegister->__set("package_id", $package_id);
							 
							$cashRegisterMapper->addNewCashRegister($cashRegister);
						}
						if($consumer_act>0){
							$cashRegisterMapper=new Application_Model_CashRegisterMapper();
							$date = new Zend_Date();
							$date->setTimezone("Asia/Kolkata");
							$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
							
							$cr_amount = str_pad($consumer_act, 4, "0", STR_PAD_LEFT);
							$transaction_id = $timestamp . "-" . $cr_amount;
							
							 
							$cashRegister = new Application_Model_CashRegister();
							$cashRegister->__set("user_id", $this->_userId);
							$cashRegister->__set("consumer_id", $consumer_id);
							$cashRegister->__set("cr_entry_type", "DEBIT"); 
							$cashRegister->__set("cr_amount", $consumer_act);
							$cashRegister->__set("receipt_number", "CA-".$this->_userId);
							$cashRegister->__set("transaction_id", $transaction_id);
							$cashRegister->__set("transaction_type", "(W)");
							$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd"));
							$cashRegister->__set("entry_status", "ACT");
							$cashRegister->__set("remark", "ACTIVATION ON NEW CONSUMER");
							$cashRegister->__set("package_id", $package_id);
							 
							$cashRegisterMapper->addNewCashRegister($cashRegister);
						}
					}
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
        }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
  }    