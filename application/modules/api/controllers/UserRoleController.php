<?php
class Api_UserRoleController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function addUserRoleAction(){
	
		try {
			$request=$this->getRequest();
			$user_role=$request->getParam("user_role");
			$site_id=$request->getParam("site_id");
			$main_menu_id=$request->getParam("menu_id");
			$menu_id=$request->getParam("submenu_id");
			$child_menu_id=$request->getParam("child_menu_id");
			$userRoleMapper=new Application_Model_UserRolesMapper();
			$userRoles= new Application_Model_UserRoles();
			$roleSiteMapper=new Application_Model_RoleSiteMapper();
			$roleSites=new Application_Model_RoleSite();
			$permissionMapper=new Application_Model_PermissionMapper();
			$permissions=new Application_Model_Permission();
			$userRoles->__set("user_role",$user_role);
			
			if($user_role_id=$userRoleMapper->addNewUserRole($userRoles)){
				$this->_logger->info("New UserRole ID ".$user_role_id." has been created in userRoles by ". $this->_userName.".");
				 
				
				 $Sites=explode(',', $site_id);
				 //print_r($Sites);
				 $role=count($Sites);
				for($i=$role;$i>0;$i--){
					$roleSites->__set("role_id",$user_role_id);
					$roleSites->__set("site_id",$Sites[$i-1]);
					$roleSiteMapper->addNewRoleSite($roleSites);
					
				}
				$this->_logger->info("RoleSite has been created Site Id (".$site_id.") for userRole ".$user_role_id." by ". $this->_userName.".");
				
				if($child_menu_id=="NULL" || $child_menu_id==NULL)
				{
					$child_menu_ids=array();
				}
				else{
					$child_menu_ids=explode(',', $child_menu_id);
				}
				$main_Menu_ids=explode(',', $main_menu_id);
				$Menu_ids=explode(',', $menu_id);
				
				$all_sub_menu=array_merge($Menu_ids,$child_menu_ids,$main_Menu_ids);
				
				$menu=count($all_sub_menu);
				
				for($i=$menu;$i>0;$i--){
					$permissions->__set("menu_id",$all_sub_menu[$i-1]);
					$permissions->__set("role_id",$user_role_id);
					$permissions->__set("permission",1);
					$permissionMapper->addNewPermission($permissions);
				}
				$this->_logger->info("Permission has been created Menu Id (".$menu_id.",".$child_menu_id.") for userRole ".$user_role_id." by ". $this->_userName.".");
				
				$user_Role=$user_role;
				$name = str_replace('_', ' ', $user_Role);
				$roles = ucwords(strtolower($name));
				$data=array(
						"user_role_id" => $user_role_id,
						"user_role" => $roles,
				);
	
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllUserRolesAction(){
	
		try {
			$userRoleMapper=new Application_Model_UserRolesMapper();
			
			$userRoles=$userRoleMapper->getAllUserRoles();
			if(count($userRoles) >0){
				$roleData=array();
				foreach ($userRoles as $userRole) {
					
					$user_Role=$userRole->__get("user_role");
					$name = str_replace('_', ' ', $user_Role);
					$roles = ucwords(strtolower($name));
					$data=array(
							"user_role_id" => $userRole->__get("user_role_id"),
							"user_role" =>$roles,
					);
	
					$user_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $user_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "Error while getting"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteUserRoleByIdAction(){
	
		try {
			$request=$this->getRequest();
			$user_role_id=$request->getParam("id");
			$userRoleMapper=new Application_Model_UserRolesMapper();
			$roleSiteMapper=new Application_Model_RoleSiteMapper();
			$permissionMapper=new Application_Model_PermissionMapper();
			if($userRole=$userRoleMapper->deleteUserRoleById($user_role_id)){
				$this->_logger->info("UserRole Id ".$user_role_id." has been deleted from UserRoles by ". $this->_userName.".");
				
				$roleSiteMapper->deleteRoleSiteById($user_role_id);
				$permissionMapper->deletePermissionById($user_role_id);
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateUserRoleByIdAction(){
	
		try {
			$request=$this->getRequest();
			$user_role_id=$request->getParam("id");
			$user_role=$request->getParam("user_role");
			$user_site_id=$request->getParam("user_site_id");
			$menu_id=$request->getParam("menu_id");
			$submenu_id=$request->getParam("submenu_id");
			$child_menu_id=$request->getParam("child_menu_id");
			
			/* echo $user_site_id;
			echo $menu_id; */
			if($user_site_id!='undefined'){
				
				$site_id=explode(",", $user_site_id);
			}
			/* if($menu_id!='undefined'){
				
				$menus_id=explode(",", $menu_id);
			} */
			
			$userRoleMapper=new Application_Model_UserRolesMapper();
			$userRoles= new Application_Model_UserRoles();
			$roleSiteMapper=new Application_Model_RoleSiteMapper();
			$roleSites=new Application_Model_RoleSite();
			$permissionMapper=new Application_Model_PermissionMapper();
			$permissions=new Application_Model_Permission();
			
			$userRoles->__set("user_role_id",$user_role_id);
			$userRoles->__set("user_role",$user_role);
			
			
			if($userRoleMapper->updateUserRole($userRoles)){
				$this->_logger->info("UserRole Id ".$user_role_id." has been updated by ". $this->_userName.".");
					
				
			 if($user_site_id!='undefined'){
			 	 $role_add=count($site_id);
				 if($role_add>0){
				 	for($i=$role_add;$i>0;$i--){
				 		$checkRole=$roleSiteMapper->checkRoleSiteById($user_role_id,$site_id[$i-1]);
				 		if($checkRole==0){
				 		$roleSites->__set("role_id",$user_role_id);
						$roleSites->__set("site_id",$site_id[$i-1]);
						$roleSiteMapper->addNewRoleSite($roleSites);
						
					 }
				   }
				 }
				 $roleSiteMapper->deleteRoleSiteById($user_role_id,$site_id); 
				 $this->_logger->info("RoleSite has been updated Site Id (".$user_site_id.") for userRole ".$user_role_id." by ". $this->_userName.".");
				 	
			}
			//if($menu_id!='undefined'||$submenu_id!='undefined'||$child_menu_id!='undefined'){
				
				//if($menu_id!='undefined'){$Menu_ids=explode(',', $menu_id);}else {$Menu_ids=[];}
				//if($submenu_id!='undefined'){$sub_Menu_ids=explode(',', $submenu_id);}else {$sub_Menu_ids=[];}
				//if($child_menu_id!='undefined'){$child_menu_ids=explode(',', $child_menu_id);}else {$child_menu_ids=[];}
				//$sub_Menu_ids=explode(',', $submenu_id);
				//$child_menu_ids=explode(',', $child_menu_id);
				if($menu_id!='null' ){$Menu_ids=explode(',', $menu_id);}else {$Menu_ids=array(0);}
				if($submenu_id!='null' ){$sub_Menu_ids=explode(',', $submenu_id);}else {$sub_Menu_ids=array(0);}
				if( $child_menu_id!='null'){$child_menu_ids=explode(',', $child_menu_id);}else {$child_menu_ids=array(0);}
				
				  
				$all_sub_menu=array_merge($Menu_ids,$sub_Menu_ids,$child_menu_ids);
				//print_r($all_sub_menu);
				$menu_add=count($all_sub_menu);
				if($menu_add>0){
					for($i=$menu_add;$i>0;$i--){
						$checkmenu=$permissionMapper->checkMenuById($user_role_id,$all_sub_menu[$i-1]);
						
						if($checkmenu==0){
							$permissions->__set("role_id",$user_role_id);
							$permissions->__set("menu_id",$all_sub_menu[$i-1]);
							$permissions->__set("permission",0);
							$permissionMapper->addNewPermission($permissions);
						}
					}
				}
				$permissionMapper->deleteMenuById($user_role_id,$all_sub_menu);
				$this->_logger->info("Permission has been updated menu Id (".$menu_id.$submenu_id.$child_menu_id.") for userRole ".$user_role_id." by ". $this->_userName.".");
					
			//}
			
			
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while updating"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getRoleSiteByIdAction() {
	
		try {
			$request = $this->getRequest();
			$role_id = $request->getParam("id");
			$roleSiteMapper = new Application_Model_RoleSiteMapper();
			$sites = $roleSiteMapper->getAllRoleSiteById($role_id);
			foreach($sites as $site){
				$data=array(
					"role_site_id" =>$site->__get("role_site_id"),
					"site_id" => $site->__get("site_id"),
					"cluster_id" =>$site->__get("cluster_id"),
					"state_id" =>$site->__get("state_id"),
				);
				$role_arr[]=$data;
			}
				//$role=implode(",", $data);
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $role_arr
				);
			
		} catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function getAllMenuByTypeAction(){
	
		try {
			$request = $this->getRequest();
			$menu_type=$request->getParam("menu_type");
			$menuMapper=new Application_Model_MenuMapper();
				
			$menues=$menuMapper->getAllMenuByType($menu_type);
			if(count($menues) >0){
				foreach ($menues as $menue) {
	
					$data=array(
							"menu_id" => $menue->__get("menu_id"),
							"menu_desc" => $menue->__get("menu_desc"),
							"parent_menu" => $menue->__get("parent_menu"),
							"menu_type" => $menue->__get("menu_type"),
							
								
					);
	
					$menu_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $menu_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllMenuIdByRoleIdAction() {
	
		try {
			$request = $this->getRequest();
			$role_id = $request->getParam("id");
			$permissionMapper=new Application_Model_PermissionMapper();
			$permissions = $permissionMapper->getAllMenuIdByRoleId($role_id);
			foreach($permissions as $permission){
				$data=array(
						"menu_id" =>$permission->__get("menu_id"),
						"role_id" => $permission->__get("role_id"),
 						"parent_menu" => $permission->__get("parent_menu"),
						"menu_desc" => $permission->__get("menu_desc"),
						"subMenu_name" => $permission->__get("subMenu_name"),
				);
				$permission_arr[]=$data;
			}
			
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" => $permission_arr
			);
				
		} catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}


public function addSmsProcessAction(){
		
		$request = $this->getRequest();
		$userRole_id=$request->getParam("user_role");
		$MsgLavel_id=$request->getParam("MsgLavel");
		$isMetered=$request->getParam("isMetered");
		$MsgType=explode(',', $MsgLavel_id);
		$role_msg_mapper=new Application_Model_RoleMessageMapper();
		$role_msg=new Application_Model_RoleMessage();
		$role_message=count($MsgType);
		
		for($i=$role_message;$i>0;$i--){
			$checkRole=$role_msg_mapper->checkRoleMessageById($userRole_id,$MsgType[$i-1]);
			if($checkRole==0){
			$role_msg->__set("role_id",$userRole_id);
			$role_msg->__set("message_id",$MsgType[$i-1]);
			$role_msg->__set("isMetered",$isMetered);
			$role_msg_mapper->addRoleMessage($role_msg);
		}
		}
		$userRole=new Application_Model_UserRolesMapper();
		$user_roles=$userRole->getUserRoleById($userRole_id);
		$data=array(
				"user_role_id" =>$user_roles->__get("user_role_id"),
				"user_role_name" =>$user_roles->__get("user_role"),
				"isMetered" =>$isMetered
		);
		$meta = array(
				"code" => 200,
				"message" => "SUCCESS"
		);
		$arr = array(
				"meta" => $meta,
				"data" => $data
		);
	
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
		}
		
public function getAllMessageAction(){
	
		try {
			$userRoleMapper=new Application_Model_MessagesMapper();
			
			$userRoles=$userRoleMapper->getAllMessages();
			if(count($userRoles) >0){
				$roleData=array();
				foreach ($userRoles as $userRole) {
					
				
					
					$data=array(
							"message_id" => $userRole->__get("message_id"),
							"message_type" => $userRole->__get("message_type"),
							"message_level" => $userRole->__get("message_level"),
							"message_name" => $userRole->__get("message_name")
							
							
					);
	
					$user_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $user_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "Error while getting"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
 public function getAllRoleMessageAction(){
	
	try {
		$userRoleMapper=new Application_Model_RoleMessageMapper();
		$messageMapper=new Application_Model_MessagesMapper();
		$userRolesMapper=new Application_Model_UserRolesMapper();	
		$userRoles=$userRoleMapper->getAllRoleMessage();
		 
		if(count($userRoles) >0){
			$userRole_id=array();
			$user_arr=array();
			foreach ($userRoles as $userRole) {
					$userRole_id[]=$userRole->__get("role_id");
			}
			$Roles_val=array_unique($userRole_id);
			$Roles=array_values($Roles_val);
		 
				for ($i=0;$i<count($Roles);$i++) {
					$roles_id=$userRoleMapper->getRoleMessageById($Roles[$i]);
					//print_r($roles_id);
					$message_id=array();
					$message_name=array();
					$is_metered;
					foreach ($roles_id as $role_id) {
						$message_id[]=$role_id["message_id"];
						$role_msg=$messageMapper->getRoleMessageById($role_id["message_id"]);
						$message_name[]=$role_msg->__get("message_name");
						$is_metered=$role_id["is_metered"];
					}
					 
					
// 					$is_meter=array_unique($is_metered);
					if($is_metered=='true'){
						$is_metered=true;
					}else{$is_metered=false;}
					$roleName=$userRolesMapper->getUserRoleById($Roles[$i]);
					$data=array(
							"role_name" => $roleName->__get("user_role"),
							"role_id" => $Roles[$i],
							"message_id" => implode(",", $message_id),
							"message_name" => implode(",", $message_name),
							"is_metered" => $is_metered
								
					);
					$user_arr[]=$data;
					
				
			}
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" => $user_arr,
			);
	
		}
		else{
			$meta = array(
					"code" => 200,
					"message" => "Error while getting"
			);
			$arr = array(
					"meta" => $meta,
					"data" =>array(),
			);
		}
	
	
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
	
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
	} 
	
	public function getAllRoleMessageByIdAction(){
	
		try {
			$request = $this->getRequest();
			$message_id = $request->getParam("id");
			$message_ids=explode(",", $message_id);
			$messageMapper=new Application_Model_MessagesMapper();
			$userRolesMapper=new Application_Model_UserRolesMapper();
			$userRoles= $messageMapper->getRoleMessageByMessageId($message_ids);
				
			if(count($userRoles) >0){
			 
				foreach ($userRoles as $userRole) {
					 
					$data=array(
							"message_id" => $userRole["message_id"],
							"message_type" => $userRole["message_type"],
							"message_level" => $userRole["message_level"],
							"message_name" => $userRole["message_name"]
					);
					$user_arr[]=$data;
						
	
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $user_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "Error while getting"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	public function deleteRoleMessageByIdAction(){
	
		try {
			$request=$this->getRequest();
			$role_message_id=$request->getParam("id");
			
			
			$RoleMessageMapper=new Application_Model_RoleMessageMapper();
			if($userRole=$RoleMessageMapper->deleteRoleMessageById($role_message_id)){
				 
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function updateRoleMsgByIdAction(){
	
	try {
		$request=$this->getRequest();
		$user_role_id=$request->getParam("id");
		$is_metered=$request->getParam("is_metered");
		$MsgLavel=$request->getParam("MsgLavel");
		 
		$userRoleMapper=new Application_Model_UserRolesMapper();
		$userRoles= new Application_Model_UserRoles();
		$RoleMessage=new Application_Model_RoleMessageMapper(); 
		$roleMsg=new Application_Model_RoleMessage();
		 
		$roleMsg->__set("role_id",$user_role_id);
		$roleMsg->__set("is_metered",$is_metered);
		$RoleMessage_is=$RoleMessage->updateRoleMsg($roleMsg); 	
	 	if($MsgLavel!='null'){
				$MsgLavel_id=explode(",", $MsgLavel);
				//print_r($site_id);
				$role_add=count($MsgLavel_id);
				if($role_add>0){
					for($i=$role_add;$i>0;$i--){
						$checkRole=$RoleMessage->checkRoleMessageById($user_role_id,$MsgLavel_id[$i-1]);
						if($checkRole==0){
							$roleMsg->__set("role_id",$user_role_id);
							$roleMsg->__set("message_id",$MsgLavel_id[$i-1]);
							$roleMsg->__set("isMetered",$is_metered);
							$RoleMessage->addRoleMessage($roleMsg);
	
						}
					}
				}
				$RoleMessage->deleteRoleMessageByMessageTypeId($user_role_id,$MsgLavel_id);
			
			}
			 
				
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
	
			);
	 
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
	
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
	}
		
}