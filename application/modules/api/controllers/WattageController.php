<?php
class Api_WattageController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	
	public function addWattageAction(){
	
		try {
			$request=$this->getRequest();
			$wattage_no=$request->getParam("wattage");
			$sites_id=$request->getParam("site_id");
			$wattageMapper=new Application_Model_WattageMapper();
			
			$site_id=explode(",", $sites_id);
			$temp=0;
			for ($i=0;$i<count($site_id);$i++){
				$checkWatt=$wattageMapper->checkWattageBySiteId($site_id[$i],$wattage_no);
				if(!$checkWatt){
					$temp=1;
					$wattage= new Application_Model_Wattage();
					$wattage->__set("wattage",intval($wattage_no));
					$wattage->__set("site_id",intval($site_id[$i]));
					
					$wattage_id=$wattageMapper->addNewWattage($wattage); 
					$this->_logger->info("New Wattage ID ".$wattage_id." has been created in wattage by ". $this->_userName.".");
				}
			}
			
			
				$data=array(
						"watt_id" => $wattage_id,
						"wattage" => $wattage_no,
				);
				
				if($temp==1){
					$meta = array(
							"code" => 200,
							"message" => "SUCCESS"
					);
				}else{
					$meta = array(
							"code" => 200,
							"message" => "Duplicate records"
					);
				}
	 		
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			 
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	

	
	
	public function getAllStatesAction(){
	
		try {
			$statesMapper=new Application_Model_StatesMapper();
			
			$states=$statesMapper->getAllStates();
	
			if(count($states) >0){
				foreach ($states as $state) {
					 
					$data=array(
							"state_id" => $state->__get("state_id"),
							"state_name" => $state->__get("state_name"),
							
					);
	
					$state_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $state_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "Error while getting"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	

	public function deleteWattageByIdAction(){

		try {
			$request=$this->getRequest();
			$site_id=$request->getParam("site_id");
			$wattageMapper=new Application_Model_WattageMapper();
			$watt=array(0);
			if($delWattage=$wattageMapper->deleteWattageBySiteId($site_id,$watt)){
				$this->_logger->info("Site Id ".$site_id." has been deleted from Wattage by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateWattageByIdAction(){
	
		try {
			$request=$this->getRequest();
			$sites_id=$request->getParam("site_id");
			$wattage_no=$request->getParam("wattage");

			$wattageMapper=new Application_Model_WattageMapper();
				
			$wattage_no_id=explode(",", $wattage_no);
			$temp=0;
			if(count($wattage_no_id)>0){
				$wattage_nos=$wattageMapper->checkWattageById($wattage_no_id);
			foreach ($wattage_nos as $wattages_no){
				$checkWatt=$wattageMapper->checkWattageBySiteId($sites_id,$wattages_no["wattage"]);
				if(!$checkWatt){
					$temp=1;
					$wattage= new Application_Model_Wattage();
					$wattage->__set("wattage",intval($wattages_no["wattage"]));
					$wattage->__set("site_id",intval($sites_id));
					
					$wattage_id=$wattageMapper->addNewWattage($wattage); 
					$this->_logger->info("New Wattage ID ".$wattage_id." has been created in wattage by ". $this->_userName.".");
				}
				$watts[]=$wattages_no["wattage"];
			}
			$delWattage=$wattageMapper->deleteWattageBySiteId($sites_id,$watts);
			}
				 
				 
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			 
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function getWattageBySiteIdAction(){
		
		try {
			$request=$this->getRequest();
			$site_id=$request->getParam("site_id");
		    $wattageMapper=new Application_Model_WattageMapper();
		    $watts=array();$wattage_vals=array();$wattage_v=array();$watt_val=array();
			if($wattages=$wattageMapper->getWattageById($site_id)){
				
				foreach ($wattages as $wattage){
					$data=array(
							"wattage" => $wattage["wattage"],
							"watt_id" => $wattage["watt_id"],
					);
					$watts[]=$data;
					$watt_val[]=$data;
					$wattage_v[]=$wattage["wattage"];
				}
			} 	
			if(count($wattage_v)==0){$wattage_v=array(0);}
				$wattagesAll=$wattageMapper->getWattageById(null,$site_id,$wattage_v);
				if($wattagesAll){
					foreach ($wattagesAll as $wattageAll){
						$data=array(
								"wattage" => $wattageAll["wattage"],
								"watt_id" => $wattageAll["watt_id"],
						);
						$watts[]=$data;
					}
				}
		        $meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $watts,
						"Allwatt" => $watt_val,
						 
				);
			  
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
		
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
		}
		
		public function getWattageByMultipleSiteIdAction(){
		
			try {
				$request=$this->getRequest();
				$site_ids=$request->getParam("site_id");
		
				$wattageMapper=new Application_Model_WattageMapper();
				$site=new Application_Model_Wattage();
				$site_id=explode(",", $site_ids);
				
		
				if($wattagesAll=$wattageMapper->getWattageByMultipleId($site_id)){
				 
					foreach ($wattagesAll as $wattageAll){
						$data=array(
								"wattage" => $wattageAll["wattage"],
								"watt_id" => $wattageAll["watt_id"],
						);
						$watts[]=$data;
					}
				 
				}
					$meta = array(
							"code" => 200,
							"message" => "SUCCESS"
					);
				$arr = array(
						"meta" => $meta,
						"Allwatt" => $watts,
						 
				);
				 
			}catch (Exception $e) {
				$meta = array(
						"code" => 501,
						"messgae" => $e->getMessage()
				);
		
				$arr = array(
						"meta" => $meta
				);
			}
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		}
}