<?php

/**
 * Created by PhpStorm.
 * User: Amit Bhardwaj
 * Date: 09-02-2017
 * Time: 15:57
 */
class Api_WalletTransactionsController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }

    //------------------------------------------------------------------------------------------------------------------
     public function getAllWalletTransactionsAction(){
 
        try {
            $request = $this->getRequest();
            $page = $request->getParam("page", 1);
            $WalletTransactionsMapper = new Application_Model_WalletTransactionsMapper();
            $consumersMapper = new Application_Model_ConsumersMapper();
            $collectionAgentMapper = new Application_Model_CollectionAgentsMapper();
            $objs = $WalletTransactionsMapper->getAllWalletTransactions();

            if(!$objs){
                throw new Exception('No results Found.');
            }
            $paginator = Zend_Paginator::factory($objs);
            $paginator->setItemCountPerPage(50);
            $paginator->setCurrentPageNumber($page);
            $objs = $paginator;
            $total_pages = $paginator->count();
            if ($page > $total_pages) {
                throw new Exception("No more pages", 555);
            }
            $current_page = $paginator->getCurrentPageNumber();

            if ($objs) {
                foreach ($objs as $obj) {
                    /* @var $obj Application_Model_WalletTransactions */
                    /*$consumer = $consumersMapper->getConsumerById($obj->__get("consumer_id"));
                    $collectionAgent = $collectionAgentMapper->getCollectionAgentById($obj->__get("user_id"));
                    $data = array(
                        "id" => $obj->__get("id"),
                        "consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
                        "consumer_id" => $obj->__get("consumer_id"),
                        "consumer_name" => ($consumer) ? $consumer->__get("consumer_name") : 'NA',
                        "user_id" => ($collectionAgent) ? $obj->__get("user_id") : null,
                        "user_full_name" => ($collectionAgent) ? sprintf('%s %s', $collectionAgent->__get("agent_fname"), $collectionAgent->__get("agent_lname")) : 'NA',
                        "receipt_number" => $obj->__get("receipt_number"),
                        "transaction_id" => $obj->__get("transaction_id"),
                        "entry_type" => $obj->__get("entry_type"),
                        "amount" => $obj->__get("amount"),
                        "timestamp" => $obj->__get("timestamp"),
                        "balance" => $obj->__get("balance"),
                        "comment" => $obj->__get("comment"),

                    );*/
                	$consumer = $consumersMapper->getConsumerById($obj["consumer_id"]);
                	$collectionAgent = $collectionAgentMapper->getCollectionAgentById($obj["user_id"]);
                	$data = array(
                			"id" => $obj["_id"],
                			"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
                			"consumer_id" => $obj["consumer_id"],
                			"consumer_name" => ($consumer) ? ucwords(strtolower($consumer->__get("consumer_name"))) : 'NA',
                			"user_id" => ($collectionAgent) ? $obj["user_id"] : null,
                			"user_full_name" => ($collectionAgent) ? sprintf('%s %s', $collectionAgent->__get("agent_fname"), $collectionAgent->__get("agent_lname")) : 'NA',
                			"receipt_number" => $obj["receipt_number"],
                			"transaction_id" => $obj["transaction_id"],
                			"entry_type" => $obj["entry_type"],
                			"amount" => number_format($obj["amount"], 2),
                			"timestamp" => $obj["timestamp"],
                			"balance" => number_format($obj["balance"],2),
                			"comment" => $obj["comment"],
                	
                	);
                    $obj_arr[] = $data;
                }

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                    "total_page" => $total_pages,
                    "current_page" => $current_page, 
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => isset($obj_arr) ? $obj_arr : [],
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "No Entry Found."
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage() 
            );
  
            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	
    public function addWalletTransactionsAction()
    {
        try {
            $request = $this->getRequest();
            //getting params
            $consumer_id = $request->getParam("consumer_id");
            $user_id = $request->getParam("user_id");
            $receipt_number = $request->getParam("receipt_number");
            $transaction_id = $request->getParam("transaction_id");
            $entry_type = $request->getParam("entry_type");
            $amount = $request->getParam("amount");
            $timestamp = $request->getParam("timestamp");
            $balance = $request->getParam("balance");

            //getting Mapper function
            $WalletTransactionsMapper = new Application_Model_WalletTransactionsMapper();

            //making obj
            $InsertRowObj = new Application_Model_WalletTransactions();

            //setting param to obj
            $InsertRowObj->__set("consumer_id", $consumer_id);
            $InsertRowObj->__set("user_id", $user_id);
            $InsertRowObj->__set("receipt_number", $receipt_number);
            $InsertRowObj->__set("transaction_id", $transaction_id);
            $InsertRowObj->__set("entry_type", $entry_type);
            $InsertRowObj->__set("amount", $amount);
            $InsertRowObj->__set("timestamp", $timestamp);
            $InsertRowObj->__set("balance", $balance);


            //Executing insert fun from mapper
            if ($insert_id = $WalletTransactionsMapper->addNewWalletTransactions($InsertRowObj)) {

                //prepairing api response data
                $data = array(
                    "id" => $insert_id,
                    "consumer_id" => $consumer_id,
                    "user_id" => $user_id,
                    "receipt_number" => $receipt_number,
                    "transaction_id" => $transaction_id,
                    "entry_type" => $entry_type,
                    "amount" => $amount,
                    "timestamp" => $timestamp,
                    "balance" => $balance,

                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding Entry"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;

    }

    public function deleteWalletTransactionByIdAction()
    {

        try {
            $request = $this->getRequest();
            $id = $request->getParam("id");
            $WalletTransactionsMapper = new Application_Model_WalletTransactionsMapper();

            if ($delStatus = $WalletTransactionsMapper->deleteWalletTransactionsById($id)) {

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting."
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }


    public function addBalanceAction()
    {
//        WHAT
//        TARA|CH|123|500
//        WHO: 9250480453 GET USER ID BY THIS

        try {
            $request = $this->getRequest();
            $who = $request->getParam("who");
            $what = $request->getParam("what");

			$mongoDriver= new MongoClient();
			$mongoDB=$mongoDriver->tara_devices; 
			$db=$mongoDB->wallet_transactions;
		
            $what = explode(" ", $what);
            if (count($what) < 4) {
                throw new Exception('Invalid Request.');
            }
            if (!$who || $who == '') {
                throw new Exception('Invalid mobile number.');
            }
            if($what[0] !== 'TARA'){
                throw new Exception('Invalid Request');
            }
            $type = $what[1]; //CH
            $consumer_connection_id = $what[2];
//            $consumerId = $what[2];
            $amount = $what[3];
            $date = new Zend_Date();
            $date->setTimezone("Asia/Calcutta");
			$timestamps = $date->toString("yyyy-MM-dd HH:mm:ss");
            $collectionAgentMapper = new Application_Model_CollectionAgentsMapper();
            $walletTransactionsMapper = new Application_Model_WalletTransactionsMapper();

            $collectionAgent = $collectionAgentMapper->getCollectionAgentByPhone($who);
            if (!$collectionAgent) {
                throw new Exception('No such Collection Agent found');
            }
            $consumerMapper = new Application_Model_ConsumersMapper();

            $consumer = $consumerMapper->getConsumerByConnectionID($consumer_connection_id);

            if (!$consumer || !$consumer->__get('consumer_id')) {
                throw new Exception('Invalid Consumer Id.');
            }
            $consumer_id = $consumer->__get('consumer_id');
            switch ($type) {
                case 'CH':

                    $walletTransaction = new Application_Model_WalletTransactions();
//                    $entryType = $type;
                    $receiptNumber = sprintf('CA-%s', $collectionAgent->__get('collection_agent_id'));

                    $timestamp = $date->toString("ddMMYYHHmmss" . rand(10, 99));
                    $intAmount = str_pad(intval($amount), 4, "0", STR_PAD_LEFT);
                    $transactionId = $timestamp . "-" . $intAmount;
                    $lastBalance = Application_Model_WalletTransactionsMapper::getConsumerBalance($consumer_id);

                    $newBalance = floatval($lastBalance) + floatval($amount);

                   /* $walletTransaction->__set('consumer_id', $consumer_id);
                    $walletTransaction->__set('user_id', $collectionAgent->__get('collection_agent_id'));
                    $walletTransaction->__set('receipt_number', $receiptNumber);
                    $walletTransaction->__set('transaction_id', $transactionId);
                    $walletTransaction->__set('entry_type', 'CREDIT');
                    $walletTransaction->__set('amount', $amount);
//                    $walletTransaction->__set('timestamp', $timestamp);
                    $walletTransaction->__set('balance', $newBalance);
//                    $walletTransaction->__set('comment', );

                    $result = $walletTransactionsMapper->addNewWalletTransaction($walletTransaction);*/ 
					$data_array= array("consumer_id" => $consumer_id,
							"user_id"=> $collectionAgent->__get('collection_agent_id'),
							"receipt_number" =>$receiptNumber,
							"transaction_id" =>$transactionId,
							"entry_type" =>'CREDIT',
							"amount" =>$amount,
							"timestamp" =>$timestamps,
							"balance"=> $newBalance,
							//"comment"=> $comment
					);
					
				   $result= $db->insert($data_array);
                    if($result){
                     
                        $consumer->__set('wallet_bal',$newBalance);
                        $updateConsumer = $consumerMapper->updateConsumer($consumer);
                    }
                    break;
                default:
                    throw new Exception('Invalid Type.');
                    break;

            }
            if (!$result) {
                throw new Exception('Some error occurred please contact admin.');
            }
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $arr = array(
                "meta" => $meta,
            );
        } catch (Exception $e) {
            $meta = array(
                "code" => 401,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }



}