<?php
class Api_OpexController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function getAllOpexAction(){

		try {
			$opexMapper=new Application_Model_OpexMapper();
			
			$opexes = $opexMapper->getAllOpex();
	
			if(count($opexes) >0){
				foreach ($opexes as $opex) {
					 
					$data=array(
							"id" => $opex->__get("id"),
							"types_of_cost" => $opex->__get("types_of_cost"),
							
					);
	
					$state_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $state_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "Error while getting"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
/*-------- 17 January 2018 (star) -----------------------*/	
	public function getAllOpexByMenuTypeAction(){
		
		try {
			$request = $this->getRequest();
			$menu_type = $request->getParam('menu');

			$opexMapper=new Application_Model_OpexMapper();
			
			$opexes = $opexMapper->getAllOpexByMenuType($menu_type);
		
			if(count($opexes) >0){
				foreach ($opexes as $opex) {
					 
					$data=array(
							"id" => $opex->__get("id"),
							"types_of_cost" => $opex->__get("types_of_cost"),
							"parent_id" => $opex->__get("parent_id"),
							"menu_type" => $opex->__get("menu_type")
					);
	
					$state_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $state_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "Error while getting"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function getAllOpexByParentIdAction(){
		
		try {
			$request = $this->getRequest();
			$opex_id = $request->getParam('id');

			$opexMapper=new Application_Model_OpexMapper();
			
			$opexes = $opexMapper->getAllOpexByParentId($opex_id);
		
			if($opexes){
				foreach ($opexes as $opex) {
					 
					$data=array(
							"id" => $opex->__get("id"),
							"types_of_cost" => $opex->__get("types_of_cost"),
							"parent_id" => $opex->__get("parent_id"),
							"menu_type" => $opex->__get("menu_type")
					);
	
					$state_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $state_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "Error while getting"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
/*----------------17january 2018(end)-------------------*/
}