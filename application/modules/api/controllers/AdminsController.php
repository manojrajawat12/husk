<?php

class Api_AdminsController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //header('Content-Type: application/json');
    }

    public function addAdminAction() {

        try {
            $request = $this->getRequest();
            $username = $request->getParam("username");
            $admin_fname = $request->getParam("admin_fname");
            $admin_email = $request->getParam("admin_email");
            $admin_lname = $request->getParam("admin_lname");
            $profile_image = $request->getParam("profile_image");
            $admin_role = $request->getParam("admin_role");
            $reset_code = $request->getParam("reset_code");
            $hashed_password = $request->getParam("hashed_password");
            $adminmapper = new Application_Model_AdminsMapper();
            // $clustermapper=new Application_Model_ClustersMapper();
            $admin = new Application_Model_Admins();
            $admin->__set("admin_fname", $admin_fname);
            $admin->__set("admin_lname", $admin_lname);
            $admin->__set("username", $username);
            $admin->__set("admin_email", $admin_email);
            $admin->__set("profile_image", $profile_image);
            $admin->__set("admin_role", $admin_role);
            $admin->__set("reset_code", $reset_code);
            $admin->__set("hashed_password", $hashed_password);

            if ($admin = $adminmapper->addNewAdmin($admin)) {

                $data = array(
                    'admin_id' => $admin,
                    'username' => $username,
                    'admin_fname' => $admin_fname,
                    'admin_lname' => $admin_lname,
                    'admin_email' => $admin_email,
                    'profile_image' => $profile_image,
                    'admin_role' => $admin_role,
                    'reset_code' => $reset_code,
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAdminByIdAction() {

        try {
            $request = $this->getRequest();
            $admin_id = $request->getParam("id");
            $adminmapper = new Application_Model_AdminsMapper();
            if ($admin = $adminmapper->getAdminById($admin_id)) {


                $data = array(
                    'username' => $admin->__get("username"),
                    'hashed_password' => $admin->__get("hashed_password"),
                    'admin_fname' => $admin->__get("admin_fname"),
                    'admin_lname' => $admin->__get("admin_lname"),
                    'admin_email' => $admin->__get("admin_email"),
                    'profile_image' => $admin->__get("profile_image"),
                    'admin_role' => $admin->__get("admin_role"),
                    'reset_code' => $admin->__get("reset_code"),
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while Fetching"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function deleteAdminByIdAction() {

        try {
            $request = $this->getRequest();
            $admin_id = $request->getParam("id");
            $adminmapper = new Application_Model_AdminsMapper();
            if ($admin = $adminmapper->deleteAdminById($admin_id)) {

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllAdminsAction() {

        try {
//             $request=$this->getRequest();
//             $admin_id=$request->getParam("id");
            $adminmapper = new Application_Model_AdminsMapper();
//            $clustermapper = new Application_Model_ClustersMapper();
            if ($admins = $adminmapper->getAllAdmins()) {

                foreach ($admins as $admin) {
                    
                     $data = array(
                    'username' => $admin->__get("username"),
                    'hashed_password' => $admin->__get("hashed_password"),
                    'admin_fname' => $admin->__get("admin_fname"),
                    'admin_lname' => $admin->__get("admin_lname"),
                    'admin_email' => $admin->__get("admin_email"),
                    'profile_image' => $admin->__get("profile_image"),
                    'admin_role' => $admin->__get("admin_role"),
                    'reset_code' => $admin->__get("reset_code"),
                    'admin_id' => $admin->__get("admin_id"),
                );

                    $admin_arr[] = $data;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $admin_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updateAdminByIdAction() {

        try {
            $request = $this->getRequest();
            $request = $this->getRequest();
            $username = $request->getParam("username");
            $admin_fname = $request->getParam("admin_fname");
            $admin_id = $request->getParam("admin_id");
            $admin_email = $request->getParam("admin_email");
            $admin_lname = $request->getParam("admin_lname");
            $profile_image = $request->getParam("profile_image");
            $admin_role = $request->getParam("admin_role");
            $reset_code = $request->getParam("reset_code");
            $hashed_password = $request->getParam("hashed_password");
            $adminmapper = new Application_Model_AdminsMapper();
            $admin = new Application_Model_Admins();
            $admin->__set("admin_fname", $admin_fname);
            $admin->__set("admin_id", $admin_id);
            $admin->__set("admin_lname", $admin_lname);
            $admin->__set("username", $username);
            $admin->__set("admin_email", $admin_email);
            $admin->__set("profile_image", $profile_image);
            $admin->__set("admin_role", $admin_role);
            $admin->__set("reset_code", $reset_code);
            $admin->__set("hashed_password", $hashed_password);
            
            if ($admins = $adminmapper->updateAdmin($admin)) {


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    
}
