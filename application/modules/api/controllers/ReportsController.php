<?php
ini_set('max_execution_time', 1000);
class Api_ReportsController extends Zend_Controller_Action {
	
    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
		$logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
		$this->_userId=$auth->getIdentity()->user_id;
    }
    public function destroyAction()
    {
    	unset($this);
    	gc_enable() ;gc_collect_cycles();gc_disable();
    }
    public function debitFromCsvAction()
    {
        defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        $filename = PUBLIC_PATH . "/html/import/missing_enteries.csv";
        $rows = $this->import($filename);
        
        $consumersMapper = new Application_Model_ConsumersMapper();
        $packagesMapper = new Application_Model_PackagesMapper();
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        $invalid = array();
        foreach($rows as $row)
        {
            $connection_id = $row["connection_id"];
            $consumer = $consumersMapper->getConsumerByConnectionID($connection_id);
            if($consumer->__get("consumer_name")=="")
            {
                $invalid[] = $connection_id;
            }
            else
            {
                $package_id = $consumer->__get("package_id");
                $package = $packagesMapper->getPackageById($package_id);
                if($package)
                {
                    $package_cost = $package->__get("package_cost");
                }
                else
                {
                    $package_cost = $package_id;
                }
                if($package_id!=5)
                {
                    $date = new Zend_Date("01-05-2015 10:00:00","dd-MM-yyyy HH:mm:ss");
                    $date->setTimezone("Asia/Kolkata");
                    $timestamp = $date->toString("ddMMYYss".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));

                    $cr_amount = str_pad($package_cost, 4, "0", STR_PAD_LEFT);
                    $transaction_id = $timestamp . "-" . $cr_amount;

                    $cashRegister = new Application_Model_CashRegister();
                    $cashRegister->__set("user_id", 0);
                    $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
                    $cashRegister->__set("cr_entry_type", "DEBIT");
                    $cashRegister->__set("cr_amount", $package_cost);
                    $cashRegister->__set("receipt_number", 0);
                    $cashRegister->__set("transaction_id", $transaction_id);
                    $cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
                    $cashRegisterMapper->addNewCashRegister($cashRegister);
                }
            }
        }
        
       unset($consumersMapper);
       unset($packagesMapper);
       unset($cashRegisterMapper);
        echo "<pre>";
        print_r($invalid);
        echo "</pre>";
    }
    public function importMonthlyCsvAction()
    {
        defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        $filename = PUBLIC_PATH . "/html/import/fakirtoli_monthly_feb15-me.csv";
        $rows = $this->import($filename);
        $consumersMapper = new Application_Model_ConsumersMapper();
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        foreach($rows as $row)
        {
            $consumer = $consumersMapper->getConsumersByConnectionId($row["consumer_connection_id"]);
            $consumer = $consumer[0];
            if($row["debit"]!=0)
            {

                if(strlen($row["date"])==0)
                {
                    $row["date"] = "00/00/00";
                }
                $date = new Zend_Date($row["date"],"dd/MM/20yy");
                $connection_date = $date->toString("yyyy-MM-dd HH:mm:ss");
                $timestamp = $date->toString("ddMMYY".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));
                $a = $row["debit"];
                if($a=="-")
                {
                    $a = 0;
                }
                $a = intval(str_replace(",","",$a));
                
                $tcr_amount = str_pad($a, 4, "0", STR_PAD_LEFT);
                $transaction_id = $timestamp . "-" . $tcr_amount;

                $cashRegister = new Application_Model_CashRegister();
                $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
                $cashRegister->__set("cr_entry_type", "DEBIT");
                $cashRegister->__set("cr_amount", intval($row["debit"]));
                $cashRegister->__set("transaction_id", $transaction_id);
                $cashRegister->__set("user_id", $row["collection_agent_id"]);
                $cashRegister->__set("receipt_number", "CA-" . $row["collection_agent_id"]);
                $cashRegister->__set("timestamp", $connection_date);

                $cashRegisterMapper->addNewCashRegister($cashRegister);
            }
            if($row["credit"]!=0)
            {
                if(strlen($row["date"])==0)
                {
                    $row["date"] = "00/00/00";
                }
                $date = new Zend_Date($row["date"],"dd/MM/20yy");
                $connection_date = $date->toString("yyyy-MM-dd HH:mm:ss");
                $timestamp = $date->toString("ddMMYY".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));
                $tcr_amount = str_pad(intval($row["credit"]), 4, "0", STR_PAD_LEFT);
                $transaction_id = $timestamp . "-" . $tcr_amount;

                $cashRegister = new Application_Model_CashRegister();
                $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
                $cashRegister->__set("cr_entry_type", "CREDIT");
                $cashRegister->__set("cr_amount", intval($row["credit"]));
                $cashRegister->__set("transaction_id", $transaction_id);
                $cashRegister->__set("user_id", $row["collection_agent_id"]);
                $cashRegister->__set("receipt_number", "CA-" . $row["collection_agent_id"]);
                $cashRegister->__set("timestamp", $connection_date);

                $cashRegisterMapper->addNewCashRegister($cashRegister);
            }
            
            if($row["activation"]!=0)
            {
                if(strlen($row["date"])==0)
                {
                    $row["date"] = "00/00/00";
                }
                $date = new Zend_Date($row["date"],"dd/MM/20yy");
                $connection_date = $date->toString("yyyy-MM-dd HH:mm:ss");
                $timestamp = $date->toString("ddMMYY".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));
                $tcr_amount = str_pad(intval($row["activation"]), 4, "0", STR_PAD_LEFT);
                $transaction_id = $timestamp . "-" . $tcr_amount;

                $cashRegister = new Application_Model_CashRegister();
                $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
                $cashRegister->__set("cr_entry_type", "ACTIVATION");
                $cashRegister->__set("cr_amount", intval($row["activation"]));
                $cashRegister->__set("transaction_id", $transaction_id);
                $cashRegister->__set("user_id", $row["collection_agent_id"]);
                $cashRegister->__set("receipt_number", "CA-" . $row["collection_agent_id"]);
                $cashRegister->__set("timestamp", $connection_date);

                $cashRegisterMapper->addNewCashRegister($cashRegister);
            }
        }
        
        unset($consumersMapper);
        unset($cashRegisterMapper);
    }
    public function importConsumersCsvAction()
    {
        defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        $filename = PUBLIC_PATH . "/html/import/fakirtoli_consumers.csv";
        $rows = $this->import($filename);
        $consumersMapper = new Application_Model_ConsumersMapper();
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        foreach($rows as $row)
        {
            $consumer = new Application_Model_Consumers();
            $consumer->__set("consumer_name",$row["consumer_name"]);
            $consumer->__set("consumer_father_name",$row["consumer_father_name"]);
            $consumer->__set("consumer_code",$row["consumer_code"]);
            $consumer->__set("consumer_connection_id",$row["consumer_connection_id"]);
            $consumer->__set("package_id",$row["package_id"]);
            $consumer->__set("site_id",$row["site_id"]);
            $consumer->__set("consumer_status",$row["status"]);
            $consumer->__set("is_micro_enterprise","0");
            $consumer->__set("consumer_act_charge",$row["consumer_act_charge"]);
            $date = new Zend_Date($row["consumer_act_date"],"dd/MM/20yy");
            
            //$date->setDate($row["consumer_act_date"]."-".$year,"dd-MMM-yyyy");
            $connection_date = $date->toString("yyyy-MM-dd HH:mm:ss");
            $consumer->__set("consumer_act_date",$connection_date);
            $consumer_id = $consumersMapper->addNewConsumer($consumer);
            
            
            $timestamp = $date->toString("ddMMYY".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));

            $amount = $row["consumer_act_collection"];
            $tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
            $transaction_id = $timestamp . "-" . $tcr_amount;

            $cashRegister = new Application_Model_CashRegister();
            $cashRegister->__set("consumer_id", $consumer_id);
            $cashRegister->__set("cr_entry_type", "ACTIVATION");
            $cashRegister->__set("cr_amount", $amount);
            $cashRegister->__set("transaction_id", $transaction_id);
            $cashRegister->__set("user_id", $row["collection_agent_id"]);
            $cashRegister->__set("receipt_number", "CA-" . $row["collection_agent_id"]);
            $cashRegister->__set("timestamp", $connection_date);

            $cashRegisterMapper->addNewCashRegister($cashRegister);
           
        }
        
        unset($consumersMapper);
        unset($cashRegisterMapper);
    }
    public function import($filename)
   {
       $file_handle = NULL;       
       $data = NULL;
       $keys = NULL;
       $record = NULL;

       if(file_exists($filename))
       {
           $file_handle = fopen($filename, "r");
       } 
       else
       {
           throw new Exception("File not found: " . $filename, null); 
       }

       while (!feof($file_handle) )
       {       
           $row = fgetcsv($file_handle, 1024);   
           
           if (is_array($row)) 
           {                    
               //set record array keys from csv header
               if ($keys == NULL) 
               {   
                   $keys = array_flip($row);
               } 
               //set record array values from csv row
               elseif ($keys != NULL)
               {
                   foreach ($keys as $key => $value) 
                   {
                       $record[$key] = $row[$value];
                   }
                   $data[] = $record; 
               }   
           }
       }   

       if($file_handle) fclose($file_handle); 

       return $data; 
   }
    public function managerReportTimeWiseAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $page = $request->getParam("page",1);
            $date = $request->getParam("date");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumers = $consumersMapper->getConsumersByColumnValue("site_id", $site_id);
            if(!$consumers)
            {
                throw new Exception("No consumers found",404);
            }
            $paginator = Zend_Paginator::factory($consumers);
            $paginator->setItemCountPerPage(500);
            $paginator->setCurrentPageNumber($page);
            $current_page = $paginator->getCurrentPageNumber();
            $total_pages = $paginator->count();
            if($page>$total_pages)
            {
                throw new Exception("No more pages", 404);
            }
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $data_arr = array();
            //echo microtime(true)."Loop start<br/>";
            foreach($paginator as $consumer)
            {
                //echo microtime(true)."Consumer start<br/>";
                $consumer_id = $consumer->__get("consumer_id");
                $consumer_status = $consumer->__get("consumer_status");
                $last_entry = $cashRegisterMapper->getLastestCHGEntryByConsumerId($consumer_id);
                $last_entry_timestamp_month = NULL;
                if($date!=NULL)
                {
                    $zendDate = new Zend_Date($date,"dd-MM-yyyy");
                }
                else
                {
                    $zendDate = new Zend_Date();
                }
                $zendDate->setTimezone("Asia/Calcutta");
                if($last_entry)
                {
                    $last_entry_timestamp = new Zend_Date($last_entry->__get("timestamp"),"yyyy-MM-dd HH:mm:ss");
                    $last_entry_timestamp_month = $last_entry_timestamp->toString(Zend_Date::MONTH_SHORT);
                    $current_month = $zendDate->toString(Zend_Date::MONTH_SHORT);
                    $month_diff = $current_month-$last_entry_timestamp_month;
                }
                else
                {
                    $month_diff = -1;
                }
                
                $month = intval($zendDate->toString("MM"));
                $year = $zendDate->toString("yyyy");
                $date_string = $zendDate->toString("01/MM/yyyy");
                $total_otp_due = $cashRegisterMapper->getTotalOTPDueByConsumerId($consumer_id, $month, $year);
                $otp_in = $cashRegisterMapper->getOTPInByConsumerId($consumer_id, $month, $year);
                $otp_carry_forward = $total_otp_due-$otp_in;
                $overdue_amount =  $cashRegisterMapper->getMonthlyOverDueByConsumerId($consumer_id,$date_string);
                $monthly_due = $cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id, $month, $year);
                $monthly_in = $cashRegisterMapper->getMonthlyInByConsumerId($consumer_id, $month, $year);
                $monthly_carry_forward = ($overdue_amount+$monthly_due)-$monthly_in;
                $otp_due = 0;
                if($total_otp_due==$otp_carry_forward)
                {
                    if($total_otp_due<100)
                    {
                        $otp_due = $total_otp_due;
                    }
                    else
                    {
                        $otp_due = 100;
                    }
                }
                $collection_amount = $otp_due+$monthly_carry_forward;
                $data = array(
                    "consumer_connection_id" => $consumer->__get("consumer_connection_id"),
                    "consumer_name" => $consumer->__get("consumer_name"),
                    "consumer_id" => $consumer->__get("consumer_id"),
                    "total_otp_due" => $total_otp_due,
                    "otp_in" => $otp_in,
                    "otp_carry_forward" => $otp_carry_forward,
                    "monthly_overdue" => $overdue_amount,
                    "monthly_due" => $monthly_due,
                    "monthly_in" => $monthly_in,
                    "monthly_carry_forward" => $monthly_carry_forward,
                    "collection_amount" => $collection_amount,
                    "last_entry_month" => $last_entry_timestamp_month,
                    "month_diff" => $month_diff
                );
                if($consumer_status=="banned")
                {
                    $data_arr["banned"][] = $data;
                }
                elseif($month_diff==0)
                {
                    $data_arr["paid"][] = $data;
                }
                elseif($month_diff==1)
                {
                    $data_arr["unpaid"][] = $data;
                }
                elseif($month_diff==-1)
                {
                    $data_arr["no_payments"][] = $data;
                }
                else
                {
                    $data_arr["over_paid"][] = $data;
                }
                //echo microtime(true)."Consumer End<br/>";
            }
            //echo microtime(true)."Loop end<br/>";
            
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS",
                "current_page" => $current_page,
                "total_pages" => $total_pages
            );
            
            $arr = array(
                "meta" => $meta,
                "data" => $data_arr
            ); 
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        
        unset($consumersMapper);
        unset($cashRegisterMapper);
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function managerReportAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $page = $request->getParam("page",1);
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumers = $consumersMapper->getConsumersByColumnValue("site_id", $site_id);
            if(!$consumers)
            {
                throw new Exception("No consumers found",404);
            }
            $paginator = Zend_Paginator::factory($consumers);
            $paginator->setItemCountPerPage(500);
            $paginator->setCurrentPageNumber($page);
            $current_page = $paginator->getCurrentPageNumber();
            $total_pages = $paginator->count();
            if($page>$total_pages)
            {
                throw new Exception("No more pages", 404);
            }
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $data_arr = array();
            foreach($paginator as $consumer)
            {
                $consumer_id = $consumer->__get("consumer_id");
                $zendDate = new Zend_Date();
                $month = intval($zendDate->toString("MM"));
                $year = $zendDate->toString("yyyy");
                $date_string = $zendDate->toString("01/MM/yyyy");
                $total_otp_due = $cashRegisterMapper->getTotalOTPDueByConsumerId($consumer_id, $month, $year);
                $otp_in = $cashRegisterMapper->getOTPInByConsumerId($consumer_id, $month, $year);
                $otp_carry_forward = $total_otp_due-$otp_in;
                $overdue_amount =  $cashRegisterMapper->getMonthlyOverDueByConsumerId($consumer_id,$date_string);
                $monthly_due = $cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id, $month, $year);
                $monthly_in = $cashRegisterMapper->getMonthlyInByConsumerId($consumer_id, $month, $year);
                $monthly_carry_forward = ($overdue_amount+$monthly_due)-$monthly_in;
                $otp_due = 0;
                if($total_otp_due==$otp_carry_forward)
                {
                    if($total_otp_due<100)
                    {
                        $otp_due = $total_otp_due;
                    }
                    else
                    {
                        $otp_due = 100;
                    }
                }
                $collection_amount = $otp_due+$monthly_carry_forward;
                $data = array(
                    "consumer_connection_id" => $consumer->__get("consumer_connection_id"),
                    "consumer_name" => $consumer->__get("consumer_name"),
                    "consumer_id" => $consumer->__get("consumer_id"),
                    "total_otp_due" => $total_otp_due,
                    "otp_in" => $otp_in,
                    "otp_carry_forward" => $otp_carry_forward,
                    "monthly_overdue" => $overdue_amount,
                    "monthly_due" => $monthly_due,
                    "monthly_in" => $monthly_in,
                    "monthly_carry_forward" => $monthly_carry_forward,
                    "collection_amount" => $collection_amount,
                );
                if($collection_amount==0)
                {
                    $data_arr["paid"][] = $data;
                }
                elseif($collection_amount>0)
                {
                    $data_arr["unpaid"][] = $data;
                }
                else
                {
                    $data_arr["over_paid"][] = $data;
                }
             
            }
        
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS",
                "current_page" => $current_page,
                "total_pages" => $total_pages
            );
            
            $arr = array(
                "meta" => $meta,
                "data" => $data_arr
            ); 
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        unset($consumersMapper);
        unset($cashRegisterMapper);
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function managerReportPlainAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $page = $request->getParam("page",1);
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumers = $consumersMapper->getConsumersByColumnValue("site_id", $site_id);
            if(!$consumers)
            {
                throw new Exception("No consumers found",404);
            }
            $paginator = Zend_Paginator::factory($consumers);
            $paginator->setItemCountPerPage(500);
            $paginator->setCurrentPageNumber($page);
            $current_page = $paginator->getCurrentPageNumber();
            $total_pages = $paginator->count();
            if($page>$total_pages)
            {
                throw new Exception("No more pages", 404);
            }
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $data_arr = array();
            //echo microtime(true)."Loop start<br/>";
            foreach($paginator as $consumer)
            {
                //echo microtime(true)."Consumer start<br/>";
                $consumer_id = $consumer->__get("consumer_id");
                $zendDate = new Zend_Date();
                $month = intval($zendDate->toString("MM"));
                $year = $zendDate->toString("yyyy");
                $date_string = $zendDate->toString("01/MM/yyyy");
                $total_otp_due = $cashRegisterMapper->getTotalOTPDueByConsumerId($consumer_id, $month, $year);
                $otp_in = $cashRegisterMapper->getOTPInByConsumerId($consumer_id, $month, $year);
                $otp_carry_forward = $total_otp_due-$otp_in;
                $overdue_amount =  $cashRegisterMapper->getMonthlyOverDueByConsumerId($consumer_id,$date_string);
                $monthly_due = $cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id, $month, $year);
                $monthly_in = $cashRegisterMapper->getMonthlyInByConsumerId($consumer_id, $month, $year);
                $monthly_carry_forward = ($overdue_amount+$monthly_due)-$monthly_in;
                $otp_due = 0;
                if($total_otp_due==$otp_carry_forward)
                {
                    if($total_otp_due<100)
                    {
                        $otp_due = $total_otp_due;
                    }
                    else
                    {
                        $otp_due = 100;
                    }
                }
                $collection_amount = $otp_due+$monthly_carry_forward;
                $data = array(
                    "consumer_connection_id" => $consumer->__get("consumer_connection_id"),
                    "consumer_name" => $consumer->__get("consumer_name"),
                    "consumer_id" => $consumer->__get("consumer_id"),
                    "total_otp_due" => $total_otp_due,
                    "otp_in" => $otp_in,
                    "otp_due" => $otp_due,
                    "otp_carry_forward" => $otp_carry_forward,
                    "monthly_overdue" => $overdue_amount,
                    "monthly_due" => $monthly_due,
                    "monthly_in" => $monthly_in,
                    "monthly_carry_forward" => $monthly_carry_forward,
                    "collection_amount" => $collection_amount,
                );
                if($collection_amount==0)
                {
                    $data["consumer_type"] = "paid";
                    $data_arr[] = $data;
                }
                elseif($collection_amount>0)
                {
                    if($otp_due==$collection_amount)
                    {
                        $data["consumer_type"] = "unpaid_otp";
                    }
                    else 
                    {
                        $data["consumer_type"] = "unpaid";
                    }
                    
                    $data_arr[] = $data;
                }
                else
                {
                    $data["consumer_type"] = "over_paid";
                    $data_arr[] = $data;
                }
                //echo microtime(true)."Consumer End<br/>";
            }
            //echo microtime(true)."Loop end<br/>";
            
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS",
                "current_page" => $current_page,
                "total_pages" => $total_pages
            );
            
            $arr = array(
                "meta" => $meta,
                "data" => $data_arr
            ); 
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function managerReportOldAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $page = $request->getParam("page",1);
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumers = $consumersMapper->getConsumersByColumnValue("site_id", $site_id);
            $paginator = Zend_Paginator::factory($consumers);
            $paginator->setItemCountPerPage(10);
            $paginator->setCurrentPageNumber($page);
            $current_page = $paginator->getCurrentPageNumber();
            $total_pages = $paginator->count();
            if($page>$total_pages)
            {
                throw new Exception("No more pages", 404);
            }
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $data_arr = array();
            //echo microtime(true)."Loop start<br/>";
            foreach($paginator as $consumer)
            {
                $latestEntry = $cashRegisterMapper->getLastestEntryByConsumerId($consumer->__get("consumer_id"));
                if($latestEntry)
                {
                    $flag = true;
                    $entryDate = new Zend_Date($latestEntry->__get("timestamp"),"yyyy-MM-dd HH:mm:ss");
                    $currentDate = new Zend_Date();
                    $currentDate->setTimezone("Asia/Calcutta");
                    $currentDate->subMonth(1);
                }
                else 
                {
                    $flag = false;
                }
                
                
                //echo microtime(true)."Consumer start<br/>";
                $consumer_id = $consumer->__get("consumer_id");
                $month = "01";
                $year = "2015";
                $zendDate = new Zend_Date();
                $monthNum = intval($zendDate->toString("MM"));
                $months_data = array();
                for($i=1;$i<=$monthNum;$i++)
                {
                    $month = $i;
                    $monthly_due = $cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id, $month, $year);
                    $monthly_in = $cashRegisterMapper->getMonthlyInByConsumerId($consumer_id, $month, $year);
                    $otp_in = $cashRegisterMapper->getOTPInByConsumerId($consumer_id, $month, $year);
                    $otp_due = $cashRegisterMapper->getOTPDueByConsumerId($consumer_id, $month, $year);
                    
                    $month_data = array(
                        "monthly_due" => $monthly_due,
                        "monthly_in" => $monthly_in,
                        "otp_in" => $otp_in,
                        "otp_due" => $otp_due,
                    );
                    $dateObj   = DateTime::createFromFormat('!m', $month);
                    $monthName = $dateObj->format('F'); // March
                    $months_data[$monthName] = $month_data;
                }
                $overdue_amount =  $cashRegisterMapper->getMonthlyOverDueByConsumerId($consumer_id);
                $data = array(
                        "consumer_connection_id" => $consumer->__get("consumer_connection_id"),
                        "consumer_name" => $consumer->__get("consumer_name"),
                        "overdue_amount" => $overdue_amount,
                        "months_data" => $months_data
                    );
                if($entryDate->isLater($currentDate) and $flag)
                {
                    $data_arr["paid"][] = $data;
                }
                else
                {
                    $data_arr["unpaid"][] = $data;
                }
                //echo microtime(true)."Consumer End<br/>";
            }
            //echo microtime(true)."Loop end<br/>";
            
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS",
                "current_page" => $current_page,
                "total_pages" => $total_pages
            );
            
            $arr = array(
                "meta" => $meta,
                "data" => $data_arr
            ); 
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function indexAction(){
        
    }
    public function templateAction()
    {
        try
        {
            $request = $this->getRequest();
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
	public function duplicateSmsReportsAction()
	    {
	    	try
	    	{
	    		$request = $this->getRequest();
	    		$site_id = $request->getParam("site_id");
	    		$startDate=$request->getParam("startdate");
	    		$endDate=$request->getParam("enddate");
	    		$page = $request->getParam("page",1);
	    		$consumersMapper = new Application_Model_ConsumersMapper();
	    		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$userMapper=new Application_Model_UsersMapper();
	    		$agentMapper=new Application_Model_CollectionAgentsMapper();
	    		
	    		$data_arr = array();
	    		//echo microtime(true)."Loop start<br/>";
	    		// for($year=date("Y");$year>=2014;$year--)
	    		//{ 
	    			$year=date("Y");
	    			$mon=($year==date("Y"))?date("n"):12;
	    			$month=$mon;
	    			//for($month=$mon;$month>=$mon-2;$month--)
	    			//{
	    		    $cycle=3;
	    			while($cycle)
	    			{
	    				//echo $cycle."**".$month."##".$year."\r\n";
	    				$cycle--;
	    				 $consumer_array_data=array();
	    				 $startDate=$year."-".$month."-01";
	    				 $endDate=$year."-".$month."-".date('t',strtotime($startDate));
	    				 $caseRegisters=$cashRegisterMapper->getDuplicateEntry($site_id,$startDate, $endDate);
	    				 $consumers = $consumersMapper->getDulplicateConsumers();
	    				
	    				if(count($consumers) >0)
	    				{
			    		foreach($consumers as $consumer)
			    		{
			    			//echo microtime(true)."Consumer start<br/>";
			    			$consumer_id = $consumer->__get("consumer_id");
			    			$cash_data=array();
			    			$cash_array=array();
			    			$count=0;
			    			$cash_count=0;
			    			foreach($caseRegisters as $cash)
			    			 {
			    			 	$cash_consumer_id= $cash->__get("consumer_id");
			    			 	if($cash_consumer_id ==$consumer_id)
			    			 	{
			    			 		$collection_agent_id=$cash->__get("user_id");
								$cr_type=$cash->__get("cr_entry_type"); 

								if($collection_agent_id==0 && $cash->__get("cr_entry_type")!="DEBIT"){
			    			 			$users_val=$userMapper->getUserById($cash->__get("user_val"));
			    			 			$first=$users_val->__get("user_fname");
			    			 			$second=$users_val->__get("user_lname")." ".$cash->__get("transaction_type");
			    			 		}else{
			    			 			$agents=$agentMapper->getCollectionAgentById($collection_agent_id);
			    			 			$first=$agents->__get("agent_fname");
			    			 			$second=$agents->__get("agent_lname")." ".$cash->__get("transaction_type");
			    			 		}

			    			 		$agents=$agentMapper->getCollectionAgentById($collection_agent_id);
			    			 		$date = new Zend_Date($cash->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
			    			 		$timestamp = $date->toString("MMM dd, yyyy HH:mm");
			    			 		
			    			 		$cashDataArray=array(
			    			 				"collection_agent_fname" => $first,
			    			 				"collection_agent_lname" => $second ,
			    			 				"collection_agent_id" => $agents->__get("collection_agent_id"),
			    			 				"cr_entry_type" => $cash->__get("cr_entry_type"),
			    			 				"cr_amount" => $cash->__get("cr_amount"),
			    			 				"timestamp"=>$timestamp,
			    			 				"cur_month"=> $date->toString("M"),
			    			 				"cur_year" =>$date->toString("YYYY"),
			    			 				"transaction_id"=> $cash->__get("transaction_id"),
			    			 				"cr_id"=>$cash->__get("cr_id"),
			    			 				"cr_status"=>$cash->__get("cr_status")
			    			 		);
			    			 	
			    			 			array_push($cash_data, $cashDataArray);
			    			 		}
			    			 
			    			 }
			    			if(count($cash_data)>0){
			    			 $consumer_data = array(
			    			 		"consumer_connection_id" => $consumer->__get("consumer_connection_id"),
			    			 		"consumer_name" => $consumer->__get("consumer_name"),
			    			 		"consumer_id" => $consumer->__get("consumer_id"),
			    			 		"cashRegisters" =>$cash_data,
			    			 		 
			    			 );
			    			array_push($consumer_array_data, $consumer_data); 
			    			}
			    			//echo microtime(true)."Consumer End<br/>";
			    		}
			    		$data= array(
			    				"month"=>date("F", mktime(0, 0, 0, $month, 10)),
			    				"year"=>$year,
			    				"consumer_data" =>$consumer_array_data,
			    		);
			    		$data_arr[] = $data;
	    				}
			    	$year=($month==1)?$year-1:$year;
			    	$month=($month==1)?12:$month-1;
			    	
	    			}
	    			
	    	//	}
	    			
	    			    
	    		$meta = array(
	    				"code" => 200,
	    				"message" => "SUCCESS",
	    				//"current_page" => $current_page,
	    				//"total_pages" => $total_pages
	    		);
	    
	    		$arr = array(
	    				"meta" => $meta,
	    				"data" => $data_arr
	    		);
	    	}
	    	catch(Exception $e)
	    	{
	    		$meta = array(
	    				"code" => $e->getCode(),
	    				"message" => $e->getMessage()
	    		);
	    
	    		$arr = array(
	    				"meta" => $meta
	    		);
	    	}
	    	$json = json_encode($arr, JSON_PRETTY_PRINT);
	    	echo $json;
	    }
	
    public function defaulterReportsAction()
	    {
	    	try
	    	{
	    		$request = $this->getRequest();
	    		$site_id = $request->getParam("site_id");
	    		$month=$request->getParam("month");
	    		$year=$request->getParam("year");
	    		$page = $request->getParam("page",1);
	    		$consumersMapper = new Application_Model_ConsumersMapper();
	    		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
	    		$consumers = $consumersMapper->getConsumersByColumnValue("site_id",$site_id);
	    	
	    		if(!$consumers)
	    		{
	    			throw new Exception("No data found",404);
	    		}
	    		$paginator = Zend_Paginator::factory($consumers);
	    		$paginator->setItemCountPerPage(500);
	    		$paginator->setCurrentPageNumber($page);
	    		$current_page = $paginator->getCurrentPageNumber();
	    		$total_pages = $paginator->count();
	    		if($page>$total_pages)
	    		{
	    			throw new Exception("No more pages", 404);
	    		}
	    
	    		$agentMapper=new Application_Model_CollectionAgentsMapper();
	    		
	    		//echo microtime(true)."Loop start<br/>";
	    		foreach($paginator as $consumer)
	    		{
	    			//echo microtime(true)."Consumer start<br/>";
	    			$consumer_id = $consumer->__get("consumer_id");
	    			$debit_amount_history=$credit_amount_history=$activation_amount_history=$monthly_amount_prev2=$total_paid_prev2=0;
	    			$monthly_amount_prev1=$total_paid_prev1=$monthly_amount_current=$total_paid_current=$otp_current=$carry_forward_amount=0;
	    			$cash_data=array();
	    			$cashRegister=$cashRegisterMapper->getDefaulterCashAmounts($consumer_id,$month, $year);
	    			$consumer_act_charge=$consumer->__get("consumer_act_charge");
	    			foreach($cashRegister as $cash)
	    			{
    					$debit_amount_history=$cash->__get("debit_amount_history");
			    		$credit_amount_history=$cash->__get("credit_amount_history");
			    		$activation_amount_history=$cash->__get("activation_amount_history");
			    		$monthly_amount_prev2=$cash->__get("monthly_amount_prev2");
			    		$total_paid_prev2=$cash->__get("total_paid_prev2");
			    		$monthly_amount_prev1=$cash->__get("monthly_amount_prev1");
			    		$total_paid_prev1=$cash->__get("total_paid_prev1");
			    		$monthly_amount_current=$cash->__get("monthly_amount_current");
			    		$total_paid_current=$cash->__get("total_paid_current");
			    		$otp_current=$cash->__get("otp_current");
			    		$carry_forward_amount=$cash->__get("carry_forward_amount");
	    			} 
	   
	    			$data = array(
	    					"consumer_connection_id" => $consumer->__get("consumer_connection_id"),
	    					"consumer_name" => $consumer->__get("consumer_name"),
	    					"consumer_id" => $consumer->__get("consumer_id"),
	    					"consumer_act_charge" =>$consumer_act_charge,
	    					"debit_amount_history"=>$debit_amount_history,
	    					"credit_amount_history"=>$credit_amount_history,
	    					"activation_amount_history"=>$activation_amount_history, 
	    					"monthly_amount_prev2"=>$monthly_amount_prev2,
	    					"total_paid_prev2"=>$total_paid_prev2,
	    					"monthly_amount_prev1"=>$monthly_amount_prev1, 
	    					"total_paid_prev1"=>$total_paid_prev1,
	    					"monthly_amount_current"=>$monthly_amount_current, 
	    					"total_paid_current"=>$total_paid_current, 
	    					"otp_current"=>$otp_current,
	    					"carry_forward_amount"=>$carry_forward_amount,
	    					"carry_forward_otp"=>($consumer_act_charge - $activation_amount_history)-$otp_current,
	    			);
	    			
	    			$data_arr[] = $data;
	    			//echo microtime(true)."Consumer End<br/>";
	    		}
	    		//echo microtime(true)."Loop end<br/>";
	    	  
	    		$meta = array(
	    				"code" => 200,
	    				"message" => "SUCCESS",
	    				"current_page" => $current_page,
	    				"total_pages" => $total_pages
	    		);
	    	  
	    		$arr = array(
	    				"meta" => $meta,
	    				"data" => $data_arr
	    		);
	    	}
	    	catch(Exception $e)
	    	{
	    		$meta = array(
	    				"code" => $e->getCode(),
	    				"message" => $e->getMessage()
	    		);
	    	  
	    		$arr = array(
	    				"meta" => $meta
	    		);
	    	}
	    	$json = json_encode($arr, JSON_PRETTY_PRINT);
	    	echo $json;
	    }

	public function changeStatusAction()
	    {
	    	try
	    	{
	    		$request = $this->getRequest();
	    		$cr_id = $request->getParam("cr_id");
	    		$status=$request->getParam("status");
	    		$mode=$request->getParam("mode");
	    		$remark=$request->getParam("remark");
	    		$pend_table=$request->getParam("pending"); 
	    		$page = $request->getParam("page",1);
	    		$consumersMapper = new Application_Model_ConsumersMapper();
	    		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$lastStatus=$cashRegisterMapper->getCashRegisterById($cr_id);
	    		$updateRecord=$cashRegisterMapper->updateStatus($cr_id, $status,$mode,$remark,$pend_table,$this->_userId);
				$NewStatus=$cashRegisterMapper->getCashRegisterById($cr_id);
			
	    		
	    		$data = array();
	    		$data_arr[] = $data; 
	    			//echo microtime(true)."Consumer End<br/>";
	    	
	    if($updateRecord){
			$lastStatausValue=$lastStatus->__get("cr_status");
			$NewStatusValue=$NewStatus->__get("cr_status");
			$this->_logger->info("CashRegister Id ".$cr_id." has been updated where Cr_status ".$lastStatausValue." change to ".$NewStatusValue." by ". $this->_userName.".");
	    	
	    		$meta = array(
	    				"code" => 200,
	    				"message" => "SUCCESS",
	    		);
	    		$arr = array(
	    				"meta" => $meta,
	    				"data" => $data_arr
	    		);
	    		}
       else{
    			$meta = array(
    					"code" => $e->getCode(),
    					"message" => "Request has been failed.Please try again.",
    			);
    				
    			$arr = array(
    					"meta" => $meta
    			);
    		}
	    
    		}
    		catch(Exception $e)
    		{
    			$meta = array(
    					"code" => $e->getCode(),
    					"message" => $e->getMessage()
    			);
    			 
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    		
    		$json = json_encode($arr, JSON_PRETTY_PRINT);
    		echo $json;
}


public function mismatchedValueReportsAction()
{
	try
	{
		$request = $this->getRequest();
		$site_id = $request->getParam("site_id");
		$startDate=$request->getParam("startdate");
		$endDate=$request->getParam("enddate");
		$page = $request->getParam("page",1);
		$consumersMapper = new Application_Model_ConsumersMapper();
		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
		$consumers = $consumersMapper->getConsumersByColumnValue("site_id",$site_id);
		if(!$consumers)
		{
			throw new Exception("No data found",404);
		}
		$paginator = Zend_Paginator::factory($consumers);
		$paginator->setItemCountPerPage(500);
		$paginator->setCurrentPageNumber($page);
		$current_page = $paginator->getCurrentPageNumber();
		$total_pages = $paginator->count();
		if($page>$total_pages)
		{
			throw new Exception("No more pages", 404);
		}
		$data_arr = array();
		foreach($paginator as $consumer)
		{
			$consumer_id = $consumer->__get("consumer_id");
			$caseRegisters=$cashRegisterMapper->getMismatchedValue($consumer_id,$site_id,$startDate, $endDate);

			//print_r($caseRegisters);
			$cash_data=array();
			
			foreach($caseRegisters as $cash)
			{
				$cash_consumer_id= $cash->__get("consumer_id");
				if($cash_consumer_id == $consumer_id)
				{
					$date = new Zend_Date($cash->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
					$timestamp = $date->toString("MMM dd, yyyy HH:mm");
					$cashDataArray=array(
							"cr_entry_type" => $cash->__get("cr_entry_type"),
							"cr_amount" => $cash->__get("cr_amount"),
							"package_cost"=>$cash->__get("package_cost"),
							"timestamp"=>$timestamp,
					);
					array_push($cash_data, $cashDataArray);
				}
			}
			$data = array(
					"consumer_id" => $consumer->__get("consumer_id"),
					"consumer_connection_id" => $consumer->__get("consumer_connection_id"),
					"consumer_name" => $consumer->__get("consumer_name"),
					"cashRegisters" =>$cash_data
			);
			$data_arr[] = $data;
			 
		}
		$meta = array(
				"code" => 200,
				"message" => "SUCCESS",
				"current_page" => $current_page,
				"total_pages" => $total_pages
		);
	  
		$arr = array(
				"meta" => $meta,
				"data" => $data_arr
		);
	}
	catch(Exception $e)
	{
		$meta = array(
				"code" => $e->getCode(),
				"message" => $e->getMessage()
		);
	  
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}

public function duplicateTransactionReportsAction()
{
	try
	{
		$request = $this->getRequest();
		
		$page = $request->getParam("page",1);
		$total_pages=0;
		$current_page=0;
		$total_consumers=0;
		$duplicate = $request->getParam("duplicate");
		$pending_delete = $request->getParam("pending_delete");
		
		if($duplicate!=true)
		{
			$duplicate=false;
		}
		if($pending_delete!=true)
		{
			$pending_delete=false;
		}
		
		
		$consumersMapper = new Application_Model_ConsumersMapper();
		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
		$siteMapper=new Application_Model_SitesMapper();
		$roleSession = new Zend_Session_Namespace('roles');
		$role_sites_id=$roleSession->site_id;
		
		$sites=$siteMapper->getAllSites(null,null,$role_sites_id);
		foreach ($sites as $site){
			$site_id=$site->__get("site_id");
			$site_name=$site->__get("site_name");
			
		$agentMapper=new Application_Model_CollectionAgentsMapper();
		 $userMapper=new Application_Model_UsersMapper();
		$data_arr = array();
		$entry_data=array();
		
		$year=date("Y");
		$mon=($year==date("Y"))?date("n"):12;
		$month=$mon;
		
		$cycle=3;
		while($cycle)
		{
			$cycle--;
			$consumer_array_data=array();
			$startDate=$year."-".$month."-01";
			$endDate=$year."-".$month."-".date('t',strtotime($startDate));
			
			$caseRegisters=$cashRegisterMapper->getDuplicateEntry($site_id,$startDate, $endDate,$duplicate,$pending_delete);
			$consumers = $consumersMapper->getDulplicateConsumers();
	
			if(count($consumers) >0)
			{
				foreach($consumers as $consumer)
				{
					//echo microtime(true)."Consumer start<br/>";
					$consumer_id = $consumer->__get("consumer_id");
					$cash_data=array();
					$cash_array=array();
					
					$count=0;
					$cash_count=0;
					
					foreach($caseRegisters as $cash)
					{
						$cash_consumer_id= $cash->__get("consumer_id");
						if($cash_consumer_id == $consumer_id)
						{
								if ($cash->__get("transaction_type")=='(W)') {
									$users = $userMapper->getUserById($cash->__get("user_id"));
									$agent_fname= $users->__get("user_fname");
									$agent_lname=$users->__get("user_lname");
                                 } else {
									$collectionAgent = $agentMapper->getCollectionAgentById($cash->__get("user_id"));
									$agent_fname= $collectionAgent->__get("agent_fname");
									$agent_lname=$collectionAgent->__get("agent_lname");
                                }
								$collection_agent_id=$cash->__get("user_id");
								$agents=$agentMapper->getCollectionAgentById($collection_agent_id);
								$date = new Zend_Date($cash->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
								$timestamp = $date->toString("MMM dd, yyyy HH:mm");
								
								$cashDataArray=array(
										"collection_agent_fname" => $agent_fname,
										"collection_agent_lname" => $agent_lname,
										"collection_agent_id" => $agents->__get("collection_agent_id"),
										"cr_entry_type" => $cash->__get("cr_entry_type"),
										"cr_amount" => $cash->__get("cr_amount"),
										"timestamp"=>$timestamp,
										"cur_month"=> $date->toString("M"),
										"cur_year" =>$date->toString("YYYY"),
										"transaction_id"=> $cash->__get("transaction_id"),
										"cr_id"=>$cash->__get("cr_id"),
										"cr_status"=>$cash->__get("cr_status")
								);

								array_push($cash_data, $cashDataArray);
						}
						 
					}
					if(count($cash_data)>0){
						$consumer_data = array(
								"consumer_connection_id" => $consumer->__get("consumer_connection_id"),
								"consumer_name" => $consumer->__get("consumer_name"),
								"consumer_id" => $consumer->__get("consumer_id"),
								
								"cashRegisters" =>$cash_data,

						);
						array_push($consumer_array_data, $consumer_data);
					}
					//echo microtime(true)."Consumer End<br/>";
				}
				$data= array(
						"month"=>date("F", mktime(0, 0, 0, $month, 10)),
						"year"=>$year,
						"consumer_data" =>$consumer_array_data,
						
				);
				$entry_data[]= $data;
				
				
				
			}
			$year=($month==1)?$year-1:$year;
			$month=($month==1)?12:$month-1;

		}
		//print_r($entry_data);
		$data_arr=array(
				"enteries"=>$entry_data,
				"site_id" => $site_id,
				"site_name"=>$site_name,
		);
		$site_data[]=$data_arr;
		}
		
			/* 	$total_consumers = count($site_data);
				$paginator=Zend_Paginator::factory($site_data);
				$paginator->setItemCountPerPage(2);
				$paginator->setCurrentPageNumber($page);
				$site_data=$paginator;
				$total_pages=$paginator->count();
				if($page>$total_pages)
				{
					throw new Exception("No more pages",555);
				}
				$current_page=$paginator->getCurrentPageNumber();

 */
		$meta = array(
				"code" => 200,
				"message" => "SUCCESS",
				//"current_page" => $current_page,
				//"total_pages" => $total_pages
		);
	  
		$arr = array(
				"meta" => $meta,
				"data" => $site_data,
			
		);
	}
	catch(Exception $e)
	{
		$meta = array(
				"code" => $e->getCode(),
				"message" => $e->getMessage()
		);
	  
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}

public function cmDefaulterReportAction()
{
	try
	{
		$request = $this->getRequest();
		$site_id = $request->getParam("site_id");
		
		$siteMapper=new Application_Model_SitesMapper();
		$sitesdata=$siteMapper->getSiteById($site_id);
		$site_name=$sitesdata->__get("site_name");
		$zendDate = new Zend_Date();
		$curDate=$zendDate->toString('yyyy-MM-dd HH:MM:ss');
		
		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
		$defaulter_arr=$cashRegisterMapper->getdefaultersEntry($curDate,$site_id);
		$data_arr = array();
		
		//print_r($defaulter_arr);
		foreach($defaulter_arr as $defaulter)
		{
			$issue_level="";
			 $cond1=$defaulter["Cond1"];
			 $cond2=$defaulter["Cond2"];
			 $notPaid=$defaulter["is_notpaid"];
			$pending= $defaulter["total_debit"]-$defaulter["total_credit"];
			if($defaulter["consumer_status"]=="banned")
			{
				$issue_level="Z-Disconnected";
			}
			else if((($cond1==1 && $cond2==1) && $notPaid==1) && $pending>240){
				$issue_level="Priority1";
			}
			else if($cond1==1 && $cond2==1)
			{
				$issue_level="Priority2";
			}
			else if($notPaid==1)
			{
				$issue_level="Not paid in current cycle";
			}
			
			$consumer_id = $defaulter["consumer_id"];
			$lastMonth=$cashRegisterMapper->lastPaymentMonth($consumer_id);
			
			$data = array(
					"consumer_connection_id" => $defaulter["consumer_connection_id"],
					"consumer_name" => $defaulter["consumer_name"],
					"consumer_id" =>  $defaulter["consumer_id"],
					"consumer_act_charge" => $defaulter["consumer_act_charge"],
					"consumer_status" => $defaulter["consumer_status"],
					"issue_level" => $issue_level,
						
					"total_debit" => $defaulter["total_debit1"],
					"total_activation" =>  $defaulter["total_activation1"],
					"total_credit" =>$defaulter["total_credit1"],
						
					"total_debit1" =>$defaulter["total_debit2"],
					"total_activation1" =>  $defaulter["total_activation2"],
					"total_credit1" =>$defaulter["total_credit2"],
						
					"total_debit2" =>$defaulter["total_debit3"],
					"total_activation2" =>  $defaulter["total_activation3"],
					"total_credit2" => $defaulter["total_credit3"],

					"totalDebit" => $defaulter["total_debit"],
					"totalActivation" =>  $defaulter["total_activation"],
					"totalCredit" => $defaulter["total_credit"],
					"total_amount" =>0,
					"month" =>$lastMonth,
					"cond1"=>$defaulter["Cond1"],
					"cond2"=>$defaulter["Cond2"],
					"notPaid"=>$defaulter["is_notpaid"],
					"pending"=> $defaulter["total_debit"]-$defaulter["total_credit"],
					
			);
			$data_arr[] = $data;
				
		}
		 

		$meta = array(
				"code" => 200,
				"message" => "SUCCESS",
				"current_page" => 1,
				"total_pages" => 1,
				"site_name"=>$site_name
		);

		$arr = array(
				"meta" => $meta,
				"data" => $data_arr,
				
		);
	}
	catch(Exception $e)
	{
		$meta = array(
				"code" => $e->getCode(),
				"message" => $e->getMessage()
		);

		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}

public function cmNextMonthReportAction()
{
	try
	{
		$request = $this->getRequest();
		$site_id = $request->getParam("site_id");
		$page = $request->getParam("page",1);
		$consumersMapper = new Application_Model_ConsumersMapper();
		$packageMapper=new Application_Model_PackagesMapper();
		
		
		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
		$data_arr = array();
		
		$cashRegister_entrys=$cashRegisterMapper->getCmNextMonthReport($site_id);
		$packageCost=0;
		foreach($cashRegister_entrys as $cashRegister_entry)
		{
			$consumers=$consumersMapper->getConsumerById($cashRegister_entry["consumer_id"]);
			$packages=$packageMapper->getPackageById($consumers->__get("package_id"));
			$packageCost=$cashRegister_entry["packageCost"];//$packages->__get("package_cost");
			$carry_forward=$cashRegister_entry["monthly_carry_forward"];
			if($consumers->__get("consumer_status")=="banned" || $consumers->__get("package_id") ==5 ){
				if($consumers->__get("consumer_status")=="banned"){
					$to_collect="INACTIVE";
					$packageCost="INACTIVE";
					$carry_forward="INACTIVE";
				}
				//elseif($packages->__get("is_postpaid") ==1 )
			elseif($consumers->__get("package_id") ==5 )
				{
					//$to_collect="METER";
					if($cashRegister_entry["prev_due"] !=0)
					{
					 $to_collect=Min( $cashRegister_entry["due100"],$cashRegister_entry["carry_forward"]);
					}
					else{
					$to_collect=0;
					}
					$packageCost="METER";
					$carry_forward="METER";
				}
			}elseif($cashRegister_entry["carry_forward"]<0){
				
				$to_collect= Max($cashRegister_entry["monthly_carry_forward"],$packageCost);
				
			}else{
				$to_collect=Min( $cashRegister_entry["due100"],$cashRegister_entry["carry_forward"])+ Max($cashRegister_entry["monthly_carry_forward"],$packageCost);
				
			}	
			
			
			$data=array(
				"consumer_id" => $cashRegister_entry["consumer_id"],
				"consumer_act_charge" => $cashRegister_entry["consumer_act_charge"],
				"consumer_name" => $cashRegister_entry["consumer_name"],
				"consumer_connection_id" => $cashRegister_entry["consumer_connection_id"],
				//"package_cost"=>$packages->__get("package_cost"),	
				"consumer_status"=>$consumers->__get("consumer_status"),
				"total_debit" => $cashRegister_entry["total_debit"],
				"total_credit" => $cashRegister_entry["total_credit"],
				"total_activation" => $cashRegister_entry["total_activation"],
				"total_debit1" => $cashRegister_entry["total_debit1"],
				"total_credit1" => $cashRegister_entry["total_credit1"],
				"total_activation1" =>$cashRegister_entry["total_activation1"],
				"prev_due" =>$cashRegister_entry["prev_due"],
				"paid" => $cashRegister_entry["paid"],
				"carry_forward" => $cashRegister_entry["carry_forward"],
				"prev_due_month" => $cashRegister_entry["prev_due_month"],
				"debit_current_month" => $packageCost,
				"paid_current_month" => $cashRegister_entry["paid_current_month"],
				"monthly_carry_forward" =>$carry_forward ,
				"due100" => $cashRegister_entry["due100"],
				"to_collect"=>$to_collect,
				//"total_amount"=>$total_amount
			);
			$data_arr[]=$data;
		
		}
		

		$meta = array(
				"code" => 200,
				"message" => "SUCCESS",
			
		);

		$arr = array(
				"meta" => $meta,
				"data" => $data_arr
		);
	}
	catch(Exception $e)
	{
		$meta = array(
				"code" => $e->getCode(),
				"message" => $e->getMessage()
		);

		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}
}

	
?>