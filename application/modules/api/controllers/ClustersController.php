<?php

class Api_ClustersController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
    }
    
        public function addClusterAction(){
      
         try {
             $request=$this->getRequest();
             $cluster_name=$request->getParam("name");
             $cluster_manager_id=$request->getParam("cluster_manager_id");
             $state_id=$request->getParam("state_id");
             $clustermapper=new Application_Model_ClustersMapper();
             $cluster= new Application_Model_Clusters();
             $cluster->__set("cluster_name",$cluster_name);
             $cluster->__set("cluster_manager_id",$cluster_manager_id);
             $cluster->__set("state_id",$state_id);
         if($cluster=$clustermapper->addNewCluster($cluster)){
         	
         	$this->_logger->info("New cluster ID ".$cluster." has been created in clusters by ". $this->_userName.".");
         	         	
            $usersMapper = new Application_Model_UsersMapper();
            $user = $usersMapper->getUserById($cluster_manager_id);
            $stateMapper=new Application_Model_StatesMapper();
            $state_name=$stateMapper->getStateById($state_id);
            $data=array(     
                "cluster_id" => $cluster,
                "cluster_name" => $cluster_name,
                "cluster_manager_id" => $cluster_manager_id,
                "cluster_manager_name" => $user->__get("user_fname")." ".$user->__get("user_lname"),
            	"state_id" => $state_id,
            	"state_name" =>$state_name->__get("state_name"),
              );
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }

          public function getClusterByIdAction(){
      
         try {
             $request=$this->getRequest();
             $cluster_id=$request->getParam("id");
             $clustermapper=new Application_Model_ClustersMapper();
         if($cluster=$clustermapper->getClusterById($cluster_id)){
            $user_id =$cluster->__get("cluster_manager_id");
            $usersMapper = new Application_Model_UsersMapper();
            $user = $usersMapper->getUserById($user_id);
            $stateMapper=new Application_Model_StatesMapper();
            $state_name=$stateMapper->getStateById($cluster->__get("state_id"));
            $data=array(     
                "cluster_id" => $cluster->__get("cluster_id"),
            	"cluster_name" => $cluster->__get("cluster_name"),
                "cluster_manager_id" => $cluster->__get("cluster_manager_id"),
                "cluster_manager_name" => $user->__get("user_fname")." ".$user->__get("user_lname"),
            	"state_id" =>$cluster->__get("state_id"),
            	"state_name" =>$state_name->__get("state_name"),
            		
            );
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function deleteClusterByIdAction(){
      
         try {
             $request=$this->getRequest();
             $cluster_id=$request->getParam("id");
             $clustermapper=new Application_Model_ClustersMapper();
             $cluster_name=$clustermapper->getClusterById($cluster_id);
             $clusters=$cluster_name->__get("cluster_name");
         if($clustermapper->deleteClusterById($cluster_id)){
         	
         	$this->_logger->info("cluster Id ".$cluster_id." has been deleted from clusters by ". $this->_userName.".");
                         
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function getAllClustersAction(){
      
         try {
//             $request=$this->getRequest();
//             $cluster_id=$request->getParam("id");
				$request=$this->getRequest();
				$allCluster=$request->getParam("allCluster");
               $clustermapper=new Application_Model_ClustersMapper();
               $auth=new My_Auth('user');
               $user=$auth->getIdentity()->user_role;
               $state_id=$auth->getIdentity()->state_id;
               $cluster_id=$auth->getIdentity()->clusters;
               $clusters=explode('c', $cluster_id);
               
               $roleSession = new Zend_Session_Namespace('roles');
               $clusters_id=$roleSession->cluster_id;
               
         if($clusters=$clustermapper->getAllClusters($user,$allCluster,$clusters_id)){
          $usersMapper = new Application_Model_UsersMapper();
          $stateMApper=new Application_Model_StatesMapper();
             foreach ($clusters as $cluster) {
                 $user_id =$cluster->__get("cluster_manager_id");
                $user = $usersMapper->getUserById($user_id);
                $state_name=$stateMApper->getStateById($cluster->__get("state_id"));
                 $data=array(
                  "cluster_id" =>$cluster->__get("cluster_id"),
                  "cluster_name" => $cluster->__get("cluster_name"),
                  "cluster_manager_id" => $cluster->__get("cluster_manager_id"),
                  "cluster_manager_name" => $user->__get("user_fname")." ".$user->__get("user_lname"),
                  "state_id"=>$cluster->__get("state_id"),
                  "state_name"=>$state_name->__get("state_name"),
                 		
                 );
                 
                 $cluster_arr[]=$data;
             }
            
            
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $cluster_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
            public function updateClusterByIdAction(){
      
         try {
              $request=$this->getRequest();
              $cluster_id=$request->getParam("id");
              $cluster_name=$request->getParam("name");
              $cluster_manager_id = $request->getParam("cluster_manager_id");
              $state_id=$request->getParam("state_id");
              $clustermapper=new Application_Model_ClustersMapper();
              $cluster=new Application_Model_Clusters();
              $cluster->__set("cluster_id" ,$cluster_id);
              $cluster->__set("cluster_name" ,$cluster_name);
              $cluster->__set("cluster_manager_id" ,$cluster_manager_id);
              $cluster->__set("state_id" ,$state_id);
              $Last_Cluster=array();
              $Last_Cluster=$clustermapper->getClusterById($cluster_id);
              if($clusters=$clustermapper->updateCluster($cluster)){
              	$lastCluster_arr=array(
              			
              			"ClusterName" => $Last_Cluster->__get("cluster_name"),
              			"ClusterManagerId" => $Last_Cluster->__get("cluster_manager_id"),
              			"StateId" => $Last_Cluster->__get("state_id"),
              	);
              	$newCluster_arr=array(
              			
              			"ClusterName" => $cluster->__get("cluster_name"),
              			"ClusterManagerId" => $cluster->__get("cluster_manager_id"),
              			"StateId" => $cluster->__get("state_id"),
              	);
       
              	$lastCluster_diff=array_diff($lastCluster_arr,$newCluster_arr);
              	$newCluster_diff=array_diff($newCluster_arr,$lastCluster_arr);
              	
              	 $change_data="";
              	 foreach ($lastCluster_diff as $key => $value)
              	 {
              	 	$change_data.=$key." ".$lastCluster_diff[$key]." change to ".$newCluster_diff[$key]." ";
              	 }
              	 $this->_logger->info("ClusterID ".$cluster_id." has been updated where ".$change_data." by ". $this->_userName.".");
                
          
                           
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function getclusterByStateIdAction() {
         
         	try {
         		$request = $this->getRequest();
         		$state_id = $request->getParam("id");
         		$clustermapper=new Application_Model_ClustersMapper();
              $cluster=new Application_Model_Clusters();
         
         		if ($clusters = $clustermapper->getClusterByStateId($state_id)) {
         
         			foreach ($clusters as $cluster) {
         
         				$data = array(
         						 "cluster_id" => $cluster->__get("cluster_id"),
            					"cluster_name" => $cluster->__get("cluster_name"),
                				"cluster_manager_id" => $cluster->__get("cluster_manager_id"),
         						"state_id" => $cluster->__get("state_id"),
               					
         				);
         
         				$cluster_arr[] = $data;
         			}
         
         
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $cluster_arr,
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while getting clusters"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	} catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
           
}    