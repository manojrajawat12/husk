<?php
class Api_SmsGatewayController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function addSmsGatewayAction(){
	
		try {
			$request=$this->getRequest();
			$user_name=$request->getParam("user_name");
			$password=$request->getParam("password");
			
			$smsGatewayMapper=new Application_Model_SmsGatewayMapper();
			$smsGateway=new Application_Model_SmsGateway();
			$smsGateway->__set("user_name",$user_name);
			$smsGateway->__set("password",$password);
			
			$sms=$smsGatewayMapper->getAllSmsGateway();
			
			if(count($sms)>0){
				$gateway_id=$sms[0]->__get("gateway_id");
				$smsGateway->__set("gateway_id",$gateway_id);
				$update=$smsGatewayMapper->updateSmsGateway($smsGateway);
			}else{
			
	
			$gateway_id=$smsGatewayMapper->addNewSmsGateway($smsGateway);
			} 
				$this->_logger->info("New SmsGateway ID ".$gateway_id." has been created in SmsGateway by ". $this->_userName.".");
				
				$data=array(
						"gateway_id" => $gateway_id,
						"user_name" => $user_name,
				);
	
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllSmsGatewayAction(){
	
		try {
			$smsGatewayMapper=new Application_Model_SmsGatewayMapper();
			$smsGateways=$smsGatewayMapper->getAllSmsGateway();
	
			if(count($smsGateways) >0){
				foreach ($smsGateways as $smsGateway) {
					 
					$data=array(
							"gateway_id" => $smsGateway->__get("gateway_id"),
							"user_name" => $smsGateway->__get("user_name"),
							
					);
	
					$smsGateway_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $smsGateway_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteSmsGatewayByIdAction(){
	
		try {
			$request=$this->getRequest();
			$gateway_id=$request->getParam("id");
			$gatewayMapper=new Application_Model_SmsGatewayMapper();
			if($gateway=$gatewayMapper->deleteSmsGatewayById($gateway_id)){
				$this->_logger->info("Scheme Id ".$gateway_id." has been deleted from Schemes by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateSmsGatewayByIdAction(){
	
		try {
			$request=$this->getRequest();
			$gateway_id=$request->getParam("id");
			$user_name=$request->getParam("user_name");

			 
			$smsGatewayMapper=new Application_Model_SmsGatewayMapper();
			$gatewayData= new  Application_Model_SmsGateway();
			
			$gatewayData->__set("gateway_id",$gateway_id);
			$gatewayData->__set("user_name",$user_name);
			
	
			if($smsGatewayMapper->updateSmsGateway($gatewayData)){
	
				$this->_logger->info("Sms Gateway Id ".$gateway_id." has been updated by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
	public function addApkFileAction(){
	
		try {
			$request=$this->getRequest();
			$apk_image=$request->getParam("apk_image");
			 	
			$ApkFileMapper=new Application_Model_ApkFileMapper();
			$ApkFile=new Application_Model_ApkFile();
			$ApkFile->__set("apk_name",$apk_image);
			 
				
			$sms=$ApkFileMapper->getAllApkFile();
				
			if(count($sms)>0){
				$gateway_id=$sms[0]->__get("apk_id");
				$ApkFile->__set("apk_id",$gateway_id);
				$update=$ApkFileMapper->updateApkFile($ApkFile);
			}else{
					
	
				$gateway_id=$ApkFileMapper->addNewApkFile($ApkFile);
			}
			 
			$data=array(
					"apk_id" => $gateway_id,
					"apk_image" => $apk_image,
			);
	
	
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" => $data
			);
				
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
}