<?php

class Api_ConsumerActivityController extends Zend_Controller_Action { 

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userId=$auth->getIdentity()->user_id;
        $this->_userName=$auth->getIdentity()->user_fname;
        
    }
    
    public function addConsumerActivityAction(){
    	 
    	try {
    		$request=$this->getRequest();
    		$consumer_id=$request->getParam("consumer_id");
    		$site_id=$request->getParam("site_id");
    		$curr_package_id=$request->getParam("current_package_id");
    		$new_package_id=$request->getParam("new_package_id");
    		$activity=$request->getParam("activity");
    		$instant=$request->getParam("instant");
    		$date=$request->getParam("date");
    		$packages_id=$request->getParam("package_id"); 
    		$act_amount=$request->getParam("act_amount");
			$remark=$request->getParam("remark");
    		$date_val = new Zend_Date();
    		$date_val->setTimezone("Asia/Calcutta");
    		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
    		
    		if($date!=NULL && $date!="" && $date!='undefined'){
    			$zendDate = new Zend_Date($date,"dd-MM-yyyy");
    			$effective_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    		}else{
    			$effective_date =$date_val->toString("yyyy-MM-dd");
    		}
    		
    		
    		$changePackageMapper=new Application_Model_ConsumerActivityMapper();
    		$changePackage=new Application_Model_ConsumerActivity();
    		
    		$changePackage->__set("consumer_id", $consumer_id);
    		$changePackage->__set("site_id", $site_id);
    		$changePackage->__set("curr_package_id", $packages_id);
    		$changePackage->__set("new_package_id", $packages_id);
    		$changePackage->__set("activity", $activity);
    		$changePackage->__set("instant", $instant);
    		$changePackage->__set("effective_date", $effective_date);
    		$changePackage->__set("user_id", $this->_userId);
    		$changePackage->__set("user_type", "(W)");
    		$changePackage->__set("timestamp", $timestamp);
    		$changePackage->__set("opening_reading",0);
    		$changePackage->__set("closing_reading",$act_amount);
			$changePackage->__set("remark",$remark);
			
    		$changePackages=$changePackageMapper->addConsumerActivity($changePackage);
    		if($changePackages){
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	public function deleteConsumerActivityIdAction() {
    	try {
    		$request = $this->getRequest();
    		$cp_id = $request->getParam("id");
			$remark = $request->getParam("remark");
    		$CPMapper = new Application_Model_ConsumerActivityMapper();
			 
			$date_val = new Zend_Date();
    		$date_val->setTimezone("Asia/Calcutta");
    		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
    		if ($CPs = $CPMapper->updateConsumerActivityById($cp_id,'disable',$remark,$timestamp,$this->_userId)) {
    			$this->_logger->info("Consumer Activity Id ".$cp_id." has been deleted from Consumer Activity table by ". $this->_userName.".");
    
    			if (!$CPs) {
    				throw new Exception("No agents found");
    			}
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	public function deleteConsumerActivityStatusByIdAction() {
    	try {
    		$request = $this->getRequest();
    		$cp_id = $request->getParam("id");
			$remark = $request->getParam("remark");
    		$CPMapper = new Application_Model_ConsumerActivityMapper();
			 
			$date_val = new Zend_Date();
    		$date_val->setTimezone("Asia/Calcutta");
    		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
    		if ($CPs = $CPMapper->updateConsumerActivityById($cp_id,'disable',$remark,$timestamp,$this->_userId)) {
    			$this->_logger->info("Consumer Activity Id ".$cp_id." has been deleted from Consumer Activity table by ". $this->_userName.".");
    
    			if (!$CPs) {
    				throw new Exception("No agents found");
    			}
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function getAllConsumerActivityAction() {
    	try {
    		$request=$this->getRequest();
    		$status=$request->getParam("status");
    		if($status==NULL || $status=="" || $status=='undefined' ){
    			$status=NULL;
    		}
			$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$packageChangeMapper = new Application_Model_ConsumerActivityMapper();
			$meterReadingMapper=new Application_Model_MeterReadingsMapper();
			
    		$packageChange = $packageChangeMapper->getAllConsumerActivity($status,$role_sites_id);
    		if (!$packageChange) {
    			throw new Exception("No Package change found");
    		}
			
    		$cp_arr=array();
    		 
    		foreach ($packageChange as $packageChanges) {
    			$packages=array();
				$package_name=array(); $is_postpaid=0;
    			$usermapper = new Application_Model_UsersMapper();
				if($packageChanges->__get("user_type")=='(W)'){
					$users = $usermapper->getUserById($packageChanges->__get("user_id"));
					$userName=$users->__get("user_fname") . " " . $users->__get("user_lname");
    			}else{
					$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($packageChanges->__get("user_id"));
					$userName=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
				}
    			$consumerMapper=new Application_Model_ConsumersMapper();
    			$consumers=$consumerMapper->getConsumerById($packageChanges->__get("consumer_id"));
    			
    			$siteMapper=new Application_Model_SitesMapper();
    			$sites=$siteMapper->getSiteById($packageChanges->__get("site_id"));
    			
    			$assign_package_id="";$assign_package_name="";$assign_package_cost="";
				
    			$zendDate = new Zend_Date($packageChanges->__get("effective_date"),"yyyy-MM-dd");
    			$timestamp_edit = $zendDate->toString("dd-MMM-yyyy");
				
				$zendDates = new Zend_Date($packageChanges->__get("timestamp"),"yyyy-MM-dd HH:mm:ss");
    			$timestamp = $zendDates->toString("dd-MMM-yyyy HH:mm:ss");
				
    			$packageMapper=new Application_Model_PackagesMapper();
    			if($packageChanges->__get("activity")=="reactive"){
    				$activity_type="Reactive";
    				$assign_package=$packageChanges->__get("new_package_id");
					$package_arr=explode(",",$assign_package);
					if(count($package_arr)>0){
						for($i=0;$i<count($package_arr);$i++){
							$meter_reading=0;$last_meter_reading=0;$meter_no=0;
							$package=$packageMapper->getPackageById($package_arr[$i]);
							$meterReading=$meterReadingMapper->getReadingByConPackID($packageChanges->__get("consumer_id"),$packageChanges->__get("timestamp"),$package->__get("package_id"));
							if($meterReading){
								$meter_reading=$meterReading["meter_reading"];
								$meter_no=$meterReading["meter_no"];
								$lastmeterReading=$meterReadingMapper->getReadingByConPackID($packageChanges->__get("consumer_id"),date('Y-m-d', strtotime($meterReading["timestamp"]."-1 day")),$package->__get("package_id"));
								if($lastmeterReading){
									$last_meter_reading=$lastmeterReading["meter_reading"];
								}
							}
							$data=array(
								"package_id"=>$package->__get("package_id"),
								"package_name"=>$package->__get("package_name"),
								"package_cost"=>$package->__get("package_cost"),
								"is_postpaid"=>$package->__get("is_postpaid"),
								"meter_reading"=>floatval($meter_reading),
								"last_reading"=>floatval($last_meter_reading),
								"activity"=>$packageChanges->__get("activity"),
								"meter_no"=>$meter_no, 
							);
							$packages[]=$data;
							$package_name[]=$package->__get("package_name");
							if($package->__get("is_postpaid")!=0){
								$is_postpaid=$package->__get("is_postpaid"); 
							}
						}
					}
    				/*if($assign_package){
    					$assign_package_id=$packageChanges->__get("new_package_id");
    					$assign_package_name=$assign_package->__get("package_name");
    					$assign_package_cost=$assign_package->__get("package_cost");
						$is_postpaid=$assign_package->__get("is_postpaid");
    				}*/
    			}else{
					$activity_type="Banned";
					
					$con_packageMapper=new Application_Model_ConsumersPackageMapper();
                    $AllPackages=$con_packageMapper->getPackageByConsumerId($packageChanges->__get("consumer_id"));
                    if ($AllPackages) {
                    	foreach ($AllPackages as $AllPackage){
                    	    $package_cost = $AllPackage["package_cost"];
							$meterReading=$meterReadingMapper->getReadingByConPackID($packageChanges->__get("consumer_id"),$packageChanges->__get("timestamp"),$AllPackage["package_id"]);
							$meter_reading=0; $last_meter_reading=0;$meter_no=0;
							if($meterReading){
								$meter_reading=$meterReading["meter_reading"];
								$meter_no=$meterReading["meter_no"];
								$lastmeterReading=$meterReadingMapper->getReadingByConPackID($packageChanges->__get("consumer_id"),date('Y-m-d', strtotime($meterReading["timestamp"]."-1 day")),$AllPackage["package_id"]);
								if($lastmeterReading){
									$last_meter_reading=$lastmeterReading["meter_reading"];
								}
							} 
							
							$data=array(
								"package_id"=>$AllPackage["package_id"],
								"package_name"=>$AllPackage["package_name"],
								"package_cost"=>$AllPackage["package_cost"],
								"is_postpaid"=>$AllPackage["is_postpaid"],
								"meter_reading"=>floatval($meter_reading),
								"last_reading"=>floatval($last_meter_reading),
								"meter_no"=>($meter_no=='null' || $meter_no=='undefined' || $meter_no==NULL)?0:$meter_no,
								"activity"=>$packageChanges->__get("activity"),
							);
							$packages[]=$data;
							$package_name[]=$AllPackage["package_name"];
							if($AllPackage["is_postpaid"]!=0){
								$is_postpaid=$AllPackage["is_postpaid"];
							}
						}
					}
				}
    			
    			
    			
    			$data = array(
    					"id"=>$packageChanges->__get("id"),
    					"consumer_id" => $packageChanges->__get("consumer_id"),
    					"consumer_name" => $consumers->__get("consumer_name")." (".$consumers->__get("consumer_connection_id").")",
						"consumer_status" => $consumers->__get("consumer_status"),
    					"site_id" => $consumers->__get("site_id"),
    					"site_name" =>$sites->__get("site_name"),
    					"assign_package_name" => implode(",",$package_name),
    					"effective_date" => $timestamp_edit,
    					"user_name" => $userName,
    					"activity"=>$packageChanges->__get("activity"),
    					"instant"=>$packageChanges->__get("instant"),
						"reactivation"=>intval($packageChanges->__get("closing_reading")),
    					"activity_type"=>$activity_type,
						"is_postpaid"=>$is_postpaid, 
						"packages"=>$packages,
						"timestamp"=>$timestamp
    					 
    			 );
    			$cp_arr[] = $data;
    		}  
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$arr = array(
    				"meta" => $meta,
    				"data" => $cp_arr,
    		);
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function updateAgentByIdAction() {
    	try {
    		$request = $this->getRequest();
    		$agent_id = $request->getParam("id");
    		$agent_mobile = $request->getParam("mobile");
    		$agent_fname = $request->getParam("fname");
    		$agent_lname = $request->getParam("lname");
    		$username = $request->getParam("username");
    		$agent_status = $request->getParam("status");
    		$agent_balance = $request->getParam("balance");
    		$agent_email = $request->getParam("email");
    		$site_id = $request->getParam("site_id");
    		$password = $request->getParam("password");
    		$permission_menu = $request->getParam("permission_menu");
    		$permission_submenu = $request->getParam("permission_submenu");
    		$permission=$permission_menu. ',' .$permission_submenu;
    			
    		$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    		$agents = new Application_Model_CollectionAgents();
    		$lastAgents = $collectionAgentsMapper->getCollectionAgentById($agent_id);
    		$agents->__set("collection_agent_id", $agent_id);
    		$agents->__set("agent_mobile", $agent_mobile);
    		$agents->__set("agent_fname", $agent_fname);
    		$agents->__set("agent_lname", $agent_lname);
    		$agents->__set("username", $username);
    		$agents->__set("agent_status", $agent_status);
    		$agents->__set("agent_balance", $agent_balance);
    		$agents->__set("agent_email", $agent_email);
    		$agents->__set("site_id", $site_id);
    		if($password!='undefined'){
    			$agents->__set("password", sha1(strtoupper($password)));
    		}else{
    			$agents->__set("password", NULL);
    		}
    		$agents->__set("permission", $permission);
    			
    		if ($collectionAgents = $collectionAgentsMapper->updateCollectionAgent($agents)) {
    			 
    			$lastAgent_data = array(
    					"collection_agent_id" => $lastAgents->__get("collection_agent_id"),
    					"agent_fname" => $lastAgents->__get("agent_fname"),
    					"agent_lname" => $lastAgents->__get("agent_lname"),
    					"agent_mobile" => $lastAgents->__get("agent_mobile"),
    					"agent_status" => $lastAgents->__get("agent_status"),
    					"agent_balance" => $lastAgents->__get("agent_balance"),
    					"agent_email" => $lastAgents->__get("agent_email"),
    					"site_id" => $lastAgents->__get("site_id"),
    					"permission" => $lastAgents->__get("permission"),
    			);
    			$newAgent_data = array(
    					"collection_agent_id" => $agents->__get("collection_agent_id"),
    					"agent_fname" => $agents->__get("agent_fname"),
    					"agent_lname" => $agents->__get("agent_lname"),
    					"agent_mobile" => $agents->__get("agent_mobile"),
    					"agent_status" => $agents->__get("agent_status"),
    					"agent_balance" => $agents->__get("agent_balance"),
    					"agent_email" => $agents->__get("agent_email"),
    					"site_id" => $agents->__get("site_id"),
    					"permission" => $agents->__get("permission"),
    			);
    			$lastAgent_diff=array_diff($lastAgent_data,$newAgent_data);
    			$newAgent_diff=array_diff($newAgent_data,$lastAgent_data);
    
    			$change_data="";
    			foreach ($lastAgent_diff as $key => $value)
    			{
    				$change_data.=$key." ".$lastAgent_diff[$key]." change to ".$newAgent_diff[$key]." ";
    			}
    
    			$this->_logger->info("Collection Agents Id ".$agent_id." has been updated where ".$change_data." by ". $this->_userName.".");
    
    			if (!$collectionAgents) {
    				throw new Exception("No agents found");
    			}
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }    
    
	public function updatePackageStatusByIdAction(){
    	 
    	try {
    		$request=$this->getRequest();
    		$cp_id=$request->getParam("id");
    		$status=$request->getParam("status");
    		$remark=$request->getParam("remark");
    
    		$zendDate = new Zend_Date();
    		$zendDate->setTimezone("Asia/Calcutta");
    		$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    
    		$packageChangemapper=new Application_Model_PackageChangeMapper();
    		$packageMapper=new Application_Model_PackagesMapper();
    		$packageHistorymapper = new Application_Model_PackageHistoryMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    		$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
    		$adddeviceMapper=new Application_Model_AddDeviceMapper();
    		$electricityMapper=new Application_Model_ElectricityInfoMapper();
    		
    		if($status=='disable'){
    			$package=$packageChangemapper->updatePackageChangeById($cp_id,$status,$remark,$timestamp,$this->_userId);
    		}else{
    			$All_Id=explode(",", $cp_id);
    			for($i=0;$i<count($All_Id);$i++){
    				$packageChange=$packageChangemapper->getPackageChangeById($All_Id[$i]);
    				if($packageChange){
    					$package=$packageChangemapper->updatePackageChangeById($All_Id[$i],$status,$remark,$timestamp,$this->_userId);
    					
    					if ($packageChange->__get("activity")=='new' && $packageChange->__get("instant")=='Yes'){
    						 
    						$package_id=$packageChange->__get("assign_package");
    						$consumer_id=$packageChange->__get("consumer_id");
    						$packages=$packageMapper->getPackageById($package_id);
    						
    						$consumerPackage= new Application_Model_ConsumersPackage();
    						$consumerPackage->__set("consumer_id", $consumer_id);
    						$consumerPackage->__set("package_id", $package_id);
    						$CP=$consumerPackageMapper->addNewConsumersPackage($consumerPackage);
    						if($CP){
    							$sendMsg=$this->SendMsgToDevice($consumer_id);
    							
    							if($packages->__get("is_postpaid")!=0){
    								$reading=$packageChange->__get("opening_reading");
	    							$meterReading = new Application_Model_MeterReadings();
	    							 
	    							$meterReading->__set("consumer_id",$consumer_id);
	    							$meterReading->__set("meter_reading",$reading);
	    							$meterReading->__set("timestamp",$timestamp);
	    							$meterReading->__set("package_id",$package_id);
	    							$meterReading->__set("start_reading",1);
									$meterReading->__set("entry_by",$this->_userId); 
									$meterReading->__set("entry_type",'W');  
	    							$meterReadingMapper->addNewMeterReading($meterReading);
    							}
    							$package_cost = $packages->__get("package_cost");
    							$user_id=$this->_userId;
    							$status="Request Received";
    							$packageHis= new Application_Model_PackageHistory();
    							$packageHis->__set("package_id",$package_id);
    							$packageHis->__set("consumer_id",$consumer_id);
    							$packageHis->__set("package_cost",$package_cost);
    							$packageHis->__set("change_by",$user_id);
    							$packageHis->__set("status",$status);
    							$packageHis->__set("last_package",NULL);
    							$packageHis->__set("change_description","New Package added");
    							
    							$packageHistory= $packageHistorymapper->addNewPackageHistory($packageHis);
    						}
    					}elseif ($packageChange->__get("activity")=='change' && $packageChange->__get("instant")=='Yes'){
    						 
    						$current_package=$packageChange->__get("curr_package_id");
    						$new_package=$packageChange->__get("new_package_id");
    						$consumer_id=$packageChange->__get("consumer_id");
    						
    						$consumerPackage=$consumerPackageMapper->updatePackageId($consumer_id, $new_package,$current_package);
    						if($consumerPackage){
    							$sendMsg=$this->SendMsgToDevice($consumer_id);
    							
    							$curr_packages=$packageMapper->getPackageById($current_package);
    							$new_packages=$packageMapper->getPackageById($new_package);
    							
    							// Change package code
    								$new_package_cost=$new_packages->__get("package_cost");
    								$is_postpaid=$new_packages->__get("is_postpaid");
    								
    								$current_date = date_parse_from_format("Y-m-d", $timestamp);
    								$day = $current_date["day"];
    								$month = sprintf("%02d", $current_date["month"]);
    								$year = $current_date["year"];
    								$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
    								 
    								$consumers=$consumerMapper->getConsumerById($consumer_id);
    								$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
    								$Act_day = $Act_date["day"];
    								$Act_month = sprintf("%02d", $Act_date["month"]);
    								$Act_year = $Act_date["year"];
    								 
    								$lastDebit=$cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$current_package);
    									
    								if($month==$Act_month && $year==$Act_year){
    									$lastAmount=($lastDebit / $total_days_in_month)*($day-$Act_day);
    								}else{
    									$lastAmount=($lastDebit / $total_days_in_month) * $day;
    								}
    									
    								if($is_postpaid==0){
    									$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
    									$package_val=$newAmount-($lastDebit-$lastAmount);
    								}else {
    									$unit=$curr_packages->__get("unit");
    									$extra_charges=$curr_packages->__get("extra_charges");
    							
    									$usedUnit=($unit/$total_days_in_month)*($total_days_in_month-$day);
    									$readings=$reading-$lastreading;
    									if($readings>$usedUnit){
    										$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
    										$unit_amt=$newAmount+(($readings-$usedUnit)*$extra_charges);
    										$package_val=($unit_amt+$lastAmount)-$lastDebit;
    									}else{
    										$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
    										$package_val=($newAmount+$lastAmount)-$lastDebit;
    									}
    							
    								}
    							
    								if($curr_packages->__get("is_postpaid")!=0){
    								
    									$reading=$packageChange->__get("closing_reading");
    									$reading_last=0;
    									$lastunit=$meterReadingMapper->checkMonthMeterReadingsByConsumerId($consumer_id);
    									if($lastunit){
    										$reading_last=$lastunit["reading"];
    									}
    								
    									$unit=$curr_packages->__get("unit");
    									$extra_charges=$curr_packages->__get("extra_charges");
    										
    									$usedUnit=($unit/$total_days_in_month)*($total_days_in_month-$day);
    									$readings=$reading-$reading_last;
    									if($readings>$usedUnit){
    										$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
    										$unit_amt=$newAmount+(($readings-$usedUnit)*$extra_charges);
    										$package_val=($unit_amt+$lastAmount)-$lastDebit;
    									}else{
    										$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
    										$package_val=($newAmount+$lastAmount)-$lastDebit;
    									}
    								
    									$meterReading = new Application_Model_MeterReadings();
    										
    									$meterReading->__set("consumer_id",$consumer_id);
    									$meterReading->__set("meter_reading",$reading);
    									$meterReading->__set("timestamp",$timestamp);
    									$meterReading->__set("package_id",$current_package);
    									$meterReading->__set("start_reading",0);
										$meterReading->__set("entry_by",$this->_userId); 
										$meterReading->__set("entry_type",'W');  
    									$meterReadingMapper->addNewMeterReading($meterReading);
    								
    								}
    								
    								if($new_packages->__get("is_postpaid")!=0){
    										
    									$reading=$packageChange->__get("opening_reading");
    									$meterReading = new Application_Model_MeterReadings();
    										
    									$meterReading->__set("consumer_id",$consumer_id);
    									$meterReading->__set("meter_reading",$reading);
    									$meterReading->__set("timestamp",$timestamp);
    									$meterReading->__set("package_id",$new_package);
    									$meterReading->__set("start_reading",1);
										$meterReading->__set("entry_by",$this->_userId); 
										$meterReading->__set("entry_type",'W');  
    									$meterReadingMapper->addNewMeterReading($meterReading);
    								}
    								
    								$timestamps = $zendDate->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
    								$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
    								$transaction_id = $timestamps . "-" . $cr_amount;
    								 
    								$cashRegister = new Application_Model_CashRegister();
    								$cashRegister->__set("user_id", $this->_userId);
    								$cashRegister->__set("consumer_id", $consumer_id);
    								$cashRegister->__set("cr_entry_type", "DEBIT");
    								$cashRegister->__set("cr_amount", intval($package_val));
    								$cashRegister->__set("receipt_number", 0);
    								$cashRegister->__set("transaction_id", $transaction_id);
    								$cashRegister->__set("transaction_type", "(W)");
    								$cashRegister->__set("timestamp", $timestamp);
    								$cashRegister->__set("entry_status", "CHG");
    								$cashRegister->__set("remark", "Package upgraded from ".$curr_packages->__get("package_name")."(Rs.".$curr_packages->__get("package_cost").") to " .$new_packages->__get("package_name")."(Rs.".$new_packages->__get("package_cost").")");
    								$cashRegister->__set("package_id", $new_package);
    								$cashRegisterMapper->addNewCashRegister($cashRegister);
    								$status="Package Updated";
    								
    							
    							 
	    							$packageMapper= new Application_Model_PackagesMapper();
	    							$packages=$packageMapper->getPackageById($new_package);
	    							$package_cost = $packages->__get("package_cost");
	    							$user_id=$this->_userId;
	    							
	    							$package= new Application_Model_PackageHistory();
	    							$package->__set("package_id",$new_package);
	    							$package->__set("consumer_id",$consumer_id);
	    							$package->__set("package_cost",$package_cost);
	    							$package->__set("change_by",$user_id);
	    							$package->__set("status",$status);
	    							$package->__set("last_package",$current_package);
	    							 
	    							$packageHistorymapper = new Application_Model_PackageHistoryMapper();
	    							$packageHistory= $packageHistorymapper->addNewPackageHistory($package);
    							 
    							
    							// End of change package code
    							
    						}
    					}
    				}
    				
    				
    			}
    			
    		} 
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		 
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	public function updateConsumerActivityStatusByIdAction(){
    	 
    	try {
			$request=$this->getRequest();
    		$ca_id=$request->getParam("id");
    		$package_id=$request->getParam("package_id");
    		$last_reading=$request->getParam("last_reading");
			$meter_reading=$request->getParam("meter_reading");
			$meter_no=$request->getParam("meter_no");
			$consumer_id=$request->getParam("consumer_id");
			$remark=$request->getParam("remark");
			$activity=$request->getParam("activity");
			$released=$request->getParam("released");
			$usable=$request->getParam("usable");
			$unit=$meter_reading-$last_reading;
			
    		$zendDate = new Zend_Date();
    		$zendDate->setTimezone("Asia/Calcutta");
    		$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    
    		$packageChangemapper=new Application_Model_PackageChangeMapper();
    		$packageMapper=new Application_Model_PackagesMapper();
    		$packageHistorymapper = new Application_Model_PackageHistoryMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    		$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
    		  
				$date = new Zend_Date();
	    		$date->setTimezone("Asia/Kolkata");
	    		$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
	    		$current_date = date_parse_from_format("Y-m-d", $curr_date);
	    		$day = $current_date["day"];
	    		$month = sprintf("%02d", $current_date["month"]);
	    		$year = $current_date["year"];
	    		$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
				$discount=0;
				$consumers=$consumerMapper->getConsumerById($consumer_id);
				if($consumers){
					$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
					$Act_day = $Act_date["day"];
					$Act_month = sprintf("%02d", $Act_date["month"]);
					$Act_year = $Act_date["year"];
					if($activity=='banned'){
						$package = $packageMapper->getPackageById($package_id);
						if($package){
							$lastDebit=$cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$package_id);
							
							if($package->__get("is_postpaid")==0){
								if($month==$Act_month && $year==$Act_year){
									$lastAmount=($lastDebit/$total_days_in_month)*($day-$Act_day);
								}else{
									$lastAmount=($lastDebit/$total_days_in_month)*$day;
								}
								$amount=intval($lastDebit-floatval($lastAmount));
								$cr_entry_type='DISCOUNT';$entry_status='CHG';
							}elseif($package->__get("is_postpaid")==1){
								$amount=round($unit*$consumers->__get("micro_entrprise_price"));
								$cr_entry_type='DEBIT';$entry_status='CHG';
							}else{
								$extra_charges=$package->__get("extra_charges");
								$package_cost=$package->__get("package_cost");
								
								$lastDebit=$package_cost;
								
								if($month==$Act_month && $year==$Act_year){
									$lastAmount=($lastDebit/$total_days_in_month)*($day-$Act_day);
									$min_unit=intval(($package->__get("unit")/$total_days_in_month)*($day-$Act_day));
								}else{
									$lastAmount=($lastDebit/$total_days_in_month)*$day;
									$min_unit=intval(($package->__get("unit")/$total_days_in_month)*$day);
								}
								$unit=floatval($meterReadingMapper->getTotalUnitByConsumerId($consumer_id,$package_id,$month,$year));
								
								if($unit<=$min_unit){
									$amount=intval($lastAmount);
								}else{
									$calUnitAmount=($unit-$min_unit)*$extra_charges;
									$amount=intval($lastAmount+$calUnitAmount);
								} 
								$cr_entry_type='DEBIT';$entry_status='CHG';
							}
							
							$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
							$cr_amount = str_pad(intval($amount), 4, "0", STR_PAD_LEFT);
							$transaction_id = $timestamp . "-" . $cr_amount;
							
							$cashRegister = new Application_Model_CashRegister();
							$cashRegister->__set("user_id", $this->_userId);
							$cashRegister->__set("consumer_id", $consumer_id);
							$cashRegister->__set("cr_entry_type", $cr_entry_type);
							$cashRegister->__set("cr_amount", intval($amount));
							$cashRegister->__set("transaction_id", $transaction_id);
							$cashRegister->__set("transaction_type", "(W)");
							$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
							$cashRegister->__set("entry_status", $entry_status);
							$cashRegister->__set("remark", $remark);
							$cashRegister->__set("package_id", $package_id);
							if(intval($amount)>0){
								$cr=$cashRegisterMapper->addNewCashRegister($cashRegister);
							}
						}
							$MeterInventoryMapper = new Application_Model_MeterInventoryMapper(); 
							if($meter_no!=NULL && $meter_no!='NULL' && $meter_no!="" && $meter_no!='undefined'){
								$check=$updateInventory=$MeterInventoryMapper->getDetailByMeterNo($meter_no);
									if($check){
										if($released==0){
												$updateInventory=$MeterInventoryMapper->updateMeterStatus($meter_no,$released,NULL,$consumer_id);
												$this->_logger->info("New meter number ".$meter_no." has been closed but not Released by ". $this->_userName.".");
											}else{
												$updateInventory=$MeterInventoryMapper->updateMeterStatus($meter_no,$usable,NULL,$consumer_id);
												$this->_logger->info("New meter number ".$meter_no." has been Released but ".$usable." by ". $this->_userName.".");
											}
										}
							}
					}else{
						$MeterInventoryMapper = new Application_Model_MeterInventoryMapper(); 
							if($meter_no!=NULL && $meter_no!='NULL' && $meter_no!="" && $meter_no!='undefined'){
								$check=$updateInventory=$MeterInventoryMapper->getDetailByMeterNo($meter_no);
								if($check){
									$updateInventory=$MeterInventoryMapper->updateMeterStatus($meter_no,1,NULL,$consumer_id);
								}			 
							}
					}
				}
				
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		 
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	public function updateActivityByIdAction(){
    	 
    	try {
			$request=$this->getRequest();
    		$ca_id=$request->getParam("id");
			$consumer_id=$request->getParam("consumer_id");
			$remark=$request->getParam("remark");
			$activity=$request->getParam("activity");
			$reactivation=intval($request->getParam("reactivation"));
			$package_id=$request->getParam("package_id");
			
			 
    		$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp_date = $date->toString("yyyy-MM-dd HH:mm:ss");
    
    		$packageChangemapper=new Application_Model_PackageChangeMapper();
    		$packageMapper=new Application_Model_PackagesMapper();
    		$packageHistorymapper = new Application_Model_PackageHistoryMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    		$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
			
						$consumers=$consumerMapper->getConsumerById($consumer_id);
						if($consumers){
							 if($reactivation>0){
								$amount=$reactivation;
								$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
								$cr_amount = str_pad(intval($amount), 4, "0", STR_PAD_LEFT);
								$transaction_id = $timestamp . "-" . $cr_amount;
								$cr_entry_type='DEBIT';$entry_status='CHG';
								$cashRegister = new Application_Model_CashRegister();
								$cashRegister->__set("user_id", $this->_userId);
								$cashRegister->__set("consumer_id", $consumer_id);
								$cashRegister->__set("cr_entry_type", 'DEBIT');
								$cashRegister->__set("cr_amount", intval($amount)); 
								$cashRegister->__set("receipt_number", "CA-".$this->_userId);
								$cashRegister->__set("transaction_id", $transaction_id);
								$cashRegister->__set("transaction_type", "(W)");
								$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
								$cashRegister->__set("entry_status", 'ACT');
								$cashRegister->__set("remark", $remark);
								$cashRegister->__set("package_id", 0);
								$cr=$cashRegisterMapper->addNewCashRegister($cashRegister);
							}
							if($activity=='reactive'){
								$sd=abs($cashRegisterMapper->getOtherAmtByConsumerId($consumers->__get("consumer_id"),'SD'));
								$package_costs_value=0;
								$removePack=$consumerPackageMapper->deletePackageByConsumerId($consumer_id);
								if($removePack){
									$consumerPackage=new Application_Model_ConsumersPackage();
									$package_ids=explode(",", $package_id);
									for($j=0;$j<count($package_ids);$j++){
										$consumerPackage->__set("package_id",$package_ids[$j]);
										$consumerPackage->__set("consumer_id",$consumer_id);
										$consumerPackageMapper->addNewConsumersPackage($consumerPackage);
										
										$package_costs=$packageMapper->getPackageById($package_ids[$j]);
										$package_costs_value=$package_costs_value+$package_costs->__get("package_cost");
									}
									if($sd>$package_costs_value){
										$credit_limit=$sd;
									}else{
										$credit_limit=$package_costs_value;
									} 
									
									$this->_logger->info("Consumer status changed to active from banned Consumer ID: ".$consumers->__get("consumer_connection_id")." By ". $this->_userName);
									$consumersUpdate=$consumerMapper->updateStatus($consumers->__get("consumer_connection_id"),'active',$timestamp_date,$remark,$this->_userId,'(W)',$credit_limit);
								
								} 
							}else{
								$this->_logger->info("Consumer status changed to banned from active Consumer ID: ".$consumers->__get("consumer_connection_id")." By ". $this->_userName);
								$consumersUpdate=$consumerMapper->updateStatus($consumers->__get("consumer_connection_id"),'banned',$timestamp_date,$remark,$this->_userId,'(W)',1);
								
							}
							
								$changePackageMapper=new Application_Model_ConsumerActivityMapper();
								$updateActivity=$changePackageMapper->updateConsumerActivityById($ca_id,'enable',$remark,$timestamp_date,$this->_userId); 
								  
								if($consumers->__get("site_id")==62 || $consumers->__get("site_id")==31){
									$meterReadingMapper=new Application_Model_MeterReadingsMapper();
									$meters=$meterReadingMapper->getLatestReadingByConsumerId($consumers->__get("consumer_id"));
									$serial=$meters->__get("meter_no");
									if($consumers->__get("site_id")==62){
										$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
									}else{
										$url="https://sparkcloud-tara-dumarsan.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
									} 
									if($activity=='reactive'){
										$activitys='on';
									}else{
										$activitys='off';
									}
									$param="state=".$activitys.""; 
									if($consumer->__get("site_id")==62){
										$destination=array(
											'Authentication-Token: .eJwFwcsRwCAIBcBeOIcZlSdqLZkc-Ej_JWT3Jal9bVfjdk0ZFZMNGQy3Y64xZlx6aOZCbgkZhewCO7JQ6jCVqN7o-wEZRhSK.DcwoVA.GIS1jju7M0zPSMjcDrGOBOW7Rp8'
										);
									}else{
										$destination=array(
											'Authentication-Token: .eJwNx7kNwDAIAMBdqGOJ13FmiVIAhv1HiK-7F3bKY-w4fEoPRd4j8NRs3tKVUbngAuvgJC9qjS7KjakktoqVvLnh-wH_yhSf.DgLTZg.K0MSUEm8_CSi_aQQde7UGW8Ikx4'
										);
									}
									$result=$this->CallCURL($url,'POST',$param,$destination);
										
									$data=(json_decode($result,true));
								}
						}
						
				$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		 
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
    public function SendMsgToDevice($consumer_id){
    	
    	$adddeviceMapper=new Application_Model_AddDeviceMapper();
    	$electricityMapper=new Application_Model_ElectricityInfoMapper();
    	$consumerPackagesMapper=new Application_Model_ConsumersPackageMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	
    	$electricitys=$adddeviceMapper->getVolByConid($consumer_id);
    	$wattage_total=0;
    	if($electricitys){
    		
    	
    		$devices=$adddeviceMapper->getDataByDeviceid($electricitys["device_name"]);
    		if($devices){
    			
    			$cp_data=$consumerPackagesMapper->getPackageByConsumerId($consumer_id);
    			if($cp_data){
    				foreach ($cp_data as $cp){
    					$watt_id=$cp["wattage"];
    					$watts=$wattageMapper->getWattageBywattId($watt_id);
    					if($watts){
    						$wattage_total=$wattage_total+$watts->__get("wattage");
    					}
    				}
    			}
    			
    			$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer_id);
    			 
    			$zendDate = new Zend_Date();
    			$zendDate->setTimezone("Asia/Calcutta");
    			$date_val = $zendDate->toString("yyyy-MM-dd");
    			$trans_date=date_parse_from_format("Y-m-d", $date_val);
    			$month_data= $trans_date["month"];
    			$year_data= $trans_date["year"];
    			 
    			$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_data, $year_data);
    			if($consumer_scheme){
    				foreach($consumer_scheme as $consumer_schemes){
    					$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
    					$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
    						
    					$discount_month=$consumer_schemes["discount_month"];
    					$feeder_hours=$consumer_schemes["feeder_hours"];
    					$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month"));
    					 
    					if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
    						$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]));
    						$wattage_total=$wattage_total + $dailyUnit;
    					}
    				}
    			}
    			
    			$sim_no=$devices[0]["device_sim"];
    			$device_name=$devices[0]["name"];
    			$time=$devices[0]["time"];
    			$times=sprintf("%04d",$time);
    			$count_device=count($devices);
    			$wattage=sprintf("%04d",$wattage_total);
    			$nodeArray=array();
    			 
    			for ($i=0;$i<$count_device;$i++){
    				if($devices[$i]["node"]==1){
    					if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODEA".$wattage;
    					}else{
    						$nodeArray[]= "NODEA".$devices[$i]["wattage"];
    					}
    					 
    				}else if($devices[$i]["node"]==2){
    				if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODEB".$wattage;
    					}else{
    						$nodeArray[]= "NODEB".$devices[$i]["wattage"];
    					}
    				}else if($devices[$i]["node"]==3){
    				if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODEC".$wattage;
    					}else{
    						$nodeArray[]= "NODEC".$devices[$i]["wattage"];
    					}
    				}else if($devices[$i]["node"]==4){
    				if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODED".$wattage;
    					}else{
    						$nodeArray[]= "NODED".$devices[$i]["wattage"];
    					}
    				}else if($devices[$i]["node"]==5){
    				if($devices[$i]["consumer_id"]==$consumer_id){
    						$nodeArray[]= "NODEE".$wattage;
    					}else{
    						$nodeArray[]= "NODEE".$devices[$i]["wattage"];
    					}
    				}
    			}
    	
    			 $msg="616629;LCDID".$device_name.";".$nodeArray[0].";".$nodeArray[1].";".$nodeArray[2].";".$nodeArray[3].";".$nodeArray[4].";"."TIME".$times.";";
				 return true;
    			//$texts= $this->_smsNotification($sim_no, $msg);
    		}
    	}
    }
    
    protected function _smsNotification($number, $sms) {
    
    	$number = substr($number, 0, 10);
    	$number = '8285859194';
    	$sms = urlencode($sms);
    	$smsMapper =new Application_Model_SmsGatewayMapper();
    	$smsgateway = $smsMapper->getAllSmsGateway();
    	$user_name=$smsgateway[0]->__get("user_name");
    	$password=$smsgateway[0]->__get("password");
    
    	$url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=".$user_name."&auth_scheme=plain&password=".$password."&v=1.1&format=text";
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    	
    	$text = file_get_contents($url);
    	
    	$file = 'people.txt';
    	$current = file_get_contents($file);
    	$current .= "IN(".$number."):".$sms." ---- Out:".$text . " ---- Time - ".$timestamp."\r\n";
    	file_put_contents($file, $current);
    	
    	return $text;
    }
	
	function CallCURL($url, $method = 'GET', $params = [], $destination = []) {
	
		if (count($destination) > 0) {
			foreach ($destination as $item) {
				$headers[] = $item;
			}
		}
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if ($method == 'GET') {
			if (count($params) > 0) {
				$url .= '?' . http_build_query($params);
			}
		} else {
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		}
		curl_setopt($ch, CURLOPT_URL, $url);
	
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

}