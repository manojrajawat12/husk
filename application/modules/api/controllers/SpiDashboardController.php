<?php
require('html2fpdf.php');
require('font/makefont/makefont.php');
ini_set('display_errors',0);
class Api_SpiDashboardController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }
    public function indexAction(){
        
    }
	
    public function getDataWidgetAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id=$request->getParam("site_id");
    		$state_id=$request->getParam("state_id");
    		$sitesMapper = new Application_Model_SitesMapper();
    		$consumersMapper=new Application_Model_ConsumersMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    		$wattageMapper=new Application_Model_WattageMapper();
    		$MMmapper=new Application_Model_SiteMasterMeterMapper();
    		$opexMapper=new Application_Model_OpexMapper();
    		$revenueMapper=new Application_Model_RevenueTargetMapper();
    		$siteMeterReading=new Application_Model_SiteMeterReadingMapper();
    		$mpptMapper=new Application_Model_MpptsMapper();
    		$mpptReadingMapper=new Application_Model_MpptsReadingMapper(); 
    		
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$entry_status = $request->getParam("entry_status");
    		
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    		$month_name = $request->getParam("month");
    		$month = date("m", strtotime($month_name));
    		$year = $request->getParam("year");
    		
    		$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
     		
    		$curr_date=$year."-".$month."-".$total_days_in_month;
    		$last_date=$year."-".$month."-01";
    		$data=array();
    		$sites=$sitesMapper->getSiteByStateId($state_id,$role_sites_id,$site_id);
    		 
    		$consumer_data=array();$revenue_data=array();$unit_per_user_data=array();
    		$opex_data=array();$ebita_data=array();$sold_unit_data=array();$consumption_data=array();
    		$generation_data=array();
    		foreach($sites as $site)
    		{
    			$site_ids=$site->__get("site_id");
    			$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    			
    			$consumers=$consumersMapper->getActiveConsumerBySite($site_ids,null,null,$curr_date,$last_date);
    			$totalEntryType = $cashRegisterMapper->getTotalBilledForEntryType($curr_date,$last_date,$site_ids);
    			$totalRevenue=$totalEntryType["CHG"]+$totalEntryType["SCHEME"]+$totalEntryType["OTHERS"]+$totalEntryType["ACT"];
                 
				 
				 
    			$Avg_revenue_per_user=$totalRevenue/intval($consumers["all_consumer"]);
    			 
    				$consumer_data[] = array(
    							"site_name" => $site->__get("site_name"),
    							"total_consumer" => intval($consumers["all_consumer"]));
    				$revenue_data[] = array(
    						"site_name" => $site->__get("site_name"),
    						"revenue" => intval($Avg_revenue_per_user),
    						"revenue_total" => intval($totalRevenue));
    				
    			$consumers_details=$consumersMapper->getTotalConsumerBySiteIdAndMonthVal($month,$year,$site_ids);	
    			$total_unit=0;
    			
    			
    			if ($consumers_details){
    				
    				foreach ($consumers_details as $consumer_detail){
    					
    						if($consumer_detail["is_postpaid"]==0){
    							
    							$feedarHrs=0;
    							$site_meter_ids=explode(",", $consumer_detail['site_meter_id']);
    							 
    							if(count($site_meter_ids)>0){
    								for ($k=0;$k<count($site_meter_ids);$k++){
    									$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
    									if($feedarDetail){
    										if($feedarDetail->__get("is_24_hr")==1){
    											$feedarHr=24;
    										}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
    											$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
    										}else{
    											$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
    										}
    										$feedarHrs=$feedarHrs+$feedarHr;
    									}
    								}
    							}
    							 
    							$watt_total=$wattageMapper->getWattageBywattId($consumer_detail["wattage"]);
    							if($watt_total){
    								$wattage=$watt_total->__get("wattage");
    							}else{
    								$wattage=0;
    							}
    							
    							$MonthlyUnit_pre=($wattage*$feedarHrs*$total_days_in_month)/1000;
    						    $total_unit=$total_unit+$MonthlyUnit_pre;
    						}else{
    							 
	    						$meterReading=$meterReadingMapper->getTotalUnitSiteWise($consumer_detail["consumer_id"],$month,$year);
	    						if($meterReading){
	    							if(floatval($meterReading["curr_reading"])>0){
	    								$totalMtrUnit=(floatval($meterReading["curr_reading"])-floatval($meterReading["last_reading"]));
	    								$monthly_Unit_post=$totalMtrUnit;
	    							}else{
	    								$monthly_Unit_post=0;
	    							}
	    						}else{
	    							$monthly_Unit_post=0;
	    						}
	    							
	    						$total_unit=$total_unit+ $monthly_Unit_post;
	    					}
    					}
    						
    					$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer_detail['consumer_id']);
    					$month_scheme_pre_total=0;
    					if($consumer_scheme){
    						foreach($consumer_scheme as $consumer_schemes){
    							$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
    							$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
    				
    							$discount_month=$consumer_schemes["discount_month"];
    							$feeder_hours=$consumer_schemes["feeder_hours"];
    							$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
    							$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month"));
    				
    							if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
    								$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]*$feeder_hours*$total_days_in_month)/1000);
    								$total_unit=$total_unit + $dailyUnit;
    							}
    						}
    					}
    			}
    			
    			
    			$avg_unit_per_unit=round($total_unit/intval($consumers["all_consumer"]));
    			
    			$unit_per_user_data[] = array(
    					"site_name" => $site->__get("site_name"),
    					"avg_unit" => $avg_unit_per_unit);
    			$totalTarget=0;
    			$opexs=$opexMapper->getOpexData(1);
    			if($opexs){
    				foreach ($opexs as $opex){
    					$dateObj   = DateTime::createFromFormat('!m', $month);
    					$monthName_opex = $dateObj->format('F');
    					$targets=$revenueMapper->getAllRevenueSiteId($site_ids, $monthName_opex, $year, $opex["id"]);
    					if($targets){
    						foreach ($targets as $target){
    							$totalTarget=$totalTarget+$target["target"];
    						}
    					}
    				}
    			}	
    			$opex_data[] = array(
    					"site_name" => $site->__get("site_name"),
    					"opex_data" => $totalTarget);
    			
    			$ebita=round(($totalRevenue-$totalTarget)/$totalRevenue);
    			
    			$ebita_data[] = array(
    					"site_name" => $site->__get("site_name"),
    					"ebita_data" => $ebita);
    			
    			$meterMasters=$MMmapper->getSiteMasterById($site_ids);
    			$totalConsumption=0;
    			if($meterMasters){
    				foreach($meterMasters as $meterMaster){
    					$meter_id=$meterMaster->__get("id");
    					$total_con = round($siteMeterReading->getEnergyConsumedById($curr_date,$last_date,$site_ids,null,null,$meter_id));
    					$totalConsumption=$totalConsumption+$total_con ;
    				}
    			}
    			
    			$mpptMasters=$mpptMapper->getMpptsBySiteId($site_ids);
    			$totalGenretion=0;
    			if($mpptMasters){
    				foreach($mpptMasters as $mpptMaster){
    					$mppt_id=$mpptMaster->__get("id");
    					$total_gen = round($mpptReadingMapper->getEnergyGenerationById($curr_date,$last_date,$site_ids,null,null,$mppt_id));
    					$totalGenretion=$totalGenretion+$total_gen;
    				}
    			}
    			
    			$consumption_data[] = array(
    					"site_name" => $site->__get("site_name"),
    					"consumption" => $totalConsumption,
    					"generation" => $totalGenretion,
    					"sold_unit" => $total_unit
    			);
    			
       		}
    		
    		$All_Data = array();
    		$All_revenue = array();
    		$total_revenue = array();
    		$unit_user = array();
    		$opex_arr = array();
    		$ebita_arr = array();$consumption_arr = array();$generetion_arr = array();$sold_arr = array();
    		foreach($consumer_data as $site_val)
    		{
    			$All_Data[] = array($site_val["site_name"], $site_val["total_consumer"]);
    		}
    		foreach($revenue_data as $revenue_datas)
    		{
    			$All_revenue[] = array($revenue_datas["site_name"], $revenue_datas["revenue"]);
    			$total_revenue[] = array($revenue_datas["site_name"], $revenue_datas["revenue_total"]);
    		}
    		foreach($unit_per_user_data as $site_val)
    		{
    			$unit_user[] = array($site_val["site_name"], $site_val["avg_unit"]);
    		}
    		foreach($opex_data as $site_val)
    		{
    			$opex_arr[] = array($site_val["site_name"], $site_val["opex_data"]);
    		}
    		foreach($ebita_data as $site_val)
    		{
    			$ebita_arr[] = array($site_val["site_name"], $site_val["ebita_data"]);
    		}
    		foreach($consumption_data as $site_val)
    		{
    			$sold_arr[] = array($site_val["site_name"], $site_val["sold_unit"]);
    			$generetion_arr[] = array($site_val["site_name"], $site_val["generation"]);
    			$consumption_arr[] = array($site_val["site_name"], $site_val["consumption"]);
    		}
    		$lines=array("lineWidth"=> 1);
    		$ConsumerCountArray = array("data" => $All_Data);
    		$RevenueArray = array("data" => $All_revenue);
    		$Revenue_totalArray = array("data" => $total_revenue);
    		$Unit_per_user_Array = array("data" => $unit_user);
    		$opex_Array = array("data" => $opex_arr);
    		$ebita_Array = array("data" => $ebita_arr);
    		$sold_Array = array("data" => $sold_arr);
    		$generetion_Array = array("data" => $generetion_arr);
    		$consumption_Array = array("data" => $consumption_arr);
    		 
    		array_push($data, $ConsumerCountArray);
    		array_push($data, $RevenueArray);
    		array_push($data, $Revenue_totalArray);
    		array_push($data, $Unit_per_user_Array);
    		array_push($data, $opex_Array);
    		array_push($data, $ebita_Array);
    		array_push($data, $consumption_Array);
    		array_push($data, $generetion_Array);
    		array_push($data, $sold_Array);
    		
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function getDataMonthWiseAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id=$request->getParam("site_id");
    		$state_id=$request->getParam("state_id");
    		$sitesMapper = new Application_Model_SitesMapper();
    		$consumersMapper=new Application_Model_ConsumersMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    		$wattageMapper=new Application_Model_WattageMapper();
    		$MMmapper=new Application_Model_SiteMasterMeterMapper();
    		$opexMapper=new Application_Model_OpexMapper();
    		$revenueMapper=new Application_Model_RevenueTargetMapper();
    		$siteMeterReading=new Application_Model_SiteMeterReadingMapper();
    		$mpptMapper=new Application_Model_MpptsMapper();
    		$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
    
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$curr_date = $request->getParam("curr_date");
    		$last_date = $request->getParam("last_date");
    		
    		$zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
    		$curr_date = $zendDate->toString("yyyy-MM-dd");
    		
    		$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
    		$last_date = $zendDate->toString("yyyy-MM-dd");
    
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    		
    		$cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
            $days = round($cal); 
            $m=0;$y=0;
			
            $counter=round($days/30);
            $next='+1 month';
    
    		$data=array();
    		  
    		$consumer_data=array();$revenue_data=array();$unit_per_user_data=array();
    		$opex_data=array();$ebita_data=array();$sold_unit_data=array();$consumption_data=array();$totalConsumption=0;
    		$generation_data=array();
    		$secondNext=$last_date; 
            for($i=0;$i<$counter;$i++){
            		
            	$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
    			 
    			$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    			 
    			$consumers=$consumersMapper->getActiveConsumerBySite($site_id,$state_id,$role_sites_id,date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext);
    			$totalEntryType = $cashRegisterMapper->getTotalBilledForEntryType(date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,$site_id,$state_id,$role_sites_id);
    			
    			$totalRevenue=$totalEntryType["CHG"]+$totalEntryType["SCHEME"]+$totalEntryType["OTHERS"]+$totalEntryType["ACT"];
    			 
    			$Avg_revenue_per_user=$totalRevenue/intval($consumers["all_consumer"]);
    
    			
    				
    			$trans_date_con=date_parse_from_format("Y-m-d", date('Y-m-d', strtotime($secondCurr."-1 day")));
    			$month_con= $trans_date_con["month"];
    			$year_con= $trans_date_con["year"];
    			$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
    			$consumers_details=$consumersMapper->getTotalConsumerBySiteIdAndMonthVal($month_con,$year_con,$site_id,$state_id,$role_sites_id);
    			$total_unit=0;
    			 
    			if ($consumers_details){
    				foreach ($consumers_details as $consumer_detail){
    					if($consumer_detail["is_postpaid"]==0){
    							
    						$feedarHrs=0;
    						$site_meter_ids=explode(",", $consumer_detail['site_meter_id']);
    
    						if(count($site_meter_ids)>0){
    							for ($k=0;$k<count($site_meter_ids);$k++){
    								$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
    								if($feedarDetail){
    									if($feedarDetail->__get("is_24_hr")==1){
    										$feedarHr=24;
    									}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
    										$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
    									}else{
    										$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
    									}
    									$feedarHrs=$feedarHrs+$feedarHr;
    								}
    							}
    						}
    
    						$watt_total=$wattageMapper->getWattageBywattId($consumer_detail["wattage"]);
    						if($watt_total){
    							$wattage=$watt_total->__get("wattage");
    						}else{
    							$wattage=0;
    						}
    							
    						$MonthlyUnit_pre=($wattage*$feedarHrs*$total_days_in_month)/1000;
    						$total_unit=$total_unit+$MonthlyUnit_pre;
    					}else{
    
    						$meterReading=$meterReadingMapper->getTotalUnitSiteWise($consumer_detail["consumer_id"],$month_con,$year_con);
    						if($meterReading){
    							if(floatval($meterReading["curr_reading"])>0){
    								$totalMtrUnit=(floatval($meterReading["curr_reading"])-floatval($meterReading["last_reading"]));
    								$monthly_Unit_post=$totalMtrUnit;
    							}else{
    								$monthly_Unit_post=0;
    							}
    						}else{
    							$monthly_Unit_post=0;
    						}
    
    						$total_unit=$total_unit+ $monthly_Unit_post;
    					}
    				}
    
    				$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer_detail['consumer_id']);
    				$month_scheme_pre_total=0;
    				 
    				if($consumer_scheme){
    					foreach($consumer_scheme as $consumer_schemes){
    						$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
    						$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
    
    						$discount_month=$consumer_schemes["discount_month"];
    						$feeder_hours=$consumer_schemes["feeder_hours"];
    						$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
    						$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month"));
    
    						if(strtotime($lastTimestamp)<=strtotime(date('Y-m-d', strtotime($secondCurr."-1 day"))) && strtotime($currTimestamp)>=strtotime(date('Y-m-d', strtotime($secondCurr."-1 day")))){
    							$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]*$feeder_hours*$total_days_in_month)/1000);
    							$total_unit=$total_unit + $dailyUnit;
    						}
    					}
    				}
    			}
    			 
    			 
    			$avg_unit_per_unit=round($total_unit/intval($consumers["all_consumer"]));
    			 
    		
    			$totalTarget=0;
    			$opexs=$opexMapper->getOpexData(1);
    			if($opexs){
    				foreach ($opexs as $opex){
    					$targets=$revenueMapper->getAllRevenueMonthSiteId($site_id, $month_con,$year_con, $opex["id"],$state_id,$role_sites_id);
    					if($targets){
    						foreach ($targets as $target){
    							$totalTarget=$totalTarget+$target["target"];
    						}
    					}
    				}
    			}
    			 
    			$ebita=round(($totalRevenue-$totalTarget)/$totalRevenue);
    			$ebita_total=round($totalRevenue-$totalTarget);
    			$totalConsumption=0;
    			$total =  round($siteMeterReading->getEnergyConsumedById(date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,$site_id,$role_sites_id,$state_id));
    			$totalConsumption=$totalConsumption+$total; 
    			 
				$total_gen=$mpptReadingMapper->getEnergyGenerationSolarById(date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,$site_id,$role_sites_id,$state_id);
				 
				if($total_gen){
					$solar=sprintf("%.2f",($total_gen["solar"]/$total_gen["totalunit"])*100);
					$dg=sprintf("%.2f",($total_gen["DG"]/$total_gen["totalunit"])*100); 
					$totalunit=sprintf("%.2f",($total_gen["totalunit"])); 
				}else{
					$solar=0;$dg=0;$totalunit=0; 
				}
				 
    			$trans_date=date_parse_from_format("Y-m-d", $secondNext);
    			$month= $trans_date["month"];
    			$year= $trans_date["year"];
    			
    			$dateObj   = DateTime::createFromFormat('!m', $month); 
    			$monthName = $dateObj->format('M');
    			$comma=",";
    			$full_month=$monthName.$comma.$year;
    			
    			$consumer_data[] = array(
    					"monthname" => $full_month,
    					"total_consumer" => intval($consumers["all_consumer"]));
    			
    			$revenue_data[] = array(
    					"monthname" => $full_month,
    					"revenue" => intval($Avg_revenue_per_user),
    					"revenue_total" => intval($totalRevenue));
    			
    			$unit_per_user_data[] = array(
    					"monthname" => $full_month,
    					"avg_unit" => $avg_unit_per_unit);
    			
    			$opex_data[] = array(
    					"monthname" => $full_month,
    					"opex_data" => $totalTarget);
    			
    			$ebita_data[] = array(
    					"monthname" => $full_month,
    					"ebita_data" => $ebita,
    					"ebita_total" => $ebita_total
    			);
    			
    			$consumption_data[] = array(
    					"monthname" => $full_month,
    					"consumption" => $totalConsumption,
    					"sold_unit" => $total_unit
    			);
				$generation_data[] = array(
    					"monthname" => $full_month,
						"solar" => $solar,
    					"dg" => $dg,
    					"totalunit" => $totalunit
    			);
    			 
    			$secondNext = date('Y-m-d', strtotime($secondNext.$next));
    		}
    
    		$All_Data = array();
    		$All_revenue = array();
    		$total_revenue = array();
    		$unit_user = array();
    		$opex_arr = array();
    		$ebita_arr = array();$ebita_total_arr=array();$consumption_arr = array();$generetion_arr = array();$sold_arr = array();
			$solar_arr=array();$totalunit_arr=array();$dg_arr=array();
    		foreach($consumer_data as $site_val)
    		{
    			$All_Data[] = array($site_val["monthname"], $site_val["total_consumer"]);
    		}
    		foreach($revenue_data as $revenue_datas)
    		{
    			$All_revenue[] = array($revenue_datas["monthname"], $revenue_datas["revenue"]);
    			$total_revenue[] = array($revenue_datas["monthname"], $revenue_datas["revenue_total"]);
    		}
    		foreach($unit_per_user_data as $site_val)
    		{
    			$unit_user[] = array($site_val["monthname"], $site_val["avg_unit"]);
    		}
    		foreach($opex_data as $site_val)
    		{
    			$opex_arr[] = array($site_val["monthname"], $site_val["opex_data"]);
    		}
    		foreach($ebita_data as $site_val)
    		{
    			$ebita_arr[] = array($site_val["monthname"], $site_val["ebita_data"]);
    			$ebita_total_arr[] = array($site_val["monthname"], $site_val["ebita_total"]);
    		}
    		foreach($consumption_data as $site_val)
    		{
    			$consumption_arr[] = array($site_val["monthname"], $site_val["consumption"]);
				$sold_arr[] = array($site_val["monthname"], $site_val["sold_unit"]);
    		}
    		foreach($generation_data as $site_val)
    		{
    			$solar_arr[] = array($site_val["monthname"], $site_val["solar"]);
				$dg_arr[] = array($site_val["monthname"], $site_val["dg"]);
				$totalunit_arr[] = array($site_val["monthname"], $site_val["totalunit"]);
    		}
    		$lines=array("lineWidth"=> 1);
    		$ConsumerCountArray = array("data" => $All_Data);
    		$RevenueArray = array("data" => $All_revenue);
    		$Revenue_totalArray = array("data" => $total_revenue);
    		$Unit_per_user_Array = array("data" => $unit_user);
    		$opex_Array = array("data" => $opex_arr);
    		$ebita_Array = array("data" => $ebita_arr);
    		$sold_Array = array("data" => $sold_arr);
    		$consumption_Array = array("data" => $consumption_arr);
    		$ebita_total_Array = array("data" => $ebita_total_arr);
			
			$solar_Array = array("data" => $solar_arr);
    		$dg_Array = array("data" => $dg_arr);
    		$totalUnit_Array = array("data" => $totalunit_arr);
    		 
    		array_push($data, $ConsumerCountArray);
    		array_push($data, $RevenueArray);
    		array_push($data, $Revenue_totalArray);
    		array_push($data, $Unit_per_user_Array);
    		array_push($data, $opex_Array);
    		array_push($data, $ebita_Array);
    		array_push($data, $consumption_Array);
    		array_push($data, $sold_Array);
    		array_push($data, $ebita_total_Array);
			
			array_push($data, $solar_Array);
    		array_push($data, $dg_Array);
    		array_push($data, $totalUnit_Array);
    
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }

    public function getStackChartConsumerRevenueAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id=$request->getParam("site_id");
    		$state_id=$request->getParam("state_id");
    		$sitesMapper = new Application_Model_SitesMapper();
    		$consumersMapper=new Application_Model_ConsumersMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    		$wattageMapper=new Application_Model_WattageMapper();
    		$MMmapper=new Application_Model_SiteMasterMeterMapper();
    		$opexMapper=new Application_Model_OpexMapper();
    		$revenueMapper=new Application_Model_RevenueTargetMapper();
    		$siteMeterReading=new Application_Model_SiteMeterReadingMapper();
    		$mpptMapper=new Application_Model_MpptsMapper();
    		$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
    
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$curr_date = $request->getParam("curr_date");
    		$last_date = $request->getParam("last_date");
    
    		$zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
    		$curr_date = $zendDate->toString("yyyy-MM-dd");
    
    		$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
    		$last_date = $zendDate->toString("yyyy-MM-dd");
    
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    
    		$cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
    		$days = round($cal);
    		$m=0;$y=0;
    			
    		$counter=round($days/30);
    		$next='+1 month';
    
    		$data=array();
    
    		$consumer_data=array();$revenue_data=array();$unit_per_user_data=array();
    		$opex_data=array();$ebita_data=array();$sold_unit_data=array();$consumption_data=array();
    		$generation_data=array();
    		$secondNext=$last_date;
    		for($i=0;$i<$counter;$i++){
    			
    			$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
    			
    			$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    			
    			$consumer_data_val=$consumersMapper->getConsumerType();
    			$household=0;$shops=0;$commercial=0;$telecom=0;
    			$household_rev=0;$shops_rev=0;$commercial_rev=0;$telecom_rev=0;
    			$totalCon=0;$totalRev=0;
    			foreach ($consumer_data_val	 as $consumer_datas){
    				$consumers=$consumersMapper->getActiveConsumerBySite($site_id,$state_id,$role_sites_id,date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,$consumer_datas["consumer_type_id"]);
    				$totalEntryType = $cashRegisterMapper->getTotalBilledForEntryType(date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,$site_id,$state_id,$role_sites_id,$consumer_datas["consumer_type_id"]);
    				
    				$totalRevenue=$totalEntryType["CHG"]+$totalEntryType["SCHEME"]+$totalEntryType["OTHERS"]+$totalEntryType["ACT"];
    				
    				if($consumer_datas["type_id"]==1){
    					$household=$household+$consumers["all_consumer"];
    					$household_rev=$household_rev+$totalRevenue;
    				}elseif ($consumer_datas["type_id"]==2){
    					$shops=$shops+$consumers["all_consumer"];
    					$shops_rev=$shops_rev+$totalRevenue;
    				}elseif($consumer_datas["type_id"]==3){
    					$commercial=$commercial+$consumers["all_consumer"];
    					$commercial_rev=$commercial_rev+$totalRevenue;
    				}elseif ($consumer_datas["type_id"]==4){
    					$telecom=$telecom+$consumers["all_consumer"];
    					$telecom_rev=$telecom_rev+$totalRevenue;
    				}
    				$totalCon=$totalCon+intval($consumers["all_consumer"]);
    				$totalRev=$totalRev+intval($totalRevenue);
    			}
    			
    			$per_household=($household/$totalCon)*100;
    			$per_shop=($shops/$totalCon)*100;
    			$per_commercial=($commercial/$totalCon)*100;
    			$per_telecom=($telecom/$totalCon)*100;
    			
    			$per_household_rev=($household_rev/$totalRev)*100;
    			$per_shop_rev=($shops_rev/$totalRev)*100;
    			$per_commercial_rev=($commercial_rev/$totalRev)*100;
    			$per_telecom_rev=($telecom_rev/$totalRev)*100;
    			
    			$trans_date=date_parse_from_format("Y-m-d", $secondNext);
    			$month= $trans_date["month"];
    			$year= $trans_date["year"];
    			
    			$dateObj   = DateTime::createFromFormat('!m', $month);
    			$monthName = $dateObj->format('M');
    			$comma=",";
    			$full_month=$monthName.$comma.$year;
    			 
    			$consumer_data[] = array(
    					"monthname" => $full_month,
    					"consumer_total" => intval($totalCon),
    					"per_household" => round($per_household),
    					"per_shop" => round($per_shop),
    					"per_commercial" => round($per_commercial),
    					"per_telecom" => round($per_telecom),
    			);
    			
    			$revenue_data[] = array(
    					"monthname" => $full_month,
    					"revenue_total" => round($totalRev),
    					"per_household_rev" => round($per_household_rev),
    					"per_shop_rev" => round($per_shop_rev),
    					"per_commercial_rev" => round($per_commercial_rev),
    					"per_telecom_rev" => round($per_telecom_rev),
    			);
    			 
    			 
    			$secondNext = date('Y-m-d', strtotime($secondNext.$next));
    		}
    
    		$All_consumer = array();$All_household = array();$All_shop=array();
    		$All_commercial=array();$All_telecom=array();
    		$All_revenue=array();$total_household_rev=array();$total_shop_rev=array();
    		$total_commercial_rev=array();$total_telecom_rev=array();
    		foreach($consumer_data as $site_val)
    		{
    			$All_consumer[] = array($site_val["monthname"], $site_val["consumer_total"]);
    			$All_household[] = array($site_val["monthname"], $site_val["per_household"]);
    			$All_shop[] = array($site_val["monthname"], $site_val["per_shop"]);
    			$All_commercial[] = array($site_val["monthname"], $site_val["per_commercial"]);
    			$All_telecom[] = array($site_val["monthname"], $site_val["per_telecom"]);
    		}
    		foreach($revenue_data as $revenue_datas)
    		{
    			$All_revenue[] = array($revenue_datas["monthname"], $revenue_datas["revenue_total"]);
    			$total_household_rev[] = array($revenue_datas["monthname"], $revenue_datas["per_household_rev"]);
    			$total_shop_rev[] = array($revenue_datas["monthname"], $revenue_datas["per_shop_rev"]);
    			$total_commercial_rev[] = array($revenue_datas["monthname"], $revenue_datas["per_commercial_rev"]);
    			$total_telecom_rev[] = array($revenue_datas["monthname"], $revenue_datas["per_telecom_rev"]);
    		}
    		 
    		$All_consumerArray = array("data" => $All_consumer);
    		$All_householdArray = array("data" => $All_household);
    		$All_shopArray = array("data" => $All_shop);
    		$All_commercialArray = array("data" => $All_commercial);
    		$All_telecomArray = array("data" => $All_telecom);
    		
    		$All_revenueArray = array("data" => $All_revenue);
    		$total_household_revArray = array("data" => $total_household_rev);
    		$total_shop_revArray = array("data" => $total_shop_rev);
    		$total_commercial_revArray = array("data" => $total_commercial_rev);
    		$total_telecom_revArray = array("data" => $total_telecom_rev);
    		 
    		 
    		array_push($data, $All_consumerArray);
    		array_push($data, $All_householdArray);
    		array_push($data, $All_shopArray);
    		array_push($data, $All_commercialArray);
    		array_push($data, $All_telecomArray);
    		
    		array_push($data, $total_household_revArray);
    		array_push($data, $total_shop_revArray);
    		array_push($data, $total_commercial_revArray);
    		array_push($data, $total_telecom_revArray);
    		 
    
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
      public function getPeopleImpactedAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id=$request->getParam("site_id");
    		$state_id=$request->getParam("state_id");
    		$sitesMapper = new Application_Model_SitesMapper();
    		$consumersMapper=new Application_Model_ConsumersMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    		$wattageMapper=new Application_Model_WattageMapper();
    		$MMmapper=new Application_Model_SiteMasterMeterMapper();
    		$opexMapper=new Application_Model_OpexMapper();
    		$revenueMapper=new Application_Model_RevenueTargetMapper();
    		$siteMeterReading=new Application_Model_SiteMeterReadingMapper();
    		$mpptMapper=new Application_Model_MpptsMapper();
    		$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
    
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$entry_status = $request->getParam("entry_status");
    
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    		$month_name = $request->getParam("month");
    		$month = date("m", strtotime($month_name));
    		$year = $request->getParam("year");
    
    		$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
    		 
    		$curr_date=$year."-".$month."-".$total_days_in_month;
    		$last_date=$year."-".$month."-01";
    		$data=array();
    		$sites=$sitesMapper->getSiteByStateId($state_id,$role_sites_id,$site_id);
    		 
    		$consumer_data=array();$revenue_data=array();$unit_per_user_data=array();
    		$opex_data=array();$ebita_data=array();$sold_unit_data=array();$consumption_data=array();
    		$generation_data=array();
    		foreach($sites as $site)
    		{
    			$site_ids=$site->__get("site_id");
    			$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    			 
    		$consumer_data_val=$consumersMapper->getConsumerType();
    			$totalConsumer=0;
    			foreach ($consumer_data_val	 as $consumer_datas){
    				$consumers=$consumersMapper->getActiveConsumerBySite($site_ids,$state_id,$role_sites_id,$curr_date,$secondNext,$consumer_datas["consumer_type_id"]);
    				 
    				if($consumer_datas["type_id"]==1){
					  $totalConsumer=$totalConsumer+($consumers["all_consumer"]*$consumer_datas["people_impact"]);
    				}elseif ($consumer_datas["consumer_type_id"]==10 || $consumer_datas["consumer_type_id"]==11 ){
    					$totalConsumer=$totalConsumer+($consumers["all_consumer"]*$consumer_datas["people_impact"]);
    				}elseif($consumer_datas["type_id"]==0){
    					 
    				}else{
    					$totalConsumer=$totalConsumer+($consumers["all_consumer"]*$consumer_datas["people_impact"]);
    				}
    				 
    			}
    			
    			 
    			$consumer_data[] = array(
    					"site_name" => $site->__get("site_name"),
    					"total_consumer" => intval($totalConsumer));
    			 
    			 
    		}
    
    		$All_Data = array();
    		foreach($consumer_data as $site_val)
    		{
    			$All_Data[] = array($site_val["site_name"], $site_val["total_consumer"]);
    		}
    		 
    		$lines=array("lineWidth"=> 1);
    		$ConsumerCountArray = array("data" => $All_Data);
    	 
    		 
    		array_push($data, $ConsumerCountArray);
    		 
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
}
?>