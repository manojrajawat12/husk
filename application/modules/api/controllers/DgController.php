<?php
class Api_DgController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
		$this->_userId=$auth->getIdentity()->user_id;
	}
	
	public function addDgAction(){
	
		try {
			 
			$request=$this->getRequest();
			$site_id=$request->getParam("site_id");
			$date_of_purchase=$request->getParam("date_of_purchase");
			$rate=$request->getParam("rate");
			$amount=$request->getParam("amount");
			$unit=$request->getParam("unit");
			$bill_attach=$request->getParam("bill_attach");
			$invoice_no=$request->getParam("invoice_no");
			  
			$zendDate = new Zend_Date($date_of_purchase,"MM-dd-yyyy");
			$timestamp = $zendDate->toString("yyyy-MM-dd");
			
			
			$zendDate = new Zend_Date();
			$zendDate->setTimezone("Asia/Kolkata");
			$timestamp_val = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
			 
			$DgMapper=new Application_Model_DgMapper();
			$Dg= new Application_Model_Dg();
			$Dg->__set("site_id",$site_id);
			$Dg->__set("date_of_purchase",$timestamp);
			$Dg->__set("rate",$rate);
			$Dg->__set("amount",$amount);
			$Dg->__set("unit",$unit);
			$Dg->__set("bill_attach",$bill_attach);
			$Dg->__set("user_id",$this->_userId);
			$Dg->__set("timestamp",$timestamp_val);
			$Dg->__set("invoice_no",$invoice_no);
			
			if($DgId=$DgMapper->addNewDG($Dg)){
	
				$this->_logger->info("New DG ID ".$DgId." has been created in Equipment by ". $this->_userName.".");
				
				$sitesMapper=new Application_Model_SitesMapper();
				$sites=$sitesMapper->getSiteById($site_id);
				
				$zendDate = new Zend_Date($date_of_purchase,"yyyy-MM-dd");
				$timestamp = $zendDate->toString("MM/dd/yyyy");
				
				$data=array(
						"dg_id" => $DgId,
						"site_id" => $site_id,
						"site_name" => $sites->__get("site_name"),
						"date_of_purchase" => $date_of_purchase,
						"rate" => $rate,
						"amount" => $amount,
						"unit" => $unit,
						"bill_attach" => $bill_attach,
						"invoice_no" => $invoice_no,
						
				);
	
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllDgAction(){
	
		try {
		$request=$this->getRequest();
			 $sites_id=$request->getParam("site_id");
			 $date_val=$request->getParam("date_val");
			    if($sites_id==null || $sites_id=='undefined' || $sites_id=='ALL'){ 
					$roleSession = new Zend_Session_Namespace('roles');
					$site_id=$roleSession->site_id;
			   }else{
					$site_id=explode(",", $sites_id);
			   }
   
			$curr_date = $request->getParam("curr_date");
            $last_date = $request->getParam("last_date");
            
			if($date_val==1){
				$zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
				$curr_date = $zendDate->toString("yyyy-MM-dd");
				$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
				$last_date = $zendDate->toString("yyyy-MM-dd");
			}else{
				$curr_date =NULL;
				$last_date =NULL;
			}
			
			$DgMapper=new Application_Model_DgMapper();
			$DGs=$DgMapper->getAllDG($site_id,$curr_date,$last_date);
	
			if($DGs){
				foreach ($DGs as $DG) {
					
					$sitesMapper=new Application_Model_SitesMapper();
					$stateMapper=new Application_Model_StatesMapper();
					$sites=$sitesMapper->getSiteById($DG->__get("site_id"));
					$state_id=$sites->__get("state_id");
					$states=$stateMapper->getStateById($state_id);
					
					$zendDate = new Zend_Date($DG->__get("date_of_purchase"),"yyyy-MM-dd");
					$timestamp = $zendDate->toString("MM/dd/yyyy");
					
					$zendDate = new Zend_Date($DG->__get("date_of_purchase"),"yyyy-MM-dd");
					$timestamp_edit = $zendDate->toString("dd-MMM-yyyy");
					
					$usermapper = new Application_Model_UsersMapper();
					$users = $usermapper->getUserById($DG->__get("user_id"));
	 
					$zendDate = new Zend_Date($DG->__get("timestamp"),"yyyy-MM-dd HH:mm:ss");
					$timestamp_time = $zendDate->toString("dd-MMM-yyyy HH:mm:ss");
					
					$data=array(
							"dg_id" => $DG->__get("dg_id"),
							"site_id" => $DG->__get("site_id"),
							"site_name" => $sites-> __get("site_name"),
							"state_name" => $states-> __get("state_name"),
							"date_of_purchase" => $timestamp,
							"date_of_purchase_edit" => $timestamp_edit,
							"rate" => $DG->__get("rate"),
							"amount" => $DG->__get("amount"),
							"unit" => $DG-> __get("unit"),
							"bill_attach" => $DG->__get("bill_attach"),
							"entry_by" =>$users->__get("user_fname") . " " . $users->__get("user_lname"),
							"timestamp" =>$timestamp_time,
							"status" => $DG->__get("status"),
							"claimed_by" => ucwords(strtolower($DG->__get("claimed_by"))),
							"invoice_no" => $DG->__get("invoice_no"),
							
					);
					 
					$Dg_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $Dg_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteDgByIdAction(){
	
		try {
			$request=$this->getRequest();
			$dg_id=$request->getParam("dg_id");
			$DgMapper=new Application_Model_DgMapper();
			if($Dgid=$DgMapper->deleteDgById($dg_id)){
				$this->_logger->info("DG Id ".$Dgid." has been deleted from DG table by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateDgByIdAction(){
	
		try {
			$request=$this->getRequest();
			$dg_id=$request->getParam("dg_id");
			$site_id=$request->getParam("site_id");
			$date_of_purchase=$request->getParam("date_of_purchase");
			$rate=$request->getParam("rate");
			$amount=$request->getParam("amount");
			$unit=$request->getParam("unit");
			$bill_attach=$request->getParam("bill_attach");
			$status=$request->getParam("status");
			$invoice_no=$request->getParam("invoice_no");
			 
			$zendDate = new Zend_Date($date_of_purchase,"MM-dd-yyyy");
			$timestamp = $zendDate->toString("yyyy-MM-dd");
			 
			$DgMapper=new Application_Model_DgMapper();
			$Dg= new Application_Model_Dg();
			$Dg->__set("dg_id",$dg_id);
			$Dg->__set("site_id",$site_id);
			$Dg->__set("date_of_purchase",$timestamp);
			$Dg->__set("rate",$rate);
			$Dg->__set("amount",$amount);
			$Dg->__set("unit",$unit);
			$Dg->__set("bill_attach",$bill_attach);
			$Dg->__set("status",$status); 
			$Dg->__set("invoice_no",$invoice_no);
	
			if($DgMapper->updateDg($Dg)){
	
				$this->_logger->info("Equipment Id ".$dg_id." has been updated by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 	public function verifyDgByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$claimed_by=$request->getParam("claimed_by");
			$DgMapper=new Application_Model_DgMapper();
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			if($DGid=$DgMapper->verifyDGById($id,$this->_userId,$timestamp,$claimed_by)){
				$this->_logger->info("DG Id ".$DGid." has been verify from DG table by ". $this->_userName.".");
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
}