<?php

/**
 * Created by PhpStorm.
 * User: Amit Bhardwaj
 * Date: 13-02-2017
 * Time: 14:14
 */
class Api_SiteSettingsController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }

    //------------------------------------------------------------------------------------------------------------------
    public function getAllSiteSettingsBySiteIdAction()
    {

        try {
            $request = $this->getRequest();

            $siteId = $request->getParam('id');
            if(!$siteId){
                throw new Exception('Invalid Request');
            }
            $siteSettingsMapper = new Application_Model_SiteSettingsMapper();
            $settingsMapper = new Application_Model_SettingsMapper();

            $siteSettings = $siteSettingsMapper->getAllSiteSettingsBySiteId($siteId,true);

            $globalSettingBRP = $settingsMapper->getSettingByName('brp');
            $brp = $globalSettingBRP?$globalSettingBRP->__get('setting_value'):0;
            $globalSettingHSF = $settingsMapper->getSettingByName('hsf');
            $hsf = $globalSettingHSF?$globalSettingHSF->__get('setting_value'):0;
//            $globalSettingX = $settingsMapper->getSettingByName('x');
//            $x = $globalSettingX?$globalSettingX->__get('setting_value'):0;
//            $globalSettingY = $settingsMapper->getSettingByName('y');
//            $y = $globalSettingY?$globalSettingY->__get('setting_value'):0;
//            var_dump($siteSettings);
//            die;
            $allSiteSettings = [];

            for ($i = 0; $i <= 23; $i++) {
                $next = $i == 23 ? 0 : $i + 1;
                $key = sprintf('%s-%s', $i, $next);

                $slotRow = $siteSettings && key_exists($key, $siteSettings) ? $siteSettings[$key] : false;

                $siteSettingArray = [
                    'time_slot' => $key,
                ];

                if ($slotRow && $slotRow instanceof Application_Model_SiteSettings) {
                    $siteSettingArray['brp'] = $slotRow->__get('brp');
                    $siteSettingArray['hsf'] = $slotRow->__get('hsf');
//                    $siteSettingArray['x'] = $slotRow->__get('x');
//                    $siteSettingArray['y'] = $slotRow->__get('y');
                } else {

                    $siteSettingArray['brp'] = $brp;
                    $siteSettingArray['hsf'] = $hsf;
//                    $siteSettingArray['x'] = $x;
//                    $siteSettingArray['y'] = $y;
                }

                $allSiteSettings[] = $siteSettingArray;

            }

            if ($allSiteSettings) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $allSiteSettings,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "No Entry Found."
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updateSiteSettingsAction()
    {
        try {
            $request = $this->getRequest();
            $jsonString = $request->getRawBody();
            $jsonData = json_decode($jsonString);
            if(!$jsonData){
                throw new Exception('Invalid Request');
            }

            $site_id = isset($jsonData->site_id)?$jsonData->site_id:false;
            $data = isset($jsonData->siteSettings) ? $jsonData->siteSettings : false;

            if(!$site_id || !$data || count($data) < 1){
                throw new Exception('Invalid Request');
            }

            $siteSettingsMapper = new Application_Model_SiteSettingsMapper();
//            $siteSettings = $siteSettingsMapper->getAllSiteSettings(true);

            foreach ($data as $siteSetting){
//                if($siteSetting->y >  $siteSetting->x){
//                    throw new Exception('Error in slot '.$siteSetting->time_slot.'. Y can not be greater then X');
//                }
                $existingSiteSetting = $siteSettingsMapper->getSiteSettingBySiteIdAndTimeSlot($site_id, $siteSetting->time_slot);

                if($existingSiteSetting){
                    $siteSettingModel = new Application_Model_SiteSettings();
                    $siteSettingModel->__set('id',$existingSiteSetting->__get('id'));
                    $siteSettingModel->__set('site_id',$site_id);
                    $siteSettingModel->__set('time_slot',(string) $siteSetting->time_slot);
                    $siteSettingModel->__set('brp',$siteSetting->brp);
                    $siteSettingModel->__set('hsf',$siteSetting->hsf);
//                    $siteSettingModel->__set('x',$siteSetting->x);
//                    $siteSettingModel->__set('y',$siteSetting->y);

                    $status = $siteSettingsMapper->updateSiteSettingById($siteSettingModel);
                    if(!$status){
                        throw new Exception('Error While update Site Settings');
                    }
                }else{
                    $siteSettingModel = new Application_Model_SiteSettings();
                    $siteSettingModel->__set('time_slot',$siteSetting->time_slot);
                    $siteSettingModel->__set('site_id',$site_id);
                    $siteSettingModel->__set('brp',$siteSetting->brp);
                    $siteSettingModel->__set('hsf',$siteSetting->hsf);
//                    $siteSettingModel->__set('x',$siteSetting->x);
//                    $siteSettingModel->__set('y',$siteSetting->y);

                    $status = $siteSettingsMapper->addNewSiteSetting($siteSettingModel);
                    if(!$status){
                        throw new Exception('Error While update Site Settings');
                    }
                }
            }

            $meta = array(
                "code" => 200,
                "message" => "Site settings updated successfully."
            );
            $arr = array(
                "meta" => $meta,
            );

        } catch (Exception $e) {
            $meta = array(
                "code" => 401,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;

    }


}