<?php
class Api_EquipmentController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function addEquipmentAction(){
	
		try {
			$request=$this->getRequest();
			$name=$request->getParam("name");
			$brand=$request->getParam("brand");
			$type=$request->getParam("type");
			 
			$EquipmentMapper=new Application_Model_EquipmentMapper();
			$Equipment= new Application_Model_Equipment();
			$Equipment->__set("name",$name);
			$Equipment->__set("brand",$brand);
			$Equipment->__set("type",$type);
	
			if($EquipmentId=$EquipmentMapper->addNewEquipment($Equipment)){
	
				$this->_logger->info("New Equipment ID ".$EquipmentId." has been created in Equipment by ". $this->_userName.".");
				
				$data=array(
						"id" => $EquipmentId,
						"name" => $name,
						"brand" => $brand,
						"type" => $type,
						
				);
	
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllEquipmentAction(){
	
		try {
			$EquipmentMapper=new Application_Model_EquipmentMapper();
			$Equipments=$EquipmentMapper->getAllEquipment();
	
			if(count($Equipments) >0){
				foreach ($Equipments as $Equipment) {
					 
					$data=array(
							"id" => $Equipment->__get("id"),
							"name" => $Equipment->__get("name"),
							"brand" => $Equipment-> __get("brand"),
							"type" => $Equipment->__get("type"),
							"type_name"=>($Equipment->__get("type")==0?"EeD":"MeD")
							
					);
	
					$Equipment_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $Equipment_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteEquipmentByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$EquipmentMapper=new Application_Model_EquipmentMapper();
			if($Equipmentid=$EquipmentMapper->deleteEquipmentById($id)){
				$this->_logger->info("Equipment Id ".$Equipmentid." has been deleted from Equipment by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateEquipmentByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$name=$request->getParam("name");
			$brand=$request->getParam("brand");
			$type=$request->getParam("type");
			 
			$EquipmentMapper=new Application_Model_EquipmentMapper();
			$EquipmentData= new Application_Model_Equipment();
			
			$EquipmentData->__set("id",$id);
			$EquipmentData->__set("name",$name);
			$EquipmentData->__set("brand",$brand);
			$EquipmentData->__set("type",$type);
	
			if($EquipmentMapper->updateEquipment($EquipmentData)){
	
				$this->_logger->info("Equipment Id ".$id." has been updated by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
}