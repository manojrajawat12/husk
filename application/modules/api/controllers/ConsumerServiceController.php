<?php
class Api_ConsumerServiceController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function addConsumerServiceAction(){
		
		try {
			$request = $this->getRequest();
			$service = $request->getParam("service");
			
			$complain = $request->getParam("complain");
			$complain_arr = explode(',', $complain);
			
			$ConsumerServiceMapper = new Application_Model_ConsumerServiceMapper();
		  
			$service_id = $ConsumerServiceMapper->addNewServiceRequest($service);

			if($service_id) {
				for($i=0;$i<count($complain_arr);$i++) { 
					$ConsumerServiceMapper->addNewComplainRemark($complain_arr[$i],$service_id);
				}
				  
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
    public function getAllConsumerServiceAction(){
    
        try {
            $ConsumerServiceMapper = new Application_Model_ConsumerServiceMapper();
            $consumerServices = $ConsumerServiceMapper->getAllServiceRequest();
			
			$service_arr=array();
            if($consumerServices){
                foreach ($consumerServices as $consumerService) {
					$reasons_arr=array();
                	$reasons = $ConsumerServiceMapper->getAllComplainRemark($consumerService["id"]);
					if($reasons){
						foreach ($reasons as $reason) {
							$data = array(
                            "id" => $reason["id"],
                            "remark" => $reason["remark"],
                            );
							$reasons_arr[] = $data;
						}
					}    
				    $data = array(
                            "id" => $consumerService["id"],
                            "service" => $consumerService["service"],
                            "reasons" => $reasons_arr,
                    );
                    $service_arr[] = $data;
                }
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $service_arr,
                );
    
            }
            else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
    
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

	public function deleteQueryByIdAction(){
	
		try {
			$request = $this->getRequest();
			$query_id = $request->getParam("id");
			$queryMapper = new Application_Model_QueryMapper();
			if($query = $queryMapper->deleteQueryById($query_id)){
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateQueryByIdAction(){
		
		try {
			// $data = json_decode(file_get_contents("php://input"));
			$request = $this->getRequest();
			$query_id = $request->getParam("id");
			$query = $request->getParam("query");
			
			$queryMapper = new Application_Model_QueryMapper();
			$queries = new Application_Model_Query();

			$temp = $queryMapper->updateQueryById($query_id, $query);

			if($temp){ 
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while updating"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
}