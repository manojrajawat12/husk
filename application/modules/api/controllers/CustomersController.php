<?php

class Api_CustomersController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
       // header('Content-Type: application/json');
    }
    
        public function addCustomerAction(){
      
         try {
            $request=$this->getRequest();
            $package_id=$request->getParam("id");
            $site_id=$request->getParam("site_id");
            $customer_code=$request->getParam("code");
            $customer_name=$request->getParam("customer_name");
            $customer_father_name=$request->getParam("father_name");
            $customer_status=$request->getParam("status");
            $customer_act_date=$request->getParam("act_date");
            $customer_act_charge=$request->getParam("act_charge");
            $customer_connection_id=$request->getParam("connection_id");
            $is_micro_enterprise=$request->getParam("is_micro");
            $micro_entrprise_price=$request->getParam("micro_price");
            $suspension_date=$request->getParam("suspension_date");
 
             
             $customermapper=new Application_Model_CustomersMapper();
             $customer= new Application_Model_Customers();
             $customer->__set("package_id",$package_id);
             $customer->__set("site_id",$site_id);
             $customer->__set("customer_code",$customer_code);
             $customer->__set("customer_name",$customer_name);
             $customer->__set("customer_father_name",$customer_father_name);
             $customer->__set("customer_status",$customer_status);
             $customer->__set("customer_act_date",$customer_act_date);
             $customer->__set("customer_act_charge",$customer_act_charge);
             $customer->__set("customer_connection_id",$customer_connection_id);
             $customer->__set("is_micro_enterprise",$is_micro_enterprise);
             $customer->__set("micro_entrprise_price",$micro_entrprise_price);
             $customer->__get("suspension_date",$suspension_date);

         if($customer=$customermapper->addNewCustomer($customer)){
             
            
              
            $data = array(
            'package_id' => $package_id,
            'site_id' => $site_id,
            'customer_code' => $customer_code,
            'customer_name' => $customer_name,
            'customer_father_name' => $customer_father_name,
            'customer_status' => $customer_status,
            'customer_act_date' => $customer_act_date,
            'customer_act_charge' => $customer_act_charge,
            'customer_connection_id' => $customer_connection_id,
             'is_micro_enterprise' => $is_micro_enterprise,
            'micro_entrprise_price' => $micro_entrprise_price,
            'suspension_date' => $suspension_date,
	);
                
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }

          public function getCustomerByIdAction(){
      
         try {
             $request=$this->getRequest();
             $customer_id=$request->getParam("id");
             $customermapper=new Application_Model_CustomersMapper();
         if($customer=$customermapper->getCustomerById($customer_id)){
             
            
           $data = array(
            'package_id' => $customer->__get("package_id"),
            'site_id' => $customer->__get("site_id"),
            'customer_code' => $customer->__get("customer_code"),
            'customer_name' => $customer->__get("customer_name"),
            'customer_father_name' => $customer->__get("customer_father_name"),
            'customer_status' => $customer->__get("customer_status"),
            'customer_act_date' => $customer->__get("customer_act_date"),
            'customer_act_charge' => $customer->__get("customer_act_charge"),
            'customer_connection_id' => $customer->__get("customer_connection_id"),
             'is_micro_enterprise' => $customer->__get("is_micro_enterprise"),
            'micro_entrprise_price' => $customer->__get("micro_entrprise_price"),
            'suspension_date' => $customer->__get("suspension_date"),
    );
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function deleteCustomerByIdAction(){
      
         try {
             $request=$this->getRequest();
             $customer_id=$request->getParam("id");
             $customermapper=new Application_Model_CustomersMapper();
         if($customer=$customermapper->deleteCustomerById($customer_id)){
             
            
            
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function getAllCustomersAction(){
      
         try {
//             $request=$this->getRequest();
//             $customer_id=$request->getParam("id");
               $customermapper=new Application_Model_CustomersMapper();
         if($customers=$customermapper->getAllCustomers()){
          
             foreach ($customers as $customer) {
                 
                  $data = array(
            'package_id' => $customer->__get("package_id"),
            'site_id' => $customer->__get("site_id"),
            'customer_code' => $customer->__get("customer_code"),
            'customer_name' => $customer->__get("customer_name"),
            'customer_father_name' => $customer->__get("customer_father_name"),
            'customer_status' => $customer->__get("customer_status"),
            'customer_act_date' => $customer->__get("customer_act_date"),
            'customer_act_charge' => $customer->__get("customer_act_charge"),
            'customer_connection_id' => $customer->__get("customer_connection_id"),
             'is_micro_enterprise' => $customer->__get("is_micro_enterprise"),
            'micro_entrprise_price' => $customer->__get("micro_entrprise_price"),
            'suspension_date' => $customer->__get("suspension_date"),
    );
                 
                 $customer_arr[]=$data;
             }
            
            
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $customer_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
            public function updateCustomerByIdAction(){
      
         try {
              $request=$this->getRequest();
              $customer_id=$request->getParam("id");
              $customer_name=$request->getParam("name");
              $customermapper=new Application_Model_CustomersMapper();
              $customer=new Application_Model_Customers();
              $customer->__set("customer_id" ,$customer_id);
              $customer->__set("customer_name" ,$customer_name);
              
              if($customers=$customermapper->updateCustomer($customer)){
          
                           
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
           
}    