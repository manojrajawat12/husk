<?php
class Api_StateController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function addStateAction(){
	
		try {
			$request=$this->getRequest();
			$state_name=$request->getParam("state_name");
			 
			$statesMapper=new Application_Model_StatesMapper();
			$states= new Application_Model_States();
			$states->__set("state_name",$state_name);
	
			if($state_id=$statesMapper->addNewState($states)){
	
			$this->_logger->info("New State ID ".$state_id." has been created in States by ". $this->_userName.".");
				
				$data=array(
						"state_id" => $state_id,
						"state_name" => $state_name,
				);
	
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllStatesAction(){
	
		try {
			$statesMapper=new Application_Model_StatesMapper();
			
			$states=$statesMapper->getAllStates();
	
			if(count($states) >0){
				foreach ($states as $state) {
					 
					$data=array(
							"state_id" => $state->__get("state_id"),
							"state_name" => $state->__get("state_name"),
							
					);
	
					$state_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $state_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "Error while getting"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteStateByIdAction(){
	
		try {
			$request=$this->getRequest();
			$state_id=$request->getParam("id");
			$statesMapper=new Application_Model_StatesMapper();
			if($state=$statesMapper->deleteStateById($state_id)){
			$this->_logger->info("State Id ".$state_id." has been deleted from States by ". $this->_userName.".");
					 
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateStateByIdAction(){
	
		try {
			$request=$this->getRequest();
			$state_id=$request->getParam("id");
			$state_name=$request->getParam("state_name");

			 
			$statesMapper=new Application_Model_StatesMapper();
			$states= new Application_Model_States();
			
			$states->__set("state_id",$state_id);
			$states->__set("state_name",$state_name);
			
	
			if($statesMapper->updateState($states)){
	
				$this->_logger->info("State ID ".$state_id." has been updated by ". $this->_userName.".");
				 
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while updating"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
}