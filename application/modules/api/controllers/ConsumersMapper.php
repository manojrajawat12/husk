<?php
class Application_Model_ConsumersMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Consumers();
			$this->_deleted_db_table = new Application_Model_DbTable_DeletedConsumers();
			$this->consumer_type_db_table = new Application_Model_DbTable_ConsumerTypes();
    }

   public function addNewConsumer(Application_Model_Consumers $consumer,$status=NULL)
    {
    	$type_of_me=$consumer->__get("type_of_me");
        if($type_of_me==NULL||$type_of_me==""){
        	$type_of_me='HH Basic';
        }
        
        $settingMapper=new Application_Model_SettingsMapper();
        
        $setings=$settingMapper->getSettingById(30);
        
        if($setings->__get("setting_value")=="no"){
        	$consumer_first_status="use"; 
        }else{
        	$consumer_first_status="new";
        }
         
        $data = array(
            'package_id' => $consumer->__get("package_id"),
            'site_id' => $consumer->__get("site_id"),
            'consumer_code' => $consumer->__get("consumer_code"),
            'consumer_name' => $consumer->__get("consumer_name"),
            'consumer_father_name' => $consumer->__get("consumer_father_name"),
            'consumer_status' => $consumer->__get("consumer_status"),
            'consumer_act_date' => $consumer->__get("consumer_act_date"),
            'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
            'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
            'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
            'meter_start_reading' => $consumer->__get("meter_start_reading"),
            'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
            'suspension_date' => $consumer->__get("suspension_date"),
         	'enterprise_type' => 'FlatUnit',
         	'type_of_me' => $type_of_me,
         	'notes' => $consumer->__get("notes"),
            'sms_opt_in' => $consumer->__get("sms_opt_in"),
            'site_meter_id' =>$consumer->__get("site_meter_id"),
            'credit_limit' =>$consumer->__get("credit_limit"),
         	'remark' =>$consumer->__get("remark"),
  			'wattage'=>$consumer->__get("wattage"),
  			'consumer_first_status'=>$consumer_first_status,
			'wattage_filter'=>$consumer->__get("wattage_filter"), 
			'created_by'=>$consumer->__get("created_by"),
			'create_type'=>$consumer->__get("create_type"), 
    );
         $result = $this->_db_table->insert($data);
        
        if(count($result)==0)
        {
        	return false;
        }
        else
        {
        	$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
        	$packageHistory=new Application_Model_PackageHistory();
        	$packageId=$consumer->__get('package_id');
        	$packages_id=explode(",", $packageId);
        	for($i=0;$i<count($packages_id);$i++){
	        	$packages=new Application_Model_PackagesMapper();
	        	
	        	$package=$packages->getPackageById($packages_id[$i]);
	        	$package_cost=$package->__get('package_cost');
	        	$consumerId=$result;
	        	$change_by =1;
	        	$change_description=null;
	        	$packageHistory->__set("package_id",$packages_id[$i]);
	        	$packageHistory->__set("consumer_id",$consumerId);
	        	$packageHistory->__set("change_by",$change_by);
	        	$packageHistory->__set("package_cost",$package_cost);
	        	$packageHistory->__set("change_description",$change_description);
	        	$packageHistory->__set("status","Request Received");
	        	$packageHistoryMapper->addNewPackageHistory($packageHistory);
	        	 
	        	if($consumer->__get("consumer_act_charge")>0){
					$cashRegisterMapper=new Application_Model_CashRegisterMapper();
					$date = new Zend_Date();
					$date->setTimezone("Asia/Kolkata");
					$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
					
					$cr_amount = str_pad($consumer->__get("consumer_act_charge"), 4, "0", STR_PAD_LEFT);
					$transaction_id = $timestamp . "-" . $cr_amount;
					
					$packageId=$consumer->__get('package_id');
					$packages_id=explode(",", $packageId);
					
					$cashRegister = new Application_Model_CashRegister();
					$cashRegister->__set("user_id", $consumer->__get("created_by"));
					$cashRegister->__set("consumer_id", $result);
					$cashRegister->__set("cr_entry_type", "DEBIT");
					$cashRegister->__set("cr_amount", $consumer->__get("consumer_act_charge"));
					$cashRegister->__set("receipt_number", "CA-".$consumer->__get("created_by"));
					$cashRegister->__set("transaction_id", $transaction_id);
					$cashRegister->__set("transaction_type", $consumer->__get("create_type"));
					$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd"));
					$cashRegister->__set("entry_status", "ACT");
					$cashRegister->__set("remark", "Activation Bill");
					$cashRegister->__set("package_id", $packages_id[0]);
					 
					$cashRegisterMapper->addNewCashRegister($cashRegister);
				}
				
        	}
        	return $result;
        }
        
    }
    public function getConsumerById($consumer_id)
    {
        $result = $this->_db_table->find($consumer_id);
        if( count($result) == 0 ) {
            $consumer = new Application_Model_Consumers(); 
            return $consumer;
        }
        $row = $result->current();
        $consumer = new Application_Model_Consumers($row);
        return $consumer;
    }
    public function getAllConsumerByPackageId($package_id,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL)
    {
    	$role_id=implode(",", $role_sites_id);
    	    	
    	if($site_id==""){
    		$site_id='null';
    	} 
    	if($state_id==""){
    		$state_id='null';
    	}

        $query ="call getAllConsumerByPackageId($package_id,$site_id,$state_id,'$role_id')";
       // echo $query;

        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
     
       if($result == "0" ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
       
        return $consumer_object_arr;
    }
    public function getConsumerByConnectionID($consumer_connection_id)
    {
    	$where = array(
            "consumer_connection_id = ?" => $consumer_connection_id
        );
        $result = $this->_db_table->fetchRow($where);
        if( count($result) == 0 ) {
            $consumer = new Application_Model_Consumers(); 
            return $consumer;
        }
        $row = $result;
        $consumer = new Application_Model_Consumers($row);
        return $consumer;
    }
	public function getAllConsumers($active=true,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$cronValue=NULL,$year=NULL,$month=NULL,$suspense=FALSE)
    {
	
		if($cronValue==null || $cronValue==""){
			$cronValue='null';
			$roleSession = new Zend_Session_Namespace('roles');
    	    $role_sites_id=$roleSession->site_id;
			$role_id=implode(",", $role_sites_id);
		}
		else{
			$role_id='null';
		}
			
    	if($site_id ==""){$site_id='null';}
    	if($active==""){$active='false';}
    	if($state_id==""){$state_id='null';}	
    	if($year !=NULL && $month !=NULL)
		{
			$curDate=$year.'-'.$month.'-01';
        	$nmonth=date('m',strtotime($curDate));
			$curDate=$year.'-'.$nmonth.'-01';
			$nextdate = date('Y-m-d', strtotime('+1 month', strtotime($curDate)));
			//$where="and date(consumer_act_date)< '$nextdate'"; 
		}
		else{
			$nextdate ='null';
		}
		
		 $query="call getAllConsumers($active,$state_id,$site_id,'$role_id',$cronValue,'$nextdate','$suspense')";
    	 
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
       // $result = $this->_db_table->fetchAll($where,array('consumer_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $key => $row)
        {
                $consumer_object = new Application_Model_Consumers($row);
                foreach($row as $key=>$value)
                {
                	$consumer_object->__set($key,$value);
                }
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    
    public function getConsumersByColumnValue($column,$value,$all=true,$year=NULL,$month=NULL,$suspense=FALSE,$order=NULL)
    {
     
      /* $where = array(
            $column." = ?" => $value
        );*/
		$where="";
        if(!$all)
        {
				$where=" and consumer_status !='banned'";
        }
 
      /*  if($year !=NULL && $month !=NULL)
		{
			$curDate=$year.'-'.$month.'-01';
        	$nmonth=date('m',strtotime($curDate));
			$curDate=$year.'-'.$nmonth.'-01';
			$nextdate = date('Y-m-d', strtotime('+1 month', strtotime($curDate)));
			if($suspense)
				$where=" and (date(suspension_date)> '$nextdate' or consumer_status !='banned')";
			else
				$where=$where."and date(consumer_act_date)< '$nextdate'";
		}*/
		 if($year !=NULL && $month !=NULL)
		{
			$curDates=$year.'-'.$month.'-01';
        	$nmonth=date('m',strtotime($curDates));
			$curDate=$year.'-'.$nmonth.'-31';
			$date_01=$year.'-'.$nmonth.'-01';
			if($suspense){
				$where=" and ((date(consumer_act_date)<='".$curDate."' and  consumer_status!='banned') or 
							 (date(suspension_date)>'".$curDate."'  and
							  date(consumer_act_date)<='".$curDate."' and consumer_status='banned'))";
			}	
			else{
				$where=$where."and date(consumer_act_date)<= '$curDate'";
			}
		}
		if($order!=NULL){
			$order_by=" consumer_name, ";
		}else{
			$order_by=" consumer_connection_id , ";
		}
		 $query="select * from consumers inner join sites on consumers.site_id=sites.site_id where consumers.".$column." ='".$value."' $where  and sites.site_status<>'Disable' order by ".$order_by." consumer_status ='banned', consumer_act_date desc,consumer_id desc";

		 
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($query);
		
        if(count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers();
                foreach($row as $key=>$value)
                {
                    $consumer_object->__set($key,$value);
                }
                array_push($consumer_object_arr,$consumer_object);
        }
    
        return $consumer_object_arr;
    }
    public function updateConsumer(Application_Model_Consumers $consumer)
    {
    	 
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		
        $data = array(
            'package_id' => $consumer->__get("package_id"),
            'site_id' => $consumer->__get("site_id"),
            'consumer_code' => $consumer->__get("consumer_code"),
            'consumer_name' => $consumer->__get("consumer_name"),
            'consumer_father_name' => $consumer->__get("consumer_father_name"),
            'consumer_status' => $consumer->__get("consumer_status"),
            'consumer_act_date' => $consumer->__get("consumer_act_date"),
            'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
            'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
             'meter_start_reading' => $consumer->__get("meter_start_reading"),
            'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
            'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
            'suspension_date' => $consumer->__get("suspension_date"),
            'sms_opt_in' => $consumer->__get("sms_opt_in"),
        	'enterprise_type' => $consumer->__get("enterprise_type"),
        	'type_of_me' => $consumer->__get("type_of_me"),
        	'notes' => $consumer->__get("notes"),
        	'site_meter_id' =>$consumer->__get("site_meter_id"),
        	'credit_limit' =>$consumer->__get("credit_limit"),
            'sms_flag' =>$consumer->__get("sms_flag"),
			'flag_date' =>$consumer->__get("flag_date"),
			'remark' =>$consumer->__get("remark"),
			'consumer_first_status'=>$consumer->__get("consumer_first_status"),
			'approve_date'=>$timestamp,
			'approve_by' =>$consumer->__get("approve_by"),
			'reason_remark' =>$consumer->__get("reason_remark"),
 
	);
      
        $where = "consumer_id = " . $consumer->__get("consumer_id");
        
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteConsumerById($consumer_id,$remark=null)
    {
    	$db_table = new Application_Model_DbTable_DeletedConsumers();
    	$consumer=$this->getConsumerById($consumer_id);
		
		$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		
    	$data = array(
    			'package_id' => $consumer_id,
    			'site_id' => $consumer->__get("site_id"),
    			'consumer_code' => $consumer->__get("consumer_code"),
    			'consumer_name' => $consumer->__get("consumer_name"),
    			'consumer_father_name' => $consumer->__get("consumer_father_name"),
    			'consumer_status' => "inactive",
    			'consumer_act_date' => $consumer->__get("consumer_act_date"),
    			'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
    			'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
    			'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
    			'meter_start_reading' => $consumer->__get("meter_start_reading"),
    			'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
    			'suspension_date' => $timestamp,
    			'enterprise_type' => $consumer->__get("enterprise_type"),
    			'type_of_me' => $consumer->__get("type_of_me"),
    			'notes' => $consumer->__get("notes"),
    			'sms_opt_in' => $consumer->__get("sms_opt_in"),
    			'site_meter_id' =>$consumer->__get("site_meter_id"),
    			'credit_limit' =>$consumer->__get("credit_limit"),
    			'sms_flag' => $consumer->__get("sms_flag"),
    			'wallet_bal' =>$consumer->__get("wallet_bal"),
    			'flag_date' =>$consumer->__get("flag_date"),
    			 "remark"=>$remark
    	);
    	
    	$result = $db_table->insert($data);
    	
        $where = "consumer_id = " . $consumer_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
        	return true;
        }
    }
    
	public function searchByColumnValue($column=NULL,$column_value=NULL,$consumer_code=FALSE){
    	$where = NULL;
        if($column!=NULL){
            if($consumer_code)
            {
                $where = " where consumers.".$column."= '".$column_value."' and sites.site_status!='Disable'" ;
            }
            else
            {
                $where = " where consumers.".$column." LIKE '%".$column_value."%' and sites.site_status!='Disable'";
            }
        }
        else
        {
            $where = NULL;
        }
        $query="select * from consumers inner join sites on consumers.site_id=sites.site_id ".$where."  order by consumer_id ASC";
       
        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);
      
 
      //  $result = $this->_db_table->fetchAll($where,array('consumer_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
            $consumer_object = new Application_Model_Consumers($row);
	    $consumer_object->__set("consumer_id", $row["consumer_id"]);
	    $consumer_object->__set("package_id", $row["package_id"]);
            $consumer_object->__set("site_id", $row["site_id"]);
            $consumer_object->__set("consumer_code", $row["consumer_code"]);
            $consumer_object->__set("consumer_name", $row["consumer_name"]);
            $consumer_object->__set("consumer_father_name", $row["consumer_father_name"]);
            $consumer_object->__set("consumer_status", $row["consumer_status"]);
            $consumer_object->__set("consumer_act_date", $row["consumer_act_date"]);
            $consumer_object->__set("consumer_act_charge", $row["consumer_act_charge"]);
            $consumer_object->__set("is_micro_enterprise", $row ["is_micro_enterprise"]);
            $consumer_object->__set("meter_start_reading", $row["meter_start_reading"]);
            $consumer_object->__set("wattage", $row["wattage"]);
            $consumer_object->__set("micro_entrprise_price", $row["micro_entrprise_price"]);
            $consumer_object->__set("consumer_connection_id", $row["consumer_connection_id"]);
            $consumer_object->__set("suspension_date", $row["suspension_date"]);
            $consumer_object->__set("sms_opt_in", $row["sms_opt_in"]);
            $consumer_object->__set("enterprise_type", $row["enterprise_type"]);
            $consumer_object->__set("type_of_me", $row["type_of_me"]);
            $consumer_object->__set("notes", $row["notes"]);
            $consumer_object->__set("site_meter_id", $row["site_meter_id"]); 
            $consumer_object->__set("credit_limit", $row["credit_limit"]);

            array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    
    public function getConsumerOutstanding($consumer_id)
    {
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        $enteries = $cashRegisterMapper->getEntryByConsumerId($consumer_id);
		$outstandingAmount = 0;
        if($enteries){
            $creditAmount = $debitAmount = 0;
            $typesArray = array("CREDIT");

            $creditEnteries = $cashRegisterMapper->applyTypeFilter($enteries, $typesArray);

            if($creditEnteries){
                foreach($creditEnteries as $entry){
                    $creditAmount += $entry->__get("cr_amount");
                }

            }

            $typesArray = array("DEBIT","PENALTY","REFUND");
            $debitEnteries = $cashRegisterMapper->applyTypeFilter($enteries, $typesArray);

            if($debitEnteries){
                foreach($debitEnteries as $entry){
                    if($entry->__get("entry_status")!='ACT'){
						$debitAmount += $entry->__get("cr_amount");
					}
                }
            }
		
            $outstandingAmount = $debitAmount - $creditAmount;
       }
       return $outstandingAmount;
    }
    public function getRemainingActivation($consumer_id)
    {
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        $enteries = $cashRegisterMapper->getEntryByConsumerId($consumer_id);
        //$consumer = $this->getConsumerById($consumer_id);
        //$actAmount = $consumer->__get("consumer_act_charge");
		$query = "SELECT sum(cr_amount) as cr_amount from cash_register where cr_status!='INACTIVE' and 
					cr_entry_type='DEBIT' and entry_status='ACT' and consumer_id=".$consumer_id; 
        
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
		$actAmount = $result[0]["cr_amount"];
        $activationAmount = 0;
        if($enteries){
            $creditAmount = $debitAmount = 0;
            $typesArray = array("ACTIVATION");

            $activationEnteries = $cashRegisterMapper->applyTypeFilter($enteries, $typesArray);

            if($activationEnteries){
                foreach($activationEnteries as $entry){
                    $activationAmount += $entry->__get("cr_amount");
                }

            }
            $actAmount = $actAmount - $activationAmount;
       }
       return $actAmount;
    }
    
    public function searchConsumerBySite($site_name=NULL,$site_id=NULL)
    {
        if($site_name!=NULL){
            $query = "SELECT sites.*,consumer.*
                     FROM sites
                     INNER JOIN consumer
                     ON sites.site_id=consumer.site_id
                     WHERE sites.site_name LIKE '%".$site_name."%'"." and sites.site_status!='Disable'";
        }
        elseif($site_id!=NULL)
        {
            $query = "SELECT sites.*,consumer.*
                     FROM sites
                     INNER JOIN consumer
                     ON sites.site_id=consumer.site_id
                     WHERE sites.site_id=".$site_id." and sites.site_status!='Disable'";
        }
        else{
            return FALSE;
        }
        
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if( count($result) == 0 ) {
                return false;
        }
        
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers();
                foreach($row as $key=>$value)
                {
                    $consumer_object->__set($key,$value);
                }
                array_push($consumer_object_arr,$consumer_object);
        }
    
        return $consumer_object_arr;
    }
    
    public function cashPaidByConsumer($consumer_id,$from=NULL,$to=NULL){
        
        $query = "SELECT * FROM cash_register
                 WHERE consumer_id=".$consumer_id." AND (cr_entry_type = 'CREDIT' OR cr_entry_type = 'ACTIVATION' OR cr_entry_type = 'REFUND')";
               // echo $query; exit;
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        
        $amount = 0;
        $filteredValues = array();
        
        if( count($result) != 0 ) {
            /*Date Filter */
            if($from != NULL && $to != NULL){
                foreach ($result as $row){
                    $date = new Zend_Date($row['timestamp'],"yyyy-MM-dd HH:mm:ss");
                    if($date->isEarlier($to) && $date->isLater($from)){         
                        $filteredValues[] = $row; 
                    }
                }
            }
            else{
                /*without Date filter */
                foreach ($result as $row){
                    $filteredValues[] = $row;
                }
            }
            
            foreach ($filteredValues as $row){
                if($row['cr_entry_type']=="CREDIT" or $row['cr_entry_type']=="ACTIVATION"){
                    $amount = $amount + $row['cr_amount'];
                }
                elseif($row['cr_entry_type']=="REFUND"){
                    $amount = $amount - $row['cr_amount'];
                }
            }
        }
        return $amount;
    }
    
    public function getConsumersByColumn($site_id,$package_id)
    {
        $where = array(
            "site_id = ?"=>$site_id,
            "package_id = ?"=>$package_id
        );
        
       //echo  $query="select * from consumers where site_id=".$site_id ." and package_id=".$package_id;
        $result = $this->_db_table->fetchAll($where,array('consumer_id DESC'));
      //  print_r($result);
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    
    public function getMonthlyActiveConsumers($site_id,$consumer_status){
        
        $where = array(
            "site_id =?"=>$site_id,
            "consumer_status =?"=>$consumer_status,
        );
        
        $result = $this->_db_table->fetchAll($where,array('consumer_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
        
    }
    public function getTotalConsumersBySite($site_id,$month=NULL)
    {
        $consumer = array();
        if($month==NULL)
        {   
            $zendDate = new Zend_Date ();
            $y = $zendDate->toString(Zend_Date::YEAR);
            $currentMonth = $zendDate->toString(Zend_Date::MONTH_SHORT);
            for($currentMonth;$currentMonth>=1;$currentMonth--)
            {
                $month31 = array(1,3,5,7,8,10,12);
                $month30 = array(4,6,9,11);
                $month28 = array(2);
                if(in_array($currentMonth, $month31))
                {
                    $date = "31";
                }
                elseif(in_array($currentMonth, $month30))
                {
                    $date = "30";
                }
                elseif(in_array($currentMonth, $month28))
                {
                    $date = "28";
                }
                else
                {
                    $date = "0";
                }
                
                
                $query = "SELECT * FROM consumer WHERE site_id='".$site_id."' AND DATE(consumer_act_date) < '".$y."-".$currentMonth."-".$date."'";
                $stmt = $this->_db_table->getAdapter()->query($query);
                $result = $stmt->fetchAll();
                $count = count($result);
                $consumer[] = $count;
            }
            return array_reverse($consumer);
        }
    }
    
    public function getConsumersByConnectionId($connection_id)
    {

         $where = array(
            "consumer_connection_id = ?" => "$connection_id"
        );
        
        $result = $this->_db_table->fetchAll($where);
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    public function getConsumersCountWithActDate($mounth)
    {
        
    
         $query = "SELECT * FROM consumers WHERE  MONTH(consumer_act_date)=$mounth"; 
       
       $consumer = $this->_db_table->getAdapter()->query($query);
        $result = $consumer->fetchAll();
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers();
                foreach($row as $key=>$value)
                {
                    $consumer_object->__set($key,$value);
                }
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
     public function getTotalConsumersCountByMonth($currdate=NULL,$lastdate=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL)
    {
        if($site_id!=NULL)
        {
        	  $query = "SELECT * FROM consumers inner join sites on consumers.site_id=sites.site_id WHERE consumers.site_id in (".$site_id.") AND
             date(consumer_act_date)>='".$lastdate."'  and date(consumer_act_date)<'".$currdate."' and consumer_status!='banned' and sites.site_status!='Disable' and sites.site_id!=1";
        }else if($state_id!=NULL){
        	$query = "SELECT * FROM consumers inner join sites on consumers.site_id=sites.site_id WHERE sites.state_id in (".$state_id.") and
            date(consumer_act_date)>='".$lastdate."'  and date(consumer_act_date)<'".$currdate."' and consumer_status!='banned' and sites.site_status!='Disable' and sites.site_id!=1";
        	 
        }
        else{	 
        	$query = "SELECT * FROM consumers inner join sites on consumers.site_id=sites.site_id  
        			  WHERE consumers.site_id in (".implode(",", $role_sites_id).")  
            	   	  and date(consumer_act_date)>='".$lastdate."'  and date(consumer_act_date)<'".$currdate."'
            	  	  and consumer_status!='banned'"." and sites.site_status!='Disable' and sites.site_id!=1"; 
        }
          
        $consumer = $this->_db_table->getAdapter()->query($query);
        $result = $consumer->fetchAll();
        if( count($result) == 0 ) {
                return false;
        }
        return count($result);
    }
	
    public function getConsumersCountWithBanned()
    {
        
    
         $query = "SELECT * FROM consumers WHERE  consumer_status='banned'"; 
       
       $consumer = $this->_db_table->getAdapter()->query($query);
        $result = $consumer->fetchAll();
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers();
                foreach($row as $key=>$value)
                {
                    $consumer_object->__set($key,$value);
                }
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    public function getConsumersCountWithSuspensionDate($mounth)
    {
    
         $query = "SELECT * FROM consumers WHERE  MONTH(suspension_date)=".$mounth;  
       
       $consumer = $this->_db_table->getAdapter()->query($query);
        $result = $consumer->fetchAll();
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers();
                foreach($row as $key=>$value)
                {
                    $consumer_object->__set($key,$value);
                }
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
   
    public function getConsumersByLastPaymentDate($site_id,$from_date,$to_date)
    {
        /* $query = "SELECT * FROM consumers "
                . "INNER JOIN cash_register "
                . "ON cash_register.consumer_id=consumers.consumer_id "
                . "WHERE cash_register.timestamp>=STR_TO_DATE('".$from_date."','%d/%m/%Y') "
                . "AND cash_register.timestamp<=STR_TO_DATE('".$to_date."','%d/%m/%Y') "
                . "AND cash_register.cr_entry_type='CREDIT' "
                . "AND consumers.site_id=".$site_id; */
    	$query="call getConsumersByLastPaymentDate($site_id,'$from_date','$to_date')";
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Consumers();
                foreach($row as $key=>$value)
                {
                    $consumer_object->__set($key,$value);
                }
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    public function getAllPaidConsumers($site_id)
    {
        $date = new Zend_Date();
        $date->setTimezone("Asia/Calcutta");
        $month = $date->toString("MM");
        $year = $date->toString("yyyy");
        $day = $date->toString("dd");
        $consumers = $this->getConsumersByLastPaymentDate($site_id, "01/".$month."/".$year, $day."/".$month."/".$year);
        return $consumers;
    }
     
    public function getTotalCollectedAmount($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$curr_date=NULL,$last_date=NULL)
    {
         if($site_id!=NULL){
            $site = " inner Join sites on sites.site_id=consumers.site_id where consumers.site_id in (".$site_id.") and sites.site_status!='Disable'";
         	
         }
         elseif ($state_id!=NULL){
         	$site=" inner Join sites on sites.site_id=consumers.site_id where sites.state_id in (".$state_id.") and sites.site_status!='Disable'";
         }
         else{
         	$site=" Where site_id in (".implode(",", $role_sites_id).") ";
        }
        if($curr_date!=NULL){
        	$date=" and date(consumer_act_date)<'".$curr_date."'";
        } else{
        	$date="";
        }
        if($last_date!=NULL){
        	$years=" and date(consumer_act_date)>='" .$last_date."'";
        } else{
        	$years="";
        }
        $query = "SELECT SUM(consumer_act_charge) AS total FROM consumers" .$site.$date.$years;
        
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        return $result[0]["total"];
    }
    public function getDulplicateConsumers()
    {
    	$selectConsumer="select * FROM consumers WHERE site_id!=1 and consumer_id in(SELECT consumer_id from credit_count where cr_count >1) OR consumer_id in(SELECT consumer_id from activation_count WHERE act_count >1);";
		$dropTempTableQuery="DROP table cash_enrty;drop table activation_count;drop table credit_count;";
		 
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($selectConsumer);
		$stmt->getConnection()->prepare($dropTempTableQuery)->execute();
		$consumer_object_arr = array();
		if( count($result) == 0 ) {
			return $consumer_object_arr;
		}
		
		foreach ($result as $row)
		{
			$consumer_object = new Application_Model_Consumers();
			foreach($row as $key=>$value)
			{
				$consumer_object->__set($key,$value);
			}
			array_push($consumer_object_arr,$consumer_object);
		}
		return $consumer_object_arr;
    }
    
    public function updateConsumerPackage($package_id,$consumer_id)
    {
    	$result =NULL;
    	try{
    	$packageMapper=new Application_Model_PackagesMapper();
    	$package=$packageMapper->getPackageById($package_id);
    	
    	if($package->__get("is_postpaid")==2 || $package->__get("is_postpaid")==1){
    		$is_micro_enterprise=1;
    	} else{	$is_micro_enterprise=0;	}
    	$data = array(
    			'package_id' => $package_id,
    			'is_micro_enterprise' => $is_micro_enterprise,
    	);
    	$where = "consumer_id = " . $consumer_id;
    
    	$result = $this->_db_table->update($data,$where);
    	}catch(Exception $e){
    		$file = '../html/test.txt';
    		$current = file_get_contents( $file);
    		$current .= $e->getMessage()."\r\n";
    		file_put_contents($file, $current);
    	}
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
    
    public function getConsumerMeterConnections($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$all=true,$type=NULL)
    {
         if($site_id!=NULL){
            $site = " Where  site_id=".$site_id ;
         }
		 else if($state_id!=NULL){
			$site=" inner join sites s on c.site_id=s.site_id WHERE  s.state_id=".$state_id." and s.site_status!='Disable'";
		} else{
         	$site=" Where  site_id in (".implode(",", $role_sites_id).") ";
        }
       
		$status="";
		if(!$all)
		{
		 $status=" and consumer_status !='banned'";
		}
		if($type=='mixmode'){
			$type_package="(2)";
		}elseif ($type=='me'){
			$type_package="(1)";
		}else{
			$type_package="(1,2)";
		}
		
           $query = "select * from (select c.consumer_id, c.consumer_connection_id,c.consumer_name, mr.meter_reading,mr.meter_reading_id,mr.timestamp 
from meter_readings mr  inner join consumers c on c.consumer_id= mr.consumer_id".
$site." $status AND c.package_id in (select package_id from packages where is_postpaid in ".$type_package.") AND c.consumer_status='ACTIVE' order by mr.timestamp desc) as meter
group by meter.consumer_id";
 
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        return $result;
    }
	
	public function getConsumerOutstandings($site_id=NULL,$state_id=NULL,$role_sites_id=NULL)
    {
         if($site_id!=NULL){
            $site = " Where site_id=".$site_id ;
         }
		 else if($state_id!=NULL){
			$site=" inner join sites s on c.site_id=s.site_id WHERE s.state_id=".$state_id." and s.site_status!='Disable'";
		}else{
         	$site=" Where site_id in (".implode(",", $role_sites_id).") ";
        }
      
        $query = "select c.consumer_id,c.site_id, c.consumer_connection_id,c.consumer_name, sum(debit_amount) as debit ,sum(credit_amount) as credit, (sum(debit_amount) - sum(credit_amount)) As outstanding
				from defaulter_months df  inner join consumers c on c.consumer_id= df.consumer_id ".$site." AND c.consumer_status='ACTIVE' and c.site_id!=1
				group by  c.consumer_id order by outstanding desc limit 50;";

        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        return $result;
    }

	public function getConsumerLogsbyConnectionId($connection_id=NULL)
    {
    	$query = "select * from logs where ( message like '% banned from active%' or message like '% active from banned%' ) and message like '%$connection_id%'";
    	//echo $query;
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
    public function  getAllConsumersOfMixmode($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$cluster_id=NULL){
    	$cond="";
		$where=" where p.is_postpaid=2 and c.consumer_status<>'banned'";
		if($role_sites_id !=NULL)
		{
			$where =$where." and c.site_id in (".implode(",", $role_sites_id).") ";
		}
		if($site_id !=NULL)
		{
			$cond=" inner join sites s on s.site_id = c.site_id ";
			$where =$where." and c.site_id = ".$site_id." and s.site_status!='Disable'";
		}
		else if($cluster_id !=NULL)
		{
			$cond=" inner join sites s on s.site_id = c.site_id inner join clusters cl on cl.cluster_id= s.cluster_id";
			$where =$where." and cl.cluster_id = ".$cluster_id." and s.site_status!='Disable'";
		}
		else if ($state_id !=NULL)
		{
			$cond=" inner join sites s on s.site_id = c.site_id inner join clusters cl on cl.cluster_id= s.cluster_id
					 inner join states st on st.state_id=cl.state_id ";
			$where =$where." and st.state_id = ".$state_id." and s.site_status!='Disable'";
			
		}else{
			$cond=" inner join sites s on s.site_id = c.site_id ";
			$where = $where." and s.site_status!='Disable'";
		}
    	    $query="select *,cp.package_id as packages_id from consumers c inner join consumer_packages cp on c.consumer_id=cp.consumer_id inner join packages p on cp.package_id=p.package_id  $cond".$where; 
    	  
		$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
    
	public function updateConsumerByConnectionId($amount, $customer_id)
    {
    	$result =NULL;
     	$data = array(
   			'consumer_act_charge' => $amount,
    	);
    	$where = "consumer_id = " . $customer_id;
    
    	$result = $this->_db_table->update($data,$where);
    	
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
    public function updateConsumerCodeByCustomer_id($keyword,$code, $customer_id)
    {
    	$result =NULL;
    	$data = array(
    			$keyword => $code,
    	);
    	$where = "consumer_id = " . $customer_id;
    
    	$result = $this->_db_table->update($data,$where);
    	 
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
    
    public function GetLateMeterReadingRemainder($role_sites_id)
    {
    	 $query="select consumers.consumer_id,consumers.consumer_name,DATEDIFF(now(),(select timestamp from  meter_readings where consumer_id=consumers.consumer_id
                order by timestamp desc limit 1)) as pending_days from consumers inner join sites
                on sites.site_id= consumers.site_id left outer join packages on consumers.package_id= packages.package_id  
    			where consumers.site_id in (".implode(",", $role_sites_id).")  and (packages.is_postpaid=1 or packages.is_postpaid=2)  and (select count(meter_reading_id) from  meter_readings where consumer_id=consumers.consumer_id) >0 and
                (DATEDIFF(now(),(select timestamp from  meter_readings where consumer_id=consumers.consumer_id
                order by timestamp desc limit 1)) =27 or  DATEDIFF(now(),(select timestamp from  meter_readings where consumer_id=consumers.consumer_id
                order by timestamp desc limit 1)) >=29) and consumers.consumer_status<>'banned'";
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
    
    public function getConsumersWithActDate($month=NULL, $year=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$firstdate=NULL,$secondDate=NULL)
    {
    
    	if($state_id!=NULL){
     		$where="  s.site_status!='Disable' and s.state_id=".$state_id;
    	}
    	elseif ($site_id!=NULL){
    		$where="  s.site_status!='Disable' and s.site_id=".$site_id;
    	}elseif ($role_sites_id!=NULL){
    		$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).")";
    	}else{
    		$where=" s.site_status!='Disable' ";
    	}
    	if($firstdate!=null){
    		$checkDate=" and date(consumer_act_date)>='".$firstdate."' and date(consumer_act_date)<='".$secondDate."'";
    	}else{
    		$checkDate=" and MONTH(consumer_act_date)=".$month." and YEAR(consumer_act_date)=".$year;
    	}
     	 $query = "SELECT * FROM consumers c inner join sites s on s.site_id=c.site_id WHERE ".$where." ".$checkDate." and c.consumer_status!='banned' order by consumer_act_date desc";
    	 
    	$consumer = $this->_db_table->getAdapter()->query($query);
    	$result = $consumer->fetchAll();
    	if( count($result) == 0 ) {
    	return false;
    	}
	    $consumer_object_arr = array();
	    foreach ($result as $row)
	    {
		    $consumer_object = new Application_Model_Consumers();
		    foreach($row as $key=>$value)
		    	{
		    	$consumer_object->__set($key,$value);
		    }
		    array_push($consumer_object_arr,$consumer_object);
	    }
	   return $consumer_object_arr;
   }
    
	public function getConsumersPackageStatus($month=NULL, $year=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$firstdate=NULL,$secondDate=NULL,$groupBy=NULL,$consumer_id=NULL,$banned=null)
   {
   
   	if($state_id!=NULL){
   		 
   		$where="  s.site_status!='Disable' and s.state_id=".$state_id;
   	}
   	elseif ($site_id!=NULL){
   		$where="  s.site_status!='Disable' and s.site_id=".$site_id;
   	}elseif ($role_sites_id!=NULL){
   		$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).")";
   	}else{
   		$where=" s.site_status!='Disable' ";
   	}
   	
   	if($firstdate!=null){
   		$checkDate=" and date(package_change_date)>='".$firstdate."' and date(package_change_date)<='".$secondDate."'";
   	}else{
   		$checkDate=" and MONTH(package_change_date)=".$month." and YEAR(package_change_date)=".$year;
   	}
   	
   	if($groupBy!=NULL){
   		$startCheck="count(p.package_id) as total_package, ";
   		$endCheck=" group by p.package_id,old_package ";
   	}else{
   		$startCheck=" ";
   		$endCheck=" ";
   	}
   	
   	if($consumer_id!=NULL){
   		$consumer_id=" and c.consumer_id=".$consumer_id." ";
   	}else{
   		 $consumer_id=" ";
   	}
   	
   	if($banned!=NULL){
   		$banneds=" and c.consumer_status='".$banned."' ";
   	}else{
   		$banneds=" ";
   	}
   	
    $query = "select ".$startCheck."  p.package_id,p.consumer_id,p.package_change_date,p.package_cost,c.consumer_connection_id,(select p1.package_id from  package_history p1 
    			where p1.package_history_id<p.package_history_id and p1.consumer_id= p.consumer_id and p1.package_change_date !='2015-10-01 06:00:00' 
 			order by p1.package_change_date  desc limit 1) as old_package
			from  package_history p inner join consumers c on c.consumer_id=p.consumer_id inner join sites s 
   			on s.site_id=c.site_id where ".$where.$banneds." and 
			package_change_date !='2015-10-01 06:00:00' ".$consumer_id.$checkDate.$endCheck."
			order by p.package_change_date desc";
   
   	$consumer = $this->_db_table->getAdapter()->query($query);
   	$result = $consumer->fetchAll();
   	if( count($result) == 0 ) {
   		return false;
   	}
   $package_history_object_arr = array();
		foreach ($result as $row)
		{
			$package=new Application_Model_PackagesMapper();
			$packages=$package->getPackageById($row["package_id"]);
			if($packages){
				$package_namess=$packages->__get("package_name");
			}else{
				$package_namess="--deleted-";
			}
			$package_history_object = new Application_Model_PackageHistory($row);
			$package_history_object->__set("package_id", $row["package_id"]);
			$package_history_object->__set("consumer_id", $row["consumer_id"]);
			$package_history_object->__set("package_change_date", $row["package_change_date"]);
			$package_history_object->__set("package_cost", $row["package_cost"]);
			$package_history_object->__set("package_name", $package_namess);
			$package_history_object->__set("old_package_id", $row["old_package"]);
	 
			$package_history_object->__set("consumer_connection_id", $row["consumer_connection_id"]);
				if($groupBy!=NULL){
					$package_history_object->__set("total_package", $row["total_package"]);
				}
			array_push($package_history_object_arr,$package_history_object);
		}
		 
		return $package_history_object_arr;
   }
   
	public function getConsumerStatus($month=NULL, $year=NULL)
   {
   	/* if($state_id!=NULL){
   	
   		$where="  s.site_status!='Disable' and s.state_id=".$state_id;
   	}
   	elseif ($site_id!=NULL){
   		$where="  s.site_status!='Disable' and s.site_id=".$site_id;
   	}elseif ($role_sites_id!=NULL){
   		$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).")";
   	}else{
   		$where=" s.site_status!='Disable' ";
   	} */
   	
   	$query = "select * from tara.logs where ( message like '% banned from active%' or message like '% active from banned%' ) 
   			and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year." order by timestamp desc";
   	//echo $query;
   	$stmt= $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	if (count($result) == 0) {
   		return false;
   	}
   	return $result;
   }
	public function getConnectionIdDetails($connection_id,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL)
   {
   	if($state_id!=NULL){
   
   	$where="  s.site_status!='Disable' and s.state_id=".$state_id;
   	}
   	elseif ($site_id!=NULL){
   	$where="  s.site_status!='Disable' and s.site_id=".$site_id;
   	}elseif ($role_sites_id!=NULL){
   	$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).")";
   	}else{
   	$where=" s.site_status!='Disable' ";
   	} 
   
   
     	$query = "select *  from consumers c inner join sites s on c.site_id=s.site_id where 
   			consumer_connection_id='".$connection_id."' and ".$where;
   	  
   	$stmt= $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	if (count($result) == 0) {
   		return false;
   	}else{
   		return $result[0];
   	}
   	 
   }
   
	public function getConsumerCountByTypeofConsumers($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$type_of_me=NULL,$month=NULL,$year=NULL){
   	if($state_id!=NULL){
   		 
   		$where="  s.site_status!='Disable' and s.state_id=".$state_id;
   	}
   	elseif ($site_id!=NULL){
   		$where="  s.site_status!='Disable' and s.site_id=".$site_id;
   	}elseif ($role_sites_id!=NULL){
   		$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).")";
   	}else{
   		$where=" s.site_status!='Disable' ";
   	}
   	 
   	 $query = "select count(*) as TypeOfConsumers  from consumers c inner join sites s on c.site_id=s.site_id where c.consumer_status!='banned' and type_of_me='".$type_of_me."' and ".$where;
   	 
   	$stmt= $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	if ($result[0]["TypeOfConsumers"] == 0) {
   		return 0;
   	}else{
   		return $result[0]["TypeOfConsumers"];
   	}
   	 
   	}
   	
   	public function getTotalDebitByTypeofConsumers($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$type_of_me=NULL,$month=NULL,$year=NULL){
   		if($state_id!=NULL){
   	
   			$where="  s.site_status!='Disable' and s.state_id=".$state_id;
   		}
   		elseif ($site_id!=NULL){
   			$where="  s.site_status!='Disable' and s.site_id=".$site_id;
   		}elseif ($role_sites_id!=NULL){
   			$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).")";
   		}else{
   			$where=" s.site_status!='Disable' ";
   		}
   		 
   		$query = "select sum(cr.cr_amount) as TotalAmount  from consumers c inner join sites s on c.site_id=s.site_id inner join cash_register cr on cr.consumer_id=c.consumer_id 
   				where c.consumer_status!='banned' and cr.cr_status='Active' and cr.cr_entry_type='Debit' and type_of_me='".$type_of_me."' and ".$where;
   		 
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if ($result[0]["TotalAmount"] == 0) {
   			return 0;
   		}else{
   			return $result[0]["TotalAmount"];
   		}
   		 
   	}
   	public function updateConsumerMicroPriseByConnectionId($amount, $customer_id)
   	{
   		$result =NULL;
   		$data = array(
   				'micro_entrprise_price' => $amount,
   		);
   		$where = "consumer_id = " . $customer_id;
   	
   		$result = $this->_db_table->update($data,$where);
   		 
   		if($result==0)
   		{
   			return false;
   		}
   		else
   		{
   			return true;
   		}
   	}
   	
   	/*public function collectionSheetDetails($consumer_id=NULL,$month=NULL,$year=NULL){
   		 
   		$current_date=$year."-".$month."-01";
   		$query = "SELECT consumer_code,consumer_connection_id,consumer_name,package_id,consumer_act_charge as total_activation,
				 
				(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and 
   				cr_entry_type='ACTIVATION' and cr_status!='INACTIVE' and timestamp<'".$current_date."') as total_paid_activation,
   				(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and 
   				cr_entry_type='ACTIVATION' and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year." and cr_status!='INACTIVE')
   				    as current_activation,
				(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and 
				cr_entry_type='CREDIT' and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year." and cr_status!='INACTIVE') as current_credit,
				(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and cr_entry_type='DEBIT' 
				and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year." and cr_status!='INACTIVE') as current_debit
				FROM consumers c where  c.consumer_id=".$consumer_id;
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result)==0) {
   			return false;
   		}else{
   			return $result[0];
   		}
   	
   	}*/
	
   	public function getConsumersByColumnValuebanned($column,$value)
   	{
   		$where="";
  	 	$where=" and consumer_status ='banned'";
   		$query="select * from consumers inner join sites on consumers.site_id=sites.site_id where consumers.".$column." ='".$value."' $where  and sites.site_status<>'Disable'";
   	
   		$stmt= $this->_db_table->getAdapter();
   		$result = $stmt->fetchAll($query);
   	
   		if(count($result) == 0 ) {
   			return false;
   		}
   		$consumer_object_arr = array();
   		foreach ($result as $row)
   		{
   			$consumer_object = new Application_Model_Consumers();
   			foreach($row as $key=>$value)
   			{
   				$consumer_object->__set($key,$value);
   			}
   			array_push($consumer_object_arr,$consumer_object);
   		}
   	
   		return $consumer_object_arr;
   	}
   	public function collectionSheetDetailsSiteWisebanned($site_id=NULL,$month=NULL,$year=NULL){
   	
   		 $current_date=$year."-".$month."-01";
   		  $query = "SELECT sum(consumer_act_charge) as total_activation,
				 
				(select sum(cr_amount) from cash_register inner join consumers on consumers.consumer_id=cash_register.consumer_id
   				 where consumers.site_id=c.site_id and 
   				cash_register.cr_entry_type='ACTIVATION' and consumers.consumer_status!='banned' and cash_register.cr_status!='INACTIVE' and cash_register.timestamp<'".$current_date."') as total_paid_activation,
   				(select sum(cr_amount) from cash_register inner join consumers on consumers.consumer_id=cash_register.consumer_id
   				 where consumers.site_id=c.site_id and 
   				cash_register.cr_entry_type='ACTIVATION' and consumers.consumer_status!='banned' and MONTH(cash_register.timestamp)=".$month." and YEAR(cash_register.timestamp)=".$year." and cash_register.cr_status!='INACTIVE')
   				    as current_activation,
				(select sum(cash_register.cr_amount) from cash_register inner join consumers on consumers.consumer_id=cash_register.consumer_id
   				 where consumers.site_id=c.site_id and 
				cash_register.cr_entry_type='CREDIT' and MONTH(cash_register.timestamp)=".$month." and YEAR(cash_register.timestamp)=".$year." and cash_register.cr_status!='INACTIVE' and consumers.consumer_status!='banned') as current_credit,
				(select sum(cr_amount) from cash_register inner join consumers on consumers.consumer_id=cash_register.consumer_id
   				 where consumers.site_id=c.site_id and cash_register.cr_entry_type='DEBIT' 
				and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year." and cr_status!='INACTIVE' and consumers.consumer_status!='banned') as current_debit
				FROM consumers c where c.consumer_status!='banned'  and c.site_id=".$site_id;
   		 
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result)==0) {
   			return false;
   		}else{
   			return $result[0];
   		}
   	
   	}
   		public function collectionSheetDetails($consumer_id=NULL,$month=NULL,$year=NULL,$package_id=NULL){
   		 
   		$current_date=$year."-".$month."-01";
   		$packages="";
   		if($package_id!=NULL){
   			$packages=" and package_id=".$package_id;
   		}
   		
   		$query = "SELECT consumer_code,consumer_connection_id,consumer_name,package_id,consumer_act_charge as total_activation,
				 
				(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and 
   				cr_entry_type='ACTIVATION' and cr_status!='INACTIVE' and timestamp<'".$current_date.$packages."') as total_paid_activation,
   				(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and 
   				cr_entry_type='ACTIVATION' and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year.$packages." and cr_status!='INACTIVE')
   				    as current_activation,
				(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and 
				cr_entry_type='CREDIT' and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year.$packages." and cr_status!='INACTIVE') as current_credit,
				(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and cr_entry_type='DEBIT' 
				and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year." and cr_status!='INACTIVE' ".$packages.") as current_debit
				FROM consumers c where  c.consumer_id=".$consumer_id;
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result)==0) {
   			return false;
   		}else{
   			return $result[0];
   		}
   	
   	}
   	
   	public function getConsumerDODatebyConnectionId($connection_id=NULL)
   	{
   		$query = "select * from tara.logs where ( message like '% banned from active%') and message like '%$connection_id%' order by timestamp desc limit 1";
   		//echo $query;
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		return $result[0];
   	}
   	
   	public function getConnectionIdsVal($connection_id,$site_id=NULL)
   	{
   	 
   		 $query = "select *,count(c.package_id) as total_pack from consumers c inner join sites s on c.site_id=s.site_id where
   			consumer_connection_id in (".implode(",", $connection_id).") and  s.site_status!='Disable' and s.site_id=".$site_id." 
   					group by c.consumer_id";
  
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}else{
   			return $result[0];
   		}
   		 
   	}
   	
   		public function getCramountbyconsumerId($consumer_id,$month=NULL,$year=NULL,$package_id=NULL){
   		if($month==1){
   			$firstDate=($year-1)."-12-23";
   		}else{
   			$firstDate=$year."-".($month-1)."-23";
   		}
   		$secondDate=$year."-".$month."-01";
   		$package="";
   		if($package_id!=NULL){
   			$package=" and package_id=".$package_id;
   		}
   		
   	$query = "select sum(cr_amount) as debit_sum from cash_register where consumer_id=".$consumer_id." and cr_entry_type='DEBIT' and
   		date(timestamp)>='".$firstDate."' and date(timestamp)<='".$secondDate."' and cr_status!='INACTIVE'".$package;
   	
   		$stmt= $this->_db_table->getAdapter()->query($query);
	   	$result = $stmt->fetchAll();
	   	if (count($result) == 0) {
	   		return false;
	   	}else{
	   		return $result[0]["debit_sum"];
   	}
   	}
   	
   	public function getConsumersBySiteId($site_id=NULL,$banned=NULL)
   	{
   		if($banned!=NULL){
	   	 	$where=" and c.consumer_status!='banned'";
	   	}else{
	   	 	$where=" and c.consumer_status='banned'";
	   	}
   		 
   		$query="select c.* from consumers c where c.site_id=".$site_id." ".$where." order by c.consumer_connection_id";
    	
   		$stmt= $this->_db_table->getAdapter();
   		$result = $stmt->fetchAll($query);
   		
   		if(count($result) == 0 ) {
   			return false;
   		}
   		$consumer_object_arr = array();
   		foreach ($result as $row)
   		{
   			$consumer_object = new Application_Model_Consumers($row);
   			$consumer_object->__set("consumer_id", $row["consumer_id"]);
   			$consumer_object->__set("package_id", $row["package_id"]);
   			$consumer_object->__set("site_id", $row["site_id"]);
   			$consumer_object->__set("consumer_code", $row["consumer_code"]);
   			$consumer_object->__set("consumer_name", $row["consumer_name"]);
   			$consumer_object->__set("consumer_father_name", $row["consumer_father_name"]);
   			$consumer_object->__set("consumer_status", $row["consumer_status"]);
   			$consumer_object->__set("consumer_act_date", $row["consumer_act_date"]);
   			$consumer_object->__set("consumer_act_charge", $row["consumer_act_charge"]);
   			$consumer_object->__set("is_micro_enterprise", $row ["is_micro_enterprise"]);
   			$consumer_object->__set("meter_start_reading", $row["meter_start_reading"]);
   			$consumer_object->__set("wattage", $row["wattage"]);
   			$consumer_object->__set("micro_entrprise_price", $row["micro_entrprise_price"]);
   			$consumer_object->__set("consumer_connection_id", $row["consumer_connection_id"]);
   			$consumer_object->__set("suspension_date", $row["suspension_date"]);
   			$consumer_object->__set("sms_opt_in", $row["sms_opt_in"]);
   			$consumer_object->__set("enterprise_type", $row["enterprise_type"]);
   			$consumer_object->__set("type_of_me", $row["type_of_me"]);
   			$consumer_object->__set("notes", $row["notes"]);
   			$consumer_object->__set("site_meter_id", $row["site_meter_id"]);
   			$consumer_object->__set("credit_limit", $row["credit_limit"]);
   			array_push($consumer_object_arr,$consumer_object);
   		}
   		
   		return $consumer_object_arr;
   	}
   
     
   	public function getConsumersPackagebySiteId($site_id=NULL)
   	{
   		$query = "SELECT p.* FROM consumers c inner join packages p on c.package_id=p.package_id
   					where c.site_id=".$site_id." group by p.package_id";
   		 
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		return $result;
   	}
   	public function getConsumersDetailbySiteId($site_id=NULL,$month=NULL,$year=NULL)
   	{
   		 $query = "SELECT p.package_name,p.package_id,c.consumer_connection_id,c.consumer_id,count(c.package_id) as total_id FROM consumers 
   				c inner join packages p on c.package_id=p.package_id
   				 where month(c.consumer_act_date)=".$month." and year(c.consumer_act_date)=".$year." 
					and c.site_id=".$site_id."  group by c.package_id;";
   	
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		return $result;
   	}
   	public function getConsumersDetailbypackageId($site_id=NULL,$package_id=NULL,$month=NULL,$year=NULL)
   	{
   		 $query = "SELECT * from consumers where month(consumer_act_date)=".$month." and year(consumer_act_date)=".$year."
					and site_id=".$site_id." and package_id=".$package_id." and consumer_status!='banned'";
   	
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		return $result;
   	}
   	public function getConsumersDetailbypackageSiteId($site_id=NULL,$package_id=NULL,$month=NULL,$year=NULL)
   	{
   		
   		$query = "SELECT * from consumers where month(consumer_act_date)<=".$month." and year(consumer_act_date)<=".$year."
					and site_id=".$site_id." and package_id=".$package_id." and consumer_status!='banned'";
   	
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		return $result;
   	}
   	public function getConsumerCountbySiteId($site_id=NULL,$month=NULL,$year=NULL)
   	{
   		$query = "SELECT * from consumers where month(consumer_act_date)<=".$month." and year(consumer_act_date)<=".$year."
					and site_id=".$site_id." and consumer_status!='banned'";
   	
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return 0;
   		}
   		return count($result);
   	}
   	
   	public function getTotalConsumers($month=NULL,$year=NULL)
   	{
   		$date=$year."-".$month."-31";
   		$query = "SELECT * from consumers inner join sites on sites.site_id=consumers.site_id
   				 where month(consumer_act_date)<=".$month." and year(consumer_act_date)<=".$year."
					and site_status!='Disable' and consumer_status!='banned'";
   	
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return 0;
   		}
   		return count($result);
   	}

   	public function getBannedConsumersWithActDate($month=NULL, $year=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$firstdate=NULL,$secondDate=NULL)
   	{
   	
   		if($state_id!=NULL){
   			$where="  s.site_status!='Disable' and s.state_id=".$state_id;
   		}
   		elseif ($site_id!=NULL){
   			$where="  s.site_status!='Disable' and s.site_id=".$site_id;
   		}elseif ($role_sites_id!=NULL){
   			$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).")";
   		}else{
   			$where=" s.site_status!='Disable' ";
   		}
   		if($firstdate!=null){
   			$checkDate=" and date(consumer_act_date)>='".$firstdate."' and date(consumer_act_date)<='".$secondDate."'";
   		}else{
   			$checkDate=" and MONTH(suspension_date)=".$month." and YEAR(suspension_date)=".$year;
   		}
   	    $query = "SELECT * FROM consumers c inner join sites s on s.site_id=c.site_id WHERE ".$where." ".$checkDate." and c.consumer_status='banned' order by consumer_act_date desc";
    
   		$consumer = $this->_db_table->getAdapter()->query($query);
   		$result = $consumer->fetchAll();
   		if( count($result) == 0 ) {
   			return false;
   		}
   		$consumer_object_arr = array();
   		foreach ($result as $row)
   		{
   			$consumer_object = new Application_Model_Consumers();
   			foreach($row as $key=>$value)
   			{
   				$consumer_object->__set($key,$value);
   			}
   			array_push($consumer_object_arr,$consumer_object);
   		}
   		return $consumer_object_arr;
   	}
   	
   	public function getReactiveConsumerDetails($month=NULL, $year=NULL)
   	{
   		 
        $query = "select * from logs where message like '% active from banned%' 
   			and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year." order by timestamp desc";
   		 
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		return $result;
   	}
   	
   	public function getLastBannedConsumerDetails($month=NULL, $year=NULL,$connection_id=NULL)
   	{
   	
   		   $query = "select * from logs where ( message like '% banned from active%' and message like '%".$connection_id."%' )
   			and MONTH(timestamp)<=".$month." and YEAR(timestamp)<=".$year." order by timestamp desc";
   
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		return $result;
   	}
   	
   	public function getNewConsumerDetails($month=NULL, $year=NULL,$site_id=NULL,$state_id=NULL,$total=null,$banned=NULL,$type=NULL,$suspenstion=NULL,$opening=null)
   	{
		if($state_id!=NULL){
			$where="  s.state_id=".$state_id." and s.site_status!='Disable' ";
		}else if($site_id!=NULL){ 
			$where="  c.site_id=".$site_id." and s.site_status!='Disable' ";
		}
   	
   	if($total!=NULL){
		    if($opening!=null){
			   $suspenstionDates=$year."-".$month."-00";
		   }else{
				$suspenstionDates=$year."-".$month."-31 23:59:59";
		   }
   		
   		if($suspenstion!=NULL){
   			
   			$con=" c.suspension_date<='".$suspenstionDates."'" ;
   		}else{
   			$con=" c.consumer_act_date<='".$suspenstionDates."'" ;
   		}
   		
   	}else {
   		if($suspenstion!=NULL){
   			$con=" month(c.suspension_date)=".$month." and year(c.suspension_date)=".$year." " ;
   		}else{
   			$con=" month(c.consumer_act_date)=".$month." and year(c.consumer_act_date)=".$year." " ;
   		}
   	}
   	
   	if($banned!=NULL){
   		$bannedcon=" c.consumer_status='banned' ";
   	}else {
   		$bannedcon=" c.consumer_status!='banned' ";
   	}

   	if($type!=NULL){
   		if($type==1){
   			$typecon=" and p.is_postpaid in (1,2)" ;
   		}else{
   			$typecon=" and p.is_postpaid=".$type." " ;
   		}
   		
   	}else {
   		$typecon=" ";
   	}

	 $query = "SELECT count(*) as consumerCount FROM consumers as c inner join sites as s on c.site_id=s.site_id 
   				inner join packages p on p.package_id=c.package_id
   				 where s.site_id!=1 and ".$con."	and ".$bannedcon." and ".$where.$typecon;
   		  
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return 0;
   		}
   		return $result[0]["consumerCount"];
   	}
   	
   	public function getConsumerByConnectionIDs($consumer_connection_id)
   	{
   		 
   		$where = array(
   				"consumer_connection_id = ?" => $consumer_connection_id
   		);
   		$result = $this->_db_table->fetchRow($where);
   		if( count($result) == 0 ) {
   			return false;
   		}
   		$row = $result;
   		$consumer = new Application_Model_Consumers($row);
   		return $consumer;
   	}

    public function getConsumersDetailsBySiteId($site_id=NULL,$banned=NULL,$packages=null,$consumer_type=NULL,$package_id=NULL)
    {

        if($banned=='active'){
            $where=" and c.consumer_status!='banned'";
        }elseif($banned=='banned'){
            $where=" and c.consumer_status='banned'";
        }else{
			$where=" ";
		}

        if($packages!=NULL){
            $fixeds=" and p.is_postpaid in ".$packages;
        }else{
            $fixeds=" ";
        }
        $consumer_types="";
        if($consumer_type!=NULL){
        	$consumer_types=" and c.type_of_me in (".implode(",", $consumer_type).")";
        }
        
        $package_ids="";
        if($package_id!=NULL){
        	$package_ids=" and cp.package_id in (".implode(",", $package_id).")";
        }
         
           $query="select c.* from consumers c
                where c.site_id=".$site_id." ".$where.$fixeds.$consumer_types.$package_ids." order by c.consumer_connection_id";

 
        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);

        if(count($result) == 0 ) {
            return false;
        }
        $consumer_object_arr = array();
    	foreach ($result as $row)
   		{
   			$consumer_object = new Application_Model_Consumers($row);
   			$consumer_object->__set("consumer_id", $row["consumer_id"]);
   			$consumer_object->__set("package_id", $row["package_id"]);
   			$consumer_object->__set("site_id", $row["site_id"]);
   			$consumer_object->__set("consumer_code", $row["consumer_code"]);
   			$consumer_object->__set("consumer_name", $row["consumer_name"]);
   			$consumer_object->__set("consumer_father_name", $row["consumer_father_name"]);
   			$consumer_object->__set("consumer_status", $row["consumer_status"]);
   			$consumer_object->__set("consumer_act_date", $row["consumer_act_date"]);
   			$consumer_object->__set("consumer_act_charge", $row["consumer_act_charge"]);
   			$consumer_object->__set("is_micro_enterprise", $row ["is_micro_enterprise"]);
   			$consumer_object->__set("meter_start_reading", $row["meter_start_reading"]);
   			$consumer_object->__set("wattage", $row["wattage"]);
   			$consumer_object->__set("micro_entrprise_price", $row["micro_entrprise_price"]);
   			$consumer_object->__set("consumer_connection_id", $row["consumer_connection_id"]);
   			$consumer_object->__set("suspension_date", $row["suspension_date"]);
   			$consumer_object->__set("sms_opt_in", $row["sms_opt_in"]);
   			$consumer_object->__set("enterprise_type", $row["enterprise_type"]);
   			$consumer_object->__set("type_of_me", $row["type_of_me"]);
   			$consumer_object->__set("notes", $row["notes"]);
   			$consumer_object->__set("site_meter_id", $row["site_meter_id"]);
   			$consumer_object->__set("credit_limit", $row["credit_limit"]);
   			array_push($consumer_object_arr,$consumer_object);
   		}

        return $consumer_object_arr;
    }


    public function updateConsumerByConsumerId(Application_Model_Consumers $consumer)
    {


        $data = array(
            'site_id' => $consumer->__get("site_id"),
            'consumer_code' => $consumer->__get("consumer_code"),
            'consumer_name' => $consumer->__get("consumer_name"),
            'consumer_father_name' => $consumer->__get("consumer_father_name"),
            'type_of_me' => $consumer->__get("type_of_me"),
           // 'package_id' => $consumer->__get("package_id"),
            'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
            'micro_entrprise_price' => $consumer->__get("micro_price"),
            'meter_start_reading' => $consumer->__get("meter_start_reading"),
			'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
			'consumer_status' => $consumer->__get("consumer_status"),
			'credit_limit' => $consumer->__get("credit_limit"),
			'suspension_date' => $consumer->__get("suspension_date"), 
        );

        $where = "consumer_id = " . $consumer->__get("consumer_id");

        $result = $this->_db_table->update($data,$where);
            return true;

    }

	public function getBannedLastConsumerDetails($curr_date=NULL, $last_date=NULL,$site_id=NULL,$state_id=NULL,$type=NULL,$opening=null,$new=null)
   	{
		if($state_id!=NULL){
			$where="  s.state_id in (".$state_id.") and s.site_status!='Disable' ";
		}else if($site_id!=NULL){ 
			$where="  c.site_id in(".$site_id.") and s.site_status!='Disable' ";
		}else{
			$where=" s.site_status!='Disable' ";
		}
   	
   	 
		if($type!=NULL){
			if($type==1){
				$typecon=" and  p.is_postpaid in (1,2)" ;
			}else{ 
				$typecon=" and p.is_postpaid=".$type." " ;
			}
		}else {
			$typecon=" ";
		}

		if($opening!=null){
			$suspenstionDates=$year."-".$month."-01";
			 
		}else{
			$suspenstions=$year."-".$month."-01";
			$suspenstionDates = date('Y-m-d', strtotime('+1 month', strtotime($suspenstions)));
		}
		if($new!=null){
				$sus=" and date(c.consumer_act_date)<='".$curr_date."' and date(c.consumer_act_date)>='".$last_date."' and (date(c.suspension_date)>='".curr_date."' and c.consumer_status ='banned')";
		}else{
				$sus=" and date(c.consumer_act_date)<='".$curr_date."' and date(c.suspension_date)>='".$last_date."' and c.consumer_status ='banned'";
		}
	    $query = "SELECT count(*) as consumerCount FROM consumers as c inner join sites as s on c.site_id=s.site_id 
   				inner join packages p on p.package_id=c.package_id
   				 where ".$where. " ".$typecon.$sus;
   	   
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return 0;
   		}
   		return $result[0]["consumerCount"];
   	}
 
 

	public function getReactiveConsumersValues($connection_id=NULL,$month=NULL, $year=NULL)
   	{
   		 
        $query = "select * from logs where message like '% active from banned%' and message like '%".$connection_id."%'
   			and MONTH(timestamp)=".$month." and YEAR(timestamp)=".$year." order by timestamp desc";
   		 
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		else {
   			$timestamp=$result[0]["timestamp"];
   			$afterNextDate = date('Y-m-d', strtotime($timestamp ."+8 day"));
   			$date = new Zend_Date();
   			$date->setTimezone("Asia/Calcutta");
   			$currenttimestamp = $date->toString("yyyy-MM-dd");
   			if(strtotime($afterNextDate)>strtotime($currenttimestamp)){
   				return $result;
   			}else{
   				return false;
   			}
   			
   		}
   	}
	
	public function  getConsumerCountByFlag()
   	{
   		$query="SELECT count(*) as totalflag from consumers where sms_flag=2";
   		$stmt = $this->_db_table->getAdapter();
   		$result = $stmt->fetchRow($query);
   	
   		if( !$result ) {
   			return 0;
   		}else{
   			return $result["totalflag"];
   		}
   	
   	}
   	
   	public function  getConsumersByFlag()
   	{
   		$query="SELECT * from consumers where sms_flag in (2,3,4) order by flag_date desc";
   		
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		
   		if (count($result) == 0) {
   			return false;
   		}else{
   			return $result;
   		}
   	
   	}
		public function geAllConsumerStatusDetails($state_id=NULL,$site_id=NULL,$connectionId=null,$packageId=null)
   	{
   		if($state_id!=NULL){
   			$where="  s.state_id=".$state_id." and s.site_status!='Disable' ";
   		}else{
   			$where=" s.site_status!='Disable' ";
   		}
   		
   		if($site_id!=NULL){
   			$where.="  and c.site_id=".$site_id;
   		}
   	    
   		if($packageId!=NULL){
   			$package=" and p.package_id=".$packageId;
   		}else {
   			$package=null;
   		}
   		$query = "SELECT * FROM consumers as c inner join consumer_packages cp on c.consumer_id=cp.consumer_id inner join sites as s on c.site_id=s.site_id
   				inner join packages p on p.package_id=c.package_id 
   				 where ".$where. $package." and c.consumer_connection_id='".$connectionId."'";
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return 0;
   		}
   		return $result[0];
   	}
   	
   	public function getPendingConsumers($status=null,$role_sites_id=NULL)
   	{
		$sites="";
		if($role_sites_id!=NULL){
				$sites=" and site_id in (".implode(",", $role_sites_id).")";
		}
   	      $query="select * from consumers where  consumer_status='".$status."' and consumer_first_status='new' ".$sites." order by consumer_id desc";  
         
   		$stmt= $this->_db_table->getAdapter();
   		$result = $stmt->fetchAll($query);
   	
   		if(count($result) == 0 ) {
   			return false;
   		}
   		$consumer_object_arr = array();
   		foreach ($result as $row)
   		{
   			$consumer_object = new Application_Model_Consumers();
   			foreach($row as $key=>$value)
   			{
   				$consumer_object->__set($key,$value);
   			}
   			array_push($consumer_object_arr,$consumer_object);
   		}
   	
   		return $consumer_object_arr;
   	}
   	
	public function getAllPendingConsumers($role_sites_id=NULL)
   	{
   		$query="select (select count(*) from consumers where consumer_status='active' and consumer_first_status='new' and site_id in (".implode(",", $role_sites_id).")) as totalactive,
				(select count(*) from consumers where consumer_status='new' and consumer_first_status='new' and site_id in (".implode(",", $role_sites_id).")) as totalnew,
				(select count(*) from consumers where consumer_status='banned' and consumer_first_status='new' and site_id in (".implode(",", $role_sites_id).")) as totalbanned from  consumers";
   		 
   		$stmt= $this->_db_table->getAdapter();
   		$result = $stmt->fetchAll($query);
   	
   		if(count($result) == 0 ) {
   			return false;
   		}
   		
   		return $result[0];
   	}
   	
   	public function deletePendingConsumerById($consumer_id,$remark=null,$all=null)
   	{
   		$db_table = new Application_Model_DbTable_DeletedConsumers();

   		$data=array(
   				"consumer_status"=>'inactive',
   				"remark"=>$remark
   		);
		if($all){
		   		$where = "consumer_id = " . $consumer_id;
		}else{
				$where = "consumer_id in ( ".$consumer_id.")";
		}
	
   		$result = $db_table->update($data,$where);
   		if($result==0)
   		{
   			return false;
   		}
   		else
   		{
   			return true;
   		}
    }
    
    public function getAllPendingConsumerById($con_id){
    	     $query="SELECT * from consumers where consumer_id in (".$con_id.") and consumer_first_status='new'";
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	$consumer_object_arr = array();
    	 
    	if (count($result) == 0) {
    		return false;
    	}
    	foreach ($result as $key => $row) {
    		 
    		$consumer_object = new Application_Model_Consumers($row);
    		foreach($row as $key=>$value)
    		{
    			$consumer_object->__set($key,$value);
    		}
    		array_push($consumer_object_arr, $consumer_object);
    		 
    	}
    	return $consumer_object_arr;
    }
     public function getConsumerType()
   	{
   		$query="Select * from consumer_types";
   		 
   		$stmt= $this->consumer_type_db_table->getAdapter();
   		$result = $stmt->fetchAll($query);
   	
   		if(count($result) == 0 ) {
   			return false;
   		}
   		
   		return $result;
   	}
 
   	
   	public function getConsumerTypeById($consumer_type_id)
   	{
   		 $query="select * from consumer_types where consumer_type_id = '".$consumer_type_id."'";
   		$stmt= $this->consumer_type_db_table->getAdapter();
   		$result = $stmt->fetchAll($query);
   		
   		if(count($result) == 0 ) {
   			return false;
   		}
   		 
   		return $result[0];
   		
   	}
   	
   	
   	public function getConnectionId($site_code, $pole_no){
   	
   		$query = " select max(SUBSTRING(consumer_connection_id, 10)) as connection_no  from consumers where consumer_connection_id like '%".$site_code.$pole_no."%'";
   	
   	
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if(count($result) == 0 ) {
   			return false;
   		}
   		 
   		return $result[0]["connection_no"];
   		}
   		
   	
     
    public function getConsumerByPackageId($package_id=null,$consumer_type=null){
    	$consumer_types="";
    	if($consumer_type!=null){
    		$consumer_types=" and type_of_me in (".$consumer_type.")";
    	}
    	  $query="Select * from consumers where package_id in (".implode(",", $package_id).") and consumer_status!='banned'".$consumer_types;
    	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    } 
    
    public function getConsumerBySomeDetails($consumers=null,$consumer_type=null,$packages=null,$package_type=null,$sites=null,$state=null,$scheme_type=null){
    	
    	$groupby="";
    	
    	if($consumers!=null){
    		$where=" where consumers.consumer_id in (".$consumers.")";
    	}elseif ($consumer_type!=null){
    		$type="  where type_of_me in (".$consumer_type.")";
    	}elseif ($packages!=null){
    		$where=" where consumer_packages.package_id in (".$packages.")";
    		$groupby=" group by consumers.consumer_id ";
    	}elseif ($package_type!=null){
    		$where=" where packages.is_postpaid in (".$package_type.")";
    	}elseif ($sites!=null){
    		$where=" where consumers.site_id in (".$sites.")";
    	}elseif($state!=null){
    		$where=" where sites.state_id in (".$state.")";
    	}else{
    		$where=NULL;
    	}
		
    	if($scheme_type=='equipment'){
    		$where=NULL;
    	}
		
    	if($where==NULL){
    		return false;
    	}
    	
         $query="Select * from consumers inner join consumer_packages on consumers.consumer_id=consumer_packages.consumer_id 
        		inner join sites on consumers.site_id=sites.site_id 
        		inner join packages on consumers.package_id=packages.package_id
        		".$where."  and consumers.consumer_status!='banned' and packages.status!='disable'".$groupby;
     
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
    public function updateSchemeStatusById($consumer_id=null,$status=null)
    {
    	$data=array(
    			"scheme_apply"=>$status,
    	);
    		
    	$where = "consumer_id = ".$consumer_id;
    		
    	$result = $db_table->update($data,$where);
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
	
	public function getConsumerTypeByName($consumer_type_name)
    {
    	$query="select * from consumer_types where consumer_type_name = '".$consumer_type_name."'";
    	$stmt= $this->consumer_type_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    	 
    	if(count($result) == 0 ) {
    		return false;
    	}
     
    	return $result[0];
    	 
    }
	
	public function getMonthlyConsumerDetails($month=NULL,$year=NULL,$status=NULL,$site_id=NULL){
    	 
    	$db_table = new Application_Model_DbTable_DeletedConsumers();
    	
    	if($status=='pending'){
    		$table="consumers";
    		$where=" and consumer_status!='banned' and consumer_first_status='new' and  month(consumer_act_date)=".$month." and year(consumer_act_date)=".$year." order by consumer_act_date ";
    	}elseif($status=='active'){
    		$table="consumers";
    		$where=" and consumer_status!='banned' and  month(consumer_act_date)=".$month." and year(consumer_act_date)=".$year." order by consumer_act_date ";
    	}elseif($status=='banned'){
    		$table="consumers";
    		$where="  and consumer_status='banned' and month(suspension_date)=".$month." and year(suspension_date)=".$year." order by suspension_date" ;
    	}else{
    		$table="deleted_consumers";
    		$where="  and consumer_status='inactive' and month(suspension_date)=".$month." and year(suspension_date)=".$year." order by suspension_date" ;
    	}
    	 
    	  $query="Select * from ".$table."  where site_id in (".implode(",", $site_id).") ".$where;
    	
    	if($status=='inactive'){
    		$stmt= $db_table->getAdapter()->query($query);
    	}else{
    		$stmt= $this->_db_table->getAdapter()->query($query);
    	}
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
    
    public function getDailyConsumerDetails($day=NULL,$month=NULL,$year=NULL,$status=NULL,$site_id=NULL){
    
    	$db_table = new Application_Model_DbTable_DeletedConsumers();
    	 
    	if($status=='pending'){
    		$table="consumers";
    		$where=" and consumer_status!='banned' and consumer_first_status='new' and  month(consumer_act_date)=".$month." and year(consumer_act_date)=".$year." order by consumer_act_date ";
    	}elseif($status=='active'){
    		$table="consumers";
    		$where="  and consumer_status!='banned' and day(consumer_act_date)=".$day." and month(consumer_act_date)=".$month." and year(consumer_act_date)=".$year." order by consumer_act_date ";
    	}elseif($status=='banned'){
    		$table="consumers";
    		$where="  and consumer_status='banned' and day(suspension_date)=".$day." and month(suspension_date)=".$month." and year(suspension_date)=".$year." order by suspension_date" ;
    	}else{
    		$table="deleted_consumers";
    		$where="  and consumer_status='inactive' and day(suspension_date)=".$day." and month(suspension_date)=".$month." and year(suspension_date)=".$year." order by suspension_date" ;
    	}
    	 
    	  $query="Select * from ".$table." where site_id in (".implode(",", $site_id).") ".$where;
    
    	if($status=='inactive'){
    		$stmt= $db_table->getAdapter()->query($query);
    	}else{
    		$stmt= $this->_db_table->getAdapter()->query($query);
    	}
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
    public function getConsumerByFilter($state_id=NULL,$site_id=NULL,$package_type=NULL,$package_id=NULL,$consumer_id=NULL,$type=NULL,$role_site=NULL){
    	
    	if($consumer_id!=NULL){
    		$where=" where c.consumer_id in (".implode(",", $consumer_id).")";
    	}elseif($package_id!=NULL){
    		$where=" where cp.package_id in (".implode(",", $package_id).")";
    	}elseif($package_type!=NULL && $site_id!=NULL){
    		$where=" where p.is_postpaid in (".implode(",", $package_type).") and c.site_id in (".implode(",", $site_id).")";
    	}elseif($package_type!=NULL){
    		$where=" where p.is_postpaid in (".implode(",", $package_type).")";
    	}elseif($site_id!=NULL){
    		$where=" where c.site_id in (".implode(",", $site_id).")";
    	}elseif($state_id!=NULL){
    		$where=" where s.state_id in (".implode(",", $state_id).")";
    	}else{
			$where=" where s.site_id in (".implode(",", $role_site).")";
		}
    	$types="";
    	if($type!=NULL){
    		$types="and p.is_postpaid!=0";
    	}
    	    $query="select *,cp.package_id as package_val from consumers c inner join consumer_packages cp on cp.consumer_id=c.consumer_id 
    			inner join packages p on p.package_id=cp.package_id inner join sites s on s.site_id=c.site_id
    			  ".$where ." ".$types. " order by c.consumer_connection_id asc"; 
    	 
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    	
    	if(count($result) == 0 ) {
    		return false;
    	}
    	
    	return $result;
    }
	public function getMonthlyConsumerDetailsForReading($month=NULL,$year=NULL,$status=NULL,$site_id=NULL,$ispostpaids=NULL){
    	 
    	 
    	$where="";
    	if($status=='active'){
    		$table="consumers";
    		$where="  and consumer_status!='banned' order by consumer_act_date ";
    	}elseif($status=='banned'){
    		$where="  and consumer_status='banned' and month(suspension_date)=".$month." and year(suspension_date)=".$year." order by suspension_date" ;
    	} 
    	$ispostpaid="";
    	if($ispostpaids!=NULL){
    		$ispostpaid=" and  p.is_postpaid!=0";
    	}
    	  $query="Select c.* from consumers c inner join consumer_packages cp on cp.consumer_id=c.consumer_id 
    			inner join packages p on p.package_id=cp.package_id where c.site_id in (".implode(",", $site_id).") ".$ispostpaid.$where;
    	
    	 
    		$stmt= $this->_db_table->getAdapter()->query($query);
    	 
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
	
	  public function getConsumersWithSuspenDate($currDate=NULL, $lastDate=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL)
    {
    	 
    	if ($site_id!=NULL){
    		$where =" s.site_id in (".$site_id.")";
    	}else if($state_id!=NULL){
    		$where  =" s.state_id in (".$state_id. ") and s.site_id in (".implode(",", $role_sites_id).")";
    	}else if ($role_sites_id!=NULL){
    		$where  =" s.site_id in (".implode(",", $role_sites_id).")";
    	}
     
    	
    	$checkDate=" and date(suspension_date)<='".$currDate."' and date(suspension_date)>='".$lastDate."' and consumer_status='banned'";
    	 
     
    	  $query = "SELECT * FROM consumers c inner join sites s on s.site_id=c.site_id WHERE ".$where.$checkDate;
		  
    	$consumer = $this->_db_table->getAdapter()->query($query);
    	$result = $consumer->fetchAll();
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$consumer_object_arr = array();
    	foreach ($result as $row)
    	{
    		$consumer_object = new Application_Model_Consumers();
    		foreach($row as $key=>$value)
    		{
    			$consumer_object->__set($key,$value);
    		}
    		array_push($consumer_object_arr,$consumer_object);
    	}
    	return $consumer_object_arr;
    }
	
	public function getConsumerDetailsByFilters($state_id=NULL,$site_id=NULL,$package_type=NULL,$package_id=NULL,$status=NULL,$wattage=NULL,
    $gender=NULL,$feeder=NULL,$equipment_type=NULL,$scheme=NULL,$consumer_type=NULL,$currDate=NULL,$lastDate=NULL,$role_sites_id=NULL,$no_of_packages=NULL){
    	
    	$join="inner join consumer_packages cp on cp.consumer_id=c.consumer_id 
    		   inner join packages p on p.package_id=cp.package_id 
    		   inner join sites s on s.site_id=c.site_id";
    	
    	$where="";
    	if($package_id!=NULL){
    		$where=" where cp.package_id in (".implode(",", $package_id).")";
    	}elseif($package_type!=NULL && $site_id!=NULL){
    		$where=" where p.is_postpaid in (".implode(",", $package_type).") and c.site_id in (".implode(",", $site_id).")";
    	}elseif($package_type!=NULL){
    		$where=" where p.is_postpaid in (".implode(",", $package_type).")";
    	}elseif($site_id!=NULL){
    		$where=" where c.site_id in (".implode(",", $site_id).")";
    	}elseif($state_id!=NULL){
    		$where=" where s.state_id in (".implode(",", $state_id).")";
    	}else{
    		$where=" where s.site_id in (".implode(",", $role_sites_id).")";
    	}
    	$where.=" and s.site_id!=1 ";
    	if($wattage!=NULL){
    		$join.="inner join wattage w on w.watt_id=p.wattage";
    		$where.=" and p.wattage in (".implode(",", $wattage).")";
    	}
    	if($status!=NULL){
    		$where.=" and c.consumer_status in (".implode(",", $status).")";
    	}
    	if($gender!=NULL){
    		$where.=" and c.gender in (".implode(",", $gender).")";
    	}
    	if($consumer_type!=NULL){
    		$where.=" and c.type_of_me in (".implode(",", $consumer_type).")";
    	}
    	if($feeder!=NULL){
    		$join.="inner join site_meter sm on sm.id=c.site_meter_id";
    		$where.=" and sm.feeder_type in (".implode(",", $feeder).")";
    	}
    	if($equipment_type!=NULL){
    		$join.="inner join celamed ce on ce.consumer_id=c.consumer_id";
    		$where.=" and sm.equipment_type in (".implode(",", $equipment_type).")";
    	}
    	if($scheme!=NULL){
    		$join.="inner join consumer_scheme cm on cm.consumer_id=c.consumer_id";
    		$where.=" and cm.consumer_scheme in (".implode(",", $scheme).")";
    	}
    	if($currDate!=NULL && $lastDate!=NULL){
    		$where.=" and date(consumer_act_date)<='".$currDate."' and date(consumer_act_date)>='".$lastDate."'";
    	}
    	if($no_of_packages!=NULL){
    		$where.=" group by cp.consumer_id having count(cp.package_id)>=".$no_of_packages." ";
    	}
    	  $query="select c.*,s.*,cp.package_id as package_val from consumers c ".$join."  ".$where ; 
    	 
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    	
    	if(count($result) == 0 ) {
    		return false;
    	}
    	
    	return $result;
    } 
	
	public function getDeletedConsumerDetails($currDate=NULL,$lastDate=NULL){
    
    	$db_table = new Application_Model_DbTable_DeletedConsumers();
     
    	$query="Select * from deleted_consumers where consumer_status='inactive' and date(suspension_date)<='".$currDate."' 
    			and date(suspension_date)>='".$lastDate."'";
     	$stmt= $db_table->getAdapter()->query($query);
    	 
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    } 
	
	public function getMonthlyWattageFilter($month=NULL,$year=NULL){
    
    	 
    	$query="Select * from consumers where wattage_filter =1 and month(consumer_act_date)=".$month." and year(consumer_act_date)=".$year;
     	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	 
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
	
	public function getSiteWiseConsumerTypeDetails($site_id=NULL,$month=NULL,$year=NULL){
    
    	$join="inner join consumer_packages cp on cp.consumer_id=c.consumer_id
    		   inner join packages p on p.package_id=cp.package_id
    		   inner join sites s on s.site_id=c.site_id";
    	
			
		  $date_31=$year."-".$month."-31";
    	    $query="Select (Select count(*) from consumers c ".$join." 
    						where c.site_id=".$site_id." and is_postpaid=0 and ((date(consumer_act_date)<='".$date_31."' and consumer_status!='banned' ) or 
							(month(suspension_date)=".$month." and year(suspension_date)=".$year." and consumer_status='banned'))) as fixed,
    					(Select count(*) from consumers c ".$join." 
    						where c.site_id=".$site_id." and is_postpaid in (1,2) and ((date(consumer_act_date)<='".$date_31."' and consumer_status!='banned' ) or 
							(month(suspension_date)=".$month." and year(suspension_date)=".$year." and consumer_status='banned'))) as metered
    			from consumers limit 1"; 
   
    	$stmt= $this->_db_table->getAdapter()->query($query);
    
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
    
    public function getConsumerByTypeDetails($site_id=NULL,$ispostpaid=null,$month=NULL,$year=NULL){
		
		$date_31=$year."-".$month."-31";
    	$join="inner join consumer_packages cp on cp.consumer_id=c.consumer_id
    		   inner join packages p on p.package_id=cp.package_id";
    	
    	    $query=" Select c.* from consumers c ".$join." where c.site_id=".$site_id." and 
    				is_postpaid in (".$ispostpaid.") and ((date(consumer_act_date)<='".$date_31."' and consumer_status!='banned' ) or 
							(month(suspension_date)=".$month." and year(suspension_date)=".$year." and consumer_status='banned'))";
    	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	
    	$result = $stmt->fetchAll();
    	
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
	public function getConsumerByType($sites=NULL,$month=NULL,$year=NULL){
    
    	  $query=" Select c.* from consumers c inner join consumer_packages cp on cp.consumer_id=c.consumer_id
    		   inner join packages p on p.package_id=cp.package_id where p.is_postpaid in (1,2) and  c.consumer_status!='banned'   and c.site_id in (".implode(",", $sites).") and c.site_id!=1";
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	 
    	$result = $stmt->fetchAll();
    	
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
	
	public function updateStatus($consumer_connection_id=NULL,$status=NULL,$timestamp=NULL,$remark=NULL,$user_id=NULL,$create_type=NULL){
    	 
    
    	$data = array(
    			'consumer_status' => $status,
    			'consumer_first_status' => "new",
    			'suspension_date' => $timestamp,
    			'reason_remark' => $remark,
    			'created_by' => $user_id,
    			'create_type' => $create_type,
    	);
    	$where = "consumer_connection_id = '" . $consumer_connection_id."'";
    
    	$result = $this->_db_table->update($data,$where);
    	 
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
	
	public function getAllBillsByConsumerId($role_sites_id=NULL,$consumer_id=NULL)
		{
			 
			$site="s.site_id in (".implode(",", $role_sites_id).")";
			 
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
					inner join sites s on c.site_id=s.site_id where ".$site." and c.consumer_id=".$consumer_id." and cr.cr_status!='INACTIVE'";
		
			 	
			$query=" select (select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='DEBIT') as BILL,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='CHG' ) as CHG,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='ACT' ) as ACT,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='EED' ) as EED,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='MED' ) as MED,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='WATER' ) as WATER,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='OTHERS' ) as OTHERS
		
							from cash_register limit 1";
		
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
	  public function getConsumerTotalConnection($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL){
    
    	$date_01=$year."-".$month."-01";
    	$date_07=$year."-".$month."-07";
    	$date_14=$year."-".$month."-14";
    	$date_21=$year."-".$month."-21";
    	$date_31=$year."-".$month."-31";
    	
    	$month_details="(month(consumer_act_date)=".$month." and year(consumer_act_date)=".$year." and consumer_status!='banned')";
    	
    	$month_new_01="(date(consumer_act_date)<='".$date_31."' and consumer_status!='banned')";
    	$month_new_07="(date(consumer_act_date)>='".$date_01."' and date(consumer_act_date)<='".$date_07."' and consumer_status!='banned')";
    	$month_new_14="(date(consumer_act_date)>='".$date_07."' and date(consumer_act_date)<='".$date_14."' and consumer_status!='banned')";
    	$month_new_21="(date(consumer_act_date)>='".$date_14."' and date(consumer_act_date)<='".$date_21."' and consumer_status!='banned')";
    	$month_new_28="(date(consumer_act_date)>='".$date_21."' and date(consumer_act_date)<='".$date_31."' and consumer_status!='banned')";
    	
    	$month_banned_07="(date(suspension_date)>='".$date_01."' and date(suspension_date)<='".$date_07."' and consumer_status='banned')";
    	$month_banned_14="(date(suspension_date)>='".$date_07."' and date(suspension_date)<='".$date_14."' and consumer_status='banned')";
    	$month_banned_21="(date(suspension_date)>='".$date_14."' and date(suspension_date)<='".$date_21."' and consumer_status='banned')";
    	$month_banned_28="(date(suspension_date)>='".$date_21."' and date(suspension_date)<='".$date_31."' and consumer_status='banned')";
    	
    	        $query=" Select (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_details." ) as new_consumer,
    	  				  
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_new_01." ) as new_consumer_01,
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_new_07." ) as new_consumer_07,
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_new_14." ) as new_consumer_14,
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_new_21." ) as new_consumer_21,
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_new_28." ) as new_consumer_28,
    					  
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_banned_07." ) as banned_consumer_07,
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_banned_14." ) as banned_consumer_14,
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_banned_21." ) as banned_consumer_21,
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and ".$month_banned_28." ) as banned_consumer_28,
    					  
    	  				  (select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and 
    					  		((date(consumer_act_date)<'".$date_01."' and consumer_status!='banned') or  
								(date(suspension_date)>'".$date_31."' and date(consumer_act_date)<='".$date_31."' and consumer_status ='banned'))) as total_consumer,
								(select count(*) from consumers where site_id=".$site_id." and type_of_me=".$consumer_type_id." and 
    					  		((date(consumer_act_date)<='".$date_31."' and consumer_status!='banned') or  
								(date(suspension_date)>'".$date_31."' and date(consumer_act_date)<='".$date_31."' and consumer_status ='banned'))) as total_consumer_all
    			   from consumers limit 1";
		 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
    
    public function getConsumerWattageByType($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL){
    
		$date_01=$year."-".$month."-01";
    	$date_31=$year."-".$month."-31";
		
    	$join="inner join consumer_types ct on c.type_of_me=ct.consumer_type_id
	            inner join consumer_packages cp on cp.consumer_id=c.consumer_id
	            inner join packages p on p.package_id=cp.package_id
				inner join wattage w on w.watt_id=p.wattage";
    	
    	$month_details=" ((date(consumer_act_date)<='".$date_31."' and consumer_status!='banned') or  
								(date(suspension_date)>'".$date_31."' and date(consumer_act_date)<='".$date_31."' and consumer_status ='banned'))";
    	 
    	  $query=" Select c.consumer_id,c.consumer_connection_id,ct.consumer_type_name,c.site_meter_id,cp.package_id,p.is_postpaid,w.wattage as wattage from consumers c ".$join." where ".$month_details." and c.type_of_me=".$consumer_type_id." and c.site_id=".$site_id;
     
    	$stmt= $this->_db_table->getAdapter()->query($query);
    
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }	 
	 
	public function getAllConsumerByProductiveLoad($month=NULL,$year=NULL,$role_sites_id=NULL)
    {
		$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
    	$timestamp=$year."-".$month."-".$total_days_in_month;
		
    	$date=" and date(consumer_act_date)<='".$timestamp."'";
    	$site=" Where c.site_id in (".implode(",", $role_sites_id).")";
    	
    	$query = "select * from consumers c inner join sites s on c.site_id=s.site_id ".$site.$date." order by consumer_connection_id ASC";
     
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
	public function getAllConsumerTable() {
    	$result = $this->_db_table->fetchAll(null, array('consumer_id DESC'));
    	if (count($result) == 0) {
    		return false;
    	}
    	$Consumers_object_arr = array();
    	foreach ($result as $row) {
    		$Consumers_object = new Application_Model_Consumers($row);
    		array_push($Consumers_object_arr, $Consumers_object);
    	}
    	return $Consumers_object_arr;
    }
	
	 public function getTotalConsumerBySegmentMonth($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL,$is_postpaid=NULL){
    	
    	$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
    	$date=$year."-".$month."-".$total_days_in_month;
    	
    	$join=" inner join consumer_packages cp on cp.consumer_id=c.consumer_id 
	            inner join packages p on p.package_id=cp.package_id";
    	
    	$month_details=" c.type_of_me=".$consumer_type_id." and c.site_id=".$site_id." and 
    					((date(c.consumer_act_date)<='".$date."' and c.consumer_status!='banned') or 
    					(date(c.consumer_act_date)<='".$date."' and date(c.suspension_date)>'".$date."' and c.consumer_status='banned'))";
    	if($is_postpaid!=NULL){
    		$is_post=" and p.is_postpaid=0";
    	}else{
    		$is_post=" and p.is_postpaid!=0";
    	}
    	 
    	$query=" Select c.*,p.* from consumers c ".$join." where ".$month_details.$is_post;
    
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	
    	$result = $stmt->fetchAll();
    	
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result; 
    }
	
	 public function getConsumerForConsumerDataReport($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL){
    
    	$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
    	
    	$date_01=$year."-".$month."-01";
    	 
    	$date_31=$year."-".$month.$total_days_in_month;
    	 
    	$month_details_active="(month(consumer_act_date)=".$month." and year(consumer_act_date)=".$year." and consumer_status!='banned')";
    	
    	$month_details_banned="(month(suspension_date)=".$month." and year(suspension_date)=".$year." and consumer_status='banned')";
    	 
    	$All_con="((date(consumer_act_date)<'".$date_01."' and consumer_status!='banned') or 
    			(date(consumer_act_date)<'".$date_01."' and date(suspension_date)>'".$date_31."' and consumer_status='banned'))";
    	 
    	  $query=" Select 
	    			(select count(*) from consumers where site_id=".$site_id." and 
	    					type_of_me=".$consumer_type_id." and ".$month_details_active." ) as new_consumer,
	    	  
	    	  		(select count(*) from consumers where site_id=".$site_id." and 
	    	  				type_of_me=".$consumer_type_id." and ".$month_details_banned." ) as banned_consumer,
	    	  			
	    	  		(select count(*) from consumers where site_id=".$site_id." and 
	    	  				type_of_me=".$consumer_type_id." and ".$All_con." ) as all_consumer
    	  						
    	  		from consumers limit 1";
    		 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
	
	public function updateConsumerTypeStatus($status=NULL,$consumer_id=NULL)
    {
    
    	$query ="update consumers set type=".$status." where consumer_id =".$consumer_id;
    	 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    
    	if($result)
    	{
    		return true;
    	}
    }
	
}



