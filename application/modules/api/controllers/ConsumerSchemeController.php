<?php

class Api_ConsumerSchemeController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
		$auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
        $this->_userId=$auth->getIdentity()->user_id;
    }
    
    public function addConsumerSchemeAction() {
     
    	try {
    		$request = $this->getRequest();
    		$consumer_scheme = $request->getParam("consumer_scheme");
    		$consumer_id = $request->getParam("consumer_id");
    		$cluster_id = $request->getParam("cluster_id");
    		$site_id = $request->getParam("site_id");   
    		$package_continue = $request->getParam("package_continue");
    		$scheme_continue = $request->getParam("scheme_continue");
    		$assign_date = $request->getParam("assign_date");
    		$consumer_schememapper = new Application_Model_ConsumerSchemeMapper();
			$consumer_package=new Application_Model_ConsumersPackageMapper();
    		$SchemesMapper=new Application_Model_SchemesMapper();
			$packageMapper=new Application_Model_PackagesMapper();
			$siteMapper=new Application_Model_SitesMapper();
			$sites=$siteMapper->getSiteById($site_id);
    		$scheme=$SchemesMapper->getSchemeById($consumer_scheme);
			$zendDate = new Zend_Date($assign_date,"dd-MM-yyyy HH:mm");
    		$assign_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");  
    		
				$equip_month_dis=NULL;
				if($scheme->__get("scheme_type")=="equipment"){
    				$total_month=$scheme->__get("emi_duration");
					$equip_month_dis=$scheme->__get("discount_month");
    			}elseif ($scheme->__get("gen_scheme_type")=="advance_pay"){
    				$total_month=$scheme->__get("advance_payment")+$scheme->__get("advance_dis_month");
    			}
    			
				$consumer_schemes = new Application_Model_ConsumerScheme();
				$consumer_schemes->__set("consumer_scheme", $consumer_scheme);
				$consumer_schemes->__set("consumer_id", $consumer_id);
				$consumer_schemes->__set("cluster_id", $sites->__get("cluster_id"));
				$consumer_schemes->__set("site_id", $site_id);
				$consumer_schemes->__set("package_continue", $package_continue);
				$consumer_schemes->__set("scheme_continue", $scheme_continue);
				$consumer_schemes->__set("total_month", $total_month);
				$consumer_schemes->__set("equip_month_dis", $equip_month_dis);
				$consumer_schemes->__set("assign_date", $assign_date); 
				
				$consumer_schemes_id = $consumer_schememapper->addNewConsumerScheme($consumer_schemes);
				if($consumer_schemes_id){
						if($scheme->__get("scheme_type")=="equipment"){
							$consumerPack=$consumer_package->getPackageByConsumerId($consumer_id);
							$package_cost=$scheme->__get("down_payment");
							$date = new Zend_Date($assign_date, 'yyyy-MM-dd HH:mm:ss'); 
							$date->setTimezone("Asia/Kolkata"); 
							
							$date_si = new Zend_Date();
							$date_si->setTimezone("Asia/Kolkata");
							$current_date_si=$date_si->toString("yyyy-MM-dd HH:mm:ss");
							$timestamp = $date_si->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9)); 
							$cr_amount = str_pad($package_cost, 4, "0", STR_PAD_LEFT);
							$transaction_id = $timestamp . "-" . $cr_amount;
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$cashRegister = new Application_Model_CashRegister();
							$cashRegister->__set("user_id",$this->_userId);
							$cashRegister->__set("consumer_id", $consumer_id);
							$cashRegister->__set("cr_entry_type", "DEBIT");
							$cashRegister->__set("cr_amount", $package_cost);
							$cashRegister->__set("receipt_number", 0);
							$cashRegister->__set("transaction_id", $transaction_id);
							$cashRegister->__set("transaction_type", "(W)");
							$cashRegister->__set("timestamp", $current_date_si); 
							$cashRegister->__set("entry_status", "EED");
							$cashRegister->__set("remark", "Downpayment");
							$cashRegister->__set("package_id", $consumerPack[0]["package_id"]);
							$cashRegisterMapper->addNewCashRegister($cashRegister); 
							
							$start_date=$assign_date;
							$date_s = new Zend_Date();
							$date_s->setTimezone("Asia/Kolkata");
							$current_date=$date_s->toString("yyyy-MM-dd");
							
							$discount_month = $scheme->__get("discount_month");
							$down_payment = $scheme->__get("down_payment");
							$emi_amt = $scheme->__get("emi_amt");
							$emi_duration = $scheme->__get("emi_duration");
							$package_id = $scheme->__get("package_id");
			
							while(strtotime($start_date)<=strtotime($current_date)){
								
								
								$date = new Zend_Date($start_date,"dd-MM-yyyy HH:mm:ss");
								  
								if ($total_month>0){
									$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
									$cr_amount = str_pad($emi_amt, 4, "0", STR_PAD_LEFT);
									$transaction_id = $timestamp . "-" . $cr_amount;
									
									$cashRegister = new Application_Model_CashRegister();
									$cashRegister->__set("user_id", 0);
									$cashRegister->__set("consumer_id", $consumer_id);
									$cashRegister->__set("cr_entry_type", "DEBIT");
									$cashRegister->__set("cr_amount", $emi_amt);
									$cashRegister->__set("receipt_number", 0);
									$cashRegister->__set("transaction_id", $transaction_id);
									$cashRegister->__set("transaction_type", "(S)");
									$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd 06:50:00"));
									$cashRegister->__set("entry_status", "EED");
									$cashRegister->__set("remark", "Generate EMI from CRON"); 
									$cashRegister->__set("package_id", NULL);
									$CRs=$cashRegisterMapper->addNewCashRegister($cashRegister);
									
									$total_month=$total_month-1;
									$consumer_schememapper->updateConsumerTotalMonth($total_month,$consumer_schemes_id,'total_month');
								}
								
								if($equip_month_dis>0){ 
									$package_data=explode(",", $package_id);
									for ($i=0;$i<count($package_data);$i++){
										$type=array("CREDIT","DEBIT");
										for ($j=0;$j<count($type);$j++){
											 
											$packages=$packageMapper->getPackageById($package_data[$i]);
											$package_costs=$packages->__get("package_cost");
											$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
											$cr_amount = str_pad($package_costs, 4, "0", STR_PAD_LEFT);
											$transaction_id = $timestamp . "-" . $cr_amount;
											if($type[$j]=='CREDIT'){
												$entry_type='SCHEME';
												$remark='DISCOUNT on scheme package';
												$date = new Zend_Date($start_date,"dd-MM-yyyy HH:mm:ss");  
												$timestamp_val= $date->toString("yyyy-MM-dd 10:15:00");
											}else{
												$entry_type='SCHEME';
												$remark='Bill on scheme package';
												$date = new Zend_Date($start_date,"dd-MM-yyyy HH:mm:ss"); 
												$timestamp_val= $date->toString("yyyy-MM-dd 10:10:00");  
											} 
											$cashRegister = new Application_Model_CashRegister();
											$cashRegister->__set("user_id", 0);
											$cashRegister->__set("consumer_id", $consumer_id);
											$cashRegister->__set("cr_entry_type", $type[$j]);
											$cashRegister->__set("cr_amount", $package_costs);
											$cashRegister->__set("receipt_number", 0);
											$cashRegister->__set("transaction_id", $transaction_id);
											$cashRegister->__set("transaction_type", "(S)");
											$cashRegister->__set("timestamp",$timestamp_val);
											$cashRegister->__set("entry_status", $entry_type); 
											$cashRegister->__set("remark", $remark);
											$cashRegister->__set("package_id", $package_data[$i]);
											
											$CRs=$cashRegisterMapper->addNewCashRegister($cashRegister);
										}  
									}  
									$equip_month_dis=$equip_month_dis-1;
									$consumer_schememapper->updateConsumerTotalMonth($equip_month_dis,$consumer_schemes_id,'equip_month_dis'); 
								}
								$start_date = date('Y-m-d', strtotime($start_date."+1 month"));
							}
						}
				}
    			$consumerMapper = new Application_Model_ConsumersMapper();
    			$SchemesMapper=new Application_Model_SchemesMapper();
    			$consumer_name = $consumerMapper->getConsumerById($consumer_id);
    			$consumer_schemes_name = $SchemesMapper->getSchemeById($consumer_scheme);
    			
    			$data = array(
    					"consumer_scheme_id" => $consumer_schemes_id,
    					"consumer_id" => $consumer_id,
    					"consumer_name" => $consumer_name->__get("consumer_name"),
    					"consumer_scheme" => $consumer_scheme,
    					"consumer_scheme_name" => $consumer_schemes_name->__get("scheme"),
    					"cluster_id" => $cluster_id,
    					"site_id" => $site_id,
    					"connection_id" => $consumer_name->__get("consumer_connection_id"),
    					"scheme_continue" => $scheme_continue,
    					"package_continue" => $package_continue
    			);
    
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $data
    			);
    		 
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    public function getAllConsumerSchemeAction() {
    
    	try {
    		$consumer_schememapper = new Application_Model_ConsumerSchemeMapper();
    		$consumerMapper = new Application_Model_ConsumersMapper();
    		$SchemesMapper=new Application_Model_SchemesMapper();
    		if ($consumer_schemes = $consumer_schememapper->getAllConsumerScheme()) {
    		 
    			foreach ($consumer_schemes as $consumer_scheme) {
    
    				$consumer_name = $consumerMapper->getConsumerById($consumer_scheme->__get("consumer_id"));
    				$consumer_schemes_name=$SchemesMapper->getSchemeById($consumer_scheme->__get("consumer_scheme"));
    				
    				if(!$consumer_schemes_name)
    				{
    					$consumer_schemes_name = new Application_Model_Schemes();
    				}
    				$data = array(
    						"consumer_scheme_id" => $consumer_scheme->__get("consumer_scheme_id"),
    						"consumer_scheme" => $consumer_scheme->__get("consumer_scheme"),
    						"consumer_id" => $consumer_scheme->__get("consumer_id"),
    						"consumer_name" => $consumer_name->__get("consumer_name"),
    						"consumer_scheme_name" => $consumer_schemes_name->__get("scheme"),
    						"cluster_id" => $consumer_scheme->__get("cluster_id"),
    						"site_id" => $consumer_scheme->__get("site_id"),
    						"connection_id" => $consumer_name->__get("consumer_connection_id"),
    						"package_continue" => $consumer_scheme->__get("package_continue"),
    						"scheme_continue" => $consumer_scheme->__get("scheme_continue"),
    						
    				);
    				$consumer_scheme_arr[] = $data;
    			}
    
         		$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $consumer_scheme_arr,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while get all Consumer Schemes"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    public function updateConsumerSchemeByIdAction() {
    
    	try {
    		$request = $this->getRequest();
    		$consumer_scheme_id = $request->getParam("id");
    		$consumer_id = $request->getParam("consumer_id");
    		$consumer_scheme = $request->getParam("consumer_scheme");
    		$cluster_id = $request->getParam("cluster_id");
    		$site_id = $request->getParam("site_id");
    		$package_continue = $request->getParam("package_continue");
    		$scheme_continue = $request->getParam("scheme_continue");
			$SchemesMapper=new Application_Model_SchemesMapper();
			$packageMapper=new Application_Model_PackagesMapper();
    		$scheme=$SchemesMapper->getSchemeById($consumer_scheme);
			$siteMapper=new Application_Model_SitesMapper();
			$sites=$siteMapper->getSiteById($site_id);
				if($scheme->__get("scheme_type")=="equipment"){
    				$total_month=$scheme->__get("emi_duration");
					$equip_month_dis=$scheme->__get("discount_month");
    			}elseif ($scheme->__get("gen_scheme_type")=="advance_pay"){
    				$total_month=$scheme->__get("advance_payment")+$scheme->__get("advance_dis_month");
    			}
			
    		$consumer_schememapper = new Application_Model_ConsumerSchemeMapper();
    		$consumer_schemes = new Application_Model_ConsumerScheme();
    		$consumer_schemes->__set("consumer_scheme_id", $consumer_scheme_id);
    		$consumer_schemes->__set("consumer_id", $consumer_id);
    		$consumer_schemes->__set("consumer_scheme", $consumer_scheme);
    		$consumer_schemes->__set("cluster_id", $sites->__get("cluster_id"));
    		$consumer_schemes->__set("site_id", $site_id);
    		$consumer_schemes->__set("package_continue", $package_continue);
    		$consumer_schemes->__set("scheme_continue", $scheme_continue);
    		$consumer_schemes->__set("total_month", $total_month);
			$consumer_schemes->__set("equip_month_dis", $equip_month_dis);
			
    		if ($consumers_scheme = $consumer_schememapper->updateConsumerScheme($consumer_schemes)) {
    
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while updating"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function getAllConsumerByPackageIdAction() {
    
    	try {
    		$request = $this->getRequest();
    		$package_id=5;
    		$consumerMapper = new Application_Model_ConsumersMapper();
    		$consumer_schemes = $consumerMapper->getAllConsumerByPackageId($package_id);
    		if(count($consumer_schemes) >0){
    		foreach($consumer_schemes as $consumer_scheme)
    		{
    			$data = array(
    					 'consumer_id' => $consumer_scheme->__get("consumer_id"),
    					'package_id' => $consumer_scheme->__get("package_id"),
    					'site_id' => $consumer_scheme->__get("site_id"),
    					'consumer_code' => $consumer_scheme->__get("consumer_code"),
    					'consumer_name' => $consumer_scheme->__get("consumer_name"),
    					'consumer_father_name' => $consumer_scheme->__get("consumer_father_name"),
    					'consumer_status' => $consumer_scheme->__get("consumer_status"),
    					'consumer_act_date' => $consumer_scheme->__get("consumer_act_date"),
    					'consumer_act_charge' => $consumer_scheme->__get("consumer_act_charge"),
    					'consumer_connection_id' => $consumer_scheme->__get("consumer_connection_id"),
    					'is_micro_enterprise' => $consumer_scheme->__get("is_micro_enterprise"),
    					'micro_entrprise_price' => $consumer_scheme->__get("micro_entrprise_price"),
    					'suspension_date' => $consumer_scheme->__get("suspension_date"),
    					'package_continue' => $consumer_scheme->__get("package_continue"),
    					'scheme_continue' => $consumer_scheme->__get("scheme_continue"),
    			);
    			$consumer_scheme_arr[]=$data;
    		}
     		$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data"=> $consumer_scheme_arr
    			);
    		 } else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while getting consumer"
    			);
    			$arr = array(
    					"meta" => $meta,
    					
    			);
    		} 
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function deleteConsumerSchemeByIdAction() {
    
    	try {
    		$request = $this->getRequest();
    		$consumer_scheme_id=$request->getParam("id");
    		$consumer_schememapper = new Application_Model_ConsumerSchemeMapper();
    		if ($consumer_schemes = $consumer_schememapper->deleteConsumerSchemeById($consumer_scheme_id)) {
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while deleting"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
   public function getConsumerSchemeByConsumerIdAction(){
    
  	try {
    		$request=$this->getRequest();
    		$consumer_id=$request->getParam("consumer_id");
    		$Consumermapper=new Application_Model_ConsumerSchemeMapper();
    		$consumerscheme=$Consumermapper->getConsumerschemeByConsumerId($consumer_id);
    		if($consumerscheme){
    		
    		$data=array(
    				"consumer_scheme_id" => $consumerscheme->__get("consumer_scheme_id"),
    				"consumer_scheme" => $consumerscheme->__get("consumer_scheme"),
    		    	"consumer_id" => $consumerscheme->__get("consumer_id"),
    				"cluster_id" =>$consumerscheme->__get("cluster_id"),
    				"site_id" =>$consumerscheme->__get("site_id"),
    				"scheme_continue" =>$consumerscheme->__get("scheme_continue"),
    				"package_continue" =>$consumerscheme->__get("package_continue")
    				
    		);
    		
    			$meta = array(
    				"code" => 200,
    		  		"message" => "SUCCESS"
    			);
    			$arr = array(
    				"meta" => $meta,
    				"data" => $data
    			);
    		} else {
    			
    			$meta = array(
    				"code" => 401,
    				"message" => "Error while getting ConsumerScheme",
    				
    			);
    			$arr = array(
    				"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
  		}

	public function getSchemeBySiteIdAction() {
    
    	try {
    		$request = $this->getRequest();
    		 $site_id = $request->getParam("site_id");
    		$schemeMapper = new Application_Model_SchemesMapper();
    		$schemes=$schemeMapper->getSchemeBySiteId($site_id);
			$consumer_scheme_arr=array(); 
    		if($schemes){
				foreach($schemes as $scheme)
				{
					 $data = array(
							"scheme_id"=>$scheme["scheme_id"],
							"scheme_name"=> $scheme["scheme"]
					);
					$consumer_scheme_arr[]=$data;
				}
     		$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data"=> $consumer_scheme_arr
    			);
    		 } else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while getting consumer"
    			);
    			$arr = array(
    					"meta" => $meta,
    					
    			);
    		} 
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
}