<?php

class Api_RevenueTargetController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
       // header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
    }

    public function addRevenueTargetAction() {

        try {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $state_id = $request->getParam("state_id");
            $month = $request->getParam("month");
            $year = $request->getParam("year");
            $target = $request->getParam("target");
            
            $revenueTarget=new Application_Model_RevenueTargetMapper();
            $revenue=new Application_Model_RevenueTarget();
            $sitemapper = new Application_Model_SitesMapper();
            
            $stateMapper=new Application_Model_StatesMapper();
        
            $revenue->__set("site_id", $site_id);
            $revenue->__set("state_id", $state_id);
            $revenue->__set("month", $month);
            $revenue->__set("year", $year);
            $revenue->__set("target", $target);
            
            $checkResult=$revenueTarget->checkRevenueTarget($site_id,$month,$year);
            if($checkResult==0){
            if ($revenues = $revenueTarget->addNewRevenueTarget($revenue)) {
            	
            	$state_name=$stateMapper->getStateById($state_id);
            	$site_name=$sitemapper->getSiteById($site_id);
                
                $data = array(
                    "site_id" => $site_id,
                	"site_name" => $site_name->__get("site_name"),
                	"state_id" => $state_id,
                	"state_name" => $state_name->__get("state_name"),
                    "month" => $month,
                    "year" => $year,
                    "target" =>intval($target),
                	
                	 
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }else{
//             	if($revenueTarget->updateRevenueTargetSiteId($site_id,$month,$year,$target)){
            		$meta = array(
            				"code" => 100,
            				"message" => "SUCCESS"
            		);
            		$arr = array(
            				"meta" => $meta,
            		);
            	//}
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
/*-------------------------NEW CODE-------------------------------*/ 
    public function getRevenueTargetByIdAction() {

        try {
            $request = $this->getRequest();
            $year = $request->getParam("year");
            $opex = $request->getParam("opex");
			$type = $request->getParam("type_id");
            $sitemapper = new Application_Model_SitesMapper();
            $revenue = new Application_Model_RevenueTargetMapper();
            $stateMapper = new Application_Model_StatesMapper();
            $site = $sitemapper->getAllSites();
			if($site){
				$revenueTarget_arr=array();
				foreach ($site as $sites){
					if($sites->__get("site_id")!=1){
					$months=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
					$j=3;$years=$year;
					for($i=0;$i<12;$i++){
						$month = $months[$j];
						 
						if($j==11){
							$j=0;}else{$j++;
						}
						if($j==1){
							$years=$years+1;
						}	 
						$revenues=$revenue->getAllRevenueSiteId($sites->__get("site_id"),$month,$years,$opex,$type);
						 
						if($revenues){
						
							foreach ($revenues as $revenue_val){
							
								$state_name=$stateMapper->getStateById($revenue_val["state_id"]);
								$site_name=$sitemapper->getSiteById($revenue_val["site_id"]);
								$data = array(
										"target_id"=>$revenue_val["target_id"],
										"site_id" => $revenue_val["site_id"],
										"site_name" => $site_name->__get("site_name"),
										"month" => $revenue_val["month"],
										"year" =>$revenue_val["year"],
										'target' => intval($revenue_val["target"]),
										'state_id' => $revenue_val["state_id"],
										'state_name' => $state_name->__get("state_name"),
								);
								
								$revenueTarget_arr[] = $data;
							}
						}else{
							$check=array(
									'target' => "",
									'target_id' => "NULL",
							);
							$revenueTarget_arr[]=$check;
						}
					}
				$state_name=$stateMapper->getStateById($sites->__get("state_id"));
				$sitedata = array(
                    "site_id" => $sites->__get("site_id"),
                    "site_name" => $sites->__get("site_name"),
                    "state_id" => $sites->__get("state_id"),
					"state_name"=>$state_name->__get("state_name"),
                	"revenue"=>$revenueTarget_arr,
                );
				$all[]=$sitedata;
				$revenueTarget_arr=array();
				}
				}
				/* $states=$stateMapper->getAllStates();
				foreach ($states as $stat){
					$sitemapper->getSiteByStateId($stat->__get("state_id"));
					
				}
 */				

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $all
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
/*-------------------------END NEW CODE-------------------------------*/            
    public function deleteRevenueTargetByIdAction() {

        try {
            $request = $this->getRequest();
            $revenue_target_id = $request->getParam("id");
           $revenueTarget=new Application_Model_RevenueTargetMapper();
            if ($revenueTargets = $revenueTarget->deleteRevenueTargetById($revenue_target_id)) {

            	$this->_logger->info("Revenue Target Id ".$revenue_target_id." has been deleted from RevenueTarget by ". $this->_userName.".");
            	

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllRevenueTargetAction() {

        try {
 			$revenueTarget=new Application_Model_RevenueTargetMapper();
            $sitemapper = new Application_Model_SitesMapper();
             
            $stateMapper=new Application_Model_StatesMapper();
           
            $revenueTargets = $revenueTarget->getAllRevenueTarget();
            
             if($revenueTargets)    { 
               
               foreach ($revenueTargets as $revenueTarget)
               {
	               	$state_name=$stateMapper->getStateById($revenueTarget->__get("state_id"));
	               	$site_name=$sitemapper->getSiteById($revenueTarget->__get("site_id"));
                    $data = array(
                    	"target_id"=>$revenueTarget->__get("target_id"),
                        "site_id" => $revenueTarget->__get("site_id"),
                        "site_name" => $site_name->__get("site_name"),
                        "month" => $revenueTarget->__get("month"),
                        "year" =>$revenueTarget->__get("year"),
                        'target' => intval($revenueTarget->__get("target")),    
                        'state_id' => $revenueTarget->__get("state_id"),
                    	'state_name' => $state_name->__get("state_name"),
                        
                    );

                    $revenueTarget_arr[] = $data;
               }  
			  $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $revenueTarget_arr,
                );
             }else{
             	$meta = array(
             			"code" => 401,
             			"message" => "Error while adding"
             	);
             	$arr = array(
             			"meta" => $meta
             	);
             } 
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updateRevenueTargetByIdAction() {

        try {
            $request = $this->getRequest();
            $target_id = $request->getParam("id");
            $site_id = $request->getParam("site_id");
            $state_id = $request->getParam("state_id");
            $month = $request->getParam("month");
            $year = $request->getParam("year");
            $target = $request->getParam("target");
      
            $revenueTarget=new Application_Model_RevenueTargetMapper();
            $revenue=new Application_Model_RevenueTarget();
           
			$revenue->__set("target_id", $target_id);
            $revenue->__set("site_id", $site_id);
            $revenue->__set("state_id", $state_id);
            $revenue->__set("month", $month);
            $revenue->__set("year", $year);
            $revenue->__set("target", $target);
             
            $checkResult=$revenueTarget->checkRevenueTarget($site_id,$month,$year,$target_id);
            if($checkResult==0){
	            if ($revenueTargets = $revenueTarget->updateRevenueTarget($revenue)) {
	               
	            	$meta = array(
	                    "code" => 200,
	                    "message" => "SUCCESS"
	                );
	                $arr = array(
	                    "meta" => $meta,
	                );
	            } else {
	                $meta = array(
	                    "code" => 401,
	                    "message" => "Error while updating"
	                );
	                $arr = array(
	                    "meta" => $meta
	                );
	            }
            }else{
            	$meta = array(
            			"code" => 100,
            			"message" => "Error while updating"
            	);
            	$arr = array(
            			"meta" => $meta
            	);
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getDateAction() {

        try {
            $request = $this->getRequest();
            
            $year = $request->getParam("year");
            $months=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
             $j=3;
            for($i=0;$i<12;$i++){
            
            	$zendDate = new Zend_Date();
            	$zend= $zendDate->setTimezone("Asia/Calcutta");
            
            	$day = $zend->toString(Zend_Date::DAY);
            	$month = $months[$j];
            	
            			if($j==11){
							$j=0;}else{$j++;
						}
						if($j==1){
							$year=$year+1;
						}	

            	$fulldate=$year."-".$month."-".$day ;
            	$dates = new Zend_Date($fulldate,"yyyy-MM-dd");
            	$timestamp = $dates->toString("MMM-yy");
            	
            	$data = array(
            			"timestamp" => $timestamp,
            	);
            	
            	$site_arr[] = $data;
            }
             $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $site_arr,
                );
             
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
/*-------------------------NEW CODE-------------------------------*/            
    public function updateAllRevenueTargetAction() {
        
        try {
            $request = $this->getRequest();
            $target_id = $request->getParam("id");
            $site_id = $request->getParam("site_id");
            $target = $request->getParam("target");
            $state_id = $request->getParam("state_id");
            $year = $request->getParam("year");
            $target_id = $request->getParam("target_id");
            $opex = $request->getParam("opex");
			$type = $request->getParam("type_id");

            if($target!='NULL' && $target_id!='NULL'){
                $revenueTarget=new Application_Model_RevenueTargetMapper();
                $revenue=new Application_Model_RevenueTarget();
                $targets=explode(",", $target);
                $targetcount=count($targets);

                $target_ids=explode(",", $target_id);
                
                $targetcount=count($target_ids);
                
                $month=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                $j=3;
                for($i=0;$i<$targetcount;$i++){ 
                	
    				$revenue->__set("target_id", $target_id);
    	            $revenue->__set("site_id", $site_id);
    	            $revenue->__set("state_id", $state_id);
    	            $revenue->__set("month", $month[$j]);
    	            $revenue->__set("year", $year);
    	            $revenue->__set("target", $targets[$i]); 
                    $revenue->__set("opex", $opex); 
					$revenue->__set("type", $type); 
    	            
                    if($j==11){
                        $j=0;}else{$j++;
                    }
                        if($j==0){
                        $year=$year+1;
                    }	
                    
                    if($target_ids[$i]!=NULL && $target_ids[$i]!="NULL" && $target_ids[$i]!='undefined'){
                        $revenue->__set("target_id", $target_ids[$i]);
                        //echo " ". $target_id[$i];
                        $revenueTar = $revenueTarget->updateRevenueTarget($revenue);
                    }else if($targets[$i]!="" && $targets[$i]!=NULL){
                        $revenueTargets = $revenueTarget->addNewRevenueTarget($revenue); 
                    }       
                }
         
            	$meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
	             
            }else{
                $meta = array(
                "code" => 100,
                "message" => "SUCCESS"
                );
                $arr = array(
                "meta" => $meta,
                );
            }
                 
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
/*-------------------------END NEW CODE-------------------------------*/                
    public function getAllSitesForSitePageAction() {
    
    	try {
    		
    		$sitemapper = new Application_Model_SitesMapper();
    		$clustermapper= new Application_Model_ClustersMapper();
    		$consumersMapper = new Application_Model_ConsumersMapper();
    		$stateMapper=new Application_Model_StatesMapper();
    		$roleSiteMapper=new Application_Model_RoleSiteMapper();
    	
    
    		if ($sites = $sitemapper->getAllSitesForSitePage()) {
    
    			foreach ($sites as $site) {
    				
    				$cluster_name=$clustermapper->getClusterById($site->__get("cluster_id"));
    				$state_name=$stateMapper->getStateById($site->__get("state_id"));
    				
    				
    				$data = array(
    						"site_id" => $site->__get("site_id"),
    						"site_name" => $site->__get("site_name"),
    						"cluster_id" => $site->__get("cluster_id"),
    						"cluster_name" =>$cluster_name->__get("cluster_name"),
    						'num_of_poles' => $site->__get("num_of_poles"),
    						'site_code' => $site->__get("site_code"),
    						'state_id' => $site->__get("state_id"),
    						'state_name' => $state_name->__get("state_name"),
    						'site_status' => $site->__get("site_status"),
    				);
    
    				$site_arr[] = $data;
    			}
    
    
    
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $site_arr,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
   
}
