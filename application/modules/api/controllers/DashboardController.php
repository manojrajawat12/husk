<?php

class Api_DashboardController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }
    public function destroyAction()
    {
    	unset($this);
    	gc_enable() ;gc_collect_cycles();gc_disable();
    }
    public function indexAction(){
        
    }
     public function monthlyBilledAction()
    {
        try
        {
        	date_default_timezone_set('Asia/Kolkata');
            $request = $this->getRequest();
            $state_id=$request->getParam("state_id");
            $site_id = $request->getParam("site_id");
            $curr_date = $request->getParam("curr_date");
            $last_date = $request->getParam("last_date");
            
            $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
            $curr_date_d = date_create($zendDate->toString("yyyy-MM-dd"));
			$curr_date = $zendDate->toString("yyyy-MM-dd");
            
            $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
            $last_date_d = date_create($zendDate->toString("yyyy-MM-dd")); 
			$last_date = $zendDate->toString("yyyy-MM-dd"); 
            
            //$cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
            //$Cal_Month= ceil($cal /30);
			 
			$Cal_Month=date_diff($last_date_d,$curr_date_d);
			$Cal_Month=intval($Cal_Month->format("%a")+1);
			
			if($Cal_Month<=31){
				$next_date=" -1 month";
			}else{
				$next_date=" -".$Cal_Month."day";
			}
			
			if(date('m', strtotime($curr_date)) == date('m', strtotime($last_date))){
				$month_no=date('m', strtotime($curr_date));
				$year_val=date('y', strtotime($curr_date));
				$total_days_in_month=cal_days_in_month(CAL_GREGORIAN,$month_no, $year_val);
				if($total_days_in_month==$Cal_Month){
					$back_lastDate = date('Y-m-d', strtotime($last_date ." -1 month"));
					$back_CurrDate = date('Y-m-d', strtotime($last_date ."-1 day"));
				}else{
					$back_lastDate = date('Y-m-d', strtotime($last_date .$next_date));
					$back_CurrDate = date('Y-m-d', strtotime($curr_date .$next_date)); 
				}
			}else{
				$back_lastDate = date('Y-m-d', strtotime($last_date .$next_date));
				$back_CurrDate = date('Y-m-d', strtotime($curr_date .$next_date)); 
			}
			
            $last_year_date = date('Y-m-d', strtotime($last_date ." -1 year"));
            $curr_year_rate = date('Y-m-d', strtotime($curr_date ." -1 year"));
			
		    //echo $last_year_date." ".$curr_year_rate;exit;
		    
            $clusterMapper=new Application_Model_ClustersMapper();
             
            if($site_id=="ALL"){
                $site_id = NULL;
            }
            if($state_id=="ALL"){
            	$state_id = NULL;
            }
            $auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		
    		
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		
    		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
            
            $totalDebit = $cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site_id,"DEBIT",$last_date,$state_id,$role_sites_id);
            $totalActivation = $cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site_id,"ACTIVATION",$last_date,$state_id,$role_sites_id);
           	$totalCredit = $cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site_id,"CREDIT",$last_date,$state_id,$role_sites_id);
			$totalEntryType = $cashRegisterMapper->getTotalCollectedForEntryType($curr_date,$last_date,$site_id,$state_id,$role_sites_id);
			$totalBilledAmount=$cashRegisterMapper->getTotalBilledForEntryType($curr_date,$last_date,$site_id,$state_id,$role_sites_id);
					
			$Back_totalDebit = $cashRegisterMapper->getTotalCollectedByMonth($back_CurrDate,$site_id,"DEBIT",$back_lastDate,$state_id,$role_sites_id);
			$Back_totalActivation = $cashRegisterMapper->getTotalCollectedByMonth($back_CurrDate,$site_id,"ACTIVATION",$back_lastDate,$state_id,$role_sites_id);
			$Back_totalCredit = $cashRegisterMapper->getTotalCollectedByMonth($back_CurrDate,$site_id,"CREDIT",$back_lastDate,$state_id,$role_sites_id);
			$Back_totalEntryType = $cashRegisterMapper->getTotalCollectedForEntryType($back_CurrDate,$back_lastDate,$site_id,$state_id,$role_sites_id);
			$Back_totalBilledAmount=$cashRegisterMapper->getTotalBilledForEntryType($back_CurrDate,$back_lastDate,$site_id,$state_id,$role_sites_id);
					
			$lastYear_totalDebit = $cashRegisterMapper->getTotalCollectedByMonth($curr_year_rate,$site_id,"DEBIT",$last_year_date,$state_id,$role_sites_id);
			$lastYear_totalActivation = $cashRegisterMapper->getTotalCollectedByMonth($curr_year_rate,$site_id,"ACTIVATION",$last_year_date,$state_id,$role_sites_id);
			$lastYear_totalCredit = $cashRegisterMapper->getTotalCollectedByMonth($curr_year_rate,$site_id,"CREDIT",$last_year_date,$state_id,$role_sites_id);
			$lastYear_totalEntryType = $cashRegisterMapper->getTotalCollectedForEntryType($curr_year_rate,$last_year_date,$site_id,$state_id,$role_sites_id);
			$lastYear_totalBilledAmount=$cashRegisterMapper->getTotalBilledForEntryType($curr_year_rate,$last_year_date,$site_id,$state_id,$role_sites_id); 
			
			$totalDebit=$totalDebit+intval($totalEntryType["SD"]);
			$Back_totalDebit=$Back_totalDebit+intval($Back_totalEntryType["SD"]); 
			//$totalCredit=$totalCredit+intval($totalEntryType["SD"]);
			//$Back_totalCredit=$Back_totalCredit+intval($Back_totalEntryType["SD"]);
			
            $zendDate = new Zend_Date();
            $zendDate->setTimezone("Asia/Calcutta");
            $date = $zendDate->toString("dd/MM/yyyy"); 
            $month1  = $zendDate->toString("MM");
            $year  = $zendDate->toString("yyyy");
            $monthlyTarget = "N/A";
            $otpTarget = "N/A";
            if($site_id!=NULL) 
            {
              // $monthlyTarget = $cashRegisterMapper->getOutstandingBySiteId($site_id, $date);
               //$otpTarget = $cashRegisterMapper->getOTPDueBySiteId($site_id, $month1, $year);
            }
            //sprintf("%.2f",
			$consumersMapper = new Application_Model_ConsumersMapper();
			$last_date_new = date('Y-m-d', strtotime($last_date ." -27 day"));
			
			$new_consumer_added = $cashRegisterMapper->getBillCount($curr_date,$last_date_new,$site_id,$state_id,$role_sites_id);
			$energyTargetColl=$new_consumer_added["CHG_count"]; 
			
            $lastYear_credit_CHG = (intval($totalEntryType["CHG"]))-(intval($lastYear_totalEntryType["CHG"]));
            if($lastYear_credit_CHG>0){
            	$lastYear_CHG_collection_percentage=intval($lastYear_credit_CHG / (intval($lastYear_totalEntryType["CHG"])==0?1:intval($lastYear_totalEntryType["CHG"]))*100);
            	$lastYear_CHG_progress_credit="INCREMENT";
            	$lastYear_CHG_cal_color_status="success";
            }else{
            	$lastYear_CHG_collection_percentage=intval(abs($lastYear_credit_CHG) / (intval($lastYear_totalEntryType["CHG"])==0?1:intval($lastYear_totalEntryType["CHG"]))*100);
            	$lastYear_CHG_progress_credit="DECREMENT";
            	$lastYear_CHG_cal_color_status="danger";
            }
            $lastYear_debit_CHG  = (intval($totalBilledAmount["CHG"]))-(intval($lastYear_totalBilledAmount["CHG"]));
            if($lastYear_debit_CHG>0){
            	$lastYear_CHG_debit_percentage=intval($lastYear_debit_CHG / (intval($lastYear_totalBilledAmount["CHG"])==0?1:intval($lastYear_totalBilledAmount["CHG"]))*100);
            	$lastYear_CHG_progress_debit="INCREMENT";
            	$lastYear_CHG_debit_color_status="success";
            }else{
            	$lastYear_CHG_debit_percentage=intval(abs($lastYear_debit_CHG) / (intval($lastYear_totalBilledAmount["CHG"])==0?1:intval($lastYear_totalBilledAmount["CHG"]))*100);
            	$lastYear_CHG_progress_debit="DECREMENT";
            	$lastYear_CHG_debit_color_status="danger";
            }
            
            $minus_credit_EED = (intval($totalEntryType["EED"]))-(intval($Back_totalEntryType["EED"]));
            if($minus_credit_EED>0){
             	$EED_collection_percentage=intval($minus_credit_EED / (intval($Back_totalEntryType["EED"])==0?1:intval($Back_totalEntryType["EED"]))*100);
            	$EED_progress_credit="INCREMENT";
            	$EED_cal_color_status="success";
            }else{
            	$EED_collection_percentage=intval(abs($minus_credit_EED) / (intval($Back_totalEntryType["EED"])==0?1:intval($Back_totalEntryType["EED"]))*100);
            	$EED_progress_credit="DECREMENT";
            	$EED_cal_color_status="danger";
            }
            
            $minus_debit_EED  = (intval($totalBilledAmount["EED"]))-(intval($Back_totalBilledAmount["EED"]));
            if($minus_debit_EED>0){
            	$EED_debit_percentage=intval($minus_debit_EED / (intval($Back_totalBilledAmount["EED"])==0?1:intval($Back_totalBilledAmount["EED"]))*100);
            	$EED_progress_debit="INCREMENT";
            	$EED_debit_color_status="success";
            }else{
            	$EED_debit_percentage=intval(abs($minus_debit_EED) / (intval($Back_totalBilledAmount["EED"])==0?1:intval($Back_totalBilledAmount["EED"]))*100);
            	$EED_progress_debit="DECREMENT";
            	$EED_debit_color_status="danger";
            }
            
            $minus_credit_MED = (intval($totalEntryType["MED"]))-(intval($Back_totalEntryType["MED"]));
            if($minus_credit_MED>0){
            	$MED_collection_percentage=intval($minus_credit_MED / (intval($Back_totalEntryType["MED"])==0?1:intval($Back_totalEntryType["MED"]))*100);
            	$MED_progress_credit="INCREMENT";
            	$MED_cal_color_status="success";
            }else{
            	$MED_collection_percentage=intval(abs($minus_credit_MED) / (intval($Back_totalEntryType["MED"])==0?1:intval($Back_totalEntryType["MED"]))*100);
            	$MED_progress_credit="DECREMENT";
            	$MED_cal_color_status="danger";
            }
            $minus_debit_MED  = (intval($totalBilledAmount["MED"]))-(intval($Back_totalBilledAmount["MED"]));
            if($minus_debit_MED>0){
            	$MED_debit_percentage=intval($minus_debit_MED / (intval($Back_totalBilledAmount["MED"])==0?1:intval($Back_totalBilledAmount["MED"]))*100);
            	$MED_progress_debit="INCREMENT";
            	$MED_debit_color_status="success";
            }else{
            	$MED_debit_percentage=intval(abs($minus_debit_MED) / (intval($Back_totalBilledAmount["MED"])==0?1:intval($Back_totalBilledAmount["MED"]))*100);
            	$MED_progress_debit="DECREMENT";
            	$MED_debit_color_status="danger";
            }
            $minus_credit_CHG = (intval($totalEntryType["CHG"]))-(intval($Back_totalEntryType["CHG"]));
            if($minus_credit_CHG>0){
            	$CHG_collection_percentage=intval($minus_credit_CHG / (intval($Back_totalEntryType["CHG"])==0?1:intval($Back_totalEntryType["CHG"]))*100);
            	$CHG_progress_credit="INCREMENT";
            	$CHG_cal_color_status="success";
            }else{
            	$CHG_collection_percentage=intval(abs($minus_credit_CHG) / (intval($Back_totalEntryType["CHG"])==0?1:intval($Back_totalEntryType["CHG"]))*100);
            	$CHG_progress_credit="DECREMENT";
            	$CHG_cal_color_status="danger";
            }
            $minus_debit_CHG  = (intval($totalBilledAmount["CHG"]))-(intval($Back_totalBilledAmount["CHG"]));
            if($minus_debit_CHG>0){
            	$CHG_debit_percentage=intval($minus_debit_CHG / (intval($Back_totalBilledAmount["CHG"])==0?1:intval($Back_totalBilledAmount["CHG"]))*100);
            	$CHG_progress_debit="INCREMENT";
            	$CHG_debit_color_status="success";
            }else{
            	$CHG_debit_percentage=intval(abs($minus_debit_CHG) / (intval($Back_totalBilledAmount["CHG"])==0?1:intval($Back_totalBilledAmount["CHG"]))*100);
            	$CHG_progress_debit="DECREMENT";
            	$CHG_debit_color_status="danger";
            }
            $minus_credit_WATER = (intval($totalEntryType["WATER"]))-(intval($Back_totalEntryType["WATER"]));
            if($minus_credit_WATER>0){
            	$WATER_collection_percentage=intval($minus_credit_WATER / (intval($Back_totalEntryType["WATER"])==0?1:intval($Back_totalEntryType["WATER"]))*100);
            	$WATER_progress_credit="INCREMENT";
            	$WATER_cal_color_status="success";
            }else{
            	$WATER_collection_percentage=intval(abs($minus_credit_WATER) / (intval($Back_totalEntryType["WATER"])==0?1:intval($Back_totalEntryType["WATER"]))*100);
            	$WATER_progress_credit="DECREMENT";
            	$WATER_cal_color_status="danger";
            }
            $minus_debit_WATER  = (intval($totalBilledAmount["WATER"]))-(intval($Back_totalBilledAmount["WATER"]));
            if($minus_debit_WATER>0){
            	$WATER_debit_percentage=intval($minus_debit_WATER / (intval($Back_totalBilledAmount["WATER"])==0?1:intval($Back_totalBilledAmount["WATER"]))*100);
            	$WATER_progress_debit="INCREMENT";
            	$WATER_debit_color_status="success";
            }else{
            	$WATER_debit_percentage=intval(abs($minus_debit_WATER) / (intval($Back_totalBilledAmount["WATER"])==0?1:intval($Back_totalBilledAmount["WATER"]))*100);
            	$WATER_progress_debit="DECREMENT";
            	$WATER_debit_color_status="danger"; 
            }
            $minus_credit_ACT = (intval($totalEntryType["ACT"]))-(intval($Back_totalEntryType["ACT"]));
            if($minus_credit_ACT>0){
            	$ACT_collection_percentage=intval($minus_credit_ACT / (intval($Back_totalEntryType["ACT"])==0?1:intval($Back_totalEntryType["ACT"]))*100);
            	$ACT_progress_credit="INCREMENT";
            	$ACT_cal_color_status="success";
            }else{
            	$ACT_collection_percentage=intval(abs($minus_credit_ACT) / (intval($Back_totalEntryType["ACT"])==0?1:intval($Back_totalEntryType["ACT"]))*100);
            	$ACT_progress_credit="DECREMENT";
            	$ACT_cal_color_status="danger";
            }
            $minus_debit_ACT  = (intval($totalBilledAmount["ACT"]))-(intval($Back_totalBilledAmount["ACT"]));
            if($minus_debit_ACT>0){
            	$ACT_debit_percentage=intval($minus_debit_ACT / (intval($Back_totalBilledAmount["ACT"])==0?1:intval($Back_totalBilledAmount["ACT"]))*100);
            	$ACT_progress_debit="INCREMENT";
            	$ACT_debit_color_status="success";
            }else{
            	$ACT_debit_percentage=intval(abs($minus_debit_ACT) / (intval($Back_totalBilledAmount["ACT"])==0?1:intval($Back_totalBilledAmount["ACT"]))*100);
            	$ACT_progress_debit="DECREMENT";
            	$ACT_debit_color_status="danger";
            }
            $minus_credit_OTHERS = (intval($totalEntryType["OTHERS"]))-(intval($Back_totalEntryType["OTHERS"]));
            if($minus_credit_OTHERS>0){
            	$OTHERS_collection_percentage=intval($minus_credit_OTHERS / (intval($Back_totalEntryType["OTHERS"])==0?1:intval($Back_totalEntryType["OTHERS"]))*100);
            	$OTHERS_progress_credit="INCREMENT";
            	$OTHERS_cal_color_status="success";
            }else{
            	$OTHERS_collection_percentage=intval(abs($minus_credit_OTHERS) / (intval($Back_totalEntryType["OTHERS"])==0?1:intval($Back_totalEntryType["OTHERS"]))*100);
            	$OTHERS_progress_credit="DECREMENT";
            	$OTHERS_cal_color_status="danger";
            }
            $minus_debit_OTHERS  = (intval($totalBilledAmount["OTHERS"]))-(intval($Back_totalBilledAmount["OTHERS"]));
            if($minus_debit_OTHERS>0){
            	$OTHERS_debit_percentage=intval($minus_debit_OTHERS / (intval($Back_totalBilledAmount["OTHERS"])==0?1:intval($Back_totalBilledAmount["OTHERS"]))*100);
            	$OTHERS_progress_debit="INCREMENT";
            	$OTHERS_debit_color_status="success";
            }else{
            	$OTHERS_debit_percentage=intval(abs($minus_debit_OTHERS) / (intval($Back_totalBilledAmount["OTHERS"])==0?1:intval($Back_totalBilledAmount["OTHERS"]))*100);
            	$OTHERS_progress_debit="DECREMENT";
            	$OTHERS_debit_color_status="danger";
            }
            
            $minus_credit_amount = ($totalCredit+$totalActivation)-($Back_totalCredit+$Back_totalActivation);
            $total_back_credit=$Back_totalCredit+$Back_totalActivation;
            if($minus_credit_amount>0){
            	$cal_collection_percentage=intval($minus_credit_amount / (intval($total_back_credit)==0?1:intval($total_back_credit))*100);
            	$cal_progress_credit="INCREMENT";
            	$cal_color_status="success";
            }else{
            	$cal_collection_percentage=intval(abs($minus_credit_amount) / (intval($total_back_credit)==0?1:intval($total_back_credit))*100);
            	$cal_progress_credit="DECREMENT";
            	$cal_color_status="danger";
            }

            $minus_debit_amount  = $totalDebit-$Back_totalDebit;
            
            if($minus_debit_amount>0){
            	$bill_collection_percentage=intval($minus_debit_amount / (intval($Back_totalDebit)==0?1:intval($Back_totalDebit))*100);
            	$bill_progress_credit="INCREMENT";
            	$bill_color_status="success";
            }else{
            	$bill_collection_percentage=intval(abs($minus_debit_amount) / (intval($Back_totalDebit)==0?1:intval($Back_totalDebit))*100);
            	$bill_progress_credit="DECREMENT";
            	$bill_color_status="danger";
            }
            
            $minus_credit_SD = (intval($totalEntryType["SD"]))-(intval($Back_totalEntryType["SD"]));
            if($minus_credit_SD>0){
            	$SD_collection_percentage=intval($minus_credit_SD / (intval($Back_totalEntryType["SD"])==0?1:intval($Back_totalEntryType["SD"]))*100);
            	$SD_progress_credit="INCREMENT";
            	$SD_cal_color_status="success";
            }else{
            	$SD_collection_percentage=intval(abs($minus_credit_SD) / (intval($Back_totalEntryType["SD"])==0?1:intval($Back_totalEntryType["SD"]))*100);
            	$SD_progress_credit="DECREMENT";
            	$SD_cal_color_status="danger";
            }
			
			 $minus_credit_DISCOUNT = (intval($totalEntryType["DISCOUNT"]))-(intval($Back_totalEntryType["DISCOUNT"]));
            if($minus_credit_DISCOUNT>0){
            	$DISCOUNT_collection_percentage=intval($minus_credit_DISCOUNT / (intval($Back_totalEntryType["DISCOUNT"])==0?1:intval($Back_totalEntryType["DISCOUNT"]))*100);
            	$DISCOUNT_progress_credit="INCREMENT";
            	$DISCOUNT_cal_color_status="success";
            }else{
            	$DISCOUNT_collection_percentage=intval(abs($minus_credit_DISCOUNT) / (intval($Back_totalEntryType["DISCOUNT"])==0?1:intval($Back_totalEntryType["DISCOUNT"]))*100);
            	$DISCOUNT_progress_credit="DECREMENT";
            	$DISCOUNT_cal_color_status="danger";
            }
			
			 $minus_credit_cr_note = (intval($totalEntryType["cr_note"]))-(intval($Back_totalEntryType["cr_note"]));
            if($minus_credit_cr_note>0){
            	$cr_note_collection_percentage=intval($minus_credit_cr_note / (intval($Back_totalEntryType["cr_note"])==0?1:intval($Back_totalEntryType["cr_note"]))*100);
            	$cr_note_progress_credit="INCREMENT";
            	$cr_note_cal_color_status="success";
            }else{
            	$cr_note_collection_percentage=intval(abs($minus_credit_cr_note) / (intval($Back_totalEntryType["cr_note"])==0?1:intval($Back_totalEntryType["cr_note"]))*100);
            	$cr_note_progress_credit="DECREMENT";
            	$cr_note_cal_color_status="danger";
            }
			
			$minus_debit_SCHEME  = (intval($totalBilledAmount["SCHEME"]))-(intval($Back_totalBilledAmount["SCHEME"]));
            if($minus_debit_SCHEME>0){
            	$SCHEME_debit_percentage=intval($minus_debit_SCHEME / (intval($Back_totalBilledAmount["SCHEME"])==0?1:intval($Back_totalBilledAmount["SCHEME"]))*100);
            	$SCHEME_progress_debit="INCREMENT";
            	$SCHEME_debit_color_status="success";
            }else{
            	$SCHEME_debit_percentage=intval(abs($minus_debit_SCHEME) / (intval($Back_totalBilledAmount["SCHEME"])==0?1:intval($Back_totalBilledAmount["SCHEME"]))*100);
            	$SCHEME_progress_debit="DECREMENT";
            	$SCHEME_debit_color_status="danger";
            }
             
			
			// for last year
			
			$lastYear_credit_amount = ($totalCredit+$totalActivation)-($lastYear_totalCredit+$lastYear_totalActivation);
            $total_lastYear_credit=$lastYear_totalCredit+$lastYear_totalActivation;
            if($lastYear_credit_amount>0){
            	$last_year_cal_collection_percentage=intval($lastYear_credit_amount / (intval($total_lastYear_credit)==0?1:intval($total_lastYear_credit))*100);
            	$last_year_cal_progress_credit="INCREMENT";
            	$last_year_cal_color_status="success";
            }else{
            	$last_year_cal_collection_percentage=intval(abs($lastYear_credit_amount) / (intval($total_lastYear_credit)==0?1:intval($total_lastYear_credit))*100);
            	$last_year_cal_progress_credit="DECREMENT";
            	$last_year_cal_color_status="danger";
            }
			
            $lastYear_debit_amount  = $totalDebit-$lastYear_totalDebit;
            if($lastYear_debit_amount>0){
            	$last_year_bill_collection_percentage=intval($lastYear_debit_amount / (intval($lastYear_totalDebit)==0?1:intval($lastYear_totalDebit))*100);
            	$last_year_bill_progress_credit="INCREMENT";
            	$last_year_bill_color_status="success";
            }else{
            	$last_year_bill_collection_percentage=intval(abs($lastYear_debit_amount) / (intval($lastYear_totalDebit)==0?1:intval($lastYear_totalDebit))*100);
            	$last_year_bill_progress_credit="DECREMENT";
            	$last_year_bill_color_status="danger";
            }
			
			
			
			$data = array(
					"total_debit" => intval($totalDebit),
					"total_activation" => intval($totalActivation),
					"total_credit" => intval($totalCredit),
					"total_collected" => $totalCredit+$totalActivation,
				   // "monthly_target" => $monthlyTarget,
				   // "otp_target" => $otpTarget,
						
					"cal_collection_percentage"=>$cal_collection_percentage,
					"cal_progress_credit"=>$cal_progress_credit,
					"cal_color_status"=>$cal_color_status,

					"bill_collection_percentage"=>$bill_collection_percentage,
					"bill_progress_credit"=>$bill_progress_credit,
					"bill_color_status"=>$bill_color_status,
					
					"last_year_cal_collection_percentage"=>$last_year_cal_collection_percentage,
					"last_year_cal_progress_credit"=>$last_year_cal_progress_credit, 
					"last_year_cal_color_status"=>$last_year_cal_color_status,

					"last_year_bill_collection_percentage"=>$last_year_bill_collection_percentage,
					"last_year_bill_progress_credit"=>$last_year_bill_progress_credit,
					"last_year_bill_color_status"=>$last_year_bill_color_status,
						
					"EED"=>intval($totalEntryType["EED"]),
					"MED"=>intval($totalEntryType["MED"]),
					"CHG"=>intval($totalEntryType["CHG"]),
					"ACT"=>intval($totalEntryType["ACT"]),
					"WATER"=>intval($totalEntryType["WATER"]),
					"OTHERS"=>intval($totalEntryType["OTHERS"]),
					"SD"=>intval($totalEntryType["SD"]),
					"DISCOUNT"=>intval($totalEntryType["DISCOUNT"]),
					"METER"=>intval($totalEntryType["METER"]),
					"cr_note"=>intval($totalEntryType["cr_note"]),
					
					"EED_count"=>intval($totalEntryType["EED_count"]),
					"MED_count"=>intval($totalEntryType["MED_count"]),
					"CHG_count"=>intval($totalEntryType["CHG_count"]),
					"ACT_count"=>intval($totalEntryType["ACT_count"]),
					"WATER_count"=>intval($totalEntryType["WATER_count"]),
					"OTHERS_count"=>intval($totalEntryType["OTHERS_count"]),
					"SD_count"=>intval($totalEntryType["SD_count"]),
					"DISCOUNT_count"=>intval($totalEntryType["DISCOUNT_count"]),
					"METER_count"=>intval($totalEntryType["METER_count"]),
					"cr_note_count"=>intval($totalEntryType["cr_note_count"]),
					
					"Bill_EED"=>intval($totalBilledAmount["EED"]),
					"Bill_MED"=>intval($totalBilledAmount["MED"]),
					"Bill_CHG"=>intval($totalBilledAmount["CHG"]),
					"Bill_ACT"=>intval($totalBilledAmount["ACT"]),
					"Bill_WATER"=>intval($totalBilledAmount["WATER"]),
					"Bill_OTHERS"=>intval($totalBilledAmount["OTHERS"]),
					"Bill_SCHEME"=>intval($totalBilledAmount["SCHEME"]),
					"Bill_METER"=>intval($totalBilledAmount["METER"]),
					
					"Bill_EED_count"=>intval($totalBilledAmount["EED_count"]),
					"Bill_MED_count"=>intval($totalBilledAmount["MED_count"]),
					"Bill_CHG_count"=>intval($totalBilledAmount["CHG_count"]),
					"Bill_ACT_count"=>intval($totalBilledAmount["ACT_count"]),
					"Bill_WATER_count"=>intval($totalBilledAmount["WATER_count"]),
					"Bill_OTHERS_count"=>intval($totalBilledAmount["OTHERS_count"]),
					"Bill_METER_count"=>intval($totalBilledAmount["METER_count"]),
					"Bill_SCHEME_count"=>intval($totalBilledAmount["SCHEME_count"]),
            		
            		"EED_collection_percentage"=>$EED_collection_percentage,
            		"EED_progress_credit"=>$EED_progress_credit,
            		"EED_cal_color_status"=>$EED_cal_color_status,
            		"EED_debit_percentage"=>$EED_debit_percentage,
            		"EED_progress_debit"=>$EED_progress_debit,
            		"EED_debit_color_status"=>$EED_debit_color_status,
            		
            		"MED_collection_percentage"=>$MED_collection_percentage,
            		"MED_progress_credit"=>$MED_progress_credit,
            		"MED_cal_color_status"=>$MED_cal_color_status,
            		"MED_debit_percentage"=>$MED_debit_percentage,
            		"MED_progress_debit"=>$MED_progress_debit,
            		"MED_debit_color_status"=>$MED_debit_color_status,
            		
            		"ACT_collection_percentage"=>$ACT_collection_percentage,
            		"ACT_progress_credit"=>$ACT_progress_credit,
            		"ACT_cal_color_status"=>$ACT_cal_color_status,
            		"ACT_debit_percentage"=>$ACT_debit_percentage,
            		"ACT_progress_debit"=>$ACT_progress_debit,
            		"ACT_debit_color_status"=>$ACT_debit_color_status,
            		
            		"CHG_collection_percentage"=>$CHG_collection_percentage,
            		"CHG_progress_credit"=>$CHG_progress_credit,
            		"CHG_cal_color_status"=>$CHG_cal_color_status,
            		"CHG_debit_percentage"=>$CHG_debit_percentage,
            		"CHG_progress_debit"=>$CHG_progress_debit,
            		"CHG_debit_color_status"=>$CHG_debit_color_status,
            		
            		"WATER_collection_percentage"=>$WATER_collection_percentage,
            		"WATER_progress_credit"=>$WATER_progress_credit,
            		"WATER_cal_color_status"=>$WATER_cal_color_status,
            		"WATER_debit_percentage"=>$WATER_debit_percentage,
            		"WATER_progress_debit"=>$WATER_progress_debit,
            		"WATER_debit_color_status"=>$WATER_debit_color_status,
            		
            		"OTHERS_collection_percentage"=>$OTHERS_collection_percentage,
            		"OTHERS_progress_credit"=>$OTHERS_progress_credit,
            		"OTHERS_cal_color_status"=>$OTHERS_cal_color_status,
            		"OTHERS_debit_percentage"=>$OTHERS_debit_percentage,
            		"OTHERS_progress_debit"=>$OTHERS_progress_debit,
            		"OTHERS_debit_color_status"=>$OTHERS_debit_color_status,
            		
            		"SD_collection_percentage"=>$SD_collection_percentage,
            		"SD_progress_credit"=>$SD_progress_credit,
            		"SD_cal_color_status"=>$SD_cal_color_status,
					
					"DISCOUNT_collection_percentage"=>$DISCOUNT_collection_percentage,
            		"DISCOUNT_progress_credit"=>$DISCOUNT_progress_credit,
            		"DISCOUNT_cal_color_status"=>$DISCOUNT_cal_color_status,
					
					"cr_note_collection_percentage"=>$cr_note_collection_percentage,
            		"cr_note_progress_credit"=>$cr_note_progress_credit,
            		"cr_note_cal_color_status"=>$cr_note_cal_color_status,
					
					"SCHEME_debit_percentage"=>$SCHEME_debit_percentage,
            		"SCHEME_progress_debit"=>$SCHEME_progress_debit,
            		"SCHEME_debit_color_status"=>$SCHEME_debit_color_status,
					
					"lastYear_CHG_collection_percentage"=>$lastYear_CHG_collection_percentage,
            		"lastYear_CHG_progress_credit"=>$lastYear_CHG_progress_credit,
            		"lastYear_CHG_cal_color_status"=>$lastYear_CHG_cal_color_status,
            		"lastYear_CHG_debit_percentage"=>$lastYear_CHG_debit_percentage,
            		"lastYear_CHG_progress_debit"=>$lastYear_CHG_progress_debit,
            		"lastYear_CHG_debit_color_status"=>$lastYear_CHG_debit_color_status, 
					
					"energyTargetColl"=>$energyTargetColl
            	 
            ); 
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );

            $arr = array(
                "meta" => $meta,
                "data" => $data
            ); 
            
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function templateAction()
    {
        try
        {
            $request = $this->getRequest();
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function sumAmountAction()
    {
        try
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            if($site_id=="ALL" || !$site_id)
            {
                $site_id = NULL;
            }
            $consumersMapper = new Application_Model_ConsumersMapper();
            $charged_act_amount  = $consumersMapper->getTotalCollectedAmount($site_id);
             
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $received_act_amount = $cashRegisterMapper->getTotalCollectedActivation($site_id);
            $data = array(
                "charged_act_amount" => intval($charged_act_amount),
                "received_act_amount" => intval($received_act_amount),
               );
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );

            $arr = array(
                "meta" => $meta,
                "data" => $data
            ); 
            
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function sumMonthAmountAction()
    {
        try
        {
           $request = $this->getRequest();
           $site_id = $request->getParam("site_id");
            if($site_id=="ALL" || !$site_id)
            {
                $site_id = NULL;
            }
             $cashRegisterMapper = new Application_Model_CashRegisterMapper();
             $data=array();
            for($i=5;$i>=0;$i--){
                $zendDate = new Zend_Date();
                $zendDate->setTimezone("Asia/Calcutta");
                 $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_NAME);
                 $data[$month]['Debit'] = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"DEBIT"));
                 $data[$month]['Credit'] = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"CREDIT"));
            }
           
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );

            $arr = array(
                "meta" => $meta,
                "data" => $data
            ); 
            
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function outstandingAmountAction()
    {
        try
        {
           $request = $this->getRequest();
           $site_id = $request->getParam("site_id");
            if($site_id=="ALL" || !$site_id)
            {
                $site_id = NULL;
            }
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $amount=$cashRegisterMapper->getOutstandingAmount($site_id);
            
            $data = array(
                "credit" => intval($amount['credit']),
                "debit" => intval($amount['debit']),
               );
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );

            $arr = array(
                "meta" => $meta,
                "data" => $data
            ); 
            
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function consumerTrackingWidgetAction()
    {
        try
        {
           $request = $this->getRequest();
           $site_id = $request->getParam("site_id");
           $state_id = $request->getParam("state_id");
		   $year = $request->getParam("year");
		   $month = $request->getParam("month");
           if($site_id=="ALL")
           {
           	$site_id = NULL;
           }
           if($state_id=="ALL")
           {
           	$state_id = NULL;
           }
           $consumersMapper = new Application_Model_ConsumersMapper();
          
           $consumers_count =0;
           $auth=new My_Auth('user');
           $user=$auth->getIdentity()->user_role;
           
           $roleSession = new Zend_Session_Namespace('roles');
           $role_sites_id=$roleSession->site_id;
           
          if($site_id !=NULL)
          {
		  if($year  !=NULL && $month  !=NULL )
          	$consumers_count = count($consumersMapper->getConsumersByColumnValue("site_id", $site_id,false,$year,$month,TRUE));
		  else
			$consumers_count = count($consumersMapper->getConsumersByColumnValue("site_id", $site_id,false));
          }
          else{
		  if($year !=NULL && $month !=NULL )
		  {
          	$consumers_count = count($consumersMapper->getAllConsumers(true,$site_id,$state_id,$role_sites_id,Null,$year,$month,true));  
			}
		  else
		  {
			$consumers_count = count($consumersMapper->getAllConsumers(true,$site_id,$state_id,$role_sites_id));
			}
          }
           
           $sitesMapper = new Application_Model_SitesMapper();
           if($state_id!=null){
           	$sites_count = count($sitesMapper->getSiteByStateId($state_id));
           }else{
           $sites_count = ($user=="1")?count($role_sites_id)-1:count($role_sites_id);
           }
            $data = array(
                "consumer_count" => $consumers_count,
                "sites_count" => $sites_count,
               );
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );

            $arr = array(
                "meta" => $meta,
                "data" => $data
            ); 
            
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function clusterOutstandingWidgetAction()
    {
        try
        {
           $clustersMapper = new Application_Model_ClustersMapper();
           $clusters = $clustersMapper->getAllClusters();
           
           $sitesMapper = new Application_Model_SitesMapper();
           $cashRegisterMapper = new Application_Model_CashRegisterMapper();
           foreach($clusters as $cluster)
           {
               $cluster_id = $cluster->__get("cluster_id");
               $sites = $sitesMapper->getSiteByClusterId($cluster_id);
               $cluster_outstanding = 0;
               if(!$sites)
               {
                   $sites = array();
               }
               foreach($sites as $site)
               {
                   $site_id = $site->__get("site_id");
                   $arr = $cashRegisterMapper->getOutstandingAmount($site_id);
                   $site_outstanding = $arr["debit"] - $arr["credit"];
                   $cluster_outstanding += $site_outstanding;
               }
               $data[] = array(
                   "cluster_name" => $cluster->__get("cluster_name"),
                   "cluster_outstanding" => $cluster_outstanding
               );
           }
           
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );

            $arr = array(
                "meta" => $meta,
                "data" => $data
            ); 
            
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
   
    public function pendingNotificationAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$mode=$request->getParam("mode");
			$cashRegisterMapper= new Application_Model_CashRegisterMapper();
			$duplicateData=null;
			$duplicateData= $cashRegisterMapper->pendingNotifications($mode);
		
			
			
			foreach ($duplicateData as $cash_register) {
				$consumersMapper = new Application_Model_ConsumersMapper();
				$consumer_id = $cash_register->__get("consumer_id");
				$consumer = $consumersMapper->getConsumerById($consumer_id);
			
				$sitesMapper = new Application_Model_SitesMapper();
				$site = $sitesMapper->getSiteById($consumer->__get("site_id"));
			
				$collection_agent_id = $cash_register->__get("user_id");
				$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
				$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($collection_agent_id); 
				$cr_type = $cash_register->__get("cr_entry_type");
				$cr_type_short = "";
				if (strlen($cr_type) > 0) {
					$cr_type_short = $cr_type[0];
				}
				$date = new Zend_Date($cash_register->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
				//$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("MMM dd, yyyy HH:mm");
				$dataArray = array(
						'cr_id' => $cash_register->__get("cr_id"),
						'consumer_id' => $consumer_id,
						"consumer_name" => $consumer->__get("consumer_name"),
						"consumer_connection_id" => $consumer->__get("consumer_connection_id"),
						'user_id' => $cash_register->__get("user_id"),
						'receipt_number' => $cash_register->__get("receipt_number"),
						'transaction_id' => $cash_register->__get("transaction_id"),
						'cr_entry_type' => $cr_type_short,
						'cr_entry_type_long' => $cr_type,
						'cr_amount' => $cash_register->__get("cr_amount"),
						'timestamp' => $timestamp,
						'site_id' => $site->__get("site_id"),
						'site_name' => $site->__get("site_name"),
					 	'collection_agent_id' => $collectionAgent->__get("collection_agent_id"),
						'agent_fname' => $collectionAgent->__get("agent_fname"),
						'agent_lname' => $collectionAgent->__get("agent_lname"), 
						'cr_status' => $cash_register->__get("cr_status"),
				);
			
				$data[] = $dataArray;
			}
			
    		/*  $data = array(
    				"duplicates" => $duplicates,
    			
    		);  */
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS",
    				"current_page"=> 1,
    				"greater_than_date"=> null,
    				"total_page"=> 606
    		);
    	
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    	
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage(),
    				"current_page"=> 1,
    				"greater_than_date"=> null,
    				"total_page"=> 606
    		);
    	
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    public function deleteDuplicateTransactionByIdAction(){
    
    	try {
    		$request=$this->getRequest();
    		$cr_id=$request->getParam("id");
    
    		$cashMapper=new Application_Model_CashRegisterMapper();
    		$cr_id_value=explode(",", $cr_id);
    		if($cash=$cashMapper->deleteDuplicateTransactionById($cr_id_value)){
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while deleting"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }

    public function getConsumersWithActDateAction(){
    	try {
	    	$request=$this->getRequest();
	    	$months=$request->getParam("month");
	    	$year=$request->getParam("year");
	    	$site_id = $request->getParam("site_id");
	    	$state_id = $request->getParam("state_id");
	    	$firstDate= $request->getParam("first_date");
	    	$last_date= $request->getParam("last_date");
	    	
	    	$roleSession = new Zend_Session_Namespace('roles');
	    	$role_sites_id=$roleSession->site_id;
	    	
	    	if($site_id=='ALL'){
	    		$site_id=NULL;
	    	}
	    	if($state_id=='ALL'){
	    		$state_id=NULL;
	    	}
	    	if($firstDate=='NULL'){
	    		$firstDate=NULL;
	    		$last_date=NULL;
	    	}else{
	    		$zendDate = new Zend_Date($firstDate,"MMMM D, YYYY");
	    		$firstDate = $zendDate->toString("yyyy-MM-dd");
	    		
	    		$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
	    		$last_date = $zendDate->toString("yyyy-MM-dd");
	    	}
	    	$month = date('m', strtotime($months));
	    	$consumerMapper=new Application_Model_ConsumersMapper();
	    	$packageMapper=new Application_Model_PackagesMapper();
	    	$consumers=$consumerMapper->getConsumersWithActDate($month,$year,$site_id,$state_id,$role_sites_id,$firstDate,$last_date);
	    	$consumer_data=array();
	    	if($consumers){
	    	foreach ($consumers as $consumer){
	    		$packageName=$packageMapper->getPackageById($consumer->__get("package_id"));
	    		$date = new Zend_Date($consumer->__get("consumer_act_date"),"yyyy-MM-dd HH:mm:ss");
	    		$timestamp = $date->toString("MMM dd, yy");
	    		$data=array(
	    				"consumer_id"=>$consumer->__get("consumer_id"),
	    				"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
	    				"consumer_name"=>$consumer->__get("consumer_name"),
	    				"package_name"=>$packageName->__get("package_name"),
	    				"date"=>$timestamp
	    		);
	    		$consumer_data[]=$data;
	    	}
		    	$meta = array(
		    			"code" => 200,
		    			"message" => "SUCCESS"
		    	);
		    	$arr = array(
		    			"meta" => $meta,
		    			"data" => $consumer_data
		    	
		    	);
	    	} else {
	    		$meta = array(
	    				"code" => 401,
	    				"message" => "No Record Found"
	    		);
	    		$arr = array(
	    				"meta" => $meta,
	    				"data"=>$consumer_data
	    		);
	    	}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function getConsumersPackageStatusAction(){
    	try {
    		$request=$this->getRequest();
    		$months=$request->getParam("month");
    		$year=$request->getParam("year");
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$firstDate= $request->getParam("first_date");
    		$last_date= $request->getParam("last_date");
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    
    		if($site_id=='ALL'){
    			$site_id=NULL;
    		}
    		if($state_id=='ALL'){
    			$state_id=NULL;
    		}
    		if($firstDate=='NULL'){
    			$firstDate=NULL;
    			$last_date=NULL;
    		}else{
	    		$zendDate = new Zend_Date($firstDate,"MMMM D, YYYY");
	    		$firstDate = $zendDate->toString("yyyy-MM-dd");
	    		
	    		$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
	    		$last_date = $zendDate->toString("yyyy-MM-dd");
	    	}
    		$month = date('m', strtotime($months));
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$packageMapper=new Application_Model_PackagesMapper();
    		$consumer_data=array();
    		$consumers=$consumerMapper->getConsumersPackageStatus($month,$year,$site_id,$state_id,$role_sites_id,$firstDate,$last_date);
    		if($consumers){
    			foreach ($consumers as $consumer){
    				if($consumer->__get("old_package_id")!=NULL){
    					$oldPackage=$packageMapper->getPackageById($consumer->__get("old_package_id"));
    					
    				 $packageCost=$consumer->__get("package_cost");
    				 $old_packageCost=$oldPackage->__get("package_cost");
	    				if($old_packageCost>$packageCost)
	    				{
	    					$packageStatus="Up";
	    					$status='label-info';
	    				}else{
	    					$packageStatus="Down";
	    					$status='label-warning';
	    				}
	    				$data=array(
	    						"consumer_id"=>$consumer->__get("consumer_id"),
	    						"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
	    						"package_name"=>$consumer->__get("package_name"),
	    						"old_package_name"=>$oldPackage->__get("package_name"),
	    						"ActionStatus"=>$packageStatus,
	    						"status"=>$status,
	    						"date"=>$consumer->__get("package_change_date")
	    				);
	    				array_push($consumer_data,$data);
	    				 
    				}
    			}
    			
    		
    		}
    		$consumersStatus=$consumerMapper->getConsumerStatus($month,$year);
    		if($consumersStatus){
    		foreach ($consumersStatus as $consumerStatus){
    			$message_arr=explode("Consumer ID: ",$consumerStatus["message"] ) ;
    		    $mes=explode(" By",$message_arr[1]);
    		    $consumerCheck=$consumerMapper->getConnectionIdDetails($mes[0],$site_id,$state_id,$role_sites_id);
    		    if($consumerCheck){
    		    	 if (strpos($consumerStatus["message"], 'active from banned') !== false){
    		    	 	$from='Banned';
    		    	 	$to='Active';
    		    	 	$status="label-success";
    		    	 	$ActiveStatus="Reconnected";
    		    	 }else{
    		    	 	$from='Active';
    		    	 	$to='Banned';
    		    	 	$status="label-danger";
    		    	 	$ActiveStatus="Disconnected";
    		    	 }
    		    	$condata=array(
    		    			"consumer_id"=>$consumerCheck["consumer_id"],
    		    			"consumer_connection_id"=>$mes[0],
    		    			"package_name"=>$to,
    		    			"old_package_name"=>$from,
    		    			"ActionStatus"=>$ActiveStatus,
    		    			"status"=>$status,
    		    			"date"=>$consumerStatus["timestamp"]
    		    	);
    		  	 array_push($consumer_data,$condata);
    		    }
    		}
    		}
    		//print_r($consumer_data);
    		if($consumers||$consumersStatus){
    			$sort_arr = array();
    			foreach ($consumer_data as $key => $row)
    			{
    				$sort_arr[$key] = $row['date'];
    			}
    			array_multisort($sort_arr, SORT_DESC ,$consumer_data);
    			
    		    $meta = array(
	    				"code" => 200,
	    				"message" => "SUCCESS"
	    		);
	    		$arr = array(
	    				"meta" => $meta,
	    				"data" => $consumer_data
	    				 
	    		);
    		}else{
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while deleting"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data"=>$consumer_data
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
	public function getConsumptionGenerationAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$year=$request->getParam("year");
    		$month=$request->getParam("month");
    		$month_val = date("m", strtotime($month));
    		 
    		$site_id=($site_id=='ALL')?NULL:$site_id;
    		$state_id=($state_id=='ALL')?NULL:$state_id;
    		 
    		$mppt_readingMapper = new Application_Model_MpptsReadingMapper();
    		$feedarMapper=new Application_Model_SiteMeterReadingMapper();
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    	 
    		$data=array();$consumer_data=array();
    		 
    		$gen_month=$mppt_readingMapper->getTotalMpptReadingconsumerID($state_id,$site_id,$role_sites_id,$month,$year);
    		$gen_reading=0;
    		if($gen_month){
    			$gen_reading=intval($gen_month);
    		} 
    		 
    		$con_month=$feedarMapper->getTotalFeedaarReadingFilters($state_id,$site_id,$role_sites_id,$month,$year);
    		$con_reading=0;
    		if($con_month){
    			$con_reading=intval($con_month);
    		} 
    	 	 
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		 
    		$arr = array(
    				"meta" => $meta,
    				"gen" => $gen_reading,
    				"con" => $con_reading,
    				"curr_date" => $month.", ".$year,
    				 
    		);
    	}
    	catch (Exception $e)
    	{
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
}
?>