<?php

class Api_CustomerCommunicationController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');   
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
        $this->_userId=$auth->getIdentity()->user_id;
    }
   
    public function getAllQueriesAction(){
    
        try {
            $queriesMapper = new Application_Model_QueryMapper();
            
            $queries = $queriesMapper->getAllQueries();

            if(count($queries) >0){
                foreach ($queries as $query) {
 /*----------------------------- 29 Jan 2018 (start) ----------------------------*/   
                    $data = array(
                            "query_id" => $query->__get("id"),
                            "query" => $query->__get("query"),
                            "parent_id" => $query->__get("parent_id")                            
                    );
/*----------------------------- 29 Jan 2018 (end) ----------------------------*/       
                    $query_arr[] = $data;
                }
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $query_arr,
                );
    
            }
            else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
    
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }


    public function getAllCustomerQueriesAction(){

        try {
			$request = $this->getRequest();
            $status = $request->getParam("status");
			
            $customerCommunicationMapper = new Application_Model_CustomerCommunicationMapper();
            $roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
            $customer_queries = $customerCommunicationMapper->getAllCustomerQueries($status,$role_sites_id);
			 
			  
            if(count($customer_queries) >0){
                foreach ($customer_queries as $customer_query) {
                    $consumersMapper = new Application_Model_ConsumersMapper();
                    $consumer = $consumersMapper->getConsumerById($customer_query->__get("consumer_id"));

                    $queryMapper = new Application_Model_QueryMapper();
                    $query = $queryMapper->getQueryById($customer_query->__get("query_id"));

                    $sitesMapper = new Application_Model_SitesMapper();
					$statesMapper = new Application_Model_StatesMapper();
 
                    $query_set = $queryMapper->getQueryById($customer_query->__get("query_id"));
                    if($query_set->parent_id != 0){ // have parent
                        $query_id = $query_set->parent_id;
                    }else{
                        $query_id = $customer_query->__get("query_id");  
                    }
                    $siteData = $sitesMapper->getSiteById($customer_query->__get("site_id"));
                    $stateData = $statesMapper->getStateById($customer_query->__get("state_id"));
					
					$userMapper=new Application_Model_UsersMapper();
					$users = $userMapper->getUserById($customer_query->__get("user_id"));
                    $Created_By= $users->__get("user_fname") . " " . $users->__get("user_lname");
					
					 if($customer_query->__get("entry_date")==NULL && $customer_query->__get("entry_date")==""){
						$entry_date=date("m/d/Y",strtotime($customer_query->__get("effective_date")));
						$entry_dates=date("d-M-Y",strtotime($customer_query->__get("effective_date"))); 
					 }else{
						$entry_date=date("m/d/Y",strtotime($customer_query->__get("entry_date")));
						$entry_dates=date("d-M-Y",strtotime($customer_query->__get("entry_date")));
					 }
                    $data = array(
                            "id" => $customer_query->__get("id"),
                            "state_id" => $customer_query->__get("state_id"),
                            "site_id" => $consumer->__get("site_id"),
                            "query_id" => $query_id,
                            "sub_query_id" => $customer_query->__get("sub_query"), 
                            "consumer_id" => $customer_query->__get("consumer_id"),
                            "consumer_name" => $consumer->consumer_name ." (".$consumer->consumer_connection_id.")", 
                            "query" => $query->query,
                            "query_status" => $customer_query->__get("query_status"),
                            "amount" => intval($customer_query->__get("amount")),
                            "attachment" => $customer_query->__get("attachment"),
							"attach_sec" => $customer_query->__get("attach_sec"),
                            "entry_date" => $entry_date,
							"entry_dates" => $entry_dates,
                            "effective_date" => date("m/d/Y",strtotime($customer_query->__get("effective_date"))),
                            "query_comment" => $customer_query->__get("query_comment"),
							"site_name" => $siteData->__get("site_name"), 
                            "state_name" => $stateData->__get("state_name"),  
							"created_by"=>$Created_By
                    );
			
                    $customer_query_arr[] = $data;
                }
				 
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $customer_query_arr,
                );
    
            }
            else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
		//Print_r($arr);exit;
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
        
    }

    public function addCustomerCommunicationAction(){
    
        try {
            $request = $this->getRequest();
            $state_id = $request->getParam("state_id");
            $site_id = $request->getParam("site_id");
            $consumer_id = $request->getParam("consumer_id");
            $query_id = $request->getParam("query_id");
			$sub_query_id = $request->getParam("sub_query_id");
            $query_comment = $request->getParam("query_comment");

			$attachment = $request->getParam("attachment");
			$attach_sec = $request->getParam("attach_sec");
            $amount = $request->getParam("amount");
            
            $tmp_entry_date = $request->getParam("entry_date");
            if($tmp_entry_date!=NULL && $tmp_entry_date!='undefined'){
                $zendDate1 = new Zend_Date($tmp_entry_date,"MM-dd-yyyy");
                $entry_date = $zendDate1->toString("yyyy-MM-dd");
            }
            $tmp_effective_date = $request->getParam("effective_date");
            if($tmp_effective_date!=NULL && $tmp_effective_date!='undefined'){
                $zendDate2 = new Zend_Date($tmp_effective_date,"MM-dd-yyyy");
                $effective_date = $zendDate2->toString("yyyy-MM-dd");
            }
			
            $customerCommunicationMapper = new Application_Model_CustomerCommunicationMapper();
            $customerCommunications = new Application_Model_CustomerCommunication();
			
            $date = new Zend_Date();
            $date->setTimezone("Asia/Calcutta");
            $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
            
            $customerCommunications->__set("state_id",$state_id);
            $customerCommunications->__set("site_id",$site_id);
            $customerCommunications->__set("consumer_id",$consumer_id);
			$customerCommunications->__set("query_id",$query_id);
            $customerCommunications->__set("sub_query_id",$sub_query_id);
			$customerCommunications->__set("user_type","(W)"); 
            $customerCommunications->__set("query_comment",$query_comment);
            $customerCommunications->__set("user_id",$this->_userId);
            $customerCommunications->__set("timestamp",$timestamp);
            $customerCommunications->__set("user_type","(W)");
           
			$customerCommunications->__set("attachment",$attachment);
			$customerCommunications->__set("attach_sec",$attach_sec);

            if(!empty($entry_date)){
                $customerCommunications->__set("entry_date",$entry_date);    
            }
            if(!empty($effective_date)){
                $customerCommunications->__set("effective_date",$effective_date);    
            }
            if(intval($amount)>0){
               $customerCommunications->__set("amount",$amount);
			   $customerCommunications->__set("query_status","Pending"); 
            }else{
			   $customerCommunications->__set("query_status","Approved");
			}
			  
            if($customer_query_id = $customerCommunicationMapper->addNewCustomerCommunication($customerCommunications)){

                $this->_logger->info("New Customer Query ID ".$customer_query_id." has been created in States by ". $this->_userName.".");
                 
                $data=array(
                        "state_id" => $state_id,
                        "site_id" => $site_id,
                        "consumer_id" => $consumer_id,
                        "query_id" => $query_id, 
                        "query_comment" => $query_comment
                );
    
    
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $data
                );
            } else {
                $meta = array(
                        "code" => 401,
                        "message" => "Error while adding"
                );
                $arr = array(
                        "meta" => $meta
                );
            }
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    

    public function deleteCustomerQueryByIdAction(){
    
        try {
            $request = $this->getRequest();
            $customer_query_id = $request->getParam("id");
            $customerCommunicationMapper = new Application_Model_CustomerCommunicationMapper();
            if($customerCommunication = $customerCommunicationMapper->deleteCustomerCommunicationById($customer_query_id)){
                $this->_logger->info("State Id ".$customer_query_id." has been deleted from States by ". $this->_userName.".");
                
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
    
                );
            } else {
                $meta = array(
                        "code" => 401,
                        "message" => "Error while deleting"
                );
                $arr = array(
                        "meta" => $meta
                );
            }
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
	public function updateCustomerCommunicationByIdAction(){
    
        try {
            $request=$this->getRequest();
            $customer_communication_id=$request->getParam("id");
            $customer_communication_state_id=$request->getParam("state_id");
            $customer_communication_site_id=$request->getParam("site_id");
			$customer_communication_consumer_id=$request->getParam("consumer_id");
            $customer_communication_query_id=$request->getParam("query_id");
			$sub_query_id=$request->getParam("sub_query_id");
            $amount=$request->getParam("amount");
			$entry_date=$request->getParam("entry_date");
			$effective_date=$request->getParam("effective_date"); 
			$attachment=$request->getParam("attachment"); 
			$attach_sec=$request->getParam("attach_sec");
            $customer_communication_query_comment=$request->getParam("query_comment");

			if($entry_date!=NULL && $entry_date!='undefined'){
                $zendDate1 = new Zend_Date($entry_date,"MM-dd-yyyy");
                $entry_date = $zendDate1->toString("yyyy-MM-dd");
            }
            if($effective_date!=NULL && $effective_date!='undefined'){
                $zendDate2 = new Zend_Date($effective_date,"MM-dd-yyyy");
                $effective_date = $zendDate2->toString("yyyy-MM-dd"); 
            }
             
            $customercommunicationMapper = new Application_Model_CustomerCommunicationMapper();
            $customercommunications = new Application_Model_CustomerCommunication();
            
            $customercommunications->__set("id",$customer_communication_id);
            $customercommunications->__set("state_id",$customer_communication_state_id);
            $customercommunications->__set("site_id",$customer_communication_site_id);
			$customercommunications->__set("consumer_id",$customer_communication_consumer_id);
            $customercommunications->__set("query_id",$customer_communication_query_id);
			$customercommunications->__set("sub_query_id",$sub_query_id);
			$customercommunications->__set("amount",$amount);
			$customercommunications->__set("entry_date",$entry_date);
			$customercommunications->__set("effective_date",$effective_date);
            $customercommunications->__set("query_comment",$customer_communication_query_comment);
			$customercommunications->__set("attachment",$attachment);
			$customercommunications->__set("attach_sec",$attach_sec);
			
            if($customercommunicationMapper->updateCustomerCommunication($customercommunications)){
                
				$this->_logger->info("State ID ".$customer_communication_state_id[0]." has been updated by ". $this->_userName.".");
                 
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
    
                ); 
            } else {
                $meta = array(
                        "code" => 401,
                        "message" => "Error while updating"
                );
                $arr = array(
                        "meta" => $meta
                );
            }
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	 
    public function updateCustomerCommunicationStatusByIdAction(){

        try {
            $request = $this->getRequest();
            $customer_communication_id = $request->getParam("id");
			$remark = $request->getParam("remark");
			$type = $request->getParam("type");
			$status = $request->getParam("status"); 
            $user_id = $this->_userId;

            $date = new Zend_Date(); 
            $date->setTimezone("Asia/Calcutta");
            $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");

            $customercommunicationMapper = new Application_Model_CustomerCommunicationMapper();
			$comm_id=explode(",",$customer_communication_id);
			if(count($comm_id)>0){
				for($i=0;$i<count($comm_id);$i++){
					
					$comms=$customercommunicationMapper->getConsumerConnectionById($comm_id[$i]); 
					$customercommunications = new Application_Model_CustomerCommunication();
					
					$customercommunications->__set("query_status",$status);
					$customercommunications->__set("verified_by", $user_id);
					$customercommunications->__set("verified_date", $timestamp); 
					$customercommunications->__set("id",$comm_id[$i]);
					
					if($customercommunicationMapper->updateCustomerCommunicationStatus($customercommunications)){
						
						$this->_logger->info("Consumer Comm ID ".$comm_id[$i]." has been updated by ". $this->_userName.".");
						$amount=intval($comms->__get("amount"));
						$consumer_id=$comms->__get("consumer_id");
						if($status=='Approved'){
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();  
							$con_packageMapper=new Application_Model_ConsumersPackageMapper();
							
							$AllPackages=$con_packageMapper->getPackageByConsumerId($consumer_id);
							
							$package_id = $AllPackages[0]["package_id"];
									 
							$dates = new Zend_Date();
							$dates->setTimezone("Asia/Kolkata");
							$timestamp = $dates->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
							
							$cr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
							$transaction_id = $timestamp . "-" . $cr_amount;
							
							$cashRegister = new Application_Model_CashRegister();
							$cashRegister->__set("user_id", $user_id);
							$cashRegister->__set("consumer_id", $consumer_id); 
							$cashRegister->__set("cr_entry_type", "DISCOUNT");
							$cashRegister->__set("cr_amount", $amount);
							$cashRegister->__set("receipt_number", 'CA-'.$user_id);
							$cashRegister->__set("transaction_id", $transaction_id);
							$cashRegister->__set("transaction_type", "(W)");
							$cashRegister->__set("timestamp", $dates->toString("yyyy-MM-dd HH:mm:ss")); 
							$cashRegister->__set("entry_status", $type);
							$cashRegister->__set("remark", $remark); 
							$cashRegister->__set("package_id", $package_id);
							$cashRegisterMapper->addNewCashRegister($cashRegister);  
						} 	 
						$meta = array(
								"code" => 200,
								"message" => "SUCCESS"
						);
						$arr = array(
								"meta" => $meta,
			
						);
					}
				}
			}else{
				$meta = array(
					"code" => 300,
					"message" => "FAILURE"
				);
				$arr = array(
					"meta" => $meta,
				);
			}
			
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
}