<?php
require_once '../PHPExcel/PHPExcel/IOFactory.php';
require_once "../PHPExcel/PHPExcel.php";
gc_enable() ;gc_collect_cycles();
class Api_ModifyEntryController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
		$this->_user_id=$auth->getIdentity()->user_id;
    }
    
        public function deleteTransactionAction(){
      
         try {
         	  $request=$this->getRequest();
        	 $csvFile=$request->getParam("delete_file");
          
            
            defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
            $filename = PUBLIC_PATH . "/html/angular_app/components/modify-entry/csv/".$csvFile;
             
            $file = fopen($filename,"r");
            $data=array();$check=array();
            while(! feof($file))
            {
            	$data[]= fgetcsv($file);
            	
            	 
            }
            
             foreach ($data as $datas){
             	$check[]=$datas[0];
             }
             $cashRegister =new Application_Model_CashRegisterMapper();
             $temp=0;$fail=array();
             for ($i=1;$i<count($check);$i++){
             	$crs=$cashRegister->deleteCashRegisterByTransactionID($check[$i]);
             	if(!$crs){
             		$temp=1;
             		$fail[]=$check[$i];
             	}
             	
             }
             
					unlink($filename);
			 
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                     "meta" => $meta,
                     "data"=>$fail
                );
           
           
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
        }
        
        public function addTransactionAction(){
        
        	try {
        		$request=$this->getRequest();
        		$csvFile=$request->getParam("add_file");
				 
        		defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        		$filename = PUBLIC_PATH . "/html/angular_app/components/modify-entry/csv/".$csvFile;
        		 
        		$file = fopen($filename,"r");
        		$data=array();$check=array();
        		while(! feof($file))
        		{
        			$data[]= fgetcsv($file);
        		}
				
        		$consumer_id=array();
        		$cr_amount=array();
        		$cr_type=array();
        		$entry_type=array();
        		$date_time=array();
        		foreach ($data as $datas){
        			$consumer_id[]=$datas[0]; 
        			$cr_amount[]=$datas[1];
        			$cr_type[]=$datas[2];
        			$entry_type[]=$datas[3];
        			$date_time[]=$datas[4];
        		}
        		
        		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
        		$consumersmapper=new Application_Model_ConsumersMapper(); 
				 $consumerpackageMapper=new Application_Model_ConsumersPackageMapper();
        		$temp=0;$fail=array();
				  
        		for ($i=1;$i<count($consumer_id);$i++){
        			if("$consumer_id[$i]"!="" && "$consumer_id[$i]"!=NULL){
        				$consumer_del=$consumersmapper->getConsumersByConnectionId("$consumer_id[$i]");
						//echo $consumer_del[0]->__get("consumer_id");exit;
						
        				if($consumer_del){
        				
        				$date = new Zend_Date("$date_time[$i]",'dd-MM-yyyy HH:mm:ss');
        				$timestamp = $date->toString("ddMMYYss".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));
						
                        $cr_amt = str_pad($cr_amount[$i], 4, "0", STR_PAD_LEFT);
                        $transaction_id = $timestamp . "-" . $cr_amt;
                        
                        $dates = new Zend_Date("$date_time[$i]",'dd-MM-yyyy HH:mm:ss');
                        
                        $timestamp_val = $dates->toString("yyyy-MM-dd HH:mm:ss");
                        
                        $cashRegister = new Application_Model_CashRegister();
						$consumer_pack=$consumerpackageMapper->getPackageByConsumerId($consumer_del[0]->__get("consumer_id"));
                        $cashRegister->__set("user_id", $this->_user_id);
						$cashRegister->__set("transaction_type", '(W)');
						$cashRegister->__set("user_val", $this->_user_id);
                        $cashRegister->__set("consumer_id", $consumer_del[0]->__get("consumer_id"));
                        $cashRegister->__set("cr_entry_type", $cr_type[$i]);
						$cashRegister->__set("entry_status", $entry_type[$i]);
                        $cashRegister->__set("cr_amount", $cr_amount[$i]);
                        $cashRegister->__set("receipt_number", 0);
                        $cashRegister->__set("transaction_id", $transaction_id);
                        $cashRegister->__set("timestamp", $timestamp_val);
						$cashRegister->__set("package_id", $consumer_pack[0]["package_id"]);
						$cashRegister->__set("approved_by",0);
						//print_r($cashRegister); 
						$crs= $cashRegisterMapper->addNewCashRegister($cashRegister,null,'pending');
	        			if(!$crs){
	        				$temp=1;
	        				$fail[]=$check[$i];
	        			}
					
                        
        			}
        		  }
        		}
        		    
					//unlink($filename);
				 //echo "success"; 
        		$meta = array(
        				"code" => 200,
        				"message" => "SUCCESS"
        		);
        		$arr = array(
        				"meta" => $meta,
        				"data"=>$fail
        		);
        		 
        		 
        	}catch (Exception $e) {
        		$meta = array(
        				"code" => 501,
        				"messgae" => $e->getMessage()
        		);
        
        		$arr = array(
        				"meta" => $meta
        		);
        	}
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        }

   		public function inactiveTransactionAction(){
        
        	try {
        		$request=$this->getRequest();
        		$csvFile=$request->getParam("inactive_file");
        
        
        		defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        		$filename = PUBLIC_PATH . "/html/angular_app/components/modify-entry/csv/".$csvFile;
        		 
        		$file = fopen($filename,"r");
        		$data=array();$check=array();
        		while(! feof($file))
        		{
        			$data[]= fgetcsv($file);
        			 
        
        		}
        
        		foreach ($data as $datas){
        			$check[]=$datas[0];
        		}
        		$cashRegister =new Application_Model_CashRegisterMapper();
        		$temp=0;$fail=array();
        		for ($i=1;$i<count($check);$i++){
        			$cr_id=$cashRegister->getCrByTxnId($check[$i]);
        			if($cr_id){
						$crs=$cashRegister->updateStatus($cr_id["cr_id"],"INACTIVE",null);
						if(!$crs){
							$temp=1;
							$fail[]=$check[$i];
						}
        			} 
        		 }
        		 unlink($filename);
				 
        		$meta = array(
        				"code" => 200,
        				"message" => "SUCCESS"
        		);
        		$arr = array(
        				"meta" => $meta,
        				"data"=>$fail
        		);
        		 
        		 
        	}catch (Exception $e) {
        		$meta = array(
        				"code" => 501,
        				"messgae" => $e->getMessage()
        		);
        
        		$arr = array(
        				"meta" => $meta
        		);
        	}
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        }
        
		public function updateTransactionAction(){
        
        	try {
        		$request=$this->getRequest();
        		$csvFile=$request->getParam("update_file");
        
        		defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        		$filename = PUBLIC_PATH . "/html/angular_app/components/modify-entry/csv/".$csvFile;
        		 
        		$file = fopen($filename,"r");
        		$data=array();$check=array();
        		while(! feof($file))
        		{
        			$data[]= fgetcsv($file);
                }
                
        		$txn_id=array();
        		$cr_amount=array();
        		$cr_type=array();
        		$date_time=array();
				$transaction_type=array();
        		foreach ($data as $datas){
					$txn_id[]=$datas[0];
        			$cr_type[]=$datas[2]; 
        			$cr_amount[]=$datas[1];
					$date_time[]=$datas[3];
					$transaction_type[]=$datas[4];
        		}
        		 
        		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
        		$consumersmapper=new Application_Model_ConsumersMapper();
        		$temp=0;$fail=array();
        		for ($i=1;$i<count($txn_id);$i++){
				 
        			if($txn_id[$i]!="" && $txn_id[$i]!=NULL){
						 
        			  $cr_id=$cashRegisterMapper->getCrByTxnId($txn_id[$i]);
					    if($cr_id){
							if($transaction_type[$i]=='update'){	 
								$date = new Zend_Date("$date_time[$i]",'dd-MM-yyyy HH:mm:ss');
								$timestamp = $date->toString("ddMMYYss".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));
			
								$cr_amt = str_pad($cr_amount[$i], 4, "0", STR_PAD_LEFT);
								$transaction_id = $timestamp . "-" . $cr_amt;
							}else{
								$transaction_id = $txn_id[$i];
							}
        					$dates = new Zend_Date("$date_time[$i]",'dd-MM-yyyy HH:mm:ss');
        				 
        					 $timestamp_val = $dates->toString("yyyy-MM-dd HH:mm:ss");
        					 
        					 	$cr=new Application_Model_CashRegister();
								 
								$cr->__set("cr_id", $cr_id["cr_id"]);
								$cr->__set("cr_amount", $cr_amount[$i]);
								$cr->__set("consumer_id", $cr_id["consumer_id"]);
								$cr->__set("user_id", $cr_id["user_id"]);
								$cr->__set("transaction_id", "$transaction_id");
								$cr->__set("cr_entry_type", "$cr_type[$i]");
								$cr->__set("timestamp","$timestamp_val");
								$crs= $cashRegisterMapper->updateCashRegister($cr); 
        					if(!$crs){
        						$temp=1;
        						$fail[]=$check[$i];
        					}
        				}
        			}
        		}
				 
					unlink($filename);
				 
        		$meta = array(
        				"code" => 200,
        				"message" => "SUCCESS"
        		);
        		$arr = array(
        				"meta" => $meta,
        				"data"=>$fail
        		);
        		 
        		 
        	}catch (Exception $e) {
        		$meta = array(
        				"code" => 501,
        				"messgae" => $e->getMessage()
        		);
        
        		$arr = array(
        				"meta" => $meta
        		);
        	}
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        }      

		public function bankStatementAction(){
        
        	try {
        		$request=$this->getRequest();
        		$csvFile=$request->getParam("statement_file");
				 
        		defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        		$filename = PUBLIC_PATH . "/html/angular_app/components/modify-entry/csv/".$csvFile;
        		$RenameFile=$csvFile;
				$tmpfname = $filename;
				$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
				$excelObj = $excelReader->load($tmpfname);
				$worksheet = $excelObj->getSheet(0);
				$lastRow = $worksheet->getHighestRow();
					
				$i=0;
				$error_row=array();
				$bankStatementMapper=new Application_Model_BankStatementMapper();
				for ($row = 2; $row <= $lastRow; $row++) {
					echo $tran_date=$worksheet->getCell('A'.$row)->getCalculatedValue();exit;
					$value_date=$worksheet->getCell('B'.$row)->getCalculatedValue();
					$description=$worksheet->getCell('C'.$row)->getCalculatedValue();
					$ref_cheque_no=$worksheet->getCell('D'.$row)->getCalculatedValue();
					$branch_code=$worksheet->getCell('E'.$row)->getCalculatedValue();
					$debit=$worksheet->getCell('F'.$row)->getCalculatedValue();
					$credit=$worksheet->getCell('G'.$row)->getCalculatedValue();
					$balance=$worksheet->getCell('H'.$row)->getCalculatedValue();
					
					$tran_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($tran_date));
					$value_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($value_date));
					
					$bankStatement=new Application_Model_BankStatement();
					$bankStatement->__set("tran_date",$tran_date);
					$bankStatement->__set("value_date",$value_date);
					$bankStatement->__set("description",$description);
					$bankStatement->__set("ref_cheque_no",$ref_cheque_no);
					$bankStatement->__set("branch_code",$branch_code);
					$bankStatement->__set("debit",$debit);
					$bankStatement->__set("credit",$credit);
					$bankStatement->__set("balance",$balance);
					$bankStatement->__set("deposit_id",0);
					print_r($bankStatement);
					//$bankStatementMapper->addNewBankStatement($bankStatement); 
				}
				//rename("/tmp/tmp_file.txt", "/home/user/login/docs/my_file.txt");
        		$meta = array(
        				"code" => 200,
        				"message" => "SUCCESS"
        		);
        		$arr = array(
        				"meta" => $meta,
        				"data"=>$fail
        		);
        		 
        		 
        	}catch (Exception $e) {
        		$meta = array(
        				"code" => 501,
        				"messgae" => $e->getMessage()
        		);
        
        		$arr = array(
        				"meta" => $meta
        		);
        	}
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        }
}
 
 