<?php

class Api_AgentSiteMappingsController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }

    public function addAgentSiteMappingAction() {

        try {
            $request = $this->getRequest();
            $collection_agent_id = $request->getParam("agent_id");
            $site_id = $request->getParam("site_id");
            $agent_site_mappingmapper = new Application_Model_AgentSiteMappingsMapper();
            $agent_site_mapping = new Application_Model_AgentSiteMappings();
            $agent_site_mapping->__set("collection_agent_id", $collection_agent_id);
            $agent_site_mapping->__set("site_id", $site_id);

            if ($agent_site_mapping = $agent_site_mappingmapper->addNewAgentSiteMapping($agent_site_mapping)) {
                $siteMapper = new Application_Model_SitesMapper();
                $agentMapper = new Application_Model_CollectionAgentsMapper();
                $site_name = $siteMapper->getSiteById($site_id);
                $agent_name = $agentMapper->getCollectionAgentById($collection_agent_id);
                $data = array(
                    "collection_agent_id" => $collection_agent_id,
                    "agent_site_mapping_name" => $site_id,
                    "agent_name" => $agent_name->__get("agent_fname") . " " . $agent_name->__get("agent_lname"),
                    "site_name" => $site_name->__get("site_name"),
                    "agent_site_mapping_id" => $agent_site_mapping,
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAgentSiteMappingByIdAction() {

        try {
            $request = $this->getRequest();
            $agent_site_mapping_id = $request->getParam("id");
            $agent_site_mappingmapper = new Application_Model_AgentSiteMappingsMapper();
            if ($agent_site_mapping = $agent_site_mappingmapper->getAgentSiteMappingById($agent_site_mapping_id)) {
                $siteMapper = new Application_Model_SitesMapper();
                $agentMapper = new Application_Model_CollectionAgentsMapper();
                $site_name = $siteMapper->getSiteById($site_id);
                $agent_name = $agentMapper->getCollectionAgentById($collection_agent_id);


                $data = array(
                    "collection_agent_id" => $agent_site_mapping->__get("collection_agent_id"),
                    "site_id" => $agent_site_mapping->__get("site_id"),
                    "agent_site_mapping_id" => $agent_site_mapping->__get("agent_site_mapping_id"),
                    "agent_name" => $agent_name->__get("agent_fname") . " " . $agent_name->__get("agent_lname"),
                    "site_name" => $site_name->__get("site_name"),
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function deleteAgentSiteMappingByIdAction() {

        try {
            $request = $this->getRequest();
            $agent_site_mapping_id = $request->getParam("id");
            $agent_site_mappingmapper = new Application_Model_AgentSiteMappingsMapper();
            if ($agent_site_mapping = $agent_site_mappingmapper->deleteAgentSiteMappingById($agent_site_mapping_id)) {





                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllAgentSiteMappingsAction() {

        try {
//             $request=$this->getRequest();
//             $agent_site_mapping_id=$request->getParam("id");
            $agent_site_mappingmapper = new Application_Model_AgentSiteMappingsMapper();
            $sitemapper = new Application_Model_SitesMapper();
            $collectionAgent = new Application_Model_CollectionAgentsMapper();
            if ($agent_site_mappings = $agent_site_mappingmapper->getAllAgentSiteMappings()) {

                foreach ($agent_site_mappings as $agent_site_mapping) {

                    $site_name = $sitemapper->getSiteById($agent_site_mapping->__get("site_id"));
                    $collAgent = $collectionAgent->getCollectionAgentById($agent_site_mapping->__get("collection_agent_id"));
                    $data = array(
                        "collection_agent_id" => $agent_site_mapping->__get("collection_agent_id"),
                        "site_id" => $agent_site_mapping->__get("site_id"),
                        "agent_site_mapping_id" => $agent_site_mapping->__get("agent_site_mapping_id"),
                        "site_name" => $site_name->__get("site_name"),
                        "agent_name" => $collAgent->__get("agent_fname") . " " . $collAgent->__get("agent_lname"),
                    );
                    $agent_site_mapping_arr[] = $data;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $agent_site_mapping_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updateAgentSiteMappingByIdAction() {

        try {
            $request = $this->getRequest();
            $agent_site_mapping_id = $request->getParam("id");
            $collection_agent_id = $request->getParam("agent_id");
            $site_id = $request->getParam("site_id");
            $agent_site_mappingmapper = new Application_Model_AgentSiteMappingsMapper();
            $agent_site_mapping = new Application_Model_AgentSiteMappings();
            $agent_site_mapping->__set("agent_site_mapping_id", $agent_site_mapping_id);
            $agent_site_mapping->__set("collection_agent_id", $collection_agent_id);
            $agent_site_mapping->__set("site_id", $site_id);

            if ($agent_site_mappings = $agent_site_mappingmapper->updateAgentSiteMapping($agent_site_mapping)) {


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllCollectionAgentBySiteIdAction() {

        try {
            $request = $this->getRequest();
            $agent_id = $request->getParam("id");
            $collectionmapper = new Application_Model_CollectionAgentsMapper();
            if ($collections = $collectionmapper->getAllCollectionAgentsBySiteId($agent_id)) {

                foreach ($collections as $collectionagent) {


                    $data = array(
                        'agent_mobile' => $collectionagent->__get("agent_mobile"),
                        'agent_fname' => $collectionagent->__get("agent_fname"),
                        'agent_lname' => $collectionagent->__get("agent_lname"),
                        'agent_email' => $collectionagent->__get("agent_email"),
                        'agent_status' => $collectionagent->__get("agent_status"),
                        'site_id' => $collectionagent->__get("site_id"),
                        'agent_balance' => $collectionagent->__get("agent_balance")
                    );
                    $agent_site_mapping_arr[] = $data;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $agent_site_mapping_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

}
