<?php

class Api_ChangePasswordController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
       // header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
        $this->_userId=$auth->getIdentity()->user_id;
    }

    public function updatePasswordAction(){
    	 
    	try {
    		$request=$this->getRequest();
    		$current=$request->getParam("current_password");
    		$new=$request->getParam("new_password");
    		$current_password=sha1($current);
    		$new_password=sha1($new);
    		
    		$Usersmapper=new Application_Model_UsersMapper();
    		
    		$passowrd=$Usersmapper->getUserById($this->_userId);
    		$hashed_password=$passowrd->__get("hashed_password");
    		
    		if($current_password==$hashed_password){
    
    		$pass=$Usersmapper->updatePassword($new_password,$this->_userId);
    		
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while updating"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
}
