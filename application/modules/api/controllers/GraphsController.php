<?php
require('html2fpdf.php');
require('font/makefont/makefont.php');
ini_set('display_errors',0);
class Api_GraphsController extends Zend_Controller_Action {

    public function init(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }
    
	public function indexAction(){
        
    }
	
	public function formatMoney($num) {
		$explrestunits = "" ;
		if(strlen($num)>3) {
			$lastthree = substr($num, strlen($num)-3, strlen($num));
			$restunits = substr($num, 0, strlen($num)-3); 
			$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);
			for($i=0; $i<sizeof($expunit); $i++) {  
				// creates each of the 2's group and adds a comma to the end
				if($i==0) {
					$explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
				} else {
					$explrestunits .= $expunit[$i].",";
				}
			}
			$thecash = $explrestunits.$lastthree;
		} else {
			$thecash = $num;
		}
		return $thecash; // writes the final format where $currency is the currency symbol.
	}
	
    public function packageConsumersCountAction(){
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $state_id = $request->getParam("state_id");
            $packagemapper=new Application_Model_PackagesMapper();
            $consumerMapper=new Application_Model_ConsumersMapper();
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            //$state_id=$auth->getIdentity()->state_id;
            
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            $packages=$packagemapper->getAllPackages(true);
           if($site_id=="ALL"){$site_id=NULL;}
           if($state_id=="ALL"){$state_id=NULL;}
            
            if($packages)
            {
                $cols = array();
                
                $col = array();
                $col["id"] = "package";
                $col["label"] = "Package";
                $col["type"] = "string";
                $cols[] = $col;
                
                $col = array();
                $col["id"] = "consumers_count";
                $col["label"] = "Consumers";
                $col["type"] = "number";
                $cols[] = $col;
                
                $rows = array();
                foreach ($packages as $package) 
                {
                    $cs = array();
                    $package_name = $package->__get("package_name");
                    $consumers=$consumerMapper->getAllConsumerByPackageId($package->__get("package_id"),$site_id,$state_id,$role_sites_id);
					$package_name_short=explode("/", $package_name);
                    $c = $package_name; // $package_name_short[0];
                    $cs["p"] = $c;
					
                    $c = count($consumers);
                    $cs["c"] = $c;
                    
                    $rows[] = $cs;
                    

                }
                $sort_arr = array();
        		foreach ($rows as $key => $row)
        		{
        			$sort_arr[$key] = $row['c'];
        		}
        		array_multisort($sort_arr, SORT_DESC ,$rows);
                $data = array(
                    
                    "rows" => $rows
                );
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data,
                );
            }  
            else 
            {
                $meta = array(
                    "code" => 404,
                    "message" => "Not found"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	public function monthlyBillingRevenueAction(){
        try 
        {
        	date_default_timezone_set('Asia/Kolkata');
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $state_id = $request->getParam("state_id");
            $yearType = $request->getParam("yearType");
			$entry_status = $request->getParam("entry_status");
			$segment = $request->getParam("segment");
			$consumer_type = $request->getParam("consumer_type"); 
	         
			if($segment=="ALL"){ 	$segment = NULL;  }
			if($consumer_type=="ALL"){ 	$consumer_type = NULL; } 
			
            if($entry_status=="" || $entry_status==NULL || $entry_status=="ALL Type"){
				$entry_status=NULL;
				$graph=NULL;
			}else{
				$entry_status=explode(",",$entry_status);
				$graph=true;
			}
            if($state_id=="ALL"){$state_id=NULL;}
            if($site_id=="ALL"){$site_id=NULL;}
            
            $curr_date = $request->getParam("curr_date");
            $last_date = $request->getParam("last_date");
            
            $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
            $curr_date = $zendDate->toString("yyyy-MM-dd");
            
            $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
            $last_date=$zendDate->toString("yyyy-MM-dd");
             
            $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
            $days = round($cal); 
            $m=0;$y=0;
			
            $counter=round($days/30);
            $next='+1 month';
           
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
			$consumerMapper=new Application_Model_ConsumersMapper();
            $revenueMapper=new Application_Model_RevenueTargetMapper();
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            //echo $days." ".$counter.' '; 
            if($yearType=='on'){
            	$data=array();
            	$site_data=array();
            	$secondNext=$last_date;
            	for($i=0;$i<$counter;$i++){
            		
            			$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
            			//echo " ".$secondNext."--".$secondCurr." ";
            			$debit = intval($cashRegisterMapper->getTotalCollectedByMonth(date('Y-m-d', strtotime($secondCurr."-1 day")),$site_id,"DEBIT",$secondNext,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type,$graph));
	                	$credit = intval($cashRegisterMapper->getTotalCollectedByMonth(date('Y-m-d', strtotime($secondCurr."-1 day")),$site_id,"CREDIT",$secondNext,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type,$graph));
						$activation = intval($cashRegisterMapper->getTotalCollectedByMonth(date('Y-m-d', strtotime($secondCurr."-1 day")),$site_id,"ACTIVATION",$secondNext,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type,$graph));
	                	$totalEntryType = $cashRegisterMapper->getTotalCollectedForEntryType(date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,$site_id,$state_id,$role_sites_id,$segment,$consumer_type);
			
						$last_year_date = date('Y-m-d', strtotime($secondNext ." -1 year"));
						$curr_year_rate = date('Y-m-d', strtotime($secondCurr ." -1 year"));
						
						$last_year_debit = intval($cashRegisterMapper->getTotalCollectedByMonth(date('Y-m-d', strtotime($curr_year_rate."-1 day")),$site_id,"DEBIT",$last_year_date,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type,$graph));
	                	$last_year_credit = intval($cashRegisterMapper->getTotalCollectedByMonth(date('Y-m-d', strtotime($curr_year_rate."-1 day")),$site_id,"CREDIT",$last_year_date,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type,$graph));
						$last_year_activation = intval($cashRegisterMapper->getTotalCollectedByMonth(date('Y-m-d', strtotime($curr_year_rate."-1 day")),$site_id,"ACTIVATION",$last_year_date,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type,$graph));
	                	$last_year_totalEntryType = $cashRegisterMapper->getTotalCollectedForEntryType(date('Y-m-d', strtotime($curr_year_rate."-1 day")),$last_year_date,$site_id,$state_id,$role_sites_id,$segment,$consumer_type);
			
						$last_year_debit=$last_year_debit+intval($last_year_totalEntryType["SD"]); 
						
						$debit=$debit+intval($totalEntryType["SD"]);
						$trans_date=date_parse_from_format("Y-m-d", $secondNext);
	                	$month= $trans_date["month"];
	                	$year= $trans_date["year"];
	                	
	                	$dateObj   = DateTime::createFromFormat('!m', $month);
	                	$monthName = $dateObj->format('F');
	                	$comma="'";
	                	if($m==1){$monthName=NULL;$comma=NULL;}
	                	 
	                	$revenue=$revenueMapper->getAllRevenueByMonthYear($monthName,$year,$state_id,$site_id,$role_sites_id);	
	                	 
						$consumers_arpu=$consumerMapper->getActiveConsumerBySite($site_id,$state_id,$role_sites_id,date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,null,$segment,$consumer_type);
						$Total_ARPU=0;$Pre_ARPU=0;$Post_ARPU=0;
						if ($consumers_arpu){
							$totalBilledAmount=$cashRegisterMapper->getOnlyBilledForARPU(date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,$site_id,$state_id,$role_sites_id,$segment,$consumer_type);
								$TotalARPU=$totalBilledAmount["Total_CHG"];
								$PreARPU=$totalBilledAmount["Pre_CHG"];
								$PostARPU=$totalBilledAmount["Post_CHG"];
							 
								$Total_ARPU=round(intval($TotalARPU)/intval($consumers_arpu["all_consumer"]));
								$Pre_ARPU=round(intval($PreARPU)/intval($consumers_arpu["pre_consumer"]));
								$Post_ARPU=round(intval($PostARPU)/intval($consumers_arpu["post_consumer"]));
						   
						} 
						 
	                	$secondNext = date('Y-m-d', strtotime($secondNext.$next));
	                	$monthShotName = $dateObj->format('M');
	              		$Revenue_Amt[] = $credit+$activation;
	                	$months[]=$monthShotName.$comma.substr($year,-2); 
						
						$lastYearMonth=$monthShotName.$comma.substr(($year-1),-2)." - ".$monthShotName.$comma.substr($year,-2);  
						
						if($entry_status!=NULL &&  !(in_array("'SD'", $entry_status))){
							//$credit=$credit-intval($totalEntryType["SD"]); 
							$debit=$debit-intval($totalEntryType["SD"]);
							
							$last_year_debit=$last_year_debit-intval($last_year_totalEntryType["SD"]);
							//$last_year_credit=$last_year_credit-intval($last_year_totalEntryType["SD"]);
						} 
						
	                	$site_data[] = array(
		                		"site_id" => $site_id,
		                		"month"=>$monthShotName.$comma.substr($year,-2),
		                		"credit" => intval($credit),
		                		"debit" => intval($debit),  
								"activation" => intval($activation),
		                		"revenue"=>$revenue,
								"Total_ARPU" => $Total_ARPU,  
								"Pre_ARPU" => $Pre_ARPU,
		                		"Post_ARPU"=>$Post_ARPU,
								"last_year_month"=>$lastYearMonth,
								"last_year_debit" => intval($last_year_debit),
		                		"last_year_credit" => intval($last_year_credit)+intval($last_year_activation),  
								);
                	 
                }
       		}else{
       			$data=array();
       			$site_data=array();
        		$cs = array();
       			$months_val=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
				$j=3;$years=$year;
				for($i=0;$i<12;$i++){
					$month = $months_val[$j];
							 
					if($j==11){
						$j=0;}else{$j++;
					}
					if($j==1){
						$years=$years+1;
					}
	       		
					$debit = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"DEBIT",$years,$state_id,$role_sites_id));
	       			$credit = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"CREDIT",$years,$state_id,$role_sites_id));
	       			$activation = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"ACTIVATION",$years,$state_id,$role_sites_id));
	       		 
	       			$revenue=$revenueMapper->getAllRevenueByMonthYear($month,$years,$state_id,$site_id,$role_sites_id);
	       		
	       			$Revenue_Amt[] = $credit+$activation;
	       			$months[]=$month;
	       			$site_data[] = array(
	       				"site_id" => $site_id,
	       				"month"=>$month,
	       				"credit" => intval($credit),
	       				"debit" => intval($debit),
						"activation" => intval($activation),
	       				"revenue"=>$revenue );
	       	
	       		}
       		}  
       		
	      $revenueData = array();
	      $billingData = array();
	      $revenueTargets=array();
		  
		  $last_year_creditData = array();
	      $last_year_billingData = array();
		  
		  $totalData = array();
	      $PreData = array();
	      $postData=array();
	      foreach($site_data as $site_val)
	      {
	      
	       $revenueData[] = array($site_val["month"], $site_val["credit"]+$site_val["activation"]);
	       $billingData[] = array($site_val["month"], $site_val["debit"]);
	       $revenueTargets[] = array($site_val["month"], $site_val["revenue"]);
 
	       $last_year_creditData[] = array($site_val["last_year_month"], $site_val["last_year_credit"]);
	       $last_year_billingData[] = array($site_val["last_year_month"], $site_val["last_year_debit"]);
		   
		   $totalData[] = array($site_val["month"], $site_val["Total_ARPU"]);
		   $PreData[] = array($site_val["month"], $site_val["Pre_ARPU"]);
		   $postData[] = array($site_val["month"], $site_val["Post_ARPU"]);
	      }
	      
	      $lines=array("lineWidth"=> 1);
	      $revenueArray = array("data" => $revenueData,
	            "label" => "Revenue (CREDIT)",
	            "lines"=>$lines,
	            "shadowSize"=>0,
	            );
	      $billingArray  = array("data" => $billingData,
	            "label" => "Billing (DEBIT)",
	            "lines"=>$lines,
	            "shadowSize"=>0,
	            );
	      $revenueArrays  = array("data" => $revenueTargets,
	      		"label" => "revenue",
	      		"lines"=>$lines,
	      		"shadowSize"=>0,
	      );
		  
			  $last_year_creditDataArray = array(
					"data" => $last_year_creditData,
				);
				
			  $last_year_billingDataArray  = array(
					"data" => $last_year_billingData,
			  );
			  
			$totalArray = array("data" => $totalData);
			$preArray = array("data" => $PreData);
			$postArray = array("data" => $postData);
			
	       array_push($data, $billingArray);
	       array_push($data, $revenueArray);
	       array_push($data, $revenueArrays);
	       array_push($data, $Revenue_Amt);
	       array_push($data, $months);
		   
		   array_push($data, $totalArray);
	       array_push($data, $preArray);
	       array_push($data, $postArray); 
		   
		   array_push($data, $last_year_creditDataArray);
	       array_push($data, $last_year_billingDataArray);  
	       
	      	$meta = array(
	        	"code" => 200,
	        	"message" => "SUCCESS"
	      	);
	    
	      	$arr = array(
	        	"meta" => $meta,
	       	    "data" => $data
	      	);
    
      }
      catch(Exception $e)
      {
      	$meta = array(
         	"code" => $e->getCode(),
      	 	"message" => $e->getMessage()
      	);
    
     	$arr = array(
      	 	"meta" => $meta
      	);
      }
      $json = json_encode($arr, JSON_PRETTY_PRINT);
      echo $json;
    }
   
    public function monthlyConsumersCountAction(){
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $state_id = $request->getParam("state_id");
			$segment = $request->getParam("segment");
            $consumer_type = $request->getParam("consumer_type"); 
	        
            if($site_id=="ALL"){ 	$site_id = NULL;  }
            if($state_id=="ALL"){ 	$state_id = NULL; }
            
			if($segment=="ALL"){ 	$segment = NULL;  }
            if($consumer_type=="ALL"){ 	$consumer_type = NULL; } 
			
              $curr_date = $request->getParam("curr_date");
		      $last_date = $request->getParam("last_date");
		      
		      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		      $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
			  $last_date=$zendDate->toString("yyyy-MM-dd");
		      
		      $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
		      $days = round($cal);
		      $m=0;$y=0;
		      	
		      $counter=round($days/30);
		      $next='+1 month';
            
            $consumersMapper = new Application_Model_ConsumersMapper();
            $logMapper=new Application_Model_LogsMapper();
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            
            $data=array();$val;$consumer_count=array();$months=array();
            $consumer_data=array();
			$prev_count=0;
			$secondNext=$last_date;
            	for($i=0;$i<$counter;$i++){
            		
            		$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
					 
                	$count = $consumersMapper->getTotalConsumersCountByMonth($secondCurr,$secondNext,$site_id,$state_id,$role_sites_id,$segment,$consumer_type);
                	$consumers=$consumersMapper->getActiveConsumerBySite($site_id,$state_id,$role_sites_id,date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext,null,$segment,$consumer_type);
                	$reactives=$logMapper->getConsumerReactiveLogsBydate($secondCurr,$secondNext); 
                	
					$reactive_count=0;
                	if($reactives){
                		foreach ($reactives as $reactive){
                			$arr=explode("Consumer ID: ", $reactive["message"]); 
                			$connection=explode(" ",$arr[1]);
                			$connection_id=$connection[0];
                			$checkReactive=$consumersMapper->checkConnectionId($site_id,$state_id,$role_sites_id,$connection_id,$segment,$consumer_type);
                			if($checkReactive){
                				$reactive_count=$reactive_count+1;
                			}
                		}
                	}
					
                	$trans_date=date_parse_from_format("Y-m-d", $secondNext);
					$month= $trans_date["month"];
					$year= $trans_date["year"];
					
					$dateObj   = DateTime::createFromFormat('!m', $month);
					$monthName = $dateObj->format('M');
					$comma="'";
					if($m==1){$monthName=NULL;$comma=NULL;}
					 
					$secondNext = date('Y-m-d', strtotime($secondNext.$next));
					
            	$consumer_count[] = intval($count);
            	$total_consumer_count[] = intval($consumers["all_consumer"]);
            	$reactive_consumer_count[]=$reactive_count;
            	$months[]=$monthName.$comma.substr($year,-2);  
            }
          
            array_push($data, $consumer_count); 
            array_push($data, $months);
            array_push($data, $total_consumer_count);
            array_push($data, $reactive_consumer_count);
            $meta = array(
            		"code" => 200,
            		"message" => "SUCCESS"
            );
            
            $arr = array(
            		"meta" => $meta,
            		"data" => $data
            );
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
    public function connectionChargeStatsAction(){
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            $state_id=$auth->getIdentity()->state_id;
            
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            
            $consumersMapper = new Application_Model_ConsumersMapper();
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
           
            	$charged_act_amount  = $consumersMapper->getTotalCollectedAmount($site_id,$state_id,$role_sites_id);
            	$received_act_amount = $cashRegisterMapper->getTotalCollectedActivation($site_id,$state_id,$role_sites_id);
            
            $pending_act_amount = $charged_act_amount-$received_act_amount;
            
           /*  $cols = array();
                
            $col = array();
            $col["id"] = "month";
            $col["label"] = "Month";
            $col["type"] = "string";
            $cols[] = $col;

            $col = array();
            $col["id"] = "received";
            $col["label"] = "Received OTP";
            $col["type"] = "number";
            $cols[] = $col;
            
            $col = array();
            $col["id"] = "pending";
            $col["label"] = "Pending OTP";
            $col["type"] = "number";
            $cols[] = $col;
            
            $rows = array();
            $cs = array();
            
            $c = array();
            $c["v"] = "";
            $cs[] = $c;

            $c = array();
            $c["v"] =$received_act_amount; 
            $cs[] = $c;

            $c = array();
            $c["v"] = $pending_act_amount;
            $cs[] = $c;


            $row["c"] = $cs;
            $rows[] = $row; */
            /* $c=array(); */
            $rows = array();
           /*  $c["r"] =$received_act_amount;
            $c["p"]=$pending_act_amount; */
            $rows["r"]=$received_act_amount;
            $rows["p"]=$pending_act_amount;
            $data = array(
//                 "cols" => $cols,
                "rows" => $rows
            );
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $arr = array(
                "meta" => $meta,
                "data" => $data,
            );
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
	public function connectionChargeStatsByMonthAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$graph_type = $request->getParam("type");
    		if($site_id=="ALL"){$site_id=NULL;}
    		if($state_id=="ALL"){$state_id=NULL;}
    		
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		
    		  $curr_date = $request->getParam("curr_date");
		      $last_date = $request->getParam("last_date");
		      
		      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		      $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
              $last_date=$zendDate->toString("yyyy-MM-dd");
		      
		      $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
		      $days = round($cal);
		      $m=0;$y=0;
		      	
		      $counter=round($days/30);
		      $next='+1 month';
           
    		
    		$consumersMapper = new Application_Model_ConsumersMapper();
    		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    		$siteMapper=new Application_Model_SitesMapper();
    		$total_month=array();
    		$cs = array();
    		$css = array();
    		
			$consumers=array();
			if($graph_type=="stack_chart"){
					
    		
    		$secondNext=$last_date;
            	for($i=0;$i<$counter;$i++){
            		
	            	$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
	    				
	    			$charged_act_amount  = $consumersMapper->getTotalCollectedAmount($site_id,$state_id,$role_sites_id,$secondCurr,$secondNext);
	    			$received_act_amount = $cashRegisterMapper->getTotalCollectedActivation($site_id,$state_id,$role_sites_id,$secondCurr,$secondNext);
	    
	    			$pending_act_amount=intval($charged_act_amount)-intval($received_act_amount);
	    			 
	                $consumerCount = $consumersMapper->getTotalConsumersCountByMonth($secondCurr,$secondNext,$site_id,$state_id,$role_sites_id);
	
	                $trans_date=date_parse_from_format("Y-m-d", $secondNext);
	                $month= $trans_date["month"];
	                $year= $trans_date["year"];
	                
	                $dateObj   = DateTime::createFromFormat('!m', $month);
	                $monthName = $dateObj->format('M');
	                $comma=",";
	                if($m==1){$monthName=NULL;$comma=NULL;}
	                 
	                $secondNext = date('Y-m-d', strtotime($secondNext.$next));
	                
	                if($received_act_amount<=0){
	                	$received_act_amount=0;
	                }
	                if($pending_act_amount<=0){
	                	$pending_act_amount=0;
	                }
	    			$c = array();
	    			$c["r"] =$received_act_amount;
	    			$cs[] = $c;
	    		
	    			$c = array();
	    			$c["p"] = $pending_act_amount;
	    			$css[] = $c;
	    		
	    			$c = array();
	    			$c["m"]=$monthName.$comma.$year;
	    			$total_month[] = $c;
	    			$consumers[]=$consumerCount;
    			}
    	
    		}else{
    			
    			 $sites=$siteMapper->getSiteByStateId($state_id,$role_sites_id,$site_id);
    			foreach($sites as $site)
    			{
    				$charged_act_amount  = $consumersMapper->getTotalCollectedAmount($site->__get("site_id"),$state_id,$role_sites_id, $curr_date,$last_date);
    				$received_act_amount = $cashRegisterMapper->getTotalCollectedActivation($site->__get("site_id"),$state_id,$role_sites_id,$curr_date,$last_date);
    				 
    				$pending_act_amount=intval($charged_act_amount)-intval($received_act_amount);
    				if($received_act_amount<=0){
    					$received_act_amount=0;
    				}
    				if($pending_act_amount<=0){
    					$pending_act_amount=0;
    				}
    				$c = array();
    				$c["r"] =$received_act_amount;
    				$cs[] = $c;
    			
    				$c = array();
    				$c["p"] = $pending_act_amount;
    				$css[] = $c;
    			
    				$c = array();
    				$c["m"]=$site->__get("site_name");
    				$total_month[] = $c;
    			
    			
    			}
    		}
    		
    		$rows=array();
    		$rows[]=$cs;
    		$rows[]=$css;
    		$rows[]=$total_month;
    		$rows[]=$consumers;
    		$data = array(
    			"rows" => $rows
    		);
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data,
    		);
    	}
    	catch (Exception $e)
    	{
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	public function billedCollectionWidgetAction(){
     try
     {
      $request = $this->getRequest();
      $site_id=$request->getParam("site_id");
      $state_id=$request->getParam("state_id");
	  $segment = $request->getParam("segment");
      $consumer_type = $request->getParam("consumer_type"); 
	         
		if($segment=="ALL"){ 	$segment = NULL;  }
        if($consumer_type=="ALL"){ 	$consumer_type = NULL; } 
		
      $sitesMapper = new Application_Model_SitesMapper();
      $auth=new My_Auth('user');
      $user=$auth->getIdentity()->user_role;
      
      $roleSession = new Zend_Session_Namespace('roles');
      $role_sites_id=$roleSession->site_id;
      $entry_status = $request->getParam("entry_status");
       if($entry_status=="" || $entry_status==NULL || $entry_status=="ALL Type"){
					$entry_status=NULL;
			}else{
				$entry_status=explode(",",$entry_status);
			}
      if($state_id=="ALL"){$state_id=NULL;}
      if($site_id=="ALL"){$site_id=NULL;}
      
      $curr_date = $request->getParam("curr_date");
      $last_date = $request->getParam("last_date");
      
      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
      $curr_date = $zendDate->toString("yyyy-MM-dd");
      
      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
      $last_date=$zendDate->toString("yyyy-MM-dd");
      
      $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
      $days = round($cal);
      $m=0;$y=0;
      	
      $counter=round($days/30);
      $next='+1 month';
      
      $data=array();
      $revenueMapper=new Application_Model_RevenueTargetMapper();
	  $consumerMapper=new Application_Model_ConsumersMapper();
           
      $sites=$sitesMapper->getSiteByStateId($state_id,$role_sites_id,$site_id);
     
      $site_data=array();$reventeTarget=array();
      foreach($sites as $site)
      {
		
		$site_id=$site->__get("site_id");
		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
  		 
      	$totalDebit = intval($cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site_id,"DEBIT",$last_date,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type));
	    $totalCredit = intval($cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site_id,"CREDIT",$last_date,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type));
		$totalActivation = intval($cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site_id,"ACTIVATION",$last_date,$state_id,$role_sites_id,null,$entry_status,$segment,$consumer_type));
	    
			$cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
            $days = ceil($cal); 
            $m=0;$y=0;
			
            $counter=ceil($days/30);
            $next='+1 month';
            if($counter==0){
				$counter=1;
			}
            $secondNexts=$last_date;
			$all_consumer=0;$pre_consumer=0;$post_consumer=0;
            for($i=0;$i<$counter;$i++){
            		
            	$secondCurr = date('Y-m-d', strtotime($secondNexts.$next));		 
				$consumers_arpu=$consumerMapper->getActiveConsumerBySite($site_id,$state_id,$role_sites_id,date('Y-m-d', strtotime($secondCurr."-1 day")),$last_date,$segment,$consumer_type);
				$secondNexts = date('Y-m-d', strtotime($secondNexts.$next));
				
				$all_consumer=$all_consumer+intval($consumers_arpu["all_consumer"]);
				$pre_consumer=$pre_consumer+intval($consumers_arpu["pre_consumer"]);
				$post_consumer=$post_consumer+intval($consumers_arpu["post_consumer"]);
			}
			$Total_ARPU=0;$Pre_ARPU=0;$Post_ARPU=0;
			
			if ($consumers_arpu){
				$totalBilledAmount=$cashRegisterMapper->getOnlyBilledForARPU($curr_date,$last_date,$site_id,$state_id,$role_sites_id,$segment,$consumer_type); 
				$TotalARPU=$totalBilledAmount["Total_CHG"];
				$PreARPU=$totalBilledAmount["Pre_CHG"];
				$PostARPU=$totalBilledAmount["Post_CHG"];
				if(intval($all_consumer)>0){				 
					$Total_ARPU=round(intval($TotalARPU)/intval($all_consumer));
				}
				if(intval($pre_consumer)>0){
					$Pre_ARPU=round(intval($PreARPU)/intval($pre_consumer));
				}
				if(intval($post_consumer)>0){
					$Post_ARPU=round(intval($PostARPU)/intval($post_consumer));
				}
			} 
						  
		$secondNext=$last_date;
		$revenue_target=0;
		for($i=0;$i<$counter;$i++){
			$trans_date=date_parse_from_format("Y-m-d", $secondNext);
			$month= $trans_date["month"];
			$year= $trans_date["year"];
			$dateObj   = DateTime::createFromFormat('!m', $month);
			$monthName = $dateObj->format('F');
			
			$revenue=$revenueMapper->getAllRevenueSumBySiteId($site_id, $monthName, $year);
			$revenue_target=$revenue_target+$revenue;
			$secondNext = date('Y-m-d', strtotime($secondNext.$next));
		}
      	 
	    $site_data[] = array(
		      "site_id" => $site_id,
		      "site_name" => $site->__get("site_name"),
		      "total_debit" => intval($totalDebit),
		      "total_activation" => intval($totalActivation),
		      "total_credit" => intval($totalCredit),
		      "total_collected" => $totalCredit+$totalActivation,
		      "revenue"=>$revenue_target,
			  "Total_ARPU" => $Total_ARPU,  
			  "Pre_ARPU" => $Pre_ARPU,
		      "Post_ARPU"=>$Post_ARPU   
		);
      }
       
      $this->array_sort_by_column($site_data, 'total_debit') ;
      
      $collectionData = array();
      $billingData = array();
      
	  $totalData = array();
	  $PreData = array();
	  $postData=array();
		   
      foreach($site_data as $site_val)
      {
		   $collectionData[] = array($site_val["site_name"], $site_val["total_collected"]);
		   $billingData[] = array($site_val["site_name"], $site_val["total_debit"]);
		   $reventeTarget[] = array($site_val["site_name"], $site_val["revenue"]);
		   
		   $totalData[] = array($site_val["site_name"], $site_val["Total_ARPU"]);
		   $PreData[] = array($site_val["site_name"], $site_val["Pre_ARPU"]);
		   $postData[] = array($site_val["site_name"], $site_val["Post_ARPU"]);
		   
      }
      $lines=array("lineWidth"=> 1);
      $collectionArray = array("data" => $collectionData,
            "label" => "Collection",
            "lines"=>$lines,
            "shadowSize"=>0,
            );
      $billingArray  = array("data" => $billingData,
            "label" => "Billed",
            "lines"=>$lines,
            "shadowSize"=>0,
            );
      $revenueArray  = array("data" => $reventeTarget,
      		"label" => "revenue",
      		"lines"=>$lines,
      		"shadowSize"=>0,
      );
	  $totalArray = array("data" => $totalData);
	  $preArray = array("data" => $PreData);
	  $postArray = array("data" => $postData);
		 
		 
		  array_push($data, $billingArray);
		  array_push($data, $collectionArray);
		  array_push($data, $revenueArray);
    	
		  array_push($data, $totalArray);
	      array_push($data, $preArray);
	      array_push($data, $postArray); 
	       
       
      $meta = array(
        "code" => 200,
        "message" => "SUCCESS"
      );
    
      $arr = array(
        "meta" => $meta,
        "data" => $data
      );
    
      }
      catch(Exception $e)
      {
      $meta = array(
        "code" => $e->getCode(),
         "message" => $e->getMessage()
      );
    
      $arr = array(
      "meta" => $meta
      );
      }
      $json = json_encode($arr, JSON_PRETTY_PRINT);
      echo $json;
    }
	
    public function array_sort_by_column(&$arr, $col, $dir = SORT_DESC) {
    	$sort_col = array();
    	foreach ($arr as $key=> $row) {
    		$sort_col[$key] = $row[$col];
    	}
    
    	array_multisort($sort_col, $dir, $arr);
    }
    
	public function destroyAction(){
    	unset($this);
    	gc_enable() ;gc_collect_cycles();gc_disable();
    }

	public function monthlyConsumersCountSiteWiseAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$sitesMapper= new Application_Model_SitesMapper();
			$packageMapper=new Application_Model_PackagesMapper();
    		  $site_id = $request->getParam("site_id"); 
    		$state_id = $request->getParam("state_id");
    		 
    		$consumersMapper = new Application_Model_ConsumersMapper();
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		
    		$Sites=array();$total_tally=array();
    		
			if($site_id!="ALL" && $site_id!=""){
    			$role_sites_id=explode(",",$site_id);
    		}elseif($state_id!="ALL" && $state_id!=""){
    			$site_datas=$sitesMapper->getSiteByStateId($state_id);
    			foreach ($site_datas as $site_data){
    				$role_sites_id[]=$site_data->__get("site_id");
    			}
    		}else{
    			$roleSession = new Zend_Session_Namespace('roles');
    			$role_sites_id=$roleSession->site_id;
    		}
    		 
    		  $curr_date = $request->getParam("curr_date");
		      $last_date = $request->getParam("last_date");
		      
		      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		      $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
              $last_date=$zendDate->toString("yyyy-MM-dd");
            
        	$data=array(); 
        	 
			 
    		for($i=0;$i<count($role_sites_id);$i++){
    			//$date_string = "01/".$month_val."/".$year;
    			$site_value=$role_sites_id[$i]; 
    			 
					$count = $consumersMapper->getTotalConsumersCountByMonth($curr_date,$last_date,$site_value); 
					$prebannedConsumer=$consumersMapper->getBannedLastConsumerDetails($curr_date,$last_date,$site_value,null,'0',null,true);
					$postbannedConsumer=$consumersMapper->getBannedLastConsumerDetails($curr_date,$last_date,$site_value,null,'1',null,true);
                	if($count==false){
                		$count=0;
						$count=$count+$prebannedConsumer+$postbannedConsumer;
                	}else{
						$count=$count+$prebannedConsumer+$postbannedConsumer;
					}
					
					$upgrade=0;$downgrade=0;$disconnection=0;
					 
					$disconnection_consumers=$consumersMapper->getConsumersWithSuspenDate($curr_date,$last_date,$site_value,$state_id,$role_sites_id);
					
					$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
					$Consumers_status=$packageHistoryMapper->getMonthlypackageHistoryStatus($curr_date,$last_date,$site_value,$state_id,$role_sites_id);
					 
					if($Consumers_status){
						foreach ($Consumers_status as $Consumer_status){
							if($Consumer_status["ph_package_id"]=="" || $Consumer_status["ph_package_id"]==NULL || $Consumer_status["ph_package_id"]=='NULL'){
								$ph_package_id=0;
							}else{
								$ph_package_id=$Consumer_status["ph_package_id"];
							}
								
							$lastPackage=$packageMapper->getPackageById($ph_package_id);
							if($lastPackage){
								$lastPackage_cost=$lastPackage->__get("package_cost");
							}else{
								$lastPackage_cost=0;
							}
							if($Consumer_status["last_package"]=="" || $Consumer_status["last_package"]==NULL || $Consumer_status["ph_package_id"]=='NULL' ){
								$last_package=0;
							}else{
								$last_package=$Consumer_status["last_package"];
							}
					
							$newPackage=$packageMapper->getPackageById($last_package);
								
							if($newPackage){
								$newPackage_cost=$newPackage->__get("package_cost");
							}else{
								$newPackage_cost=0;
							}
							if($lastPackage_cost>$newPackage_cost){
								$upgrade=$upgrade+1;
							}elseif($lastPackage_cost<$newPackage_cost){
								$downgrade=$downgrade+1;
							}
								
						}
					}
					
					if ($disconnection_consumers){$disconnection=count($disconnection_consumers);}
					
						$site=$sitesMapper->getSiteById($site_value);
						$trans_date=date_parse_from_format("Y-m-d", $last_date);
						
	                	$month= $trans_date["month"];
	                	$year= $trans_date["year"];
	                	
	                	$dateObj   = DateTime::createFromFormat('!m', $month);
	                	$monthName = $dateObj->format('M');
	                	$comma=",";
	            
	    		$data=array(
	    				"site_id"=>$site_value,
	    				"site_name"=>$site->__get("site_name"),
	    				"Consumer_count"=>intval($count),
						"Consumer_upgrade"=>$upgrade,
	    				"Consumer_downgrade"=>$downgrade,
	    				"Consumer_disconnection"=>$disconnection,
	    				"Month"=>$monthName
	    		);
	    		$total_tally[]=$data;
    			
    		}
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $total_tally
    		);
    	}
    	catch (Exception $e)
    	{
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function energyConsumedAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$yearType = $request->getParam("yearType");
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    		
    	 	$curr_date = $request->getParam("curr_date");
		      $last_date = $request->getParam("last_date");
		      
		      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		      $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
              $last_date=$zendDate->toString("yyyy-MM-dd");
		      
		      $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
		      $days = round($cal);
		      $m=0;$y=0;
		      	
		      $counter=round($days/30);
		      $next='+1 month';
            
    		$siteMeterReadingMapper = new Application_Model_SiteMeterReadingMapper();
    		
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$utilization_arr=array();
    		if($yearType=='on'){
    			$data=array();
    			$site_data=array();
    			$secondNext=$last_date;
            	for($i=0;$i<$counter;$i++){
            		
            		$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
            		$total=0;
    				$total = intval($siteMeterReadingMapper->getEnergyConsumedById($secondCurr,$secondNext,$site_id,$role_sites_id,$state_id));
					
					
    				$trans_date=date_parse_from_format("Y-m-d", $secondNext);
    				$month= $trans_date["month"];
    				$year= $trans_date["year"];
    				
    				$dateObj   = DateTime::createFromFormat('!m', $month);
    				$monthName = $dateObj->format('M');
    				$comma="'";
    				if($m==1){$monthName=NULL;$comma=NULL;}
    				
    				$secondNext = date('Y-m-d', strtotime($secondNext.$next));
    				
    				$reading[]= $total;
					
    				$month_val[]=$monthName.$comma.substr($year,-2); 
    			}
    		}else{
    			$data=array();
    			$site_data=array();
    			 
    			$cs = array();
    			$months=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    			$j=3;$years=$year;
    			for($i=0;$i<12;$i++){
    				$month = $months[$j];
    				 
    				if($j==11){
    					$j=0;}else{$j++;
    					}
    					if($j==1){
    						$years=$years+1;
    					}
    					$total = intval($siteMeterReadingMapper->getEnergyConsumedById($years,$month,$site_id,$role_sites_id));
    					$reading[]= $total;
    					$month_val[]=$month;
    
    			}
    		}
    		 
    		array_push($data, $reading);
    		array_push($data, $month_val);
			array_push($data, $utilization_arr);
			 
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function energyConsumedSiteIdAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$yearType = $request->getParam("yearType");
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    		$curr_date = $request->getParam("curr_date");
		      $last_date = $request->getParam("last_date");
		      
		      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		      $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
              $last_date=$zendDate->toString("yyyy-MM-dd");
		      
		      $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
		      $days = round($cal);
		      $m=0;$y=0;
		      	
		      $counter=round($days/30);
		      $next='+1 month';
    
    		$siteMasterMeterMapper = new Application_Model_SiteMasterMeterMapper();
    		$siteMasterReading=new Application_Model_SiteMeterReadingMapper();
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		 
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		if($yearType=='on'){
    			$year=$year-1;
    			$data=array();
    			$site_data=array();
    			$meterMasters=$siteMasterMeterMapper->getSiteMasterById($site_id);
    			$SiteReading=array();
    			$month_val=array();
    			if($meterMasters){
    
    				foreach($meterMasters as $meterMaster){
    					$meter_id=$meterMaster->__get("id");
    					$month_val=array();
    					$meter_Reading=array();
    					$secondNext=$last_date;
    					for($i=0;$i<$counter;$i++){
    
    						$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
    						$total=0;
    						$total = intval($siteMasterReading->getEnergyConsumedById($secondCurr,$secondNext,$site_id,$role_sites_id,$state_id,$meter_id));
    							
    						$trans_date=date_parse_from_format("Y-m-d", $secondNext);
    						$month= $trans_date["month"];
    						$year= $trans_date["year"];
    							
    						$dateObj   = DateTime::createFromFormat('!m', $month);
    						$monthName = $dateObj->format('M');
    						$comma="'";
    						if($m==1){$monthName=NULL;$comma=NULL;}
    							
    						$secondNext = date('Y-m-d', strtotime($secondNext.$next));
    							
    
    						$month_val[]=$monthName.$comma.substr($year,-2); 
    						$meter_Reading[]=$total;
    					}
    					$siteMaster=$siteMasterMeterMapper->getMMById($meter_id);
    					$total_reding=array(
    							"meter_id"=>$siteMaster->__get("meter_name"),
    							"reading"=>$meter_Reading );
    
    					$meter_readings[]=$total_reding;
    				}
    			}else{
    				$meter_readings=array();
    				for($i=11;$i>=0;$i--){
    					$zendDate = new Zend_Date();
    					$zendDate->setTimezone("Asia/Calcutta");
    
    					$day = $zendDate->toString(Zend_Date::DAY);
    					/* if($day>=25){
    					 $month = $zendDate->subMonth($i-1)->toString(Zend_Date::MONTH_NAME);
    					}else{ */
    					$month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_NAME);
    					//}
    					$month_val[]=$month;
    				}
    			}
    
    		}else{
    			$data=array();
    			$site_data=array();
    
    			$cs = array();
    			$months=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    
    			$meterMasters=$siteMasterMeterMapper->getSiteMasterById($site_id);
    			$SiteReading=array();
    			// $meter_ids=array();
    			if($meterMasters){
    				foreach($meterMasters as $meterMaster){
    
    					$meter_id=$meterMaster->__get("id");
    					$month_val=array();
    					$meter_Reading=array();
    					$j=3;$years=$year;
    					for($i=0;$i<12;$i++){
    						$month = $months[$j];
    							
    						if($j==11){
    							$j=0;}else{$j++;
    							}
    							if($j==1){
    								$years=$years+1;
    							}
    
    
    							$total = intval($siteMasterReading->getEnergyConsumedById($years,$month,$site_id,$role_sites_id,$state_id,$meter_id));
    							$month_val[]=$month;
    							$meter_Reading[]=$total;
    					}
    					$siteMaster=$siteMasterMeterMapper->getMMById($meter_id);
    					$total_reding=array(
    							"meter_id"=>$siteMaster->__get("meter_name"),
    							"reading"=>$meter_Reading );
    
    					$meter_readings[]=$total_reding;
    
    				}
    			}else{
    				$meter_readings=array();
    				for($i=0;$i<12;$i++){
    					$month = $months[$j];
    
    					if($j==11){
    						$j=0;
    					}
    					else{
    						$j++;
    					}
    					$month_val[]=$month;
    				}
    			}
    
    		}
    		 
    		 
    		array_push($data, $meter_readings);
    		array_push($data, $month_val);
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function energyGenerationAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$yearType = $request->getParam("yearType");
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    		$curr_date = $request->getParam("curr_date");
		      $last_date = $request->getParam("last_date");
		      
		      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		      $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
              $last_date=$zendDate->toString("yyyy-MM-dd");
		      
		      $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
		      $days = round($cal);
		      $m=0;$y=0;
		      	
		      $counter=round($days/30);
		      $next='+1 month';
    
			$mpptReadingMapper = new Application_Model_MpptsReadingMapper();
    		$siteMeterReadingMapper = new Application_Model_SiteMeterReadingMapper();
    		$sitesMapper = new Application_Model_SitesMapper();
    		$consumersMapper=new Application_Model_ConsumersMapper();
    		$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    		$wattageMapper=new Application_Model_WattageMapper();
    		$MMmapper=new Application_Model_SiteMasterMeterMapper();
    		 
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
			$bio_arr=array();$dg_arr=array();$pv_arr=array();
    		if($yearType=='on'){
    			$data=array();
    			$site_data=array();
    			$secondNext=$last_date;
    			for($i=0;$i<$counter;$i++){
    
    				$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
    				$totalGen=0; 
    				
					$totalGen = intval($mpptReadingMapper->getEnergyGenerationById($secondCurr,$secondNext,$site_id,$role_sites_id,$state_id));
					
					$dataValue=$mpptReadingMapper->getPlantDataByFilters($site_id,$state_id,$role_sites_id,$secondCurr,$secondNext);
					 
					
					  
    				$trans_date=date_parse_from_format("Y-m-d", $secondNext);
    				$month= $trans_date["month"];
    				$year= $trans_date["year"];
    
    				$dateObj   = DateTime::createFromFormat('!m', $month);
    				$monthName = $dateObj->format('M');
    				$comma="'";
    				if($m==1){$monthName=NULL;$comma=NULL;}
					
    				$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
					
					$meter_reading_pre=$consumersMapper->getConsumerWattageBySiteId($site_id,$state_id,$role_sites_id,$secondCurr,$secondNext);	
					$meter_reading_prepaid=floatval((floatval($meter_reading_pre["wattage"])*floatval($meter_reading_pre["hours"])*$total_days_in_month)/1000);
					$meter_reading_postpaid=$meterReadingMapper->getTotalUnitBySites($site_id,$state_id,$role_sites_id,$secondCurr,$secondNext);
					//echo " ".$meter_reading_prepaid." d- ".$total_days_in_month." ";
					$totalReadingSold=floatval($meter_reading_postpaid)+$meter_reading_prepaid;
					 
					$secondNext = date('Y-m-d', strtotime($secondNext.$next));
					
					
					/*$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
					if ($consumers_details){
    				foreach ($consumers_details as $consumer_detail){
    					
    						if($consumer_detail["is_postpaid"]==0){
    							
    							$feedarHrs=0;
    							$site_meter_ids=explode(",", $consumer_detail['site_meter_id']);
    							 
    							if(count($site_meter_ids)>0){
    								for ($k=0;$k<count($site_meter_ids);$k++){
    									$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
    									if($feedarDetail){
    										if($feedarDetail->__get("is_24_hr")==1){
    											$feedarHr=24;
    										}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
    											$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
    										}else{
    											$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
    										}
    										$feedarHrs=$feedarHrs+$feedarHr;
    									}
    								}
    							}
    							 
    							$watt_total=$wattageMapper->getWattageBywattId($consumer_detail["wattage"]);
    							if($watt_total){
    								$wattage=$watt_total->__get("wattage");
    							}else{
    								$wattage=0;
    							}
    							
    							$MonthlyUnit_pre=($wattage*$feedarHrs*$total_days_in_month)/1000;
    						    $total_unit=$total_unit+$MonthlyUnit_pre;
    						}else{
    							 
	    						$meterReading=$meterReadingMapper->getTotalUnitSiteWise($consumer_detail["consumer_id"],$month,$year);
	    						if($meterReading){
	    							if(floatval($meterReading["curr_reading"])>0){
	    								$totalMtrUnit=(floatval($meterReading["curr_reading"])-floatval($meterReading["last_reading"]));
	    								$monthly_Unit_post=$totalMtrUnit;
	    							}else{
	    								$monthly_Unit_post=0;
	    							}
	    						}else{
	    							$monthly_Unit_post=0;
	    						}
	    							
	    						$total_unit=$total_unit+ $monthly_Unit_post;
	    					}
    					}
    						
    					$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer_detail['consumer_id']);
    					$month_scheme_pre_total=0;
    					if($consumer_scheme){
    						foreach($consumer_scheme as $consumer_schemes){
    							$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
    							$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
    				
    							$discount_month=$consumer_schemes["discount_month"];
    							$feeder_hours=$consumer_schemes["feeder_hours"];
    							$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
    							$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month"));
    				 
								$dates = new Zend_Date(); 
								$dates->setTimezone("Asia/Calcutta");
								$date_val = $dates->toString("yyyy-MM-dd HH:mm:ss");
								
    							if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
    								$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]*$feeder_hours*$total_days_in_month)/1000);
    								$total_unit=$total_unit + $dailyUnit;
    							}
    						}
    					}
    			}*/
					 
    				$reading[]= round($dataValue["mppt"]); 
    				$month_val[]=$monthName.$comma.substr($year,-2); 
					$PV_gen[]= round(($dataValue["mppt"]-round($dataValue["BIOGAS"])-round($dataValue["DG"])));
					$BIOGAS_gen[]= round($dataValue["BIOGAS"]);
					$DG_gen[]= round($dataValue["DG"]);
					
					$total_con[]= round($dataValue["feeder"]);
					$consum_percentage=0;$PV=0;
					if(floatval($dataValue["mppt"])!=0){
						$PV=round(floatval($dataValue["mppt"])-floatval($dataValue["DG"])-floatval($dataValue["BIOGAS"]));
						$consum_percentage=(floatval($dataValue["feeder"])/floatval($dataValue["mppt"]))*100;
						$con_dg=round($consum_percentage*$dataValue["DG"]/100); 
						$con_bio=round($consum_percentage*$dataValue["BIOGAS"]/100);
						$con_pv=round($consum_percentage*$PV/100);
					}
			
					$PV_con[]= round($con_pv);
					$BIOGAS_con[]= round($con_bio);
					$DG_con[]= round($con_dg);
					$total_unit=$totalReadingSold;//93*$dataValue["feeder"]/100; 
					$total_sold[]= round($total_unit);
					$sold_percentage=0;
					if(floatval($total_unit)!=0){
						//$PV=round(floatval($dataValue["mppt"])-floatval($dataValue["DG"])-floatval($dataValue["BIOGAS"]));
						$sold_percentage=(floatval($total_unit)/floatval($dataValue["feeder"]))*100;
						$sold_dg=round($sold_percentage*$con_dg/100);  
						$sold_bio=round($sold_percentage*$con_bio/100);
						$sold_pv=round($sold_percentage*$con_pv/100);
					} 
					$PV_sold[]= round($sold_pv);
					$BIOGAS_sold[]= round($sold_bio);
					$DG_sold[]= round($sold_dg); 
					
					$plantDetails=$sitesMapper->getPlantDetailsBySites($site_id,$state_id,$role_sites_id);
					$GenCapacity =floatval( $sold_pv+$sold_bio ); 
					$StandardCcapacity =floatval( $plantDetails["Solar_panel_kw"] + $plantDetails["bio_capacity"] ) * $plantDetails["radiation"] * $total_days_in_month;
					
					$percentStand=(80*$StandardCcapacity)/100; 
					
					//$utilization=($GenCapacity/$percentStand)*100; 
					//echo round($dataValue["feeder"])." ".round($dataValue["mppt"])." ";
					$genStan=80*round(floatval($plantDetails["Solar_panel_kw"])*floatval($plantDetails["radiation"])*$total_days_in_month)/100; 
					
					$utilization=($total_unit/$genStan)*100;
					$utilization_arr[]=round($utilization); 
					
    			}
    		}else{
    			$data=array();
    			$site_data=array();
    
    			$cs = array();
    			$months=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    			$j=3;$years=$year;
    			for($i=0;$i<12;$i++){
    				$month = $months[$j];
    					
    				if($j==11){
    					$j=0;}else{$j++;
    					}
    					if($j==1){
    						$years=$years+1;
    					}
    					$total = intval($mpptReadingMapper->getEnergyGenerationById($years,$month,$site_id,$role_sites_id));
    					$reading[]= $total;
    					$month_val[]=$month;
    
    			}
    		}
    		 
    		array_push($data, $reading);
    		array_push($data, $month_val);
			array_push($data, $PV_gen);
			array_push($data, $DG_gen);
			array_push($data, $BIOGAS_gen);
			
			
			array_push($data, $total_con);
			array_push($data, $PV_con);
			array_push($data, $DG_con);
			array_push($data, $BIOGAS_con);
			
			array_push($data, $total_sold);
			array_push($data, $PV_sold);
			array_push($data, $DG_sold);
			array_push($data, $BIOGAS_sold); 
			array_push($data, $utilization_arr); 
			
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }

    public function energyGenerationSiteIdAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$yearType = $request->getParam("yearType");
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    		$curr_date = $request->getParam("curr_date");
		      $last_date = $request->getParam("last_date");
		      
		      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		      $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
              $last_date=$zendDate->toString("yyyy-MM-dd");
		      
		      $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
		      $days = round($cal);
		      $m=0;$y=0;
		      	
		      $counter=round($days/30);
		      $next='+1 month';
    
    		$siteMasterMeterMapper = new Application_Model_MpptsMapper();
    		$siteMasterReading=new Application_Model_MpptsReadingMapper();
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		 
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		if($yearType=='on'){
    			$year=$year-1;
    			$data=array();
    			$site_data=array();
    			$meterMasters=$siteMasterMeterMapper->getMpptsBySiteId($site_id);
    			$SiteReading=array();
    			$month_val=array();
    			if($meterMasters){
    
    				foreach($meterMasters as $meterMaster){
    					$meter_id=$meterMaster->__get("id");
    					$month_val=array();
    					$meter_Reading=array();
    					$secondNext=$last_date;
    					for($i=0;$i<$counter;$i++){
    
    						$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
    						$total=0;
    						$total = intval($siteMasterReading->getEnergyGenerationById($secondCurr,$secondNext,$site_id,$role_sites_id,$state_id,$meter_id));
    							
    						$trans_date=date_parse_from_format("Y-m-d", $secondNext);
    						$month= $trans_date["month"];
    						$year= $trans_date["year"];
    							
    						$dateObj   = DateTime::createFromFormat('!m', $month);
    						$monthName = $dateObj->format('M');
    						$comma="'";
    						if($m==1){$monthName=NULL;$comma=NULL;}
    							
    						$secondNext = date('Y-m-d', strtotime($secondNext.$next));
    							
    
    						$month_val[]=$monthName.$comma.substr($year,-2); 
    						$meter_Reading[]=$total;
    					}
    					$siteMaster=$siteMasterMeterMapper->getMpptsById($meter_id);
    					$total_reding=array(
    							"meter_id"=>$siteMaster->__get("mppt_name"),
    							"reading"=>$meter_Reading );
    
    					$meter_readings[]=$total_reding;
    				}
    			}else{
    				$meter_readings=array();
    				for($i=11;$i>=0;$i--){
    					$zendDate = new Zend_Date();
    					$zendDate->setTimezone("Asia/Calcutta");
    
    					$day = $zendDate->toString(Zend_Date::DAY);
    					/* if($day>=25){
    					 $month = $zendDate->subMonth($i-1)->toString(Zend_Date::MONTH_NAME);
    					}else{ */
    					$month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_NAME);
    					//}
    					$month_val[]=$month;
    				}
    			}
    			 
    		}else{
    			$data=array();
    			$site_data=array();
    
    			$cs = array();
    			$months=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    
    			$meterMasters=$siteMasterMeterMapper->getMpptsBySiteId($site_id);
    			$SiteReading=array();
    			// $meter_ids=array();
    			if($meterMasters){
    				foreach($meterMasters as $meterMaster){
    					 
    					$meter_id=$meterMaster->__get("id");
    					$month_val=array();
    					$meter_Reading=array();
    					$j=3;$years=$year;
    					for($i=0;$i<12;$i++){
    						$month = $months[$j];
    							
    						if($j==11){
    							$j=0;}else{$j++;
    							}
    							if($j==1){
    								$years=$years+1;
    							}
    								
    
    							$total = intval($siteMasterReading->getEnergyGenerationById($years,$month,$site_id,$role_sites_id,$state_id,$meter_id));
    							$month_val[]=$month;
    							$meter_Reading[]=$total;
    					}
    					$siteMaster=$siteMasterMeterMapper->getMpptsById($meter_id);
    					$total_reding=array(
    							"meter_id"=>$siteMaster->__get("meter_name"),
    							"reading"=>$meter_Reading );
    
    					$meter_readings[]=$total_reding;
    					 
    				}
    			}else{
    				$meter_readings=array();
    				for($i=0;$i<12;$i++){
    					$month = $months[$j];
    
    					if($j==11){
    						$j=0;
    					}
    					else{
    						$j++;
    					}
    					$month_val[]=$month;
    				}
    			}
    
    		}
    		 
    		 
    		array_push($data, $meter_readings);
    		array_push($data, $month_val);
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function getDebitPendingAction(){
    	try{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		
    		$curr_date = $request->getParam("curr_date");
	        $last_date = $request->getParam("last_date");
	            
	        if($state_id=="ALL"){$state_id=NULL;}
	        if($site_id=="ALL"){$site_id=NULL;}
	            
	        $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
	        $curr_date = $zendDate->toString("yyyy-MM-dd");
	            
	        $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
	        $last_date=$zendDate->toString("yyyy-MM-dd");
	        
	    	$cR=new Application_Model_CashRegisterMapper();
	    	$consumersMapper=new Application_Model_ConsumersMapper();
	    	$roleSession = new Zend_Session_Namespace('roles');
	    	$role_sites_id=$roleSession->site_id;
	    	
    			$cashRegister=$cR->getAgeingofDebtorsByConsumerId($site_id,$state_id,$role_sites_id,$curr_date);
    			if($cashRegister){
					
    				$TotalConsumerCount=$cashRegister["TotalConsumerCount"];
    				$consumer_30=$cashRegister["consumer_30"];
    				$consumer_60=$cashRegister["consumer_60"];
    				$consumer_90=$cashRegister["consumer_90"];
    				$consumer_120=$cashRegister["consumer_120"];
    				$consumer_180=$cashRegister["consumer_180"];
    				$total_Credit=intval($cashRegister["total_Credit"]);
    				$total_Debit=intval($cashRegister["total_Debit"]);
    
    				$sum_debit_30=intval($cashRegister["sum_debit_30"]);
    				$sum_debit_60=intval($cashRegister["sum_debit_60"]);
    				$sum_debit_90=intval($cashRegister["sum_debit_90"]);
    				$sum_debit_120=intval($cashRegister["sum_debit_120"]);
    				$sum_debit_180=intval($cashRegister["sum_debit_180"]);
    				$sum_debit_above_180=intval($cashRegister["sum_debit_above_180"]);
    				
					//$total_debit_30=intval($cashRegister["sum_debit_30"])-intval($cashRegister["sum_credit_30"]);
    				//$total_debit_60=intval($cashRegister["sum_debit_60"])-intval($cashRegister["sum_credit_60"]);
    				//$total_debit_90=intval($cashRegister["sum_debit_90"])-intval($cashRegister["sum_credit_90"]);
					//$total_debit_120=intval($cashRegister["sum_debit_120"])-intval($cashRegister["sum_credit_120"]);
    				//$total_debit_180=intval($cashRegister["sum_debit_180"])-intval($cashRegister["sum_credit_180"]);
    				//$total_debit_above_180=intval($cashRegister["sum_debit_above_180"])-intval($cashRegister["sum_credit_above_180"]);
					
    				$total_pending=(($total_Debit-$total_Credit)<=0?0:($total_Debit-$total_Credit)); 
					  
					/*$total_debit_above_180=($total_pending>=$sum_debit_above_180?$sum_debit_above_180:$total_pending);
    				$total_debit_180=(($total_pending-$total_debit_above_180)>=$sum_debit_180?$sum_debit_180:($total_pending-$total_debit_above_180));
    				$total_debit_120=(($total_pending-$total_debit_180-$total_debit_above_180)>=$sum_debit_120?$sum_debit_120:($total_pending-$total_debit_180-$total_debit_above_180));
    				$total_debit_90=(($total_pending-$total_debit_120-$total_debit_180-$total_debit_above_180)>=$sum_debit_90?$sum_debit_90:($total_pending-$total_debit_120-$total_debit_180-$total_debit_above_180));
    				$total_debit_60=(($total_pending-$total_debit_90-$total_debit_120-$total_debit_180-$total_debit_above_180)>=$sum_debit_60?$sum_debit_60:($total_pending-$total_debit_90-$total_debit_120-$total_debit_180-$total_debit_above_180));
    				
    				$total_debit_30=((($total_pending-$total_debit_60-$total_debit_90-$total_debit_120-$total_debit_180-$total_debit_above_180))>=$sum_debit_30?$sum_debit_30:($total_pending-$total_debit_60-$total_debit_90-$total_debit_120-$total_debit_180-$total_debit_above_180));
					*/
					
    				$total_debit_30=($total_pending>=$sum_debit_30?$sum_debit_30:$total_pending);
    				$total_debit_60=(($total_pending-$total_debit_30)>=$sum_debit_60?$sum_debit_60:($total_pending-$total_debit_30));
    				$total_debit_90=(($total_pending-$total_debit_60-$total_debit_30)>=$sum_debit_90?$sum_debit_90:($total_pending-$total_debit_60-$total_debit_30));
    				$total_debit_120=(($total_pending-$total_debit_90-$total_debit_60-$total_debit_30)>=$sum_debit_120?$sum_debit_120:($total_pending-$total_debit_90-$total_debit_60-$total_debit_30));
    				$total_debit_180=(($total_pending-$total_debit_120-$total_debit_90-$total_debit_60-$total_debit_30)>=$sum_debit_180?$sum_debit_180:($total_pending-$total_debit_120-$total_debit_90-$total_debit_60-$total_debit_30));
    				 
    				$total_debit_above_180=((($total_pending-$total_debit_180-$total_debit_120-$total_debit_90-$total_debit_60-$total_debit_30)>=$sum_debit_above_180)?$sum_debit_above_180:($total_pending-$total_debit_180-$total_debit_120-$total_debit_90-$total_debit_60-$total_debit_30));
					 
    				if(intval($total_debit_30)!=0 && intval($total_pending)!=0){
    					$percent_30=floatval((intval($total_debit_30)/intval($total_pending))*100);
    				}else{$percent_30=0;}
    				if(intval($total_debit_60)!=0 && intval($total_pending)!=0){
    					$percent_60=floatval((intval($total_debit_60)/intval($total_pending))*100);
    				}else{$percent_60=0;}
    				if(intval($total_debit_90)!=0 && intval($total_pending)!=0){
    					$percent_90=intval((floatval($total_debit_90)/intval($total_pending))*100);
    				}else{$percent_90=0;}
    				if(intval($total_debit_120)!=0 && intval($total_pending)!=0){
    					$percent_120=intval((floatval($total_debit_120)/intval($total_pending))*100);
    				}else{$percent_120=0;}
    				if(intval($total_debit_180)!=0 && intval($total_pending)!=0){
    					$percent_180=intval((floatval($total_debit_180)/intval($total_pending))*100);
    				}else{$percent_180=0;}
    				if(intval($total_debit_above_180)!=0 && intval($total_pending)!=0){
    					$percent_above_180=floatval((intval($total_debit_above_180)/intval($total_pending))*100);
    				}else{$percent_above_180=0;}
					
					
					
    				$data=array(
    					"sum_debit_30"=>$total_debit_above_180,
    					"consumer_30"=>$consumer_30,
    						
    					"sum_debit_60"=>$total_debit_180,
    					"consumer_60"=>$consumer_60,
    						
    					"sum_debit_90"=>$total_debit_120,
    					"consumer_90"=>$consumer_90,
    						
    					"sum_debit_120"=>$total_debit_90, 
    					"consumer_120"=>$consumer_120,
    						
    					"sum_debit_180"=>$total_debit_60,  
    					"consumer_180"=>$consumer_180,
    						
    					"sum_debit_above_180"=>$total_debit_30, 
    					"TotalConsumerCount"=>$TotalConsumerCount,
    						
    					"percent_30"=>round($percent_above_180),
    					"percent_60"=>round($percent_180),
    					"percent_90"=>round($percent_120),
    					"percent_120"=>round($percent_90),
    					"percent_180"=>round($percent_60),
    					"percent_above_180"=>round($percent_30), 
    					"totalPending"=>$total_debit_30+$total_debit_60+$total_debit_90+$total_debit_120+$total_debit_180+$total_debit_above_180
						
    					/*"sum_debit_30"=>($total_debit_30<0)?0:$total_debit_30,
    					"consumer_30"=>$consumer_30,
    						
    					"sum_debit_60"=>($total_debit_60<0)?0:$total_debit_60,
    					"consumer_60"=>$consumer_60,
    						
    					"sum_debit_90"=>($total_debit_90<0)?0:$total_debit_90,
    					"consumer_90"=>$consumer_90,
    						
    					"sum_debit_120"=>($total_debit_120<0)?0:$total_debit_120, 
    					"consumer_120"=>$consumer_120,
    						
    					"sum_debit_180"=>($total_debit_180<0)?0:$total_debit_180, 
    					"consumer_180"=>$consumer_180,
    						
    					"sum_debit_above_180"=>($total_debit_above_180<0)?0:$total_debit_above_180, 
    					"TotalConsumerCount"=>$TotalConsumerCount,
    						
    					"percent_30"=>round($percent_30<0?0:$percent_30),
    					"percent_60"=>round($percent_60<0?0:$percent_60),
    					"percent_90"=>round($percent_90<0?0:$percent_90),
    					"percent_120"=>round($percent_120<0?0:$percent_120),
    					"percent_180"=>round($percent_180<0?0:$percent_180),
    					"percent_above_180"=>round($percent_above_180<0?0:$percent_above_180), 
    					"totalPending"=>$total_debit_30+$total_debit_60+$total_debit_90+$total_debit_120+$total_debit_180+$total_debit_above_180*/
    							
    				);
    				
    			} 
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			
    			$arr = array(
    					"meta" => $meta,
    					"data" => $data
    			);
    	 
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function getDebitPendingConsumerRecordAction(){
    	try{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$date_val = $request->getParam("dates");
    		$days = $request->getParam("Days");
			if($date_val=='NULL')
    		{
    			$date = new Zend_Date();
    			$date->setTimezone("Asia/Calcutta");
    			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    		}else{
    			$zendDate = new Zend_Date($date_val,"MMMM D, YYYY");
    			$timestamp = $zendDate->toString("yyyy-MM-dd");
    		}
    		if($site_id=='ALL'){
    			$site_id=NULL;
    		}
    		if($state_id=='ALL'){
    			$state_id=NULL;
    		}
    		if($days==30){
    			$day_value="Pending &lt; 30";
    		}elseif($days==60){
    			$day_value="Pending 30-60";
    		}elseif($days==90){
    			$day_value="Pending 60-90";
    		} elseif($days==120){
    			$day_value="Pending 90-120";
    		} elseif($days==180){
    			$day_value="Pending 120-180";
    		}elseif ($days==200){
    			$day_value="Pending &gt; 180";
    		}
			date_default_timezone_set("Asia/Kolkata");
    		$cR=new Application_Model_CashRegisterMapper();
    		$consumersMapper=new Application_Model_ConsumersMapper();
    		$siteMapper=new Application_Model_SitesMapper();
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$data=" ";
			$data.= "<h3><center>Sheet generated for ".$day_value." on ".date('d M Y')." at ".date('h:i A')." from TARAUrja.in</center></h3>";
  	 			
    		$data.= " <div id='cm_defaulter_email'>
    			
            	<div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>
	                <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
                        <tbody>";
    			 
    			$data.= "<tr>
                                <th width='50px'>S.No</th>
  	 							<th width='120px'>Location</th>
  	 						    <th width='150px'>Name</th>
  	                            <th width='120px'>Active</th>
								<th width='180px'>Total Pending</th>
                                <th width='180px'>Connection ID</th>
  	 	                        <th width='80px'>".$day_value."</th>
								
					
    			
                            </tr>";
    				
    		  
    		
    			//$count=1;
    		 
    		//$consumers=$cR->getAgeingsofDebtorsByConsumerId($site_id,$state_id,$role_sites_id,$timestamp,NULL);
    			 $consumers=$consumersMapper->getAllConsumers(true,$site_id,$state_id,$role_sites_id);
    		
    			$count=1;$cal=0;$cal1=0;$totalSum=0;
    			foreach($consumers as $cashRegisters){
				 
				 
    			$cashRegister=$cR->getAgeingsofDebtorsByConsumerId($site_id,$state_id,$role_sites_id,$timestamp,$cashRegisters->__get("consumer_id"),$days);
    	 	if($cashRegister){
    			//foreach($consumers as $cashRegister){
    			$total_Credit=($cashRegister["total_Credit"]==NULL||$cashRegister["total_Credit"]<=0?0:$cashRegister["total_Credit"]);
    			$total_Debit=($cashRegister["total_Debit"]==NULL||$cashRegister["total_Debit"]<=0?0:$cashRegister["total_Debit"]);
     
    			$sum_debit_30=($cashRegister["sum_debit_30"]==NULL||$cashRegister["sum_debit_30"]<=0?0:$cashRegister["sum_debit_30"]);
    			$sum_debit_60=($cashRegister["sum_debit_60"]==NULL||$cashRegister["sum_debit_60"]<=0?0:$cashRegister["sum_debit_60"]);
    			$sum_debit_90=($cashRegister["sum_debit_90"]==NULL||$cashRegister["sum_debit_90"]<=0?0:$cashRegister["sum_debit_90"]);
    			$sum_debit_120=($cashRegister["sum_debit_120"]==NULL||$cashRegister["sum_debit_120"]<=0?0:$cashRegister["sum_debit_120"]);
    			$sum_debit_180=($cashRegister["sum_debit_180"]==NULL||$cashRegister["sum_debit_180"]<=0?0:$cashRegister["sum_debit_180"]);
    
    			$sum_debit_above_180=($cashRegister["sum_debit_above_180"]==NULL||$cashRegister["sum_debit_above_180"]<=0?0:$cashRegister["sum_debit_above_180"]);
   
    			$total_pending=(($total_Debit-$total_Credit)<=0?'0':($total_Debit-$total_Credit));
    			$total_debit_30=($total_pending>=$sum_debit_30?$sum_debit_30:$total_pending);
    			$total_debit_60=(($total_pending-$total_debit_30)>=$sum_debit_60?$sum_debit_60:($total_pending-$total_debit_30));
    			$total_debit_90=(($total_pending-$total_debit_60-$total_debit_30)>=$sum_debit_90?$sum_debit_90:($total_pending-$total_debit_60-$total_debit_30));
    			$total_debit_120=(($total_pending-$total_debit_90-$total_debit_60-$total_debit_30)>=$sum_debit_120?$sum_debit_120:($total_pending-$total_debit_90-$total_debit_60-$total_debit_30));
    			$total_debit_180=(($total_pending-$total_debit_120-$total_debit_90-$total_debit_60-$total_debit_30)>=$sum_debit_180?$sum_debit_180:($total_pending-$total_debit_120-$total_debit_90-$total_debit_60-$total_debit_30));
    
    			$total_debit_above_180=((($total_pending-$total_debit_180-$total_debit_120-$total_debit_90-$total_debit_60-$total_debit_30)>=$sum_debit_above_180)?$sum_debit_above_180:($total_pending-$total_debit_180-$total_debit_120-$total_debit_90-$total_debit_60-$total_debit_30));
     
    			if($days==30){
    				$day_values=$total_debit_30;
    			}elseif($days==60){
    				$day_values=$total_debit_60;
    			}elseif($days==90){
    				$day_values=$total_debit_90;
    			} elseif($days==120){
    				$day_values=$total_debit_120;
    			} elseif($days==180){
    				$day_values=$total_debit_180;
    			}elseif ($days==200){
    				$day_values=$total_debit_above_180;
    			}
				$totalSum=$totalSum+$day_values;
    			 if($day_values>0){
    					$sites_details=$siteMapper->getSiteById($cashRegister["site_id"]);
    						$data.= "  <tr>
	                                    <td>".$count++."</td>
	                                    <td>".$sites_details->__get("site_name")."</td>
	                                    <td>".$cashRegister["consumer_name"]."</td>
									    <td>".$cashRegister["consumer_status"]."</td>
	                                    <td>".$total_pending."</td>
									    <td>".$cashRegister["consumer_connection_id"]."</td>
	                                    <td>".$day_values."</td>
		                              
    			
	                                </tr> ";
    					}
    			 }
				   
    		 	}
				
    			 
    				
    			$data.=  "
				<tr><td colspan='6'>Total</td><td>".$totalSum."</td></tr>
				</tbody> </table>
                		</div>
           			</div>
		   		</div>
         	 		</div> ";
    			
    			 
    			
    		 
    		 
    		date_default_timezone_set("Asia/Kolkata");
    		$filename="Ageing of debtor Report ".date('d M Y hi A').'.pdf';
    		$files=(string)$filename;
    		$checkPdf=$this->pdfGenerateAction($data,$files);
		    //$path="http://taraurja.in/".$files;
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		); 
    		 
    		$arr = array(
    				"meta" => $meta,
    			 	"data"=>"http://taraurja.in/".$files
    		);
    		
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	public function pdfGenerateAction($data,$files){
    	 
    	$request=$this->getRequest();
    	 
     
    	date_default_timezone_set('Asia/Kolkata');
    	$filename=$files;
    
    	$pdf=new HTML2FPDF('P','mm','a2');
    	$pdf->AddPage();
    
    	$strContent=$data;
    	$pdf->WriteHTML($strContent);
    
    	$pdf->Output($filename);
    
    	//echo "http://taraurja.in/".$filename; 
     
    }

	public function electricityForConsumerAction(){
        try
        {
            $request = $this->getRequest();
            $adddevicemapper=new Application_Model_AddDeviceMapper();
            $consumer_id = $request->getParam("consumer_id");
            $second_date = $request->getParam("second_date");
            $first_date = $request->getParam("first_date");
            //if($first_date=="ALL"){$first_date=NULL;}
            //if($second_date=="ALL"){$second_date=NULL;}
            /*if($first_date=='NULL')
            {
                $date = new Zend_Date();
                $date->setTimezone("Asia/Calcutta");
                $first_date = $date->toString("yyyy-MM-dd HH:mm:ss");

            }else{*/
                $zendDate = new Zend_Date($first_date,"MMMM D, YYYY");
             $first_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");

           // }

            /*if($second_date=='NULL')
            {
                $date = new Zend_Date();
                $date->setTimezone("Asia/Calcutta");
                $second_date = $date->toString("yyyy-MM-dd HH:mm:ss");
            }else{*/
                $zendDate = new Zend_Date($second_date,"MMMM D, YYYY");
                $second_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
            //}

            $cal=(strtotime($first_date)-strtotime($second_date)) / (60 * 60 * 24);
              $days= $cal;

            if(intval($days)==0){
                $days=1;
            }
            $con=0;
            $mins=$days*1440;  
            $sec=$days*86400;
            $hrs=$mins/60;
            if(intval($hrs)<=24){
                $con=1;
                $counter=intval($hrs);
                $next='+1 hour';
            }else if($days<=60){
                $counter=$days;
                $next='+1 day';
            }elseif ($days<=365){
                $counter=intval($days/7);
                $next='+1 month';
            }else{ 
                $counter=intval($days/365);
                $next='+1 year';
            }
             
		    $data=array();
 
            $electricity=new Application_Model_ElectricityInfoMapper();
             $time=array();$YKahArray=array();
            $check=explode(" ",$next);
		//	echo $second_date."".$counter." ".$next." ";
          //  $second_date=date('Y-m-d H:i:s', strtotime($second_date .$next));
		  $secondNext = date('Y-m-d H:i:s', strtotime($second_date .$next));

            for($i=1;$i<=$counter;$i++){

                $volttage=0;$node_current=0;$date_info=array();$YKah=0;
                $electricConsumer=$electricity->getElectricityForAccount($consumer_id,$second_date,$secondNext);
				 
				$dataCount=$electricConsumer->count(); 
                if($dataCount>0) {

                     
                    if ($dataCount > 0) {
                        foreach ($electricConsumer as $electricConsumers) {
							//print_r($electricConsumers);
							foreach( $electricConsumers["current"] as $cur)
							{
								if( $cur["consumer_id"] ==$consumer_id)
									 $volttage = $volttage + $cur["volt"];
							}
                            
                            $node_current = $node_current + $electricConsumers["votage"];
                            $date_info[] = $electricConsumers["info_date"];
							//echo $volttage." ".$node_current."\n";
                        }

                        $count = count($date_info) - 1;
                        $info_dates = (strtotime($date_info[0]) - strtotime($date_info[$count])) /  (60 * 60 * 24);
                        $sec=$info_dates*86400;
                        $info_date=$sec/3600;

                    } else {
                      //  $volttage = $electricConsumer[0]["voltage"];
                      $node_current = $electricConsumer[0]["votage"];
                      $info_date = 1;
                    }
					
                    $calVol = $volttage / $dataCount;
                    $calNode_current = $node_current / $dataCount;
              //echo $volttage." ".$node_current." ".$check[1]." ".$dataCount." ".$calVol." ".$calNode_current." ".$info_date."\n";
                    $Xvah = $calVol * $calNode_current * $info_date;
                    $YKah = $Xvah / 1000;

                    if($con==1){
                        $time[] = date('H:i', strtotime($second_date));
                    }else{
                        $time[] = $i.$check[1];
                    }
                    $YKahArray[] =  $YKah ;
				
                }
                    else{
                        if($con==1){
                            $time[] = date('H:i', strtotime($second_date));
                        }else{
                            $time[] = $i.$check[1];
                        }

                        $YKahArray[] = 0;
						
                    }
			    $second_date=$secondNext;
				$secondNext = date('Y-m-d H:i:s', strtotime($second_date .$next));
             
				

            }
			
            if(count($YKahArray)<15){
                for($i=count($YKahArray);$i<15;$i++){
                    array_push($time,"-");
                    array_push($YKahArray,0);
                }
            }
			$device_vol=$adddevicemapper->getVolByConid($consumer_id);
            $vol=array();
            for($j=0;$j<count($YKahArray);$j++){
                array_push($vol,ltrim($device_vol["wattage"], '0'));
            }
			 
            array_push($data, $time);
            array_push($data, $YKahArray);
            array_push($data, $vol);

            $meta = array(
                "code" => 200,
                "message" => "SUCCESS" 
            );

            $arr = array(
                "meta" => $meta,
                "data" => $data
            );

        }
        catch(Exception $e)
        {
            $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json; 
    }
	
	public function monthlyConsumersStatusCountAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
			
			$segment = $request->getParam("segment");
    		$consumer_type = $request->getParam("consumer_type");
    		 
    		$site_id=($site_id=='ALL')?NULL:$site_id;
    		$state_id=($state_id=='ALL')?NULL:$state_id;
			 
			if($segment=="ALL"){ 	$segment = NULL;  }
            if($consumer_type=="ALL"){ 	$consumer_type = NULL; } 
			
    		$curr_date = $request->getParam("curr_date");
		      $last_date = $request->getParam("last_date");
		      
		      $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		      $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		      $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
              $last_date=$zendDate->toString("yyyy-MM-dd");
		      
		      $cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
		      $days = round($cal);
		      $m=0;$y=0;
		      	
		      $counter=round($days/30);
		      $next='+1 month';
    		
    		$consumersMapper = new Application_Model_ConsumersMapper();
    		$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
    		$packageMapper=new Application_Model_PackagesMapper();
    		$logMapper=new Application_Model_LogsMapper();
			
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    	 
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    
    		$data=array();$val;$upgrade_count=array();$downgrage_count=array();$disconnection_count=array();$months=array();
    		$consumer_data=array();
    		$prev_count=0;
    		$secondNext=$last_date;
    		 
            	for($i=0;$i<$counter;$i++){
            	
            	$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
            	$upgrade=0;$downgrade=0;$disconnection=0;
		 
            	//$disconnection_consumers=$consumersMapper->getConsumersWithSuspenDate($secondCurr,$secondNext,$site_id,$state_id,$role_sites_id,$segment,$consumer_type);
    			$Consumers_status=$packageHistoryMapper->getMonthlypackageHistoryStatus($secondCurr,$secondNext,$site_id,$state_id,$role_sites_id,$segment,$consumer_type);
    			  
    			 if($Consumers_status){
    				foreach ($Consumers_status as $Consumer_status){
						if($Consumer_status["ph_package_id"]=="" || $Consumer_status["ph_package_id"]==NULL || $Consumer_status["ph_package_id"]=='NULL'){
    						$ph_package_id=0;
    					}else{
    						$ph_package_id=$Consumer_status["ph_package_id"];
    					}
    					
    					$lastPackage=$packageMapper->getPackageById($ph_package_id);
    					if($lastPackage){
    						$lastPackage_cost=$lastPackage->__get("package_cost");
    					}else{
    						$lastPackage_cost=0;
    					}
    					if($Consumer_status["last_package"]=="" || $Consumer_status["last_package"]==NULL || $Consumer_status["ph_package_id"]=='NULL' ){
    						$last_package=0;
    					}else{
    						$last_package=$Consumer_status["last_package"];
    					}
    					 
    					$newPackage=$packageMapper->getPackageById($last_package);
    					
    					if($newPackage){
    						$newPackage_cost=$newPackage->__get("package_cost");
    					}else{
    						$newPackage_cost=0;
    					}
    					if($lastPackage_cost>$newPackage_cost){
    						$upgrade=$upgrade+1;
    					}elseif($lastPackage_cost<$newPackage_cost){
    						$downgrade=$downgrade+1;
    					}  
    					
    				}
    			}
    			
    			//if ($disconnection_consumers){$disconnection=count($disconnection_consumers);}
    			
				$disconnection_con=$logMapper->getConsumerBannedLogsBydate($secondCurr,$secondNext); 
                	
				if($disconnection_con){
					foreach ($disconnection_con as $disconnection_cons){
						$arr=explode("Consumer ID: ", $disconnection_cons["message"]);
						$connection=explode(" ",$arr[1]);
						$connection_id=$connection[0];
						$checkReactive=$consumersMapper->checkConnectionId($site_id,$state_id,$role_sites_id,$connection_id,$segment,$consumer_type);
						if($checkReactive){
							$disconnection=$disconnection+1;
						}
					}
                }
				
    			$trans_date=date_parse_from_format("Y-m-d", $secondNext);
    			$month= $trans_date["month"];
    			$year= $trans_date["year"];
    			
    			$dateObj   = DateTime::createFromFormat('!m', $month);
    			$monthName = $dateObj->format('M');
    			$comma="'";
    			if($m==1){$monthName=NULL;$comma=NULL;}
    			 
    			$secondNext = date('Y-m-d', strtotime($secondNext.$next));
    			
    			$upgrade_count[] = $upgrade;
    			$downgrage_count[] = $downgrade;
    			$disconnection_count[] = $disconnection;
    			$months[]=$monthName.$comma.substr($year,-2); 
    		}
    		array_push($data, $upgrade_count);
    		array_push($data, $downgrage_count);
    		array_push($data, $disconnection_count);
    		array_push($data, $months);
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    	}
    	catch (Exception $e)
    	{
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json; 
    }
    
    public function monthlyOutstandingDataAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$year=$request->getParam("year");
    		$month=$request->getParam("month");
    		$month_val = date("m", strtotime($month));
    		if($month=='January'){
    			$lastMonth=12;
    			$lastYear=$year-1;
    		}else{
    			$lastMonth=$month_val-1;
    			$lastYear=$year;
    		}
    		$site_id=($site_id=='ALL')?NULL:$site_id;
    		$state_id=($state_id=='ALL')?NULL:$state_id;
    		 
    		$CRMapper = new Application_Model_CashRegisterMapper();
    	 
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		
    		$data=array();$consumer_data=array();
    		 
    			$Curr_month=$CRMapper->getOutstandingByFilter($month_val,$year,$site_id,$state_id,$role_sites_id);
    			$curr_debit=intval($Curr_month["DEBIT"]);
    			$curr_credit=intval($Curr_month["CREDIT"]);
    			$Curr_outstanding=$curr_debit-$curr_credit;
    			
    			$last_month=$CRMapper->getOutstandingByFilter($lastMonth,$lastYear,$site_id,$state_id,$role_sites_id);
    			$last_debit=intval($last_month["DEBIT"]);
    			$last_credit=intval($last_month["CREDIT"]);
    			$last_outstanding=$last_debit-$last_credit;
    			
    		 
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$dateObj   = DateTime::createFromFormat('!m', $lastMonth);
    		$monthName = $dateObj->format('F'); 
    		$arr = array(
    				"meta" => $meta,
    				"curr_out" => $Curr_outstanding,
    				"last_out" => $last_outstanding,
    				"curr_date" => $month.", ".$year,
    				"last_date" => $monthName.", ".$lastYear,
    		);
    	}
    	catch (Exception $e)
    	{
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	  
	public function newConsumerCountAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		
    		$curr_date = $request->getParam("curr_date");
    		$last_date = $request->getParam("last_date");
    		
    		$zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
    		$curr_date = $zendDate->toString("yyyy-MM-dd");
    		$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
    		$last_date = $zendDate->toString("yyyy-MM-dd");
    		
    		$consumerMapper=new Application_Model_ConsumersMapper();
    		$cashRegisterMapper= new Application_Model_CashRegisterMapper();
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    		 
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		 
    		$consumers=$consumerMapper->getActiveConsumerBySite($site_id,$state_id,$role_sites_id,$curr_date,$last_date);
    		if ($consumers){
    			$totalBilledAmount=$cashRegisterMapper->getOnlyBilledForARPU($curr_date,$last_date,$site_id,$state_id,$role_sites_id,NULL,NULL,true);
					
					if(date('m', strtotime($curr_date)) == date('m', strtotime($last_date))){
						$next_date=" -1 month";
						$back_lastDate = date('Y-m-d', strtotime($last_date .$next_date));
						$back_currDate=date('Y-m-d', strtotime($last_date."-1 day"));
					}else{
						$next_date=" -1 month";
						$back_lastDate = date('Y-m-d', strtotime($last_date .$next_date));
						$back_currDate=date('Y-m-d', strtotime($curr_date .$next_date));
					}
					
					$LastBilledAmount=$cashRegisterMapper->getOnlyBilledForARPU($back_currDate,$back_lastDate,$site_id,$state_id,$role_sites_id,NULL,NULL,true);
				
				 	$TotalARPU=$totalBilledAmount["Total_CHG"];
    				$PreARPU=$totalBilledAmount["Pre_CHG"];
    				$PostARPU=$totalBilledAmount["Post_CHG"];
					//echo $totalBilledAmount["Pre_debit"]." ".$totalBilledAmount["Post_debit"];exit;  
					$Pre_debit=intval($totalBilledAmount["Pre_debit"])+intval($totalBilledAmount["Pre_debit_chg"]);
    				$Post_debit=intval($totalBilledAmount["Post_debit"])+intval($totalBilledAmount["Post_debit_chg"]);
    				$Pre_credit=intval($totalBilledAmount["Pre_credit"])+intval($totalBilledAmount["Pre_credit_chg"]);
					$Post_credit=intval($totalBilledAmount["Post_credit"])+intval($totalBilledAmount["Post_credit_chg"]);
    				$Pre_SD=intval($totalBilledAmount["Pre_SD"]);
    				$Post_SD=intval($totalBilledAmount["Post_SD"]);
    				
					$lastPost_debit=intval($LastBilledAmount["Post_debit_chg"]);
    				$lastPost_credit=intval($LastBilledAmount["Post_credit_chg"]);
					
					$lastPre_debit=intval($LastBilledAmount["Pre_debit_chg"]);
    				
					$totalLastDebit=intval($LastBilledAmount["Pre_debit_chg"])+intval($LastBilledAmount["Post_debit_chg"]);;
					$totalCurrDebit=intval($totalBilledAmount["Pre_debit_chg"])+intval($totalBilledAmount["Post_debit_chg"]);
					
					$totaldebit=intval($totalBilledAmount["Pre_debit_chg"])+intval($LastBilledAmount["Post_debit_chg"]);
					$totalcredit=intval($totalBilledAmount["Pre_credit_chg"])+intval($totalBilledAmount["Post_credit_chg"]);
					
					$prepaidPercentage=0;$postpaidPercentage=0;$totalPercentage=0;
					
					if(intval($totalBilledAmount["Pre_debit_chg"])!=0){
						$prepaidPercentage=round(((intval($totalBilledAmount["Pre_credit_chg"])/intval($totalBilledAmount["Pre_debit_chg"]))*100));
					}
					if(intval($totalBilledAmount["Post_credit_chg"])!=0){
						$postpaidPercentage=round(((intval($totalBilledAmount["Post_credit_chg"])/$lastPost_debit)*100)); 
					}
					if($totalcredit!=0){ 
						$totalPercentage=round((($totalcredit/$totaldebit)*100));
					}
					$prepaidDebitPercentage=0;$postpaidDebitPercentage=0;$totalDebitPercentage=0;
					if(intval($totalBilledAmount["Pre_debit_chg"])!=0){
						$prepaidDebitPercentage=round(((intval($totalBilledAmount["Pre_debit_chg"])/intval($LastBilledAmount["Pre_debit_chg"]))*100));
					}
					if(intval($totalBilledAmount["Post_debit_chg"])!=0){
						$postpaidDebitPercentage=round(((intval($totalBilledAmount["Post_debit_chg"])/intval($LastBilledAmount["Post_debit_chg"]))*100));
					}
					 
					if(intval($totalCurrDebit)!=0){
						$totalDebitPercentage=round((($totalCurrDebit/$totalLastDebit)*100));
					}
					
					$cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
					$days = ceil($cal); 
					$m=0;$y=0;
					
					$counter=ceil($days/30);
					if($counter==0){
						$counter=1;
					}
					$next='+1 month';
					
					$secondNexts=$last_date;
					$all_consumer=0;$pre_consumer=0;$post_consumer=0;
					for($i=0;$i<$counter;$i++){
							
						$secondCurr = date('Y-m-d', strtotime($secondNexts.$next));		 
						$consumers_arpu=$consumerMapper->getActiveConsumerBySite($site_id,$state_id,$role_sites_id,date('Y-m-d', strtotime($secondCurr."-1 day")),$last_date);
						$secondNexts = date('Y-m-d', strtotime($secondNexts.$next));
						
						$all_consumer=$all_consumer+intval($consumers_arpu["all_consumer"]);
						$pre_consumer=$pre_consumer+intval($consumers_arpu["pre_consumer"]);
						$post_consumer=$post_consumer+intval($consumers_arpu["post_consumer"]);
					}
					 
					$data=array(
	    				"total"=>intval($consumers["all_consumer"]),
	    				"pre_total"=>intval($consumers["pre_consumer"]),
	    				"post_total"=>intval($consumers["post_consumer"]),
	    				"TotalARPU"=>round(intval($TotalARPU)/intval($all_consumer)),
	    				"PreARPU"=>round(intval($PreARPU)/intval($pre_consumer)),
	    				"PostARPU"=>round(intval($PostARPU)/intval($post_consumer)),
						
						"PreRev"=> intval($Pre_debit+$Pre_SD),
	    				"PostRev"=> intval($Post_debit+$Post_SD),
						"PreColl"=> intval($Pre_credit+$Pre_SD),
	    				"PostColl"=> intval($Post_credit+$Post_SD),

						"Pre_debit_chg"=> intval($totalBilledAmount["Pre_debit_chg"]),
	    				"Post_debit_chg"=> intval($totalBilledAmount["Post_debit_chg"]),
						"total_debit_chg"=>intval($totalBilledAmount["Pre_debit_chg"])+intval($totalBilledAmount["Post_debit_chg"]),
						"Pre_credit_chg"=> intval($totalBilledAmount["Pre_credit_chg"]),
	    				"Post_credit_chg"=> intval($totalBilledAmount["Post_credit_chg"]), 
						"total_credit_chg"=>intval($totalBilledAmount["Pre_credit_chg"])+intval($totalBilledAmount["Post_credit_chg"]),
						
						"prepaidPercentage"=>$prepaidPercentage,
						"postpaidPercentage"=>$postpaidPercentage,
						"totalPercentage"=>$totalPercentage,
						"prepaidDebitPercentage"=>$prepaidDebitPercentage,
						"postpaidDebitPercentage"=>$postpaidDebitPercentage,
						"totalDebitPercentage"=>$totalDebitPercentage, 
						
					);
    		}else{$data=array(); }
			
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data,
    		);
    	}
    	catch (Exception $e)
    	{
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	 
	public function plantDataForWidgetAction(){
    	try
    	{ 
		 
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$mppt_id = $request->getParam("mppt_id");
    		$feeder_id = $request->getParam("feeder_id");
    		
    		$yearType = $request->getParam("yearType");
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    		if($mppt_id=="" || $mppt_id==NULL || $mppt_id=='undefined'){$mppt_id=NULL;}
    		if($feeder_id=="" || $feeder_id==NULL || $feeder_id=='undefined'){$feeder_id=NULL;}
    		
    		$curr_date = $request->getParam("curr_date");
    		$last_date = $request->getParam("last_date");
    
    		$zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
    		$curr_date = $zendDate->toString("yyyy-MM-dd");
    		
    		$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
    		$last_date = $zendDate->toString("yyyy-MM-dd");
			
			$cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
            $days = ceil($cal)+1; 
				if($days==0){
					$days =1;
				} 
			$counter=ceil($days/30);
		    $next='+1 month';
			 
			$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    		$wattageMapper=new Application_Model_WattageMapper();
			
    		$mpptMapper = new Application_Model_MpptsMapper();
    		$mpptReadingMapper = new Application_Model_MpptsReadingMapper();
    		$feederMapper =new Application_Model_SiteMasterMeterMapper();
    		$feederReadingMapper =new Application_Model_SiteMeterReadingMapper();
    		$siteStatusMapper=new Application_Model_SiteStatusMapper();
			$consumerMapper=new Application_Model_ConsumersMapper();
			$siteMapper=new Application_Model_SitesMapper();
    		
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		
    		$dataValue=$mpptReadingMapper->getPlantDataByFilters($site_id,$state_id,$role_sites_id,$curr_date,$last_date,$feeder_id,$mppt_id);
    		$feederList=$feederMapper->getAllFeederBySiteID($site_id,$state_id,$role_sites_id,$curr_date,$feeder_id);
    		$feedarHrs=0;
			 
    		if($feederList){
    			foreach ($feederList as $feedarDetail){
    				if($feedarDetail["is_24_hr"]==1){
    					$feedarHr=24;
    				}elseif ($feedarDetail["start_time"] >$feedarDetail["end_time"]){
    					$feedarHr=24+($feedarDetail["end_time"] - $feedarDetail["start_time"]);
    				}else{
    					$feedarHr=$feedarDetail["end_time"] - $feedarDetail["start_time"];
    				}
    				$feedarHrs=$feedarHrs+$feedarHr;
				}
    		}
			
    		/*$prepaidUnit=0;$postPaidUnit=0;
    		$consumers_details=$consumerMapper->getTotalConsumerByRange($curr_date,$last_date,$site_id,$state_id,$role_sites_id);
    		
    		foreach ($consumers_details as $consumer_detail){
    			if($consumer_detail["is_postpaid"]==0){
    				$site_meter_ids=explode(",", $consumer_detail['site_meter_id']);
					$feedarHr_val=0;
    				if(count($site_meter_ids)>0){
    					for ($k=0;$k<count($site_meter_ids);$k++){
    						$feedarDetail=$feederMapper->getMMById($site_meter_ids[$k]);
    						if($feedarDetail){
    							if($feedarDetail->__get("is_24_hr")==1){
    								$feedarHr=24;
    							}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
    								$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
    							}else{
    								$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
    							}
    							$feedarHr_val=$feedarHr_val+$feedarHr;
    						}
    					}
    				}
					
    				$watt_total=$wattageMapper->getWattageBywattId($consumer_detail["wattage"]);
    				if($watt_total){
    					$wattage=$watt_total->__get("wattage");
    				}else{
    					$wattage=0;
    				}
    				
    				$MonthlyUnit_pre=($wattage*$feedarHr_val*$days)/1000;
    				$prepaidUnit=$prepaidUnit+$MonthlyUnit_pre;
    			}else{
					$meterReadings=$meterReadingMapper->getTotalReadingByLastDate($consumer_detail["consumer_id"],$last_date,$curr_date);
    				if($meterReadings){
    					foreach ($meterReadings as $meterReading){
    						 	if($meterReading["start_reading"]!=1){
    								$readings=$meterReadingMapper->getTotalReadingByPreviousDate($consumer_detail["consumer_id"],$last_date);
    								if($readings){
    									$reading=$readings["meter_reading"];
    								}else{
    									$reading=0;
    								}
    								$monthly_Unit_post=$meterReading["meter_reading"]-$reading;
    							}
    						$postPaidUnit=$postPaidUnit+ $monthly_Unit_post;
    					}
    				}
				}
    		}*/
    		
			$plantDetails=$siteMapper->getPlantDetailsBySites($site_id,$state_id,$role_sites_id);
			
			$feederUpHrs=($feedarHrs*$days)-floatval($dataValue["down"]);
			 
			$dayfactor=(floatval($plantDetails["day_factor"])*floatval($dataValue["feeder_DN"]))/100;
			$nightfactor=(floatval($plantDetails["night_factor"])*floatval($dataValue["feeder_DN"]))/100;
			
			$consum_percentage=0;
			if(floatval($dataValue["mppt"])!=0){
				$PV=floatval($dataValue["mppt"])-floatval($dataValue["DG"])-floatval($dataValue["BIOGAS"]);
				$consum_percentage=(floatval($dataValue["feeder"])/floatval($dataValue["mppt"]))*100;
				$con_dg=round($consum_percentage*$dataValue["DG"]/100);
				$con_bio=round($consum_percentage*$dataValue["BIOGAS"]/100);
				$con_pv=round($consum_percentage*$PV/100);
			}
			
				/*$secondNext=$last_date;
				$total_unit=0;
    			for($i=0;$i<$counter;$i++){
					$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
			
					$trans_date=date_parse_from_format("Y-m-d", $secondNext);
    				$month= $trans_date["month"];
    				$year= $trans_date["year"];
					
					$consumers_details=$consumerMapper->getTotalConsumerBySiteIdAndMonthVal($month,$year,$site_id,$state_id,$role_sites_id);	
					
					$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
					
					$secondNext = date('Y-m-d', strtotime($secondNext.$next));
					if ($consumers_details){
						foreach ($consumers_details as $consumer_detail){
							
								if($consumer_detail["is_postpaid"]==0){
									
									$feedarHrs=0;
									$site_meter_ids=explode(",", $consumer_detail['site_meter_id']);
									 
									if(count($site_meter_ids)>0){
										for ($k=0;$k<count($site_meter_ids);$k++){
											$feedarDetail=$feederMapper->getMMById($site_meter_ids[$k]);
											if($feedarDetail){
												if($feedarDetail->__get("is_24_hr")==1){
													$feedarHr=24;
												}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
													$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
												}else{
													$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
												}
												$feedarHrs=$feedarHrs+$feedarHr;
											}
										}
									}
									 
									$watt_total=$wattageMapper->getWattageBywattId($consumer_detail["wattage"]);
									if($watt_total){
										$wattage=$watt_total->__get("wattage");
									}else{
										$wattage=0;
									}
									
									$MonthlyUnit_pre=($wattage*$feedarHrs*$total_days_in_month)/1000;
									$total_unit=$total_unit+$MonthlyUnit_pre;
								}else{
									 
									$meterReading=$meterReadingMapper->getTotalUnitSiteWise($consumer_detail["consumer_id"],$month,$year);
									if($meterReading){
										if(floatval($meterReading["curr_reading"])>0){
											$totalMtrUnit=(floatval($meterReading["curr_reading"])-floatval($meterReading["last_reading"]));
											$monthly_Unit_post=$totalMtrUnit;
										}else{
											$monthly_Unit_post=0;
										}
									}else{
										$monthly_Unit_post=0;
									}
										
									$total_unit=$total_unit+ $monthly_Unit_post;
								}
							}
								
							$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer_detail['consumer_id']);
							$month_scheme_pre_total=0;
							if($consumer_scheme){
								foreach($consumer_scheme as $consumer_schemes){
									$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
									$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
						
									$discount_month=$consumer_schemes["discount_month"];
									$feeder_hours=$consumer_schemes["feeder_hours"];
									$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
									$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month"));
						 
									$dates = new Zend_Date(); 
									$dates->setTimezone("Asia/Calcutta");
									$date_val = $dates->toString("yyyy-MM-dd HH:mm:ss");
									
									if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
										$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]*$feeder_hours*$total_days_in_month)/1000);
										$total_unit=$total_unit + $dailyUnit;
									}
								}
							}
					}
				}*/
				
					$meter_reading_pre=$consumerMapper->getConsumerWattageBySiteId($site_id,$state_id,$role_sites_id,date('Y-m-d', strtotime($curr_date."+1 day")),$last_date);	
					$meter_reading_prepaid=floatval((floatval($meter_reading_pre["wattage"])*floatval($meter_reading_pre["hours"])*$days)/1000);
					
					$meter_reading_postpaid=$meterReadingMapper->getTotalUnitBySites($site_id,$state_id,$role_sites_id,date('Y-m-d', strtotime($curr_date."+1 day")),$last_date);
					
					$totalReadingSold=floatval($meter_reading_postpaid)+$meter_reading_prepaid;
					
					//echo " ".$meter_reading_postpaid." ".$meter_reading_prepaid." ".$totalReadingSold;exit;   
					 
					$total_unit=$totalReadingSold; //93*$dataValue["feeder"]/100;  
					$sold_percentage=0;
					if(floatval($total_unit)!=0){
						$sold_percentage=(floatval($total_unit)/floatval($dataValue["feeder"]))*100;
						$sold_dg=round($sold_percentage*$con_dg/100); 
						$sold_bio=round($sold_percentage*$con_bio/100);
						$sold_pv=round($sold_percentage*$con_pv/100);
					} 
					 
				$genStan=80*round(floatval($plantDetails["Solar_panel_kw"])*floatval($plantDetails["radiation"])*$days)/100; 
				$genbench=80*round(floatval($plantDetails["Solar_panel_kw"])*floatval($plantDetails["site_target"])*$days)/100;				
    		$data=array(
    				"mppt"=>round($dataValue["mppt"]),
					"BIOGAS"=>round($dataValue["BIOGAS"]),
					"DG"=>round($dataValue["DG"]),
					"PV"=>round(floatval($dataValue["mppt"])-floatval($dataValue["DG"])-floatval($dataValue["BIOGAS"])),
					
					"feeder"=>round($dataValue["feeder"]),
					"con_dg"=>round($con_dg),
					"con_bio"=>round($con_bio),
					"con_pv"=>round($con_pv),
					
					"unit_sold"=>round($total_unit),
					"sold_dg"=>round($sold_dg),
					"sold_bio"=>round($sold_bio),
					"sold_pv"=>round($sold_pv),
					
    				"down"=>round($dataValue["down"]),
    				"up"=>round($feederUpHrs),
					"dd_feeder"=>round($dataValue["feeder_DD"]+$dayfactor),
    				"nn_feeder"=>round($dataValue["feeder_NN"]+$nightfactor),
					"dd_feeder_avg"=>round(($dataValue["feeder_DD"]+$dayfactor)/$days),
    				"nn_feeder_avg"=>round(($dataValue["feeder_NN"]+$nightfactor)/$days), 
					"gen_standard"=>round($genStan),
					"gen_target"=>round($genbench),  
					"Solar_panel_kw"=>floatval($plantDetails["Solar_panel_kw"]),
    				"Battery_no_v"=>floatval($plantDetails["Battery_no_v"]),  
    				"dg_capacity"=>floatval($plantDetails["dg_capacity"]),
					"bio_capacity"=>floatval($plantDetails["bio_capacity"]),
    				"radiation"=>floatval(($plantDetails["radiation"])), 
    				"site_target"=>floatval(($plantDetails["site_target"])),
					"plant_duration"=>round(floatval($plantDetails["Solar_panel_kw"])*floatval($plantDetails["radiation"])),
    				"plant_target"=>round(floatval($plantDetails["Solar_panel_kw"])*floatval($plantDetails["site_target"])),
    				"Inverter_no"=>floatval($plantDetails["Inverter_no"]),
					"Charges_controller_no"=>floatval($plantDetails["Charges_controller_no"]),
    				"Battery_no"=>floatval($plantDetails["Battery_no"]),
    				"array_no"=>floatval($plantDetails["array_no"]),
					"Solar_panel_no"=>floatval($plantDetails["Solar_panel_no"]),
    				"array_capacity"=>floatval($plantDetails["array_capacity"]),
					"day_factor"=>round($plantDetails["day_factor"]),
    				"night_factor"=>round($plantDetails["night_factor"]),
					
					"com_rcv"=>1,
					"com_closing"=>1,
    				"com_opening"=>1,
    				
    				//"prepaidUnit"=> floatval($prepaidUnit ),
    				//"postpaidUnit"=>floatval($postPaidUnit),
    				//"total"=>floatval($prepaidUnit+$postPaidUnit)
    		);
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
	public function plantDataForTableAction(){
    	try
    	{
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		$mppt_id = $request->getParam("mppt_id");
    		$feeder_id = $request->getParam("feeder_id");
    
    		$yearType = $request->getParam("yearType");
    		if($state_id=="ALL"){$state_id=NULL;}
    		if($site_id=="ALL"){$site_id=NULL;}
    
    		if($mppt_id=="" || $mppt_id==NULL || $mppt_id=='undefined'){$mppt_id=NULL;}
    		if($feeder_id=="" || $feeder_id==NULL || $feeder_id=='undefined'){$feeder_id=NULL;}
    
    		$curr_date = $request->getParam("curr_date");
    		$last_date = $request->getParam("last_date");
    		
    		$lastYear_curr_date = $request->getParam("lastYear_curr_date");
    		$lastYear_last_date = $request->getParam("lastYear_last_date");
    		
    
    		$zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
    		$curr_date = $zendDate->toString("yyyy-MM-dd");
    
    		$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
    		$last_date = $zendDate->toString("yyyy-MM-dd");
    		
    		$zendDate = new Zend_Date($lastYear_curr_date,"MMMM D, YYYY");
    		$lastYear_curr_date = $zendDate->toString("yyyy-MM-dd");
    		
    		$zendDate = new Zend_Date($lastYear_last_date,"MMMM D, YYYY");
    		$lastYear_last_date = $zendDate->toString("yyyy-MM-dd");
    
    		$mpptMapper = new Application_Model_MpptsMapper();
    		$mpptReadingMapper = new Application_Model_MpptsReadingMapper();
    		$feederMapper =new Application_Model_SiteMasterMeterMapper();
    		$feederReadingMapper =new Application_Model_SiteMeterReadingMapper();
    		
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    
    		$totalMppt=0;$totalLastMppt=0;$totalFeeder=0;$totalLastFeeder=0;
    		
    		$subtotalMppt=0;$subtotalOther=0;$subtotalLastMppt=0;$subtotalFeeder=0;$subtotalLastFeeder=0;
    		$mppt_arr=array();$mppt_other_arr=array();
    		$mppts=$mpptReadingMapper->getFeederAndMppt("mppts","mppt_name");
    		if($mppts){
    			foreach ($mppts as $mppt){
    				$mppt_names=$mpptReadingMapper->getMpptTotalByFilter($site_id,$state_id,$role_sites_id,$curr_date,$last_date,$mppt["mppt_name"]);
    				if($mppt_names){
    					$totalMppt=$totalMppt+floatval($mppt_names["reading"]);
    					if (strpos($mppt["mppt_name"], 'MPPT') !== false)
    					{
    						$subtotalMppt=$subtotalMppt+floatval($mppt_names["reading"]);
    					}
    				}
    				
    				$mppt_last_names=$mpptReadingMapper->getMpptTotalByFilter($site_id,$state_id,$role_sites_id,$lastYear_curr_date,$lastYear_last_date,$mppt["mppt_name"]);
    				if($mppt_last_names){
    					$totalLastMppt=$totalLastMppt+floatval($mppt_last_names["reading"]);
    					if (strpos($mppt["mppt_name"], 'MPPT') !== false)
    					{
    						$subtotalLastMppt=$subtotalLastMppt+floatval($mppt_last_names["reading"]);
    					}
    				}
    				
    				if($mppt_names || $mppt_last_names){
    					$data=array(
    						"mppt_name"=>$mppt["mppt_name"],
    						"last_year_unit"=>round($mppt_last_names["reading"]),
    						"curr_year_unit"=>round($mppt_names["reading"]),
    					);
    					
    					if (strpos($mppt["mppt_name"], 'MPPT') !== false)
    					{
    						$mppt_arr[]=$data;
    					}else{
    						$mppt_other_arr[]=$data;
    					}
    				}
    				
    			}
				 $mppt_arrs = array();
				foreach ($mppt_arr as  $row)
				{
					$mpptName=explode("MPPT",$row["mppt_name"]);
					$data=array(
    						"mppt_name"=>$row["mppt_name"],
    						"last_year_unit"=>round($row["last_year_unit"]),
    						"curr_year_unit"=>round($row["curr_year_unit"]),
							"no"=>intval($mpptName[1])
    					);
						
						$mppt_arrs[]=$data;
				}
				 $sort_arr = array();
					foreach ($mppt_arrs as $key => $row)
					{
						$sort_arr[$key] = $row['no'];
					}
					array_multisort($sort_arr, SORT_ASC ,$mppt_arrs);
    			$data=array(
    					"mppt_name"=>"Total MPPT",
    					"last_year_unit"=>round($subtotalLastMppt),
    					"curr_year_unit"=>round($subtotalMppt),
    			);
    			$mppt_arrs[]=$data;
    			$data=array(
    					"mppt_name"=>"Grand Total",
    					"last_year_unit"=>round($totalLastMppt),
    					"curr_year_unit"=>round($totalMppt),
    			);
    			$mppt_other_arr[]=$data;
    			
    		}
    		
    		$feeder_arr=array();
    		$feeders=$mpptReadingMapper->getFeederAndMppt("site_meter","meter_name");
    		if($feeders){
    			foreach ($feeders as $feeder){
    				$feeder_names=$feederReadingMapper->getFeederTotalByFilter($site_id,$state_id,$role_sites_id,$curr_date,$last_date,$feeder["meter_name"]);
    				if($feeder_names){
    					$totalFeeder=$totalFeeder+floatval($feeder_names["reading"]);
    					 
    				}
    		
    				$feeder_last_names=$feederReadingMapper->getFeederTotalByFilter($site_id,$state_id,$role_sites_id,$lastYear_curr_date,$lastYear_last_date,$feeder["meter_name"]);
    				if($feeder_last_names){
    					$totalLastFeeder=$totalLastFeeder+floatval($feeder_last_names["reading"]);
    				}
    				 
    				if($feeder_names || $feeder_last_names){
						$feederName=explode("FEEDER",$feeder["meter_name"]);
    					$data=array(
    							"meter_name"=>"FEEDER ".$feederName[1],
    							"last_year_unit"=>round($feeder_last_names["reading"]),
    							"curr_year_unit"=>round($feeder_names["reading"]),
    					);
    					$feeder_arr[]=$data; 
    				}
    		 
    			}
    			
    			$data=array(
    					"meter_name"=>"TOTAL FEEDER",
    					"last_year_unit"=>round($totalLastFeeder),
    					"curr_year_unit"=>round($totalFeeder),
    			);
    			$feeder_arr[]=$data;
    			 
    		} 
    		 
    	 
    		$data=array(
    				"mppt"=>$mppt_arrs,
    				"mppt_other"=>$mppt_other_arr,
    				"feeder"=>$feeder_arr,
    				 
    		);
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    
    	}
    	catch(Exception $e)
    	{
    		$meta = array(
    				"code" => $e->getCode(),
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
	public function readingForConsumerAction(){
        try
        {
            $request = $this->getRequest();
            $adddevicemapper=new Application_Model_AddDeviceMapper();
            $consumer_id = $request->getParam("consumer_id");
            $first_date = $request->getParam("first_date");
            $second_date = $request->getParam("second_date");
            
            $zendDate = new Zend_Date($first_date,"MMMM D, YYYY");
            $first_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
 
            $zendDate = new Zend_Date($second_date,"MMMM D, YYYY");
            $second_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
            
            $cal=(strtotime($first_date)-strtotime($second_date)) / (60 * 60 * 24);
              $days= $cal;

            if(intval($days)==0){
                $days=1;
            }
            $con=0;
            $mins=$days*1440;
            $sec=$days*86400;
            $hrs=$mins/60;
            if(intval($hrs)<=24){
                $con=1;
                $counter=intval($hrs);
                $next='+1 hour';
            }else if($days<=60){
                $counter=$days;
                $next='+1 day';
            }elseif ($days<=365){
                $counter=intval($days/7);
                $next='+1 month';
            }else{ 
                $counter=intval($days/365);
                $next='+1 year';
            }
           
		    $data=array();
 
            $meterReadingMapper=new Application_Model_MeterReadingsMapper();
            $time=array();$unit=array();
            $check=explode(" ",$next);
		 
			$secondNext = date('Y-m-d H:i:s', strtotime($second_date .$next));

            for($i=1;$i<=$counter;$i++){

                $readings=$meterReadingMapper->getTotalUnitByConsumerIdDate($consumer_id,$secondNext,$second_date);
				if($readings) {
					$unit[]=sprintf("%.2f",($readings)); 
				}else{
					$unit[]=0; 
				}
				
				if($con==1){
					$time[] = date('H:i', strtotime($second_date));
				}else{
					$time[] = $i.$check[1];
                }
				
				$second_date=$secondNext;
				$secondNext = date('Y-m-d H:i:s', strtotime($second_date .$next));
             
            }
			 
            array_push($data, $time);
            array_push($data, $unit);

            $meta = array(
                "code" => 200,
                "message" => "SUCCESS" 
            );

            $arr = array(
                "meta" => $meta,
                "data" => $data
            );

        }
        catch(Exception $e)
        {
            $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json; 
    }
	
}
?>