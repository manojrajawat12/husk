<?php

class Api_LoginHistoryController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
    }

    public function getAllLastLoginHistoryAction() {

        try {
			$request = $this->getRequest();
            $entry_by = $request->getParam("entry_by");
            $loginHistoryMapper = new Application_Model_LoginHistoryMapper();
            $UsersMapper = new Application_Model_UsersMapper();
			$collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
            
			if($entry_by=='Website'){ 
            $users = $UsersMapper->getAllUsers();
			
            if(count($users) >0){
                foreach ($users as $user) {
					$loginHistories = $loginHistoryMapper->getAllLastLoginHistoryById($user->__get("user_id"),$entry_by);
					 
					if($loginHistories){
						$loginHistory=$loginHistories[0]; 
						$hits=count($loginHistories); 
						$timestamp=$loginHistory["timestamp"];
						 
						$curr_date = new Zend_Date();
						$curr_date->setTimezone("Asia/Kolkata");
						$curr_timestamp = $curr_date->toString("yyyy-MM-dd HH:mm:ss");

						$timestamps =date("d-M-Y g:i a",strtotime($timestamp));
						 
						$cal=(strtotime($curr_timestamp)-strtotime($timestamp)) / (60 * 60 * 24);
         				 $days= $cal ;
						 
         				$mins=$days*1440; 
         				$sec=$days*86400;
						$hrs=$mins/60;
				
						if($sec<60){
							$next=intval($sec)." second ago";
						}elseif($mins<60){
							$next=intval($mins)." minute ago";
						}elseif($hrs<24){
							$next=intval($hrs)." hour ago";
						}elseif($days<=7){
							$next=intval($days)." day ago";
						}elseif ($days<=31){
							$days=intval($days/7);
							$next=$days." week ago";
						}elseif ($days<=365){
							$days=intval($days/30);
         					$next=$days." month ago";
						}else{
							$days=intval($days/365);
         					$next=$days." year ago";
						}
					}else{
						$next = "Stil not Login";
						$hits=NULL;
						$timestamps=null;$curr_timestamp=null;
					} 
                    $data=array(
							"user_id" => $user->__get("user_id"),
                            "user_name" => $user->__get("user_fname")." ".$user->__get("user_lname"),
                            "timestamp" => $timestamps,
                            "curr_timestamp" => $curr_timestamp,
                            "time_lap" => $next,
							"hits" => $hits
                        );
                    $login_arr[] = $data;
                }
				 $sort_arr = array();
				foreach ($login_arr as $key => $row)
				{
					$sort_arr[$key] = $row['hits'];
				}
				array_multisort($sort_arr, SORT_DESC ,$login_arr);
				
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $login_arr,
                );
    
            }else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
		}else{
			$agents = $collectionAgentMapper->getAllCollectionAgents();
			
            if(count($agents) >0){
                foreach ($agents as $agent) {
					$loginHistories = $loginHistoryMapper->getAllLastLoginHistoryById($agent->__get("collection_agent_id"),$entry_by);
					 
					if($loginHistories){
						$loginHistory=$loginHistories[0]; 
						$hits=count($loginHistories); 
						$timestamp=$loginHistory["timestamp"];
						 
						$curr_date = new Zend_Date();
						$curr_date->setTimezone("Asia/Kolkata");
						$curr_timestamp = $curr_date->toString("yyyy-MM-dd HH:mm:ss");

						$timestamps =date("d-M-Y g:i a",strtotime($timestamp));
						 
						$cal=(strtotime($curr_timestamp)-strtotime($timestamp)) / (60 * 60 * 24);
         				 $days= $cal ;
						 
         				$mins=$days*1440; 
         				$sec=$days*86400;
						$hrs=$mins/60;
				
						if($sec<60){
							$next=intval($sec)." second ago";
						}elseif($mins<60){
							$next=intval($mins)." minute ago";
						}elseif($hrs<24){
							$next=intval($hrs)." hour ago";
						}elseif($days<=7){
							$next=intval($days)." day ago";
						}elseif ($days<=31){
							$days=intval($days/7);
							$next=$days." week ago";
						}elseif ($days<=365){
							$days=intval($days/30);
         					$next=$days." month ago";
						}else{
							$days=intval($days/365);
         					$next=$days." year ago";
						}
					}else{
						$next = "Stil not Login";
						$hits=NULL;
						$timestamps=null;$curr_timestamp=null;
					} 
                    $data=array(
                            "user_id" => $agent->__get("collection_agent_id"),
                            "user_name" => $agent->__get("agent_fname")." ".$agent->__get("agent_lname"),
                            "timestamp" => $timestamps,
                            "curr_timestamp" => $curr_timestamp,
                            "time_lap" => $next,
							"hits" => $hits,
							
                        );
                    $login_arr[] = $data;
                }
				 $sort_arr = array();
				foreach ($login_arr as $key => $row)
				{
					$sort_arr[$key] = $row['hits'];
				}
				array_multisort($sort_arr, SORT_DESC ,$login_arr);
				
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $login_arr,
                );
    
            }else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
		}	
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

	public function getAllLastLoginHistoryIpAction() {

        try {
			$request = $this->getRequest();
            $user_id = $request->getParam("user_id");
			$entry_by = $request->getParam("entry_by");
            $loginHistoryMapper = new Application_Model_LoginHistoryMapper();
            $UsersMapper = new Application_Model_UsersMapper();
            $collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
			 
			$loginHistories = $loginHistoryMapper->getAllLastLoginHistoryByIP($user_id,$entry_by);
			$login_arr=array();
			if($loginHistories){
				foreach($loginHistories as $loginHistorie){
					if($entry_by=='Website'){
						$user=$UsersMapper->getUserById($user_id);
						$userId=$user->__get("user_id");
						$userName=$user->__get("user_fname")." ".$user->__get("user_lname");
					}else{
						$collectionAgents=$collectionAgentMapper->getCollectionAgentById($user_id);
						$userId=$collectionAgents->__get("collection_agent_id");
						$userName=$collectionAgents->__get("agent_fname")." ".$collectionAgents->__get("agent_lname");
					}
					
					$data=array(
                            "user_id" => $userId,
                            "user_name" =>$userName,
                            "timestamp" => date("d-M-Y g:i a",strtotime($loginHistorie["timestamp"])),
                            "ip" => $loginHistorie["ip_address"],
							
                        );
                    $login_arr[] = $data;
				}
                    
                
				
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $login_arr,
                );
    
            }
            else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
    
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	}
