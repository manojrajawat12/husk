<?php

class Api_GraphsController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }
    public function indexAction(){
        
    }
    public function packageConsumersCountAction()
    {
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $packagemapper=new Application_Model_PackagesMapper();
            $consumerMapper=new Application_Model_ConsumersMapper();
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            $state_id=$auth->getIdentity()->state_id;
            
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            $packages=$packagemapper->getAllPackages();
           
            
            if($packages)
            {
                $cols = array();
                
                $col = array();
                $col["id"] = "package";
                $col["label"] = "Package";
                $col["type"] = "string";
                $cols[] = $col;
                
                $col = array();
                $col["id"] = "consumers_count";
                $col["label"] = "Consumers";
                $col["type"] = "number";
                $cols[] = $col;
                
                $rows = array();
                foreach ($packages as $package) 
                {
                    $cs = array();
                    $package_name = $package->__get("package_name");
                  /*   if($user=="3" ||$user=="5"){ */
                    	$consumers=$consumerMapper->getAllConsumerByPackageId($package->__get("package_id"),$site_id,$state_id,$role_sites_id);
                   /*  }
                    else{
                    	$consumers=$consumerMapper->getAllConsumerByPackageId($package->__get("package_id"),$site_id);
                    } */
                    $c = array();
                    $c["v"] = $package_name;
                    $cs[] = $c;
                    
                    $c = array();
                    $c["v"] = count($consumers);
                    $cs[] = $c;
                    
                    $row["c"] = $cs;
                    $rows[] = $row;

                }
                $data = array(
                    "cols" => $cols,
                    "rows" => $rows
                );
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data,
                );
            } 
            else 
            {
                $meta = array(
                    "code" => 404,
                    "message" => "Not found"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	public function monthlyBillingRevenueAction()
    {
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $month = $request->getParam("month");
            $year = $request->getParam("year");
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            $state_id=$auth->getIdentity()->state_id;
            
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            
            $data=array();
            $site_data=array();
            for($i=5;$i>=0;$i--){
                $cs = array();
                $zendDate = new Zend_Date();
                $zendDate->setTimezone("Asia/Calcutta");
                $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_NAME);
                $year = $zendDate->toString("yyyy");
               /*  if($user=="3" || $user=="5"){ */
                	$debit = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"DEBIT",$year,$state_id,$role_sites_id));
                	$credit = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"CREDIT",$year,$state_id,$role_sites_id));
                	
               /*  }else{
                	$debit = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"DEBIT",$year));
                	$credit = intval($cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"CREDIT",$year));
                	
                } */
                
                $site_data[] = array(
                		"site_id" => $site_id,
                		"month"=>$month,
                		"credit" => intval($credit),
                		"debit" => intval($debit),
                		);
             }
            
      $revenueData = array();
      $billingData = array();
      foreach($site_data as $site_val)
      {
      
       $revenueData[] = array($site_val["month"], $site_val["credit"]);
       $billingData[] = array($site_val["month"], $site_val["debit"]);
      }
      
      $lines=array("lineWidth"=> 1);
      $revenueArray = array("data" => $revenueData,
            "label" => "Revenue (CREDIT)",
            "lines"=>$lines,
            "shadowSize"=>0,
            );
      $billingArray  = array("data" => $billingData,
            "label" => "Billing (DEBIT)",
            "lines"=>$lines,
            "shadowSize"=>0,
            );
     
       array_push($data, $billingArray);
       array_push($data, $revenueArray);
    
          
      	$meta = array(
        	"code" => 200,
        	"message" => "SUCCESS"
      	);
    
      	$arr = array(
        	"meta" => $meta,
       	    "data" => $data
      	);
    
      }
      catch(Exception $e)
      {
      	$meta = array(
         	"code" => $e->getCode(),
      	 	"message" => $e->getMessage()
      	);
    
     	$arr = array(
      	 	"meta" => $meta
      	);
      }
      $json = json_encode($arr, JSON_PRETTY_PRINT);
      echo $json;
    }
    public function monthlyConsumersCountAction()
    {
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            $state_id=$auth->getIdentity()->state_id;
           
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            
            $data=array();
            $consumer_data=array();
            for($i=5;$i>=0;$i--){
            
            	$zendDate = new Zend_Date();
                $zendDate->setTimezone("Asia/Calcutta");
                $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_SHORT);
                $month_name = $zendDate->toString(Zend_Date::MONTH_NAME);
                $year = $zendDate->toString("yyyy");
                $date_string = "01/".$month."/".$year;
             /*    if($user=="3" ||$user=="5"){ */
                	$count = $consumersMapper->getTotalConsumersCountByMonth($date_string,$site_id,$state_id,$role_sites_id);
                	
               /*  }else{
                	$count = $consumersMapper->getTotalConsumersCountByMonth($date_string,$site_id);
                	
                } */
                
            	$consumer_data[] =array($month_name, $count); 
            }
            $consumerData = array();
          
            $lines=array("lineWidth"=> 1);
            $consumerData = array("data" => $consumer_data,
            		"label" => "Consumer Count",
            		"lines"=>$lines,
            		"shadowSize"=>0,
            );
        
            array_push($data, $consumerData);
          
            $meta = array(
            		"code" => 200,
            		"message" => "SUCCESS"
            );
            
            $arr = array(
            		"meta" => $meta,
            		"data" => $data
            );
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function connectionChargeStatsAction()
    {
        try 
        {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id");
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            $state_id=$auth->getIdentity()->state_id;
            
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            
            $consumersMapper = new Application_Model_ConsumersMapper();
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
           
            	$charged_act_amount  = $consumersMapper->getTotalCollectedAmount($site_id,$state_id,$role_sites_id);
            	$received_act_amount = $cashRegisterMapper->getTotalCollectedActivation($site_id,$state_id,$role_sites_id);
            
            $pending_act_amount = $charged_act_amount-$received_act_amount;
            
            $cols = array();
                
            $col = array();
            $col["id"] = "month";
            $col["label"] = "Month";
            $col["type"] = "string";
            $cols[] = $col;

            $col = array();
            $col["id"] = "received";
            $col["label"] = "Received OTP";
            $col["type"] = "number";
            $cols[] = $col;
            
            $col = array();
            $col["id"] = "pending";
            $col["label"] = "Pending OTP";
            $col["type"] = "number";
            $cols[] = $col;
            
            $rows = array();
            $cs = array();
            
            $c = array();
            $c["v"] = "";
            $cs[] = $c;

            $c = array();
            $c["v"] =$received_act_amount; 
            $cs[] = $c;

            $c = array();
            $c["v"] = $pending_act_amount;
            $cs[] = $c;


            $row["c"] = $cs;
            $rows[] = $row;
            
            $data = array(
                "cols" => $cols,
                "rows" => $rows
            );
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $arr = array(
                "meta" => $meta,
                "data" => $data,
            );
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
 public function billedCollectionWidgetAction()
    {
     try
     {
      $request = $this->getRequest();
      $month = $request->getParam("month");
      $year = $request->getParam("year");
      $state_id=$request->getParam("state_id");
      $sitesMapper = new Application_Model_SitesMapper();
      $auth=new My_Auth('user');
      $user=$auth->getIdentity()->user_role;
      
      $roleSession = new Zend_Session_Namespace('roles');
      $role_sites_id=$roleSession->site_id;
      
      $data=array();
      $sites=$sitesMapper->getSiteByStateId($state_id,$role_sites_id,$user);
    
      $site_data=array();
      foreach($sites as $site)
      {
      
      $site_id=$site->__get("site_id");
      $cashRegisterMapper = new Application_Model_CashRegisterMapper();
  
      	$totalDebit = $cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"DEBIT",$year);
      	$totalActivation = $cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"ACTIVATION");
      	$totalCredit = $cashRegisterMapper->getTotalCollectedByMonth($month,$site_id,"CREDIT",$year);
     
      $site_data[] = array(
      "site_id" => $site_id,
      "site_name" => $site->__get("site_name"),
      "total_debit" => intval($totalDebit),
      "total_activation" => intval($totalActivation),
      "total_credit" => intval($totalCredit),
      "total_collected" => $totalCredit+$totalActivation,
      );
      }
      $this->array_sort_by_column($site_data, 'total_debit') ;
      $collectionData = array();
      $billingData = array();
      foreach($site_data as $site_val)
      {
       $collectionData[] = array($site_val["site_name"], $site_val["total_credit"]);
       $billingData[] = array($site_val["site_name"], $site_val["total_debit"]);
      }
      $lines=array("lineWidth"=> 1);
      $collectionArray = array("data" => $collectionData,
            "label" => "Collection",
            "lines"=>$lines,
            "shadowSize"=>0,
            );
      $billingArray  = array("data" => $billingData,
            "label" => "Billed",
            "lines"=>$lines,
            "shadowSize"=>0,
            );
      array_push($data, $billingArray);
     array_push($data, $collectionArray);
    
    
       
      $meta = array(
        "code" => 200,
        "message" => "SUCCESS"
      );
    
      $arr = array(
        "meta" => $meta,
        "data" => $data
      );
    
      }
      catch(Exception $e)
      {
      $meta = array(
        "code" => $e->getCode(),
         "message" => $e->getMessage()
      );
    
      $arr = array(
      "meta" => $meta
      );
      }
      $json = json_encode($arr, JSON_PRETTY_PRINT);
      echo $json;
    }
    
    public function array_sort_by_column(&$arr, $col, $dir = SORT_DESC) {
    	$sort_col = array();
    	foreach ($arr as $key=> $row) {
    		$sort_col[$key] = $row[$col];
    	}
    
    	array_multisort($sort_col, $dir, $arr);
    }
    public function destroyAction()
    {
    	unset($this);
    	gc_enable() ;gc_collect_cycles();gc_disable();
    }
}
?>