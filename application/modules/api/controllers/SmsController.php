<?php

class Api_SmsController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //header('Content-Type: application/json');
    }
    public function indexAction(){
        
    }
    public function sendWelcomeSmsAction()
    {
        try
        {
            $request = $this->getRequest();
            $connection_id = $request->getParam("connection_id");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumer = $consumersMapper->getConsumerByConnectionID($connection_id);
            if(!$consumer)
            {
                throw new Exception("Consumer not found",404);
            }
            
            $number = $consumer->__get("consumer_code");
            $name = $consumer->__get("consumer_name");
            $sms = "Namastey ".$name.". TARAurja mein aapka swagat hai.";
            $result = $this->_smsNotification($number, $sms);
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );

            $arr = array(
                "meta" => $meta,
                "data" => $result
            ); 
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function sendCsaRevenueTargetAction()
    {
        try
        {
            $sitesMapper = new Application_Model_SitesMapper();
            $sites = $sitesMapper->getAllSites();
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $zendDate = new Zend_Date();
            $zendDate->setTimezone("Asia/Calcutta");
            $month  = $zendDate->toString("MM");
            $month_name = $zendDate->toString("MMM");
            $year  = $zendDate->toString("yyyy");
            $date = $zendDate->toString("dd/MM/yyyy");
            $agentMappingsMapper = new Application_Model_AgentSiteMappingsMapper();
            $agentMapper = new Application_Model_CollectionAgentsMapper();
            foreach($sites as $site)
            {
                $agentMappings = $agentMappingsMapper->getAgentSiteMappingBySiteId($site->__get("site_id"));
                $site_name = $site->__get("site_name");
                $deadline_date = "10/".$month."/".$year;
                if($agentMappings)
                {
                    $month_amount = $cashRegisterMapper->getOutstandingBySiteId($site->__get("site_id"), $date);
                    $otp_amount = $cashRegisterMapper->getOTPDueBySiteId($site->__get("site_id"), $month, $year);
                    //$sms = "The Target for ".$site_name." for the month of ".$month_name." is Rs. ".$month_amount." for Monthly collection and Rs. ".$otp_amount." as outstanding OTP";
                    foreach($agentMappings as $agentMap)
                    {
                        $agent = $agentMapper->getCollectionAgentById($agentMap->__get("collection_agent_id"));
                        if($agent)
                        {
                            $number = $agent->__get("agent_mobile");
                            $ca_name = $agent->__get("agent_fname")." ".$agent->__get("agent_lname");
                            $sms = "Dear ".$ca_name.", Monthly Target for ".$site_name." is Rs.".$month_amount.". Monthly OTP Recovery Amount is Rs.".$otp_amount.". Deadline ".$deadline_date.".";
                            $this->_smsNotification($number, $sms);
                        }
                    }
                }
            }
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    protected function _smsNotification($number, $sms) {
        $number = substr($number, 0, 10);
        $sms = urlencode($sms);
        //$url = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=TARA&pwd=921118&to=91" . $number . "&sid=TARAMU&msg=" . $sms . "&fl=0&gwid=2";
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=2000140530&auth_scheme=plain&password=wlVYuapVg&v=1.1&format=text";
        //echo $url;exit;
        $text = file_get_contents($url);
        //echo $text;
        return $text;
    }
    public function templateAction()
    {
        try
        {
            $request = $this->getRequest();
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
public function sendProgressSmsAction()
{
	
	 try
        {
            $request = $this->getRequest();
			$role_id=$request->getParam("user_role");
			$userMapper=new Application_Model_UsersMapper();
			//$users=$userMapper->getUserByRoleId($role_id);//$userMapper->getAllUsers();//
			$type=$request->getParam("type");
		
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			$trans_date=date_parse_from_format("Y-m-d", $timestamp);
			$day=$trans_date["day"];
			$month= $trans_date["month"];
			$year= $trans_date["year"];
			$next_month=$month;
			$next_year=$year;
			if($day==25){
				$next_month=($month==12)?1:$month+1;
				$next_year=($month==12)?$year+1:$year;
			}
			$months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
			$month_name=$months[$month-1];
			$next_month_name=$months[$next_month-1];
			
		    $date=date('Y-m-t',strtotime($date));
			if($users=$userMapper->getUserByRoleId($role_id))
			{
			foreach($users as $user)
		 	{
	     	 $email_id=$user->__get("user_email");
	     	 $number=$user->__get("phone");
			
			$roleMessageMapper= new Application_Model_RoleMessageMapper();
			$roleMessages= $roleMessageMapper->getRoleMessageByRole($role_id,$type);
			
			for($r=0;$r<count($roleMessages);$r++)
			{
			
			$roleMapper=new Application_Model_RoleSiteMapper();
	    	$role_sites=$roleMapper->getAllRoleSiteById($role_id);
			$sites_arr=array();
			$cluster_arr=array();
			$state_arr=array();
			if(count($role_sites)>0){
	    	foreach ($role_sites as $role_site){
	    		$sites_arr[]=$role_site->__get("site_id");
	    		$cluster_arr[]=$role_site->__get("cluster_id");
	    		$state_arr[]=$role_site->__get("state_id");
	    	}
	    	
			}
			$role_sites_id=implode(',', $sites_arr);
			$siteMapper=new Application_Model_SitesMapper();
			$clusters=array_unique($cluster_arr);
			
			$clustermapper=new Application_Model_ClustersMapper();
			$stateMapper= new Application_Model_StatesMapper();
			$states=array_unique($state_arr);
			$msg="";
			$msgName=$roleMessages[$r]["message_name"];
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();		
				switch($msgName)
				{
					case "Begin Level1":
						$totalDebit_current = $cashRegisterMapper->getTotalCollectedByMonth($month,NULL,"DEBIT",$year,NULL,$sites_arr);	
						$totalDebit_current=($totalDebit_current=="" ||$totalDebit_current==NULL)?0:$totalDebit_current;
					    $totalDebit_next =$cashRegisterMapper->getTotalCollectedNextMonth(NULL,NULL,$sites_arr,NULL);//$cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr);	
						$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
						$tot_mix_deb= $this->generateNextBillsMixmode($date,NULL,NULL,$sites_arr,NULL) ;
						$totalDebit_next=$totalDebit_next+$tot_mix_deb;
						
						$totalDebit_current_Up = $cashRegisterMapper->getTotalCollectedByMonth($month,NULL,"DEBIT",$year,1,$sites_arr);	
						$totalDebit_current_Up=($totalDebit_current_Up=="" ||$totalDebit_current_Up==NULL)?0:$totalDebit_current_Up;
				        $totalDebit_next_Up = $cashRegisterMapper->getTotalCollectedNextMonth(NULL,1,$sites_arr,NULL);//$cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,1,$sites_arr);	
						$totalDebit_next_Up=($totalDebit_next_Up=="" ||$totalDebit_next_Up==NULL)?0:$totalDebit_next_Up;
						$tot_mix_deb_Up= $this->generateNextBillsMixmode($date,NULL,1,$sites_arr,NULL) ;
						$totalDebit_next_Up=$totalDebit_next_Up+$tot_mix_deb_Up;
						
						$totalDebit_current_Bihar = $cashRegisterMapper->getTotalCollectedByMonth($month,NULL,"DEBIT",$year,2,$sites_arr);	
						$totalDebit_current_Bihar=($totalDebit_current_Bihar=="" ||$totalDebit_current_Bihar==NULL)?0:$totalDebit_current_Bihar;
					    $totalDebit_next_Bihar = $cashRegisterMapper->getTotalCollectedNextMonth(NULL,2,$sites_arr,NULL);//$cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,2,$sites_arr);
						$totalDebit_next_Bihar=($totalDebit_next_Bihar=="" ||$totalDebit_next_Bihar==NULL)?0:$totalDebit_next_Bihar;
						$tot_mix_deb_Bihar= $this->generateNextBillsMixmode($date,NULL,2,$sites_arr,NULL) ;
						$totalDebit_next_Bihar=$totalDebit_next_Bihar+$tot_mix_deb_Bihar;
						
						$msg="Billed Amount: ($month_name) Rs.$totalDebit_current ($next_month_name) Rs.$totalDebit_next.UP ($month_name) Rs.$totalDebit_current_Up ($next_month_name) $totalDebit_next_Up. Bihar ($month_name) 
						Rs.$totalDebit_current_Bihar ($next_month_name) $totalDebit_next_Bihar.";
					break;
						
					case "Begin Level2":
						for($i=0;$i<count($clusters);$i++)
						{
							$cluster_data=$clustermapper->getClusterById($clusters[$i]);
							$cluster_name=$cluster_data->__get("cluster_name");
							$totalDebit_current = $cashRegisterMapper->getTotalCollectedByMonth($month,NULL,"DEBIT",$year,NULL,$sites_arr,$clusters[$i]);	
							$totalDebit_current=($totalDebit_current=="" ||$totalDebit_current==NULL)?0:$totalDebit_current;
							$totalDebit_next = $cashRegisterMapper->getTotalCollectedNextMonth(NULL,NULL,$sites_arr,$clusters[$i]);//$cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr,$clusters[$i]);	
							$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
							$tot_mix_deb= $this->generateNextBillsMixmode($date,NULL,NULL,$sites_arr,$clusters[$i]) ;
							$totalDebit_next=$totalDebit_next+$tot_mix_deb;
							
							$msg=$msg." $cluster_name Billed Amount: ($month_name) Rs.$totalDebit_current ($next_month_name) Rs.$totalDebit_next.";
						
						}
						
					break;
					case "Begin Level3":
					
						for($i=0;$i<count($clusters);$i++)
						{
							$cluster_data=$clustermapper->getClusterById($clusters[$i]);
							$cluster_name=$cluster_data->__get("cluster_name");
							$totalDebit_current = $cashRegisterMapper->getTotalCollectedByMonth($month,NULL,"DEBIT",$year,NULL,$sites_arr,$clusters[$i]);	
							$totalDebit_current=($totalDebit_current=="" ||$totalDebit_current==NULL)?0:$totalDebit_current;
							$totalDebit_next =$cashRegisterMapper->getTotalCollectedNextMonth(NULL,NULL,$sites_arr,$clusters[$i]);// $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr,$clusters[$i]);	
							$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
							$tot_mix_deb= $this->generateNextBillsMixmode($date,NULL,NULL,$sites_arr,$clusters[$i]) ;
							$totalDebit_next=$totalDebit_next+$tot_mix_deb;
							
							$msg=$msg."$cluster_name Total Billed: ($month_name) Rs.$totalDebit_current ($next_month_name) Rs.$totalDebit_next.";
						}
					break;
					
					case "Weekly Level1":
							
						$totalDebit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr);
						$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
						$totalCredit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,NULL,$sites_arr);		
						$totalCredit_next=($totalCredit_next=="" ||$totalCredit_next==NULL)?0:$totalCredit_next;
						$total_percentage=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next/$totalDebit_next)*100);
						
						$totalDebit_next_Up = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,1,$sites_arr);	
						$totalDebit_next_Up=($totalDebit_next_Up=="" ||$totalDebit_next_Up==NULL)?0:$totalDebit_next_Up;
						$totalCredit_next_Up = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,1,$sites_arr);	
						$totalCredit_next_Up=($totalCredit_next_Up=="" ||$totalCredit_next_Up==NULL)?0:$totalCredit_next_Up;
						$total_percentage_Up=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next_Up/$totalDebit_next_Up)*100);
						
						$totalDebit_next_Bihar = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,2,$sites_arr);
						$totalDebit_next_Bihar=($totalDebit_next_Bihar=="" ||$totalDebit_next_Bihar==NULL)?0:$totalDebit_next_Bihar;
						$totalCredit_next_Bihar = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,2,$sites_arr);		
						$totalCredit_next_Bihar=($totalCredit_next_Bihar=="" ||$totalCredit_next_Bihar==NULL)?0:$totalCredit_next_Bihar;
						$total_percentage_Bihar=($totalDebit_next_Bihar=="" ||$totalDebit_next_Bihar==NULL)?0:intval(($totalCredit_next_Bihar/$totalDebit_next_Bihar)*100);
						
						$msg="Overall $next_month_name Collections  Rs.$totalCredit_next of Rs.$totalDebit_next ($total_percentage%). UP Rs.$totalCredit_next_Up of Rs.$totalDebit_next_Up ($total_percentage_Up%). Bihar Rs.$totalCredit_next_Bihar of Rs.$totalDebit_next_Bihar ($total_percentage_Bihar%).";
					break;
					case "Weekly Level2":
						
						for($j=0;$j<count($states);$j++)
						{
							$msg=($msg=="")?$msg:$msg."#";
							$state_data=$stateMapper->getStateById($states[$j]);
							$state_name=$state_data->__get("state_name");
							$totalDebit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,$states[$j],$sites_arr);	
							$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
							$totalCredit_next= $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,$states[$j],$sites_arr);	
							$totalCredit_next=($totalCredit_next=="" ||$totalCredit_next==NULL)?0:$totalCredit_next;
							
							$total_percentage=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next/$totalDebit_next)*100);
							$msg=$msg."$state_name Overall $next_month_name Collections Rs.$totalCredit_next of Rs.$totalDebit_next ($total_percentage%).";
							for($i=0;$i<count($clusters);$i++)
							{
								$cluster_data=$clustermapper->getClusterById($clusters[$i]);
								$cluster_name=$cluster_data->__get("cluster_name");
								$cluster_state_id=$cluster_data->__get("state_id");
								if($cluster_state_id ==$states[$j])
								{
									$totalDebit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr,$cluster_arr[$i]);	
									$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
									$totalCredit_next= $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,NULL,$sites_arr,$cluster_arr[$i]);	
									$totalCredit_next=($totalCredit_next=="" ||$totalCredit_next==NULL)?0:$totalCredit_next;
									$total_percentage=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next/$totalDebit_next)*100);
									$msg=$msg." $cluster_name Rs.$totalCredit_next of Rs.$totalDebit_next ($total_percentage%).";
								}
								
							}
							
						}
						
					break;
					case "Weekly Level3":
						
							for($i=0;$i<count($clusters);$i++)
							{
								
								$cluster_data=$clustermapper->getClusterById($clusters[$i]);
								$cluster_name=$cluster_data->__get("cluster_name");
									
								$totalDebit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr,$cluster_arr[$i]);	
								$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
								$totalCredit_next= $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,NULL,$sites_arr,$cluster_arr[$i]);	
								$totalCredit_next=($totalCredit_next=="" ||$totalCredit_next==NULL)?0:$totalCredit_next;
								$total_percentage=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next/$totalDebit_next)*100);
								$msg=$msg."$cluster_name Overall $next_month_name Collections Rs.$totalCredit_next of Rs.$totalDebit_next ($total_percentage%). ";
							}
	
					break;
					case "Ending Level1":
						$totalDebit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr);
						$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
						$totalCredit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,NULL,$sites_arr);		
						$totalCredit_next=($totalCredit_next=="" ||$totalCredit_next==NULL)?0:$totalCredit_next;
						$total_percentage=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next/$totalDebit_next)*100);
						
						$totalDebit_next_Up = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,1,$sites_arr);	
						$totalDebit_next_Up=($totalDebit_next_Up=="" ||$totalDebit_next_Up==NULL)?0:$totalDebit_next_Up;
						$totalCredit_next_Up = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,1,$sites_arr);	
						$totalCredit_next_Up=($totalCredit_next_Up=="" ||$totalCredit_next_Up==NULL)?0:$totalCredit_next_Up;
						$total_percentage_Up=($totalDebit_next_Up=="" ||$totalDebit_next_Up==NULL)?0:intval(($totalCredit_next_Up/$totalDebit_next_Up)*100);
						
						$totalDebit_next_Bihar = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,2,$sites_arr);
						$totalCredit_next_Bihar = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,2,$sites_arr);		
						$total_percentage_Bihar=($totalDebit_next_Bihar=="" ||$totalDebit_next_Bihar==NULL)?0:intval(($totalCredit_next_Bihar/$totalDebit_next_Bihar)*100);
						
						$msg="Overall $next_month_name Closing @ Rs.$totalCredit_next of Rs.$totalDebit_next ($total_percentage%). UP Rs.$totalCredit_next_Up of Rs.$totalDebit_next_Up ($total_percentage_Up%). Bihar Rs.$totalCredit_next_Bihar of Rs.$totalDebit_next_Bihar ($total_percentage_Bihar%).";
					
					break;
					case "Ending Level2":
						for($j=0;$j<count($states);$j++)
						{
							$msg=($msg=="")?$msg:$msg."#";
							$state_data=$stateMapper->getStateById($states[$j]);
							$state_name=$state_data->__get("state_name");
							$totalDebit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,$states[$j],$sites_arr);	
							$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
							$totalCredit_next= $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,$states[$j],$sites_arr);	
							$totalCredit_next=($totalCredit_next=="" ||$totalCredit_next==NULL)?0:$totalCredit_next;
							$total_percentage=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next/$totalDebit_next)*100);
							$msg=$msg."$state_name $next_month_name Closing @ Rs.$totalCredit_next of Rs.$totalDebit_next ($total_percentage%).";
							for($i=0;$i<count($clusters);$i++)
							{
								$cluster_data=$clustermapper->getClusterById($clusters[$i]);
								$cluster_name=$cluster_data->__get("cluster_name");
								$cluster_state_id=$cluster_data->__get("state_id");
								
								if($cluster_state_id ==$states[$j])
								{	
									$totalDebit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr,$cluster_arr[$i]);	
									$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
									$totalCredit_next= $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,NULL,$sites_arr,$cluster_arr[$i]);	
									$totalCredit_next=($totalCredit_next=="" ||$totalCredit_next==NULL)?0:$totalCredit_next;
									$total_percentage=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next/$totalDebit_next)*100);
									$msg=$msg."$cluster_name Rs.$totalCredit_next of Rs.$totalDebit_next ($total_percentage%).";
								}
							}
						}
					break;
					case "Ending Level3":
							for($i=0;$i<count($clusters);$i++)
							{
								$cluster_data=$clustermapper->getClusterById($clusters[$i]);
								$cluster_name=$cluster_data->__get("cluster_name");
								
								$totalDebit_next = $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"DEBIT",$next_year,NULL,$sites_arr,$cluster_arr[$i]);	
								$totalDebit_next=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:$totalDebit_next;
								$totalCredit_next= $cashRegisterMapper->getTotalCollectedByMonth($next_month,NULL,"Credit",$next_year,NULL,$sites_arr,$cluster_arr[$i]);	
								$totalCredit_next=($totalCredit_next=="" ||$totalCredit_next==NULL)?0:$totalCredit_next;
								$total_percentage=($totalDebit_next=="" ||$totalDebit_next==NULL)?0:intval(($totalCredit_next/$totalDebit_next)*100);
								$msg=$msg."$cluster_name $next_month_name Closing Rs.$totalCredit_next of Rs.$totalDebit_next ($total_percentage%).";
							}
						
					break;
				}
			$messages=explode("#",$msg);
			//$number="7533038564";
			for($i=0;$i<count($messages);$i++)
			{
				//echo $messages[$i]."1";
				$result[]=$messages[$i];
				if($number !="" && $messages[$i]!="")
				{
					 $res= $this->_smsNotification($number, $messages[$i]);
				}
			}
			
			
			}
		$meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );

            $arr = array(
                "meta" => $meta,
                "data" => $result
            );
	 }
			}
			else{
			$meta = array(
                "code" => 401,
                "message" => "No data found"
            );

            $arr = array(
                "meta" => $meta
            ); 
			}
        }
        catch(Exception $e)
        {
           $meta = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            ); 
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
	
}


public function generateNextBillsMixmode($date,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$cluster_id=NULL) {
  
 		$date =  date($date);//date('2016-03-31');
  		$sum_amount=0;
	   	$packagesMapper = new Application_Model_PackagesMapper();
	   	$cashRegisterMapper = new Application_Model_CashRegisterMapper();
	    $consumersMapper = new Application_Model_ConsumersMapper();
	   	$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
	    
	    $meterMapper=new Application_Model_MeterReadingsMapper();
	    $consumers = $consumersMapper->getAllConsumersOfMixmode($site_id,$state_id,$role_sites_id,$cluster_id);
	    
	    if ($consumers) {
	    	
	   		foreach ($consumers as $consumer) {
	   			$package_id = $consumer["package_id"];
	   		//	if($consumer["consumer_id"]==1335){
	   			$is_postPaid =  $consumer["is_postpaid"];
	   			$package_cost =  $consumer["package_cost"];
	   			$extra_charges =  $consumer["extra_charges"];
	   			$consumers= $consumer["consumer_id"];
	   			$unit = $consumer["unit"];
	   			
	   			$ckeckMeter=$meterMapper->checkMeterReadingsByConsumerId($consumer["consumer_id"]);
		   		if($ckeckMeter){
		   				$zendDate = new Zend_Date($date);
		   				$day= intval($zendDate->toString("dd"));
		   				$month= $zendDate->toString("MM");
		   				$year= intval($zendDate->toString("yyyy"));
		   				
		   						$week=($day<=8)?2:(($day<=15)?3:4);
		   						
		   						$packageWeeklyCosts=$packageHistoryMapper->getWeeklyPackageHistoryByConsumerId($consumer["consumer_id"], $year, $month);
		   						
		   						$weekCost2=$packageWeeklyCosts[0]->__get("weekCost2");
		   						$weekCost3=$packageWeeklyCosts[0]->__get("weekCost3");
		   					    $weekCost4=$packageWeeklyCosts[0]->__get("weekCost4");
		   						
		   						$weekPackage2=$packageWeeklyCosts[0]->__get("weekPackage2");
		   						$weekPackage3=$packageWeeklyCosts[0]->__get("weekPackage3");
		   						$weekPackage4=$packageWeeklyCosts[0]->__get("weekPackage4");
		   						
		   						$weekPackage2Type=$packageWeeklyCosts[0]->__get("weekPackage2Type");
		   						$weekPackage3Type=$packageWeeklyCosts[0]->__get("weekPackage3Type");
		   						$weekPackage4Type=$packageWeeklyCosts[0]->__get("weekPackage4Type");
		   						
		   						$initialReading=$packageWeeklyCosts[0]->__get("initialReading");
		   						$week2Reading=$packageWeeklyCosts[0]->__get("week2Reading");
		   						$week3Reading=$packageWeeklyCosts[0]->__get("week3Reading");
		   						$week4Reading=$packageWeeklyCosts[0]->__get("week4Reading"); 
		   					  
		   						if($weekPackage4Type==2 && $weekPackage3Type==2 && $weekPackage2Type==2){
		   							if($week4Reading !=null)
		   							{
		   								$pack=$packagesMapper->getPackageById($weekPackage3);
		   								$unit=$pack->__get("unit");
		   								$charge=$pack->__get("extra_charges");
		   								if($week3Reading !=null)
		   									$readings=$week4Reading-$week3Reading;
		   								else if($week2Reading !=null)
		   									$readings=$week4Reading-$week2Reading;
		   								else
		   								$readings=$week4Reading-$initialReading;
		   								
		   								$week_unit=$unit/4;
		   							
		   								
		   								if($readings>$week_unit){
		   									$diff_meter=$readings-$week_unit;
		   									$total_amount=($weekCost4/4)+($diff_meter*$charge);
		   								}else{
		   									$total_amount=($weekCost4/4);
		   								}
		   							}
		   							else{
		   								$total_amount=($weekCost4/4);
		   							}
		   							
		   						}
		   						elseif($weekPackage3Type==2 && $weekPackage2Type==2 && $weekPackage4Type!=2){
		   							if($week3Reading !=null)
		   							{
		   								$pack=$packagesMapper->getPackageById($weekPackage3);
		   								$unit=$pack->__get("unit");
		   								$charge=$pack->__get("extra_charges");
		   								if($week2Reading !=null)
											$readings=$week3Reading-$week2Reading;
										else
											$readings=$week3Reading-$initialReading;
		   									$week_unit=$unit/4;
		   							
		   								
				   						if($readings>$week_unit){
						   					 $diff_meter=$readings-$week_unit;
						   					 $total_amount=(($weekCost3/4)*2)+($diff_meter*$charge);
						   				}else{
						   					$total_amount=($weekCost3/4)*2;
						   				}
		   							}
		   							else{
		   								$total_amount=($weekCost3/4)*2;
		   							}
		   							
		   						}
		   						elseif($weekPackage2Type==2 && $weekPackage3Type!=2 && $weekPackage4Type!=2){
		   							if($week2Reading !=null)
		   							{
		   								$pack=$packagesMapper->getPackageById($weekPackage2);
		   								$unit=$pack->__get("unit");
		   								$charge=$pack->__get("extra_charges");

		   								$readings=$week2Reading-$initialReading;
	   									$week_unit=$unit/4;
		   							
		   									
		   								if($readings>$week_unit){
		   									$diff_meter=$readings-$week_unit;
		   									$total_amount=($weekCost2/4)*3+($diff_meter*$charge);
		   								}else{
		   									$total_amount=($weekCost2/4)*3;
		   								}
		   							}
		   							else{
		   								$total_amount=($weekCost2/4)*3;
		   							}
		   							
		   						}else{
		   							$meter=floatval($meterMapper->getCurrentMeterReadingsByConsumerId($consumer["consumer_id"]));
		   							if($meter>$unit){
		   								$diff_meter=$meter-$unit;
		   								$total_amount=$package_cost+($diff_meter*$extra_charges);
		   							}else{
		   								$total_amount=$package_cost;
		   							}
		   						}
		   						
		 		   			}else{
		   			$total_amount=$package_cost;
		   		}
	   			$sum_amount=$sum_amount+$total_amount;	 	
	   			//echo "success";
	   				
	    		 // }
	   		}
	   	   }
	   	 return   $sum_amount;
  	 }

	
	
	}
?>