<?php
require('html2fpdf.php');
require('font/makefont/makefont.php');
ini_set('display_errors',0);
class Api_PdfController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}

	public function generatePdfReportAction()
	{
		//echo $this->baseUrl();
		$request=$this->getRequest();
		$data=$request->getParam("data");
		$page_size=$request->getParam("pageSize");
		date_default_timezone_set('Asia/Kolkata');
		$filename=$request->getParam("fileName").date('d M,Y hi A').'.pdf';//"manageReport.pdf";
		
		
		$pdf=new HTML2FPDF('P','mm',$page_size);
		$pdf->AddPage();
		
		$strContent=$data;
		$pdf->WriteHTML($strContent);
				
		$pdf->Output($filename);
		
		echo "http://taraurja.in/".$filename;
		//header("Location: http://52.74.99.106/$filename");
		//echo "PDF file is generated successfully!";
	}
}