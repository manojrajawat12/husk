<?php
class Api_IndividualDeviceController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		
	}
	
	public function addIndividualDeviceAction(){
	try {
	
			$request=$this->getRequest();
			$device_name=$request->getParam("device_name");
			$token=$request->getParam("token");
			
				$myFile = "global.json";
				$arr_data = array(); 
				
				$formdata = array(
						"Device_name" => $device_name,
						"token" => $token,
				);
			
			if(file_get_contents($myFile)==""){
				$current = file_get_contents($myFile);
				$current .= "[]";
				file_put_contents($myFile, $current);
			}
			$jsondata = file_get_contents($myFile);
			// converts json data into array
        	$arr_data = json_decode($jsondata, true); 
        	// Push user data to array
			array_push($arr_data,$formdata);
			//Convert updated array to JSON
			$jsondata = json_encode($arr_data, JSON_PRETTY_PRINT);
			//write json data into data.json file
	        if(file_put_contents($myFile, $jsondata)) {
	        	echo 'Data successfully saved';
		    }
		    else {
	        	echo "error";
      		}
		}
		catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
			$arr = array(
					"meta" => $meta
			);
		}
	}
 
}