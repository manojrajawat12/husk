<?php

class Api_CelamedController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname; 
    }
 
    public function addCelamedAction(){
      
         try {
         	$packagemapper=new Application_Model_PackagesMapper();  
         	$equipment=new Application_Model_EquipmentMapper();
         	$Intervention=new Application_Model_InterventionMapper();
         	
            $request=$this->getRequest();
            $consumer_id=$request->getParam("consumer_id"); 
            $site_id=$request->getParam("site_id");
            $intervention_type=$request->getParam("intervention_type");
            $equipment_cost_total=$request->getParam("equipment_cost_total");
            $equipment_cost_tara=$request->getParam("equipment_cost_tara");
            $equipment_type=$request->getParam("equipment_type");
            $equipment_summary=$request->getParam("equipment_summary");
            $Contact_date=$request->getParam("Contact_date");
            $payment_date=$request->getParam("payment_date");
            $completion_date=$request->getParam("completion_date");
            
            $wattage=$request->getParam("wattage");
            $size=$request->getParam("size");
            $connection_type=$request->getParam("connection_type");
            $description=$request->getParam("description");
           
	  
             $consumermapper=new Application_Model_CelamedMapper();
             $checkCon=$consumermapper->checkConsumerByConsumerId($consumer_id);
             if($checkCon){
             $consumer= new Application_Model_Celamed();
             $consumer->__set("consumer_id",$consumer_id);
             $consumer->__set("site_id",$site_id);
             $consumer->__set("intervention_type",$intervention_type);
             $consumer->__set("equipment_cost_total",$equipment_cost_total);
             $consumer->__set("equipment_cost_tara",$equipment_cost_tara);
             
             $consumer->__set("wattage",$wattage);
             $consumer->__set("size",$size);
             $consumer->__set("device_type", $connection_type);
             $consumer->__set("description",$description);
            
             
             $consumer->__set("equipment_type",$equipment_type);
             $consumer->__set("equipment_summary",$equipment_summary);
             
             $zendDate = new Zend_Date($Contact_date,"dd-MM-yyyy");
             $date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
              
             $consumer->__set("Contact_date",$date);
             
  	     if($payment_date=='undefined')
             {  
             	$date =NULL;
             }else{
           
             	$zendDate = new Zend_Date($payment_date,"dd-MM-yyyy");
             	$date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
             }
             $consumer->__set("payment_date",$date);
             if($completion_date=='undefined')
             {   
		$date = NULL;
             }	
	     else{   
             	$zendDate = new Zend_Date($completion_date,"dd-MM-yyyy");
             	$date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
             }
             $consumer->__set("completion_date",$date);
              
           
            
            $Celamed_id=$consumermapper->addNewCelamed($consumer);
            if($Celamed_id){
         	
            	$this->_logger->info("New Celamed ID ".$Celamed_id." has been created in Celamed by ". $this->_userName.".");
             
          
            $siteMapper=new Application_Model_SitesMapper();
            $site_name=$siteMapper->getSiteById($site_id);
             
            $consumerMappers=new Application_Model_ConsumersMapper();
            $consumer_data=$consumerMappers->getConsumerById($consumer_id);
            $zendDate = new Zend_Date($Contact_date,"dd-MM-yyyy");
            $Contact_date = $zendDate->toString("dd-MM-yyyy");
             
 	   if($payment_date==NULL || $payment_date=='undefined')
             {  
             	$payment_date =NULL;
             }else{

            $zendDate1 = new Zend_Date($payment_date,"dd-MM-yyyy");
            $payment_date = $zendDate1->toString("dd-MM-yyyy");
           } 
 	   if($completion_date==NULL || $completion_date=='undefined')
             {  
             	$completion_date =NULL;
             }else{

            $zendDate2 = new Zend_Date($completion_date,"dd-MM-yyyy");
            $completion_date = $zendDate2->toString("dd-MM-yyyy");
           }   
            $equipmentName=$equipment->getEquipmentById($consumer->__get("equipment_type"));
            $InterventionName=$Intervention->getInterventionById($consumer->__get("intervention_type"));
            
            $data = array(
	"id"=>$Celamed_id,
            'site_name' => $site_name->__get("site_name"), 
            "consumer_name"=>$consumer_data->__get("consumer_name"),
            "consumer_connection_id"=>$consumer_data->__get("consumer_connection_id"),
            'site_id' => $site_id,
            'consumer_id' => $consumer_id,    
            'intervention_type' => $intervention_type,
            'intervention_type_id' => $InterventionName->__get("type"),
            'equipment_cost_total' => intval($equipment_cost_total),
            'equipment_cost_tara' => intval($equipment_cost_tara),
            'equipment_type' => $equipment_type,
            'equipment_type_id' => $equipmentName->__get("type"),
            'equipment_summary' => $equipment_summary,
            'Contact_date' => $Contact_date,
            'payment_date' => $payment_date,
            'completion_date' => $completion_date,
            
           
			);
            
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
             }else{
             	 $meta = array(
                    "code" => 301,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
             	
             }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }

     public function getCelamedByIdAction(){
      
         try {
             $request=$this->getRequest();
             $id=$request->getParam("id");
            $consumermapper=new Application_Model_CelamedMapper();
         if($consumer=$consumermapper->getCelamedById($id)){
         	
           	  $siteMapper=new Application_Model_SitesMapper();
              $equipment=new Application_Model_EquipmentMapper();
              $Intervention=new Application_Model_InterventionMapper();   
              
              $equipmentName=$equipment->getEquipmentById($consumer->__get("equipment_type"));
              $InterventionName=$Intervention->getAllIntervention($consumer->__get("intervention_type"));
                        $site_name=$siteMapper->getSiteById($consumer->__get("site_id"));
                	
                        $date = new Zend_Date($consumer->__get("Contact_date"),"yyyy-MM-dd");
                        $Contact_date = $date->toString("dd-MM-yyyy");
                        
                        $dates = new Zend_Date($consumer->__get("payment_date"),"yyyy-MM-dd");
                        $payment_date = $dates->toString("dd-MM-yyyy");
                        
                        $date_s = new Zend_Date($consumer->__get("completion_date"),"yyyy-MM-dd");
                        $completion_date = $date_s->toString("dd-MM-yyyy");
                        
                        
                        
                    	$data = array(
                        'id' => $package_name->__get("id"),
                        'site_name' => $site_name->__get("site_name"),  
                        'consumer_id' => $consumer->__get("consumer_id"), 
                    	'site_id' => $consumer->__get("site_id"),
                        'intervention_type_id' => $InterventionName->__get("type"),
                    	'intervention_type' => $consumer->__get("intervention_type"),
                        'equipment_cost_total' => intval($consumer->__get("equipment_cost_total")),
                        'equipment_cost_tara' => intval($consumer->__get("equipment_cost_tara")),
                        'equipment_type_id' => $equipmentName->__get("type"),
                    	'equipment_type' => $consumer->__get("equipment_type"),
                        'equipment_summary' => $consumer->__get("equipment_summary"),
                        'Contact_date' => $Contact_date,
                        'payment_date' => $payment_date,
                        'completion_date' => $completion_date,
          				);
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function deleteCelamedByIdAction(){
      
         try {
             $request=$this->getRequest();
             $id=$request->getParam("id");
             $consumermapper=new Application_Model_CelamedMapper();
         if($consumer=$consumermapper->deleteCelamedsById($id)){
         	$this->_logger->info("Celamed Id ".$id." has been deleted from Celameds by ". $this->_userName.".");
         	
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
    public function getAllCelamedAction()
    {
        try 
        {
                $request=$this->getRequest();
                $pagination = $request->getParam('pagination');
                if($pagination==NULL){
                	$pagination='true';
                }
                $page = $request->getParam("page",1);
                $total_pages=0;
                $current_page=0;
                $total_consumers=0;
                $new_consumer=0;
                $invalid_consumer=0;
	            $auth=new My_Auth('user');
	            $user=$auth->getIdentity()->user_role;
	            $state_id=$auth->getIdentity()->state_id;
	            
	            $roleSession = new Zend_Session_Namespace('roles');
	            $role_sites_id=$roleSession->site_id;
	            
                $consumermapper=new Application_Model_CelamedMapper();
                if($consumers=$consumermapper->getAllCelameds()){
                 if($pagination=='true'){
                    $total_consumers = count($consumers);
					
                   
                    $paginator=Zend_Paginator::factory($consumers);
                    $paginator->setItemCountPerPage(50);
                    $paginator->setCurrentPageNumber($page);
                    $consumers=$paginator;
                    $total_pages=$paginator->count();
                    if($page>$total_pages)
                    {
                        throw new Exception("No more pages",555);
                    }
                    $current_page=$paginator->getCurrentPageNumber(); 
                	}
                	$equipment=new Application_Model_EquipmentMapper();
                	$Intervention=new Application_Model_InterventionMapper();
                	$consumerName=new Application_Model_ConsumersMapper();
                	
                    foreach ($consumers as $consumer) {
                        $siteMapper=new Application_Model_SitesMapper();

                        $consumer_name=$consumerName->getConsumerById($consumer->__get("consumer_id"));
                        $equipmentName=$equipment->getEquipmentById($consumer->__get("equipment_type"));
                        $InterventionName=$Intervention->getInterventionById($consumer->__get("intervention_type"));
                        $site_name=$siteMapper->getSiteById($consumer->__get("site_id"));
                	
                        $date = new Zend_Date($consumer->__get("Contact_date"),"yyyy-MM-dd HH:mm:ss");
                        $Contact_date = $date->toString("dd-MM-yyyy");
                        
if($consumer->__get("payment_date")==NULL)
             {  
             	$payment_date =NULL;
             }else{

                        $dates = new Zend_Date($consumer->__get("payment_date"),"yyyy-MM-dd HH:mm:ss");
                        $payment_date = $dates->toString("dd-MM-yyyy");
                    } 
if($consumer->__get("completion_date")==NULL)
             {  
             	$completion_date =NULL;
             }else{   
                        $date_s = new Zend_Date($consumer->__get("completion_date"),"yyyy-MM-dd HH:mm:ss");
                        $completion_date = $date_s->toString("dd-MM-yyyy");
                 }       
                
                 
                    	$data = array(
                        'id' => $consumer->__get("id"),
                        'site_name' => $site_name->__get("site_name"),  
                        'consumer_id' => $consumer->__get("consumer_id"), 
                    	"consumer_name"=>$consumer_name->__get("consumer_name"),
                    	"consumer_connection_id"=>$consumer_name->__get("consumer_connection_id"), 
                    	'site_id' => $consumer->__get("site_id"),
                    	'intervention_type_id' => $InterventionName->__get("type"),
                        'intervention_type' => $consumer->__get("intervention_type"),
                        'equipment_cost_total' => intval($consumer->__get("equipment_cost_total")),
                        'equipment_cost_tara' => intval($consumer->__get("equipment_cost_tara")),
                        'equipment_type' => $consumer->__get("equipment_type"),
                    	'equipment_type_id' => $equipmentName->__get("name"),
                        'equipment_summary' => $consumer->__get("equipment_summary"),
                        'Contact_date' => $Contact_date,
                        'payment_date' =>$payment_date,
                        'completion_date' => $completion_date,
                    	'type_of_me' => $consumer_name->__get("type_of_me"),
                    	'notes' => $consumer_name->__get("notes"),
                    	
                    				
                    			 
                        
                    );
                    $consumer_arr[]=$data;
                }
				$total_consumers= $total_consumers-($new_consumer +$invalid_consumer);
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                    "total_page" => $total_pages,
                    "current_page" =>$current_page,
                    "total_consumers" => $total_consumers,
                	"new_consumer" => $new_consumer,
                	"invalid_consumer" => $invalid_consumer
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

         
     public function updateCelamedByIdAction(){
      
         try {
			$packagemapper=new Application_Model_PackagesMapper(); 
            $request=$this->getRequest();
            $id=$request->getParam("id"); 
            $consumer_id=$request->getParam("consumer_id"); 
            $site_id=$request->getParam("site_id");
            $intervention_type=$request->getParam("intervention_type");
            $equipment_cost_total=$request->getParam("equipment_cost_total");
            $equipment_cost_tara=$request->getParam("equipment_cost_tara");
            $equipment_type=$request->getParam("equipment_type");
            $equipment_summary=$request->getParam("equipment_summary");
            $Contact_date=$request->getParam("Contact_date");
            $payment_date=$request->getParam("payment_date");
            $completion_date=$request->getParam("completion_date");
            $wattage=$request->getParam("wattage");
            $size=$request->getParam("size");
            
            $type_of_me =$request->getParam("type_of_me");
            $notes= $request->getParam("notes");
            $zendDateContact_date = new Zend_Date($Contact_date,"dd-MM-yyyy");
            $Contact_date = $zendDateContact_date->toString("yyyy-MM-dd HH:mm:ss");
            
           
          
            if($payment_date=='null'||$payment_date=="")
             {
             	$payment_date=NULL;
             }else{
 	    $zendDatepayment_date= new Zend_Date($payment_date,"dd-MM-yyyy");
            $payment_date = $zendDatepayment_date->toString("yyyy-MM-dd HH:mm:ss");
	}
             if($completion_date=='null'||$completion_date=="")
             {
             	$completion_date=NULL;
             }else{
    
            $zendDatecompletion_date = new Zend_Date($completion_date,"dd-MM-yyyy");
            $completion_date = $zendDatecompletion_date->toString("yyyy-MM-dd HH:mm:ss");
     	  
}
             $consumermapper=new Application_Model_CelamedMapper();
             $consumer= new Application_Model_Celamed();
            
             $consumer->__set("consumer_id",$consumer_id);
             $consumer->__set("id",$id);
             $consumer->__set("site_id",$site_id);
             $consumer->__set("intervention_type",$intervention_type);
             $consumer->__set("equipment_cost_total",$equipment_cost_total);
             $consumer->__set("equipment_cost_tara",$equipment_cost_tara);
             $consumer->__set("equipment_type",$equipment_type);
             $consumer->__set("equipment_summary",$equipment_summary);
             $consumer->__set("Contact_date",$Contact_date);
             $consumer->__set("payment_date",$payment_date);
             $consumer->__set("completion_date",$completion_date);
             $consumer->__set("wattage",$wattage);
             $consumer->__set("size",$size);
       
             if($consumermapper->updateCelamed($consumer)){
             	$consumers_mapper=new Application_Model_ConsumersMapper();
             	$consumersData=$consumers_mapper->getConsumerById($consumer_id);
             	$consumersData->__set("notes",$notes);
             	$consumersData->__set("type_of_me",$type_of_me);
             	$consumers_mapper->updateConsumer($consumersData);
              
              	 $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                 );
                 $arr = array(
                    "meta" => $meta,
                 );
             } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while updating"
                );
                $arr = array(
                    "meta" => $meta
                );
            } 
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         
         public function getCelamedBySiteIdAction()
         {
         	try
         	{
         		$request=$this->getRequest();
         		$page = $request->getParam("page",1);
         		$site_id = $request->getParam("site_id");
         		$state_id = $request->getParam("state_id");
         		$total_pages=0;
         		$current_page=0;
         		$roleSession = new Zend_Session_Namespace('roles');
	            $role_sites_id=$roleSession->site_id;
         		$consumermapper=new Application_Model_CelamedMapper();
         		if($state_id=='ALL' && $site_id=="ALL")
         		{
         			$consumers = $consumermapper->getAllCelameds();
         		}elseif($state_id!='ALL' && $site_id=="ALL"){
         			$consumers=$consumermapper->getCelamedByStateId($state_id,$role_sites_id);
         		}
         		else
         		{
         			$consumers=$consumermapper->getCelamedBySiteId("site_id", $site_id);
         		}
         		if($consumers)
         		{
         			$total_consumers = count($consumers);
         			$new_consumer=0;
         			$invalid_consumer=0;
         			 
         			$paginator=Zend_Paginator::factory($consumers);
         			$paginator->setItemCountPerPage(100);
         			$paginator->setCurrentPageNumber($page);
         			$consumers=$paginator;
         			$total_pages=$paginator->count();
         			if($page>$total_pages)
         			{
         				throw new Exception("No more pages",555);
         			}
         			$current_page=$paginator->getCurrentPageNumber();
         			$equipment=new Application_Model_EquipmentMapper();
         			$Intervention=new Application_Model_InterventionMapper();
         			$consumerName=new Application_Model_ConsumersMapper();
         			    foreach ($consumers as $consumer) {
                        $siteMapper=new Application_Model_SitesMapper();

                        $consumer_name=$consumerName->getConsumerById($consumer->__get("consumer_id"));
                        $equipmentName=$equipment->getEquipmentById($consumer->__get("equipment_type"));
                        $InterventionName=$Intervention->getInterventionById($consumer->__get("intervention_type"));
                        $site_name=$siteMapper->getSiteById($consumer->__get("site_id"));
                	
                        $date = new Zend_Date($consumer->__get("Contact_date"),"yyyy-MM-dd");
                        $Contact_date = $date->toString("dd-MM-yyyy");
                        if($consumer->__get("payment_date")==NULL)
             			{  
             				$payment_date =NULL;
             			}else{
                       	 	$dates = new Zend_Date($consumer->__get("payment_date"),"yyyy-MM-dd");
                        	$payment_date = $dates->toString("dd-MM-yyyy");
                        }
 						if($consumer->__get("completion_date")==NULL)
             			{  
             				$completion_date =NULL;
             			}else{
                        	$date_s = new Zend_Date($consumer->__get("completion_date"),"yyyy-MM-dd");
                        	$completion_date = $date_s->toString("dd-MM-yyyy");
                        }
                    	$data = array(
                        'id' => $consumer->__get("id"),
                        'site_name' => $site_name->__get("site_name"),  
                        'consumer_id' => $consumer->__get("consumer_id"), 
                    	"consumer_name"=>$consumer_name->__get("consumer_name"),
                    	"consumer_connection_id"=>$consumer_name->__get("consumer_connection_id"),
                    	'site_id' => $consumer->__get("site_id"),
                    	'intervention_type_id' => $InterventionName->__get("type"),
                        'intervention_type' => $consumer->__get("intervention_type"),
                        'equipment_cost_total' => intval($consumer->__get("equipment_cost_total")),
                        'equipment_cost_tara' => intval($consumer->__get("equipment_cost_tara")),
                        'equipment_type' => $consumer->__get("equipment_type"),
                    	'equipment_type_id' => $equipmentName->__get("type"),
                        'equipment_summary' => $consumer->__get("equipment_summary"),
                        'Contact_date' =>  $Contact_date,
                        'payment_date' =>  $payment_date ,
                        'completion_date' =>$completion_date,
                    	'type_of_me' => $consumer_name->__get("type_of_me"),
                    	'notes' => $consumer_name->__get("notes"),
                    			);
                    			$consumer_arr[]=$data;
         			    }
         			$total_consumers= $total_consumers-($new_consumer +$invalid_consumer);
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS",
         					"total_page" => $total_pages,
         					"current_page" =>$current_page,
         					"total_consumers" => $total_consumers,
         					"new_consumer" => $new_consumer,
         					"invalid_consumer" => $invalid_consumer
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $consumer_arr,
         			);
         		} else {
         			$meta = array(
         					"code" => 404,
         					"message" => "No consumers found"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}
         	catch (Exception $e)
         	{
         		$meta = array(
         				"code" => 501,
         				"message" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }      
         
         public function getAllConsumerByCelamedAction(){
         
         	try {
         		$request=$this->getRequest();
         		 
         		$consumermapper=new Application_Model_CelamedMapper();
         		if($consumers=$consumermapper->getAllConsumersByCelamed()){

		         	foreach ($consumers as $consumer){
		         			$data = array(
		                       
		                        
		                        'consumer_id' => $consumer->__get("consumer_id"),    
		                        'package_id' => $consumer->__get("package_id"),
		                        'site_id' => $consumer->__get("site_id"),
		                        'consumer_code' => $consumer->__get("consumer_code"),
		                        'consumer_name' => $consumer->__get("consumer_name"),
		                        'consumer_father_name' => $consumer->__get("consumer_father_name"),
		                        'consumer_status' => $consumer->__get("consumer_status"),
		                      	
		                        'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
		                        'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
		                        'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
		                        'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
		                        'suspension_date' => $consumer->__get("suspension_date"),
		                    	'enterprise_type' => $consumer->__get("enterprise_type"),
		                    	'type_of_me' => $consumer->__get("type_of_me"),
		                    	'notes' => $consumer->__get("notes"),
		         		 
		                    );
		         	$consumer_arr[]=$data;
		         	}
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $consumer_arr
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while adding"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
}