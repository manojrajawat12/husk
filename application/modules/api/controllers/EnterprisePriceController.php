<?php

class Api_EnterprisePriceController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
    }
    
        public function addEnterprisePriceAction(){
      
         try {
         	//$duplicate=0;
             $request=$this->getRequest();
        	 $unit_from=$request->getParam("unit_from");
             $unit_to=$request->getParam("unit_to");
             $enterprise_price=$request->getParam("enterprise_price");
             $enterprise_scheme=$request->getParam("enterprise_scheme");
            
             $SchemesMapper=new Application_Model_SchemesMapper();
             $enterpriseMapper=new Application_Model_EnterprisePriceMapper();
            /*  $enterprises=$enterpriseMapper->getEnterprisePriceBySchemeId($enterprise_scheme);
             foreach ($enterprises as $enter){
			 	$data=array(
            		"enterprise_price_id" => $enter->__get("enterprise_price_id"),
            		"unit_from" => $enter->__get("unit_from"),
            		"unit_to" => $enter->__get("unit_to"),
            		"enterprise_price" =>$enter->__get("enterprise_price"),
           			 "enterprise_scheme" => $enter->__get("enterprise_scheme"),
			 			);
			 			
			 	if(($unit_from>=$data['unit_from']&&$unit_from<= $data['unit_to']) ||($unit_to>=$data['unit_from']&&$unit_to<= $data['unit_to']))
			 	{
			 		$duplicate=1;
			 	}
			}
         if($duplicate==0){ */
         	
             $enterprise= new Application_Model_EnterprisePrice();
             $enterprise->__set("unit_from",$unit_from);
             $enterprise->__set("unit_to",$unit_to);
             $enterprise->__set("enterprise_price",$enterprise_price);
             $enterprise->__set("enterprise_scheme",$enterprise_scheme);
              
              
           if($enterprise=$enterpriseMapper->addNewEnterprisePrice($enterprise)){
           	$this->_logger->info("New Enterprice price ID ".$enterprise." has been created in Enterprice price by ". $this->_userName.".");
           	 
           	$enterprise_scheme_name = $SchemesMapper->getSchemeById($enterprise_scheme);
            $data=array(     
            "enterprise_price_id" => $enterprise,
            "unit_from" => $unit_from,
            "unit_to" => $unit_to,
            "enterprise_price" =>$enterprise_price,
            "enterprise_scheme" => $enterprise_scheme,
            "enterprise_scheme_name" => $enterprise_scheme_name->__get("scheme"),
            		
              );
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
           /*  } else {
            	$meta = array(
            			"code" => 301,
            			"message" => "duplicate value"
            	);
            	$arr = array(
            			"meta" => $meta
            	);
            } */
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
        }

        
        
      public function getEnterprisePriceByIdAction(){
      
         try {
             $request=$this->getRequest();
             $Enterprise_price_id=$request->getParam("id");
             $SchemesMapper=new Application_Model_SchemesMapper();
             $enterpriseMapper=new Application_Model_EnterprisePriceMapper();
         if($enterprise=$enterpriseMapper->getEnterprisePriceById($Enterprise_price_id)){
             
         	$enterprise_scheme_name = $SchemesMapper->getSchemeById($enterprise->__get("enterprise_scheme"));
            $data=array(     
            "enterprise_price_id" => $enterprise->__get("enterprise_price_id"),
            "unit_from" => $enterprise->__get("unit_from"),
            "unit_to" => $enterprise->__get("unit_to"),
            "enterprise_price" =>$enterprise->__get("enterprise_price"),
            "enterprise_scheme" => $enterprise->__get("enterprise_scheme"),
            "enterprise_scheme_name" => $enterprise_scheme_name->__get("scheme"),
                 
              );
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while getting data"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function deleteEnterprisePriceByIdAction(){
      
         try {
             $request=$this->getRequest();
             $scheme_id=$request->getParam("id");
             $scheme_only=$request->getParam("scheme_only");
             $enterpriseMapper=new Application_Model_EnterprisePriceMapper();
             $schemeMapper=new Application_Model_SchemesMapper();
             
             if($scheme_only=="true"){
          		$enterprise=$enterpriseMapper->deleteEnterprisePriceBySchemeId($scheme_id) ;
             }
         	
            $schemeMapper->deleteSchemeById($scheme_id);
         	
         	$this->_logger->info("Enterprice Price Id ".$scheme_id." has been deleted from EnterpricePrice price by ". $this->_userName.".");
         	 
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
             
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function deleteEnterprisePriceByEnterpriceIdAction(){
         
         	try {
         		$request=$this->getRequest();
         		$enterprice_id=$request->getParam("id");
         		$enterpriseMapper=new Application_Model_EnterprisePriceMapper();
         		
         		if($enterprise=$enterpriseMapper->deleteEnterprisePriceById($enterprice_id)){
         			 
         			$this->_logger->info("Enterprice Price Id ".$enterprice_id." has been deleted from EnterpricePrice price by ". $this->_userName.".");
         
         			 
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while deleting"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
         public function getAllEnterprisePriceAction(){
      
         try {
         	 $SchemesMapper=new Application_Model_SchemesMapper();
             $enterpriseMapper=new Application_Model_EnterprisePriceMapper();
             $enterprises=$enterpriseMapper->getAllEnterprisePrice();
          
         	if(count($enterprises) >0){
             foreach ($enterprises as $enterprise) {
             	
             	$enterprise_scheme_name = $SchemesMapper->getSchemeById($enterprise->__get("enterprise_scheme"));
             	if(!$enterprise_scheme_name)
             	{
             		$enterprise_scheme_name = new Application_Model_Schemes();
             	}
	            $data=array(     
		            "enterprise_price_id" => $enterprise->__get("enterprise_price_id"),
	            	 "unit_from" => $enterprise->__get("unit_from"),
		            "unit_to" => $enterprise->__get("unit_to"),
		            "enterprise_price" =>$enterprise->__get("enterprise_price"),
	            	"enterprise_scheme" => $enterprise->__get("enterprise_scheme"),
	            	"enterprise_scheme_name" => $enterprise_scheme_name->__get("scheme"),
	              );
	                 
	                 $enterprise_arr[]=$data;
	         }
	         $meta = array(
	         		"code" => 200,
	         		"message" => "SUCCESS"
	         );
	         $arr = array(
	         		"meta" => $meta,
	         		"data" => $enterprise_arr,
	         );
            
            }
            else{
            	$meta = array(
            			"code" => 200,
            			"message" => "SUCCESS"
            	);
            	$arr = array(
            			"meta" => $meta,
            			"data" =>array(),
            	);
            }
            
            
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
      public function updateEnterprisePriceByIdAction(){
      
         try {
             $request=$this->getRequest();
             $enterprise_price_id=$request->getParam("enterprise_price_id");
             $unit_from=$request->getParam("unit_from");
             $unit_to=$request->getParam("unit_to");
             $enterprise_price=$request->getParam("enterprise_price");
             $enterprise_scheme=$request->getParam("enterprise_scheme");

             
             $enterpriseMapper=new Application_Model_EnterprisePriceMapper();
             $enterprise= new Application_Model_EnterprisePrice();
             $lastSchemePrice=$enterpriseMapper->getEnterprisePriceById($enterprise_price_id);
             $enterprise->__set("enterprise_price_id",$enterprise_price_id);
             $enterprise->__set("unit_from",$unit_from);
             $enterprise->__set("unit_to",$unit_to);
             $enterprise->__set("enterprise_price",$enterprise_price);
             $enterprise->__set("enterprise_scheme",$enterprise_scheme);
            
           if($enterprisePrice=$enterpriseMapper->updateEnterprisePrice($enterprise)){
           	
           	$lastSchemePrice_arr=array(
           			"unit_from" => $lastSchemePrice->__get("unit_from"),
           			"unit_to" => $lastSchemePrice->__get("unit_to"),
           			"enterprise_price" =>$lastSchemePrice->__get("enterprise_price"),
           			"enterprise_scheme" => $lastSchemePrice->__get("enterprise_scheme"),
           	);
           	$newSchemePrice_arr=array(
           			"unit_from" => $enterprise->__get("unit_from"),
           			"unit_to" => $enterprise->__get("unit_to"),
           			"enterprise_price" =>$enterprise->__get("enterprise_price"),
           			"enterprise_scheme" => $enterprise->__get("enterprise_scheme"),
           	);
           	$lastScheme_diff=array_diff($lastSchemePrice_arr,$newSchemePrice_arr);
           	$newScheme_diff=array_diff($newSchemePrice_arr,$lastSchemePrice_arr);
           	 
           	$change_data="";
           	foreach ($lastScheme_diff as $key => $value)
           	{
           		$change_data.=$key." ".$lastScheme_diff[$key]." change to ".$newScheme_diff[$key]." ";
           	}
           	$this->_logger->info("Enterprice Price Id ".$enterprise_price_id." has been updated where ".$change_data." by ". $this->_userName.".");
           	
            	$meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while updating"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
          } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function updateSchemeByIdAction(){
         
         	try {
         		$request=$this->getRequest();
         		$scheme_id=$request->getParam("id");
         		$scheme_name=$request->getParam("scheme_name");
         		 
         		$enterpriseMapper=new Application_Model_SchemesMapper();
         		$schemes= new Application_Model_Schemes();
         		$lastSchemePrice=$enterpriseMapper->getSchemeById($scheme_id);
         		$schemes->__set("scheme_id",$scheme_id);
         		$schemes->__set("scheme",$scheme_name);
         		 
         
         		if($enterprisePrice=$enterpriseMapper->updateScheme($schemes)){
         
         			$lastSchemePrice_arr=array(
         					"scheme_id" => $lastSchemePrice->__get("scheme_id"),
         					"scheme" => $lastSchemePrice->__get("scheme"),
         				
         			);
         			$newSchemePrice_arr=array(
         					"scheme_id" => $schemes->__get("scheme_id"),
         					"scheme" => $schemes->__get("scheme"),
         					
         			);
         			$lastScheme_diff=array_diff($lastSchemePrice_arr,$newSchemePrice_arr);
         			$newScheme_diff=array_diff($newSchemePrice_arr,$lastSchemePrice_arr);
         			 
         			$change_data="";
         			foreach ($lastScheme_diff as $key => $value)
         			{
         				$change_data.=$key." ".$lastScheme_diff[$key]." change to ".$newScheme_diff[$key]." ";
         			}
         			$this->_logger->info("Scheme Id ".$scheme_id." has been updated where ".$change_data." by ". $this->_userName.".");
         
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while updating"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	} catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
         
         public function getEnterprisePriceBySchemeIdAction(){
         
         	try {
         	 $duplicate=0;
             $request=$this->getRequest();
        	 $unit_from=$request->getParam("unit_from");
             $unit_to=$request->getParam("unit_to");
             $enterprise_price=$request->getParam("enterprise_price");
             $enterprise_scheme=$request->getParam("enterprise_scheme");
             $enterprise_price_id=$request->getParam('enterprise_price_id');
             $enterpriseMapper=new Application_Model_EnterprisePriceMapper();
             $enterprises=$enterpriseMapper->getEnterprisePriceBySchemeId($enterprise_scheme);
             
         		if(count($enterprises) >0){
         		 foreach ($enterprises as $enter){
         		 	
			 	$data=array(
            		"enterprise_price_id" => $enter->__get("enterprise_price_id"),
            		"unit_from" => $enter->__get("unit_from"),
            		"unit_to" => $enter->__get("unit_to"),
            		"enterprise_price" =>$enter->__get("enterprise_price"),
           			 "enterprise_scheme" => $enter->__get("enterprise_scheme"),
			 			);
			 	if($enterprise_price_id!=$data["enterprise_price_id"])
			 	{
			 		if(($unit_from>=$data['unit_from']&&$unit_from<= $data['unit_to'])||($unit_to>=$data['unit_from']&&$unit_to<= $data['unit_to']))
			 		{
			 			$duplicate=1;
			 		}
				}
         	 }
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $duplicate,
         					
         			);
         
         		}else{
         			$meta = array(
         					"code" => 200,
         					"message" => "No Scheme Price Found"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" =>array(),
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
         
         
         public function getSchemePriceBySchemeIdAction(){
         	 
         	try {
         		$duplicate=0;
         		$request=$this->getRequest();
         	
         		$enterprise_price_id=$request->getParam('scheme_id');
         		$enterpriseMapper=new Application_Model_EnterprisePriceMapper();
         		$enterprises=$enterpriseMapper->getEnterprisePriceBySchemeId($enterprise_price_id);
         		// print_r($enterprises);
         		if(count($enterprises) >0){
         			foreach ($enterprises as $enter){
         				 
         				$data=array(
         						"enterprise_price_id" => $enter->__get("enterprise_price_id"),
         						"unit_from" => $enter->__get("unit_from"),
         						"unit_to" => $enter->__get("unit_to"),
         						"enterprise_price" =>$enter->__get("enterprise_price"),
         						"enterprise_scheme" => $enter->__get("enterprise_scheme"),
         				);
         				$schemePrice[]=$data;
         			}
         		
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $schemePrice,
         					
         			);
         			 
         		}else{
         			$meta = array(
         					"code" => 200,
         					"message" => "No Scheme Price Found"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" =>array(),
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         		 
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         
         }
 }
 
 