<?php

class Api_LogsController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
    }

    public function getAllLogsAction() {
        try {
            $logmapper = new Application_Model_LogsMapper();

            if ($logs = $logmapper->getAllLogs()) {

                foreach ($logs as $log) {
                    $data = array(
                        'user_id' => $log->__get("user_id"),
                        'username' => $log->__get("username"),
                        'priority_level' => $log->__get("priority_level"),
                        'priority_name' => $log->__get("priority_name"),
                        'timestamp' => $log->__get("timestamp"),
                        'message' => $log->__get("message"),
                    );

                    $log_arr[] = $data;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $log_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

}
