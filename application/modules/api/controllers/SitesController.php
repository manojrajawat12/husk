<?php

class Api_SitesController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
       // header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
		$this->_userId=$auth->getIdentity()->user_id;
    }

    public function addSiteAction() {

        try {
            $request = $this->getRequest();
            $cluster_id = $request->getParam("id");
            $site_name = $request->getParam("name");
            $num_of_poles = $request->getParam("num_of_poles");
            $site_code = $request->getParam("site_code");
            $state_id = $request->getParam("state_id");
            $x = $request->getParam("x");
            $y = $request->getParam("y");
            $size_of_plant = $request->getParam("size_of_plant");
            $Solar_panel_size = $request->getParam("Solar_panel_size");
            $Solar_panel_no = $request->getParam("Solar_panel_no");
            $Charges_controller_size = $request->getParam("Charges_controller_size");
            $Charges_controller_no = $request->getParam("Charges_controller_no");
            $Inverter_size = $request->getParam("Inverter_size");
            $Inverter_no = $request->getParam("Inverter_no");
            $Battery_size = $request->getParam("Battery_size");
            $Battery_no = $request->getParam("Battery_no");
			$otp_act_id = $request->getParam("otp_act_id");
            $otp_mtr_id = $request->getParam("otp_mtr_id");
            
            $sitemapper = new Application_Model_SitesMapper();
            $clustermapper=new Application_Model_ClustersMapper();
            $stateMapper=new Application_Model_StatesMapper();
            $site = new Application_Model_Sites();
            $site->__set("site_name", $site_name);
            $site->__set("cluster_id", $cluster_id);
            $site->__set("num_of_poles", $num_of_poles);
            $site->__set("site_code", $site_code);
            $site->__set("state_id", $state_id);
            $site->__set("x", $x);
            $site->__set("y", $y);
            $site->__set("size_of_plant", $size_of_plant);
            $site->__set("Solar_panel_size", $Solar_panel_size);
            $site->__set("Solar_panel_no", $Solar_panel_no);
            $site->__set("Charges_controller_size", $Charges_controller_size);
            $site->__set("Charges_controller_no", $Charges_controller_no);
            $site->__set("Inverter_size", $Inverter_size); 
            $site->__set("Inverter_no", $Inverter_no);
            $site->__set("Battery_size", $Battery_size);
            $site->__set("Battery_no", $Battery_no);
             $site->__set("otp_act_id", $otp_act_id);
            $site->__set("otp_mtr_id", $otp_mtr_id);
			
            if ($site = $sitemapper->addNewSite($site,$this->_userId)) {
            	
             $this->_logger->info("New Site ID ".$site." has been created in Sites by ". $this->_userName.".");
               $cluster_name=$clustermapper->getClusterById($cluster_id);  
               $state_name=$stateMapper->getStateById($state_id);
                $data = array(
                    "site_id" => $site,
                    "cluster_id" => $cluster_id,
                    "site_name" => $site_name,
                    "num_of_poles" => $num_of_poles,
                    "site_code" => $site_code,
                    "cluster_name" =>$cluster_name->__get("cluster_name"),
                	"state_id" => $state_id,
                	"state_name" => $state_name->__get("state_name"),
                	"site_status" => "Enable",
                    "site_order" => $site,
                    "x" => $x,
                    "y" => $y,
                	"size_of_plant" => $size_of_plant,
                	"Solar_panel_size" => $Solar_panel_size,
                	"Solar_panel_no" => $Solar_panel_no,
                	"Charges_controller_size" => $Charges_controller_size,
                	"Charges_controller_no" => $Charges_controller_no,
                	"Inverter_size" => $Inverter_size,
                	"Inverter_no" => $Inverter_no,
                	"Battery_size" => $Battery_size,
                	"Battery_no" => $Battery_no,
                	"otp_act_id" => $otp_act_id,
                	"otp_mtr_id" => $otp_mtr_id,
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getSiteByIdAction() {

        try {
            $request = $this->getRequest();
            $site_id = $request->getParam("id");
            $sitemapper = new Application_Model_SitesMapper();
            if ($site = $sitemapper->getSiteById($site_id)) {


                $data = array(
                    "site_id" => $site->__get("site_id"),
                    "cluster_id" => $site->__get("cluster_id"),
                    "site_name" => $site->__get("site_name"),
                    'num_of_poles' => $site->__get("num_of_poles"),    
                    'site_code' => $site->__get("site_code"), 
                	'state_id' => $site->__get("state_id"),
                	'site_status' => $site->__get("site_status"),
                    'site_order' => $site->__get("site_order"),
                    'x' => $site->__get("x"),
                    'y' => $site->__get("y"),
                	'size_of_plant' => $site->__get("size_of_plant"),
    				'Solar_panel_size' => $site->__get("Solar_panel_size"),
    				'Solar_panel_no' => intval($site->__get("Solar_panel_no")),
    				'Charges_controller_size' => $site->__get("Charges_controller_size"),
					'Charges_controller_no' => intval($site->__get("Charges_controller_no")),
    				'Inverter_size' => $site->__get("Inverter_size"),
    				'Inverter_no' => intval($site->__get("Inverter_no")),
  					'Battery_size' => $site->__get("Battery_size"),
    				'Battery_no' => intval($site->__get("Battery_no")),
					'otp_act_id' => $site->__get("otp_act_id"),
                	'otp_mtr_id' => $site->__get("otp_mtr_id"),
                		
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function deleteSiteByIdAction() {

        try {
            $request = $this->getRequest();
            $site_id = $request->getParam("id");
            $sitemapper = new Application_Model_SitesMapper();
            if ($site = $sitemapper->deleteSiteById($site_id,$this->_userId)) {

            	$this->_logger->info("Site Id ".$site_id." has been deleted from sites by ". $this->_userName.".");
            	

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllSitesAction() {

        try {
//             $request=$this->getRequest();
//             $site_id=$request->getParam("id");
            $sitemapper = new Application_Model_SitesMapper();
            $clustermapper= new Application_Model_ClustersMapper();
            $consumersMapper = new Application_Model_ConsumersMapper();
            $stateMapper=new Application_Model_StatesMapper();
            $roleSiteMapper=new Application_Model_RoleSiteMapper();
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
            //$state_id=$auth->getIdentity()->state_id;
            
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            $state_id=$roleSession->state_id;
            
            
            if ($sites = $sitemapper->getAllSites($user,$state_id,$role_sites_id)) {
            
                foreach ($sites as $site) {
                    $active_consumers = $consumersMapper->getMonthlyActiveConsumers($site->__get("site_id"), "active");
                    $banned_consumers = $consumersMapper->getMonthlyActiveConsumers($site->__get("site_id"), "banned");
                    $active_consumers_count = 0;
                    $banned_consumers_count = 0;
                    if($active_consumers)
                    {
                        $active_consumers_count = count($active_consumers);
                    }
                    if($banned_consumers)
                    {
                        $banned_consumers_count = count($banned_consumers);
                    }
                $cluster_name=$clustermapper->getClusterById($site->__get("cluster_id"));    
                $state_name=$stateMapper->getStateById($site->__get("state_id"));  
                if(!$cluster_name)   
                {
                    $cluster_name = new Application_Model_Clusters();
                }
                    $data = array(
                        "site_id" => $site->__get("site_id"),
                        "site_name" => $site->__get("site_name"),
                        "cluster_id" => $site->__get("cluster_id"),
                        "cluster_name" =>$cluster_name->__get("cluster_name"),
                        'num_of_poles' => $site->__get("num_of_poles"),    
                        'site_code' => $site->__get("site_code"), 
                    	'state_id' => $site->__get("state_id"),
                    	'state_name' => $state_name->__get("state_name"),
                        "active_consumers_count" => $active_consumers_count,
                        "banned_consumers_count" => $banned_consumers_count,
                    	'site_status' => $site->__get("site_status"),
                        'site_order' => $site->__get("site_order"),
                        'x' => $site->__get("x"),
                        'y' => $site->__get("y"),
                    	'size_of_plant' => $site->__get("size_of_plant"),
    					'Solar_panel_size' => $site->__get("Solar_panel_size"),
    					'Solar_panel_no' => intval($site->__get("Solar_panel_no")),
						'Solar_panel_kw' => floatval($site->__get("Solar_panel_kw")),
    					'Charges_controller_size' => $site->__get("Charges_controller_size"),
    					'Charges_controller_no' => intval($site->__get("Charges_controller_no")),
    					'Inverter_size' => $site->__get("Inverter_size"),
    					'Inverter_no' => intval($site->__get("Inverter_no")),
   						'Battery_size' => $site->__get("Battery_size"),
   						'Battery_no' => intval($site->__get("Battery_no")),
						'Battery_no_v' => floatval($site->__get("Battery_no_v")),
						'otp_act_id' => $site->__get("otp_act_id"),
						'otp_mtr_id' => $site->__get("otp_mtr_id"),
						
						'dg_capacity' => floatval($site->__get("dg_capacity")),
						'dg_no' => intval($site->__get("dg_no")),
						'Battery_no' => intval($site->__get("Battery_no")),
						'bio_capacity' => floatval($site->__get("bio_capacity")),
						'bio_no' => intval($site->__get("bio_no")),
						'radiation' => floatval($site->__get("radiation")),
						'site_target' => floatval($site->__get("site_target")),
						'plant_duration' => floatval($site->__get("plant_duration")),
						'plant_target' => floatval($site->__get("plant_target")),
						'array_no' => intval($site->__get("array_no")),
						'array_capacity' => floatval($site->__get("array_capacity")),
						'day_factor' => intval($site->__get("day_factor")),
						'night_factor' => intval($site->__get("night_factor")),
						
						
                    		
                    );

                    $site_arr[] = $data;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $site_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updateSiteByIdAction() {

        try {
            $request = $this->getRequest();
            $site_id = $request->getParam("id");
            $site_name = $request->getParam("name");
            $site_code = $request->getParam("site_code");
            $num_of_poles = $request->getParam("num_of_poles");
            $cluster_id = $request->getParam("cluster_id");
            $state_id = $request->getParam("state_id");
            $site_status = $request->getParam("site_status");
            $site_order = $request->getParam("site_order");
            $x = $request->getParam("x");
            $y = $request->getParam("y");
            $size_of_plant = $request->getParam("size_of_plant");
            $Solar_panel_size = $request->getParam("Solar_panel_size");
            $Solar_panel_no = $request->getParam("Solar_panel_no");
				$Solar_panel_kw = $request->getParam("Solar_panel_kw");
            $Charges_controller_size = $request->getParam("Charges_controller_size");
            $Charges_controller_no = $request->getParam("Charges_controller_no");
            $Inverter_size = $request->getParam("Inverter_size");
            $Inverter_no = $request->getParam("Inverter_no");
            $Battery_size = $request->getParam("Battery_size");
            $Battery_no = $request->getParam("Battery_no");
				$Battery_no_v = $request->getParam("Battery_no_v");
			$otp_act_id = $request->getParam("otp_act_id");
            $otp_mtr_id = $request->getParam("otp_mtr_id");
			
				$dg_capacity = $request->getParam("dg_capacity");
				$dg_no = $request->getParam("dg_no");
				$bio_capacity = $request->getParam("bio_capacity");
				$bio_no = $request->getParam("bio_no");
				$radiation = $request->getParam("radiation");
				$site_target = $request->getParam("site_target");
				$plant_duration = $request->getParam("plant_duration");
				$plant_target = $request->getParam("plant_target");
				$array_no = $request->getParam("array_no");
				$array_capacity = $request->getParam("array_capacity");
				$day_factor = $request->getParam("day_factor");
				$night_factor = $request->getParam("night_factor");
			
            $sitemapper = new Application_Model_SitesMapper();
            $site = new Application_Model_Sites();
			 $lastSites=$sitemapper->getSiteById($site_id);
            $site->__set("site_id", $site_id);
            $site->__set("site_name", $site_name);
            $site->__set("site_code", $site_code);
            $site->__set("num_of_poles", $num_of_poles);
            $site->__set("cluster_id", $cluster_id);
            $site->__set("state_id", $state_id);
            $site->__set("site_status", $site_status);
            $site->__set("site_order", $site_order);
            $site->__set("x", $x);
            $site->__set("y", $y);
            $site->__set("size_of_plant", $size_of_plant);
            $site->__set("Solar_panel_size", $Solar_panel_size);
            $site->__set("Solar_panel_no", $Solar_panel_no);
				 $site->__set("Solar_panel_kw", $Solar_panel_kw);
            $site->__set("Charges_controller_size", $Charges_controller_size);
            $site->__set("Charges_controller_no", $Charges_controller_no);
            $site->__set("Inverter_size", $Inverter_size);
            $site->__set("Inverter_no", $Inverter_no);
            $site->__set("Battery_size", $Battery_size);
            $site->__set("Battery_no", $Battery_no);
				 $site->__set("Battery_no_v", $Battery_no_v);
			$site->__set("otp_act_id", $otp_act_id);
            $site->__set("otp_mtr_id", $otp_mtr_id);
				
				$site->__set("dg_capacity", $dg_capacity);
				$site->__set("dg_no", $dg_no);
				$site->__set("bio_capacity", $bio_capacity);
				$site->__set("bio_no", $bio_no);
				$site->__set("radiation", $radiation);
				$site->__set("site_target", $site_target); 
				$site->__set("plant_duration", $plant_duration);
				$site->__set("plant_target", $plant_target);
				$site->__set("array_no", $array_no); 
				$site->__set("array_capacity", $array_capacity);
				$site->__set("day_factor", $day_factor);
				$site->__set("night_factor", $night_factor);
				 
            if ($sites = $sitemapper->updateSite($site)) {
            	if($site_status!=$lastSites->__get("site_status")){
            		$siteDetails=$sitemapper->addSiteStatusDetail($site_id,$site_status,$this->_userId);
            	}
            	$lastSite_arr = array(
            			"site_id" => $lastSites->__get("site_id"),
            			"site_name" => $lastSites->__get("site_name"),
            			"cluster_id" => $lastSites->__get("cluster_id"),
            			'num_of_poles' => $lastSites->__get("num_of_poles"),
            			'site_code' => $lastSites->__get("site_code"),
            			'state_id' => $lastSites->__get("state_id"),
            			'site_status' => $lastSites->__get("site_status"),
                        'site_order' => $lastSites->__get("site_order"),
                        'x' => $lastSites->__get("x"),
                        'y' => $lastSites->__get("y"),
            			'size_of_plant' => $site->__get("size_of_plant"),
    					'Solar_panel_size' => $site->__get("Solar_panel_size"),
    					'Solar_panel_no' => intval($site->__get("Solar_panel_no")),
    					'Charges_controller_size' => $site->__get("Charges_controller_size"),
    					'Charges_controller_no' => intval($site->__get("Charges_controller_no")),
    					'Inverter_size' => $site->__get("Inverter_size"),
    					'Inverter_no' => intval($site->__get("Inverter_no")),
   						'Battery_size' => $site->__get("Battery_size"),
   						'Battery_no' => intval($site->__get("Battery_no")),
						'otp_act_id' => $site->__get("otp_act_id"),
            			'otp_mtr_id' => $site->__get("otp_mtr_id"),
          

            	);
            	$newCluster_arr = array(
            			"site_id" => $site->__get("site_id"),
            			"site_name" => $site->__get("site_name"),
            			"cluster_id" => $site->__get("cluster_id"),
            		    'num_of_poles' => $site->__get("num_of_poles"),
            			'site_code' => $site->__get("site_code"),
            			'state_id' => $site->__get("state_id"),
            			'site_status' => $site->__get("site_status"),
                        'site_order' => $site->__get("site_order"),
                        'x' => $site->__get("x"),
                        'y' => $site->__get("y"),
            			'size_of_plant' => $site->__get("size_of_plant"),
    					'Solar_panel_size' => $site->__get("Solar_panel_size"),
    					'Solar_panel_no' => intval($site->__get("Solar_panel_no")),
    					'Charges_controller_size' => $site->__get("Charges_controller_size"),
    					'Charges_controller_no' => intval($site->__get("Charges_controller_no")),
    					'Inverter_size' => $site->__get("Inverter_size"),
    					'Inverter_no' => intval($site->__get("Inverter_no")),
    					'Battery_size' => $site->__get("Battery_size"),
    					'Battery_no' => intval($site->__get("Battery_no")),
						'otp_act_id' => $site->__get("otp_act_id"),
            			'otp_mtr_id' => $site->__get("otp_mtr_id"),

            	);
            	$lastSite_diff=array_diff($lastSite_arr,$newCluster_arr);
            	$newSite_diff=array_diff($newCluster_arr,$lastSite_arr);
            	 
            	$change_data="";
            	foreach ($lastSite_diff as $key => $value)
            	{
            		$change_data.=$key." ".$lastSite_diff[$key]." change to ".$newSite_diff[$key]." ";
            	}
            	$this->_logger->info("Site Id ".$site_id." has been where ".$change_data." updated by ". $this->_userName.".");


            	$meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getSiteByClusterIdAction() {

        try {
            $request = $this->getRequest();
            $cluster_id = $request->getParam("id");
            $sitemapper = new Application_Model_SitesMapper();
            $site = new Application_Model_Sites();

            if ($sites = $sitemapper->getSiteByClusterId($cluster_id)) {

                foreach ($sites as $site) {

                    $data = array(
                        "cluster_id" => $site->__get("cluster_id"),
                    	"site_id" => $site->__get("site_id"),
                        "site_name" => $site->__get("site_name"),
                        'num_of_poles' => $site->__get("num_of_poles"),    
                        'site_code' => $site->__get("site_code"),
                    	'state_id' => $site->__get("state_id"),
                    	'site_status' => $site->__get("site_status"),
                    	'x' => $site->__get("x"),
                    	'y' => $site->__get("y"),
                    );

                    $site_arr[] = $data;
                }


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $site_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllSitesByCollectionAgentAction() {

        try {
            $request = $this->getRequest();
            $c_email = $request->getParam("email");
            $sitemapper = new Application_Model_SitesMapper();
            if ($sites = $sitemapper->getSiteByClusterId($c_email)) {

                foreach ($sites as $site) {

                    $data = array(
                        "site_id" => $site->__get("site_id"),
                        "site_name" => $site->__get("site_name"),
                        'num_of_poles' => $site->__get("num_of_poles"),    
                        'site_code' => $site->__get("site_code"),    
                    );

                    $site_arr[] = $data;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $site_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function getAllSitesForSitePageAction() {
    
    	try {
    		
    		$sitemapper = new Application_Model_SitesMapper();
    		$clustermapper= new Application_Model_ClustersMapper();
    		$consumersMapper = new Application_Model_ConsumersMapper();
    		$stateMapper=new Application_Model_StatesMapper();
    		$roleSiteMapper=new Application_Model_RoleSiteMapper();
    	
    
    		if ($sites = $sitemapper->getAllSitesForSitePage()) {
    
    			foreach ($sites as $site) {
    				 
    				$cluster_name=$clustermapper->getClusterById($site->__get("cluster_id"));
    				$state_name=$stateMapper->getStateById($site->__get("state_id"));
    				
    				
    				$data = array(
						"site_id" => $site->__get("site_id"),
                        "site_name" => $site->__get("site_name"),
                        "cluster_id" => $site->__get("cluster_id"),
                        "cluster_name" =>$cluster_name->__get("cluster_name"),
                        'num_of_poles' => $site->__get("num_of_poles"),    
                        'site_code' => $site->__get("site_code"), 
                    	'state_id' => $site->__get("state_id"),
                    	'state_name' => $state_name->__get("state_name"),
                        
                    	'site_status' => $site->__get("site_status"),
                        'site_order' => $site->__get("site_order"),
                        'x' => $site->__get("x"),
                        'y' => $site->__get("y"),
                    	'size_of_plant' => $site->__get("size_of_plant"),
    					'Solar_panel_size' => $site->__get("Solar_panel_size"),
    					'Solar_panel_no' => intval($site->__get("Solar_panel_no")),
						'Solar_panel_kw' => floatval($site->__get("Solar_panel_kw")),
    					'Charges_controller_size' => $site->__get("Charges_controller_size"),
    					'Charges_controller_no' => intval($site->__get("Charges_controller_no")),
    					'Inverter_size' => $site->__get("Inverter_size"),
    					'Inverter_no' => intval($site->__get("Inverter_no")),
   						'Battery_size' => $site->__get("Battery_size"),
   						'Battery_no' => intval($site->__get("Battery_no")),
						'Battery_no_v' => floatval($site->__get("Battery_no_v")),
						'act_charges' => $site->__get("otp_act_id"),
    					'mtr_charges' => $site->__get("otp_mtr_id"), 
						
						'dg_capacity' => floatval($site->__get("dg_capacity")),
						'dg_no' => intval($site->__get("dg_no")),
						'bio_capacity' => floatval($site->__get("bio_capacity")),
						'bio_no' => intval($site->__get("bio_no")),
						'radiation' => floatval($site->__get("radiation")),
						'site_target' => floatval($site->__get("site_target")),
						'plant_duration' => floatval($site->__get("plant_duration")),
						'plant_target' => floatval($site->__get("plant_target")),
						'array_no' => intval($site->__get("array_no")),
						'array_capacity' => floatval($site->__get("array_capacity")),
						'day_factor' => intval($site->__get("day_factor")),
						'night_factor' => intval($site->__get("night_factor")),
    				); 
    
    				$site_arr[] = $data;
    			}
    
    
    
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $site_arr,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
   public function getAllChargesAction() {
    
    	try {
     
    		$SitesMapper=new Application_Model_SitesMapper();
    		
    		if ($sites = $SitesMapper->getChargesbyType()) {
    
    			foreach ($sites as $site) {
      				$data = array(
    						"id" => $site["id"],
    						"name" => $site["name"],
    						"charges" => $site["charges"],
    						"otp_type" => $site["otp_type"],
    				);
    
    				$site_arr[] = $data;
    			}
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $site_arr,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
	  public function getAllSitesApproveAction() {
    
    	try {
    		$request=$this->getRequest();
    		
    		$sitemapper = new Application_Model_SitesMapper();
    		$clustermapper= new Application_Model_ClustersMapper();
    		$consumersMapper = new Application_Model_ConsumersMapper();
    		$stateMapper=new Application_Model_StatesMapper();
    		$roleSiteMapper=new Application_Model_RoleSiteMapper();
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		//$state_id=$auth->getIdentity()->state_id;
    
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    		$state_id=$roleSession->state_id;
    
    		$site_arr=array();
    		if ($sites = $sitemapper->getAllNewSites('New',$role_sites_id)) {
    			foreach ($sites as $site) {
    				
    				$cluster_name=$clustermapper->getClusterById($site->__get("cluster_id"));
    				$state_name=$stateMapper->getStateById($site->__get("state_id"));
    				if(!$cluster_name)
    				{
    					$cluster_name = new Application_Model_Clusters();
    				}
    				$Act_charges=$sitemapper->getChargesbyID($site->__get("otp_act_id"));
    				$Mtr_charges=$sitemapper->getChargesbyID($site->__get("otp_mtr_id"));
    				$data = array(
    						"site_id" => $site->__get("site_id"),
    						"site_name" => $site->__get("site_name"),
    						"cluster_id" => $site->__get("cluster_id"),
    						"cluster_name" =>$cluster_name->__get("cluster_name"),
    						'num_of_poles' => $site->__get("num_of_poles"),
    						'site_code' => $site->__get("site_code"),
    						'state_id' => $site->__get("state_id"),
    						'state_name' => $state_name->__get("state_name"),
    						'site_status' => $site->__get("site_status"),
    						'site_order' => $site->__get("site_order"),
    						'x' => $site->__get("x"),
    						'y' => $site->__get("y"),
    						'size_of_plant' => $site->__get("size_of_plant"),
    						'Solar_panel_size' => $site->__get("Solar_panel_size"),
    						'Solar_panel_no' => intval($site->__get("Solar_panel_no")),
    						'Charges_controller_size' => $site->__get("Charges_controller_size"),
    						'Charges_controller_no' => intval($site->__get("Charges_controller_no")),
    						'Inverter_size' => $site->__get("Inverter_size"),
    						'Inverter_no' => intval($site->__get("Inverter_no")),
    						'Battery_size' => $site->__get("Battery_size"),
    						'Battery_no' => intval($site->__get("Battery_no")),
    						'otp_act_id' => $site->__get("otp_act_id"),
    						'otp_mtr_id' => $site->__get("otp_mtr_id"),
    						'act_charges' => $Act_charges[0]["charges"],
    						'mtr_charges' => $Mtr_charges[0]["charges"],
    						'timestamp' => $site->__get("site_timestamp"),
    
    				);
    				 
    				$site_arr[] = $data;
    			}
     
    
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $site_arr,
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
    public function updateSiteStatusByIdAction(){
    	 
    	try {
    		$request=$this->getRequest();
    		$site_id=$request->getParam("site_id");
    		$status=$request->getParam("status");
    		$remark=$request->getParam("remark");
    
    		$zendDate = new Zend_Date();
    		$zendDate->setTimezone("Asia/Calcutta");
    		$consumer_act_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    
    		$Sitesmapper=new Application_Model_SitesMapper();
    
    		if($site=$Sitesmapper->updateFeedersById($site_id,$status,$remark, $this->_userId)){
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					 
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while updating"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
}
