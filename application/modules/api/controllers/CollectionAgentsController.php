<?php

class Api_CollectionAgentsController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
       // header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
    }

    public function addCollectionAgentAction() {
        try {

            $request = $this->getRequest();
            $agent_mobile = $request->getParam("mobile");
            $agent_fname = $request->getParam("fname");
            $agent_lname = $request->getParam("lname");
			$username = $request->getParam("username");
            $agent_status = $request->getParam("status");
            $agent_balance = $request->getParam("balance");
            $agent_email = $request->getParam("email");
            $site_id = $request->getParam("site_id");
			$password = $request->getParam("password");
			$permission = $request->getParam("permission");
			$assigned_site = $request->getParam("assigned_site");
			$line_manager_no = $request->getParam("line_manager_no");
			
            $agentMapper = new Application_Model_CollectionAgentsMapper();
            $agents = new Application_Model_CollectionAgents();
            $agents->__set("agent_mobile", $agent_mobile);
            $agents->__set("agent_fname", $agent_fname);
            $agents->__set("agent_lname", $agent_lname);
			$agents->__set("username", $username);
            $agents->__set("agent_status", $agent_status);
            $agents->__set("agent_balance", $agent_balance);
            $agents->__set("agent_email", $agent_email);
            $agents->__set("site_id", $site_id);
			$agents->__set("password", sha1(strtoupper($password)));
			$agents->__set("permission", $permission); 
			$agents->__set("assigned_site", $assigned_site); 
			$agents->__set("line_manager_no", $line_manager_no); 
			
          if ($agent_id = $agentMapper->addNewCollectionAgent($agents)) {
            	$this->_logger->info("New Collection Agent ID ".$agent_id." has been created in Collection Agents by ". $this->_userName.".");
                $siteMapper = new Application_Model_SitesMapper();
                $site_name = $siteMapper->getSiteById($site_id);
                $data = array(
                    "collection_agent_id" => $agent_id,
                    "agent_fname" => $agent_fname,
                    "agent_lname" => $agent_lname,
                    "agent_mobile" => $agent_mobile,
                    "agent_status" => $agent_status,
                    "agent_balance" => $agent_balance,
                    "agent_email" => $agent_email,
					"username" => $username,
                    "site_name" => $site_name->__get("site_name"),
                    "site_id" => $site_id,
                    "permission" => $permission,
					"assigned_site"=>$assigned_site,
					"line_manager_no"=>$line_manager_no
                );

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data,
                );
            }
        } catch (Exception $e) {

            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }

        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getCollectionAgentByIdAction() {
        try {
            $request = $this->getRequest();
            $agent_id = $request->getParam("id");
            $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
            if ($collectionAgents = $collectionAgentsMapper->getCollectionAgentById($agent_id)) {
                if (!$collectionAgents) {
                    throw new Exception("No agents found");
                }


                $data = array(
                    "collection_agent_id" => $collectionAgents->__get("collection_agent_id"),
                    "agent_fname" => $collectionAgents->__get("agent_fname"),
                    "agent_lname" => $collectionAgents->__get("agent_lname"),
                    "agent_status" => $collectionAgents->__get("agent_status"),
                   "username" => $collectionAgents->__get("username"),
                    "agent_mobile" => $collectionAgents->__get("agent_mobile"),
                    "agent_email" => $collectionAgents->__get("agent_email"),
                    "agent_balance" => $collectionAgents->__get("agent_balance"),
					"assigned_site" => $collectionAgents->__get("assigned_site")
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data,
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function deleteAgentByIdAction() {
        try {
            $request = $this->getRequest();
            $agent_id = $request->getParam("id");
            $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
            if ($collectionAgents = $collectionAgentsMapper->deleteCollectionAgentById($agent_id)) {
            	$this->_logger->info("Collection Agent Id ".$agent_id." has been deleted from Collection Agents by ". $this->_userName.".");
            	 
                if (!$collectionAgents) {
                    throw new Exception("No agents found");
                }
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllCollectionAgentsAction() {
        try {
            $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
            $collectionAgents = $collectionAgentsMapper->getAllCollectionAgents();
            if (!$collectionAgents) {
                throw new Exception("No agents found");
            }
			
            foreach ($collectionAgents as $agent) {
                $siteMapper = new Application_Model_SitesMapper();
                $site_name = $siteMapper->getSiteById($agent->__get("site_id"));
                $data = array(
                    "collection_agent_id" => $agent->__get("collection_agent_id"),
                    "agent_fname" => $agent->__get("agent_fname"),
                    "agent_lname" => $agent->__get("agent_lname"),
                    "agent_mobile" => $agent->__get("agent_mobile"),
                     "username" => $agent->__get("username"),
                    "agent_status" => $agent->__get("agent_status"),
                    "agent_balance" => $agent->__get("agent_balance"),
                    "agent_email" => $agent->__get("agent_email"),
                    "site_id" => $agent->__get("site_id"),
                    "site_name" => $site_name->__get("site_name"),
                    "permission" => $agent->__get("permission"),
					"assigned_site" => $agent->__get("assigned_site"),
					"line_manager_no" => intval($agent->__get("line_manager_no"))
                );
                $agents_arr[] = $data;
            }
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $arr = array(
                "meta" => $meta,
                "data" => $agents_arr,
            );
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updateAgentByIdAction() {
        try {
            $request = $this->getRequest();
            $agent_id = $request->getParam("id");
            $agent_mobile = $request->getParam("mobile");
            $agent_fname = $request->getParam("fname");
            $agent_lname = $request->getParam("lname");
			$username = $request->getParam("username");
            $agent_status = $request->getParam("status");
            $agent_balance = $request->getParam("balance");
            $agent_email = $request->getParam("email");
            $site_id = $request->getParam("site_id");
			$password = $request->getParam("password");
			$permission = $request->getParam("permission");
			$assigned_site = $request->getParam("assigned_site");
			$line_manager_no = $request->getParam("line_manager_no");
			
            $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
            $agents = new Application_Model_CollectionAgents();
            $lastAgents = $collectionAgentsMapper->getCollectionAgentById($agent_id);
            $agents->__set("collection_agent_id", $agent_id);
            $agents->__set("agent_mobile", $agent_mobile);
            $agents->__set("agent_fname", $agent_fname);
            $agents->__set("agent_lname", $agent_lname);
			$agents->__set("username", $username);
            $agents->__set("agent_status", $agent_status);
            $agents->__set("agent_balance", $agent_balance);
            $agents->__set("agent_email", $agent_email);
            $agents->__set("site_id", $site_id);
			$agents->__set("assigned_site", $assigned_site);
			$agents->__set("line_manager_no", $line_manager_no);
			
			if($password!='undefined'){
				$agents->__set("password", sha1(strtoupper($password)));
			}else{
				$agents->__set("password", NULL);
			}
			
			$agents->__set("permission", $permission); 
			
            if ($collectionAgents = $collectionAgentsMapper->updateCollectionAgent($agents)) {
            	
            	$lastAgent_data = array(
            			"collection_agent_id" => $lastAgents->__get("collection_agent_id"),
            			"agent_fname" => $lastAgents->__get("agent_fname"),
            			"agent_lname" => $lastAgents->__get("agent_lname"),
            			"agent_mobile" => $lastAgents->__get("agent_mobile"),
            			"agent_status" => $lastAgents->__get("agent_status"),
            			"agent_balance" => $lastAgents->__get("agent_balance"),
            			"agent_email" => $lastAgents->__get("agent_email"),
            			"site_id" => $lastAgents->__get("site_id"),
            			"permission" => $lastAgents->__get("permission"),
						"assigned_site" => $lastAgents->__get("assigned_site"),
						"line_manager_no" => $lastAgents->__get("line_manager_no")
            	);
            	$newAgent_data = array(
            			"collection_agent_id" => $agents->__get("collection_agent_id"),
            			"agent_fname" => $agents->__get("agent_fname"),
            			"agent_lname" => $agents->__get("agent_lname"),
            			"agent_mobile" => $agents->__get("agent_mobile"),
            			"agent_status" => $agents->__get("agent_status"),
            			"agent_balance" => $agents->__get("agent_balance"),
            			"agent_email" => $agents->__get("agent_email"),
            			"site_id" => $agents->__get("site_id"),
            			"permission" => $agents->__get("permission"),
						"assigned_site" => $agents->__get("assigned_site"),
						"line_manager_no" => $agents->__get("line_manager_no")
            	);
            	$lastAgent_diff=array_diff($lastAgent_data,$newAgent_data);
            	$newAgent_diff=array_diff($newAgent_data,$lastAgent_data);
            	 
            	$change_data="";
            	foreach ($lastAgent_diff as $key => $value)
            	{
            		$change_data.=$key." ".$lastAgent_diff[$key]." change to ".$newAgent_diff[$key]." ";
            	}

            	$this->_logger->info("Collection Agents Id ".$agent_id." has been updated where ".$change_data." by ". $this->_userName.".");
            	 
                if (!$collectionAgents) {
                    throw new Exception("No agents found");
                }
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function getCollectionAgentByPhoneAction() {
    	try {
    		 $agent_mobile='7533038564';
    		$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    		$collectionAgents = $collectionAgentsMapper->getCollectionAgentByPhone($agent_mobile);
    		if (!$collectionAgents) {
    			throw new Exception("No agents found");
    		}
    			$agent=$collectionAgents;
    		
    			$siteMapper = new Application_Model_SitesMapper();
    			$site_name = $siteMapper->getSiteById($agent->__get("site_id"));
    			$data = array(
    					"collection_agent_id" => $agent->__get("collection_agent_id"),
    					"agent_fname" => $agent->__get("agent_fname"),
    					"agent_lname" => $agent->__get("agent_lname"),
    					"username" => $collectionAgents->__get("username"),
    					"agent_mobile" => $agent->__get("agent_mobile"),
    					"agent_status" => $agent->__get("agent_status"),
    					"agent_balance" => $agent->__get("agent_balance"),
    					"agent_email" => $agent->__get("agent_email"),
    					"site_id" => $agent->__get("site_id"),
    					"site_name" => $site_name->__get("site_name"),
						"assigned_site" => $agent->__get("assigned_site"),
    			);
    			$agents_arr[] = $data;

    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$arr = array(
    				"meta" => $meta,
    				"data" => $agents_arr,
    		);
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }

    public function loginAgent() {
        try {
            $request = $this->getRequest();
            $agent_email = $request->getParam("username");
            $password = $request->getParam("password");
            
            $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
			$input=array(username=>$agent_email,
						password=>$password);
            if ($collectionAgents = $collectionAgentsMapper->loginAgent($password,$agent_email)) {
            	
            	if(count($collectionAgents) >0)
				{
					
					$permission=$collectionAgents->__get("permission");
	            	$data=array(
					 "agent_id" => $collectionAgents->__get("collection_agent_id"),
					 "permission"=>$permission
					 );
					$commandResult = array(
					"success"=>0,
					"message" =>"Incorrect Username or Password",
					"data"=>$data,
               		 );
				}
				else {
					$data=array();
					$commandResult = array(
					"success"=>0,
					"message" =>"Login Successfully",
					"data"=>$data,
              		);
				}

                $arr = array(
                    "input" => $input,
                    "commandResult" => $commandResult,
                );
            }
        } catch (Exception $e) {
           
			$commandResult = array(
					"success"=>1,
					"message" =>$e->getMessage(),
					"data"=>array(),
                );

                $arr = array(
                    "input" => $input,
                    "commandResult" => $commandResult,
                );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	 public function getAllCollectionAgentBySiteAction() {
    	try {
    		$request = $this->getRequest();
    		$site_id = $request->getParam("site_id");
    		$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    		$collectionAgents = $collectionAgentsMapper->getAgentBySiteId($site_id);
    		if (!$collectionAgents) {
    			throw new Exception("No agents found");
    		}
    			
    		foreach ($collectionAgents as $agent) {
    			$data = array(
    					"collection_agent_id" => $agent["collection_agent_id"],
    					"agent_fname" => $agent["agent_fname"],
    					"agent_lname" => $agent["agent_lname"],
    					"agent_mobile" => $agent["agent_mobile"],
    					"username" => $agent["username"],
    					"agent_status" => $agent["agent_status"],
    					"agent_balance" => $agent["agent_balance"],
    					"agent_email" => $agent["agent_email"],
    					"site_id" => $agent["site_id"],
    					"permission" => $agent["permission"],
    					"assigned_site" => $agent["assigned_site"],
						"line_manager_no" => intval($agent["assigned_site"]),
    			);
    			$agents_arr[] = $data;
    		}
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$arr = array(
    				"meta" => $meta,
    				"data" => $agents_arr,
    		);
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
    }
