<?php

class Api_AddDeviceController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
    }
    
    public function addDeviceNameAction(){
      
         try {
             $request=$this->getRequest();
             $poles=$request->getParam("poles");
             $site_id=$request->getParam("site_id");
			 $device_sim=$request->getParam("device_sim");
			 $type=$request->getParam("type");
			 $times=$request->getParam("time");
             $devicemapper=new Application_Model_DeviceMapper();
             $addDevices=new Application_Model_AddDeviceMapper();
             $device= new Application_Model_Device();
             $siteMapper=new Application_Model_SitesMapper();
             $sites=$siteMapper->getSiteById($site_id);
             
             $pole=sprintf("%03d",$poles);
             $time=sprintf("%04d",$times);
             
             $device_name = $sites->__get("site_code").$pole.$type;
             $device->__set("device_name",$device_name);
             $device->__set("site_id",$site_id);
			 $device->__set("device_sim",$device_sim);
			 $device->__set("time",$time);
            
			 $deviceCheck=$addDevices->getDataByDeviceName($device_name);
			 if($deviceCheck==false){
	         	if($device=$devicemapper->addDevice($device)){
	         	
	         	$this->_logger->info("New Device ID ".$device." has been created in Device by ". $this->_userName.".");
	         	 
	         	 $data=array(     
	                "device_id" => $device,
	                "device_name" => $device_name,
	         	 	"site_id" => $site_id,
					"device_sim" => $device_sim
	            	 
	              );
	          
	             
	            $meta = array(
	                    "code" => 200,
	                    "message" => "SUCCESS"
	                );
	                $arr = array(
	                    "meta" => $meta,
	                     "data" => $data
	                );
	            } else {
	                $meta = array(
	                    "code" => 401,
	                    "message" => "Error while adding"
	                );
	                $arr = array(
	                    "meta" => $meta
	                );
	            }
			 }else{
			 	$meta = array(
			 			"code" => 401,
			 			"message" => "Error while adding"
			 	);
			 	$arr = array(
			 			"meta" => $meta
			 	);
			 }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
	public function addDeviceAction(){
         
         	try {
         		$request=$this->getRequest();
         		$device_name=$request->getParam("device_name");
         		$consumer_id=$request->getParam("consumer_id");
         		$node=$request->getParam("node");
         		$wattage=$request->getParam("wattage");
         		if($wattage=='undefined'){
         			$wattage='0000';
         		}
         		$wattage=sprintf("%04d",$wattage);
         		$devicemapper=new Application_Model_AddDeviceMapper();
         		$device= new Application_Model_AddDevice();
         		 
         		$device->__set("device_name",$device_name);
         		$device->__set("consumer_id",$consumer_id);
         		$device->__set("node",$node);
         		$device->__set("wattage",$wattage);
         
         		if($device=$devicemapper->addDevice($device)){
         
         			$this->_logger->info("New Device ID ".$device." has been created in Device by ". $this->_userName.".");
         			 
         			$data=array(
         					"device_id" => $device,
         					"device_name" => $device_name,
         					"consumer_id" => $consumer_id,
         					"node" => $node,
         					"wattage" => $wattage,
         
         			);
         
         			 
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $data
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while adding"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
          
    public function getDeviceByIdAction(){
      
         try {
             $request=$this->getRequest();
             $device_id=$request->getParam("device_id");
             $devicemapper=new Application_Model_AddDeviceMapper();
             $consumers=new Application_Model_ConsumersMapper();
             $device_arr=array();
         if($devices=$devicemapper->getAllAddDevice($device_id)){
            foreach($devices as $device){
            	$consumer=$consumers->getConsumerById($device->__get("consumer_id"));
	            $data=array(     
	                
	                "device_name" => $device->__get("device_name"),
	                "device_id" =>   $device->__get("device_id"),
	            	"consumer_id" => $device->__get("consumer_id"),
	            	"sequence" =>	 $device->__get("sequence"),
	            	"site_id"=>		 $consumer->__get("site_id"),
	            	"wattage" =>	 intval($device->__get("wattage")),
	            		
	            );
	            $device_arr[]=$data;
            }
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $device_arr
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
    public function deleteDeviceByIdAction(){
      
         try {
             $request=$this->getRequest();
             $device_id=$request->getParam("device_id");
            $deviceMapper=new Application_Model_AddDeviceMapper();
            $delete=$deviceMapper->deleteDeviceById($device_id);
            
            $devices=new Application_Model_DeviceMapper();
         	$delDevice=$devices->deleteDeleteById($device_id);
         	
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
    public function getAllDeviceAction(){
         
         	try {
         		$request=$this->getRequest();
         		$site_id=$request->getParam("site_id");
				$page = $request->getParam("page");
				$order = $request->getParam("order");
				
				if($page!=0){
					$pages=$page*6;
				}else{
					$pages=null;
				}
         		if($site_id=='All'){
         			$site_id=NULL;
         		}
         		$devicemapper=new Application_Model_AddDeviceMapper();
         		$demapper=new Application_Model_DeviceMapper();
         		$consumerMapper=new Application_Model_ConsumersMapper();
         		$electricity=new Application_Model_ElectricityInfoMapper();
         		$cluster_arr=array();
         		$roleSession = new Zend_Session_Namespace('roles');
	            $role_sites_id=$roleSession->site_id; 
         		$elec=$electricity->getAllElectricitystatus($role_sites_id);
         		$IERMS_data=array();
         		foreach ($elec as $elect_data){
         			$consumer=explode(",", $elect_data["consumer_id"]);
         			$volt=explode(",", $elect_data["volt"]);
         			$current=array();
         			for($i=0;$i<count($consumer);$i++){
         				
         				$currents=array(
         						"consumer_id"=>$consumer[$i],
         						"volt"=>$volt[$i]
         				);
         				$current[]=$currents;
         				
         			}
         			
         			$IE_data=array(
         					"node"=>$elect_data["node"],
         					"version"=>$elect_data["version"],
         					"iccid"=>$elect_data["iccid"],
         					"site_id"=>$elect_data["site_id"],
         					"votage"=>$elect_data["votage"],
         					"info_date"=>$elect_data["info_date"],
         					"device_name"=>$elect_data["device_name"],
         					"current"=>$current,
         					
         					
         			);
         			$IERMS_data[]=$IE_data;
         		}
         		 
         		if($Devices=$demapper->getAllDevice($site_id,$pages,$order)){
         			 
         
         			foreach ($Devices as $Device) {
         				$device_arr=array();
         				$elec=array();
         				foreach ($IERMS_data as $IERMS_datas){
         					if($Device["device_name"]==$IERMS_datas["device_name"]){
         						$elec[]=$IERMS_datas;
         					}
         					
         				}
         				 
         				 
         				if(count($elec)>0) {
         					foreach($elec as $elecs){
         						 
         						foreach($elecs["current"] as $elect){
         							  
         						 	 $consumers=$consumerMapper->getConsumerById($elect["consumer_id"]);
										if($consumers){
												$consumer_connection_id=$consumers->__get("consumer_connection_id");
												$consumer_name=$consumers->__get("consumer_name");
										}else{
												$consumer_connection_id=null;
												$consumer_name=null;
										}
         							$string=$elect["consumer_id"];
         							if (strpos($string, 'con') !== false)
         							{
         								$valid=0;
         							}else{
         								$valid=1;
         							}
         
         							if($valid==1){
         								if($elect["volt"]=='0000'){
         									$active=0;
         								}else{
         									$active=1;
         								} 
         							}else{
         								$active=2;
         							}
         							$node=$devicemapper->getNodeByConid($elect["consumer_id"],$Device["device_id"]);
         							$datas=array(
         									"consumer_device_id" =>   $Device["device_id"],
         									"consumer_id" => $elect["consumer_id"],
         									"sequence" =>	 $node["node"],
         									"wattage" =>	 intval($elect["volt"]),
         									"consumer_connection_id"=>$consumers->__get("consumer_connection_id"),
											"consumer_name"=>$consumers->__get("consumer_name"),
         									"valid"=>$valid,
         									"active"=>$active
         
         							);
         							$device_arr[]=$datas;
         						}
         						 
          						$date = new Zend_Date();
         						$date->setTimezone("Asia/Calcutta");
         						$current_date = $date->toString("yyyy-MM-dd HH:mm:ss");
         						if($elecs["info_date"]){
         							$lasttime=$elecs["info_date"];
         							$cal=(strtotime($current_date)-strtotime($lasttime)) / (60 * 60 * 24);
         							$days= $cal ;
         							$sec=$days;
         
         							$mins=$days*1440;
         							$sec=$days*86400;
         							$hrs=$mins/60;
         
         							if($sec<60){
         								$next=intval($sec)." second ago";
         							}elseif($mins<60){
         								$next=intval($mins)." minute ago";
         							}elseif($hrs<24){
         								$next=intval($hrs)." hour ago";
         							}elseif($days<=7){
         								$next=intval($days)." day ago";
         							}elseif ($days<=31){
         								$days=intval($days/7);
         								$next=$days." week ago";
         							}elseif ($days<=365){
         								$days=intval($days/30);
         								$next=$days." month ago";
         							}else{
         								$days=intval($days/365);
         								$next=$days." year ago";
         							}
         
         						}else{
         							$next="No Details Found";
         							$hrs=-1;
         						}
         						/* $sort_arr=array();
         						foreach ($device_arr as $key => $row)
         						{
         							$sort_arr[$key] = $row['sequence'];
         						}
         						array_multisort($sort_arr, SORT_ASC ,$device_arr); */
         
         						$data=array(
         								"device_id" =>$Device["device_id"],
         								"device_name" => $Device["device_name"],
         								"site_id" => $Device["site_id"],
         								"lastDate"=> $next,
         								"lastdata"=>intval($hrs),
         								"consumerDetails"=>$device_arr,
         						);
         						 
         						$cluster_arr[]=$data;
         					}
         
         					 
         				}else{
         
         					$devices_val=$devicemapper->getAllAddDevice($Device["device_id"]);
							if($devices_val){
								foreach($devices_val as $device){
									$consumers=$consumerMapper->getConsumerById($device->__get("consumer_id"));
									$string=$device->__get("consumer_id");
									if (strpos($string, 'con') !== false)
									{
										$valid=0;
									}else{
										$valid=1;
									}
			 
									$active=2;
									 
									$datas=array(
											"consumer_device_id" =>   $device->__get("device_id"),
											"consumer_id" => $device->__get("consumer_id"),
											"sequence" =>	 $device->__get("node"),
											"wattage" =>	 intval($device->__get("wattage")),
											"status" =>"",
											"consumer_connection_id"=>$consumers->__get("consumer_connection_id"),
											"consumer_name"=>$consumers->__get("consumer_name"),
											"valid"=>$valid,
											"active"=>$active
			 
									); 
									$device_arr[]=$datas;
								}
							} 
         					$next="No Details Found";
         					$hrs=-1;
         					/* $sort_arr=array();
         					foreach ($device_arr as $key => $row)
         					{
         						$sort_arr[$key] = $row['sequence'];
         					}
         					array_multisort($sort_arr, SORT_ASC ,$device_arr); */
         					$data=array(
         							"device_id" =>$Device["device_id"],
         							"device_name" => $Device["device_name"],
         							"site_id" => $Device["site_id"],
         							"lastDate"=>$next,
         							"consumerDetails"=>$device_arr,
         							"lastdata"=>intval($hrs)
         					);
         					 
         					$cluster_arr[]=$data;
         
         				}
         			}
         			 
         
         			$countval=$demapper->getDeviceCount($site_id); 
         			 
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $cluster_arr,
							"total"=>$countval
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while adding"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $cluster_arr,
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
         
    public function addUpdateDeviceAction(){
      
         try {
              $request=$this->getRequest();
             $device_name=$request->getParam("device_name");
             $consumer_id=$request->getParam("consumer_id");
             $node=$request->getParam("node");
             $wattage=$request->getParam("wattage");
             if($wattage=='undefined'){
                 $wattage='0000';
             }
             $wattage=sprintf("%04d",$wattage);
             $devicemapper=new Application_Model_AddDeviceMapper();
             $device= new Application_Model_AddDevice();

             $device->__set("device_name",$device_name);
             $device->__set("consumer_id",$consumer_id);
             $device->__set("node",$node);
             $device->__set("wattage",$wattage);

             $devicemapper->updateDevice($device);


            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
	public function getAllConsumerForDeviceAction(){
				try 
				{
                $request=$this->getRequest();
                $all=$request->getParam("all");
				 $device_id=$request->getParam("device_id");
	            $auth=new My_Auth('user');
	            $user=$auth->getIdentity()->user_role;
	            $state_id=$auth->getIdentity()->state_id;
	            
	            $roleSession = new Zend_Session_Namespace('roles');
	            $role_sites_id=$roleSession->site_id;
	            $consumer_arr=array();
                $devicemapper=new Application_Model_AddDeviceMapper();
				$consumerPackagesMapper=new Application_Model_ConsumersPackageMapper();
                $wattageMapper=new Application_Model_WattageMapper();
                if($consumers=$devicemapper->getAllConsumerForDevice($all,$device_id)){
                 
                    foreach ($consumers as $consumer) {
                          $wattage_total=0;
							$cps=$consumerPackagesMapper->getPackageByConsumerId($device["consumer_id"]);
							if($cps){
								foreach ($cps as $cp){
									$watt_id=$cp["wattage"];
									$watts=$wattageMapper->getWattageBywattId($watt_id);
									if($watts){
										$wattage_total=$wattage_total+$watts->__get("wattage");
									}
								}
							}
 						$data = array(
	                        'consumer_id' => $consumer->__get("consumer_id"), 
 							'consumer_name' => $consumer->__get("consumer_name"),
 							'site_id' => $consumer->__get("site_id"),
 							'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
							'wattage'=>$wattage_total
	                    	 
                    	);
                    $consumer_arr[]=$data;
               
                 }
				
				$meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                     
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function updateDeviceByIdAction(){
    
    	try {
    		$request=$this->getRequest();
    		$device_id=$request->getParam("device_id");
    		$sim_no=$request->getParam("sim_no");
    		$time=$request->getParam("time");
    		$pole=$request->getParam("pole");
    		$type=$request->getParam("type");
    	 
    		
    		$siteMapper=new Application_Model_SitesMapper();
    		$deviceMapper=new Application_Model_DeviceMapper();
    		$site_id=$deviceMapper->getDeviceById($device_id);
    		$sites=$siteMapper->getSiteById($site_id->__get("site_id"));
    		$device_name=$sites->__get("site_code").sprintf("%03d",$pole).$type;
    		$check=$deviceMapper->getDeviceByDeviceName($device_id,$device_name);
    		if(!$check){
    		$device= new Application_Model_Device();
    			
    		$device->__set("device_id",$device_id);
    		$device->__set("time",$time);
    		$device->__set("device_sim",$sim_no);
    		$device->__set("device_name",$device_name);
    
    		if($deviceMapper->updateDevice($device)){
    			$this->_logger->info("State ID ".$device_id." has been updated by ". $this->_userName.".");
    
    				
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while updating"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    		}else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while updating"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    			
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }

    public function deleteconsumerdeviceByIdAction(){
    
    	try {
    		$request=$this->getRequest();
    		$device_id=$request->getParam("device_id");
    		$deviceMapper=new Application_Model_AddDeviceMapper();
    		$delete=$deviceMapper->deleteConsumerDeviceById($device_id);
    
    		 
    		 
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$arr = array(
    				"meta" => $meta,
    
    		);
    
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }

	public function getConsumerByPoleAction(){
        try 
        {
                $request=$this->getRequest();
                $site_id=$request->getParam("site_id");
	            $poles=$request->getParam('poles');
				$device_type=$request->getParam('device_type');
	            
	            $consumer_arr=array();
                $consumersMapper=new Application_Model_ConsumersMapper();
                $sitesMapper=new Application_Model_SitesMapper();
                $packageMapper=new Application_Model_PackagesMapper();
                $sites=$sitesMapper->getSiteById($site_id);
                $deviceType=$device_type;
                $pole=sprintf("%03d",$poles);
                for($i=1;$i<6;$i++){
                	$consumer_connection_id= $sites->__get("site_code").$pole.$deviceType;
                	$consumer=$consumersMapper->getConsumerByConnectionIDs($consumer_connection_id);
                	 
                	if($consumer){
                	$package=$packageMapper->getPackageById($consumer->__get("package_id"));
 						$data = array(
	                        'consumer_id' => $consumer->__get("consumer_id"), 
 							'consumer_name' => $consumer->__get("consumer_name"),
 							'site_id' => $consumer->__get("site_id"),
 							'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
 						  	'package_name'=>$package->__get("package_name"),
	                    	"node"=>"Node ".$i,
 							"checkbox_val"=>1
                    	);
                	}else{
                		$data = array(
                				'consumer_id' => "con".$i,
                				'consumer_name' =>null,
                				'site_id' => null,
                				'consumer_connection_id' =>null,
                				'package_name'=>null,
                				"node"=>"Node ".$i,
                				"checkbox_val"=>0
                		);
                	}
                    $consumer_arr[]=$data;
                    $deviceType=$deviceType+01;
                    $deviceType=sprintf("%02d",$deviceType);
                }
              
				
				$meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                     
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	 
	public function checkSimnoInDeviceAction(){
        try 
        {
                $request=$this->getRequest();
                $sim_no=$request->getParam("sim_no");
	             

                $deviceMapper=new Application_Model_DeviceMapper();
                $devices=$deviceMapper->getDeviceBySimNo($sim_no);
                if($devices){
                	$consumer_arr=true;
                }else{
                	$consumer_arr=false;
                }
                 
				$meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                     
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $consumer_arr,
                );
            
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function sendMsgToDeviceAction(){
    	try
    	{
    		$request=$this->getRequest();
    		 $device_id=$request->getParam("device_id");
    
			 $mongoDriver= new MongoClient();
	    	 $mongoDB=$mongoDriver->tara_devices;
	    	 $db=$mongoDB->logs;
    		 
			 $date = new Zend_Date();
	    	 $date->setTimezone("Asia/Calcutta");
	    	 $timestamps = $date->toString("yyyy-MM-dd HH:mm:ss");
			 
    		$deviceMapper=new Application_Model_AddDeviceMapper();
    		$devices=$deviceMapper->getDataByDeviceid($device_id);
    		$msg="";
    		if($devices){
    			$sim_no=$devices[0]["device_sim"];
    			$device_name=$devices[0]["name"];
    			$time=$devices[0]["time"];
    			$times=sprintf("%04d",$time);
    			  $count_device=count($devices);
    			   
    			  $nodeArray=array();
    			  for ($i=0;$i<$count_device;$i++){
    			  		if($devices[$i]["node"]==1){
    			  			$nodeArray[]="NODEA".$devices[$i]["wattage"];
    			  		}else if($devices[$i]["node"]==2){
    			  			$nodeArray[]="NODEB".$devices[$i]["wattage"];
    			  		}else if($devices[$i]["node"]==3){
    			  			$nodeArray[]="NODEC".$devices[$i]["wattage"];
    			  		}else if($devices[$i]["node"]==4){
    			  			$nodeArray[]="NODED".$devices[$i]["wattage"];
    			  		}else if($devices[$i]["node"]==5){
    			  			$nodeArray[]="NODEE".$devices[$i]["wattage"];
    			  		} 
    			  }
				
    			$msg="616629;LCDID".$device_name.";".$nodeArray[0].";".$nodeArray[1].";".$nodeArray[2].";".$nodeArray[3].";".$nodeArray[4].";"."TIME".$times.";";
				$texts= $this->_smsNotification($sim_no, $msg);
                //$this->_logger->info($msg);
				$data_array= array("device_name" => $device_name,
									"timestamp"=> $timestamps,
									"message" =>$msg,  
				);
				   
				$result= $db->insert($data_array);
                $meta = array(
    					"code" => 200,
    					"message" => "SUCCESS",
						"sms" =>  $texts,
    						
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $msg,
    					"sim_no"=>$sim_no
    			);
    		}else{
    			$meta = array(
    					"code" => 200,
    					"message" => "Fail",
    						
    			);
    			$arr = array(
    					"meta" => $meta,
    			);
    		}
    	}
    	catch (Exception $e)
    	{
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	protected function _smsNotification($number, $sms) {
    	 date_default_timezone_set('Asia/Kolkata');
    	$number = substr($number, 0, 10);
		//$number = 8285859194;
    	$sms = urlencode($sms);
    	$smsMapper =new Application_Model_SmsGatewayMapper();
    	$smsgateway = $smsMapper->getAllSmsGateway(); 
    	$user_name=$smsgateway[0]->__get("user_name");
    	$password=$smsgateway[0]->__get("password");
    
    	$url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=".$user_name."&auth_scheme=plain&password=".$password."&v=1.1&format=text";
    
    	$text = file_get_contents($url);
		 
		$file = 'test.txt';
        $current = file_get_contents($file);
        $current .= "\r\n".$sms.";".date('Y-m-d H:i:s').$text."\r\n"; 
		file_put_contents($file, $current);
		
    	return $text;
    }

    public function getAllMsgAction(){

        try {
            $request=$this->getRequest();
            $device_id=$request->getParam("device_id");


            $deviceMapper=new Application_Model_AddDeviceMapper();
            $devices=$deviceMapper->getDataByDeviceid($device_id);
            $deviceMapper=new Application_Model_AddDeviceMapper();
            $msg="";
            if($devices){
                $sim_no=$devices[0]["device_sim"];
                $timestamp=$devices[0]["timestamp"];
                $device_name=$devices[0]["name"];
                $time=$devices[0]["time"];
                $times=sprintf("%04d",$time);
                $count_device=count($devices);
                $device_log=$deviceMapper->getSocketDatabyDeviceid($device_name);
                $record=array();
                if($device_log){
                    foreach ($device_log as $device_logs){
                    	if (strpos($device_logs["message"], 'LCDID') !== false)
                    	{
                    		$color="yes";
                    	}else{
                    		$color="no";
                    	}
                        $data=array("timestamp" =>$device_logs["timestamp"],
                                    "message" =>date('H:i:s d,M ', strtotime($device_logs["timestamp"]))." - ".$device_logs["message"],
									"color"=>$color
                                );
                        $record[]= $data;
                    }
                }
                
                /*$nodeArray=array();
                for ($i=0;$i<$count_device;$i++){
                    if($devices[$i]["node"]==1){
                        $nodeArray[]="NODEA".$devices[$i]["wattage"];
                    }else if($devices[$i]["node"]==2){
                        $nodeArray[]="NODEB".$devices[$i]["wattage"];
                    }else if($devices[$i]["node"]==3){
                        $nodeArray[]="NODEC".$devices[$i]["wattage"];
                    }else if($devices[$i]["node"]==4){
                        $nodeArray[]="NODED".$devices[$i]["wattage"];
                    }else if($devices[$i]["node"]==5){
                        $nodeArray[]="NODEE".$devices[$i]["wattage"];
                    }
                }
                $msg="LCDID".$device_name.";".$nodeArray[0].";".$nodeArray[1].";".$nodeArray[2].";".$nodeArray[3].";".$nodeArray[4].";"."TIME".$times."; ";
                $time_stamp=$timestamp;
                $data=array("timestamp" =>$timestamp,
                    "message" =>date('H:i:s d,M ', strtotime($timestamp))." ".$msg,
					"color"=>"yes"
                );
                $record[]= $data; */
                
				$sort_arr = array();
				foreach ($record as $key => $row)
				{
					$sort_arr[$key] = $row['timestamp'];
				}
				array_multisort($sort_arr, SORT_DESC ,$record);
				$device_logs=$deviceMapper->getFullSocketDatabyDeviceid($device_name);
                $records=array();
                if($device_logs){
                    foreach ($device_logs as $device_logs){
                        $data=array("timestamp" =>$device_logs["timestamp"],
                                    "message" =>date('H:i:s d,M ', strtotime($device_logs["timestamp"]))." - ".$device_logs["message"]
                                );
                        $records[]= $data;
                    }
                }
				 $records[]= $data;
                $sort_arr = array();
                foreach ($records as $key => $row)
                { 
                    $sort_arr[$key] = $row['timestamp'];
                }
                array_multisort($sort_arr, SORT_DESC ,$records);
                $filename = "logsCsv.csv";
                $fp = fopen($filename, 'w');
                foreach ($records as $recordss) {
                    fputcsv($fp, $recordss);
                }

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",

                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $record,

                );
				 
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta,
                );
            }
        }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getConsumerByDeviceidAction(){
        try
        {
                $request=$this->getRequest();
                $device_id=$request->getParam("device_id");

	            $consumer_arr=array();
                $consumersMapper=new Application_Model_ConsumersMapper();
                $sitesMapper=new Application_Model_SitesMapper();

                $AddDeviceMapper=new Application_Model_AddDeviceMapper();
				$consumerPackagesMapper=new Application_Model_ConsumersPackageMapper();
                $wattageMapper=new Application_Model_WattageMapper();
                $deviceMapper=new Application_Model_DeviceMapper();
				$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
                $device_arr=array();
                $devices=$AddDeviceMapper->getDataByDeviceid($device_id);
                if($devices){ 
                    foreach($devices as $device){
                        $consumers_val=$consumersMapper->getConsumerById($device["consumer_id"]);
                        $string=$device["consumer_id"];
						$wattage_total=0;
                        if (strpos($string, 'con') !== false){
                            $checkbox_val=0;
                        }else{
                            $checkbox_val=1;
							$cps=$consumerPackagesMapper->getPackageByConsumerId($device["consumer_id"]);
                            if($cps){
                            	foreach ($cps as $cp){
									if($cp["is_postpaid"]==0){
										$watt_id=$cp["wattage"];
										$watts=$wattageMapper->getWattageBywattId($watt_id);
										if($watts){
											$wattage_total=$wattage_total+$watts->__get("wattage");
										}
									}
                            	}
                            }
							
							/*$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($device["consumer_id"]);
							
							$zendDate = new Zend_Date();
							$zendDate->setTimezone("Asia/Calcutta");
							$date_val = $zendDate->toString("yyyy-MM-dd"); 
							$trans_date=date_parse_from_format("Y-m-d", $date_val);
							$month_data= $trans_date["month"];
							$year_data= $trans_date["year"]; 
							 
							$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_data, $year_data);  
							if($consumer_scheme){
								foreach($consumer_scheme as $consumer_schemes){
									$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
									$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
							
									$discount_month=$consumer_schemes["discount_month"];
									$feeder_hours=$consumer_schemes["feeder_hours"];
									$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month"));
									 
									if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
										$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]));
										$wattage_total=$wattage_total + $dailyUnit;
									}
								}
							}*/ 
                        }
                        if($device["wattage"]=='0000'){
                            $active=0;
                        }else{
                            $active=1;
                        }
                        $datas=array(
                            "consumer_device_id" =>   $device["device_id"],
                            "consumer_id" => $device["consumer_id"],
                            "sequence" =>	 $device["node"],
                            "node" =>	 "Node ".$device["node"],
                            "wattage" =>	 intval($wattage_total),
                            "consumer_name"=>$consumers_val->__get("consumer_name"),
                            "consumer_connection_id"=>$consumers_val->__get("consumer_connection_id"),
                            "checkbox_val"=>$checkbox_val,
                            "active"=>$active

                        );
                        $device_arr[]=$datas;
						$site_id=$device["site_id"];
                    }
					$Adddevicemapper=new Application_Model_AddDeviceMapper();
					
					if($consumers=$Adddevicemapper->getAllConsumerForDevice(1,$device_id,$site_id)){
						
						foreach ($consumers as $consumer) {
							 $wattage_total=0;
							$cps=$consumerPackagesMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
							if($cps){
								foreach ($cps as $cp){
									if($cp["is_postpaid"]==0){
										$watt_id=$cp["wattage"];
										$watts=$wattageMapper->getWattageBywattId($watt_id);
										if($watts){
											$wattage_total=$wattage_total+$watts->__get("wattage");
										}
									}
								}
							}
							
							/*$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer->__get("consumer_id"));
							
							$zendDate = new Zend_Date();
							$zendDate->setTimezone("Asia/Calcutta");
							$date_val = $zendDate->toString("yyyy-MM-dd"); 
							$trans_date=date_parse_from_format("Y-m-d", $date_val);
							$month_data= $trans_date["month"];
							$year_data= $trans_date["year"];
							 
							$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_data, $year_data);  
							if($consumer_scheme){
								foreach($consumer_scheme as $consumer_schemes){
									$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
									$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
							
									$discount_month=$consumer_schemes["discount_month"];
									$feeder_hours=$consumer_schemes["feeder_hours"];
									//$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
									$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month"));
									 
									if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
										$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]));
										$wattage_total=$wattage_total + $dailyUnit;
									}
								}
							}*/
							 
							$data = array(
								'consumer_id' => $consumer->__get("consumer_id"), 
								'consumer_name' => $consumer->__get("consumer_name"),
								'site_id' => $consumer->__get("site_id"),
								'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
								'wattage' => $wattage_total, 
								 
							);
							$consumer_arr[]=$data;
						}
					}
					$sitesData=$sitesMapper->getSiteById($devices[0]["site_id"]);
					$site_code=$sitesData->__get("site_code");
					$pole=explode($site_code, $devices[0]["name"]);
				 	$stringPole=$pole[1];
					$pole=substr($stringPole,0,3);   
					$device_type=substr($stringPole,3,1);
					 
					/* if($device_type=="A"){
						$type=01;
					}elseif ($device_type=="B"){
						$type=06;
					}elseif($device_type=="C"){
						$type=11;
					}elseif ($device_type=="D"){
						$type=15;
					}elseif ($device_type=="E"){
						$type=20;
					}  
				  */
                    $data=array(
                        "device_id" =>$devices[0]["device_id"],
                        "device_name" => $devices[0]["name"],
                        "site_id" => $devices[0]["site_id"],
                        "sim_no"=>intval($devices[0]["device_sim"]),
                        "time"=>intval($devices[0]["time"]),
                    	"poleNo"=>intval(sprintf("%03d",$pole)),
                    	"device_type"=>$device_type,
                        "consumerDetails"=>$device_arr,
                    	"consumers"=>$consumer_arr
                    );

                    $dev_arr[]=$data;
                }

				$meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",

                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $dev_arr,
                );

        }
        catch (Exception $e)
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	public function getAllIermsConsumerAction()
		{
			try 
				{ 
                $request=$this->getRequest();
                $site_id=$request->getParam("site_id");
				//$device_id=$request->getParam("device_id");
	            $auth=new My_Auth('user');
	            $user=$auth->getIdentity()->user_role;
	            $state_id=$auth->getIdentity()->state_id;
	            if($site_id=="All" || $site_id==""){
					$site_id=NULL;
				} 
				$count=0;
	            $roleSession = new Zend_Session_Namespace('roles');
	            $role_sites_id=$roleSession->site_id;
	            $consumer_arr=array();
                $devicemapper=new Application_Model_AddDeviceMapper();
				$consumerPackagesMapper=new Application_Model_ConsumersPackageMapper();
                $wattageMapper=new Application_Model_WattageMapper();
				$electricityMapper=new Application_Model_ElectricityInfoMapper();
							
                if($consumers=$devicemapper->getAllConsumerForIERMS($site_id,$role_sites_id)){
                 $count=count($consumers); 
                    foreach ($consumers as $consumer) { 
                          $wattage_total=0;$package_name=array();
						  
						  $cps=$consumerPackagesMapper->getPackageByConsumerId($consumer["consumer_id"]);
							if($cps){
								foreach ($cps as $cp){
									if($cp["is_postpaid"]==0){
										$watt_id=$cp["wattage"];
										$watts=$wattageMapper->getWattageBywattId($watt_id);
										if($watts){
											$wattage_total=$wattage_total+$watts->__get("wattage");
										}
									}
										$package_name[]=$cp["package_name"];
									
								}
							}
							$electricitys=$electricityMapper->getElectricitystatusByDevice($consumer["device_name"]);
							if($electricitys){
								
								$electricity=$electricitys[0];
								$last_timestamp=date("d-M-Y g:i a",strtotime($electricity["info_date"]));
								$consumers_data=explode(",",$electricity["consumer_id"]);
								$volt_data=explode(",",$electricity["volt"]);
								for($e=0;$e<count($consumers_data);$e++){
									if($consumer["consumer_id"]==$consumers_data[$e]){
										if($volt_data[$e]=='0000'){
											$active=0;
										}else{
											$active=1;
										}
									}
								}
								
								$date = new Zend_Date();
								$date->setTimezone("Asia/Calcutta");
         						$current_date = $date->toString("yyyy-MM-dd HH:mm:ss");
         						if($electricity["info_date"]){
         							$lasttime=$electricity["info_date"];
         							$cal=(strtotime($current_date)-strtotime($lasttime)) / (60 * 60 * 24);
         							$days= $cal ; $sec=$days;
									$mins=$days*1440; $sec=$days*86400; $hrs=$mins/60;
									
         							if($sec<60){
         								$next=intval($sec)." second ago";
         							}elseif($mins<60){
         								$next=intval($mins)." minute ago";
         							}elseif($hrs<24){
         								$next=intval($hrs)." hour ago";
         							}elseif($days<=7){
         								$next=intval($days)." day ago";
         							}elseif ($days<=31){
         								$days=intval($days/7);
         								$next=$days." week ago";
         							}elseif ($days<=365){
         								$days=intval($days/30);
         								$next=$days." month ago";
         							}else{
         								$days=intval($days/365);
         								$next=$days." year ago";
         							}
								}else{
         							$next="No Details Found";
         							$hrs=-1;
         						}
							}else{
								$last_timestamp=NULL;
								$next="No Details Found";
         						$hrs=-1;
								$active=0;
							}
							$feedarDetail=array();
							$MMmapper=new Application_Model_SiteMasterMeterMapper();
							$site_meter_ids=explode(",", $consumer['site_meter_id']);
								if(($site_meter_ids)>0){
									for ($k=0;$k<count($site_meter_ids);$k++){
										$feedarDetails=$MMmapper->getMMById($site_meter_ids[$k]);	
										if($feedarDetails){
											$feedarDetail[]=$feedarDetails->__get("meter_name")." ".$feedarDetails->__get("description");
										}
									}
								}
							$data = array(
								'device_id'=>$consumer["device_id"],
								'device_name'=>$consumer["device_name"],
								'device_sim'=>$consumer["device_sim"],
								'consumer_id' => $consumer["consumer_id"],  
								'sms_status'=>$consumer["sms_status"],
								'sms_timestamp' => ($consumer["sms_timestamp"]==NULL || $consumer["sms_timestamp"]=="")?NULL:date("d-M-Y g:i a",strtotime($consumer["sms_timestamp"])), 
								'consumer_name' => $consumer["consumer_name"] ." (".$consumer["consumer_connection_id"].")",
								'site_id' => $consumer["site_id"],
								'consumer_connection_id' => $consumer["consumer_connection_id"],
								'consumer_status' => $consumer["consumer_status"],
								'wattage'=>$wattage_total,
								'node_no'=>$consumer["node"],
								'package_name'=>implode(",",$package_name),
								"lastDate"=>$next,
								"lastdata"=>intval($hrs),
								"active"=>$active,
								"last_timestamp"=>$last_timestamp, 
								"feeder_details"=>implode(",",$feedarDetail),  
							);
						
							$consumer_arr[]=$data;
					}
					$meta = array(
						"code" => 200,
						"message" => "SUCCESS",
						 
					);
					$arr = array(
						"meta" => $meta,
						"data" => $consumer_arr,
						"countCon"=>$count
					);
				} else {
					$meta = array(
						"code" => 401,
						"message" => "Error while getting",
						"data" => $consumer_arr,
						"countCon"=>$count
					);
					$arr = array(
						"meta" => $meta
					);
				}
        }
        catch (Exception $e) 
        {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
	
	public function sendMsgToAllConsumerDevicesAction(){
    	try
    	{
    		$request=$this->getRequest();
    		
			 $date = new Zend_Date();
	    	 $date->setTimezone("Asia/Calcutta");
	    	 $timestamps = $date->toString("yyyy-MM-dd HH:mm:ss");
			
			$iermsMapper=new Application_Model_DeviceMapper();
    		$deviceMapper=new Application_Model_AddDeviceMapper(); 
			$wattageMapper=new Application_Model_WattageMapper();
			
			$allDevices=$iermsMapper->getAllDevice();
			 
			if($allDevices){
				foreach($allDevices as $allDevice){
				  
				$devices=$deviceMapper->getDataByDeviceid($allDevice["device_id"]); 
				$msg="";
				if($devices){
					$sim_no=$devices[0]["device_sim"];
					$device_name=$devices[0]["name"];
					$time=$devices[0]["time"];
					$times=sprintf("%04d",$time);
					  $count_device=count($devices);
					  $consumerPackagesMapper=new Application_Model_ConsumersPackageMapper();
							
					  $nodeArray=array();
					  for ($i=0;$i<$count_device;$i++){
							if($devices[$i]["node"]==1){
								$wattage_total='0000';
								$cps=$consumerPackagesMapper->getPackageByConsumerId($devices[$i]["consumer_id"]);
								if($cps){
									foreach ($cps as $cp){
										if($cp["is_postpaid"]==0){
											$watt_id=$cp["wattage"];
											$watts=$wattageMapper->getWattageBywattId($watt_id);
											if($watts){
												$wattage_total=$wattage_total+$watts->__get("wattage");
											}
										}
									}
								}
								$nodeArray[]="NODEA".sprintf("%04d",$wattage_total);
							}else if($devices[$i]["node"]==2){
								$wattage_total='0000';
								$cps=$consumerPackagesMapper->getPackageByConsumerId($devices[$i]["consumer_id"]);
								if($cps){
									foreach ($cps as $cp){
										if($cp["is_postpaid"]==0){
											$watt_id=$cp["wattage"];
											$watts=$wattageMapper->getWattageBywattId($watt_id);
											if($watts){
												$wattage_total=$wattage_total+$watts->__get("wattage");
											}
										}
									}
								}
								$nodeArray[]="NODEB".sprintf("%04d",$wattage_total);
							}else if($devices[$i]["node"]==3){
								$wattage_total='0000';
								$cps=$consumerPackagesMapper->getPackageByConsumerId($devices[$i]["consumer_id"]);
								if($cps){
									foreach ($cps as $cp){
										if($cp["is_postpaid"]==0){
											$watt_id=$cp["wattage"];
											$watts=$wattageMapper->getWattageBywattId($watt_id);
											if($watts){
												$wattage_total=$wattage_total+$watts->__get("wattage");
											}
										}
									}
								}
								$nodeArray[]="NODEC".sprintf("%04d",$wattage_total);
							}else if($devices[$i]["node"]==4){
								$wattage_total='0000';
								$cps=$consumerPackagesMapper->getPackageByConsumerId($devices[$i]["consumer_id"]);
								if($cps){
									foreach ($cps as $cp){
										if($cp["is_postpaid"]==0){
											$watt_id=$cp["wattage"];
											$watts=$wattageMapper->getWattageBywattId($watt_id);
											if($watts){
												$wattage_total=$wattage_total+$watts->__get("wattage");
											}
										}
									}
								}
								$nodeArray[]="NODED".sprintf("%04d",$wattage_total);
							}else if($devices[$i]["node"]==5){
								$wattage_total='0000';
								$cps=$consumerPackagesMapper->getPackageByConsumerId($devices[$i]["consumer_id"]);
								if($cps){
									foreach ($cps as $cp){
										if($cp["is_postpaid"]==0){
											$watt_id=$cp["wattage"];
											$watts=$wattageMapper->getWattageBywattId($watt_id);
											if($watts){
												$wattage_total=$wattage_total+$watts->__get("wattage");
											}
										}
									}
								}
								$nodeArray[]="NODEE".sprintf("%04d",$wattage_total);
							}
					  }
						
					$msg="616629;LCDID".$device_name.";".$nodeArray[0].";".$nodeArray[1].";".$nodeArray[2].";".$nodeArray[3].";".$nodeArray[4].";"."TIME".$times.";";
					echo " ".$msg." "; 
					//$sim_no='1234543232323'; 
					$texts= $this->_smsNotification($sim_no, $msg);
					
					if (strpos($texts, 'success') !== false){
						$iermsMapper->updateSmsDeviceStatsus('Success',$timestamps,$allDevice["device_id"]);
					}else{
						$iermsMapper->updateSmsDeviceStatsus('Failure',$timestamps,$allDevice["device_id"]);
					}   
					//echo $texts." ".$allDevice->__get("device_id"); 
					/*$this->_logger->info($msg);
					$data_array= array("device_name" => $device_name,
										"timestamp"=> $timestamps,
										"message" =>$msg,  
					);*/
				 
				}
				 
			 }
		   }
    	} catch (Exception $e)
			{
				$meta = array(
						"code" => 501,
						"message" => $e->getMessage()
				);
		
				$arr = array(
						"meta" => $meta
				);
			}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
    
	
}   