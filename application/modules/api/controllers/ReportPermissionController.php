<?php

class Api_ReportPermissionController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	

	public function addReportPermissionAction(){
	
		try {
			$request=$this->getRequest();
	
			$report_name=$request->getParam("report_name");
			$user_id=$request->getParam("user_id");
			
     		$reportPermissionMapper=new Application_Model_ReportPermissionMapper();
     		
     		$checkReport=$reportPermissionMapper->getReportsByReportName($report_name);
				if($checkReport){
					$user_val=explode(",", $checkReport["user_id"]);
					$user_arr=explode(",", $user_id);
					
					$merge_user=array_merge($user_arr,$user_val);
					$users=array_unique($merge_user);
					$user=array_values($users);
					
					$reports= new Application_Model_ReportPermission();
					
					$reports->__set("report_name",$report_name);
					$reports->__set("user_id",$user);
					$reports->__set("id",$checkReport["id"]);
					
					$reportPermission=$reportPermissionMapper->updateReportPermission($reports);
				}else{
					$reportPermission=$reportPermissionMapper->addReportPermission($report_name,$user_id);
				}
     		
	     		if($reportPermission)
	     		{
		     		$data=array(
		     				"report_name" => $report_name,
		     				"user_id" => $user_id,
		     				
		     		);
		     		$meta = array(
		     				"code" => 200,
		     				"message" => "SUCCESS"
		     		);
		     		
		     		$arr = array(
		     				"meta" => $meta,
		     				"data" => $data
		     		);
	     		} else {
	     			$meta = array(
	     					"code" => 401,
	     					"message" => "Error while adding"
	     			);
	     			$arr = array(
	     					"meta" => $meta
	     			);
	     		}
     		}catch (Exception $e) {
     			$meta = array(
     					"code" => 501,
     					"messgae" => $e->getMessage()
     			);
     		
     			$arr = array(
     					"meta" => $meta
     			);
     		}
     		$json = json_encode($arr, JSON_PRETTY_PRINT);
     		echo $json;
     }
	
	public function updateReportPermissionAction(){
     		
     			try {
     				$request=$this->getRequest();
     				$report_name=$request->getParam("report_name");
     				$user_id=$request->getParam("user_id");
     				$id=$request->getParam("id");
     		
     				$user=explode(",", $user_id);
     				$reportPermissionMapper=new Application_Model_ReportPermissionMapper();
     				$reports= new Application_Model_ReportPermission();
     					
     				$reports->__set("report_name",$report_name);
     				$reports->__set("user_id",$user);
     				$reports->__set("id",$id);
     					
     		
     				if($reportPermissionMapper->updateReportPermission($reports)){
     					$this->_logger->info("Report Name ".$report_name." has been updated by ". $this->_userName.".");
     		
     						
     					$meta = array(
     							"code" => 200,
     							"message" => "SUCCESS"
     					);
     					$arr = array(
     							"meta" => $meta,
     		
     					);
     				} else {
     					$meta = array(
     							"code" => 401,
     							"message" => "Error while updating"
     					);
     					$arr = array(
     							"meta" => $meta
     					);
     				}
     			}catch (Exception $e) {
     				$meta = array(
     						"code" => 501,
     						"messgae" => $e->getMessage()
     				);
     		
     				$arr = array(
     						"meta" => $meta
     				);
     			}
     			$json = json_encode($arr, JSON_PRETTY_PRINT);
     			echo $json;
     		}
	
	public function deleteReportByNameAction(){
     		
     			try {
     				$request=$this->getRequest();
     				$id=$request->getParam("id");
     				
     				$reportPermissionMapper=new Application_Model_ReportPermissionMapper();
     				if($Report=$reportPermissionMapper->deleteReportById($id)){
     					$this->_logger->info("Report ".$report_name." has been deleted from Report Permission by ". $this->_userName.".");
     		
     					$meta = array(
     							"code" => 200,
     							"message" => "SUCCESS"
     					);
     					$arr = array(
     							"meta" => $meta,
     		
     					);
     				} else {
     					$meta = array(
     							"code" => 401,
     							"message" => "Error while deleting"
     					);
     					$arr = array(
     							"meta" => $meta
     					);
     				}
     			}catch (Exception $e) {
     				$meta = array(
     						"code" => 501,
     						"messgae" => $e->getMessage()
     				);
     		
     				$arr = array(
     						"meta" => $meta
     				);
     			}
     			$json = json_encode($arr, JSON_PRETTY_PRINT);
     			echo $json;
     		}
     		 
    public function getAllReportPermissionAction(){
     		 
     			try {
     				$reportPermissionMapper=new Application_Model_ReportPermissionMapper();
     					
     				$reports=$reportPermissionMapper->getAllReports();
     				$report_arr=array();
     				if($reports){
     					foreach ($reports as $report) {
     		
     						$data=array(
     								"report_name" => $report->__get("report_name"),
     								"user_id" => $report->__get("user_id"),
     								"id" => $report->__get("id"),
     									
     						);
     		
     						$report_arr[]=$data; 
     					}
     					$meta = array(
     							"code" => 200,
     							"message" => "SUCCESS"
     					);
     					$arr = array(
     							"meta" => $meta,
     							"data" => $report_arr,
     					);
     		
     				} else{
     					$meta = array(
     							"code" => 200,
     							"message" => "No result found"
     					);
     					$arr = array(
     							"meta" => $meta,
     							"data" =>$report_arr,
     					);
     				}
     		
     		
     			}catch (Exception $e) {
     				$meta = array(
     						"code" => 501,
     						"messgae" => $e->getMessage()
     				);
     		
     				$arr = array(
     						"meta" => $meta
     				);
     			}
     			$json = json_encode($arr, JSON_PRETTY_PRINT);
     			echo $json;
     		}
	
     		
     		
}