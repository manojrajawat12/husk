<?php
class Api_FormsController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth = new My_Auth('user');
		$this->_userName = $auth->getIdentity()->user_fname;
		$this->_user_id = $auth->getIdentity()->user_id;
	}
	
	public function addFormsAction(){
		try {
			$request=$this->getRequest();
			$form_attachment = $request->getParam("form_attachment");
			$form_name = $request->getParam("form_name");

			$date = new Zend_Date(); 
            $date->setTimezone("Asia/Calcutta");
            $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			$user_id = $this->_user_id;
			$FormsMapper = new Application_Model_FormsMapper();
			$Forms = new Application_Model_Forms();

			$Forms->__set("form_name", $form_name);
			$Forms->__set("form_attachment",$form_attachment);
			$Forms->__set("time_stamp",$timestamp);
			$Forms->__set("user_id", $user_id);
			$Forms->__set("user_type",'(W)');

			if( $FormId = $FormsMapper->addNewForms($Forms) ){
	
				$this->_logger->info("New Form ID ".$FormId." has been created in Form table by ". $this->_userName.".");
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		
 		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllFormsAction(){
	
		try {
			$FormsMapper = new Application_Model_FormsMapper();
			$Forms = $FormsMapper->getAllForms();
			if($Forms){
				foreach ($Forms as $Form) {
					
					$usersMapper = new Application_Model_UsersMapper();
					$user = $usersMapper->getUserById($Form->__get("user_id")); 
					$data=array(
							"id" => $Form->__get("id"),
							"form_name" => $Form->__get("form_name"),
							"form_attachment" => $Form->__get("form_attachment"),
							"time_stamp" => date("d-M-Y g:i a",strtotime($Form-> __get("time_stamp"))), 
							"user_id" => $user->user_fname." ".$user->user_lname,
							"user_type" => $Form->__get("user_type"),
					); 
					
					$Forms_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $Forms_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteFormsByIdAction(){
	
		try {
			$request = $this->getRequest();
			$id = $request->getParam("id");
			$FormsMapper = new Application_Model_FormsMapper();
			if($Formsid = $FormsMapper->deleteFormsById($id)){
				$this->_logger->info("Form Id ".$Formsid." has been deleted from Forms table by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	} 
}