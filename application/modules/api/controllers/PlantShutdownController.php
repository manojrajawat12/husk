<?php
class Api_PlantShutdownController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
		$this->user_id=$auth->getIdentity()->user_id;
	}
/*---------------------- 16 Feb 2018 (start) -----------------------*/	
	public function addPlantShutdownAction(){
		
		try {
			$request = $this->getRequest();
			$feeder_id = $request->getParam("feeder_id");
			$site_id = $request->getParam("site_id");
			$inactive_hours = $request->getParam("inactive_hours");
			$tmp_date = $request->getParam("date");

			$siteMapper = new Application_Model_SiteMasterMeterMapper();
			$master_meter = $siteMapper->getMMById($feeder_id);
			
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp_curr = $date->toString("yyyy-MM-dd HH:mm:ss");
			
			if($inactive_hours){

				foreach ($inactive_hours as $inactive_hour) {

					$siteStatusMapper = new Application_Model_SiteStatusMapper();
					$zendDate = new Zend_Date($tmp_date,"dd-MM-yyyy");
					$date = $zendDate->toString("yyyy-MM-dd");
					$siteStatus = new Application_Model_SiteStatus();
					$result = $siteStatusMapper->checkEntry($site_id,$feeder_id,$date,$inactive_hour);

					if(!$result){
						$siteStatus->__set("site_id",$site_id);
		         		$siteStatus->__set("feedar_id",$feeder_id);
		         		$siteStatus->__set("status","INACTIVE");
						$siteStatus->__set("timestamp",$date);
						$siteStatus->__set("start_date",$inactive_hour);
						$siteStatus->__set("entry_by",$this->user_id);
						$siteStatus->__set("entry_type","Web");
						$siteStatus->__set("entry_date",$timestamp_curr);
							
						 
						if($siteStatusMapper->addNewSiteStatus($siteStatus)){
							//echo "success"; 
						}else{
							//echo "fail";
						}
					}else{
						//echo "found";
					}
				}
			}
			$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						// "data" => $state_arr,
				);
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
/* ------------------------  16 Feb 2018 (start) ----------------------*/
	public function getAllPlantsAction(){
		
		try {
			$plantsMapper=new Application_Model_PlantManagementMapper();
			
			$plants=$plantsMapper->getAllPlants();
			
			foreach ($plants as $plant) {
				
			$site_id=$plant->__get("site_id");
			$state_id=$plant->__get("state_id");
			
			$stateMapper=new Application_Model_StatesMapper();
			$states=$stateMapper->getStateById($state_id);
			$state_name=$states->__get("state_name");
			
			$siteMapper=new Application_Model_SitesMapper();
			$sites=$siteMapper->getSiteById($site_id);
			$site_name=$sites->__get("site_name");
			
					$data=array(
							"id" => $plant->__get("id"),
							"state_name" => $state_name,
							"site_name" => $site_name,
							"activity_by" => $plant->__get("activity_by"),
							"activity_date" => $plant->__get("activity_date"),
							"maintenance_date" => $plant->__get("maintenance_date"),
							"sup_by" => $plant->__get("sup_by"),
							"remark" => $plant->__get("remark"),
							"user_id" => $plant->__get("user_id"),
							"timestamp" => $plant->__get("timestamp"),
							"activity" => $plant->__get("activity_type"),
							"attachment" => $plant->__get("attachment"),
							"site_id" => $plant->__get("site_id"),
							"state_id" => $plant->__get("state_id"),					
					);
	
					$state_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $state_arr,
				);
			}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	public function deletePlantByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$plantMapper=new Application_Model_PlantManagementMapper();
			if($plant=$plantMapper->deletePlantById($id)){
			//	$this->_logger->info("State Id ".$state_id." has been deleted from States by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	public function updatePlantByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$state_id=$request->getParam("state_id");
			$site_id=$request->getParam("site_id");
			$activity_by=$request->getParam("activity_by");
			$plant_activity_date=$request->getParam("activity_date");
			$plant_maintenance_date=$request->getParam("maintenance_date");
			$sup_by=$request->getParam("sup_by");
			$attachment=$request->getParam("attachment");
			$activity_type=$request->getParam("activity_type");
			$remark=$request->getParam("remark");
			$user_id=$this->user_id;
			
			$zendDate = new Zend_Date($plant_activity_date,"dd-MM-yyyy");
			$activity_date = $zendDate->toString("yyyy-MM-dd");

			$zendDate = new Zend_Date($plant_maintenance_date,"dd-MM-yyyy");
			$maintenance_date = $zendDate->toString("yyyy-MM-dd");
		 
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			
			$plantMapper=new Application_Model_PlantManagementMapper();
			$plant= new Application_Model_PlantManagement();
			$plant->__set("id",$id);
			$plant->__set("state_id",$state_id);
			$plant->__set("site_id",$site_id);
			$plant->__set("activity_by",$activity_by);
			$plant->__set("activity_date",$activity_date);
			$plant->__set("maintenance_date",$maintenance_date);
			$plant->__set("sup_by",$sup_by);
			$plant->__set("remark",$remark);
			$plant->__set("user_id",$user_id);
			$plant->__set("timestamp",$timestamp);
			$plant->__set("activity_type",$activity_type);
			$plant->__set("attachment",$attachment);
	
			if($update_plant=$plantMapper->updatePlantById($plant)){
				
			//	$this->_logger->info("State ID ".$state_id." has been updated by ". $this->_userName.".");
				
				 
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while updating"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	 public function getAllActvitiesAction(){

		try {
			$activitiesMapper = new Application_Model_TypesOfActivitiesMapper();
			
			$activities = $activitiesMapper->getAllActivities();
			foreach ($activities as $activity) {
				$data = array(
						"id" => $activity->__get("id"),
						"activity" => $activity->__get("activity"),
					);

			$state_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $state_arr,
				);
			}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}


}