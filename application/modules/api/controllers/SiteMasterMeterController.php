<?php

class Api_SiteMasterMeterController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();  
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
		$this->_userId=$auth->getIdentity()->user_id;
    }
    
    	public function checkNumAction(){
    	try{
    	$request=$this->getRequest();
    	
    	 $sim_no=$request->getParam("number");
    	 $site_id=$request->getParam("site_id");
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$check_site=$siteMasterMapper->checkSiteMasterBySiteId($site_id);
    	if($check_site){
			$checkno=$siteMasterMapper->getMasterMeterByNumber($sim_no,$site_id);
			if($checkno){
				$temp=0;
			}else{
				$temp=1;
			}
		}else{
    		$temp=0;
    	}
    	$meta = array(
    			"code" => 200,
    			"message" => "SUCCESS"
    	);
    	$arr = array(
    			"meta" => $meta,
    			"data" =>$temp
    	);
    	
    	
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
		public function editCheckNumAction(){
    	try{
    	$request=$this->getRequest();
    	
    	 $sim_no=$request->getParam("number");
    	 $site_id=$request->getParam("site_id");
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$check_site=$siteMasterMapper->checkSiteMasterBySiteId($site_id);
    	if($check_site){
			$checkno=$siteMasterMapper->geteditMasterMeterByNumber($sim_no,$site_id);
			if($checkno){
				$temp=0;
			}else{
				$temp=1;
			}
		}else{
    		$temp=0;
    	}
    	$meta = array(
    			"code" => 200,
    			"message" => "SUCCESS"
    	);
    	$arr = array(
    			"meta" => $meta,
    			"data" =>$temp
    	);
    	
    	
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
		protected function _smsNotification($number, $sms) {
		$sms_ori=$sms;
    	$number = substr($number, 0, 10);
    	//$number = 8285859194;
    	$sms = urlencode($sms);
    	$smsMapper =new Application_Model_SmsGatewayMapper();
    	$smsgateway = $smsMapper->getAllSmsGateway();
    	$user_name=$smsgateway[0]->__get("user_name");
    	$password=$smsgateway[0]->__get("password");
    
    	$url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=".$user_name."&auth_scheme=plain&password=".$password."&v=1.1&format=text";
    
    	  $text = file_get_contents($url);
    	
    	$string = "Number - ".$number;
    	$string .= "|";
    	$string .= "Message - ".$sms_ori;
    	$string .= "|";
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$string .= "Time - ".$zendDate->toString();
    	$file = 'mini.txt';
    	$current = file_get_contents($file);
    	$current .= $string ." | ".$text. "\r\n";
    	file_put_contents($file, $current);
    	
    	return $text;
    }
	
	    public function sendMsgAction(){
	    	try {
		    	
	    		$request=$this->getRequest();
		    	 
		    	$msg=$request->getParam("msg");
		    	$sim_no=$request->getParam("sim_no");
		    	$all_msg=explode(",", $msg);
		    	for ($i=0;$i<count($all_msg);$i++){
		    		$texts= $this->_smsNotification($sim_no, $all_msg[$i]);
		    		sleep(5);
		    	}
		    	 
		    	$meta = array(
		    			"code" => 200,
		    			"message" => "SUCCESS"
		    	);
		    	$arr = array(
		    			"meta" => $meta,
		    	);
		    	 
		    	 
		    	}catch (Exception $e) {
		    		$meta = array(
		    				"code" => 501,
		    				"messgae" => $e->getMessage()
		    		);
		    	
		    		$arr = array(
		    				"meta" => $meta
		    		);
		    	}
	    	$json = json_encode($arr, JSON_PRETTY_PRINT);
	    	echo $json;
	    }
    
        public function addSiteMeterAction(){
      
         try {
         	  $request=$this->getRequest();
         	 
        	 $site_id=$request->getParam("site_id");
        	 $sim_no=$request->getParam("sim_no");
             $masterMeter=$request->getParam("masterMeter");
             $description=$request->getParam("description");
			 $start_time1=  ($request->getParam("start_time1"));
			 $end_time1= ($request->getParam("end_time1")); 
			 $start_time2= ($request->getParam("start_time2"));
			 $end_time2= ($request->getParam("end_time2"));
			 $feederType=$request->getParam("feederType");
			 
			 $is_24_hr=$request->getParam("is_24_hr");
			 if($is_24_hr=='true'){
			 	$time=1;
			 }else {
			 	$time=0;
			 }
			 
			 if(($start_time1=='0.0' && $end_time1=='0.0')){
			 	$entry=1;
			 	 $timeArr=array($start_time2,$end_time2);
			 }elseif(($start_time2=='0.0' && $end_time2=='0.0')){
			 	$entry=1;
			 	$timeArr=array($start_time1,$end_time1);
			 }else{
			 	$entry=2;
			 	$timeArr=array($start_time1,$end_time1,$start_time2,$end_time2);
			 }
			 
             $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
             $siteMaster=new Application_Model_SiteMasterMeter();
             $sitesMapper=new Application_Model_SitesMapper();
             $sites=$sitesMapper->getSiteById($site_id);
             $j=0;
             $feedar1=array('A','B');
             $feedar2=array('C','D');
             $feedar3=array('E','F');
             $allMsg=array();
          		for($i=0;$i<$entry;$i++){
          			 
                    $siteMaster->__set("site_id",$site_id);
                    $siteMaster->__set("sim_no",$sim_no);
             		$siteMaster->__set("meter_name",strtoupper($masterMeter));
             		$siteMaster->__set("meter_keyword",$sites->__get("site_code").strtoupper($masterMeter));
             		$siteMaster->__set("description",$description);
					$siteMaster->__set("start_time",$timeArr[$j]);
					$siteMaster->__set("end_time",$timeArr[$j+1]);
					$siteMaster->__set("is_24_hr",$time);
					$siteMaster->__set("feeder_type",$feederType);
                    $Site_MM=$siteMasterMapper->addNewSiteMaster($siteMaster);
                    
							$start=explode(".", $timeArr[$j]);
		                    $end=explode(".", $timeArr[$j+1]);
		                    $startHH=sprintf("%02d",$start[0]);
		                    $startMM= sprintf("%02d",$start[1]);
		                    $endHH=sprintf("%02d",$end[0]);
		                    $endMM= sprintf("%02d",$end[1]);
		                    
		         				if(strtoupper($masterMeter)=='FEEDER1'){
					           		$msg="GRIDFDR".$feedar1[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}elseif (strtoupper($masterMeter)=='FEEDER2'){
					           		$msg="GRIDFDR".$feedar2[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}elseif (strtoupper($masterMeter)=='FEEDER3'){
					           		$msg="GRIDFDR".$feedar3[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}else{
					           		$msg="Invalid request";
					           	}
					           	$allMsg[]=$msg;
                     //$texts= $this->_smsNotification($sim_no, $msg);
                    $j=2;
          		}
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "msg"=>implode(",", $allMsg),
                	 
                	
                );
           
           
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
        }


        public function getSiteMeterBySiteIdAction(){
      
         try {
             $request=$this->getRequest();
             $site_id=$request->getParam("site_id");
             $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
            $siteMeter=$siteMasterMapper->getSiteMasterById($site_id);
            $sim_no="";
            if($siteMeter){ 
            	foreach($siteMeter as $siteMaster_val){
            	
            	
		         if(intval($siteMaster_val->__get("is_24_hr"))==1){
		         	$time=true;
		         }else{
		         	$time=false;
		         }
		         if($siteMaster_val->__get("sim_no")!=null && $siteMaster_val->__get("sim_no")!=""){
		         	$sim_no=$siteMaster_val->__get("sim_no");
		         }
		         $timming=array();
		          $start_time_hh1=00;$start_time_mm1=00; $end_time_hh1=00; $end_time_mm1=00; 
		          $start_time_hh2=00;$start_time_mm2=00;$end_time_hh2=00; $end_time_mm2=00;
		         if($siteMaster_val->__get("start_time")!=null){
			         $sitemeter_data=$siteMasterMapper->getMasterMeterByMeterName($siteMaster_val->__get("meter_keyword"));
			     
			        
			         if(count($sitemeter_data)>1){
			         	 
			         	    $start_time=explode(".", $sitemeter_data[0]["start_time"]);
			         		$end_time=explode(".", $sitemeter_data[0]["end_time"]);
			         		$id1=$sitemeter_data[0]["id"];
			         			$start_time_hh1=$start_time[0];
			         			$start_time_mm1 = $start_time[1];
			         			$end_time_hh1 = $end_time[0];
			         			$end_time_mm1 = $end_time[1];
			           
			         			$start_time1=explode(".", $sitemeter_data[1]["start_time"]);
			         			$end_time1=explode(".", $sitemeter_data[1]["end_time"]);
			         			$id2=$sitemeter_data[1]["id"];
			         			
			         			$start_time_hh2=$start_time1[0];
			         			$start_time_mm2 = $start_time1[1];
			         			$end_time_hh2 = $end_time1[0];
			         			$end_time_mm2 = $end_time1[1];
			         			 
			         }elseif(count($sitemeter_data)==1){
			         	
				         	$start_time=explode(".", $sitemeter_data[0]["start_time"]);
				         	$end_time=explode(".", $sitemeter_data[0]["end_time"]);
				         		
				         		$id1=$sitemeter_data[0]["id"];
			         			$start_time_hh1=$start_time[0];
			         			$start_time_mm1 = $start_time[1];
			         			$end_time_hh1 = $end_time[0];
			         			$end_time_mm1 = $end_time[1];
			         			
			         			$id2=0;
			         			$start_time_hh2=00;
			         			$start_time_mm2 = 00;
			         			$end_time_hh2 = 00;
			         			$end_time_mm2 = 00;
			         }
		         }
	             $data=array(
		         		"id1" => $id1,
	             		"id2" => $id2,
	             		"meter_name" => $siteMaster_val->__get("meter_name"),
		         		"meter_keyword" =>$siteMaster_val->__get("meter_keyword"),
	             		"description" =>$siteMaster_val->__get("description"),
						"is_24_hr" =>$time,
	             		"feeder_type" => $siteMaster_val->__get("feeder_type"),
	             		"start_time_hh1" =>$start_time_hh1,
	             		"start_time_mm1" =>$start_time_mm1,
	             		"end_time_hh1" =>$end_time_hh1,
	             		"end_time_mm1" =>$end_time_mm1,
	             		
	             		"start_time_hh2" =>$start_time_hh2,
	             		"start_time_mm2" =>$start_time_mm2,
	             		"end_time_hh2" =>$end_time_hh2,
	             		"end_time_mm2" =>$end_time_mm2,
	            	);
	             $site_arr[]=$data;
            	}
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $site_arr,
                	"sim_no"=>$sim_no
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while getting data"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
        public function deleteSiteMeterBySiteIdAction(){
      
         try {
             $request=$this->getRequest();
             $site_id=$request->getParam("site_id");
             
              $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
             
          		$enterprise=$siteMasterMapper->deleteSiteMeterBySiteId($site_id) ;
          		
              $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
             
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
		public function deleteSiteMeterByIdAction(){
      
         try {
             $request=$this->getRequest();
             $meter_name=$request->getParam("meter_name");
             $site_id=$request->getParam("site_id");
             
              $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
             
          		$enterprise=$siteMasterMapper->deleteSiteMeterBySiteIdMeter($meter_name, $site_id);
          		
              $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
             
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
        public function getAllSiteMeterAction(){
      
         try {
         	 $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
             
         	 $sitesMapper=new Application_Model_SitesMapper();
            
             
             
             $siteMasters=$siteMasterMapper->getAllSiteMeter();
          
         	if($siteMasters){
             foreach ($siteMasters as $siteMaster_val) {
             	$site_master_arr[]=$siteMaster_val->__get("site_id");
	         }
	         
	         $unique_site=array_unique($site_master_arr);
	         $arr_val=array_values($unique_site);
	       /*   $sites=$sitesMapper->getSiteById($siteMaster_val->__get("site_id"));
	         $data=array(
	         		"id" => $siteMaster_val->__get("id"),
	         		"site_id" => $siteMaster_val->__get("site_id"),
	         		"meter_name" => $siteMaster_val->__get("meter_name"),
	         		"meter_keyword" =>$siteMaster_val->__get("meter_keyword"),
	         		"site_name"=>$sites->__get("site_name")
	         ); */
	         for($i=0;$i<count($arr_val);$i++){
	         	$sites=$sitesMapper->getSiteById($arr_val[$i]);
	         	$data=array(
	         			"site_id" => $arr_val[$i],
	         			"site_name"=>$sites->__get("site_name")
	         	);
	         	$site_arr[]=$data;
	         }
	         $meta = array(
	         		"code" => 200,
	         		"message" => "SUCCESS"
	         );
	         $arr = array(
	         		"meta" => $meta,
	         		"data" => $site_arr,
	         );
            
            }
            else{
            	$meta = array(
            			"code" => 200,
            			"message" => "SUCCESS"
            	);
            	$arr = array(
            			"meta" => $meta,
            			"data" =>array(),
            	);
            }
            
            
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
        public function updateSiteMeterAction(){
      
         try {
             $request=$this->getRequest();
             $site_id=$request->getParam("site_id");
             $mater_meter=$request->getParam("masterMeter");
             $id1=$request->getParam("id1");
             $id2=$request->getParam("id2");
             $description=$request->getParam("description");
             $feeder_type=$request->getParam("feederType");

             $start_time1=   $request->getParam("start_time1") ;
			 $end_time1=  $request->getParam("end_time1") ;
			 $start_time2=  $request->getParam("start_time2") ;
			 $end_time2=   $request->getParam("end_time2") ;
			 
			 $is_24_hr=$request->getParam("is_24_hr");
			 $sim_no=$request->getParam("sim_no");
			 if($is_24_hr=='true'){
			 	$time=1;
			 }else {
			 	$time=0;
			 }
			 $timeArrs=array($start_time1,$end_time1,$start_time2,$end_time2);
			 $timeIds=array($id1,$id2);
			 if(($start_time1=='0.0' && $end_time1=='0.0')){
			 	$entry=1;
			 	$timeArr=array($start_time2,$end_time2);
			 	$timeId=array($id2);
			 }elseif(($start_time2=='0.0' && $end_time2=='0.0')){
			 	$entry=1;
			 	$timeId=array($id1);
			 	$timeArr=array($start_time1,$end_time1);
			 }else{
			 	$entry=2;
			 	$timeId=array($id1,$id2);
			 	$timeArr=array($start_time1,$end_time1,$start_time2,$end_time2);
			 }
			 
			 $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
             $siteMaster=new Application_Model_SiteMasterMeter();
             $sites=new Application_Model_SitesMapper();
             $feedar1=array('A','B');
             $feedar2=array('C','D');
             $feedar3=array('E','F');
             $site=$sites->getSiteById($site_id);
             $allMsg=array();
           
               if($id1=='undefined'){
               	$j=0;
               
               	for($i=0;$i<$entry;$i++){
	              	$siteMaster->__set("sim_no",$sim_no);
	              	$siteMaster->__set("site_id",$site_id);
	              	$siteMaster->__set("meter_name",strtoupper($mater_meter));
	              	$siteMaster->__set("meter_keyword",$site->__get("site_code").strtoupper($mater_meter));
	              	$siteMaster->__set("description",$description);
					$siteMaster->__set("start_time",$timeArr[$j]);
					$siteMaster->__set("end_time",$timeArr[$j+1]);
					$siteMaster->__set("is_24_hr",$time);
					$siteMaster->__set("feeder_type",$feeder_type);
	              	$Site_MM=$siteMasterMapper->addNewSiteMaster($siteMaster);
	              	
               		 $start=explode(".", $timeArr[$j]);
		                    $end=explode(".", $timeArr[$j+1]); 
		                    $startHH=sprintf("%02d",$start[0]);
		                    $startMM= sprintf("%02d",$start[1]);
		                    $endHH=sprintf("%02d",$end[0]);
		                    $endMM= sprintf("%02d",$end[1]);
		                    
               					if(strtoupper($mater_meter)=='FEEDER1'){
					           		$msg="GRIDFDR".$feedar1[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}elseif (strtoupper($mater_meter)=='FEEDER2'){
					           		$msg="GRIDFDR".$feedar2[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}elseif (strtoupper($mater_meter)=='FEEDER3'){
					           		$msg="GRIDFDR".$feedar3[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}else{
					           		$msg="Invalid request";
					           	}
					           	$allMsg[]=$msg;
	                	//$texts= $this->_smsNotification($sim_no, $msg);
	              	$j=2;
               	}	
	               
              }else{
              
              	 //$MMDatas=$siteMasterMapper->getMasterMeterByMeterSite(strtoupper($mater_meter),$site_id);
              	$j=0;
              	 for($i=0;$i<2;$i++){
              	 	if(($timeArrs[$j]=='0.0' && $timeArrs[$j+1]=='0.0') && $time==0){
              	 		if($timeIds[$i]!=0){
	              	 		$siteMasterMapper->deleteSiteMeterById($timeIds[$i]);
	              	 		 
              	 		}else{
              	 			 
              	 		}
              	 	}elseif(($timeIds[$i]!=0 && $timeIds[$i]!='undefined') || $time==1){
              	 	
              	 		$getbytime=$siteMasterMapper->getMasterMeterByStartEndTime($timeArrs[$j],$timeArrs[$j+1],$timeIds[$i]);
              	 	
              	 		if(!$getbytime || count($getbytime)>0){
              	 			
			              	$siteMaster->__set("site_id",$site_id);
			              	$siteMaster->__set("id",$timeIds[$i]);
			              	$siteMaster->__set("sim_no",$sim_no);
			              	$siteMaster->__set("meter_name",strtoupper($mater_meter));
			              	$siteMaster->__set("meter_keyword",$site->__get("site_code").strtoupper($mater_meter));
			              	$siteMaster->__set("description",$description);
							$siteMaster->__set("start_time",$timeArrs[$j]);
							$siteMaster->__set("end_time",$timeArrs[$j+1]);
							$siteMaster->__set("is_24_hr",$time);
							$siteMaster->__set("feeder_type",$feeder_type);
				            $enterprisePrice=$siteMasterMapper->updateSiteMeter($siteMaster); 
				           
								$start=explode(".", $timeArrs[$j]);
			                    $end=explode(".", $timeArrs[$j+1]);
			                    $startHH=sprintf("%02d",$start[0]);
			                    $startMM= sprintf("%02d",$start[1]);
			                    $endHH=sprintf("%02d",$end[0]);
			                    $endMM= sprintf("%02d",$end[1]);
			                    
	              	 				if(strtoupper($mater_meter)=='FEEDER1'){
						           		$msg="GRIDFDR".$feedar1[$i].$startHH.$startMM.";".$endHH.$endMM.";";
						           	}elseif (strtoupper($mater_meter)=='FEEDER2'){
						           		$msg="GRIDFDR".$feedar2[$i].$startHH.$startMM.";".$endHH.$endMM.";";
						           	}elseif (strtoupper($mater_meter)=='FEEDER3'){
						           		$msg="GRIDFDR".$feedar3[$i].$startHH.$startMM.";".$endHH.$endMM.";";
						           	}else{
						           		$msg="Invalid request";
						           	}
									//$texts= $this->_smsNotification($sim_no, $msg);
						           	$allMsg[]=$msg;
              	 		}    	
				           	$j=2;
			            	 
	              	}
              	 else{
              		    $siteMaster->__set("sim_no",$sim_no);
              			$siteMaster->__set("site_id",$site_id);
              			$siteMaster->__set("meter_name",strtoupper($mater_meter));
              			$siteMaster->__set("meter_keyword",$site->__get("site_code").strtoupper($mater_meter));
              			$siteMaster->__set("description",$description);
              			$siteMaster->__set("start_time",$timeArrs[$j]);
              			$siteMaster->__set("end_time",$timeArrs[$j+1]);
              			$siteMaster->__set("is_24_hr",$time);
              			$siteMaster->__set("feeder_type",$feederType);
              			$Site_MM=$siteMasterMapper->addNewSiteMaster($siteMaster);
              		
							$start=explode(".", $timeArrs[$j]);
		                    $end=explode(".", $timeArrs[$j+1]);
		                    $startHH=sprintf("%02d",$start[0]);
		                    $startMM= sprintf("%02d",$start[1]);
		                    $endHH=sprintf("%02d",$end[0]);
		                    $endMM= sprintf("%02d",$end[1]);
		                    
              	 				if(strtoupper($mater_meter)=='FEEDER1'){
					           		$msg="GRIDFDR".$feedar1[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}elseif (strtoupper($mater_meter)=='FEEDER2'){
					           		$msg="GRIDFDR".$feedar2[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}elseif (strtoupper($mater_meter)=='FEEDER3'){
					           		$msg="GRIDFDR".$feedar3[$i].$startHH.$startMM.";".$endHH.$endMM.";";
					           	}else{
					           		$msg="Invalid request";
					           	}
					           	$allMsg[]=$msg;
              			 	//$texts= $this->_smsNotification($sim_no, $msg);
              			$j=2;
              		 
              		 
              	 
              	 }
              	}
              }
              $siteMasterMapper->updateSiteNo($sim_no,$site_id);
              $meta = array(
              		"code" => 200,
              		"message" => "Add SUCCESS"
              );
              $arr = array(
              		"meta" => $meta,
              		"msg"=>implode(",", $allMsg)
              		 
              );
          } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT); 
        echo $json;
         }
		 
        public function getAllMasterMeterAction(){
         
         	try {
         		$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
         		 
         		$sitesMapper=new Application_Model_SitesMapper();
         		$stateMapper=new Application_Model_StatesMapper();
         
         		$request=$this->getRequest();
         		$status=$request->getParam("status");
         		 
         		 
         		$siteMasters=$siteMasterMapper->getAllSiteMeter($status);
          
         		if($siteMasters){
         			foreach ($siteMasters as $siteMaster_val) {
         				
         				$sites=$sitesMapper->getSiteById($siteMaster_val->__get("site_id"));
         				$states=$stateMapper->getStateById($sites->__get("state_id"));
         				
         				$data=array(
         						"site_id"=>$siteMaster_val->__get("site_id"),
         						"site_name"=>$sites->__get("site_name"),
         						"state_name"=>$states->__get("state_name"),
								"state_id"=>$states->__get("state_id"),
         						"meter_name"=>$siteMaster_val->__get("meter_name"),
         						"meter_keyword"=>$siteMaster_val->__get("meter_keyword"),
         						"id"=>$siteMaster_val->__get("id"),
         						"description"=>$siteMaster_val->__get("description"),
								"start_time"=>$siteMaster_val->__get("start_time"),
								"end_time"=>$siteMaster_val->__get("end_time"),
         						"feeder_type"=>$siteMaster_val->__get("feeder_type"),
         						"timestamp"=>$siteMaster_val->__get("timestamp"),
         				 	);
         				
         				$master_meter[]=$data;
						$feeder_type[]=(string)$siteMaster_val->__get("feeder_type");
         				$description[]=(string)$siteMaster_val->__get("description");
         			}
         			 
         			$unique_feeder_type = array_keys(array_flip($feeder_type));
         			$unique_description = array_keys(array_flip($description));
         			$data_feeder=array();$data_description=array();
         			for ($i=0;$i<count($unique_feeder_type);$i++){
         				$data=array(
         						"id"=>1,
         						"type"=>$unique_feeder_type[$i]
         				);
         				$data_feeder[]=$data;
         			}
         			for ($i=0;$i<count($unique_description);$i++){
         				$data=array(
         						"id"=>1,
         						"desc"=>$unique_description[$i]
         				);
         				$data_description[]=$data;
         			}
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $master_meter,
         					"feeder_type" => $data_feeder,
         					"description" => $data_description,
         					
         			);
         
         		}
         		else{
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" =>array(),
         			);
         		}
         
         
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
         
        public function addSiteMeterReadingAction(){
         
         	 try {
         	  $request=$this->getRequest();
        	 $site_id=$request->getParam("site_id");
             $meter_keyword=$request->getParam("meter_keyword");
             $reading_date=$request->getParam("reading_date");
             $reading=$request->getParam("reading");
			 $unit=$request->getParam("unit");
             $zendDate = new Zend_Date($reading_date,"dd-MM-yyyy");
             $reading_dates = $zendDate->toString("yyyy-MM-dd");
             
             $siteMasterMapper=new Application_Model_SiteMeterReadingMapper();
             $siteMaster=new Application_Model_SiteMeterReading();
             
             $checkReading=$siteMasterMapper->checkSiteReading($site_id, $meter_keyword,$reading_dates);
//              $next_reading=$checkReading["reading"];
//              $last_reading=$checkReading["last_reading"];
//              $equalReading=$checkReading["equalReading"];
//              if($equalReading==NULL){$equalReading=0;}
//              if($reading>$checkReading["reading"]){
// 		if($equalReading<$reading){
//              if(($next_reading==null && $last_reading==NULL)||($next_reading==NULL && $last_reading<$reading)||($next_reading>$reading && $last_reading==NULL)||($last_reading<$reading && $next_reading>$reading)){
             if($checkReading!=true){ 
              	    $sitesMapper=new Application_Model_SitesMapper();
                    $sites=$sitesMapper->getSiteById($site_id);
                    
                    $siteMaster->__set("site_id",$site_id); 
             		$siteMaster->__set("meter_id",$meter_keyword);
             		$siteMaster->__set("reading_date",$reading_dates);
             		$siteMaster->__set("reading",$unit);
					$siteMaster->__set("actual_reading",$reading);
					$siteMaster->__set("entry_by",$this->_userId);
					$siteMaster->__set("entry_type",'(W)');
                    $Site_MM=$siteMasterMapper->addNewSiteMeterReading($siteMaster);
	             
		            $data=array(  
		            	"site_id" => $site_id,
		            	"site_name" => $sites->__get("site_name"),
			            "meter_id" => $meter_keyword,
		            	"reading_date" => $reading_dates,
		            	"reading" => $reading,
		              );
		          
		             
		            $meta = array(
		                    "code" => 200,
		                    "message" => "SUCCESS"
		                );
		                $arr = array(
		                    "meta" => $meta,
		                     "data" => $data
		                );
           
             	}else{
             		$meta = array(
             				"code" => 100,
             				"message" => "Wrong Meter Reading"
             		);
             		$arr = array(
             				"meta" => $meta,
             		);
             	}
		   
			 
		  
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
        public function getAllSiteMeterReadingAction(){
         
         	try {
         		
         	    $siteMaster=new Application_Model_SiteMeterReadingMapper();
         	    $masterMeter=new Application_Model_SiteMasterMeterMapper();
         	    $sitesMapper=new Application_Model_SitesMapper();
         		$siteMeters=$siteMaster->getAllSiteMeterReading();
         
         		if($siteMeters){
         			foreach ($siteMeters as $siteMeter) {
         				$sites=$sitesMapper->getSiteById($siteMeter->__get("site_id"));
         				$meter=$masterMeter->getMMById($siteMeter->__get("meter_id"));
         				if($meter){
         					$meter_name=$meter->__get("meter_keyword");
         				}else{$meter_name='--Deleted--'; }
         				$zendDate = new Zend_Date($siteMeter->__get("reading_date"),"dd-MM-yyyy");
         				$reading_dates = $zendDate->toString("dd-MM-yyyy");
         				
         				$data=array(
         						"site_name"=>$sites->__get("site_name"),
         						"site_id"=>$siteMeter->__get("site_id"),
         						"meter_id"=>$siteMeter->__get("meter_id"),
         						"reading_date"=>$reading_dates,
         						"id"=>$siteMeter->__get("id"),
         						"reading"=>floatval($siteMeter->__get("reading")),
         						"meter_name"=>$meter_name
         				 	);
         				
         				$master_meter[]=$data;
         			}
         
         			
         			
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $master_meter,
         			);
         
         		}
         		else{
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" =>array(),
         			);
         		}
         
         
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
         
        public function deleteSiteReadingByIdAction(){
         
       try {
         		$request=$this->getRequest();
         		$id=$request->getParam("id");
         		 
         		 $siteMaster=new Application_Model_SiteMeterReadingMapper();
         		 
         		$siteReading=$siteMaster->deleteSiteReadingById($id) ;
         
         		$meta = array(
         				"code" => 200,
         				"message" => "SUCCESS"
         		);
         		$arr = array(
         				"meta" => $meta,
         
         		);
         		 
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
   }
 
		public function updateSiteReadingByIdAction(){
	
	try {
		$request=$this->getRequest();
		$site_id=$request->getParam("site_id");
 		$id=$request->getParam("id");
 		$meter_name=$request->getParam("meter_name");
		$reading_date=$request->getParam("reading_date");
		$reading=$request->getParam("reading");
		$zendDate = new Zend_Date($reading_date,"dd-MM-yyyy");
		$reading_dates = $zendDate->toString("yyyy-MM-dd");
	
		$siteReadingMapper=new Application_Model_SiteMeterReadingMapper();
		$siteReading=new Application_Model_SiteMeterReading();
		
		$checkReading=$siteReadingMapper->checkSiteReading($site_id, $meter_name,$reading_dates);
// 		$next_reading=$checkReading["reading"];
// 		$last_reading=$checkReading["last_reading"];
// 		$equalReading=$checkReading["equalReading"];
// 		//if($equalReading==NULL){$equalReading=0;}
		 
// 		//if($equalReading<$reading){
// 		if(($next_reading==null && $last_reading==NULL)||($next_reading==NULL && $last_reading<$reading)||($next_reading>$reading && $last_reading==NULL)||($last_reading<$reading && $next_reading>$reading)){
		if($checkReading){
		    $siteReading->__set("id",$checkReading["id"]);
			$siteReading->__set("site_id",$site_id);
			$siteReading->__set("reading_date",$reading_dates);
			$siteReading->__set("reading",$reading);
			$siteReading->__set("meter_id",$meter_name);
			$Site_MM=$siteReadingMapper->updateSiteReading($siteReading);
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
			);
		}else{
			$meta = array(
					"code" => 100,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
			
			);
		} 
	/*}else{
			$meta = array(
					"code" => 100,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
						
			);
	} */
		 
	} catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
	
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
	
	
}

	/*public function getSiteMeterReadingByMeterIdAction(){
	 
	try {
		$request=$this->getRequest();
		$site_id=$request->getParam("site_id");
		$siteMaster=new Application_Model_SiteMeterReadingMapper();
		$masterMeter=new Application_Model_SiteMasterMeterMapper();
		$sitesMapper=new Application_Model_SitesMapper();
		
		$lastDate = $request->getParam("firstDate");
		$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
		$lastDate = $zendDate->toString("yyyy-MM-dd");

		$currDate = $request->getParam("secondDate");
		$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
		$currDate = $zendDates->toString("yyyy-MM-dd");
		
		  
		$desc=array();
		$meter_name=array();
		$siteMeters=$masterMeter->getSiteMasterById($site_id);
	 	if($siteMeters){
			 foreach ($siteMeters as $siteMeter){
			 	$meter_name[]=$siteMeter->__get("meter_name");
			 }
			$allData=array();
		    $days=(strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24); 
		     
		    $next_date=$lastDate;
					$FeedarReadings=$siteMaster->getFeedarReadingBySiteIdDesc($site_id);
					$data_desc=array(
							"date"=>"",
							"FEEDER1"=>$FeedarReadings["description1"],
							"FEEDER2"=>$FeedarReadings["description2"],
							"FEEDER3"=>$FeedarReadings["description3"],
							"FEEDER4"=>$FeedarReadings["description4"],
							"FEEDER5"=>$FeedarReadings["description5"],
							"FEEDER6"=>$FeedarReadings["description6"],
							"FEEDER7"=>$FeedarReadings["description7"],
							"total"=>"",
					);
					$allData[]=$data_desc;
					
				if($days==0){
					$days=1;
				}else{
					$days=$days+1;
				}
				
				for($j=1;$j<=ceil($days);$j++){
					 
					$FeedarReadings=$siteMaster->getFeedarReadingBySiteIdDate($site_id,$next_date);
					if(floatval($FeedarReadings["FEEDER1"])!=0 || floatval($FeedarReadings["FEEDER2"])!=0 || floatval($FeedarReadings["FEEDER3"])!=0
							 || floatval($FeedarReadings["FEEDER4"])!=0 || floatval($FeedarReadings["FEEDER5"])!=0 || 
							 floatval($FeedarReadings["FEEDER6"])!=0 || floatval($FeedarReadings["FEEDER7"])!=0){
						$total=floatval($FeedarReadings["FEEDER1"]) + floatval($FeedarReadings["FEEDER2"]) + floatval($FeedarReadings["FEEDER3"])
								+ floatval($FeedarReadings["FEEDER4"])+floatval($FeedarReadings["FEEDER5"])+ floatval($FeedarReadings["FEEDER6"])+floatval($FeedarReadings["FEEDER7"]);
									
						$data=array(
							"date"=>date('d-M-Y', strtotime($next_date)),
							"FEEDER1"=>floatval($FeedarReadings["FEEDER1"]),
							"FEEDER2"=>floatval($FeedarReadings["FEEDER2"]),
							"FEEDER3"=>floatval($FeedarReadings["FEEDER3"]),
							"FEEDER4"=>floatval($FeedarReadings["FEEDER4"]),
							"FEEDER5"=>floatval($FeedarReadings["FEEDER5"]),
							"FEEDER6"=>floatval($FeedarReadings["FEEDER6"]),
							"FEEDER7"=>floatval($FeedarReadings["FEEDER7"]),
							"total"=>floatval($total),
					);
					$allData[]=$data;
					}
					$next="+1 day";
					$next_date = date('Y-m-d', strtotime($next_date .$next)); 	 
				}
		     
				$FeedarReadings=$siteMaster->getFeedarReadingBySiteIdDate($site_id,$lastDate,$currDate);
				
					$total_Value=floatval($FeedarReadings["FEEDER1"])+floatval($FeedarReadings["FEEDER2"])+floatval($FeedarReadings["FEEDER3"])+floatval($FeedarReadings["FEEDER4"])+
									floatval($FeedarReadings["FEEDER5"])+floatval($FeedarReadings["FEEDER6"])+floatval($FeedarReadings["FEEDER7"]);
					$data=array(
							"date"=>"Total",
							"FEEDER1"=>floatval($FeedarReadings["FEEDER1"]),
							"FEEDER2"=>floatval($FeedarReadings["FEEDER2"]),
							"FEEDER3"=>floatval($FeedarReadings["FEEDER3"]),
							"FEEDER4"=>floatval($FeedarReadings["FEEDER4"]),
							"FEEDER5"=>floatval($FeedarReadings["FEEDER5"]),
							"FEEDER6"=>floatval($FeedarReadings["FEEDER6"]),
							"FEEDER7"=>floatval($FeedarReadings["FEEDER7"]),
							"total"=>$total_Value ,
					);
					$allData[]=$data;
					
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" => $allData,
					"name"=>$meter_name
				 
			);
			 
		}
		else{
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" =>array(),
			);
		}
		 
		 
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
		 
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}*/
public function getSiteMeterReadingByMeterIdAction(){
	 
	try {
		$request=$this->getRequest();
		$site_id=$request->getParam("site_id");
		$siteMaster=new Application_Model_SiteMeterReadingMapper();
		$masterMeter=new Application_Model_SiteMasterMeterMapper();
		$sitesMapper=new Application_Model_SitesMapper();
		
		$lastDate = $request->getParam("firstDate");
		$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
		$lastDate = $zendDate->toString("yyyy-MM-dd");

		$currDate = $request->getParam("secondDate");
		$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
		$currDate = $zendDates->toString("yyyy-MM-dd");
		
		  
		$desc=array();
		$meter_id=array();$Namearray=array();$existing_array=array();$description=array();$total_unit=array();$feeder_unit_val=0;
		$siteMeters=$masterMeter->getSiteMasterById($site_id);
	 	if($siteMeters){
	 		$Namearray[]="Date";
	 		$total_unit[]="Total";
			 foreach ($siteMeters as $siteMeter){
			 	$meter_id[]=$siteMeter->__get("id");
			 	$Namearray[]=$siteMeter->__get("meter_name");
			 	$description[]=$siteMeter->__get("description");
			 	$feeder_unit=sprintf("%.2f",($siteMaster->getFeedarReadingByFeederIdDate($siteMeter->__get("id"),$lastDate,$currDate)));
			 	$total_unit[]=$feeder_unit;
			 	$feeder_unit_val=$feeder_unit_val+$feeder_unit;
			 }
			 $total_unit[]=$feeder_unit_val;
			 $Namearray[]="Total";
			 
				$allData=array();
			    $days=intval((strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24));
			    $next_date=$lastDate;
			    if($days==0){
			    	$days=1;
			    }else{
			    	$days=$days+1;
			    }	
			    	 
				for($j=1;$j<=ceil($days);$j++){
					$existing_array=array();
					 
						$existing_array[]=date("d-M-Y",strtotime($next_date));
						
						$oneDayTotal=0;
						
						foreach ($siteMeters as $siteMeter){
							$FeedarReadings=$siteMaster->getFeedarReadingByDateVal($site_id,$next_date,$siteMeter->__get("id"));
							$oneDayTotal=$oneDayTotal+floatval($FeedarReadings);
							$existing_array[]=sprintf("%.2f",($FeedarReadings));
						}
						$existing_array[]=sprintf("%.2f",($oneDayTotal));
						if(floatval($oneDayTotal)>0){
							$allData[]=$existing_array;
						}
						
					$next="+1 day";
					$next_date = date('Y-m-d', strtotime($next_date .$next)); 	 
				}

			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" => $allData,
					"total_unit"=>$total_unit,
					"name"=>$Namearray,
					"description"=>$description
			);
			 
		}
		else{
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" =>array(),
			);
		}
		 
		 
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
		 
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}
		public function updateFeederStatusByIdAction(){

		try {
		$request=$this->getRequest();
		$id=$request->getParam("id");
		$status=$request->getParam("status");
		$remark=$request->getParam("remark");
 
		$Feedermapper=new Application_Model_SiteMasterMeterMapper();

		if($Feeder=$Feedermapper->updateFeedersById($id,$status,$remark, $this->_userId)){
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,

			);
		} else {
			$meta = array(
					"code" => 401,
					"message" => "Error while updating"
			);
			$arr = array(
					"meta" => $meta
			);
		}
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
		 
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}
		
		public function getMasterMeterByIdAction(){
			try {
				$request = $this->getRequest();
				$id = $request->getParam("id");
		
				$masterMeterMapper = new Application_Model_SiteMasterMeterMapper();
				$result = $masterMeterMapper->getMMById($id); 
				// print_r($result);
				$data=array(
						"meter_name"=>$result->__get("meter_name"),
						"site_id"=>$result->__get("site_id"),
						"meter_keyword"=>$result->__get("meter_keyword"),
						"description"=>$result->__get("description"),
						"start_time"=>$result->__get("start_time"),
						"end_time"=>$result->__get("end_time"),
						"is_24_hr"=>$result->__get("is_24_hr"),
				);
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data,
				);
			}catch (Exception $e) {
				$meta = array(
						"code" => 501,
						"messgae" => $e->getMessage()
				);
		
				$arr = array(
						"meta" => $meta
				);
			}
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		}

		public function getMasterMeterReadingByDateAction(){
			try {
				$request = $this->getRequest();
				$reading_date = $request->getParam("date");
				$meter_id = $request->getParam("meter_id");
				$site_id = $request->getParam("site_id");
				
				$zendDate = new Zend_Date($reading_date,"dd-MM-yyyy");
				$reading_date = $zendDate->toString("yyyy-MM-dd");
				
				$siteMasterReadingMapper=new Application_Model_SiteMeterReadingMapper();
				$feeders = $siteMasterReadingMapper->getLastReadingByMeterId($meter_id,$reading_date,$site_id); 
				 if($feeders){
					 $lastReading=$feeders["actual_reading"];
					$meta = array(
							"code" => 200,
							"message" => "SUCCESS"
					);
					$arr = array(
							"meta" => $meta,
							"data" =>  floatval($lastReading),
					);
				}
			}catch (Exception $e) {
				$meta = array(
						"code" => 501,
						"messgae" => $e->getMessage()
				);
		
				$arr = array(
						"meta" => $meta
				);
			}
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		}
		
		public function getSiteMeterBySiteIdDateAction(){
		
			try {
				$request=$this->getRequest();
				$site_id=$request->getParam("site_id");
				$feedar_date=$request->getParam("feedar_date");
				$zendDates = new Zend_Date($feedar_date,"dd-MMM-YYYY");
				$feedar_date = $zendDates->toString("yyyy-MM-dd");
				$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
				$masterMeter=new Application_Model_SiteMasterMeterMapper();
				$sitesMapper=new Application_Model_SitesMapper();
		 
				$data_arr=array();
				$siteMeters=$feedarReadingMapper->getFeedarReadingBySiteIdReadingDate($site_id,$feedar_date);
				if($siteMeters){
				 	foreach ($siteMeters as $siteMeter){
				 			$readingStatus=$feedarReadingMapper->getFeedarReadingByTwoSideCheck($siteMeter["meter_id"],$feedar_date);
				 			$data=array(
				 					"feedar_detail"=>$siteMeter["meter_name"]." (".$siteMeter["description"].")"." (Unit : ".$siteMeter["reading"].")",
				 					"reading"=>floatval(sprintf("%.2f",($siteMeter["actual_reading"]))),
				 					"feedar_id"=>$siteMeter["meter_id"],
				 					"last_reading"=>floatval($readingStatus["last_reading"]),
				 					"next_reading"=>floatval($readingStatus["next_reading"]),
				 			);
				 			$data_arr[]=$data;
				 	}
					$meta = array(
							"code" => 200,
							"message" => "SUCCESS"
					);
					$arr = array(
							"meta" => $meta,
							"data" => $data_arr,
							 
					);
		
				}
				else{
					$meta = array(
							"code" => 200,
							"message" => "SUCCESS"
					);
					$arr = array(
							"meta" => $meta,
							"data" =>array(),
					);
				}
					
					
			}catch (Exception $e) {
				$meta = array(
						"code" => 501,
						"messgae" => $e->getMessage()
				);
					
				$arr = array(
						"meta" => $meta
				);
			}
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		}	
		
	public function updateFeederbyDateAndIdAction(){
		
			try {
				$request=$this->getRequest();
				$id=$request->getParam("id");
				$reading=sprintf("%.2f",($request->getParam("reading")));
				$unit=sprintf("%.2f",($request->getParam("unit")));
				$date=$request->getParam("date");
				
				$zendDates = new Zend_Date($date,"dd-MMM-YYYY");
				$date = $zendDates->toString("yyyy-MM-dd");
				
				$Feedermapper=new Application_Model_SiteMasterMeterMapper();
		
				if($Feeder=$Feedermapper->updateFeedersByDateAndId($id,$reading,$unit,$date)){
					$meta = array(
							"code" => 200,
							"message" => "SUCCESS"
					);
					$arr = array(
							"meta" => $meta,
		
					);
				} else {
					$meta = array(
							"code" => 401,
							"message" => "Error while updating"
					);
					$arr = array(
							"meta" => $meta
					);
				}
			}catch (Exception $e) {
				$meta = array(
						"code" => 501,
						"messgae" => $e->getMessage()
				);
					
				$arr = array(
						"meta" => $meta
				);
			}
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
	}
}
 
 