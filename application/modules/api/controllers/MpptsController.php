<?php

class Api_MpptsController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
		$this->_userId=$auth->getIdentity()->user_id;
    }
    
        public function addMpptsAction(){
      
         try {
         	  $request=$this->getRequest();
        	 $site_id=$request->getParam("site_id");
             $mppts_val=$request->getParam("mppts");
             $description=$request->getParam("description");
             $mpptsMapper=new Application_Model_MpptsMapper();
             $mppts=new Application_Model_Mppts();
             $sitesMapper=new Application_Model_SitesMapper();
             $sites=$sitesMapper->getSiteById($site_id);
            
                    $mppts->__set("site_id",$site_id);
             		$mppts->__set("mppt_name",strtoupper($mppts_val));
             		$mppts->__set("mppt_keyword",$sites->__get("site_code").strtoupper($mppts_val));
             		$mppts->__set("description",$description);
                    $mppt=$mpptsMapper->addNewMppts($mppts);
            
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     
                );
           
           
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
        }

        
        
      public function getMpptsBySiteIdAction(){
      
         try {
             $request=$this->getRequest();
             $site_id=$request->getParam("site_id");
             $MpptsMapper=new Application_Model_MpptsMapper();
            $Mppts=$MpptsMapper->getMpptsBySiteId($site_id);
            if($Mppts){ 
            	foreach($Mppts as $Mppts_val){
         	 
	             $data=array(
		         		"id" => $Mppts_val->__get("id"),
		         		"mppt_name" => $Mppts_val->__get("mppt_name"),
		         		"mppt_keyword" =>$Mppts_val->__get("mppt_keyword"),
	             		"description" =>$Mppts_val->__get("description"),
	            	);
	             $site_arr[]=$data;
            	}
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $site_arr
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while getting data"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function deleteMpptsBySiteIdAction(){
      
         try {
             $request=$this->getRequest();
             $site_id=$request->getParam("site_id");
             
              $MpptsMapper=new Application_Model_MpptsMapper();
             
          		$enterprise=$MpptsMapper->deleteMpptsBySiteId($site_id) ;
          		
              $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
             
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
 public function deleteMpptsByIdAction(){
      
         try {
             $request=$this->getRequest();
             $id=$request->getParam("id");
             
              $MpptsMapper=new Application_Model_MpptsMapper();
             
          		$enterprise=$MpptsMapper->deleteMpptsById($id) ;
          		
              $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
             
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         
         public function getAllMpptsSiteIdAction(){
      
         try {
         	 $MpptsMapper=new Application_Model_MpptsMapper();
             
         	 $sitesMapper=new Application_Model_SitesMapper();
            
             
             
             $Mppts=$MpptsMapper->getAllMppts();
          
         	if($Mppts){
             foreach ($Mppts as $Mppts_val) {
             	$site_master_arr[]=$Mppts_val->__get("site_id");
	         }
	         
	         $unique_site=array_unique($site_master_arr);
	         $arr_val=array_values($unique_site);
	       
	         for($i=0;$i<count($arr_val);$i++){
	         	$sites=$sitesMapper->getSiteById($arr_val[$i]);
	         	$data=array(
	         			"site_id" => $arr_val[$i],
	         			"site_name"=>$sites->__get("site_name")
	         	);
	         	$site_arr[]=$data;
	         }
	         $meta = array(
	         		"code" => 200,
	         		"message" => "SUCCESS"
	         );
	         $arr = array(
	         		"meta" => $meta,
	         		"data" => $site_arr,
	         );
            
            }
            else{
            	$meta = array(
            			"code" => 200,
            			"message" => "SUCCESS"
            	);
            	$arr = array(
            			"meta" => $meta,
            			"data" =>array(),
            	);
            }
            
            
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
      public function updateMpptsAction(){
      
         try {
             $request=$this->getRequest();
             $site_id=$request->getParam("site_id");
             $mppts=$request->getParam("mppts");
             $id=$request->getParam("id");
             $description=$request->getParam("description");
              
			 $MpptsMapper=new Application_Model_MpptsMapper();
             $MpptsMaster=new Application_Model_Mppts();
             $sites=new Application_Model_SitesMapper();
             
             $site=$sites->getSiteById($site_id);
             
               if($id=='undefined'){
	              	$MpptsMaster->__set("site_id",$site_id);
	              	$MpptsMaster->__set("mppt_name",strtoupper($mppts));
	              	$MpptsMaster->__set("mppt_keyword",$site->__get("site_code").strtoupper($mppts));
	              	$MpptsMaster->__set("description",$description);
	              	$Site_MM=$MpptsMapper->addNewMppts($MpptsMaster);
	              	$meta = array(
	              			"code" => 200,
	              			"message" => "SUCCESS"
	              	);
	              	$arr = array(
	              			"meta" => $meta,
	              	
	              	);
              }else{
	              	$MpptsMaster->__set("site_id",$site_id);
	              	$MpptsMaster->__set("id",$id);
	              	$MpptsMaster->__set("mppt_name",strtoupper($mppts));
	              	$MpptsMaster->__set("mppt_keyword",$site->__get("site_code").strtoupper($mppts));
	              	$MpptsMaster->__set("description",$description);
		           if($enterprisePrice=$MpptsMapper->updateMppts($MpptsMaster)){
			           
		            	$meta = array(
		                    "code" => 200,
		                    "message" => "SUCCESS"
		                );
		                $arr = array(
		                    "meta" => $meta,
		                    
		                );
		            } else {
		                $meta = array(
		                    "code" => 401,
		                    "message" => "Error while updating"
		                );
		                $arr = array(
		                    "meta" => $meta
		                );
		            }
              }
          } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }

    public function getAllMpptsAction(){
         
         	try {
         		$siteMasterMapper=new Application_Model_MpptsMapper();
         		 
         		$sitesMapper=new Application_Model_SitesMapper();
         		$stateMapper=new Application_Model_StatesMapper();
         		$request=$this->getRequest();
         		$status=$request->getParam("status");
         		 
         		$siteMasters=$siteMasterMapper->getAllMppts($status);
         
         		if($siteMasters){
         			foreach ($siteMasters as $siteMaster_val) {
         				
         				$sites=$sitesMapper->getSiteById($siteMaster_val->__get("site_id"));
         				$states=$stateMapper->getStateById($sites->__get("state_id"));
         				$data=array(
         						"site_id"=>$siteMaster_val->__get("site_id"),
         						"site_name"=>$sites->__get("site_name"),
         						"state_name"=>$states->__get("state_name"),
								"state_id"=>$states->__get("state_id"), 
         						"mppt_name"=>$siteMaster_val->__get("mppt_name"),
         						"mppt_keyword"=>$siteMaster_val->__get("mppt_keyword"),
         						"id"=>$siteMaster_val->__get("id"),
         						"description"=>$siteMaster_val->__get("description"),
         						"timestamp"=>$siteMaster_val->__get("timestamp"),
         				 	);
         				$master_meter[]=$data;
         			}
         
         			
         			
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $master_meter,
         			);
         
         		}
         		else{
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" =>array(),
         			);
         		}
         
         
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
         
         
         public function addMpptsReadingAction(){
         
         	 try {
         	  $request=$this->getRequest();
        	 $site_id=$request->getParam("site_id");
             $meter_keyword=$request->getParam("mppt_keyword");
             $reading_date=$request->getParam("reading_date");
             $reading=$request->getParam("reading");
             $zendDate = new Zend_Date($reading_date,"dd-MM-yyyy");
             $reading_dates = $zendDate->toString("yyyy-MM-dd");
             
             $mpptsReadingMapper=new Application_Model_MpptsReadingMapper();
             $siteMaster=new Application_Model_MpptsReading();
             
             $checkReading=$mpptsReadingMapper->checkSiteReading($site_id, $meter_keyword,$reading_dates);

             if($checkReading!=true){
              	    $sitesMapper=new Application_Model_SitesMapper();
                    $sites=$sitesMapper->getSiteById($site_id);
                    
                    $siteMaster->__set("site_id",$site_id);
             		$siteMaster->__set("mppt_id",$meter_keyword);
             		$siteMaster->__set("reading_date",$reading_dates);
             		$siteMaster->__set("reading",$reading);
					$siteMaster->__set("entry_by",$this->_userId);
             		$siteMaster->__set("entry_type",'(W)');
                    $Site_MM=$mpptsReadingMapper->addNewMpptsReading($siteMaster);
			
		            $data=array(  
		            	"site_id" => $site_id,
		            	"site_name" => $sites->__get("site_name"),
			            "meter_id" => $meter_keyword,
		            	"reading_date" => $reading_dates,
		            	"reading" => $reading,
		              );
		          
		             
		            $meta = array(
		                    "code" => 200,
		                    "message" => "SUCCESS"
		                );
		                $arr = array(
		                    "meta" => $meta,
		                     "data" => $data
		                );
           
             	}else{
             		$meta = array(
             				"code" => 100,
             				"message" => "Wrong Meter Reading"
             		);
             		$arr = array(
             				"meta" => $meta,
             		);
             	}
		   
			 
		  
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         
         
         
public function getAllMpptsReadingAction(){
         
         	try {
         		
         	    $siteMaster=new Application_Model_MpptsReadingMapper();
         	    $masterMeter=new Application_Model_MpptsMapper();
         	    $sitesMapper=new Application_Model_SitesMapper();
         		$siteMeters=$siteMaster->getAllMpptsReading();
         
         		if($siteMeters){
         			foreach ($siteMeters as $siteMeter) {
         				$sites=$sitesMapper->getSiteById($siteMeter->__get("site_id"));
         				$meter=$masterMeter->getMpptsById($siteMeter->__get("mppt_id"));
         				if($meter){
         					$meter_name=$meter->__get("mppt_keyword");
         				}else{$meter_name='--Deleted--'; }
         				$zendDate = new Zend_Date($siteMeter->__get("reading_date"),"dd-MM-yyyy");
         				$reading_dates = $zendDate->toString("dd-MM-yyyy");
         				
         				$data=array(
         						"site_name"=>$sites->__get("site_name"),
         						"site_id"=>$siteMeter->__get("site_id"),
         						"mppt_id"=>$siteMeter->__get("mppt_id"),
         						"reading_date"=>$reading_dates,
         						"id"=>$siteMeter->__get("id"),
         						"reading"=>floatval($siteMeter->__get("reading")),
         						"mppt_name"=>$meter_name
         				 	);
         				
         				$master_meter[]=$data;
         			}
         
         			
         			
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $master_meter,
         			);
         
         		}
         		else{
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" =>array(),
         			);
         		}
         
         
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
         
         
   
   public function deleteMpptsReadingByIdAction(){
         
       try {
         		$request=$this->getRequest();
         		$id=$request->getParam("id");
         		 
         		 $siteMaster=new Application_Model_MpptsReadingMapper();
         		 
         		$siteReading=$siteMaster->deleteMpptReadingById($id) ;
         
         		$meta = array(
         				"code" => 200,
         				"message" => "SUCCESS"
         		);
         		$arr = array(
         				"meta" => $meta,
         
         		);
         		 
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
   }
 
public function updateMpptsReadingByIdAction(){
	
	try {
		$request=$this->getRequest();
		$site_id=$request->getParam("site_id");
 		$id=$request->getParam("id");
 		$meter_name=$request->getParam("mppt_name");
		$reading_date=$request->getParam("reading_date");
		$reading=$request->getParam("reading");
		$zendDate = new Zend_Date($reading_date,"dd-MM-yyyy");
		$reading_dates = $zendDate->toString("yyyy-MM-dd");
	
		$siteReadingMapper=new Application_Model_MpptsReadingMapper();
		$siteReading=new Application_Model_MpptsReading();
		
		$checkReading=$siteReadingMapper->checkSiteReading($site_id, $meter_name,$reading_dates);
 	if($checkReading!=false){
		    $siteReading->__set("id",$checkReading["id"]);
			$siteReading->__set("site_id",$site_id);
			$siteReading->__set("reading_date",$reading_dates);
			$siteReading->__set("reading",$reading);
			$siteReading->__set("mppt_id",$meter_name);
			$Site_MM=$siteReadingMapper->updateMpptsReading($siteReading);
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
	
			);
		}else{
			$meta = array(
					"code" => 100,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
			
			);
		} 
	 
		 
	} catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
	
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
	
	
}

/*public function getMpptsReadingByMpptIdAction(){
	 
	try {
		$request=$this->getRequest();
		$meter_id=$request->getParam("mppt_id");
		$siteMaster=new Application_Model_MpptsReadingMapper();
		$masterMeter=new Application_Model_MpptsMapper();
		$sitesMapper=new Application_Model_SitesMapper();
		
		$siteMeters=$siteMaster->getMpptsReadingByMeterId($meter_id);
		$mppts=array();
		if($siteMeters){
			$site_id=$siteMeters[0][0]["site_id"];
			for ($i=0;$i<count($siteMeters);$i++) {
				if($i==0){
					$lastTimestamp=0;
				} else{
					$lastTimestamp=$siteMeters[$i-1][count($siteMeters[$i-1])-1]["mppt_reading"];
				}
				$data=array();
				for($j=0;$j<count($siteMeters[$i]);$j++){
					
					$data[]=$siteMeters[$i][$j]["mppt_reading"];
					
					$timestamp=$siteMeters[$i][$j]["timestamp"];
					$mppt_reading=$siteMeters[$i][$j]["mppt_reading"];
				}
				if(count($data)>0){
					$maxi=max($data);
					$maximum_value=($maxi-$lastTimestamp)+$mppt_reading;
					 
					$meter=$masterMeter->getMpptsById($meter_id);
					 $sites=$sitesMapper->getSiteById($site_id);
					 if($meter){
					 	$meter_name=$meter->__get("mppt_keyword");
					 }else{$meter_name='--Deleted--'; }
  						 $reading_dates = date('Y-m-d', strtotime($timestamp));
						 $datas=array(
							 "site_name"=>$sites->__get("site_name"),
							 "site_id"=>$site_id,
							 "mppt_id"=>$meter_id,
							 "reading_date"=>$reading_dates,
							 "reading"=>floatval($maximum_value),
							 "mppt_name"=>$meter_name
						 ); 
				}
				$mppts[]=$datas;
				$sort_arr = array();
				foreach ($mppts as $key => $row)
				{
					$sort_arr[$key] = $row['reading_date'];
				}
				array_multisort($sort_arr, SORT_DESC ,$mppts);
			}
			
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" => $mppts,
			);
			 
		}
		else{
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" =>array(),
			);
		}
		 
		 
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
		 
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}*/

/*public function getMpptsReadingByMpptIdAction(){
	 
	try {
		$request=$this->getRequest();
		$site_id=$request->getParam("site_id");
		$siteMaster=new Application_Model_MpptsReadingMapper();
		$masterMeter=new Application_Model_MpptsMapper();
		$sitesMapper=new Application_Model_SitesMapper();
		
		$lastDate = $request->getParam("firstDate");
		$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
		$lastDate = $zendDate->toString("yyyy-MM-dd");
		
		$currDate = $request->getParam("secondDate");
		$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
		$currDate = $zendDates->toString("yyyy-MM-dd");
		
		$siteMeters=$siteMaster->getMpptsById($site_id);
		$mppts=array();
		if($siteMeters){
			 
			$allData=array(); 
		    $days=ceil((strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24));
		    $next_date=$lastDate;
			if($days==0){
				$days=1;
			}else{
				$days=$days+1;
			}
				for($j=1;$j<=($days);$j++){
					 
					$FeedarReadings=$siteMaster->getTotalMpptReadingBySiteDate($site_id,$next_date);
					if(floatval($FeedarReadings["MPPT1"])!=0 || floatval($FeedarReadings["MPPT2"])!=0 || floatval($FeedarReadings["MPPT3"])!=0
					 || floatval($FeedarReadings["MPPT4"])!=0 || floatval($FeedarReadings["MPPT5"])!=0 || floatval($FeedarReadings["MPPT6"])!=0
							|| floatval($FeedarReadings["MPPT7"])!=0 || floatval($FeedarReadings["MPPT8"])!=0
					 || floatval($FeedarReadings["MPPT9"])!=0 || floatval($FeedarReadings["MPPT10"])!=0 || floatval($FeedarReadings["BIOGAS1"])!=0
					 || floatval($FeedarReadings["DG"])!=0){
						
						$total=floatval($FeedarReadings["MPPT1"]) + floatval($FeedarReadings["MPPT2"]) + floatval($FeedarReadings["MPPT3"])
							+ floatval($FeedarReadings["MPPT4"])+floatval($FeedarReadings["MPPT5"]) +floatval($FeedarReadings["MPPT6"]) + 
							floatval($FeedarReadings["MPPT7"]) + floatval($FeedarReadings["MPPT8"])
							+ floatval($FeedarReadings["MPPT9"])+floatval($FeedarReadings["MPPT10"])+ floatval($FeedarReadings["DG"]) + floatval($FeedarReadings["BIOGAS1"]);
						$data=array( 
							"date"=>date('d-M-Y', strtotime($next_date)),
							"MPPT1"=>floatval($FeedarReadings["MPPT1"]),
							"MPPT2"=>floatval($FeedarReadings["MPPT2"]),
							"MPPT3"=>floatval($FeedarReadings["MPPT3"]),
							"MPPT4"=>floatval($FeedarReadings["MPPT4"]),
							"MPPT5"=>floatval($FeedarReadings["MPPT5"]),
							"MPPT6"=>floatval($FeedarReadings["MPPT6"]),
							"MPPT7"=>floatval($FeedarReadings["MPPT7"]),
							"MPPT8"=>floatval($FeedarReadings["MPPT8"]),
							"MPPT9"=>floatval($FeedarReadings["MPPT9"]),
							"MPPT10"=>floatval($FeedarReadings["MPPT10"]),
							"DG"=>floatval($FeedarReadings["DG"]),
							"BIOGAS1"=>floatval($FeedarReadings["BIOGAS1"]),
							"total"=>floatval($total),
					);
					$allData[]=$data;
					}
					$next="+1 day";
					$next_date = date('Y-m-d', strtotime($next_date .$next)); 	 
				}
		     
				$FeedarReadings=$siteMaster->getTotalMpptReadingBySiteDate($site_id,$lastDate,$currDate);
					$total_Value=floatval($FeedarReadings["MPPT1"])+floatval($FeedarReadings["MPPT2"])+floatval($FeedarReadings["MPPT3"])+floatval($FeedarReadings["MPPT4"])+
									floatval($FeedarReadings["MPPT5"])+floatval($FeedarReadings["MPPT6"])+floatval($FeedarReadings["MPPT7"])+floatval($FeedarReadings["MPPT8"])
									+floatval($FeedarReadings["MPPT9"])+floatval($FeedarReadings["MPPT10"])+floatval($FeedarReadings["DG"])+floatval($FeedarReadings["BIOGAS1"]); 
					$data=array(
							"date"=>"Total",
							"MPPT1"=>floatval($FeedarReadings["MPPT1"]),
							"MPPT2"=>floatval($FeedarReadings["MPPT2"]),
							"MPPT3"=>floatval($FeedarReadings["MPPT3"]),
							"MPPT4"=>floatval($FeedarReadings["MPPT4"]),
							"MPPT5"=>floatval($FeedarReadings["MPPT5"]),
							"MPPT6"=>floatval($FeedarReadings["MPPT6"]),
							"MPPT7"=>floatval($FeedarReadings["MPPT7"]),
							"MPPT8"=>floatval($FeedarReadings["MPPT8"]),
							"MPPT9"=>floatval($FeedarReadings["MPPT9"]),
							"MPPT10"=>floatval($FeedarReadings["MPPT10"]),
							"DG"=>floatval($FeedarReadings["DG"]),
							"BIOGAS1"=>floatval($FeedarReadings["BIOGAS1"]), 
							"total"=>$total_Value,
					);
					$allData[]=$data;
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" => $allData,
			);
			 
		}
		else{
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" =>array(),
			);
		}
		 
		 
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
		 
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}*/

public function getMpptsReadingByMpptIdAction(){
	 
	try {
		$request=$this->getRequest();
		$site_id=$request->getParam("site_id");
		$siteMaster=new Application_Model_MpptsReadingMapper();
		$masterMeter=new Application_Model_MpptsMapper();
		$sitesMapper=new Application_Model_SitesMapper();
		
		$lastDate = $request->getParam("firstDate");
		$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
		$lastDate = $zendDate->toString("yyyy-MM-dd");
		
		$currDate = $request->getParam("secondDate");
		$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
		$currDate = $zendDates->toString("yyyy-MM-dd");
		
		//$siteMeters=$siteMaster->getMpptsById($site_id);
		$desc=array();$meter_id=array();$Namearray=array();$existing_array=array();
		$description=array();$total_unit=array();$feeder_unit_val=0;
		$siteMeters=$masterMeter->getMpptsBySiteId($site_id);
	 	if($siteMeters){
	 		$Namearray[]="Date";
			$NamearrayMppt=array();$NamearrayOther=array();$mppt_val=0;$other_val=0;
	 		$total_unit[]="Total";$total_unit_mppt=array();$total_unit_other=array();
			foreach ($siteMeters as $siteMeter){
					$meter_id[]=$siteMeter->__get("id");
					
					$description[]=$siteMeter->__get("description");
					$feeder_unit=sprintf("%.2f",($siteMaster->getmpptReadingByFeederIdDate($siteMeter->__get("id"),$lastDate,$currDate)));
					
					if (strpos($siteMeter->__get("mppt_name"), 'MPPT') !== false){
						$total_unit_mppt[]=$feeder_unit; 
						$NamearrayMppt[]=$siteMeter->__get("mppt_name");
						$mppt_val=$mppt_val+$feeder_unit;
					}else{
						$total_unit_other[]=$feeder_unit; 
						$NamearrayOther[]=$siteMeter->__get("mppt_name");
						$other_val=$other_val+$feeder_unit;
					}
					$feeder_unit_val=$feeder_unit_val+$feeder_unit;
			}
			
				$total_unit=array_merge($total_unit,$total_unit_mppt);
				$total_unit[]=sprintf("%.2f",($mppt_val));
				$total_unit=array_merge($total_unit,$total_unit_other);
				$total_unit[]=sprintf("%.2f",($other_val)); 
				$total_unit[]=$feeder_unit_val;
				 
				$Namearray=array_merge($Namearray,$NamearrayMppt);
				$Namearray[]="Mppt Sub Total";
				$Namearray=array_merge($Namearray,$NamearrayOther);
				$Namearray[]="Others Sub Total";
				$Namearray[]="Total";
			 
				$allData=array();
			    $days=intval((strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24));
			    $next_date=$lastDate;
			    if($days==0){
			    	$days=1;
			    }else{
			    	$days=$days+1;
			    }
			    	 
				for($j=1;$j<=ceil($days);$j++){
					$existing_array=array();$existing_array_mppt=array();$existing_array_other=array();
					 
						$existing_array[]=date("d-M-Y",strtotime($next_date));
						
						$oneDayTotal=0;
						$mppt_vals=0;$other_vals=0;
						foreach ($siteMeters as $siteMeter){
							$FeedarReadings=$siteMaster->getFeedarReadingByDateVal($site_id,$next_date,$siteMeter->__get("id"));
							
							if (strpos($siteMeter->__get("mppt_name"), 'MPPT') !== false){
								$existing_array_mppt[]=sprintf("%.2f",($FeedarReadings));
								$mppt_vals=$mppt_vals+sprintf("%.2f",($FeedarReadings));
							}else{
								$existing_array_other[]=sprintf("%.2f",($FeedarReadings));
								$other_vals=$other_vals+sprintf("%.2f",($FeedarReadings));
							}
							
							$oneDayTotal=$oneDayTotal+floatval($FeedarReadings);
							
						}
						$existing_array=array_merge($existing_array,$existing_array_mppt);
						$existing_array[]=$mppt_vals;
						$existing_array=array_merge($existing_array,$existing_array_other);
						$existing_array[]=$other_vals;
						$existing_array[]=sprintf("%.2f",($oneDayTotal));
						if(floatval($oneDayTotal)>0){
							$allData[]=$existing_array;
						}
						
					$next="+1 day";
					$next_date = date('Y-m-d', strtotime($next_date .$next)); 	 
				}

			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" => $allData,
					"total_unit"=>$total_unit,
					"name"=>$Namearray,
					"description"=>$description
			);
			 
		}
		else{
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,
					"data" =>array(),
			);
		}
		 
		 
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
		 
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}

public function updateMpptStatusByIdAction(){

	try {
		$request=$this->getRequest();
		$id=$request->getParam("id");
		$status=$request->getParam("status");
		$remark=$request->getParam("remark");

		$mpptmapper=new Application_Model_MpptsMapper();

		if($mppt=$mpptmapper->updateMpptsById($id,$status,$remark, $this->_userId)){
			$meta = array(
					"code" => 200,
					"message" => "SUCCESS"
			);
			$arr = array(
					"meta" => $meta,

			);
		} else {
			$meta = array(
					"code" => 401,
					"message" => "Error while updating"
			);
			$arr = array(
					"meta" => $meta
			);
		}
	}catch (Exception $e) {
		$meta = array(
				"code" => 501,
				"messgae" => $e->getMessage()
		);
			
		$arr = array(
				"meta" => $meta
		);
	}
	$json = json_encode($arr, JSON_PRETTY_PRINT);
	echo $json;
}

public function getMpptBySiteIdDateAction(){
	
		try {
			$request=$this->getRequest();
			$site_id=$request->getParam("site_id");
			$mppt_date=$request->getParam("mppt_date");
			$zendDates = new Zend_Date($mppt_date,"dd-MMM-YYYY");
			$mppt_date = $zendDates->toString("yyyy-MM-dd");
			$MpptsReadingMapper=new Application_Model_MpptsReadingMapper();
			$MpptMapper=new Application_Model_MpptsMapper();
			$sitesMapper=new Application_Model_SitesMapper();
				
			$data_arr=array();
			$siteMeters=$MpptsReadingMapper->getMpptReadingBySiteIdReadingDate($site_id,$mppt_date);
			if($siteMeters){
				foreach ($siteMeters as $siteMeter){ 
				 $data=array(
							"mppt_detail"=>$siteMeter["mppt_name"],
							"reading"=>floatval(sprintf("%.2f",($siteMeter["reading"]))),
							"mppt_id"=>$siteMeter["mppt_id"],
							 
					);
					$data_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data_arr,
	
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
				
				
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
				
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function updateMpptbyDateAndIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$unit=sprintf("%.2f",($request->getParam("unit")));
			$date=$request->getParam("date");
	
			$zendDates = new Zend_Date($date,"dd-MMM-YYYY");
			$date = $zendDates->toString("yyyy-MM-dd");
	
			$Feedermapper=new Application_Model_MpptsReadingMapper();
	
			if($Feeder=$Feedermapper->updateMpptByDateAndId($id,$unit,$date)){
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while updating"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
				
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
}
 
 