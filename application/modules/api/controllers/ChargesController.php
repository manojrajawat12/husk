<?php
class Api_ChargesController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
		$this->_user_id=$auth->getIdentity()->user_id;
	}
	
	public function addChargesAction(){
	
		try {
			 
			$request=$this->getRequest();
			$charge_name=$request->getParam("charge_name");
			$charge_parameter=$request->getParam("charge_parameter");
			$hindi_text=$request->getParam("hindi_text");
			$charge_code=$request->getParam("charge_code");
									 
			$ChargesMapper=new Application_Model_ChargesMapper();
			$ChargesData= new Application_Model_Charges();
			$ChargesData->__set("charge_name",$charge_name);
			$ChargesData->__set("charge_parameter",$charge_parameter);
			$ChargesData->__set("hindi_text",$hindi_text);
			$ChargesData->__set("charge_code",$charge_code);
				
			if($Charges=$ChargesMapper->addNewCharges($ChargesData)){
	
					
				$data=array(
				        "consumer_name" => $charges_name,
				        "charge_parameter" => $charge_parameter,
				        "hindi_text" => $hindi_text,
				        "charge_code" => $charge_code,
					
				);
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllChargesAction(){
	
		try {
		    $ChargesMapper=new Application_Model_ChargesMapper();
			
		    $Charges=$ChargesMapper->getAllCharges();
	
		    if(count($Charges) >0){
		        foreach ($Charges as $Charge) {
										
					$data=array(
						 "id" => $Charge->__get("charge_id"),
					    "charge_name" => $Charge->__get("charge_name"),
					    "charge_parameter" => $Charge->__get("charge_parameter"),
					    "hindi_text" => $Charge-> __get("hindi_text"),
					    "charge_code" => $Charge->__get("charge_code"),
					
					);
					 
					$charge_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
				        "data" => $charge_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteChargesByIdAction(){
	
		try {
			$request=$this->getRequest();
			$charge_id=$request->getParam("id"); 
			$ChargesMapper=new Application_Model_ChargesMapper();
			if($Charges=$ChargesMapper->deleteChargesById($charge_id)){ 
			//	$this->_logger->info("DG Id ".$Dgid." has been deleted from DG table by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateChargesByIdAction(){
	
		try {
			$request=$this->getRequest();
			$charge_id=$request->getParam("id");
			$charge_name=$request->getParam("charge_name");
			$charge_parameter=$request->getParam("charge_parameter");
			$hindi_text=$request->getParam("hindi_text");
			$charge_code=$request->getParam("charge_code");
			
			$ChargesMapper=new Application_Model_ChargesMapper();
			$ChargesData= new Application_Model_Charges();
			$ChargesData->__set("charge_id",$charge_id);
			$ChargesData->__set("charge_name",$charge_name);
			$ChargesData->__set("charge_parameter",$charge_parameter);
			$ChargesData->__set("hindi_text",$hindi_text);
			$ChargesData->__set("charge_code",$charge_code);
			 
			if($Charges=$ChargesMapper->updateCharges($ChargesData)){
	
				//$this->_logger->info("Equipment Id ".$dg_id." has been updated by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	
	 
}