<?php
class Api_CollectionsController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
		$this->_user_id=$auth->getIdentity()->user_id;
	}
	
	public function addCollectionsAction(){
	
		try {
			 
			$request=$this->getRequest();
			$site_id=$request->getParam("site_id");
			$timestamp=$request->getParam("timestamp");
			$deposit_by=$request->getParam("deposit_by");
			$amount=$request->getParam("amount");
			$deposit_location=$request->getParam("deposit_location");
			$attachment=$request->getParam("attachment"); 
			 
			$zendDate = new Zend_Date($timestamp,"MM-dd-yyyy");
			$timestamp = $zendDate->toString("yyyy-MM-dd");
			
			$CollectionsMapper=new Application_Model_CollectionsMapper();
			$Collections= new Application_Model_Collections();
			$Collections->__set("site_id",$site_id);
			$Collections->__set("timestamp",$timestamp);
			$Collections->__set("deposit_by",$deposit_by);
			$Collections->__set("amount",$amount);
			$Collections->__set("deposit_location",$deposit_location);
			$Collections->__set("attachment",$attachment);
			$Collections->__set("user_id",$this->_user_id);
			
			if($CollectionId=$CollectionsMapper->addNewCollections($Collections)){
	
				$this->_logger->info("New Deposit ID ".$CollectionId." has been created in Collection table by ". $this->_userName.".");
				
				$sitesMapper=new Application_Model_SitesMapper();
				$sites=$sitesMapper->getSiteById($site_id);
				
				$zendDate = new Zend_Date($timestamp,"yyyy-MM-dd");
				$timestamp = $zendDate->toString("MM/dd/yyyy");
				
				$data=array(
						"id" => $CollectionId,
						"site_id" => $site_id,
						"site_name" => $sites->__get("site_name"),
						"timestamp" => $timestamp,
						"deposit_by" => $deposit_by,
						"amount" => $amount,
						"deposit_location" => $deposit_location,
						"attachment" => $attachment,
				);
	
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllCollectionsAction(){
	
		try {
			 $request=$this->getRequest();
			   $sites_id=$request->getParam("site_id");
				$date_val=$request->getParam("date_val");
			 if($sites_id==null || $sites_id=='undefined' || $sites_id=='ALL'){ 
				$roleSession = new Zend_Session_Namespace('roles');
				$site_id=$roleSession->site_id;
			}else{
				$site_id=explode(",", $sites_id);
			}
 
			$curr_date = $request->getParam("curr_date");
            $last_date = $request->getParam("last_date");
            
			if($date_val==1){
				$zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
				$curr_date = $zendDate->toString("yyyy-MM-dd");
				$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
				$last_date = $zendDate->toString("yyyy-MM-dd");
			}else{
				$curr_date =NULL;
				$last_date =NULL;
			}
			$bankStatementMapper=new Application_Model_BankStatementMapper(); 
					
				$CollectionsMapper=new Application_Model_CollectionsMapper();
				$Collections=$CollectionsMapper->getAllCollections($site_id,$curr_date,$last_date); 
			$totalDeposit=0;
			if($Collections){
				foreach ($Collections as $Collection) {
					
					$sitesMapper=new Application_Model_SitesMapper();
					$sites=$sitesMapper->getSiteById($Collection->__get("site_id"));
					
					$zendDate = new Zend_Date($Collection->__get("timestamp"),"yyyy-MM-dd");
					$timestamp = $zendDate->toString("MM/dd/yyyy");
					
					$zendDate = new Zend_Date($Collection->__get("timestamp"),"yyyy-MM-dd");
					$timestamp_edit = $zendDate->toString("dd-MMM-yyyy");
					
					$usermapper = new Application_Model_UsersMapper();
					$users = $usermapper->getUserById($Collection->__get("user_id"));
					
					$verify_user="";
					$verified_by = $usermapper->getUserById($Collection->__get("verified_by"));
					if($verified_by){
						$verify_user=$verified_by->__get("user_fname") . " " . $verified_by->__get("user_lname");
					}
					$bank_credit_date="";
					if($Collection->__get("bank_credit_date")!=NULL && $Collection->__get("bank_credit_date")!="" && $Collection->__get("bank_credit_date")!='undefined'){
						$zendDate = new Zend_Date($Collection->__get("bank_credit_date"),"yyyy-MM-dd");
						$bank_credit_date = $zendDate->toString("dd-MMM-yyyy");
					}
					$zendDate = new Zend_Date($Collection->__get("curr_timestamp"),"yyyy-MM-dd HH:mm:ss");
					$timestamp_time = $zendDate->toString("dd-MMM-yyyy HH:mm:ss");
					$bankStatement=$bankStatementMapper->getAllBankStatement($Collection->__get("id")); 
					$bank_statement=NULL;
					if($bankStatement){
						$bank_statement=$bankStatement[0]["id"];
					}
					$data=array(
							"id" => $Collection->__get("id"),
							"site_id" => $Collection->__get("site_id"),
							"site_name" => $sites-> __get("site_name"),
							"timestamp" => $timestamp,
							"timestamp_edit" => $timestamp_edit,
							"deposit_by" => $Collection->__get("deposit_by"),
							"amount" => $Collection->__get("amount"),
							"deposit_location" => $Collection-> __get("deposit_location"),
							"attachment" => $Collection->__get("attachment"),
							"entry_by" =>$users->__get("user_fname") . " " . $users->__get("user_lname"),
							"timestamp_time" =>$timestamp_time,
							"status" =>$Collection->__get("status"),
							"verified_by" =>$verify_user,
							"remark" =>$Collection->__get("remark"),
							"bank_amount" =>$Collection->__get("bank_amount"),
							"bank_credit_date"=>$bank_credit_date,
							"bank_statement"=>$bank_statement
					);
					 
					if($Collection->__get("status")!='Inactive'){
						$totalDeposit=$totalDeposit+$Collection->__get("amount"); 
					}
					$Collections_arr[]=$data;  
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS",
						"totalDeposit"=>$totalDeposit
				);
				$arr = array(
						"meta" => $meta,
						"data" => $Collections_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteCollectionsByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$CollectionsMapper=new Application_Model_CollectionsMapper();
			
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			
			if($Collectionsid=$CollectionsMapper->deleteCollectionsById($id,$this->_user_id,$timestamp)){
				$this->_logger->info("Deposit Id ".$Collectionsid." has been deleted from Collections table by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateCollectionsByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$site_id=$request->getParam("site_id");
			$timestamp=$request->getParam("timestamp");
			$deposit_by=$request->getParam("deposit_by");
			$amount=$request->getParam("amount");
			$deposit_location=$request->getParam("deposit_location");
			$attachment=$request->getParam("attachment");
			$status=$request->getParam("status");
			 
			$zendDate = new Zend_Date($timestamp,"MM-dd-yyyy");
			$timestamps = $zendDate->toString("yyyy-MM-dd");
			 
			$CollectionsMapper=new Application_Model_CollectionsMapper();
			$Collections= new Application_Model_Collections();
			$Collections->__set("id",$id);
			$Collections->__set("site_id",$site_id);
			$Collections->__set("timestamp",$timestamps);
			$Collections->__set("deposit_by",$deposit_by);
			$Collections->__set("amount",$amount);
			$Collections->__set("deposit_location",$deposit_location);
			$Collections->__set("attachment",$attachment);
			$Collections->__set("status",$status);
			 
			if($CollectionsMapper->updateCollections($Collections)){
	
				$this->_logger->info("Deposit Id ".$id." has been updated by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
	public function verifyCollectionsByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$statement_id=$request->getParam("statement_id");
			$bank_credit_date=$request->getParam("bank_credit_date");
			$zendDate = new Zend_Date($bank_credit_date,"MM-dd-yyyy");
			$bank_credit_date = $zendDate->toString("yyyy-MM-dd");
			$bankStatementMapper=new Application_Model_BankStatementMapper(); 
					
			$bank_amount=$request->getParam("bank_amount"); 
			//$months_arr=explode(",",$months);
			$remark=$request->getParam("remark");
			
			$CollectionsMapper=new Application_Model_CollectionsMapper();
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			$bankStatementcheck=$bankStatementMapper->checkBankStatement($id,$statement_id); 
			$deposite_data=$bankStatementMapper->getAllBankStatement($id); 
			if($deposite_data){
				$bankStatement=$bankStatementMapper->updateBankStatement(0,$id); 
			}
			
			if($bankStatementcheck){
				if($Collectionsid=$CollectionsMapper->verifyCollectionsById($id,$this->_user_id,$timestamp,$bank_credit_date,$bank_amount,$remark)){
					$this->_logger->info("Deposit Id ".$Collectionsid." has been verify from Collections table by ". $this->_userName.".");
					$bankStatement=$bankStatementMapper->updateBankStatement($statement_id,$id); 
					 
					$meta = array(
							"code" => 200,
							"message" => "SUCCESS"
					); 
					$arr = array(
							"meta" => $meta,
		
					);
				} else {
					$meta = array(
							"code" => 401,
							"message" => "Error while deleting"
					);
					$arr = array(
							"meta" => $meta
					);
				}
			} else {
				$meta = array(
						"code" => 301,
						"message" => "This Transaction is already assign to other Deposit."
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function getAllBankStatementAction(){
	
		try {
			$request=$this->getRequest();
			$bankStatementMapper=new Application_Model_BankStatementMapper(); 
			$bankStatements=$bankStatementMapper->getAllBankStatement();
			
			$bankStatement_arr=array();
			if($bankStatements){
				foreach ($bankStatements as $bankStatement) {
					 
					$data=array(
							"id" => $bankStatement["id"],
							"tran_date" => date('d-M-Y h:i:s',strtotime($bankStatement["tran_date"])),
							"value_date" => date('d-M-Y h:i:s',strtotime($bankStatement["value_date"])),
							"description" => $bankStatement["description"],
							"ref_cheque_no" =>$bankStatement["ref_cheque_no"],
							"branch_code" => $bankStatement["branch_code"],
							"debit" => $bankStatement["debit"],
							"credit" => $bankStatement["credit"],
							"balance" => $bankStatement["balance"],
					);
					
					$bankStatement_arr[]=$data;  
				}
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS",
				);
				$arr = array(
						"meta" => $meta,
						"data" => $bankStatement_arr,
				);
			}else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

}