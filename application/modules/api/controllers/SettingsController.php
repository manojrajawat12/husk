<?php

class Api_SettingsController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
    }
    
        public function addSettingAction(){
      
         try {
             $request=$this->getRequest();
             $setting_name=$request->getParam("name");
               $setting_value=$request->getParam("value");
             $settingmapper=new Application_Model_SettingsMapper();
             $setting= new Application_Model_Settings();
             $setting->__set("setting_name",$setting_name);
             $setting->__set("setting_value",$setting_value);
         if($setting=$settingmapper->addNewSetting($setting)){
             
            
            $data=array(     
            "setting_id" => $setting,
            "setting_name" => $setting_name,
            "setting_value" => $setting_value,    
              );
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }

          public function getSettingByIdAction(){
      
         try {
             $request=$this->getRequest();
             $setting_id=$request->getParam("id");
             $settingmapper=new Application_Model_SettingsMapper();
         if($setting=$settingmapper->getSettingById($setting_id)){
             
           
            $data=array(     
              "setting_id" => $setting->__get("setting_id"),
            "setting_name" => $setting->__get("setting_name"),
           
              );
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function deleteSettingByIdAction(){
      
         try {
             $request=$this->getRequest();
             $setting_id=$request->getParam("id");
             $settingmapper=new Application_Model_SettingsMapper();
         if($setting=$settingmapper->deleteSettingById($setting_id)){
             
            
            
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
         public function getAllSettingsAction(){
      
         try {
//             $request=$this->getRequest();
//             $setting_id=$request->getParam("id");
               $settingmapper=new Application_Model_SettingsMapper();
         if($settings=$settingmapper->getAllSettings()){
          
             foreach ($settings as $setting) {
                 
                 $data=array(
                  "setting_id" =>$setting->__get("setting_id"),
                  "setting_name" => $setting->__get("setting_name"),
                   "setting_value" => $setting->__get("setting_value"),
                 );
                 
                 $setting_arr[]=$data;
             }
            
            
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $setting_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
            public function updateSettingByIdAction(){
      
         try {
           $request=$this->getRequest();
             $setting_id=$request->getParam("id");
             $setting_name=$request->getParam("name");
             $setting_value=$request->getParam("value");
             $settingmapper=new Application_Model_SettingsMapper();
             $setting= new Application_Model_Settings();
             $setting->__set("setting_id",$setting_id);
             $setting->__set("setting_name",$setting_name);
             $setting->__set("setting_value",$setting_value);
            
              if($settings=$settingmapper->updateSetting($setting)){
          
                           
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
           
}    