<?php
class Api_SchemeController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function addSchemeAction(){
		
		try {
			$request=$this->getRequest();
			$scheme=$request->getParam("scheme");
			$scheme_type=$request->getParam("scheme_type");
			$celamed_id=$request->getParam("celamed_id");
			$down_payment=$request->getParam("down_payment");
			$emi_amt=$request->getParam("emi_amt");
			$emi_duration=$request->getParam("emi_duration");
			$discount_amt=$request->getParam("discount_amt");
			$discount_month=$request->getParam("discount_month");
			$gen_scheme_type=$request->getParam("gen_scheme_type");
			$advance_payment=$request->getParam("advance_payment");
			$advance_dis_month=$request->getParam("advance_dis_month");
			$advance_dis_amt=$request->getParam("advance_dis_amt");
			$flat_dis=$request->getParam("flat_dis");
			$per_dis=$request->getParam("per_dis");
			$state_id=$request->getParam("state_id");
			$site_id=$request->getParam("site_id");
			$package_id=$request->getParam("package_id");
			$consumer_id=$request->getParam("consumer_id");
			$starts_date=$request->getParam("start_date");
			$ends_date=$request->getParam("end_date");
			$enables_date=$request->getParam("enable_date");
			$scheme_applied=$request->getParam("scheme_applied");
			$package_type=$request->getParam("is_postpaid");
			$consumer_type=$request->getParam("consumer_type");
			
			$package_continue=$request->getParam("package_continue");
			$scheme_continue=$request->getParam("scheme_continue");
			
			$zendDate = new Zend_Date($starts_date,"MM-dd-yyyy");
			$start_date = $zendDate->toString("yyyy-MM-dd");
			
			$zendDate = new Zend_Date($ends_date,"MM-dd-yyyy");
			$end_date = $zendDate->toString("yyyy-MM-dd");
			
			$zendDate = new Zend_Date($enables_date,"MM-dd-yyyy");
			$enable_date = $zendDate->toString("yyyy-MM-dd");
			
			$schemesMapper=new Application_Model_SchemesMapper();
			$schemes= new Application_Model_Schemes();
			$schemes->__set("scheme",$scheme);
			$schemes->__set("scheme_type",$scheme_type);
			$schemes->__set("celamed_id",$celamed_id);
			$schemes->__set("down_payment",$down_payment);
			$schemes->__set("emi_amt",$emi_amt);
			$schemes->__set("emi_duration",$emi_duration);
			$schemes->__set("discount_amt",$discount_amt);
			$schemes->__set("discount_month",$discount_month);
			$schemes->__set("gen_scheme_type",$gen_scheme_type);
			$schemes->__set("advance_payment",$advance_payment);
			$schemes->__set("advance_dis_month",$advance_dis_month);
			$schemes->__set("advance_dis_amt",$advance_dis_amt);
			$schemes->__set("flat_dis",$flat_dis);
			$schemes->__set("per_dis",$per_dis);
			$schemes->__set("state_id",$state_id);
			$schemes->__set("site_id",$site_id);
			$schemes->__set("package_id",$package_id);
			$schemes->__set("consumer_id",$consumer_id);
			$schemes->__set("start_date",$start_date);
			$schemes->__set("end_date",$end_date);
			$schemes->__set("enable_date",$enable_date);
			$schemes->__set("scheme_applied",$scheme_applied);
			$schemes->__set("is_postpaid",$package_type);
			$schemes->__set("consumer_type",$consumer_type);
			$schemes->__set("package_continue",$package_continue);
			$schemes->__set("scheme_continue",$scheme_continue);
			
			if($schemeId=$schemesMapper->addNewScheme($schemes)){
	
				$this->_logger->info("New Scheme ID ".$schemeId." has been created in schemes by ". $this->_userName.".");
				$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
				$consumerScheme=new Application_Model_ConsumerScheme();
				$consumersMapper=new Application_Model_ConsumersMapper();
				
					$consuner_val=($consumer_id=='null' || $consumer_id=='undefined')?NULL:$consumer_id;
					$package_val=($package_id=='null' || $package_id=='undefined')?NULL:$package_id;
					$site_val=($site_id=='null' || $site_id=='undefined')?NULL:$site_id;
					$state_val=($state_id=='null' || $state_id=='undefined')?NULL:$state_id;
					$consumer_type=($consumer_type=='null' || $consumer_type=='undefined')?NULL:$consumer_type;
					$package_type=($package_type=='null' || $package_type=='undefined')?NULL:$package_type;
					if($scheme_applied=='automatic'){	
					$consumers=$consumersMapper->getConsumerBySomeDetails($consuner_val,$consumer_type,$package_val,$package_type,$site_val,$state_val,$scheme_type);
					if($scheme_type=="equipment"){
						$total_month=$emi_duration;
					}elseif ($gen_scheme_type=="advance_pay"){
						$total_month=$advance_payment+$advance_dis_month;
					}
					if($consumers){
					foreach ($consumers as $consumer){
						$consumerScheme->__set("consumer_scheme", $schemeId);
						$consumerScheme->__set("consumer_id", $consumer["consumer_id"]);
						$consumerScheme->__set("cluster_id", NULL);
						$consumerScheme->__set("site_id", $consumer["site_id"]);
						$consumerScheme->__set("package_continue", "no");
						$consumerScheme->__set("scheme_continue", "no");
						$consumerScheme->__set("total_month", $total_month);
						
						
						if ($consumer_schemes = $consumerSchemeMapper->addNewConsumerScheme($consumerScheme)) {
							
						}
					}
					}
				}
				$packageMapper=new Application_Model_PackagesMapper();
				$package=$packageMapper->getPackageByPackageConsumerId($package_val,$consuner_val);
					$ispostpaid=null;
					$consumer_type=null;
					if($package){
						$ispostpaid=$package["is_postpaid"];
						$consumer_type=$package["type_of_me"];
					}
				$data=array(
						"scheme_id" => $schemeId,
						"scheme" => $scheme,
						"scheme_type" => $scheme_type,
						"celamed_id" => $celamed_id,
						"down_payment" => $down_payment,
						"emi_amt" => $emi_amt,
						"emi_duration" => $emi_duration,
						"discount_amt" => $discount_amt,
						"discount_month" => $discount_month,
						"gen_scheme_type" => $gen_scheme_type,
						"advance_payment" => $advance_payment,
						"advance_dis_month" => $advance_dis_month,
						"advance_dis_amt" => $advance_dis_amt,
						"flat_dis" => $flat_dis,
						"per_dis" => $per_dis,
						"state_id" => $state_id,
						"package_id" => $package_id,
						"consumer_id" => $consumer_id,
						"end_date" => $end_date,
						"start_date" => $start_date,
						"enable_date" => $enable_date,
						"scheme_applied" => $scheme_applied,
						"is_postpaid"=>$ispostpaid,
						"consumer_type"=>$consumer_type,
						"scheme_status"=>"enable",
						"package_continue"=>$package_continue,
						"scheme_continue"=>$scheme_continue,
				);
	
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllSchemesAction(){
	
		try {
			$schemesMapper=new Application_Model_SchemesMapper();
			$packageMapper=new Application_Model_PackagesMapper();
			$request=$this->getRequest();
			$scheme_val=$request->getParam("scheme");
			$schemes=$schemesMapper->getAllSchemes($scheme_val);
	 
			if($schemes){
				foreach ($schemes as $scheme) {
					//$package=$packageMapper->getPackageByPackageConsumerId($scheme->__get("package_id"),$scheme->__get("consumer_id"));
					
					if($scheme->__get("start_date")==null || $scheme->__get("start_date")=='undefined'){
						$start_date=null;
					}else{
						$zendDate = new Zend_Date($scheme->__get("start_date"),"yyyy-MM-dd");
						$start_date = $zendDate->toString("MM/dd/yyyy");
					}
					if($scheme->__get("end_date")==null || $scheme->__get("end_date")=='undefined'){
						$end_date=null;
					}else{
						$zendDate = new Zend_Date($scheme->__get("end_date"),"yyyy-MM-dd");
						$end_date = $zendDate->toString("MM/dd/yyyy");
					}
					if($scheme->__get("enable_date")==null || $scheme->__get("enable_date")=='undefined'){
						$enable_date=null;
					}else{
						$zendDate = new Zend_Date($scheme->__get("enable_date"),"yyyy-MM-dd");
						$enable_date = $zendDate->toString("MM/dd/yyyy"); 
					}
					/*$ispostpaid=null;
					$consumer_type=null;
					if($package){
						$ispostpaid=$package["is_postpaid"];
						$consumer_type=$package["type_of_me"];
					}*/
					
					$data = array(
							'scheme_id'=>$scheme->__get("scheme_id"),
							'scheme' => $scheme->__get("scheme"),
							'scheme_type' => $scheme->__get("scheme_type"),
							'equip_name' => $scheme->__get("celamed_id"),
							'down_pay' => $scheme->__get("down_payment"),
							'emi_amt' => $scheme->__get("emi_amt"),
							'emi_dur' => $scheme->__get("emi_duration"),
							'discount_amt' => $scheme->__get("discount_amt"),
							'discount_month' => $scheme->__get("discount_month"),
							'gen_scheme_type' => $scheme->__get("gen_scheme_type"),
							'gen_advance_pay' => $scheme->__get("advance_payment"),
							'gen_discount_month' => $scheme->__get("advance_dis_month"),
							'gen_discount_amt' => $scheme->__get("advance_dis_amt"),
							'flat' => $scheme->__get("flat_dis"),
							'discount_per' => $scheme->__get("per_dis"),
							'state_id' => $scheme->__get("state_id"),
							'site_id' => $scheme->__get("site_id"),
							'package_id' => $scheme->__get("package_id"),
							'consumer_id' => $scheme->__get("consumer_id"),
							'start_date' =>$start_date,
							'end_date' => $end_date,
							"enable_date" => $enable_date,
							"scheme_applied" => $scheme->__get("scheme_applied"),
							"enable_date" => $enable_date,
							"package_type"=>$scheme->__get("is_postpaid"),
							"consumer_type"=>$scheme->__get("consumer_type"),
							"scheme_status"=>$scheme->__get("scheme_status"),
							"package_continue"=>$scheme->__get("package_continue"),
							"scheme_continue"=>$scheme->__get("scheme_continue"),
						
					);
	
					$enterprise_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $enterprise_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteSchemeByIdAction(){
	
		try {
			$request=$this->getRequest();
			$scheme_id=$request->getParam("id");
			$schemeMapper=new Application_Model_SchemesMapper();
			if($scheme=$schemeMapper->deleteSchemeById($scheme_id)){
				$this->_logger->info("Scheme Id ".$scheme_id." has been deleted from Schemes by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateSchemeByIdAction(){
	
		try {
			$request=$this->getRequest();
			$scheme_id=$request->getParam("id");
			$scheme=$request->getParam("scheme");
			$scheme_type=$request->getParam("scheme_type");
			$celamed_id=$request->getParam("celamed_id");
			$down_payment=$request->getParam("down_payment");
			$emi_amt=$request->getParam("emi_amt");
			$emi_duration=$request->getParam("emi_duration");
			$discount_amt=$request->getParam("discount_amt");
			$discount_month=$request->getParam("discount_month");
			$gen_scheme_type=$request->getParam("gen_scheme_type");
			$advance_payment=$request->getParam("advance_payment");
			$advance_dis_month=$request->getParam("advance_dis_month");
			$advance_dis_amt=$request->getParam("advance_dis_amt");
			$flat_dis=$request->getParam("flat_dis");
			$per_dis=$request->getParam("per_dis");
			$state_id=$request->getParam("state_id");
			$site_id=$request->getParam("site_id");
			$package_id=$request->getParam("package_id");
			$consumer_id=$request->getParam("consumer_id");
			$starts_date=$request->getParam("start_date");
			$ends_date=$request->getParam("end_date");
			$enables_date=$request->getParam("enable_date");
			$scheme_applied=$request->getParam("scheme_applied");
			$package_type=$request->getParam("is_postpaid");
			$consumer_type=$request->getParam("consumer_type");
			$scheme_status=$request->getParam("scheme_status");
			$package_continue=$request->getParam("package_continue");
			$scheme_continue=$request->getParam("scheme_continue");
							
			$zendDate = new Zend_Date($starts_date,"MM-dd-yyyy");
			$start_date = $zendDate->toString("yyyy-MM-dd");
				
			$zendDate = new Zend_Date($ends_date,"MM-dd-yyyy");
			$end_date = $zendDate->toString("yyyy-MM-dd");
			
			$zendDate = new Zend_Date($enables_date,"MM-dd-yyyy");
			$enable_date = $zendDate->toString("yyyy-MM-dd");
			
			$schemesMapper=new Application_Model_SchemesMapper();
			$schemes= new Application_Model_Schemes();
			
			
			$schemes->__set("scheme_id",$scheme_id);
			$schemes->__set("scheme",$scheme);
			$schemes->__set("scheme_type",$scheme_type);
			$schemes->__set("celamed_id",$celamed_id);
			$schemes->__set("down_payment",$down_payment);
			$schemes->__set("emi_amt",$emi_amt);
			$schemes->__set("emi_duration",$emi_duration);
			$schemes->__set("discount_amt",$discount_amt);
			$schemes->__set("discount_month",$discount_month);
			$schemes->__set("gen_scheme_type",$gen_scheme_type);
			$schemes->__set("advance_payment",$advance_payment);
			$schemes->__set("advance_dis_month",$advance_dis_month);
			$schemes->__set("advance_dis_amt",$advance_dis_amt);
			$schemes->__set("flat_dis",$flat_dis);
			$schemes->__set("per_dis",$per_dis);
			$schemes->__set("state_id",$state_id);
			$schemes->__set("site_id",$site_id);
			$schemes->__set("package_id",$package_id);
			$schemes->__set("consumer_id",$consumer_id);
			$schemes->__set("start_date",$start_date);
			$schemes->__set("end_date",$end_date);
			$schemes->__set("enable_date",$enable_date);
			$schemes->__set("scheme_applied",$scheme_applied);
			$schemes->__set("scheme_status",$scheme_status);
			$schemes->__set("is_postpaid",$package_type);
			$schemes->__set("consumer_type",$consumer_type);
			$schemes->__set("package_continue",$package_continue);
			$schemes->__set("scheme_continue",$scheme_continue); 
			
			if($schemesMapper->updateScheme($schemes)){
	
				$this->_logger->info("Scheme Id ".$scheme_id." has been updated by ". $this->_userName.".");
				
				$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
				$consumerScheme=new Application_Model_ConsumerScheme();
				$consumersMapper=new Application_Model_ConsumersMapper();
				if($scheme_applied=='automatic'){
					$consuner_val=($consumer_id=='null' || $consumer_id=='undefined')?NULL:$consumer_id;
					$package_val=($package_id=='null' || $package_id=='undefined')?NULL:$package_id;
					$site_val=($site_id=='null' || $site_id=='undefined')?NULL:$site_id;
					$state_val=($state_id=='null' || $state_id=='undefined')?NULL:$state_id;
					$consumer_type=($consumer_type=='null' || $consumer_type=='undefined')?NULL:$consumer_type;
					$package_type=($package_type=='null' || $package_type=='undefined')?NULL:$package_type;
						
					$consumers=$consumersMapper->getConsumerBySomeDetails($consuner_val,$consumer_type,$package_val,$package_type,$site_val,$state_val);
					$consumer_data=array();
					foreach ($consumers as $consumer){
						$consumer_data[]=$consumer["consumer_id"];
						$checkScheme=$consumerSchemeMapper->checkSchemeByConsumerID($consumer["consumer_id"],$consumer["site_id"],$scheme_id);
						if(!$checkScheme){
							$consumerScheme->__set("consumer_scheme", $scheme_id);
							$consumerScheme->__set("consumer_id", $consumer["consumer_id"]);
							$consumerScheme->__set("cluster_id", null);
							$consumerScheme->__set("site_id", $consumer["site_id"]);
							$consumerScheme->__set("package_continue", "no");
							$consumerScheme->__set("scheme_continue", "no");
					
							if ($consumer_schemes = $consumerSchemeMapper->addNewConsumerScheme($consumerScheme)) {
									
							}
						}
					}
					$delete_schemes = $consumerSchemeMapper->deleteschemeByConsumerId($consumer_data,$scheme_id);
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
			
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
} 