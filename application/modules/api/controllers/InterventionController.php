<?php
class Api_InterventionController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function addInterventionAction(){
	
		try {
			$request=$this->getRequest();
			$type=$request->getParam("type");
			 
			$InterventionMapper=new Application_Model_InterventionMapper();
			$Intervention= new Application_Model_Intervention();
			
			$Intervention->__set("type",$type);
	
			if($InterventionId=$InterventionMapper->addNewIntervention($Intervention)){
	
				$this->_logger->info("New Intervention ID ".$InterventionId." has been created in Intervention by ". $this->_userName.".");
				
				$data=array(
						"id" => $InterventionId,
						"type" => $type,
				);
	
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllInterventionAction(){
	
		try {
			$InterventionMapper=new Application_Model_InterventionMapper();
			$Intervention=$InterventionMapper->getAllIntervention();
	
			if(count($Intervention) >0){
				foreach ($Intervention as $Interventions) {
					 
					$data=array(
							"id" => $Interventions->__get("id"),
							"type" => $Interventions->__get("type"),
							
					);
	
					$Intervention_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $Intervention_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteInterventionByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$InterventionMapper=new Application_Model_InterventionMapper();
			if($Intervention=$InterventionMapper->deleteInterventionById($id)){
				$this->_logger->info("Intervention Id ".$Intervention." has been deleted from Intervention by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateInterventionByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$type=$request->getParam("type");

			$InterventionMapper=new Application_Model_InterventionMapper();
			$Intervention= new Application_Model_Intervention();
			;
			$Intervention->__set("id",$id);
			$Intervention->__set("type",$type);
			
	
			if($InterventionMapper->updateIntervention($Intervention)){
	
				$this->_logger->info("Intervention Id ".$id." has been updated by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
}