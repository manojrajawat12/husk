<?php

class Api_SiteStatusController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
		$this->_user_id=$auth->getIdentity()->user_id;
    }
    
        
 public function addSiteStatusAction(){
      
         try {
         	  $request=$this->getRequest();
        	  $site_id=$request->getParam("site_id");
              $feedar_id=$request->getParam("feedar_id");
              $status=$request->getParam("status");
			  $timestamp=$request->getParam("timestamp");
			  $Start_date=$request->getParam("start_date");
			 
			  $Start_date_array=explode(",", $Start_date);
			 
			 $date = new Zend_Date();
			  $date->setTimezone("Asia/Calcutta");
			  $timestamp_curr = $date->toString("yyyy-MM-dd HH:mm:ss");
			  
	             $siteStatusMapper=new Application_Model_SiteStatusMapper();
	             $siteStatus=new Application_Model_SiteStatus();

	             for($i=0;$i<count($Start_date_array);$i++){
	          
             	    $siteStatus->__set("site_id",$site_id);
             		$siteStatus->__set("feedar_id",$feedar_id);
             		$siteStatus->__set("status","INACTIVE");
					$siteStatus->__set("timestamp",$timestamp);
					$siteStatus->__set("start_date",$Start_date_array[$i]);
					$siteStatus->__set("entry_by",$this->_user_id);
					$siteStatus->__set("entry_type","Web");
					$siteStatus->__set("entry_date",$timestamp_curr);
				 
                    $Site_MM=$siteStatusMapper->addNewSiteStatus($siteStatus);
	                	
	             }
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     
                );
           
           
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
        }

 public function updateSiteStatusAction(){
        
        	try {
        		$request=$this->getRequest();
        		$site_id=$request->getParam("site_id");
        		$feeder_id=$request->getParam("feeder_id");
        		$status=$request->getParam("status");
        	    $timestamp=$request->getParam("timestamp");
        		$start_date=$request->getParam("start_date");
       		   /*$start_time=$request->getParam("start_time");
        	    $end_time=$request->getParam("end_time");
        		$is_24_hr=$request->getParam("is_24_hr");
        		if($is_24_hr=='true'){
        			$time=1;
        		}else {
        			$time=0;
        		}*/
        
        		$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
        		$siteMaster=new Application_Model_SiteMasterMeter();
        		$sites=new Application_Model_SitesMapper();
        		 
        		$site=$sites->getSiteById($site_id);
        		 
        		if($id=='undefined'){
        			$siteMaster->__set("site_id",$site_id);
        			$siteMaster->__set("feeder_id",strtoupper($feeder_id));
        			//$siteMaster->__set("meter_keyword",$site->__get("site_code").strtoupper($mater_meter));
        			$siteMaster->__set("status",$status);
        			$siteMaster->__set("timestamp",$timestamp);
        			$siteMaster->__set("start_date",$start_date);
        		/*	$siteMaster->__set("is_24_hr",$time);
        			$Site_MM=$siteMasterMapper->addNewSiteMaster($siteMaster);
        			$meta = array(
        					"code" => 200,
        					"message" => "SUCCESS"
        			);
        			$arr = array(
        					"meta" => $meta,
        
        			);*/
        		}else{
        			$siteMaster->__set("site_id",$site_id);
        			$siteMaster->__set("feeder_id",$feeder_id);
        			$siteMaster->__set("status",strtoupper($status));
        			//$siteMaster->__set("meter_keyword",$site->__get("site_code").strtoupper($mater_meter));
        			$siteMaster->__set("timestamp",$timestamp);
        			$siteMaster->__set("start_date",$start_date);
        			$siteMaster->__set("end_time",$end_time);
        			$siteMaster->__set("is_24_hr",$time);
        			if($enterprisePrice=$siteMasterMapper->updateSiteMeter($siteMaster)){
        
        				$meta = array(
        						"code" => 200,
        						"message" => "SUCCESS"
        				);
        				$arr = array(
        						"meta" => $meta,
        
        				);
        			} else {
        				$meta = array(
        						"code" => 401,
        						"message" => "Error while updating"
        				);
        				$arr = array(
        						"meta" => $meta
        				);
        			}
        		}
        	} catch (Exception $e) {
        		$meta = array(
        				"code" => 501,
        				"messgae" => $e->getMessage()
        		);
        
        		$arr = array(
        				"meta" => $meta
        		);
        	}
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        }

 public function getAllSiteStatusAction(){
        
        	try {
        		$SiteStatusMapper=new Application_Model_SiteStatusMapper();
        		  
        		$siteStatus=$SiteStatusMapper->getAllSiteStatus();
        		$sites_status=array();
        		if($siteStatus){
        			foreach ($siteStatus as $siteMaster_val) {
        				$data=array(
        						"site_id" => $siteMaster_val->__get("site_id"),
        						"site_name"=>$siteMaster_val->__get("site_name"),
        						"feedar_id"=>$siteMaster_val->__get("feedar_id"),
        						"timestamp"=>$siteMaster_val->__get("timestamp"),
        						"status"=>$siteMaster_val->__get("status"),
        				);
        				$sites_status[]=$data;
        			}
        			$meta = array(
        					"code" => 200,
        					"message" => "SUCCESS"
        			);
        			$arr = array(
        					"meta" => $meta,
        					"data" => $sites_status,
        			);
        
        		}
        		else{
        			$meta = array(
        					"code" => 200,
        					"message" => "SUCCESS"
        			);
        			$arr = array(
        					"meta" => $meta,
        					"data" =>array(),
        			);
        		}
        
        
        	}catch (Exception $e) {
        		$meta = array(
        				"code" => 501,
        				"messgae" => $e->getMessage()
        		);
        
        		$arr = array(
        				"meta" => $meta
        		);
        	}
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        }
 
public function getStartDateAction() {
        
        	try {
        		$request = $this->getRequest();
        		$site_id = $request->getParam("site_id");
        		$feeder_id = $request->getParam("feeder_id");
        		$date = $request->getParam("date");
        
        		$zendDate = new Zend_Date($date,"dd-MM-yyyy");
        		$timestamp = $zendDate->toString("yyyy-MM-dd");
        
        		$siteStatusMapper = new Application_Model_SiteStatusMapper();
        		$siteStatus = new Application_Model_SiteStatus();
        
        		$result = $siteStatusMapper->getStartDate($site_id,$feeder_id,$timestamp);
        		if($result){
        			$arr = array();
        			foreach ($result as $v) {
        				// print_r($v);
        				array_push($arr, $v['start_date']);
        			}
        			// print_r($arr);
        			$meta = array(
        					"code" => 200,
        					"message" => "SUCCESS"
        			);
        			$arr = array(
        					"meta" => $meta,
        					"data" => $arr
        
        			);
        		} else {
        			$meta = array(
        					"code" => 401,
        					"message" => "Error while getting"
        			);
        			$arr = array(
        					"meta" => $meta
        			);
        		}
        
        	} catch (Exception $e) {
        		$meta = array(
        				"code" => 501,
        				"messgae" => $e->getMessage()
        		);
        
        		$arr = array(
        				"meta" => $meta
        		);
        	}
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        }
}