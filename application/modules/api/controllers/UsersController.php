<?php

class Api_UsersController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
      //  header('Content-Type: application/json');
	   $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
    }

    public function getAllClusterManagersAction() {

        try {
            $usersMapper = new Application_Model_UsersMapper();
            $users = $usersMapper->getAllClusterManagers();
            if ($users) {
           
                foreach ($users as $user) {
                    $data = array(
                        "user_id" => $user->__get("user_id"),
                        "user_fname" => $user->__get("user_fname"),
                        "user_lname" => $user->__get("user_lname"),
                        "user_email" => $user->__get("user_email"),
                        "phone" => $user->__get("phone"),
                    );

                    $users_arr[] = $data;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $users_arr,
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "Users not found"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => $e->getCode(),
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
    public function addUserAction() {

        try {
            $request = $this->getRequest();
            $username = $request->getParam("username");
            $user_fname = $request->getParam("user_fname");
            $user_email = $request->getParam("user_email");
            $user_lname = $request->getParam("user_lname");
            $profile_image = $request->getParam("profile_image");
            $user_role = $request->getParam("user_role_id");
            $reset_code = $request->getParam("reset_code");
            $hashed_password = $request->getParam("hashed_password");
            $phone=$request->getParam("phone");
            $usermapper = new Application_Model_UsersMapper();
            $userRoleMapper=new Application_Model_UserRolesMapper();
            // $clustermapper=new Application_Model_ClustersMapper();
            $user = new Application_Model_Users();
            $user->__set("user_fname", $user_fname);
            $user->__set("user_lname", $user_lname);
            $user->__set("username", $username);
            $user->__set("user_email", $user_email);
            $user->__set("profile_image", $profile_image);
            $user->__set("user_role", $user_role);
            $user->__set("reset_code", $reset_code);
            $user->__set("hashed_password", sha1($hashed_password));
            $user->__set("phone", $phone);

            if ($user = $usermapper->addNewUser($user)) {
				$this->_logger->info("New user ID ".$user." has been created in users by ". $this->_userName.".");
            	
				$userRole= $userRoleMapper->getUserRoleById($user_role);
				$user_Role=$userRole->__get('user_role');
				$name = str_replace('_', ' ', $user_Role);
				$roles = ucwords(strtolower($name));
                $data = array(
                    'user_id' => $user,
                    'username' => $username,
                    'user_fname' => $user_fname,
                    'user_lname' => $user_lname,
                    'user_email' => $user_email,
                    'profile_image' => $profile_image,
                    'user_role_id' => $user_role,
                    'reset_code' => $reset_code,
                    'phone' => $phone,
                	'user_role' => $roles,
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getUserByIdAction() {

        try {
            $request = $this->getRequest();
            $user_id = $request->getParam("id");
            $usermapper = new Application_Model_UsersMapper();
            $userRoleMapper=new Application_Model_UserRolesMapper();
            if ($user = $usermapper->getUserById($user_id)) {

            	$user_role= $userRoleMapper->getUserRoleById($user->__get("user_role"));
                $data = array(
                    'username' => $user->__get("username"),
                    'hashed_password' => $user->__get("hashed_password"),
                    'user_fname' => $user->__get("user_fname"),
                    'user_lname' => $user->__get("user_lname"),
                    'user_email' => $user->__get("user_email"),
                    'profile_image' => $user->__get("profile_image"),
                    'user_role_id' => $user->__get("user_role"),
                    'reset_code' => $user->__get("reset_code"),
                    'phone' => $user->__get("phone"),
                	'user_role' => $user_role->__get("user_role"),
                );


                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while Fetching"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function deleteUserByIdAction() {

        try {
            $request = $this->getRequest();
            $user_id = $request->getParam("id");
            $usermapper = new Application_Model_UsersMapper();
            if ($user = $usermapper->deleteUserById($user_id)) {
				$this->_logger->info("USer Id ".$user_id." has been deleted from users by ". $this->_userName.".");
            	
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllUsersAction() {

        try {
//             $request=$this->getRequest();
//             $user_id=$request->getParam("id");
            $usermapper = new Application_Model_UsersMapper();
            $userRoleMapper=new Application_Model_UserRolesMapper();
//            $clustermapper = new Application_Model_ClustersMapper();
            if ($users = $usermapper->getAllUsers()) {
              
                foreach ($users as $user) {
                	$user_role= $userRoleMapper->getUserRoleById($user->__get("user_role"));
                	$user_Role=$user_role->__get('user_role');
                	$name = str_replace('_', ' ', $user_Role);
                	$roles = ucwords(strtolower($name));
                     $data = array(
                    'username' => $user->__get("username"),
                    'hashed_password' => $user->__get("hashed_password"),
                    'user_fname' => $user->__get("user_fname"),
                    'user_lname' => $user->__get("user_lname"),
                    'user_email' => $user->__get("user_email"),
                    'profile_image' => $user->__get("profile_image"),
                    'user_role_id' => $user->__get("user_role"),
                    'reset_code' => $user->__get("reset_code"),
                    'user_id' => $user->__get("user_id"),
                    'phone' => $user->__get("phone"),
                     'user_role' => $roles,
                );

                    $user_arr[] = $data;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $user_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updateUserByIdAction() {

        try {
            $request = $this->getRequest();
            $request = $this->getRequest();
            $username = $request->getParam("username");
            $user_fname = $request->getParam("user_fname");
            $user_id = $request->getParam("user_id");
            $user_email = $request->getParam("user_email");
            $user_lname = $request->getParam("user_lname");
            $profile_image = $request->getParam("profile_image");
            $user_role = $request->getParam("user_role_id");
            $reset_code = $request->getParam("reset_code");
            $hashed_password = $request->getParam("hashed_password");
            $phone= $request->getParam("phone");
            $usermapper = new Application_Model_UsersMapper();
            $user = new Application_Model_Users();
			$lastUser= $usermapper->getUserById($user_id);
            $user->__set("user_fname", $user_fname);
            $user->__set("user_id", $user_id);
            $user->__set("user_lname", $user_lname);
            $user->__set("username", $username);
            $user->__set("user_email", $user_email);
            $user->__set("profile_image", $profile_image);
            $user->__set("user_role", $user_role);
            $user->__set("reset_code", $reset_code);
            $user->__set("hashed_password", $hashed_password);
            $user->__set("phone", $phone);
        
            
            if ($users = $usermapper->updateUser($user)) {
				$lastUser_arr = array(
            			'username' => $lastUser->__get("username"),
            			'user_fname' => $lastUser->__get("user_fname"),
            			'user_lname' => $lastUser->__get("user_lname"),
            			'user_email' => $lastUser->__get("user_email"),
            			'user_role_id' => $lastUser->__get("user_role"),
            			'phone' => $lastUser->__get("phone"),
						'profile_image' => $lastUser->__get("profile_image"),
            			
            	);
            	
            	$newUser_arr = array(
                   		'username' => $user->__get("username"),
            			'user_fname' => $user->__get("user_fname"),
            			'user_lname' => $user->__get("user_lname"),
            			'user_email' => $user->__get("user_email"),
            			'user_role_id' => $user->__get("user_role"),
            			'phone' => $user->__get("phone"),
						'profile_image' => $user->__get("profile_image"),
                );
            	$lastUser_diff=array_diff($lastUser_arr,$newUser_arr);
            	$newUser_diff=array_diff($newUser_arr,$lastUser_arr);
            	
            	$change_data="";
            	foreach ($lastUser_diff as $key => $value)
            	{
            		$change_data.=$key." ".$lastUser_diff[$key]." change to ".$newUser_diff[$key]." ";
            	}
            	$this->_logger->info("User Id ".$user_id." has been updated where ".$change_data." by ".$this->_userName.".");
            	

                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
}
