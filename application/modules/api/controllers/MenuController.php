<?php
class Api_MenuController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
		$this->_user_id=$auth->getIdentity()->user_id;
	}
	
	public function addMenuAction(){
	
		try {
			 
			$request=$this->getRequest();
			$menu_desc=$request->getParam("menu_desc");
			$sub_menu=$request->getParam("menu_type");
			$menu_items=$request->getParam("add_menu_item");
			$menu_type="menu";
			/* echo $menu_items;
			exit; */
			$MenuMapper=new Application_Model_MenuMapper();
			$menus= new Application_Model_Menu();
			$menus->__set("menu_desc",$menu_desc);
			$menus->__set("menu_type",$menu_type);
			
			if($Menu=$MenuMapper->addNewMenu($menus)){
			    
			    $menu_item=explode(",",$menu_items);
			    for($j=0;$j<count($menu_item);$j++){
			       /*  $cp->__set("menu_desc",$menu_items[$j]);
			        for($p=0;$p<count($packageIDs);$p++){ */
			      //  $sub_menu = 'sub_menu';
			    $menusData= new Application_Model_Menu();
			    $menusData->__set("menu_type",$sub_menu);
			    $menusData->__set("parent_menu",$Menu);
			    $menusData->__set("menu_desc",$menu_item[$j]);
			    
			    
			    $MenuData=$MenuMapper->addNewMenu($menusData);
			    print_r($MenuData);
		  } exit;
			    
				$data=array(
				        "menu_desc" => $menu_desc,
				        "parent_menu" => $parent_menu,
				        "menu_type" => $menu_type,
					
				);
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllMenusAction(){
	
		try {
		    $MenuMapper=new Application_Model_MenuMapper();
			
		    $Menus=$MenuMapper->getAllMenu();
	
		    if(count($Menus) >0){
		        foreach ($Menus as $Menu) {
										
					$data=array(
					    "menu_id" => $Menu->__get("menu_id"),
					    "menu_desc" => $Menu->__get("menu_desc"),
					    "parent_menu" => $Menu-> __get("parent_menu"),
					    "menu_type" => $Menu-> __get("menu_type"),
					    							
					);
					 
					$ConsumerType_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
				        "data" => $ConsumerType_arr,
				);
	
			}
			else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteMenusByIdAction(){
	
		try {
			$request=$this->getRequest();
			$menu_id=$request->getParam("id");
			$MenuMapper=new Application_Model_MenuMapper();
			if($Menu=$MenuMapper->deleteMenuById($menu_id)){
			//	$this->_logger->info("DG Id ".$Dgid." has been deleted from DG table by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateMenusByIdAction(){
	
		try {
			$request=$this->getRequest();
			$menu_id=$request->getParam("id");
			$menu_desc=$request->getParam("menu_desc");
			$parent_menu=$request->getParam("parent_menu");
			$menu_type=$request->getParam("menu_type");
			
			$MenuMapper=new Application_Model_MenuMapper();
			$menu= new Application_Model_ConsumerType();
			$menu->__set("menu_id",$menu_id);
			$menu->__set("menu_desc",$menu_desc);
			$menu->__set("parent_menu",$parent_menu);
			$menu->__set("menu_type",$menu_type);
			
			if($Menus=$MenuMapper->updateMenu($menu)){
	
				//$this->_logger->info("Equipment Id ".$dg_id." has been updated by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	
	 
}