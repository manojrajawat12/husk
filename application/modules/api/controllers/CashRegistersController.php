<?php

class Api_CashRegistersController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
		 $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
        $this->user_id=$auth->getIdentity()->user_id;
    }

    public function addCashRegisterAction() {

        try {
            $request = $this->getRequest();
            $cr_id = $request->getParam("cr_id");
            $consumer_id = $request->getParam("consumer_id");
            $user_id = $request->getParam("user_id");
            $reciept_id = $request->getParam("reciept_number");

            $transaction_id = $request->getParam("transaction_id");
            $cr_entry_type = $request->getParam("cr_entry_type");
            $cr_amount = $request->getParam("cr_amount");

            $crmapper = new Application_Model_CashRegisterMapper();
            $cr = new Application_Model_CashRegister();
            $params = $request->getParams();


            foreach ($params as $key => $value) {
                $cr->__set($key, $value);
            }

            if ($cr_registers = $crmapper->addNewCashRegister($cr)) {
				 
                $cr_data = array();
                foreach ($params as $key => $value) {


                    $cr_data[$key] = $value;
                }




                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $cr_data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getCashRegisterByIdAction() {

        try {
            $request = $this->getRequest();
            $cr_id = $request->getParam("id");
            $crmapper = new Application_Model_CashRegisterMapper();
            $cr = new Application_Model_CashRegister();
            if ($cash_register = $crmapper->getCashRegisterById($cr_id)) {

                $data = array(
                    'consumer_id' => $cash_register->__get("consumer_id"),
                    'user_id' => $cash_register->__get("user_id"),
                    'receipt_number' => $cash_register->__get("receipt_number"),
                    'transaction_id' => $cash_register->__get("transaction_id"),
                    'cr_entry_type' => $cash_register->__get("cr_entry_type"),
                    'cr_amount' => $cash_register->__get("cr_amount"),
                );



                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function getOutstandingAmountAction()
    {
        try 
        {
            $date = new Zend_Date();
            $date->setTimezone("Asia/Calcutta");
            $date_string = $date->toString("01/MM/yyyy");
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $outstandingAmount = $cashRegisterMapper->getTotalOutstanding($date_string);
            $data = array(
                    'outstanding_amount' => $outstandingAmount
                );
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
        } 
        catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "message" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function getSumOfAllCreditAndDebitAction() {

        try {
            $request = $this->getRequest();
            $date = new Zend_Date();
            $date->setTimezone("Asia/Calcutta");
            $month = $date->toString(Zend_Date::MONTH_NAME);            
            $site_id = $request->getParam("site_id");
            $crmapper = new Application_Model_CashRegisterMapper();
            $cr = new Application_Model_CashRegister();
            $credit = $crmapper->getTotalCollectedByMonth($month, $site_id, 'CREDIT');
            $debit = $crmapper->getTotalCollectedByMonth($month, $site_id, 'DEBIT');
                $data = array(
                    'credit' => $credit,
                    'debit' => $debit,
                    
                );



                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $data
                );
             
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function deleteCashRegisterByIdAction() {
        try {
            $request = $this->getRequest();
            $cr_id = $request->getParam("id");
            $crmapper = new Application_Model_CashRegisterMapper();
            if ($cr = $crmapper->deleteCashRegisterById($cr_id)) {
				$this->_logger->info("cashRegister Id ".$cr_id." has been deleted from cashRegisters by ". $this->_userName.".");
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function getAllCashRegistersAction() {
        try {
            $request = $this->getRequest();
            $site_id = $request->getParam("site_id", NULL);
            $agent_id = $request->getParam("agent_id", NULL);
            $cluster_id = $request->getParam("cluster_id", NULL);
            $cluster_name = $request->getParam("cluster_name", NULL);
			$entry_status = $request->getParam("entry_status");
            $month = $request->getParam("month", NULL);
            $year = $request->getParam("year", NULL);
            $page = $request->getParam("page", 1);
			$cr_entry_type = $request->getParam("cr_entry_type"); 
			$package_type = $request->getParam("package_type");			
            $auth=new My_Auth('user');
            $user=$auth->getIdentity()->user_role;
//          $cluster=$auth->getIdentity()->clusters;
            $state_id=$auth->getIdentity()->state_id;
//          $clusters=explode('c', $cluster); 
          
            $roleSession = new Zend_Session_Namespace('roles');
            $role_sites_id=$roleSession->site_id;
            
			$curr_date = $request->getParam("curr_dates");
		    $last_date = $request->getParam("last_dates");
		      
		    $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
		    $curr_date = $zendDate->toString("yyyy-MM-dd");
		      
		    $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
            $last_date=$zendDate->toString("yyyy-MM-dd");
			 
            if ($site_id == "All") {
                $site_id = NULL;
                if($cluster_name=="All")
                {
                    $cluster_id = NULL;
                }
            }
            if ($agent_id == "All") {
                $agent_id = NULL;
            }
			 if($entry_status!=NULL && $entry_status!="" && $entry_status!='undefined'){
            	$entry_status=explode(",", $entry_status);
            }else{
            	$entry_status=NULL;
            }
			
            $total_pages = 0;
            $current_page = 0;
            $crmapper = new Application_Model_CashRegisterMapper();
             
 
            $crs = $crmapper->getAllCashRegisterBysiteAndCollectionId($site_id, $agent_id, $curr_date,$cluster_id,$user,$state_id,$role_sites_id,$last_date,$entry_status,$cr_entry_type,$package_type);
            
		   if ($crs) { 
                $paginator = Zend_Paginator::factory($crs);
                $paginator->setItemCountPerPage(50);
                $paginator->setCurrentPageNumber($page);
                $chapters = $paginator;
                $total_pages = $paginator->count();
                if ($page > $total_pages) {
                    throw new Exception("No more pages", 555);
                }
                $current_page = $paginator->getCurrentPageNumber();
                $consumersMapper = new Application_Model_ConsumersMapper();

                foreach ($chapters as $cash_register) {
					 
						$consumer_id = $cash_register->__get("consumer_id");
						$consumer = $consumersMapper->getConsumerById($consumer_id);

						$sitesMapper = new Application_Model_SitesMapper();
						$site = $sitesMapper->getSiteById($consumer->__get("site_id"));

						$collection_agent_id = $cash_register->__get("user_id");
						$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
						$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($collection_agent_id);
						$cr_type = $cash_register->__get("cr_entry_type");
						$cr_type_short = "";
						if (strlen($cr_type) > 0) {
							$cr_type_short = $cr_type[0];
						}
						$date = new Zend_Date($cash_register->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
						//$date->setTimezone("Asia/Calcutta");
						$timestamp = $date->toString("MMM dd, yyyy HH:mm");
						
						$userMapper=new Application_Model_UsersMapper();
	                     
						if ($cash_register->__get("cr_entry_type")!='DEBIT' || $cash_register->__get("transaction_type")=='(W)') {
							if ($cash_register->__get("transaction_type")=='(W)') {
								$transaction_type="Website";
								$users = $userMapper->getUserById($cash_register->__get("user_id"));
								$entry_by=$users->__get("user_fname") . " " . $users->__get("user_lname");
							} elseif ($cash_register->__get("transaction_type")=='(M)')  {
								$transaction_type="POS";
								$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cash_register->__get("user_id"));
								$entry_by=$collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
							}elseif ($cash_register->__get("transaction_type")=='(S)' && $cash_register->__get("entry_status")!='SCHEME' && $cash_register->__get("cr_entry_type")!='DISCOUNT')  {
								$transaction_type="SMS";
								$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cash_register->__get("user_id"));
								$entry_by=$collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
							}else{ 
								$entry_by="Server";
								$transaction_type="Server";
							} 
						}else{
								$entry_by="Server";
								$transaction_type="Server";
						} 
						
						$data = array(
							'cr_id' => $cash_register->__get("cr_id"),
							'consumer_id' => $consumer_id,
							"consumer_name" => $consumer->__get("consumer_name"),
							"consumer_connection_id" => $consumer->__get("consumer_connection_id"),
							'user_id' => $cash_register->__get("user_id"),
							'receipt_number' => $cash_register->__get("receipt_number"),
							'transaction_id' => $cash_register->__get("transaction_id"),
							'cr_entry_type' => $cr_type_short,
							'cr_entry_type_long' => $cr_type,
							'cr_amount' => $cash_register->__get("cr_amount"),
							'timestamp' => $timestamp,
							'site_id' => $site->__get("site_id"),
							'site_name' => $site->__get("site_name"),
							'collection_agent_id' => $collectionAgent->__get("collection_agent_id"),
							'agent_fname' => $collectionAgent->__get("agent_fname"),
							'agent_lname' => $collectionAgent->__get("agent_lname"),
							'cr_status' => $cash_register->__get("cr_status"),
							'entry_status' => $cash_register->__get("entry_status"),
							'transaction_type'=>$transaction_type,
							'entry_by'=>$entry_by
						);

						$cr_arr[] = $data;
					 
                }
				$total_amounts=$crmapper->getACTCHGBills($site_id,$curr_date,$last_date,$agent_id,$role_sites_id,$cr_entry_type,true,$package_type);
					if($entry_status==NULL || $entry_status=="" || $entry_status=='undefined'){
						$entry_status=array("'ACT'","'CHG'","'EED'","'MED'","'OTHERS'","'WATER'","'SD'","'METER'");
					}
					 
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                    "total_page" => $total_pages,
                    "current_page" => $current_page,
                	"CREDIT"=>intval($total_amounts["CREDIT"]+$total_amounts["SD"]),
                	"DEBIT"=>intval($total_amounts["DEBIT"]+$total_amounts["SD"]),
                	"ACT"=>in_array("'ACT'", $entry_status)?intval($total_amounts["ACT"]):0, 
					"CHG"=>in_array("'CHG'", $entry_status)?intval($total_amounts["CHG"]):0,
					"EED"=>in_array("'EED'", $entry_status)?intval($total_amounts["EED"]):0,
					"MED"=>in_array("'MED'", $entry_status)?intval($total_amounts["MED"]):0,
					"OTHERS"=>in_array("'OTHERS'", $entry_status)?intval($total_amounts["OTHERS"]):0,
					"WATER"=>in_array("'WATER'", $entry_status)?intval($total_amounts["WATER"]):0,
					"METER"=>in_array("'METER'", $entry_status)?intval($total_amounts["METER"]):0,
					"SD"=>in_array("'SD'", $entry_status)?intval($total_amounts["SD"]):0,
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $cr_arr,
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND",
                    "crs"=>$crs
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => $e->getCode(),
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updateCashRegisterByIdAction() {
        try {
            $request = $this->getRequest();
            $cr_amount = $request->getParam("cr_amount");
            $cr_id = $request->getParam("cr_id");
            $consumer_id = $request->getParam("consumer_id");
            $user_id = $request->getParam("user_id");
            $transaction_id = $request->getParam("transaction_id");
            $cr_entry_type = $request->getParam("cr_entry_type");
			$entry_status= $request->getParam("entry_status");
            $consumer_connection_id = $request->getParam("consumer_connection_id");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumer = $consumersMapper->getConsumerByConnectionId($consumer_connection_id);
            $consumer_id = $consumer->__get("consumer_id");
            $crmapper = new Application_Model_CashRegisterMapper();
            $cr=new Application_Model_CashRegister();
            $lastCr = $crmapper->getCashRegisterById($cr_id);
            
            $cr->__set("cr_id", $cr_id);
            $cr->__set("cr_amount", $cr_amount);
            $cr->__set("consumer_id", $consumer_id);
            $cr->__set("user_id", $user_id);
            $cr->__set("transaction_id", $transaction_id);
            $cr->__set("cr_entry_type", $cr_entry_type);
            $cr->__set("timestamp",$lastCr->__get("timestamp"));
			$cr->__set("entry_status", $entry_status);
            if ($crmapper->updateCashRegister($cr)) {
				
				$lastCr_arr = array(
            			'consumer_id' => $lastCr->__get("consumer_id"),
            		    'cr_entry_type' => $lastCr->__get("cr_entry_type"),
            			'cr_amount' => $lastCr->__get("cr_amount"),
							'entry_status' => $lastCr->__get("entry_status")
            	);
            	$newCr_arr = array(
            			'consumer_id' => $cr->__get("consumer_id"),
            			'cr_entry_type' => $cr->__get("cr_entry_type"),
            			'cr_amount' => $cr->__get("cr_amount"),
							'entry_status' => $lastCr->__get("entry_status")
            	);
            	$lastCr_diff=array_diff($lastCr_arr,$newCr_arr);
            	$newCr_diff=array_diff($newCr_arr,$lastCr_arr);
            	 
            	$change_data="";
            	foreach ($lastCr_diff as $key => $value)
            	{
            		$change_data.=$key." ".$lastCr_diff[$key]." change to ".$newCr_diff[$key]." ";
            	}
            	$this->_logger->info("CashRegister Id ".$cr_id." has been updated where ".$change_data." by ". $this->_userName.".");
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
        } catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

   public function insertCashRegisterAction(){
    	try {
    		$request = $this->getRequest();
    		$consumer_id = $request->getParam("consumer_id");
    		$cr_amount = $request->getParam("cr_amount");
    		$cr_entry_type = $request->getParam("cr_entry_type");
    		$transaction_date = $request->getParam("date");
    		$remark = $request->getParam("remark");
			$check = $request->getParam("check");
    		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    		$cashRegister=new Application_Model_CashRegister();
    		$consumers=new Application_Model_ConsumersMapper();
    		$siteMapper=new Application_Model_SitesMapper();
			if($transaction_date==null || $transaction_date=="" || $transaction_date=='undefined'){
				$date = new Zend_Date(); 
				$zendDate->setTimezone("Asia/Calcutta");
				$timestamp=$zendDate->toString("yyyy-MM-dd HH:mm:ss"); 
			}else{
				$zendDate = new Zend_Date($transaction_date,"dd-MM-yyyy HH:mm:ss");
				$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
			} 
    		 
    		$times = $zendDate->toString("ddMMYYHHmmss".rand(10,99));
    		$tcr_amount = str_pad($cr_amount, 4, "0", STR_PAD_LEFT);
    		$transaction_id = $times . "-" . $tcr_amount;
    		$receipt_number="CA-".$this->user_id; 
    		$user_id=0;$entry_type=NULL;
			if($cr_entry_type=='DEBIT'){
				$entry_type=$request->getParam("tran_type");
			}elseif($cr_entry_type=='CREDIT'){
				$entry_type=$request->getParam("tran_type");
			}elseif($cr_entry_type=='DISCOUNT'){
				$entry_type=$request->getParam("tran_type"); 
			}else{
				$entry_type='ACT';
			} 
			$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
			$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
			
    		$cashRegister->__set("consumer_id", $consumer_id);
    		$cashRegister->__set("cr_entry_type", $cr_entry_type);
    		$cashRegister->__set("cr_amount", $cr_amount);
    		$cashRegister->__set("transaction_id", $transaction_id);
    		$cashRegister->__set("user_id", $this->user_id);
    		$cashRegister->__set("user_val", $this->user_id);
    		$cashRegister->__set("receipt_number", $receipt_number);
    		$cashRegister->__set("transaction_type", "(W)");
    		$cashRegister->__set("remark", $remark);
    		$cashRegister->__set("timestamp",$timestamp);
			$cashRegister->__set("entry_status",$entry_type);
			$cashRegister->__set("package_id", $consumerPackages[0]["package_id"]);  
			 
    		if($res=$cashRegisterMapper->addNewCashRegister($cashRegister)){ 
			 
			
				
				if(($entry_type=='OTHERS' || $entry_type=='MED') && $cr_entry_type=='CREDIT'){
						$date_val = new Zend_Date();
						$date_val->setTimezone("Asia/Kolkata");
						 
						$cashRegister_val = new Application_Model_CashRegister();
						$cashRegister_val->__set("consumer_id", $consumer_id);
						$cashRegister_val->__set("cr_entry_type", 'DEBIT');
						$cashRegister_val->__set("cr_amount", $cr_amount);
						$cashRegister_val->__set("transaction_id", $transaction_id);
						$cashRegister_val->__set("user_id", $this->user_id);
						$cashRegister_val->__set("receipt_number", "CA-" . $this->user_id);
						$cashRegister_val->__set("entry_status", $entry_type); 
						$cashRegister_val->__set("package_id", $consumerPackages[0]["package_id"]); 
						$cashRegister_val->__set("timestamp", $zendDate->toString("yyyy-MM-dd 04:05:30"));
						if($sender=="mobile"){
							$cashRegister_val->__set("transaction_type", "(M)");
						}else{
							$cashRegister_val->__set("transaction_type", "(S)");
						}
						 
						$cashRegisterMapper->addNewCashRegister($cashRegister_val); 
					}
				
					$consumer = $consumers->getConsumerById($consumer_id);
					if($consumer->__get("site_id")==62 || $consumer->__get("site_id")==31){
						$meterReadingMapper=new Application_Model_MeterReadingsMapper();
						
						$meters=$meterReadingMapper->getLatestReadingByConsumerId($consumer->__get("consumer_id"));
						$serial=$meters->__get("meter_no");
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$siteOutstaning=$cashRegisterMapper->getMonthlyDueBySiteId($consumer->__get("site_id"),null,null,null,$consumer->__get("consumer_id"));
						$outstanding=intval($siteOutstaning["debit_amount"])-intval($siteOutstaning["credit_amount"]);
						
						//$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
						if($consumer->__get("site_id")==62){ 
							$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
						}else{
							$url="https://sparkcloud-tara-dumarsan.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
						} 
						if(intval($consumer->__get("credit_limit")<=intval($outstanding))){  
								$activitys='off'; 
								$param="state=".$activitys."";
								if($consumer->__get("site_id")==62){
									$destination=array(
										'Authentication-Token: .eJwNx8sNgEAIBcBeOEuy4fNWajEeEKT_EnRuc5FWYqzA3brZ3IoD58MZWac4dqjQQa6D1FnTSB8pwLzqtV7x14TuD-xVE7c.DS9luw.Oo8U_ohr0GHFlfZMPgpqb03sa8I'
									);
								}else{
									$destination=array(
										'Authentication-Token: .eJwFwcsNgEAIBcBeOEuy2X38ajEeRKD_Epy5aZ2wgQmvqWQMhH03eKQrj3l2gC6qT81tBUoSkQJ9fW8PE9V0aXp-2MUTGw.DZESAA.9aH1COipJ3UNcpkBGKk_BUZiNc0'
									);
								}
								$result=$this->CallCURL($url,'POST',$param,$destination);
									
								$data=(json_decode($result,true));
						}else{
								$activitys='on'; 
								$param="state=".$activitys."";
								if($consumer->__get("site_id")==62){
									$destination=array(
										'Authentication-Token: .eJwNx8sNgEAIBcBeOEuy4fNWajEeEKT_EnRuc5FWYqzA3brZ3IoD58MZWac4dqjQQa6D1FnTSB8pwLzqtV7x14TuD-xVE7c.DS9luw.Oo8U_ohr0GHFlfZMPgpqb03sa8I'
									);
								}else{
									$destination=array(
										'Authentication-Token: .eJwFwcsNgEAIBcBeOEuy2X38ajEeRKD_Epy5aZ2wgQmvqWQMhH03eKQrj3l2gC6qT81tBUoSkQJ9fW8PE9V0aXp-2MUTGw.DZESAA.9aH1COipJ3UNcpkBGKk_BUZiNc0'
									);
								}
								$result=$this->CallCURL($url,'POST',$param,$destination);
									
								$data=(json_decode($result,true));
						}
					}
					
				
    			$consumer=$consumers->getConsumerById($consumer_id);
    			$consumer_name=$consumer->__get('consumer_name');
    			$consumer_connection_id=$consumer->__get('consumer_connection_id');
    			$site_id=$consumer->__get('site_id');
    			$site=$siteMapper->getSiteById($site_id);
    			$site_name=$site->__get('site_name');
    			$cr_type_short = "";
    			if (strlen($cr_entry_type) > 0) {
    				$cr_type_short = $cr_entry_type[0];
    			}
    			$date = new Zend_Date($timestamp);
    			$timestamp = $date->toString("MMM dd, yyyy HH:mm");
    			$data=array(
    					"cr_id" => $res,
    					"user_id" => $user_id,
    					"receipt_number" => $receipt_number,
    					"transaction_id" =>$transaction_id,
    					"cr_amount" => $cr_amount,
    					"timestamp" => $timestamp,
    					"cr_type_short" => $cr_type_short,
    					"cr_entry_type_long" => $cr_entry_type,
    					"consumer_id"=>$consumer_id,
    					"consumer_name" => $consumer_name,
    					"consumer_connection_id" => $consumer_connection_id,
    					"site_name"=>$site_name,
    					"cr_status"=>'ACTIVE',
    					"remark"=>$remark,
    			);
    			 
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $data
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while adding"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    			 
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
   function CallCURL($url, $method = 'GET', $params = null, $destination = [])
    {
		if (count($destination) > 0) {
    		foreach ($destination as $item) {
    			$headers[] = $item;
    		}
    	}
    
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	if ($method == 'GET') {
    		if (count($params) > 0) {
    			$url .= '?' . http_build_query($params);
    		}
    	} else {
    		curl_setopt($ch, CURLOPT_POST, true);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    	}
    	curl_setopt($ch, CURLOPT_URL, $url);
    
    	$result = curl_exec($ch);
    	curl_close($ch);
    	return $result;
    } 
   
    public function getPendingOtpAction() {
        try {
        	$request = $this->getRequest();
    		$site_name = $request->getParam("site");
    		$state_id = $request->getParam("state_id");
    		$site_id = $request->getParam("site_id");
    		$year =$request->getParam("year");
			$mon =$request->getParam("month");
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    
    		$consumersMapper = new Application_Model_ConsumersMapper();
    		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    		$siteMapper=new Application_Model_SitesMapper();
    		$total_month=array();
    		$cs = array();
    		$css = array();
    		 
			 
			
    		if($site_name !=NULL && $site_name !=""){
    			$siteMapper=new Application_Model_SitesMapper();
            	$site_data=$siteMapper->getSiteByName($site_name);
				$site_id=$site_data["site_id"];
			 
    		}else{
				if($site_id=='ALL'){
					$site_id=NULL;
				}
				if($state_id=='ALL'){
					$state_id=NULL;
				}
    		}
			$arr=array();
				$consumerData= $cashRegisterMapper->getPendingOtp($site_id,$state_id,$role_sites_id,$mon,$year);
				if($consumerData){
				foreach($consumerData as $consumers)
				{
					$date = new Zend_Date($consumers["timestamp"], "yyyy-MM-dd HH:mm:ss");
                    $timestamp = $date->toString("MMM dd, yyyy HH:mm");
                    $siteName=$siteMapper->getSiteById($consumers["site_id"]);
                    
					$data = array(
                        'consumer_id' => $consumers["consumer_id"],
                        'consumer_name' => $consumers["consumer_name"],
                        'consumer_connection_id' => $consumers["consumer_connection_id"],
                        'consumer_act_charge' => $consumers["consumer_act_charge"],
                        'otpin' => $consumers["otpin"],
                        'last_payment_date'=>$timestamp,
						'site_name'=>$siteName->__get("site_name"),
						'month_name'=>$consumers["month_name"],
                       
                    );
					$arr[]=$data;
				}
				}
			
         $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS",
                   
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $arr,
                );
        } catch (Exception $e) {
            $meta = array(
                "code" => $e->getCode(),
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
       echo $json;
    }
	
   public function getOutstandingAmountBySiteIdAction()
    {
    	try
    	{
    		$request = $this->getRequest();
    	 
    		$site_id = $request->getParam("site_id");
    		$state_id = $request->getParam("state_id");
    		
    		$curr_date = $request->getParam("curr_date");
	        $last_date = $request->getParam("last_date");
	            
	            if($state_id=="ALL"){$state_id=NULL;}
	            if($site_id=="ALL"){$site_id=NULL;}
	            
	        $zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
	        $curr_date = $zendDate->toString("yyyy-MM-dd");
	            
	        $zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
	        $last_date=$zendDate->toString("yyyy-MM-dd");
	        
    		 $roleSession = new Zend_Session_Namespace('roles');
    		 $role_sites_id=$roleSession->site_id;
    		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
    		   
    		$outstandingAmount = $cashRegisterMapper->getTotalOutstandingAmountBySiteId($curr_date,$site_id,$state_id,$role_sites_id);
    	 
    		$data = array(
    				'outstanding_amount' =>$outstandingAmount
    		);
    		$meta = array(
    				"code" => 200,
    				"message" => "SUCCESS"
    		);
    		$arr = array(
    				"meta" => $meta,
    				"data" => $data
    		);
    	}
    	catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"message" => $e->getMessage()
    		);
    
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
	
public function pendingCashTransactionAction(){
    	try {
    		/* $request = $this->getRequest();
    		$site_id = $request->getParam("site_id", NULL);
    		$agent_id = $request->getParam("agent_id", NULL);
    		$cluster_id = $request->getParam("cluster_id", NULL);
    		$cluster_name = $request->getParam("cluster_name", NULL);
    		$month = $request->getParam("month", NULL);
    		$year = $request->getParam("year", NULL);
    		$page = $request->getParam("page", 1);
    		
    		$auth=new My_Auth('user');
    		$user=$auth->getIdentity()->user_role;
    		$state_id=$auth->getIdentity()->state_id;
    		$roleSession = new Zend_Session_Namespace('roles');
    		$role_sites_id=$roleSession->site_id;
    	
    		if ($site_id == "All") {
    			$site_id = NULL;
    			if($cluster_name=="All")
    			{
    				$cluster_id = NULL;
    			}
    		}
    		
    		if ($agent_id == "All") {
    			$agent_id = NULL;
    		} */
    		$total_pages = 0;
    		$current_page = 0;
    		$crmapper = new Application_Model_CashRegisterMapper();
    		/* if ($month != NULL and $year != NULL) {
    			$greater_than_date = array(
    					"month" => $month,
    					"year" => $year
    			);
    		} else {
    			$greater_than_date = NULL;
    		} */
    		$page=1;
				$request = $this->getRequest();
				$site_id = $request->getParam("site_id");
				if ($site_id == "ALL") {
					$site_id = NULL;
				}
				$roleSession = new Zend_Session_Namespace('roles');
				$role_sites_id=$roleSession->site_id;
		      
		      $crs = $crmapper->getAllCashRegisterPending($role_sites_id,$site_id);
    		if ($crs) {
    			$paginator = Zend_Paginator::factory($crs);
    			$paginator->setItemCountPerPage(30);
    			$paginator->setCurrentPageNumber($page);
    			$chapters = $paginator;
    			$total_pages = $paginator->count();
    			if ($page > $total_pages) {
    				throw new Exception("No more pages", 555);
    			}
    			$current_page = $paginator->getCurrentPageNumber();
    			$consumersMapper = new Application_Model_ConsumersMapper();
				$userMapper=new Application_Model_UsersMapper();
    			foreach ($crs as $cash_register) {
    				$consumer_id = $cash_register->__get("consumer_id");
    				$consumer = $consumersMapper->getConsumerById($consumer_id);
    	
    				$sitesMapper = new Application_Model_SitesMapper();
    				$site = $sitesMapper->getSiteById($consumer->__get("site_id"));
    	
    				$collection_agent_id = $cash_register->__get("user_id");
    				$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    				
    				$cr_type = $cash_register->__get("cr_entry_type");
    				$cr_type_short = "";
    				if (strlen($cr_type) > 0) {
    					$cr_type_short = $cr_type[0];
    				}
					if($cash_register->__get("transaction_type")=="(W)"){
						$users=$userMapper->getUserById($collection_agent_id);
						$fname=$users->__get("user_fname");
						$lname=$users->__get("user_lname");
					}elseif($cash_register->__get("transaction_type")=="(M)" || ($cash_register->__get("transaction_type")=="(S)" && $collection_agent_id!=NULL)){
						$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($collection_agent_id);
						$fname=$collectionAgent->__get("agent_fname");
						$lname=$collectionAgent->__get("agent_lname");
					}else{
						$fname="Server";
						$lname="";
					}
    				$date = new Zend_Date($cash_register->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
    				//$date->setTimezone("Asia/Calcutta"); 
    				$timestamp = $date->toString("MMM dd, yyyy HH:mm");
    				$data = array(
    						'cr_id' => $cash_register->__get("cr_id"),
    						'consumer_id' => $consumer_id,
    						"consumer_name" => $consumer->__get("consumer_name"),
    						"consumer_connection_id" => $consumer->__get("consumer_connection_id"),
    						'user_id' => $cash_register->__get("user_id"),
    						'receipt_number' => $cash_register->__get("receipt_number"),
    						'transaction_id' => $cash_register->__get("transaction_id"),
    						'cr_entry_type' => $cr_type_short,
    						'cr_entry_type_long' => $cr_type,
    						'cr_amount' => $cash_register->__get("cr_amount"),
    						'timestamp' => $timestamp,
    						'site_id' => $site->__get("site_id"),
    						'site_name' => $site->__get("site_name"),
    						'collection_agent_id' => $collection_agent_id,
    						'agent_fname' => $fname,
    						'agent_lname' => $lname,
    						'cr_status' => $cash_register->__get("cr_status"),
    						'remark' => $cash_register->__get("remark"),
							'entry_status' => $cash_register->__get("entry_status"),
    				);
    	
    				$cr_arr[] = $data;
    			}
    	
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS",
    					"total_page" => $total_pages,
    					"current_page" => $current_page,
    					 
    			);
    			$arr = array(
    					"meta" => $meta,
    					"data" => $cr_arr,
    			);
    		} else {
    			$meta = array(
    					"code" => 404,
    					"message" => "NOTFOUND",
    					"crs"=>$crs
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	} catch (Exception $e) {
    		$meta = array(
    				"code" => $e->getCode(),
    				"messgae" => $e->getMessage()
    		);
    	
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
 

    
    public function insertCreditTransactionByIdAction(){
    	try {
    		$request=$this->getRequest();
    		$cr_id=$request->getParam("id");
    		$remark=$request->getParam("remark");
			$status=$request->getParam("status");
    		$cashMapper=new Application_Model_CashRegisterMapper();
    		$cashVal=new Application_Model_CashRegister();
    		$cr_id_value=explode(",", $cr_id);
    		if($cashs=$cashMapper->getAllCashRegisterPendingById($cr_id_value)){
    				if($status!='reject'){
						foreach ($cashs as $cash){
						
							$updateRecord=$cashMapper->updateStatus($cash->__get("cr_id"),'ACTIVE','(W)',$remark,NULL,$this->user_id);
							//$cash->__set("approved_by", $this->user_id); 
							//$cash->__set("remark", $remark);
							//$cashReg=$cashMapper->addNewCashRegister($cash,null,"pending"); 
						}
						///$cashMapper->updateCashRegisterPendingById($cr_id_value);
					}else{
						$cashMapper->updateCRPendingById($cr_id_value,$remark,$this->user_id);
					}
    			$meta = array(
    					"code" => 200,
    					"message" => "SUCCESS"
    			);
    			$arr = array(
    					"meta" => $meta,
    	
    			);
    		} else {
    			$meta = array(
    					"code" => 401,
    					"message" => "Error while deleting"
    			);
    			$arr = array(
    					"meta" => $meta
    			);
    		}
    	}catch (Exception $e) {
    		$meta = array(
    				"code" => 501,
    				"messgae" => $e->getMessage()
    		);
    		 
    		$arr = array(
    				"meta" => $meta
    		);
    	}
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    }
}