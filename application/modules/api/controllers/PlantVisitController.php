<?php

class Api_PlantVisitController extends Zend_Controller_Action {

    public function init() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
       // header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userName=$auth->getIdentity()->user_fname;
        $this->_userId=$auth->getIdentity()->user_id;
    }
    public function addPlantVisitAction(){
        
        try {
            $request = $this->getRequest();
            $tmp_date_of_visit = $request->getParam("date_of_visit");
            $hours_on_plant = $request->getParam("hours_on_plant");
            $attachment = $request->getParam("attachment");
            $issues = $request->getParam("issues");
            $remark = $request->getParam("remark");
            $site_id = $request->getParam("site_id");

            if($tmp_date_of_visit){
                $zendDate2 = new Zend_Date($tmp_date_of_visit,"MM-dd-yyyy");
                $date_of_visit = $zendDate2->toString("yyyy-MM-dd");
            }
            $date = new Zend_Date();
            $date->setTimezone("Asia/Calcutta");
            $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
            
            $plantvisitMapper = new Application_Model_PlantVisitMapper();
            $plantvisit = new Application_Model_PlantVisit();

            $plantvisit->__set("date_of_visit",$date_of_visit);
            $plantvisit->__set("hours_on_plant",$hours_on_plant);
            $plantvisit->__set("attachment",$attachment);
            $plantvisit->__set("issues",$issues);
            $plantvisit->__set("remark",$remark);
            $plantvisit->__set("entry_by",$this->_userId);
            $plantvisit->__set("entry_date",$timestamp);
            $plantvisit->__set("site_id",$site_id);
           
            if($plant_visit_id = $plantvisitMapper->addNewPlantVisit($plantvisit)){

                $this->_logger->info("New Visit ID ".$plant_visit_id." has been created in States by ". $this->_userName.".");
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                );
            } else {
                $meta = array(
                        "code" => 401,
                        "message" => "Error while adding"
                );
                $arr = array(
                        "meta" => $meta
                );
            }
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function getAllPlantVisitsAction(){
    
        try {
            $visitsMapper = new Application_Model_PlantVisitMapper();
            $userMapper=new Application_Model_UsersMapper();
           	$siteMapper=new Application_Model_SitesMapper();
            
            $visits = $visitsMapper->getAllPlantVisits();

            if(count($visits) >0){
                foreach ($visits as $visit) {
                    $users=$userMapper->getUserById($visit->__get("entry_by"));
                	$sites=$siteMapper->getSiteById($visit->__get("site_id"));
                    $data = array(
                        "id" => $visit->__get("id"),
                    	"site_id" => $visit->__get("site_id"),
						"site_name" => $sites->__get("site_name"),
                    	"state_id"=>$sites->__get("state_id"),
                        "date_of_visit" => date("d-M-Y",strtotime($visit->__get("date_of_visit"))),
                        "hours_on_plant" => (int) $visit->__get("hours_on_plant"),
                        "attachment" => $visit->__get("attachment"),
                        "issues" => $visit->__get("issues"),
                        "remark" => $visit->__get("remark"),
                        "entry_by" => $users->__get("user_fname")." ".$users->__get("user_lname"),
                        "entry_date" => date("d-M-Y g:i a",strtotime($visit->__get("entry_date"))),
                       
                    );
                    $query_arr[] = $data;
                }
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $query_arr,
                );
    
            }
            else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
    
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    public function deletePlantVisitByIdAction(){
    
        try {
            $request = $this->getRequest();
            $visit_id = $request->getParam("id");

            $plantVisitMapper = new Application_Model_PlantVisitMapper();
            
            if($plantVisit = $plantVisitMapper->deletePlantVisitById($visit_id)){
                // $this->_logger->info("State Id ".$customer_query_id." has been deleted from States by ". $this->_userName.".");
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
    
                );
            } else {
                $meta = array(
                        "code" => 401,
                        "message" => "Error while deleting"
                );
                $arr = array(
                        "meta" => $meta
                );
            }
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function updatePlantVisitByIdAction(){

        try {
            $request = $this->getRequest();
            $id = $request->getParam("id");
            $tmp_date_of_visit = $request->getParam("date_of_visit");
            $hours_on_plant = $request->getParam("hours_on_plant");
            $attachment = $request->getParam("attachment");
            $issues = $request->getParam("issues");
            $remark = $request->getParam("remark");
            $site_id = $request->getParam("site_id");

            $plantVisitMapper = new Application_Model_PlantVisitMapper();
            $plantvisit = new Application_Model_PlantVisit();
            
            $zendDate1 = new Zend_Date($tmp_date_of_visit,"MM-dd-yyyy");
            $date_of_visit = $zendDate1->toString("yyyy-MM-dd");
        
            $plantvisit->__set("id",$id);
            $plantvisit->__set("date_of_visit",$date_of_visit);
            $plantvisit->__set("hours_on_plant",$hours_on_plant);
            $plantvisit->__set("attachment",$attachment);
            $plantvisit->__set("issues",$issues);
            $plantvisit->__set("remark",$remark);
            $plantvisit->__set("site_id",$site_id);

            if($plantVisitMapper->updatePlantVisit($plantvisit)){
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
    
                );
            } else {
                $meta = array(
                        "code" => 401,
                        "message" => "Error while updating"
                );
                $arr = array(
                        "meta" => $meta
                );
            }
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }
    
}