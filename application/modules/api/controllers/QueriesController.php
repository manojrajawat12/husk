<?php
class Api_QueriesController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
	}
	
	public function addQueriesAction(){
		
		try {
			$request = $this->getRequest();
			$query = $request->getParam("query");
			// $sub_query = $request->getParam("sub_query");
			$temp_add_sub_query = $request->getParam("add_sub_query");
			$temp_queries = explode(',', $temp_add_sub_query);

			$queryMapper = new Application_Model_QueryMapper();
			$queries = new Application_Model_Query();

			$queries->__set("query",$query);
			$queries->__set("parent_id",0);
			$parent_id = $queryMapper->addNewQuery($queries);

			if($parent_id) {
				if($temp_queries){
					foreach ($temp_queries as $temp_query) {
						$queries->__set("query",$temp_query);
						$queries->__set("parent_id",$parent_id);

						$queryMapper->addNewQuery($queries);
					}
				}
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
   
    public function getAllQueriesAction(){
    
        try {
            $queriesMapper = new Application_Model_QueryMapper();
            $parent = 0;           
            $queries = $queriesMapper->getAllParentQueries($parent);

            if(count($queries) >0){
                foreach ($queries as $query) {
                	$reasons = array();
                	$parent_id = $query["id"];
                	$reasons = $queriesMapper->getAllParentQueries($parent_id);
                    $data = array(
                            "query_id" => $query["id"],
                            "query" => $query["query"],
                            "parent_id" => $query["parent_id"],
                            "reasons" => $reasons,
                    );
                    $query_arr[] = $data;
                }
                $meta = array(
                        "code" => 200,
                        "message" => "SUCCESS"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" => $query_arr,
                );
    
            }
            else{
                $meta = array(
                        "code" => 200,
                        "message" => "Error while getting"
                );
                $arr = array(
                        "meta" => $meta,
                        "data" =>array(),
                );
            }
    
    
        }catch (Exception $e) {
            $meta = array(
                    "code" => 501,
                    "messgae" => $e->getMessage()
            );
    
            $arr = array(
                    "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

	public function deleteQueryByIdAction(){
	
		try {
			$request = $this->getRequest();
			$query_id = $request->getParam("id");
			$queryMapper = new Application_Model_QueryMapper();
			if($query = $queryMapper->deleteQueryById($query_id)){
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateQueryByIdAction(){
		
		try {
			// $data = json_decode(file_get_contents("php://input"));
			$request = $this->getRequest();
			$query_id = $request->getParam("id");
			$query = $request->getParam("query");
			
			$queryMapper = new Application_Model_QueryMapper();
			$queries = new Application_Model_Query();

			$temp = $queryMapper->updateQueryById($query_id, $query);

			if($temp){ 
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while updating"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	 
}