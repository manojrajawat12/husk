<?php
class Api_ConsumerTypeController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
		$this->_user_id=$auth->getIdentity()->user_id;
	}
	
	public function addConsumerTypeAction(){
	
		try {
			 
			$request=$this->getRequest();
			$consumer_name=$request->getParam("consumer_name");
			$consumer_code=$request->getParam("consumer_code");
			$status=$request->getParam("status");
			$parent_id=$request->getParam("parent_id");
			$people_impact=$request->getParam("people_impact");
									 
			$ConsumerTypeMapper=new Application_Model_ConsumerTypeMapper();
			$ConsumerType= new Application_Model_ConsumerType();
			$ConsumerType->__set("consumer_type_name",$consumer_name);
			$ConsumerType->__set("consumer_type_code",$consumer_code);
			$ConsumerType->__set("status",$status);
			$ConsumerType->__set("parent_id",$parent_id);
			$ConsumerType->__set("people_impact",$people_impact);
				
			if($ConsumerType=$ConsumerTypeMapper->addNewConsumerType($ConsumerType)){
	
					$this->_logger->info("New Consumer Type ".$dg_id." has been created by ". $this->_userName.".");
			/*	$this->_logger->info("New DG ID ".$DgId." has been created in Equipment by ". $this->_userName.".");
				
				$sitesMapper=new Application_Model_SitesMapper();
				$sites=$sitesMapper->getSiteById($site_id);
				
				$zendDate = new Zend_Date($date_of_purchase,"yyyy-MM-dd");
				$timestamp = $zendDate->toString("MM/dd/yyyy");*/
				
				$data=array(
				        "consumer_name" => $consumer_name,
				        "consumer_code" => $consumer_code,
						"id" => $id,
				        "people_impact" => $people_impact,
					
				);
	
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $data
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function getAllConsumerTypeAction(){
	
		try {
		    $ConsumerTypeMapper=new Application_Model_ConsumerTypeMapper();
			
		    $ConsumerTypes=$ConsumerTypeMapper->getAllConsumerType();
	
		    if(count($ConsumerTypes) >0){
		        foreach ($ConsumerTypes as $ConsumerType) {
										
					$data=array(
					    "consumer_type_id" => $ConsumerType->__get("consumer_type_id"),
					    "consumer_type_name" => $ConsumerType->__get("consumer_type_name"),
					    "consumer_type_code" => $ConsumerType-> __get("consumer_type_code"),
					    "type_id" => $ConsumerType->__get("type_id"),
					    "people_impact" => $ConsumerType->__get("people_impact"),
						"status"=>$ConsumerType->__get("status"),
						"parent_id"=>$ConsumerType->__get("parent_id"), 
							
					);
					
					$ConsumerType_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
				        "data" => $ConsumerType_arr,
				);
	
			}else{
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" =>array(),
				);
			}
	
	
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function deleteConsumerTypeByIdAction(){
	
		try {
			$request=$this->getRequest();
			$id=$request->getParam("id");
			$ConsumerTypeMapper=new Application_Model_ConsumerTypeMapper();
			if($ConsumerType=$ConsumerTypeMapper->deleteConsumerTypeById($id)){
			//	$this->_logger->info("DG Id ".$Dgid." has been deleted from DG table by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while deleting"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}

	public function updateConsumerTypeByIdAction(){
	
		try {
			$request=$this->getRequest();
			$consumer_type_id=$request->getParam("id");
			$consumer_name=$request->getParam("consumer_name");
			$consumer_code=$request->getParam("consumer_code");
			$status=$request->getParam("status");
			$parent_id=$request->getParam("parent_id");
			$people_impact=$request->getParam("people_impact");
									 
			$ConsumerTypeMapper=new Application_Model_ConsumerTypeMapper();
			$ConsumerType= new Application_Model_ConsumerType();
			$ConsumerType->__set("consumer_type_name",$consumer_name);
			$ConsumerType->__set("consumer_type_code",$consumer_code);
			$ConsumerType->__set("status",$status);
			$ConsumerType->__set("parent_id",$parent_id); 
			$ConsumerType->__set("people_impact",$people_impact);
			$ConsumerType->__set("consumer_type_id",$consumer_type_id); 
			
			if($ConsumerTypes=$ConsumerTypeMapper->updateConsumerType($ConsumerType)){
	
				$this->_logger->info("Consumer Type ".$dg_id." has been updated by ". $this->_userName.".");
				
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
	
				);
			} else {
				$meta = array(
						"code" => 401,
						"message" => "Error while adding"
				);
				$arr = array(
						"meta" => $meta
				);
			}
		}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	
	 
}