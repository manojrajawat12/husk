<?php
class Api_TypeOfActivitesController extends Zend_Controller_Action {

	public function init() {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		header('Content-Type: application/json');
		$logger = new My_Logger();
		$this->_logger = $logger->getLogger();
		$auth=new My_Auth('user');
		$this->_userName=$auth->getIdentity()->user_fname;
		$this->user_id=$auth->getIdentity()->user_id;
	}
		
	public function getAllActvitiesAction(){
		
		try {
			$activitiesMapper = new Application_Model_TypesOfActivitiesMapper();
			
			$activities = $activitiesMapper->getAllActivities();
			
			foreach ($activities as $activity) {
				$data = array(
						"id" => $activity->__get("id"),
						"activity" => $activity->__get("activity"),
					);

			$state_arr[]=$data;
				}
				$meta = array(
						"code" => 200,
						"message" => "SUCCESS"
				);
				$arr = array(
						"meta" => $meta,
						"data" => $state_arr,
				);
			}catch (Exception $e) {
			$meta = array(
					"code" => 501,
					"messgae" => $e->getMessage()
			);
	
			$arr = array(
					"meta" => $meta
			);
		}
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
	}
	
	public function addActivityTypeAction(){
	    
	    try {
	        
	        $request=$this->getRequest();
	        $activity_name=$request->getParam("activity_name");
	        
	        $ActivityTypeMapper=new Application_Model_TypesOfActivitiesMapper();
	        	        
	        if($ActivityType=$ActivityTypeMapper->addNewActivityType($activity_name)){
	            
	            
	            $data=array(
	                "activity" => $activity_name,
	                
	            );
	            
	            $meta = array(
	                "code" => 200,
	                "message" => "SUCCESS"
	            );
	            $arr = array(
	                "meta" => $meta,
	                "data" => $data
	            );
	        } else {
	            $meta = array(
	                "code" => 401,
	                "message" => "Error while adding"
	            );
	            $arr = array(
	                "meta" => $meta
	            );
	        }
	    }catch (Exception $e) {
	        $meta = array(
	            "code" => 501,
	            "messgae" => $e->getMessage()
	        );
	        
	        $arr = array(
	            "meta" => $meta
	        );
	    }
	    $json = json_encode($arr, JSON_PRETTY_PRINT);
	    echo $json;
	}
	
	public function deleteActivityTypeByIdAction(){
	    
	    try {
	        $request=$this->getRequest();
	        $id=$request->getParam("id");
	        $ActivityTypeMapper=new Application_Model_TypesOfActivitiesMapper();
	        if($ActivityType=$ActivityTypeMapper->deleteActivityTypeById($id)){
	            //	$this->_logger->info("DG Id ".$Dgid." has been deleted from DG table by ". $this->_userName.".");
	            
	            $meta = array(
	                "code" => 200,
	                "message" => "SUCCESS"
	            );
	            $arr = array(
	                "meta" => $meta,
	                
	            );
	        } else {
	            $meta = array(
	                "code" => 401,
	                "message" => "Error while deleting"
	            );
	            $arr = array(
	                "meta" => $meta
	            );
	        }
	    }catch (Exception $e) {
	        $meta = array(
	            "code" => 501,
	            "messgae" => $e->getMessage()
	        );
	        
	        $arr = array(
	            "meta" => $meta
	        );
	    }
	    $json = json_encode($arr, JSON_PRETTY_PRINT);
	    echo $json;
	}
	
	
	public function updateActivityTypeByIdAction(){
	    
	    try {
	        $request=$this->getRequest();
	        $id=$request->getParam("id");
	        $activity=$request->getParam("activity");
	        
	        $ActivityTypeMapper=new Application_Model_TypesOfActivitiesMapper();
	        $ActivityType= new Application_Model_TypesOfActivities();
	        $ActivityType->__set("id",$id);
	        $ActivityType->__set("activity",$activity);
	       
	        if($ActivityTypes=$ActivityTypeMapper->updateActivityType($ActivityType)){
	            
	            //$this->_logger->info("Equipment Id ".$dg_id." has been updated by ". $this->_userName.".");
	            
	            $meta = array(
	                "code" => 200,
	                "message" => "SUCCESS"
	            );
	            $arr = array(
	                "meta" => $meta,
	                
	            );
	        } else {
	            $meta = array(
	                "code" => 401,
	                "message" => "Error while adding"
	            );
	            $arr = array(
	                "meta" => $meta
	            );
	        }
	    }catch (Exception $e) {
	        $meta = array(
	            "code" => 501,
	            "messgae" => $e->getMessage()
	        );
	        
	        $arr = array(
	            "meta" => $meta
	        );
	    }
	    $json = json_encode($arr, JSON_PRETTY_PRINT);
	    echo $json;
	}
	
		
}

?>