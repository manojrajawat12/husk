<?php

class Api_PackagesController extends Zend_Controller_Action {

    public function init() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-Type: application/json');
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
        $auth=new My_Auth('user');
        $this->_userId=$auth->getIdentity()->user_id;
    }
    
        public function addPackageAction(){
      
         try {
             $request=$this->getRequest();
            
             $package_name=$request->getParam("package_name");
             $package_cost=$request->getParam("package_cost");
             $package_details=$request->getParam("package_details");
             $is_postpaid=$request->getParam("is_postpaid");
             $wattage=$request->getParam("watt_id");
			 $unit=$request->getParam("unit");
			 $state_id=$request->getParam("state_id");
			 $site_id=$request->getParam("site_id");
			 $feeder_id=$request->getParam("feeder_id");
			 $consumer_type=$request->getParam("consumer_type");
			 $extra_charges=$request->getParam("extra_charges");
			 $light_load=$request->getParam("light_load");
			 $security_deposit=$request->getParam("security_deposit");
			 
			 $zendDate = new Zend_Date();
			 $zendDate->setTimezone("Asia/Calcutta");
			 $created_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
			 
			 $wattagemapper = new Application_Model_WattageMapper();
			 $wattage_id=$wattagemapper->getWattageBywattId($wattage);
			 $watt_no= $wattage_id->__get("wattage");
			 
			 if($is_postpaid==0){
			 	$package_name=$package_name."/".$watt_no."/".$package_cost;
			 }elseif($is_postpaid==1){
			 	$package_name=$package_name."/".$extra_charges;
			 }else{
			 	$package_name=$package_name."/".$package_cost."/".$unit."/".$extra_charges;
			 }
			 
			 $sitemapper = new Application_Model_SiteMasterMeterMapper();
			 $MM=$sitemapper->getMMById($feeder_id);
			 $feeder_type = $MM->__get("feeder_type");
             $packagemapper=new Application_Model_PackagesMapper();
             $package= new Application_Model_Packages();
            
             $package->__set("package_name",$package_name);
             $package->__set("package_cost",$package_cost);
             $package->__set("package_details",$package_details);
             $package->__set("is_postpaid",$is_postpaid);
             $package->__set("wattage",$wattage);
			 $package->__set("unit",$unit);
			 $package->__set("extra_charges",$extra_charges);
			 $package->__set("connection_type", $consumer_type);
			 $package->__set("state_id", $state_id);
			 $package->__set("site_id", $site_id);
			 $package->__set("feeder_id", $feeder_id);
			 $package->__set("created_date", $created_date);
			 $package->__set("user_id", $this->_userId);
			 $package->__set("status","new");
			 $package->__set("light_load",$light_load);
			  $package->__set("security_deposit",$security_deposit);
			 $packages=$packagemapper->addNewPackage($package);
             
             if($packages){  
             
           		$this->_logger->info("New Package ID ".$packages." has been created in Packages by ". $this->_userName.".");
           	
	            $data=array();
	             
	            $meta = array(
	                    "code" => 200,
	                    "message" => "SUCCESS"
	                );
	                $arr = array(
	                    "meta" => $meta,
	                     "data" => $data
	                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }


        public function getPackageByIdAction(){
      
         try {
             $request=$this->getRequest();
             $package_id=$request->getParam("id");
             $packagemapper=new Application_Model_PackagesMapper();
         if($package=$packagemapper->getPackageById($package_id)){
             
            
            $data=array(     
            "package_id" => $package->__get("package_id"),
            "package_cost" => $package->__get("package_cost"),
            "package_name" => $package->__get("package_name"),
            "package_details" =>$package->__get("package_details"),
            "is_postpaid" =>$package->__get("is_postpaid"),
            //"package_billing_period" => $package->__get("package_billing_period"),
            //"package_grace_period" => $package->__get("package_grace_period"),
            "wattage" => $package->__get("wattage"),
			"unit" => $package->__get("unit"),
            "extra_charges" => $package->__get("extra_charges"),
			"status" => $package->__get("status"),
              );
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                     "data" => $data
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
        public function deletePackageByIdAction(){
      
         try {
             $request=$this->getRequest();
             $package_id=$request->getParam("id");
             $packagemapper=new Application_Model_PackagesMapper();
         if($package=$packagemapper->deletePackageById($package_id)){
              $auth=new My_Auth('user');
			  $this->_userName=$auth->getIdentity()->user_fname;
         	$this->_logger->info("package Id ".$package_id." has been deleted from Packages by ". $this->_userName.".");
         	  
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while deleting"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
       /*  public function getAllPackagesAction(){
      
         try {

               $packagemapper=new Application_Model_PackagesMapper();
         if($packages=$packagemapper->getAllPackages()){
          
             foreach ($packages as $package) {
                 
             if($package->__get("is_postpaid")==2)
             	{
             		$value='Mix Mode' ;
             	}
             	elseif($package->__get("is_postpaid") ==1)
             	{
             		$value= 'Yes';
             	}
             	else{
             		$value= 'No' ;
             	} 
                  $data=array(     
            "package_id" => $package->__get("package_id"),
            "package_cost" => $package->__get("package_cost"),
            "package_name" => $package->__get("package_name"),
            "package_details" =>$package->__get("package_details"),
            "is_postpaid" =>$value,
            "is_postpaid_value" =>$package->__get("is_postpaid"),
            "wattage" =>$package->__get("wattage"),
            "unit" =>$package->__get("unit"),
            "extra_charges" =>$package->__get("extra_charges"),
           
              );
                 
                 $package_arr[]=$data;
             }
            
            
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $package_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         */
        
        public function getAllPackagesAction(){
      
         try {
             $request=$this->getRequest();
             $who = $request->getParam("who");
             $what = $request->getParam("what"); 
             $sender = $request->getParam("sender"); 
			 $status = $request->getParam("status");
			 
             $number=$who;
          	 $success=1;
       		 $message="All Package List";
             $packagemapper=new Application_Model_PackagesMapper();
			 $site_arr=array();
             $package_arr=array();
        	 if($packages=$packagemapper->getAllPackages($status)){
         	
         		foreach ($packages as $package) {
         		 
	         		if($package->__get("is_postpaid")==2)
	         		{
	         			$value='Mix Mode' ;
	         		}
	         		elseif($package->__get("is_postpaid") ==1)
	         		{
	         			$value= 'Yes';
	         		}
	         		else{
	         			$value= 'No' ;
	         		}
         			$wattageMapper=new Application_Model_WattageMapper();
	         		$watage=$wattageMapper->getWattageBywattId($package->__get("wattage"));
	         		$watt_no=NULL;
	         		if($watage){
	         			$watt_no=$watage->__get("wattage");
	         		}
					if($watage){
	         			$watt_no=$watage->__get("wattage");
	         		}
	         		$package_name_short=explode("/", $package->__get("package_name"));
         			$data=array(
	         				"package_id" => $package->__get("package_id"),
	         				"package_cost" => intval($package->__get("package_cost")),
	         				"package_name" => $package->__get("package_name"),
         					"package_shot"=>$package_name_short[0],
	         				"package_details" =>$package->__get("package_details"),
	         				"is_postpaid" =>$value,
	         				"is_postpaid_value" =>$package->__get("is_postpaid"),
	         				"wattage" =>$package->__get("wattage"),
	         				"unit" =>$package->__get("unit"),
	         				"extra_charges" =>$package->__get("extra_charges"),
							"status" => $package->__get("status"),
							"status_cap" => ucwords($package->__get("status")),
							"site_id" =>$package->__get("site_id"),
         					"state_id" =>$package->__get("state_id"),
         					"connection_type" =>$package->__get("connection_type"),
         					"site_meter_id" =>$package->__get("feeder_id"),
         					"watt_no"=>$watt_no,
         					"update_type_value"=>$package->__get("update_type"),
         					"update_type"=>($package->__get("update_type")==0?false:true),
         					"light_load"=>$package->__get("light_load"),
							"security_deposit"=>$package->__get("security_deposit")
	         		);
	         		  
	         		$package_arr[]=$data;
         		
         	}
			 
	if($sender=="mobile"){
	 
		$agentSite=array();
		$collectionMapper=new Application_Model_CollectionAgentsMapper();
		$Agents=$collectionMapper->getCollectionAgentByPhone($number);
		if($Agents){
			$Sites_details=$Agents->__get("site_id"); 
			$agentSite=explode(",", $Sites_details);
		}
		
		$siteMapper=new Application_Model_SitesMapper();
         	$sites=$siteMapper->getAllSites();
         	foreach($sites as $site){
			if(in_array($site->__get("site_id"), $agentSite)){
			$MM_detail=array();$MM_arr=array();
         		$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		         	$siteMasters=$siteMasterMapper->getSiteMasterById($site->__get("site_id"));
					if($siteMasters){
		         	foreach($siteMasters as $siteMaster){
		         		$siteMasterMeters=$siteMasterMapper->getMasterMeterReadingByMeterId($siteMaster->__get("id"));
		         		 
		         		if($siteMasterMeters){
		         			$siteMasterMeter=$siteMasterMeters[0];
		         			$last_reading=$siteMasterMeter->__get("reading");
		         			$reading_on = $siteMasterMeter->__get("reading_date");
		         		}else{
		         			$last_reading=0;
		         			$reading_on=0;
		         		}
		         		$MM_detail=array(
							"meter_id"=>$siteMaster->__get("id"),
		         			"meter_name"=>$siteMaster->__get("meter_name"),	
		         			"last_reading" =>$last_reading,
		         			"reading_on" =>$reading_on,
		         			"meter_keyword"=>$siteMaster->__get("meter_keyword"),
		         			"description" =>$siteMaster->__get("description"),
		         			"start_time" =>$siteMaster->__get("start_time"),
		         			"end_time"=>$siteMaster->__get("end_time"),
		         			"is_24_hr"=>$siteMaster->__get("is_24_hr"),
		         		);
		         		$MM_arr[]=$MM_detail;
		         		
		         	} 
		         	}
		         		
		         		$site_data=array(
		         				"site_id"=>$site->__get("site_id"),
		         				"site_name"=>$site->__get("site_name"),
		         				"site_code"=>$site->__get("site_code"),
		         				"meters"=>$MM_arr
		         		);
		         		$site_arr[]=$site_data;
         	}
			}
					$customer_type[]=array("type_text"=>"HH Basic","type_value"=>"HH_Basic");
		         	$customer_type[]=array("type_text"=>"HH Advanced","type_value"=>"HH_Advanced");
		         	$customer_type[]=array("type_text"=>"Shops Basic","type_value"=>"Shops_Basic");
		         	$customer_type[]=array("type_text"=>"Shops Advanced","type_value"=>"Shops_Advanced");
		         	$customer_type[]=array("type_text"=>"Commercial- Appliance (Existing)","type_value"=>"Commercial-_Appliance_(Existing)");
		         	$customer_type[]=array("type_text"=>"Commercial- Appliance (New)","type_value"=>"Commercial-_Appliance_(New)");
		         	$customer_type[]=array("type_text"=>"Commercial- Motor","type_value"=>"Commercial-_Motor");
		         	$customer_type[]=array("type_text"=>"New Micro-enterprise","type_value"=>"New_Micro-enterprise");
		         	$customer_type[]=array("type_text"=>"CDKN - DGO","type_value"=>"CDKN_-_DGO");
		         	$customer_type[]=array("type_text"=>"Institutional Load (Existing)","type_value"=>"Institutional_Load_(Existing)");
		         	$customer_type[]=array("type_text"=>"Institutional Load (New)","type_value"=>"Institutional_Load_(New)");
		         	$customer_type[]=array("type_text"=>"Telecom Tower","type_value"=>"Telecom_Tower");
					
					$charges=array();
					$charges_arr=$packagemapper->getAllCharges();
					if($charges_arr){
						foreach ($charges_arr as $charge){
							$data_val=array("charge_id"=>$charge["charge_id"],
											"charge_name"=>$charge["charge_name"],
											"charge_parameter"=>$charge["charge_parameter"],
											"hindi_text"=> $charge["hindi_text"]
							);
							$charges[]=$data_val; 
						}
					}
  
	}
         	$meta = array(
         			"code" => 200,
         			"message" => "SUCCESS"
         	);
         	$arr = array(
         			"meta" => $meta,
         			"data" => $package_arr,
	         	);
         
            } else  {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while fetching Package List."
                );
                $arr = array(
                    "meta" => $meta
                );
            }

            $input_arr=array();
             
            if($sender=="mobile"){
            	if($who=="" || $who==null){
            		$who="";
            	}
            	if($what=="" || $what==null){
            		$what_string="";
            	}
            	if($who=="" || $who==null || $what=="" || $what==null){
            		$success=0;
            		$message="Paramerter missing";
            		$package_arr=array();
					$site_arr=array();
            	}
            	else if(count($package_arr)==0){
            		$message="No package found";
            	}

            
            	 
            	$input=array('what'=>$what,
            			'who'=>$who);
            	$commandResult = array(
            			"success"=>$success,
            			"message" =>$message,
            			"data"=>$package_arr,
						"SiteData" => $site_arr,
						"customer_type"=>$customer_type,
						"charges"=>$charges
            	);
            	$arr = array(
            			"input" => $input,
            			"commandResult" => $commandResult,
            	);

            }
            
             
            
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
        
        public function updatePackageByIdAction(){
      
         try {
              $request=$this->getRequest();
             $package_id=$request->getParam("id");
             $package_name=$request->getParam("package_name");
             $package_cost=$request->getParam("package_cost");
             $package_details=$request->getParam("package_details");
             $is_postpaid=$request->getParam("is_postpaid");
             $wattage=$request->getParam("watt_id");
			 $unit=$request->getParam("unit");
			 $state_id=$request->getParam("state_id");
			 $site_id=$request->getParam("site_id");
			 $feeder_id=$request->getParam("feeder_id");
			 $consumer_type=$request->getParam("consumer_type");
			 $extra_charges=$request->getParam("extra_charges");
			 $light_load=$request->getParam("light_load");
			 $status=$request->getParam("status");
			 $security_deposit=$request->getParam("security_deposit");
			 $zendDate = new Zend_Date();
			 $zendDate->setTimezone("Asia/Calcutta");
			 $created_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
			 
			 $wattagemapper = new Application_Model_WattageMapper();
			 $wattage_id=$wattagemapper->getWattageBywattId($wattage);
			 $watt_no= $wattage_id->__get("wattage");
			 
			 if($is_postpaid==0){
			 	$package_name=$package_name."/".$watt_no."/".$package_cost;
			 }elseif($is_postpaid==1){
			 	$package_name=$package_name."/".$unit;
			 }else{
			 	$package_name=$package_name."/".$package_cost."/".$unit."/".$extra_charges;
			 }
           		
             $packagemapper=new Application_Model_PackagesMapper();
             $package= new Application_Model_Packages();
             $last_package=$packagemapper->getPackageById($package_id);
             
             $package->__set("package_id",$package_id);
             $package->__set("package_name",$package_name);
             $package->__set("package_cost",$package_cost);
             $package->__set("package_details",$package_details);
             $package->__set("is_postpaid",$is_postpaid);
             $package->__set("wattage",$wattage);
			 $package->__set("unit",$unit);
			 $package->__set("extra_charges",$extra_charges);
			 $package->__set("connection_type", $consumer_type);
			 $package->__set("state_id", $state_id);
			 $package->__set("site_id", $site_id);
			 $package->__set("feeder_id", $feeder_id);
			 $package->__set("status",$status);
			 $package->__set("light_load",$light_load);
         	 $package->__set("security_deposit",$security_deposit);
             $packages=$packagemapper->updatePackage($package);
             
              if($packages){
              	 $lastPackage_arr=array(
              			"package_id" => $last_package->__get("package_id"),
              			"package_cost" => $last_package->__get("package_cost"),
              			"package_name" => $last_package->__get("package_name"),
              			"package_details" =>$last_package->__get("package_details"),
              			"is_postpaid" =>$last_package->__get("is_postpaid"),
              			"wattage" =>$last_package->__get("wattage"),
						"unit" =>$last_package->__get("unit"),
              			"extra_charges" =>$last_package->__get("extra_charges"),
              			"status" =>$last_package->__get("status"),
              			"connection_type" =>$last_package->__get("connection_type"),
              			"site_id" =>$last_package->__get("site_id"),
              			"feeder_id" =>$last_package->__get("feeder_id"),
              		 
              	);
              	$newPackage_arr=array(
              			"package_id" => $package_id,
              			"package_cost" => $package->__get("package_cost"),
              			"package_name" => $package->__get("package_name"),
              			"package_details" =>$package->__get("package_details"),
              			"is_postpaid" =>$package->__get("is_postpaid"),
              			"wattage" =>$package->__get("wattage"),
						"unit" =>$package->__get("unit"),
              			"extra_charges" =>$package->__get("extra_charges"),
              			"status" =>$package->__get("status"),
              		    "connection_type" =>$package->__get("connection_type"),
              			"site_id" =>$package->__get("site_id"),
              			"feeder_id" =>$package->__get("feeder_id"),
              	 );
              	
              	$lastPackage_diff=array_diff($lastPackage_arr,$newPackage_arr);
              	$newPackage_diff=array_diff($newPackage_arr,$lastPackage_arr);

              	$change_data="";
              	foreach ($lastPackage_diff as $key => $value)
              	{
              		$change_data.=$key." ".$lastPackage_diff[$key]." change to ".$newPackage_diff[$key]." ";
              	} 
              	$this->_logger->info("Packages Id ".$package->__get("package_id")." has been updated where ".$change_data." by ". $this->_userName.".");
              	 
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    
                ); 
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
        public function getAllPackagesWithConsumerCountAction(){
      
         try {
//             $request=$this->getRequest();
//             $package_id=$request->getParam("id");
            $packagemapper=new Application_Model_PackagesMapper();
            $consumerMapper=new Application_Model_ConsumersMapper();
         if($packages=$packagemapper->getAllPackages()){
          
             foreach ($packages as $package) {
                
             $consumers=$consumerMapper->getAllConsumerByPackageId($package->__get("package_id"));
            
             $data=array( 
            "consumer_count" => count($consumers),   
            "package_id" => $package->__get("package_id"),
            "package_cost" => $package->__get("package_cost"),
            "package_name" => $package->__get("package_name"),
            "package_details" =>$package->__get("package_details"),
            "is_postpaid" =>$package->__get("is_postpaid"),
            //"package_billing_period" => $package->__get("package_billing_period"),
           // "package_grace_period" => $package->__get("package_grace_period")
                
              );
                 
                 $package_arr[]=$data;
             }
            
            
          
             
            $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $arr = array(
                    "meta" => $meta,
                    "data" => $package_arr,
                );
            } else {
                $meta = array(
                    "code" => 401,
                    "message" => "Error while adding"
                );
                $arr = array(
                    "meta" => $meta
                );
            }
            }catch (Exception $e) {
            $meta = array(
                "code" => 501,
                "messgae" => $e->getMessage()
            );

            $arr = array(
                "meta" => $meta
            );
        }
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
         }
         
		public function checkPackageId($data){
         
         	$site_id=$data['site_id'];
         	$state_id=$data['state_id'];
         	$site_code=$data['site_code'];
         	$site_meter_id=$data['site_meter_id'];
         	$watt_no=$data['watt_no'];
         	$connection_type=$data['connection_type'];
         	$consumer_act_date=$data['consumer_act_date'];
         	$price=$data['price'];
         	$MM_unit=$data['MM_unit'];
         	$MM_charges=$data['MM_charges'];
         	$type_of_me=$data['type_of_me'];
         	$wattage=$data['wattage'];
         	$package_type=$data['package_type'];
         	$status=$data['status'];
         	$package_id=$data['package_id'];
         	$package_details=$data['package_details'];
         	 
         	$siteMasterMeterMapper = new Application_Model_SiteMasterMeterMapper();
         	$packagemapper = new Application_Model_PackagesMapper();
         	 
         	$package_arr=array();
          
         		$feeder_data=$siteMasterMeterMapper->getMMById($site_meter_id);
         		 
         		$feeder_type= $feeder_data->__get("feeder_type");
         		$start_time=$feeder_data->__get("start_time");
         		$end_time=$feeder_data->__get("end_time");
         
         		if($start_time > $end_time){
         			$feeder_hour= intval(24+($end_time - $start_time));
         		}
         		else {
         			$feeder_hour= intval($end_time - $start_time);
         		}
         		if($feeder_hour<10){
					$zero=0;
				}else{
					$zero="";
				}
         		if($package_type==0){
         			$pacakge_name=$site_code.$zero.$feeder_hour.$feeder_type.$watt_no.$type_of_me.$price;
         		}elseif ($package_type==1){
         			$pacakge_name=$site_code.$zero.$feeder_hour.$feeder_type.$watt_no."ME00".$price;
         		}elseif ($package_type==2){
         			$pacakge_name=$site_code.$zero.$feeder_hour.$feeder_type.$watt_no."MM".$price.$MM_unit.$MM_charges;
         		} 
				 
         		$packages=$packagemapper->getPackagesByPackageName($pacakge_name,$package_id);
         		if($packages){
         			 return false;
         		}else{
         			$package=new Application_Model_Packages();
         			$package->__set("package_cost",$price);
         			$package->__set("package_details",$package_details);
         			$package->__set("is_postpaid",$package_type);
         			$package->__set("wattage",$wattage);
         			$package->__set("connection_type", $connection_type);
         			$package->__set("state_id", $state_id);
         			$package->__set("site_id", $site_id);
         			$package->__set("feeder_id", $site_meter_id);
         			$package->__set("feeder_type", $feeder_type);
         			$package->__set("unit",$MM_unit);
         			$package->__set("extra_charges",$MM_charges);
         			$package->__set("package_id",$package_id);
         			$package->__set("status",$status);
         			
         			if($package_id!=NULL){
         				$packages=$packagemapper->updatePackage($package,$this->_userId);
         			}else{
         				$packages=$packagemapper->addNewPackage($package,$this->_userId);
         			}
         			if($packages){
         				return $packages;
         			}else{
         				return false;
         			}
         		 
         	}
          
         }      
		 
		public function getAllPackagesWithFilterAction(){
         	 
         	try {
         		$request=$this->getRequest();
         		$site_id=$request->getParam("site_id");
         		$is_postpaid=$request->getParam("is_postpaid");
         		$status=$request->getParam("status");
         		
         		$site_id=($site_id!=null && $site_id!='undefined')? $site_id:NULL;
         		$is_postpaid=($is_postpaid!=null && $is_postpaid!='undefined')? $is_postpaid:NULL;
         		$status=($status!=null && $status!='undefined')? $status:NULL;
         		$packagemapper=new Application_Model_PackagesMapper();
         		$site_arr=array();
         		$package_arr=array();
         		if($packages=$packagemapper->getPackagesByFilter($site_id,$is_postpaid,$status)){
         			 
         			foreach ($packages as $package) {
         				 
         				if($package["is_postpaid"]==2){
         					$value='Mix Mode' ;
         				}elseif($package["is_postpaid"] ==1){
         					$value= 'Yes';
         				}else{
         					$value= 'No' ;
         				}
						$wattageMapper=new Application_Model_WattageMapper();
						$watage=$wattageMapper->getWattageBywattId($package["wattage"]);
						$watt_no=NULL;
						if($watage){
							$watt_no=$watage->__get("wattage");
						}
         				$data=array(
         						"package_id" => $package["package_id"],
         						"package_cost" => intval($package["package_cost"]),
         						"package_name" => $package["package_name"],
         						"package_details" =>$package["package_details"],
         						"is_postpaid" =>$value,
         						"is_postpaid_value"=>$package["is_postpaid"],
         						"wattage" =>$package["wattage"],
         						"unit" =>$package["unit"],
         						"extra_charges" =>$package["extra_charges"],
         						"status" =>$package["status"],
         						"status_cap" =>ucwords($package["status"]),
         						"site_id"=>$package["site_id"],
         						"site_meter_id"=>$package["feeder_id"],
         						"connection_type"=>$package["connection_type"],
								"watt_no"=>$watt_no         
         				);
         
         				$package_arr[]=$data;
         
         			}
         
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $package_arr,
         			);
         			 
         		} else  {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while fetching Package List."
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         		 
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }      
		
		public function getPackagesBySitePackagetypeAction(){
         	 
         try {
         		$request=$this->getRequest();
         	    $packagemapper=new Application_Model_PackagesMapper();
         		$consumerMapper=new Application_Model_ConsumersMapper();
         		$wattageMapper=new Application_Model_WattageMapper();
         		$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
         		$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
         			
         		if($packages=$packagemapper->getPackagesBySitePackagetype($site_id,$package_type)){
         		 
         			foreach ($packages as $package) {
         				$watage=$wattageMapper->getWattageBywattId($package["wattage"]);
         				$watt_no=NULL;
         				if($watage){
         					$watt_no=$watage->__get("wattage");
         				}
         				$data=array(
	         				"package_id" => $package["package_id"],
	         				"package_cost" => intval($package["package_cost"]),
	         				"package_name" => $package["package_name"],
	         				"package_details" =>$package["package_details"],
	         				"is_postpaid" =>($package["is_postpaid"]==0)? "No" : $package["package_details"]==1?"Yes" : "Mix Mode",
	         				"is_postpaid_value" =>$package["is_postpaid"],
	         				"wattage" =>$package["wattage"],
	         				"unit" =>$package["unit"],
	         				"extra_charges" =>$package["extra_charges"],
							"status" => $package["status"],
							"status_cap" => ucwords($package["status"]),
							"site_id" =>$package["site_id"],
         					"state_id" =>$package["state_id"],
         					"connection_type" =>$package["connection_type"],
         					"site_meter_id" =>$package["feeder_id"],
         					"watt_no"=>$watt_no
         					 
	         		);
         				 
         				$package_arr[]=$data;
         			}
         
          			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $package_arr,
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while adding"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
		 
		public function pendingPackageAction(){
         
         	try {
         	  
         		$packagemapper=new Application_Model_PackagesMapper();
         		$consumerMapper=new Application_Model_ConsumersMapper();
         		$wattageMapper=new Application_Model_WattageMapper();
         		$roleSession = new Zend_Session_Namespace('roles');
         		$role_sites_id=$roleSession->site_id;
         		 
         		if($packages=$packagemapper->getAllPackagesNew("new",$role_sites_id)){
         		 
         			foreach ($packages as $package) {
         				$watage=$wattageMapper->getWattageBywattId($package->__get("wattage"));
         				$watt_no=NULL;
         				if($watage){
         					$watt_no=$watage->__get("wattage");
         				}
         				if($package->__get("is_postpaid")=='0'){
         					$type="Prepaid";
         				}elseif ($package->__get("is_postpaid")=='1'){
         					$type="Postpaid";
         				}else{
         					$type="Mix Mode";
         				} 
         				
         				$data=array(
	         				"package_id" => $package->__get("package_id"),
	         				"package_cost" => intval($package->__get("package_cost")),
	         				"package_name" => $package->__get("package_name"),
	         				"package_details" =>$package->__get("package_details"),
	         				"is_postpaid" =>$type,
	         				"is_postpaid_value" =>$package->__get("is_postpaid"),
	         				"wattage" =>$package->__get("wattage"),
	         				"unit" =>$package->__get("unit"),
	         				"extra_charges" =>$package->__get("extra_charges"),
							"status" => $package->__get("status"),
							"status_cap" => ucwords($package->__get("status")),
							"site_id" =>$package->__get("site_id"),
         					"state_id" =>$package->__get("state_id"),
         					"connection_type" =>$package->__get("connection_type"),
         					"site_meter_id" =>$package->__get("feeder_id"),
         					"watt_no"=>$watt_no,
							"security_deposit" =>$package->__get("security_deposit"),
         					 
	         		);
         				 
         				$package_arr[]=$data;
         			}
         
          			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $package_arr,
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while adding"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
        
        public function updatePackageStatusByIdAction(){
         
         	try {
         		$request=$this->getRequest();
         		$package_id=$request->getParam("package_id");
         		$status=$request->getParam("status");
         		$remark=$request->getParam("remark");
         		 
         		$zendDate = new Zend_Date();
         		$zendDate->setTimezone("Asia/Calcutta");
         		$consumer_act_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
          
          		$packagemapper=new Application_Model_PackagesMapper();
         		 
         		if($package=$packagemapper->updatePackageById($package_id,$status,$remark, $this->_userId)){
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error while updating"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }
		
		public function getPackageByConsumerIdAction(){
         	 
         	try {
         		$request=$this->getRequest();
         		$consumer_id=$request->getParam("consumer_id");
         		 
         		$packagemapper=new Application_Model_PackagesMapper(); 
				$meterReadingMapper=new Application_Model_MeterReadingsMapper();
         		$package_arr=array();
				$date = new Zend_Date(); 
				$date->setTimezone("Asia/Kolkata");
				$date = $date->toString("yyyy-MM-dd");
		
         		if($packages=$packagemapper->getPackageByPackageConsumerId(NULL,$consumer_id)){
         			foreach ($packages as $package){
						$meterReading=$meterReadingMapper->getReadingByConPackID($consumer_id,$date,$package["package_id"]);
						if($meterReading){
							$meter_reading=$meterReading["meter_reading"];
							$meter_no=$meterReading["meter_no"];
						}else{
							$meter_reading=0;
							$meter_no=0;
						}
	         			$data=array(
	          					"package_id" => $package["package_id"],
	         					"package_cost" => $package["package_cost"],
	         					"package_name" => $package["package_name"],
	         					"package_details" =>$package["package_details"],
	         					"is_postpaid" =>$package["is_postpaid"],
	         					"wattage" => $package["wattage"],
	         					"unit" => $package["unit"],
	         					"extra_charges" => $package["extra_charges"],
	         					"status" => $package["status"],
								"meter_reading"=>floatval($meter_reading),
								"meter_no"=>$meter_no 
	         			);
						$package_arr[]=$data;
         			}
         			 
         			$meta = array(
         					"code" => 200,
         					"message" => "SUCCESS"
         			);
         			$arr = array(
         					"meta" => $meta,
         					"data" => $package_arr,
							"total_package"=>count($package_arr)
         			);
         		} else {
         			$meta = array(
         					"code" => 401,
         					"message" => "Error"
         			);
         			$arr = array(
         					"meta" => $meta
         			);
         		}
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         		 
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }   

		public function getAllPackagesByFeederAction(){
         	 
         	try {
         		$request=$this->getRequest();
         		$packagemapper=new Application_Model_PackagesMapper();
         		$consumerMapper=new Application_Model_ConsumersMapper();
         		$wattageMapper=new Application_Model_WattageMapper();
         		$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL: $request->getParam("site_id");
         		$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:$request->getParam("package_type");
         		$feeder = ($request->getParam("feeder_id")=="" || $request->getParam("feeder_id")=='undefined' )?NULL: $request->getParam("feeder_id");
         		
         		if($feeder!=NULL){
         			$AllFeeder=explode(",", $feeder);
         			$AllFeederCount=count($AllFeeder);
         		}else{
         			$AllFeederCount=1;
         			$AllFeeder=array(NULL);
         		}
         		$package_arr=array();
         		for($i=0;$i<$AllFeederCount;$i++){
	         		if($packages=$packagemapper->getPackagesForFilter($site_id,$package_type,$AllFeeder[$i])){
	         
	         			foreach ($packages as $package) {
	         				$watage=$wattageMapper->getWattageBywattId($package["wattage"]);
	         				$watt_no=NULL;
	         				if($watage){
	         					$watt_no=$watage->__get("wattage");
	         				}
	         				$data=array(
	         						"package_id" => $package["package_id"],
	         						"package_cost" => intval($package["package_cost"]),
	         						"package_name" => $package["package_name"],
	         						"package_details" =>$package["package_details"],
	         						"is_postpaid" =>($package["is_postpaid"]==0)? "No" : $package["package_details"]==1?"Yes" : "Mix Mode",
	         						"is_postpaid_value" =>$package["is_postpaid"],
	         						"wattage" =>$package["wattage"],
	         						"unit" =>$package["unit"],
	         						"extra_charges" =>$package["extra_charges"],
	         						"status" => $package["status"],
	         						"status_cap" => ucwords($package["status"]),
	         						"site_id" =>$package["site_id"],
	         						"state_id" =>$package["state_id"],
	         						"connection_type" =>$package["connection_type"],
	         						"site_meter_id" =>$package["feeder_id"],
	         						"watt_no"=>$watt_no
	         						 
	         				);
	         
	         				$package_arr[]=$data;
	         			}
	         		}
         		}
         		 
         		$meta = array(
         				"code" => 200,
         				"message" => "SUCCESS"
         		);
         		$arr = array(
         				"meta" => $meta,
         				"data" => $package_arr,
         		);
         		
         	}catch (Exception $e) {
         		$meta = array(
         				"code" => 501,
         				"messgae" => $e->getMessage()
         		);
         		 
         		$arr = array(
         				"meta" => $meta
         		);
         	}
         	$json = json_encode($arr, JSON_PRETTY_PRINT);
         	echo $json;
         }		 
}    