<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = new My_Auth("admin");
        if(!$auth->hasIdentity())
        {
                $this->_helper->redirector('index', 'auth');
        }
        $this->_helper->layout()->setLayout('admin');
    }

    public function indexAction()
    {
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        
        $today = new Zend_Date();
        $monthName = $today->toString("MMMM");
        $this->view->monthName = $monthName;
        $month = $today->toString("MM");
        $year = $today->toString("yyyy");
        
        $cashRegister = $cashRegisterMapper->getCashRegisterByMonthYear($month, $year);
        
        $this->view->year = $year;
        $str = "01-".$month."-".$year;
        $startDate = new Zend_Date($str,"dd-MM-yyyy");
        
        foreach ($cashRegister as $value){
            $date = new Zend_Date($value->__get("timestamp"),"yyyy-MM-dd HH:mm:ss");

            if($date->isLater($startDate)){         
                $filteredValues[] = $value; 
            }

        }
        $typesArray = array("CREDIT","ACTIVATION","REFUND");
        $creditEnteries = $cashRegisterMapper->applyTypeFilter($filteredValues, $typesArray);
        $amount = 0;
        foreach($creditEnteries as $entry){
            if($entry->__get("cr_entry_type")=="REFUND")
            {
                $amount -= $entry->__get("cr_amount");
            }
            else 
            {
                $amount += $entry->__get("cr_amount");
            }
        }  
         
       $this->view->monthRevenue = $amount;
         
    }


}

