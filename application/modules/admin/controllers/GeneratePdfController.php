
<?php
include('MPDF/mpdf.php'); 
require('html2fpdf.php');
require('font/makefont/makefont.php');
ini_set('display_errors',1);

/** PHPExcel_IOFactory */

require_once '../PHPExcel/PHPExcel/IOFactory.php';
gc_enable() ;gc_collect_cycles();

class Admin_GeneratePdfController extends Zend_Controller_Action
{
	public function init()
	{
		 $this->_helper->layout()->disableLayout();
         	 $this->_helper->viewRenderer->setNoRender(true);
		
	}
	
	public function indexAction()
	{
	
		$request = $this->getRequest();
		 $type = $request->getParam("type");
		 
		date_default_timezone_set('Asia/Kolkata');
		switch ($type)
		{
			case 'Collection_Sheet':
 
				$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
				$trans_date=date_parse_from_format("Y-m-d", $timestamp);
				$day=$trans_date["day"];
				$month= $trans_date["month"];
				$year= $trans_date["year"];
				$stateMapper=new Application_Model_StatesMapper();
				$clusterMapper=new Application_Model_ClustersMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$siteMapper=new Application_Model_SitesMapper();
				$consumerMapper=new Application_Model_ConsumersMapper();
				$packageMapper=new Application_Model_PackagesMapper();
		 
		 		$StateName_Array=array();
			  $sites=$siteMapper->getAllSites(); 
				$counters=0;
			  foreach ($sites as $site)
			  {
			  	if($site->__get("site_id")!=1){
			  $consumerdata="";
			  
			  $consumerdata.= " <h4><center> Customer Account Sheet for ".date('F, Y')."</center></h4>";
			  
			  	$stateName=$stateMapper->getStateById($site->__get("state_id"));
			  $consumerdata.= "<div>
			  		
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>
	                <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
                        <tbody>
                         <tr>
						  <th colspan='4'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th>
                          <th colspan='2'>One-Time Payment</th>
						  <th colspan='3'>Monthly Billing</th>
						  <th colspan='1'>Total</th>";
			 
			  $consumerdata.= "<tr>
                                <th width='50px'>S.No</th>
                                <th width='150px'>Connection ID</th>
				  

				 				<th width='100px'>Consumers Name</th>
				 				<th width='120px'>Mobile Number</th>
								
			  					<th width='70px'>OS</th>
			  					<th width='120px'>Collected This Month</th>

			  					<th width='80px'>OS</th>
								<th width='120px'>Billed This Month</th>
                                <th width='120px'>Collected This Month</th>
			  
                                <th width='80px'>To Collect</th>
					
			  
                            </tr>";
							$mainArray=array();
			  				$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"),true);
			  				 
			  				if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			  				foreach ($consumers as $consumer){	
				  				$totalOutStanding=$cashRegisterMapper->getOutstandingBySiteIdAndDate($consumer->__get("consumer_id"), $timestamp);

				  				$consumersVal=$consumerMapper->collectionSheetDetails($consumer->__get("consumer_id"),$month,$year);
				  				
				  				$total_paid_activation=($consumersVal['total_paid_activation']==null?0:$consumersVal['total_paid_activation']);
				  				$total_activation=($consumersVal['total_activation']==null?0:$consumersVal['total_activation']);
				  				$current_activation=($consumersVal['current_activation']==null?0:$consumersVal['current_activation']);
				  				$current_debit=($consumersVal['current_debit']==null?0:$consumersVal['current_debit']);
				  				$current_credit=($consumersVal['current_credit']==null?0:$consumersVal['current_credit']);
				  			//	$totalOutStanding=($totalOutStanding==0?0:$totalOutStanding);
				  				$OS_OTP=$total_activation-$total_paid_activation;
	
								if($OS_OTP<=$current_activation){
				  					$OS_OTP=0;
				  				}

				  				if($OS_OTP<=0){
				  					$OS_OTP="-";
				  				}
								$sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM) </b>";
				  					$consumers_details=$consumerMapper->getCramountbyconsumerId($consumer->__get("consumer_id"),$month,$year);
				  					$current_debit=$consumers_details;
									$totalOutStanding=$totalOutStanding-$current_debit;
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				} 
								}
				  				$TotalCollected = (($OS_OTP - $current_activation) + (($totalOutStanding + $current_debit) - $current_credit));
			  					if($current_activation==0){
			  						$current_activation='-';
			  					}
			  					if($totalOutStanding==0){
			  						$totalOutStanding='-';
			  					}
			  					if($current_debit==0){
			  						$current_debit='-';
			  					}
			  					if($current_credit==0){
			  						$current_credit='-';
			  					}
			  					if($TotalCollected==0){
			  						$TotalCollected='-';
			  					}
								/*$sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
			  					$metered=""; 
			  					if($sites_val->__get("is_postpaid")!=0){
			  						$metered="M";
			  					}*/
			  					$data_val=array(
			  							"count"=>$count,
			  							"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
			  							"metered"=>$metered,
			  							"consumer_name"=>$consumer->__get("consumer_name"),
			  							"consumer_code"=>$consumer->__get("consumer_code"),
			  							"OS_OTP"=>$OS_OTP,
			  							"current_activation"=>$current_activation,
			  							"totalOutStanding"=>$totalOutStanding,
			  							"current_debit"=>$current_debit,
			  							"current_credit"=>$current_credit,
			  							"TotalCollected"=>$TotalCollected,
			  							 
			  							
			  					);
			  					 
			  					$mainArray[]=$data_val;
			  					
				  				$s_otpOs = $s_otpOs + $OS_OTP;
				  				$s_cacti = $s_cacti + $current_activation;
				  				$s_totalOutStanding = $s_totalOutStanding + $totalOutStanding;
				  				$s_debit = $s_debit + $current_debit;
				  				$s_credit=$s_credit + $current_credit;
				  				$s_total = $s_total + $TotalCollected;
			  				}
			  				$sort_arr = array();
			  				foreach ($mainArray as $key => $row)
			  				{
			  					$sort_arr[$key] = $row['consumer_connection_id'];
			  				}
			  				array_multisort($sort_arr, SORT_ASC ,$mainArray);
			  				$total_data_val = $mainArray;
			  				 
			  				foreach ($total_data_val as $total_data_vals){
			  					$count= $count+1;
			  				 
			  				$consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$total_data_vals["consumer_connection_id"].$total_data_vals["metered"]."</td>
										 
		                                    <td>".ucwords(strtolower($total_data_vals["consumer_name"]))."</td>
								  			<td>".$total_data_vals["consumer_code"]."</td>
			  				
										    <td align='center'>".$total_data_vals["OS_OTP"]."</td>
			                                <td align='center'>".$total_data_vals["current_activation"]."</td>
										    <td align='center'>".$total_data_vals["totalOutStanding"]."</td>
		                                    <td align='center'>".$total_data_vals["current_debit"]."</td>
			                                <td align='center'>".$total_data_vals["current_credit"]."</td>
			                                <td align='center'>".$total_data_vals["TotalCollected"]."</td>
				                </tr> ";
			  				}
				  				$consumerdata.= "  <tr>
		                                    <td colspan='4' align='right'>Total</td>
		                                  
										    <td>".$s_otpOs."</td>
			                                <td>".$s_cacti."</td>
										    <td>".$s_totalOutStanding."</td>
		                                    <td>".$s_debit."</td>
			                                <td>".$s_credit."</td>
			                                <td>".$s_total."</td>
				                </tr> ";
			  			
			  		  }
			  		  
			  		 
				 
			  $consumerdata.=  "</tbody> </table>
                		</div>
           			</div>
		   		</div>
         	 		</div> <br>";
			  
			  $consumerdata.="<h4><center>Disconnected Customers</center></h4>";
			  $consumerdata.= "<div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>
	                <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
                        <tbody>";
                          
			  
			  $consumerdata.= "<tr>
                                <th width='50px'>S.No</th>
                                <th width='150px' >Connection ID</th>
				 				
				 				<th width='100px'>Consumers Name</th>
								 

				 				<th width='120px'>Mobile Number</th>

			  					<th width='70px'>Banned Date</th>
			  					
			  					<th width='70px'>OS OTP</th>
			  					<th width='80px'>OS Monthly</th>
								 
			
			
                            </tr>";
			  $consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"));
			 
			  if($consumers){
			  	$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			  	foreach ($consumers as $consumer){
			  		$totalOutStanding=$cashRegisterMapper->getOutstandingBySiteIdAndDate($consumer->__get("consumer_id"), $timestamp);
			  
			  		$total_paid_activation=($consumersVal['total_paid_activation']==null?0:$consumersVal['total_paid_activation']);
			  		$total_activation=($consumersVal['total_activation']==null?0:$consumersVal['total_activation']);
			  		 $DODate=$consumerMapper->getConsumerDODatebyConnectionId($consumer->__get('consumer_connection_id')); 
			  		$OS_OTP=$total_activation-$total_paid_activation;
			  		if($OS_OTP<=0){
			  			$OS_OTP=0;
			  		}
			  		$consumersVal=$consumerMapper->collectionSheetDetails($consumer->__get("consumer_id"),$month,$year);
			  		$count= $count+1;
			  		 
			  		 
			  		if($OS_OTP==0){
			  			$OS_OTP='-';
			  		}
			  		if($totalOutStanding==0){
			  			$totalOutStanding='-';
			  		}
					$sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
			  		$metered=""; 
					if($sites_val){
			  		if($sites_val->__get("is_postpaid")==2){
			  			$metered="<b> (MM)</b>";
			  		}elseif ($sites_val->__get("is_postpaid")==1){
			  			$metered="<b> (ME)</b>";
			  		}}
			  		$consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$consumer->__get("consumer_connection_id").$metered."</td>
										 
		                                    <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
										    <td>".$consumer->__get("consumer_code")."</td>
			  
										    <td>".$DODate["timestamp"]."</td>
										    		
			                                <td align='center'>".$OS_OTP."</td>  
			                                <td align='center'>".$totalOutStanding."</td>
				                </tr> ";
			  		$s_otpOs=$s_otpOs+$OS_OTP;
			  		$s_totalOutStanding=$totalOutStanding+$s_totalOutStanding;
			  		 
			  	}
			  		$consumerdata.= "  <tr>
		                                    <td colspan='5' align='right'>Total</td>
			  
										    <td>".$s_otpOs."</td>
			                                <td>".$s_totalOutStanding."</td>
		                                 
				                </tr> ";
			  	
			  }
			   
			  
			  	
			  $consumerdata.=  "</tbody> </table>
                		</div>
           			 </div>
		   		   </div>
         	    </div>
				<h5 align='right'><i>File created on ".date('jS F, Y')." at ".date('h:i A')."  </i></h5>";
			
			  $filename="Customer Account Sheet for ".$site->__get("site_name")." ".date('d M Y hi A').'.pdf';
			  $files=(string)$filename;
			  $checkPdf=$this->pdfGenerateAction($consumerdata,$files);
			 
			  array_push($StateName_Array, $files);
			  }
			  } 
 
	if(count($sites)==(count($StateName_Array))+1){
			  		  
		 
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		
			$trans_date=date_parse_from_format("Y-m-d", $timestamp);
			$day=$trans_date["day"];
			$month= $trans_date["month"];
			$year= $trans_date["year"];  
			$stateMapper=new Application_Model_StatesMapper();
			$clusterMapper=new Application_Model_ClustersMapper();
			$cashRegisterMapper = new Application_Model_CashRegisterMapper();
			$siteMapper=new Application_Model_SitesMapper();
			$consumerMapper=new Application_Model_ConsumersMapper();
		
			$states=$stateMapper->getAllStates();
			 if(count($states)>0){
			 	$data="";
			 	
			 $data.= " <h4><center>Summary Sheet for Active customers ".date('jS F, Y')." at ".date('h:i A')."</center></h4>";
			 $data.= " <div>
		
			 <div class='panel-body'>
			 <div class='table-scrollable'>
			 <div class='adv-table'>
			 <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
			 <tbody>
			 <tr>
			 <th colspan='2'>Summary Sheet for ".date('M, Y')."</th>
			 <th colspan='3'>One-Time Payment</th>
			 <th colspan='4'>Monthly Billing</th>
			 <th colspan='1'>Total</th>";
		
			 $data.= "<tr>
			 <th width='50px'>S.No</th>
			 <th width='150px'>Site Name</th>
			  
			 <th width='100px'>OS</th>
			 <th width='140px'>Collected This Month</th>
			 <th width='90px'>Balance</th>
		
			 <th width='100px'>Prev. OS</th>
			 <th width='130px'>Billed This Month</th>
			 <th width='140px'>Collected This Month</th>
			 <th width='90px'>Balance</th>
		
			 <th width='80px'>To Collect</th>
			 	
		
			 </tr>";
			 $ALLotp_os=0; $ALLcurrent_otp=0; $ALLotp_balance=0;$ALLmonthly_os=0;
			 $ALLmonthly_debit=0; $ALLmonthly_credit=0; $ALLmonthly_balance=0; $ALLto_total=0; 
				foreach($states as $state) {
					$oneotp_os=0;
					$onecurrent_otp=0;
					$oneotp_balance=0;
					$onemonthly_os=0;
					$onemonthly_debit=0;
					$onemonthly_credit=0;
					$onemonthly_balance=0;
					$oneto_total=0;
					 
				$data.=" <tr bgcolor='#000000'><th colspan='10'><font color='#FFFFFF'>".$state->__get("state_name")."</font> </th></td>";
				$clusters=$clusterMapper->getClusterByStateId($state->__get("state_id"));
				foreach ($clusters as $cluster){
					$otp_os=0;$current_otp=0;$otp_balance=0;$monthly_os=0;$monthly_debit=0;$monthly_credit=0;
					$monthly_balance=0;$to_total=0;
					$sites=$siteMapper->getSiteByClusterId($cluster->__get("cluster_id"),1);
					$count=0;$main_summary_array=array();
					if($sites){
						$data.=" <tr bgcolor='#808080'><th colspan='10' align='left'><font color='#FFFFFF'> ".$cluster->__get("cluster_name").",".$state->__get("state_name")."</font> </th></td>";
						$total_data_valss=array();
							foreach ($sites as $site)
							{
								 
							$totalOutStanding=$cashRegisterMapper->getTotalOutstandingAmountForSummarybanned($timestamp,$site->__get("site_id"));
							$consumersVal=$consumerMapper->collectionSheetDetailsSiteWisebanned($site->__get("site_id"),$month,$year);	
							 
								$total_paid_activation=($consumersVal['total_paid_activation']==null?0:$consumersVal['total_paid_activation']);
				  				$total_activation=($consumersVal['total_activation']==null?0:$consumersVal['total_activation']);
				  				$current_activation=($consumersVal['current_activation']==null?0:$consumersVal['current_activation']);
				  				$current_debit=($consumersVal['current_debit']==null?0:$consumersVal['current_debit']);
				  				$current_credit=($consumersVal['current_credit']==null?0:$consumersVal['current_credit']);
				  			 
				  				$OS_OTP=$total_activation-$total_paid_activation;
				  				
				  				$OTP_balance=$OS_OTP-$current_activation;
				  				$OS_balance=($totalOutStanding+$current_debit)-$current_credit;
				  				
				  				$total=$OTP_balance+$OS_balance;
				  				
				  				if($OS_OTP==0){$OS_OTP="-";}
				  				if($current_activation==0){$current_activation="-";}
				  				if($OTP_balance==0){$OTP_balance="-";}
				  				if($totalOutStanding==0){$totalOutStanding="-";}
				  				if($current_debit==0){$current_debit="-";}
				  				if($current_credit==0){$current_credit="-";}
				  				if($OS_balance==0){$OS_balance="-";}
				  				if($total==0){$total="-";}
				  				 
				  		$datas=array(
				  				"count"=>$count,	
				  				"site_name"=>$site->__get("site_name"),
				  				"OS_OTP"=>$OS_OTP,
				  				"current_activation"=>$current_activation,
				  				"OTP_balance"=>$OTP_balance,
				  				"totalOutStanding"=>$totalOutStanding,
				  				"current_debit"=>$current_debit,
				  				"current_credit"=>$current_credit,
				  				"OS_balance"=>$OS_balance,
				  				"total"=>$total,
				  		);		
				  		$main_summary_array[]=$datas;
				  		
				  		
							 
							$otp_os=$otp_os+$OS_OTP;
							$current_otp=$current_otp+$current_activation;
							$otp_balance=$otp_balance+$OTP_balance;
							$monthly_os=$monthly_os+$totalOutStanding;
							$monthly_debit=$monthly_debit+$current_debit;
							$monthly_credit=$monthly_credit+$current_credit;
							$monthly_balance=$monthly_balance+$OS_balance;
							$to_total=$to_total+$total;
				  		 
					}
				  		$sort_arr = array();
				  		foreach ($main_summary_array as $key => $row)
				  		{
				  			$sort_arr[$key] = $row['total'];
				  		}
				  		array_multisort($sort_arr, SORT_DESC ,$main_summary_array);
				  		$total_data_valss = $main_summary_array;
				  		
				  		foreach ($total_data_valss as $total_data_vals){
				  			$count= $count+1;
				  			 
				  			$data.= "  <tr>
							<td>".$count."</td>
							<td>".$total_data_vals['site_name']."</td>
							<td align='center'>".$total_data_vals['OS_OTP']."</td>
				  			<td align='center'>".$total_data_vals['current_activation']."</td>
							<td align='center'>".$total_data_vals['OTP_balance']."</td>
							<td align='center'>".$total_data_vals['totalOutStanding']."</td>
							<td align='center'>".$total_data_vals['current_debit']."</td>
							<td align='center'>".$total_data_vals['current_credit']."</td>
							<td align='center'>".$total_data_vals['OS_balance']."</td>
							<td align='center'>".$total_data_vals['total']."</td>
			
							</tr> ";
				  		}
						
						$data.= "  <tr>
							<td colspan='2'><b>Subtotal</b></td>
							<td align='center'><b>".$otp_os."</b></td>
							<td align='center'><b>".$current_otp."</b></td>
							<td align='center'><b>".$otp_balance."</b></td>
							<td align='center'><b>".$monthly_os."</b></td>
							<td align='center'><b>".$monthly_debit."</b></td>
							<td align='center'><b>".$monthly_credit."</b></td>
							<td align='center'><b>".$monthly_balance."</b></td>
							<td align='center'><b>".$to_total."</b></td>
							</tr>";
						$ALLotp_os=$ALLotp_os+$otp_os;
						$ALLcurrent_otp=$ALLcurrent_otp+$current_otp;
						$ALLotp_balance=$ALLotp_balance+$otp_balance;
						$ALLmonthly_os=$ALLmonthly_os+$monthly_os;
						$ALLmonthly_debit=$ALLmonthly_debit+$monthly_debit;
						$ALLmonthly_credit=$ALLmonthly_credit+$monthly_credit;
						$ALLmonthly_balance=$ALLmonthly_balance+$monthly_balance;
						$ALLto_total=$ALLto_total+$to_total;
						
						$oneotp_os=$oneotp_os+$otp_os;
						$onecurrent_otp=$onecurrent_otp+$current_otp;
						$oneotp_balance=$oneotp_balance+$otp_balance;
						$onemonthly_os=$onemonthly_os+$monthly_os;
						$onemonthly_debit=$onemonthly_debit+$monthly_debit;
						$onemonthly_credit=$onemonthly_credit+$monthly_credit;
						$onemonthly_balance=$onemonthly_balance+$monthly_balance;
						$oneto_total=$oneto_total+$to_total;
					}
					}
					$val=0;
					$data.=  "
						<tr border=0>
							<td border=0 colspan='10'>".$val."</td>
						</tr>
						<tr>
						<td colspan='2'><b>Total for ".$state->__get('state_name')."</b></td>
						    <td><b>".$oneotp_os."</b></td>
							<td align='center'><b>".$onecurrent_otp."</b></td>
							<td align='center'><b>".$oneotp_balance."</b></td>
							<td align='center'><b>".$onemonthly_os."</b></td>
							<td align='center'><b>".$onemonthly_debit."</b></td>
							<td align='center'><b>".$onemonthly_credit."</b></td>
							<td align='center'><b>".$onemonthly_balance."</b></td>
							<td align='center'><b>".$oneto_total."</b></td></tr> 
							<tr border=0>
								<td border=0 colspan='10'>".$val."</td>
							</tr>";
				}
				$val=0;
				$data.=  "
						 
						<tr bgcolor='#000000'>
							<td colspan='2'><b><font color='#FFFFFF'>Total (Total for all states)</font></b></td>
						  	<td  align='center'><font color='#FFFFFF'>".$ALLotp_os."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLcurrent_otp."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLotp_balance."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLmonthly_os."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLmonthly_debit."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLmonthly_credit."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLmonthly_balance."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLto_total."</font></td></tr>";
				$data.=  "</tbody> </table>
				 
				</div>
				</div>
				</div>
				</div> ";
		
				}
			  
	     
			$filename="Summary Sheet for Active consumer ".date('d M Y hi A').'.pdf';
			$files=(string)$filename;
			
			$checkPdf=$this->pdfGenerateAction($data,$files);
			array_push($StateName_Array, $files);
			
		
			$states=$stateMapper->getAllStates();
			if(count($states)>0){
				//$data="";
				$datas_val="";
				$datas_val.= " <h4><center>Summary Sheet for All consumer ".date('jS F, Y')." at ".date('h:i A')."</center></h4>";
				$datas_val.= " <div>
			
			 <div class='panel-body'>
			 <div class='table-scrollable'>
			 <div class='adv-table'>
			 <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
			 <tbody>
			 <tr>
			 <th colspan='2'>Summary Sheet for ".date('M, Y')."</th>
			 <th colspan='3'>One-Time Payment</th>
			 <th colspan='4'>Monthly Billing</th>
			 <th colspan='1'>Total</th>";
			
				$datas_val.= "<tr>
			 <th width='50px'>S.No</th>
			 <th width='150px'>Site Name</th>
			
			 <th width='100px'>OS</th>
			 <th width='140px'>Collected This Month</th>
			 <th width='90px'>Balance</th>
			
			 <th width='100px'>Prev. OS</th>
			 <th width='130px'>Billed This Month</th>
			 <th width='140px'>Collected This Month</th>
			 <th width='90px'>Balance</th>
			
			 <th width='80px'>To Collect</th>
			
			
			 </tr>";
				$ALLotp_os=0; $ALLcurrent_otp=0; $ALLotp_balance=0;$ALLmonthly_os=0;
				$ALLmonthly_debit=0; $ALLmonthly_credit=0; $ALLmonthly_balance=0; $ALLto_total=0;
				foreach($states as $state) {
					$oneotp_os=0;
					$onecurrent_otp=0;
					$oneotp_balance=0;
					$onemonthly_os=0;
					$onemonthly_debit=0;
					$onemonthly_credit=0;
					$onemonthly_balance=0;$oneto_total=0;
					$datas_val.=" <tr bgcolor='#000000'><th colspan='10'><font color='#FFFFFF'>".$state->__get("state_name")."</font> </th></td>";
					$clusters=$clusterMapper->getClusterByStateId($state->__get("state_id"));
					foreach ($clusters as $cluster){
						$otp_os=0;$current_otp=0;$otp_balance=0;$monthly_os=0;$monthly_debit=0;$monthly_credit=0;
						$monthly_balance=0;$to_total=0;
						$sites=$siteMapper->getSiteByClusterId($cluster->__get("cluster_id"),1);
						$count=0;$main_summary_array=array();
						if($sites){
							$datas_val.=" <tr bgcolor='#808080'><th colspan='10' align='left'><font color='#FFFFFF'> ".$cluster->__get("cluster_name").",".$state->__get("state_name")."</font> </th></td>";
							$total_data_valss=array();
							foreach ($sites as $site)
							{
									 
								$totalOutStanding=$cashRegisterMapper->getTotalOutstandingAmountForSummary($timestamp,$site->__get("site_id"));
								$consumersVal=$consumerMapper->collectionSheetDetailsSiteWise($site->__get("site_id"),$month,$year);
			
								$total_paid_activation=($consumersVal['total_paid_activation']==null?0:$consumersVal['total_paid_activation']);
								$total_activation=($consumersVal['total_activation']==null?0:$consumersVal['total_activation']);
								$current_activation=($consumersVal['current_activation']==null?0:$consumersVal['current_activation']);
								$current_debit=($consumersVal['current_debit']==null?0:$consumersVal['current_debit']);
								$current_credit=($consumersVal['current_credit']==null?0:$consumersVal['current_credit']);
			
								$OS_OTP=$total_activation-$total_paid_activation;
			
								$OTP_balance=$OS_OTP-$current_activation;
								$OS_balance=($totalOutStanding+$current_debit)-$current_credit;
			
								$total=$OTP_balance+$OS_balance;
			
								if($OS_OTP==0){$OS_OTP="-";}
								if($current_activation==0){$current_activation="-";}
								if($OTP_balance==0){$OTP_balance="-";}
								if($totalOutStanding==0){$totalOutStanding="-";}
								if($current_debit==0){$current_debit="-";}
								if($current_credit==0){$current_credit="-";}
								if($OS_balance==0){$OS_balance="-";}
								if($total==0){$total="-";}
							  
								$datas=array(
										"count"=>$count,
										"site_name"=>$site->__get("site_name"),
										"OS_OTP"=>$OS_OTP,
										"current_activation"=>$current_activation,
										"OTP_balance"=>$OTP_balance,
										"totalOutStanding"=>$totalOutStanding,
										"current_debit"=>$current_debit,
										"current_credit"=>$current_credit,
										"OS_balance"=>$OS_balance,
										"total"=>$total,
								);
								$main_summary_array[]=$datas;
			
			
			
								$otp_os=$otp_os+$OS_OTP;
								$current_otp=$current_otp+$current_activation;
								$otp_balance=$otp_balance+$OTP_balance;
								$monthly_os=$monthly_os+$totalOutStanding;
								$monthly_debit=$monthly_debit+$current_debit;
								$monthly_credit=$monthly_credit+$current_credit;
								$monthly_balance=$monthly_balance+$OS_balance;
								$to_total=$to_total+$total;
							 
						}
							$sort_arr = array();
							foreach ($main_summary_array as $key => $row)
							{
								$sort_arr[$key] = $row['total'];
							}
							array_multisort($sort_arr, SORT_DESC ,$main_summary_array);
							$total_data_valss = $main_summary_array;
			
							foreach ($total_data_valss as $total_data_vals){
								$count= $count+1;
			
								$datas_val.= "  <tr>
							<td>".$count."</td>
							<td>".$total_data_vals['site_name']."</td>
							<td align='center'>".$total_data_vals['OS_OTP']."</td>
				  			<td align='center'>".$total_data_vals['current_activation']."</td>
							<td align='center'>".$total_data_vals['OTP_balance']."</td>
							<td align='center'>".$total_data_vals['totalOutStanding']."</td>
							<td align='center'>".$total_data_vals['current_debit']."</td>
							<td align='center'>".$total_data_vals['current_credit']."</td>
							<td align='center'>".$total_data_vals['OS_balance']."</td>
							<td align='center'>".$total_data_vals['total']."</td>
		
							</tr> ";
							}
			
							$datas_val.= "  <tr>
							<td colspan='2'><b>Subtotal</b></td>
							<td align='center'><b>".$otp_os."</b></td>
							<td align='center'><b>".$current_otp."</b></td>
							<td align='center'><b>".$otp_balance."</b></td>
							<td align='center'><b>".$monthly_os."</b></td>
							<td align='center'><b>".$monthly_debit."</b></td>
							<td align='center'><b>".$monthly_credit."</b></td>
							<td align='center'><b>".$monthly_balance."</b></td>
							<td align='center'><b>".$to_total."</b></td>
							</tr>";
						$ALLotp_os=$ALLotp_os+$otp_os;
							$ALLcurrent_otp=$ALLcurrent_otp+$current_otp;
							$ALLotp_balance=$ALLotp_balance+$otp_balance;
							$ALLmonthly_os=$ALLmonthly_os+$monthly_os;
							$ALLmonthly_debit=$ALLmonthly_debit+$monthly_debit;
							$ALLmonthly_credit=$ALLmonthly_credit+$monthly_credit;
							$ALLmonthly_balance=$ALLmonthly_balance+$monthly_balance;
							$ALLto_total=$ALLto_total+$to_total;			
	
						$oneotp_os=$oneotp_os+$otp_os;
						$onecurrent_otp=$onecurrent_otp+$current_otp;
						$oneotp_balance=$oneotp_balance+$otp_balance;
						$onemonthly_os=$onemonthly_os+$monthly_os;
						$onemonthly_debit=$onemonthly_debit+$monthly_debit;
						$onemonthly_credit=$onemonthly_credit+$monthly_credit;
						$onemonthly_balance=$onemonthly_balance+$monthly_balance;
						$oneto_total=$oneto_total+$to_total;
					}
					}
					$val=0;
					$datas_val.=  "
						<tr border=0>
							<td border=0 colspan='10'>".$val."</td>
						</tr>
						<tr >
						<td colspan='2'><b>Total for ".$state->__get('state_name')."</b></td>
						    <td><b>".$oneotp_os."</b></td>
							<td align='center'><b>".$onecurrent_otp."</b></td>
							<td align='center'><b>".$oneotp_balance."</b></td>
							<td align='center'><b>".$onemonthly_os."</b></td>
							<td align='center'><b>".$onemonthly_debit."</b></td>
							<td align='center'><b>".$onemonthly_credit."</b></td>
							<td align='center'><b>".$onemonthly_balance."</b></td>
							<td align='center'><b>".$oneto_total."</b></td></tr>
					<tr border=0>
					<td border=0 colspan='10'>".$val."</td>
					</tr>";
				}
				$datas_val.=  "
						 
						<tr bgcolor='#000000'>
							<td colspan='2'><b><font color='#FFFFFF'>Total (Total for all states)</font></b></td>
						  	<td  align='center'><font color='#FFFFFF'>".$ALLotp_os."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLcurrent_otp."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLotp_balance."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLmonthly_os."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLmonthly_debit."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLmonthly_credit."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLmonthly_balance."</font></td>
							<td align='center'><font color='#FFFFFF'>".$ALLto_total."</font></td></tr>";
				$datas_val.=  "</tbody> </table>
			
				</div>
				</div>
				</div>
				</div> ";
			
			}
				
			 
			$filename="Summary Sheet for All consumer ".date('d M Y hi A').'.pdf';
			$files=(string)$filename;
			$checkPdf=$this->pdfGenerateAction($datas_val,$files);
			array_push($StateName_Array, $files); 
			 
			
		 	}
		 	$auth=new My_Auth('user');
		 	$email_id=$auth->getIdentity()->user_email;
		  	 $this->sendMailStateWiseAction($StateName_Array,$email_id);   
		 	 
		  break;
	  

	 case 'Ytd_Sheet':
				
				$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
				$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
				$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
				$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
				$consumer_type = ($request->getParam("consumer_type")=="" || $request->getParam("consumer_type")=='undefined' )?NULL:explode(",", $request->getParam("consumer_type"));
				$file_type = $request->getParam("file_type");
				$cron = $request->getParam("cron");
				
				$packages=NULL;
				if ($package_type!=NULL){
					$packages="(".implode(",", $package_type).")";
				}
				
				$stateMapper=new Application_Model_StatesMapper();
				$clusterMapper=new Application_Model_ClustersMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$siteMapper=new Application_Model_SitesMapper();
				$consumerMapper=new Application_Model_ConsumersMapper();
				$packageMapper=new Application_Model_PackagesMapper();
				$schemeMapper=new Application_Model_SchemesMapper();
				$userMapper=new Application_Model_UsersMapper();
				$roleSiteMapper=new Application_Model_RoleSiteMapper();
				$collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
				$consumersMapper = new Application_Model_ConsumersMapper();
				
		 		$StateName_Array=array();

		 		$date = new Zend_Date();
		 		$date->setTimezone("Asia/Calcutta");
		 		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
		 		$month = $trans_date["month"];
		 		$year= $trans_date["year"];
		 		
		 		if($cron=="yes"){
		 			$role_sites_id=NULL; $site_id=NULL; $state_id=NULL;
		 			$sites_arr=$siteMapper->getAllSites();
		 			if($sites_arr){
		 				foreach ($sites_arr as $sites_arrs){
		 					$role_sites_id[]=$sites_arrs->__get("site_id"); 
		 				}
		 			}
		 			$year_out=$year;
		 			for ($y=4;$y>=1;$y--){
		 				$dateObj   = DateTime::createFromFormat('!m', $month);
		 				$monthName = $dateObj->format('M');
		 				
		 				$months_name[] = $monthName;
		 				$month_num[] = $month;
		 				$year_arr[]=$year_out;
		 			 	$year_out=($month==1)?$year_out-1:$year_out;
		 			
		 				$month=($month==1)?12:$month-1;
		 				$data[]=$date;
		 			}
		 				
		 			for($y=3;$y>=0;$y--){
		 				$months[]=$months_name[$y]; $month_no[]=$month_num[$y]; $year_arrs[]=$year_arr[$y];
		 			}
		 				
		 			$outstanding_month=$month_no[0];
		 			$outstanding_year=$year_arrs[0];
					$dateObj   = DateTime::createFromFormat('!m', $outstanding_month);
		 			$monthName_data = $dateObj->format('M');
					$from=$monthName_data.", ".$outstanding_year;
					$dateObj   = DateTime::createFromFormat('!m', $month_no[3]);
		 			$monthName_data = $dateObj->format('M');
					$to=$monthName_data.", ".$year_arrs[3];
		 			$datetime=$outstanding_year."-".$outstanding_month."-01";
		 			 
		 		}else{
		 			$currDate = $request->getParam("secondDate");
		 			$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
		 			$currDate = $zendDates->toString("yyyy-MM-dd");
		 			
		 			$lastDate = $request->getParam("firstDate");
		 			$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
		 			$lastDate = $zendDate->toString("yyyy-MM-dd");
		 			 
		 			$counter=0;
		 			$secondNext=$lastDate;
		 			while (strtotime($secondNext)<strtotime($currDate)){
		 				$counter=$counter+1;
		 				$secondCurr = date('Y-m-d', strtotime($secondNext."+1 month"));

		 				$trans_date=date_parse_from_format("Y-m-d", $secondNext);
		 				$month = $trans_date["month"];
		 				$year_out= $trans_date["year"];
		 				
		 				$dateObj   = DateTime::createFromFormat('!m', $month);
		 				$monthName = $dateObj->format('M');
		 				
		 				$months[] = $monthName;
		 				$month_no[] = $month;
		 				$year_arrs[]=$year_out;
		 				
		 				$year_out=($month==1)?$year_out-1:$year_out;
		 				
		 				$month=($month==1)?12:$month-1;
		 				
		 				$secondNext = date('Y-m-d', strtotime($secondNext."+1 month"));
		 			}
		 			 	
		 			$outstanding_month=$month_no[0];
		 			$outstanding_year=$year_arrs[0];
					$dateObj   = DateTime::createFromFormat('!m', $outstanding_month);
		 			$monthName_data = $dateObj->format('M');
					$from=$monthName_data.", ".$outstanding_year;
					$dateObj   = DateTime::createFromFormat('!m', $month_no[$counter-1]);
		 			$monthName_data = $dateObj->format('M');
					$to=$monthName_data.", ".$year_arrs[$counter-1];
		 			$datetime=$outstanding_year."-".$outstanding_month."-01";
		 			
			 		$roleSession = new Zend_Session_Namespace('roles');
			 		$role_sites_id=$roleSession->site_id;
			 	}
			 	
			 	
			 	$sites=$siteMapper->getSitesByStateAndSite($site_id,$state_id,$role_sites_id);
			 	$counters=0; 
					  
 				if($sites){  
					foreach ($sites as $site)
				  	{
				  		$consumerdata="";
			    	 	if($site["site_id"]!=1){  
					 		$stateName=$stateMapper->getStateById($site["state_id"]);
			    	 		$consumerdata.= " <h4 align='center'>Collection Sheet from ".$from." to ".$to."</h4> 
									  		  <h4 align='left'>".$site["site_name"].", ".$stateName->__get("state_name")."</h4> ";
				    	 	
			    	 		$collectionAgent=$collectionAgentMapper->getCollBySiteId($site["site_id"]);
				    	 	$siteOutstaning=$cashRegisterMapper->getMonthlyDueBySiteId($site["site_id"]);
				    	 	$totalOutSite=$siteOutstaning["debit_amount"]-$siteOutstaning["credit_amount"];
							   
				    	 	if($collectionAgent){
				    	 		$consumerdata.= " <h4 align='right'>Agent Name - ".$collectionAgent[0]["agent_fname"]." ".$collectionAgent[0]["agent_lname"]."</h4>
												  <h4 align='right'> Mobile No. - ".$collectionAgent[0]["agent_mobile"]."</h4>
											      <h4 align='right'> Total Outstanding - ".intval($totalOutSite)."</h4> ";
				    	 	}
				    	 	$consumerdata.= "<div>
				     							<table border=1 align='center'>
	                       	 						<tbody>
	                         							<tr>";
				    	 	$consumerdata.= "<tr> <th colspan='13'></th>";
				    	 	
							if($file_type=='Excel'){
								$consumerdata.= "<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>"; 
							}							
				    	 	for($m=0 ; $m<sizeof($month_no);$m++)
				    	 	{
								if($file_type=='Excel'){
									$consumerdata.= "<th></th>"; 
								}	
								$consumerdata.= "<th colspan='3'>".$months[$m].",".$year_arrs[$m]."</th>"; 
								if($file_type=='Excel'){
									$consumerdata.= "<th></th>"; 
								}	
				    	 	}
							
				    	 	$consumerdata.= "</tr>";
				    	 	$consumerdata.= "<tr>
						    	 	<th width='50px'>S.No</th>
						    	 	<th width='130px'>Connection ID</th>
						    	 	<th width='130px'>Consumers Name</th>
						    	 	<th width='50px'>Package type</th>
						    	 	<th width='100px'>Mobile No.</th>
						    	 	<th width='50px'>Status</th>
						    	 	<th width='50px'>Count EMIs/ Packages</th>
									<th width='50px'>Act/ Banned/ Scheme Date</th>
									<th width='50px'>Security deposit</th>
									<th width='50px'>Dues ACT</th>
									<th width='50px'>Dues OTHERS</th>
									<th width='50px'>Type</th>
									<th>Opening Balance</th>";
			    	 	
				    	 	for($m=0 ; $m<sizeof($month_no);$m++)
				    	 	{
								$consumerdata.= "<th>Pre Debit</th><th>post Debit</th><th>Credit</th>";
							}
							$consumerdata.= "<th>Balance</th><th>Remark</th></tr>";
							 
			    	 		$consumers=$consumerMapper->getConsumersDetailsBySiteId($site["site_id"],null,$packages,$consumer_type,$package_id);
    	 					if($consumers){
			    	 			$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			    	 			$total_ACT=0;$total_OTHER=0;$total_SD=0;$total_out_val=0;$otherOut=0;
			    	 			foreach ($consumers as $consumer){
			    	 				$consumer_id=$consumer->__get("consumer_id");
								//if($consumer->__get("consumer_connection_id")=='BHBHLD01101'){
			    	 				$count= $count+1;
			    	 				$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
			    	 				$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
			    	 				$meteres=array();$totalPackages=0;
			    	 				if($consumerPackages){
			    	 					$totalPackages=count($consumerPackages);
			    	 					foreach ($consumerPackages as $consumerPackage){
											$sites_val=$packageMapper->getPackageById($consumerPackage["package_id"]);
											if($sites_val){
												if($sites_val->__get("is_postpaid")==2){
													$meteres[]="MM ";
												}elseif ($sites_val->__get("is_postpaid")==1){
													$meteres[]="ME ";
												}else{
													$meteres[]="FIXED ";
												}
											}
			    	 					}
			    	 			   }     
			    	 			$metered=" (".implode(",", $meteres).")";
			    	 			$other_value=intval($cashRegisterMapper->getOtherAmtByConsumerId($consumer_id,'OTHERS')) + intval($cashRegisterMapper->getOtherAmtByConsumerId($consumer_id,'METER'));
								$SD_value=abs($cashRegisterMapper->getOtherAmtByConsumerId($consumer_id,'SD'));
								      
			    	 			$ACT_value=($consumerMapper->getRemainingActivation($consumer_id)) - intval($cashRegisterMapper->getDiscountByConsumerId($consumer_id,'ACT')); 
			    	 			$total_OTHER=$total_OTHER+$other_value; 
			    	 			$total_ACT=$total_ACT+$ACT_value;
			    	 			$total_SD=$total_SD+$SD_value;
			    	 			if($consumer->__get("consumer_status")=='banned'){
			    	 				$zendDate = new Zend_Date($consumer->__get("suspension_date"),"yyyy-MM-dd");
			    	 				$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
			    	 			}else{
			    	 				$zendDate = new Zend_Date($consumer->__get("consumer_act_date"),"yyyy-MM-dd");
			    	 				$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
			    	 			}
			    	 			$consumerdata.= "<tr>
			    	 						  	 	<td>".$count."</td>
			    	 								<td>".$consumer->__get("consumer_connection_id")."</td>
			    	 								<td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
			    	 								<td>".$metered."</td>
			    	 								<td>".$consumer->__get("consumer_code")."</td>
			    	 								<td>".$consumer->__get("consumer_status")."</td>";
			    	 			$total_type=array("CHG","EED","MED","WATER");
								$counter=0;
			    	 			for ($j=0;$j<count($total_type);$j++){
									$year_balance=0; $totalDebit=0; $totalCredit=0;
			    	 				$checkStatus=$cashRegisterMapper->checkEntryAmtByConsumerId($consumer_id,$total_type[$j],$datetime);
			    	 				if($checkStatus){
			    	 					if($counter!=0){
			    	 						$consumerdata.="<tr>
			    	 											<td>".NULL."</td><td>".NULL."</td><td>".NULL."</td>
			    	 											<td>".NULL."</td><td>".NULL."</td> <td>".NULL."</td>";
			    	 					}
			    	 	 				if($total_type[$j]=='CHG'){
			    	 						$consumerdata.="<td>".$totalPackages."</td>
			    	 										<td>".$consumer_act_date."</td>";
			    	 					}elseif ($total_type[$j]=='EED'){
			    	 						$total_date=array();$total_EMI=array();
			    	 						$con_schemes=$schemeMapper->getSchemeDetailsByConsumerId($consumer_id);
			    	 						if($con_schemes){
			    	 							foreach ($con_schemes as $con_scheme){
			    	 								$zendDate = new Zend_Date($con_scheme["timestamp"],"yyyy-MM-dd");
			    	 								$con_scheme_date = $zendDate->toString("dd-MM-yyyy");
			    	 								$total_date[]=" ".$con_scheme_date;
			    	 								$total_EMI[]=$con_scheme["emi_duration"];
			    	 							}
			    	 							$consumerdata.="<td>".implode(",", $total_EMI)."</td>
			    	 											<td>".implode(",", $total_date)."</td>";
			    	 						}else{
			    	 							$consumerdata.="<td>".NULL."</td><td>".NULL."</td>";
			    	 						}
			    	 					}else{
			    	 						$consumerdata.="<td>".NULL."</td><td>".NULL."</td>";
			    	 					}
			    	 														  												
			    	 					if($counter==0){
			    	 						$consumerdata.="<td>".intval($SD_value)."</td>
			    	 										<td>".$ACT_value."</td>
			    	 										<td>".($other_value)."</td>";
			    	 					}else{
			    	 						$consumerdata.="<td>".NULL."</td><td>".NULL."</td><td>".NULL."</td>";
			    	 					}
										
			    	 					$counter=$counter+1;
			    	 					$outstanding_value=$cashRegisterMapper->getOutByConsumerId($consumer_id,$month,$year,null,null,$total_type[$j],$datetime);
										/*$trans_dates=date_parse_from_format("Y-m-d", $datetime);
										$month_data_val = $trans_dates["month"];
										$year_data_val= $trans_dates["year"];
										if($month_data_val==1){
											$month_data_val=12;
											$year_data_val=$year_data_val-1;
										}else{
											$month_data_val=$month_data_val-1;
											$year_data_val=$year_data_val;
										}
										$last_debit=0;
										if($total_type[$j]=='CHG'){
											$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_data_val, $year_data_val);
											$last_debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_data_val,$year_data_val,null,$total_type[$j],true,1,$total_days_in_month);
										}*/
										
										//$outstanding_value=$outstanding_value-($last_debit);
			    	 					$consumerdata.="<td>".$total_type[$j]."</td>
			    	 									<td>".$outstanding_value."</td>";
									  	$total_out_val=$total_out_val+$outstanding_value;
									  
										for($m=0 ; $m<sizeof($month_no);$m++)
				    	 				{
											$debit_change=0;
											if($total_type[$j]=='CHG'){
												$pre_debits=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$m],$year_arrs[$m],null,$total_type[$j],true,0);
												$post_debits=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$m],$year_arrs[$m],null,$total_type[$j],true,2);
												/*if($month_no[$m]==1){
													$month_data=12;
													$year_data=$year_arrs[$m]-1;
												}else{
													$month_data=$month_no[$m]-1;
													$year_data=$year_arrs[$m];
												}
												
												$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_data, $year_data);
												$debit1=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_data,$year_data,null,$total_type[$j],true,1,$total_days_in_month);
												$total_days_in_months=cal_days_in_month(CAL_GREGORIAN, $month_no[$m],$year_arrs[$m]);
												$debit_change=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$m],$year_arrs[$m],null,$total_type[$j],true,1,$total_days_in_months,true);
												
												$debits=$debit+$debit1+$debit_change; */
												$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_no[$m],$year_arrs[$m]);
												$debit_change=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$m],$year_arrs[$m],null,$total_type[$j],true,1,$total_days_in_months,true);
											}else{
												$pre_debits=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$m],$year_arrs[$m],null,$total_type[$j]);
												$post_debits=0;
											}
											
											$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$m],$year_arrs[$m],null,$total_type[$j]);
											if($debit_change>0){
												$consumerdata.= "<td>".$pre_debits."</td><td>".$post_debits."<sup>*</sup></td>";
											}else{
												$consumerdata.= "<td>".$pre_debits."</td><td>".$post_debits."</td>";
											}
											$debits=$pre_debits+$post_debits;
			    	 						$consumerdata.= "<td>".$credit."</td>";
			    	 									  				
			    	 						$totalDebit=$totalDebit+$debits;
			    	 						$totalCredit=$totalCredit+$credit;
			    	 												  			 
			    	 					}
										
			    	 					$year_balances=$totalDebit - $totalCredit;
			    	 												  						
										$year_balance=$year_balances+$outstanding_value+$ACT_value+$other_value; 
										$otherOut=$otherOut+$year_balance;
			    	 					$consumerdata.= "<td align='center'>". $year_balance."</td><td></td></tr> ";
			    	 				}
			    	 			}
								  
									if($counter==0){
										$consumerdata.="<td>".$totalPackages."</td>
														<td>".$consumer_act_date."</td>
														<td>".intval($SD_value)."</td>
														<td>".$ACT_value."</td>
														<td>".$other_value."</td>";
										$outstanding_value=$cashRegisterMapper->getOutByConsumerId($consumer_id,$month,$year,null,null,null,$datetime);
										 
										$consumerdata.="<td>CHG</td> 
														<td>".$outstanding_value."</td>";
										
										$total_out_val=$total_out_val+$outstanding_value;
										for($m=0 ; $m<sizeof($month_no);$m++)
				    	 				{
			    	 						$consumerdata.= "<td>".NULL."</td><td>".NULL."</td> <td>".NULL."</td>";
			    	 					} 
										$overall=$outstanding_value+$ACT_value+$other_value;
										$otherOut=$otherOut+$overall;
										$consumerdata.= "<td align='center'>".$overall."</td><td></td></tr> ";	
									}
									//}   //comment
								
							}
							 
									$totalDebit_bal=0;$totalCredit_bal=0;
									$Total_ACT_value=0;$Total_other_value=0;
									$consumerdata.= "<tr>";
									
									if($file_type=='Excel'){
										$consumerdata.= "<th></th><th></th><th></th><th></th><th></th><th></th><th></th>"; 
									}
									
									$consumerdata.= "<td colspan='8' align='right'>Grand total</td>
													 <td>".$total_SD."</td><td>".$total_ACT."</td>
													 <td>".$total_OTHER."</td><td></td><td>".$total_out_val."</td>";
									
									$total_type=array("CHG","EED","MED","WATER");
									for($m=0;$m<sizeof($month_no);$m++)
									{
										$total_pre_dt=0;$total_post_dt=0;$total_ct=0;
										for ($j=0;$j<count($total_type);$j++){
											if($total_type[$j]=='CHG'){
												$total_amount_val_pre=$cashRegisterMapper->getMonthlyDueBySiteId($site["site_id"],$month_no[$m],$year_arrs[$m],true,NULL,true,0,null,null,$total_type[$j]);
												$total_amount_val_post=$cashRegisterMapper->getMonthlyDueBySiteId($site["site_id"],$month_no[$m],$year_arrs[$m],true,NULL,true,2,null,null,$total_type[$j]);
												/*if($month_no[$m]==1){ 
													$month_data=12;$year_data=$year_arrs[$m]-1;
												}else{
													$month_data=$month_no[$m]-1;$year_data=$year_arrs[$m];
												}
												$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_data, $year_data);
												$total_outstanding_value=$cashRegisterMapper->getMonthlyDueBySiteId($site["site_id"],$month_data,$year_data,true,NULL,true,1,$total_days_in_month,null,$total_type[$j]);
												$total_days_in_months=cal_days_in_month(CAL_GREGORIAN, $month_no[$m],$year_arrs[$m]);
												$total_outstanding_value_activity=$cashRegisterMapper->getMonthlyDueBySiteId($site["site_id"],$month_no[$m],$year_arrs[$m],true,NULL,true,1,$total_days_in_months,true,$total_type[$j]);
												 
												$total_debit_data=$total_amount_val["debit_amount"]+$total_outstanding_value["debit_amount"]+$total_outstanding_value_activity["debit_amount"];*/
												
												$total_pre_dt=$total_pre_dt+$total_amount_val_pre["debit_amount"];
												$total_post_dt=$total_post_dt+$total_amount_val_post["debit_amount"];
												$total_ct=$total_ct+$total_amount_val_pre["credit_amount"];
											}else{
												$total_amount_val=$cashRegisterMapper->getMonthlyDueBySiteId($site["site_id"],$month_no[$m],$year_arrs[$m],true,NULL,null,null,null,null,$total_type[$j]);
												$total_pre_dt=$total_pre_dt+$total_amount_val["debit_amount"];
												$total_ct=$total_ct+$total_amount_val["credit_amount"];
											}
										}
										
										$consumerdata.= "<td>".$total_pre_dt."</td>
														 <td>".$total_post_dt."</td>
														 <td>".$total_ct."</td>";
										
										$totalDebit_bal=$totalDebit_bal+$total_pre_dt+$total_post_dt;
										$totalCredit_bal=$totalCredit_bal+$total_ct;
									}
									
			    	 		$bal=($totalDebit_bal-$totalCredit_bal)+$total_out_val+$total_ACT+$total_OTHER;  
			    	 		$consumerdata.= "<td align='center'>". $bal."</td><td></td></tr> ";
			    	 	}
						//echo $consumerdata;exit;  
																
			    		$consumerdata.=  "</tbody> </table>
			    	 						</div>
			    	 						<h5 align='right'><i>File created on ".date('jS F, Y')." at ".date('h:i A'). " </i></h5>";
			    	 																									
					  	 
         	 		 if($file_type=='PDF'){
         	 		 	$filename="Collection Sheet for ".$site["site_name"]." ".date('jS F, Y').'.pdf';
         	 		 	$files=(string)$filename;
         	 		 	$checkPdf=$this->pdfGeneratesAction(null,$consumerdata,$files);
         	 		 	array_push($StateName_Array, $files);
         	 		 }else{
         	 		 	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
         	 		 	$cacheSettings = array( 'memoryCacheSize' => '8192MB');
         	 		 	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings); $objPHPExcel=null;
						$filename = "Collection Sheet for ".$site["site_name"]." ".date('jS F, Y').'.xlsx';
         	 		 	$files=(string)$filename; $inputFileType = 'Excel2007';
         	 		 	
         	 		 	$tmpfile = tempnam("/html/excelBackup/", 'html'); file_put_contents($tmpfile, $consumerdata);
         	 		 	
         	 		 	$objPHPExcel = new PHPExcel(); $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
         	 		 	$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
         	 		 	$objPHPExcel->getActiveSheet()->setTitle('Collection Sheet');
         	 		 	
         	 		 	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         	 		 	$objWriter->save($files); $objPHPExcel->disconnectWorksheets();
         	 		 	unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings); gc_enable() ;gc_collect_cycles();
         	 		 }
         	 		 array_push($StateName_Array, $files); 
				  }		
				}
			  }
			  
			  if($cron!='yes'){
					$zipname = 'Collection Sheet '.date('jS F, Y').' at '.date('h:i A'). '.zip';
					$zip = new ZipArchive(); 
					defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
					if(file_exists(PUBLIC_PATH . "/html/".$zipname)) {
						unlink (PUBLIC_PATH . "/html/".$zipname);
					}
					if ($zip->open(PUBLIC_PATH . "/html/".$zipname, ZIPARCHIVE::CREATE) != TRUE) {
						die ("Could not open archive");
					}
				    foreach ($StateName_Array as $file) {
					  $zip->addFile($file);
					}
					$zip->close(); 
					$arr = array( "data" => "http://taraurja.in/".$zipname );
					$json = json_encode($arr, JSON_PRETTY_PRINT);
					echo $json;exit;
					
			  }else{
				  	    $reportPerMapper=new Application_Model_ReportPermissionMapper();
				   		$userMapper=new Application_Model_UsersMapper();
				   		$RPs=$reportPerMapper->getReportsByReportName('Collection Sheet');
				   		$roleSiteMapper=new Application_Model_RoleSiteMapper();
						$temp=0;$Email_id="";
						if($RPs){ 
							$users_details=explode(",", $RPs['user_id']);
							for ($i=0;$i<count($users_details);$i++){
								$users=$userMapper->getUserById($users_details[$i]);
								$user_Role_id=$users->__get("user_role");
								$Email_id=$users->__get("user_email");
								$role_site=$roleSiteMapper->getAllRoleSiteById($user_Role_id);
								$sites_val=array();
								if($role_site){ 
									foreach ($role_site as $role_sites){
										$site_id=$role_sites->__get("site_id");
										$sites_val=$siteMapper->getSiteById($site_id);
										$site_name=$sites_val->__get('site_name');
										if(in_array('Collection Sheet for '.$site_name.' '.date('jS F, Y').'.pdf', $StateName_Array)){
											$filename='Collection Sheet for '.$site_name.' '.date('jS F, Y').'.pdf';
											$files=(string)$filename; 
											//$Email_id='manoj.kumar@acuminous.in'; 
											$checkPdf=$this->sendMailForCollectionAction($files,$Email_id);    
										}
									}
								}
							 }
						 }    
						 echo " success"; exit; 
			  }
		break;
		
		 
		
		case 'Ytd_Sheet_his':

                $month_name= $request->getParam("month");
                $month = date("m", strtotime($month_name));

                $year= $request->getParam("year");

                $fixed= $type = $request->getParam("fixed");
                $mm= $request->getParam("mm");
                $me= $request->getParam("me");


                if($fixed==1 && $mm==0 && $me==0){
                    $packages="(0)";
                    $footer="Showing fixed package";
                }elseif($fixed==0 && $mm==1 && $me==0){
                    $packages="(2)";
                    $footer="Showing mixed mode package";
                }elseif($fixed==0 && $mm==0 && $me==1){
                    $packages="(1)";
                    $footer="Showing metered package";
                }elseif($fixed==1 && $mm==1 && $me==1){
                    $packages="(0,1,2)";
                    $footer="Showing fixed package, mixed mode package, metered package";
                }elseif ($fixed==1 && $mm==1 && $me==0){
                    $packages="(0,2)";
                    $footer="Showing fixed package, mixed mode package";
                }elseif ($fixed==1 && $mm==0 && $me==1){
                    $packages="(0,1)";
                    $footer="Showing fixed package, metered package";
                }elseif ($fixed==0 && $mm==1 && $me==1){
                    $packages="(2,1)";
                    $footer="Showing mixed mode package, metered package";
                }else {
                    $packages="(5)";
                    $footer="No result found";
                }
                $stateMapper=new Application_Model_StatesMapper();
                $clusterMapper=new Application_Model_ClustersMapper();
                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $siteMapper=new Application_Model_SitesMapper();
                $consumerMapper=new Application_Model_ConsumersMapper();
                $packageMapper=new Application_Model_PackagesMapper();

                $StateName_Array=array();
                $sites=$siteMapper->getAllSites();
                $counters=0;


                foreach ($sites as $site)
                {
                    $consumerdata="";
                    if($site->__get("site_id")!=1){

                         $consumerdata.= " <h4 align='center'>Collection Sheet for ".$year."</h4>";

                        $stateName=$stateMapper->getStateById($site->__get("state_id"));
                        $consumerdata.= "<div>
			     <table border=1 align='center'>
                        <tbody>
                         <tr>
						  <th colspan='9' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";

                        $consumerdata.= "<tr>
                                <th colspan='4'></th>";

                        $years=$year-1;
                        for($i=11;$i>=0;$i--) {
                            $cs = array();
                            $zendDate = new Zend_Date();
                            $zendDate->setTimezone("Asia/Calcutta");
                                $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_NAME);

                            if ($month == 'January') {
                                $years = $years + 1;
                            }
                            $consumerdata.= "<th colspan='2'>".$month.",".$years."</th>";
                        }

                        $consumerdata.= "</tr>";
                        $consumerdata.= "<tr>
                                <th>S.No</th>
                                <th>Connection ID</th>
				 				<th >Consumers Name</th> 
							  	<th >Mobile No.</th>";

                        for($i=11;$i>=0;$i--) {
                             $consumerdata.= "<th >D</th><th  >C</th>";
                        }
                        $consumerdata.= "
                                <th>Balance</th>
							 </tr>";

                        $consumers=$consumerMapper->getConsumersDetailsBySiteId($site->__get("site_id"),true,$packages);

                        if($consumers){
                            $count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
                            foreach ($consumers as $consumer){
                                $year_balance=0;
                                $totalDebit=0;$totalCredit=0;
                                $consumer_id=$consumer->__get("consumer_id");
                                $count= $count+1;
                                $sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
                                $metered="";
                                if($sites_val){
                                    if($sites_val->__get("is_postpaid")==2){
                                        $metered="<b> (MM)</b>";
                                    }elseif ($sites_val->__get("is_postpaid")==1){
                                        $metered="<b> (ME)</b>";
                                    }}
                                $consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$consumer->__get("consumer_connection_id").$metered."</td>
										    <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
											<td>".$consumer->__get("consumer_code")."</td>";

                               $years=$year-1;
                                for($i=11;$i>=0;$i--) {
                                    $cs = array();
                                    $zendDate = new Zend_Date();
                                    $zendDate->setTimezone("Asia/Calcutta");
                                    $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_SHORT);

                                    if ($month == 1) {
                                        $years = $years + 1;
                                    }
                                    $debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month,$years);
                                    $credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month,$years);
                                    $consumerdata.= "<td>".$debit."</td>
										    <td>".$credit."</td>";
                                    $totalDebit=$totalDebit+$debit;
                                    $totalCredit=$totalCredit+$credit;
                                }
                                $year_balance=$totalDebit - $totalCredit;
                                $consumerdata.= "<td align='center'>". $year_balance."</td>
				                </tr> ";
                            }
                        }


                        $consumerdata.=  "</tbody> </table> </div>
			  <h4 align='center'>Banned Consumers</h4>
			  <div>
			  	
			  		<table border=1 align='center' >
			  			<tbody>
			  			<tr>
			  			<th colspan='10' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";

                        $consumerdata.= "<tr>
							<th colspan='5'></th>";

                        $years=$year-1;
                        for($i=11;$i>=0;$i--) {
                            $cs = array();
                            $zendDate = new Zend_Date();
                            $zendDate->setTimezone("Asia/Calcutta");
                            $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_NAME);

                            if ($month == 'January') {
                                $years = $years + 1;
                            }
                            $consumerdata.= "<th colspan='2'>".$month.",".$years."</th>";
                        }
                        $consumerdata.= "</tr>";
                        $consumerdata.= "<tr>
							  		<th>S.No</th>
							  		<th>Connection ID</th>
							  		<th>Consumers Name</th>
							  		<th>Mobile No.</th>
<th>Banned Date</th>";


                        for($i=11;$i>=0;$i--) {
                            $consumerdata.= "<th>D</th><th>C</th>";
                        }
                        $consumerdata.= "
							  	<th >Balance</th>
							  </tr>";

                        $consumers=$consumerMapper->getConsumersDetailsBySiteId($site->__get("site_id"),null,$packages);

                        if($consumers){
                            $count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
                            foreach ($consumers as $consumer){
                                $year_balance=0;
                                $totalDebit=0;$totalCredit=0;
                                $consumer_id=$consumer->__get("consumer_id");
                                $count= $count+1;
                                $DODate=$consumerMapper->getConsumerDODatebyConnectionId($consumer->__get("consumer_connection_id"));

                                $sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));

                                $metered="";
                                if($sites_val){
                                    if($sites_val->__get("is_postpaid")==2){
                                        $metered="<b> (MM)</b>";
                                    }elseif ($sites_val->__get("is_postpaid")==1){
                                        $metered="<b> (ME)</b>";
                                    }}
                                $consumerdata.= "  <tr>
							  <td>".$count."</td>
							  <td>".$consumer->__get("consumer_connection_id").$metered."</td>
							  <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
							  		<td>".$consumer->__get("consumer_code")."</td>
									<td>".$DODate["timestamp"]."</td>";

                            $years=$year-1;
                                for($i=11;$i>=0;$i--) {

                                    $cs = array();
                                    $zendDate = new Zend_Date();
                                    $zendDate->setTimezone("Asia/Calcutta");
                                    $month = $zendDate->subMonth($i)->toString(Zend_Date::MONTH_SHORT);

                                    if ($month == 1) {
                                        $years = $years + 1;
                                    }

                                    $debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month,$years);
                                    $credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month,$years);

                                    $consumerdata.= "<td>".$debit."</td>
							  			<td>".$credit."</td>";
                                    $totalDebit=$totalDebit+$debit;
                                    $totalCredit=$totalCredit+$credit;
								}
								$year_balance=$totalDebit - $totalCredit;
                                $consumerdata.= "<td align='center'>". $year_balance."</td>
				                </tr> ";
                        }
                        $consumerdata.=  "</tbody> </table>
				                		 
				         	</div> 
				<h5 align='right'><i>File created on ".date('jS F, Y')." at ".date('h:i A')." <br>".$footer."</i></h5>";

                        $filename="Collection Sheet for ".$site->__get("site_name").$footer." ".date('d M Y hi A').'.pdf';
                        $files=(string)$filename;
                        $checkPdf=$this->pdfGeneratesAction(null,$consumerdata,$files);
                        array_push($StateName_Array, $files);
 
                    }

                }
                }

                $auth=new My_Auth('user');
                $email_id=$auth->getIdentity()->user_email;
                $this->sendMailStateWiseAction($StateName_Array,$email_id,'ytd');
                break;
		case 'Ytd_Sheet_25_cycle':
				$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
				$trans_date=date_parse_from_format("Y-m-d", $timestamp);
				$day=$trans_date["day"];
			 	$month= $trans_date["month"];
				//$month= 8;$year=2016;
				$year= $trans_date["year"];
				$stateMapper=new Application_Model_StatesMapper();
				$clusterMapper=new Application_Model_ClustersMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$siteMapper=new Application_Model_SitesMapper();
				$consumerMapper=new Application_Model_ConsumersMapper();
				$packageMapper=new Application_Model_PackagesMapper();
				
		 		$StateName_Array=array();
			  	$sites=$siteMapper->getAllSites(); 
				$counters=0;
				$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
				$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
				
			  foreach ($sites as $site)
			  {
			  	$consumerdata="";
			  	if($site->__get("site_id")!=1){
			  		 
					 $consumerdata.= " <h4 align='center'>Collection Sheet (Old Cycle) for ".date('F, Y')."</h4>";
			  
			  $stateName=$stateMapper->getStateById($site->__get("state_id"));
			  $consumerdata.= "<div>
			  	                <table border=1 align='center'>
                        <tbody>
                         <tr>
						  <th colspan='9' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			 
			  $consumerdata.= "<tr>
                                <th  colspan='4'></th>";

							for($m=0 ; $m<sizeof($months);$m++)
							{
								
									$year_vals=$year;
									if($month >=4)
										$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
										$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th colspan='2'>".$months[$m].",".$year_vals."</th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
                                <th>S.No</th>
                                <th>Connection ID</th>
				 				<th>Consumers Name</th> 
							  	<th>Mobile No.</th>";
								
							for($m=0 ; $m<sizeof($months);$m++)
							{
								 
									$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "
                                <th>Balance</th>
							 </tr>";

			  				$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"),true);
			  				 
			  				if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			  				foreach ($consumers as $consumer){
			  					$year_balance=0;
			  					$totalDebit=0;$totalCredit=0;
			  					$consumer_id=$consumer->__get("consumer_id");	
				  				$count= $count+1;
				  				$sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM)</b>";
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				}
								}
								$consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$consumer->__get("consumer_connection_id").$metered."</td>
										    <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
											<td>".$consumer->__get("consumer_code")."</td>";
								for($n=0 ; $n<sizeof($month_no);$n++)
								{
									 
									$year_val=$year;
									if($month >=4)
										$year_val=($month_no[$n] >=4)?$year:$year+1;
									else {
										$year_val=($month_no[$n] >=4)?$year-1:$year;
									}
									$debit=$cashRegisterMapper->getMonthlyDebitByConsumerId($consumer_id,$month_no[$n],$year_val);
				  					$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
									$consumerdata.= "<td>".$debit."</td>
										    <td>".$credit."</td>";
									$totalDebit=$totalDebit+$debit;
									$totalCredit=$totalCredit+$credit;
									if($month_no[$n]==$month){
										break;
									}
								}		    
								$year_balance=$totalDebit - $totalCredit;
			                     $consumerdata.= "<td align='center'>". $year_balance."</td>
				                </tr> ";
			  				}
				  	 }
			  		  
			  		 
				 
			  $consumerdata.=  "</tbody> </table> </div>
			  <h4 align='center'>Banned Consumers</h4>
			  <div>
			 	
			  		<table border=1 align='center'>
			  			<tbody>
			  			<tr>
			  			<th colspan='10' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			  
						$consumerdata.= "<tr>
							<th colspan='5'></th>";
							  
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
							  	  $year_vals=$year;
								  if($month >=4)
								  	$year_vals=($month_no[$m] >=4)?$year:$year+1;
								  else {
								  	$year_vals=($month_no[$m] >=4)?$year-1:$year;
								  }
								  $consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>";
								  if($month_no[$m]==$month){
								  	break;
								  }
							  }
							  $consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
							  		<th>S.No</th>
							  		<th>Connection ID</th>
							  		<th>Consumers Name</th>
							  		<th>Mobile No.</th>
										<th>Banned Date</th>";
							  
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
							  		$consumerdata.= "<th>D</th><th>C</th>";
							  		if($month_no[$m]==$month){
							  			break;
							 		}
							  }
							  $consumerdata.= "
							  	<th>Balance</th>
							  </tr>";
							  
							  $consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"));
							  
							  if($consumers){
							  $count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
							  foreach ($consumers as $consumer){
							  $year_balance=0;
							  $totalDebit=0;$totalCredit=0;
							  $consumer_id=$consumer->__get("consumer_id");
							  $count= $count+1;
							  $sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
							$DODate=$consumerMapper->getConsumerDODatebyConnectionId($consumer->__get("consumer_connection_id"));
				  				
				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM)</b>";
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				}}
							  $consumerdata.= "  <tr>
							  <td>".$count."</td>
							  <td>".$consumer->__get("consumer_connection_id").$metered."</td>
							  <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
							  		<td>".$consumer->__get("consumer_code")."</td>
										<td>".$DODate["timestamp"]."</td>";
										  		for($n=0 ; $n<sizeof($month_no);$n++)
							  		{
							  			$year_val=$year;
							  			if($month >=4)
							  				$year_val=($month_no[$n] >=4)?$year:$year+1;
							  			else {
							  				$year_val=($month_no[$n] >=4)?$year-1:$year;
							  			}
							  			$debit=$cashRegisterMapper->getMonthlyDebitByConsumerId($consumer_id,$month_no[$n],$year_val);
							  			$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
							  			$consumerdata.= "<td>".$debit."</td>
							  			<td>".$credit."</td>";
							  			$totalDebit=$totalDebit+$debit;
							  			$totalCredit=$totalCredit+$credit;
							  			if($month_no[$n]==$month){
							  					break;
							  		}
							  }
							  		$year_balance=$totalDebit - $totalCredit;
							  		$consumerdata.= "<td align='center'>". $year_balance."</td>
							  		</tr> ";
							  }
							}
					$consumerdata.=  "</tbody> </table>
				                		 
				         	</div> 
						<h5 align='right'><i>File created on ".date('jS F, Y')." at ".date('h:i A')." </i></h5>";
					
         	 		 $filename="Collection Sheet (Old Cycle) for ".$site->__get("site_name")." ".date('d M Y hi A').'.pdf';
         	 		 $files=(string)$filename;
         	 		 $checkPdf=$this->pdfGeneratesAction(null,$consumerdata,$files);
         	 		 array_push($StateName_Array, $files);
         	 		  
				}
			  }
			 
			$auth=new My_Auth('user');
		        $email_id=  $auth->getIdentity()->user_email; 
			$this->sendMailStateWiseAction($StateName_Array,$email_id,'ytd',true);
				break;
	case 'Ytd_Sheet_san': 
					$date = new Zend_Date();
					$date->setTimezone("Asia/Calcutta");
					$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
					$trans_date=date_parse_from_format("Y-m-d", $timestamp);
					$day=$trans_date["day"];
					$month= $trans_date["month"];
					//$month= 8;$year=2016;
					$year= $trans_date["year"];
					$stateMapper=new Application_Model_StatesMapper();
					$clusterMapper=new Application_Model_ClustersMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
				
					$StateName_Array=array();
					$sites=$siteMapper->getAllSites();
					$counters=0;
					$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
					$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
					$consumerdata="<script>
							function check(txnId){ 
								document.getElementById('txn_id').value=txnId; 
							}
							</script>
							<div> 
							<h4 id='tips'>Transaction Id:</h4> <input type='text' id='txn_id'></div>";
					foreach ($sites as $site)
					{
						
						if($site->__get("site_id")!=1){
				
							$consumerdata.= " <h4><center>File created on ".date('jS F, Y')." at ".date('h:i A')."  <br>
			  		YTD Collection Sheet for ".date('F, Y')."</center></h4>";
								
							$stateName=$stateMapper->getStateById($site->__get("state_id"));
							$consumerdata.= "<div>
		<div class='panel-body'>
            	    <div class='table-scrollable'>
             		<div class='adv-table'>
	                <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
                        <tbody>
                         <tr>
						  <th colspan='9' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
				
							$consumerdata.= "<tr>
                                <th width='50px' colspan='5'></th>";
				
							for($m=0 ; $m<sizeof($months);$m++)
							{
				
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>";
											if($month_no[$m]==$month){
											break;
							}
							}
							$consumerdata.= "</tr>";
						 		$consumerdata.= "<tr>
								<th width='50px'>S.No</th>
								<th width='150px'>Connection ID</th>
				 				<th width='130px'>Consumers Name</th>
							  	<th width='130px'>Mobile No.</th>
								<th width='130px'>Outstanding</th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
					
									$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
								if($month_no[$m]==$month){
								break;
								}
								}
									$consumerdata.= "
									<th  width='80px'>Balance</th>
							 </tr>";
				
					$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"),true);
				
									if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
							  					foreach ($consumers as $consumer){
							  							$year_balance=0;
							  							$totalDebit=0;$totalCredit=0;
							  							$consumer_id=$consumer->__get("consumer_id");
							  							$count= $count+1;
							  							$oustanding=$cashRegisterMapper->getOutByConsumerId($consumer_id,$month,$year);
							  								
							  							$consumerdata.= "  <tr>
							  								<td>".$count."</td>
							  								<td>
					  										<a href='http://taraurja.in/consumers/view/id/".$consumer->__get('consumer_id')."' target='_blank'>".$consumer->__get("consumer_connection_id")."</a></td>
							  								<td>".$consumer->__get("consumer_name")."</td>
							  								<td>".$consumer->__get("consumer_code")."</td>
							  								<td>".$oustanding."</td>";
							  								for($n=0 ; $n<sizeof($month_no);$n++)
							  								{
				
							  								$year_val=$year;
							  								if($month >=4)
							  								$year_val=($month_no[$n] >=4)?$year:$year+1;
							  										else {
							  										$year_val=($month_no[$n] >=4)?$year-1:$year;
							  							}
							  							/* $debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$n],$year_val);
							  							$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
							  							$consumerdata.= "<td>".$debit."</td>
							  							<td>".$credit."</td>";
							  							$totalDebit=$totalDebit+$debit;
							  							$totalCredit=$totalCredit+$credit; */
							  							$debit=$cashRegisterMapper->getMonthDebitsByConsumerId($consumer_id,$month_no[$n],$year_val);
							  							$credit=$cashRegisterMapper->getMonthCreditsByConsumerId($consumer_id,$month_no[$n],$year_val);
							  							 
							 $debit_count=count($debit);
							 $credit_count=count($credit);
							 
							 $debit[0]["cr_amount"];
							 if($debit_count<=1&& $credit_count<=1){							
									$consumerdata.= "<td><a href='#tips' onclick='check(".$debit[0]["transaction_id"].")'> ".$debit[0]["cr_amount"]."
											</a> </td>
							  						 <td> <a href='#tips' onclick='check(".$credit[0]["transaction_id"].")'>".$credit[0]["cr_amount"]."
							  						 	</a>	 </td>";
									$totalDebit=$totalDebit+$debit[0]["cr_amount"];
									$totalCredit=$totalCredit+$credit[0]["cr_amount"];
							 }else{
							 	$max_loop=max($debit_count,$credit_count);
							 	$consumerdata.= "<td >";
							 	for($k=0;$k<$debit_count;$k++){
							 		$consumerdata.= "<a href='#tips' onclick='check(".$debit[$k]["transaction_id"].")'>".$debit[$k]["cr_amount"]." </a>| ";
							 		$totalDebit=$totalDebit+$debit[$k]["cr_amount"];
							 		 
							 	}
							 	$consumerdata.= "</td>
							 			<td>";
							 	for($k=0;$k<$credit_count;$k++){
							 		$consumerdata.= "<a href='#tips' onclick='check(".$credit[$k]["transaction_id"].")'>".$credit[$k]["cr_amount"]." </a>| ";
							 		
							 		$totalCredit=$totalCredit+$credit[$k]["cr_amount"];
							 	}
							 	$consumerdata.="</td>";
							 }							  							if($month_no[$n]==$month){
							  							break;
							  							}
								}
								$year_balance=$totalDebit - $totalCredit;
								$year_balance=$oustanding-$year_balance;
												$consumerdata.= "<td align='center'>". $year_balance."</td>
							  							</tr> ";
							  							}
							  							}
							  							 
				
							  							$consumerdata.=  "</tbody> </table> </div></div></div></div>
							  							<h4><center>Banned Consumers</center></h4>
							  							<div>
			   <div class='panel-body'>
							   <div class='table-scrollable'>
							   <div class='adv-table'>
			  		<table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
			  			<tbody>
			  			<tr>
			  			<th colspan='10' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			
						$consumerdata.= "<tr>
							<th width='50px' colspan='5'></th>";
					
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
											  $year_vals=$year;
											  		if($month >=4)
											  				$year_vals=($month_no[$m] >=4)?$year:$year+1;
								  else {
								  	$year_vals=($month_no[$m] >=4)?$year-1:$year;
							  							}
							  							$consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>";
							  							if($month_no[$m]==$month){
							  							break;
							  							}
							  							}
							  							$consumerdata.= "</tr>";
							  							$consumerdata.= "<tr>
							  		<th width='50px'>S.No</th>
											  		<th width='150px'>Connection ID</th>
											  		<th width='130px'>Consumers Name</th>
											  		<th width='130px'>Mobile No.</th>
							  						<th width='130px'>Outstanding</th>";
											  		for($m=0 ; $m<sizeof($months);$m++)
							  {
							  		$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
							  		if($month_no[$m]==$month){
							  			break;
							  							}
							  							}
							  							$consumerdata.= "
							  							<th  width='80px'>Balance</th>
							  </tr>";
					
							  $consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"));
							  								
							  							if($consumers){
							  $count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
											  foreach ($consumers as $consumer){
											  		$year_balance=0;
											  		$totalDebit=0;$totalCredit=0;
											  		$consumer_id=$consumer->__get("consumer_id");
											  		$oustanding=$cashRegisterMapper->getOutByConsumerId($consumer_id,$month,$year);
											  		$count= $count+1;
											  		$consumerdata.= "  <tr>
											  			<td>".$count."</td>
											  			<td><a href='http://taraurja.in/consumers/view/id/".$consumer->__get('consumer_id')."' target='_blank'>".$consumer->__get("consumer_connection_id")."</a></td>
											  			<td>".$consumer->__get("consumer_name")."</td>
											  			<td>".$consumer->__get("consumer_code")."</td>
											  			<td>".$oustanding."</td>";
											  					for($n=0 ; $n<sizeof($month_no);$n++)
							  		{
											  					$year_val=$year;
											  					if($month >=4)
											  					$year_val=($month_no[$n] >=4)?$year:$year+1;
											  					else {
											  					$year_val=($month_no[$n] >=4)?$year-1:$year;
											  					}
											  					/* $debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$n],$year_val);
											  					$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
											  					$consumerdata.= "<td>".$debit."</td>
											  					<td>".$credit."</td>";
											  					$totalDebit=$totalDebit+$debit;
											  					$totalCredit=$totalCredit+$credit; */
											  					$debit=$cashRegisterMapper->getMonthDebitsByConsumerId($consumer_id,$month_no[$n],$year_val);
							  							   $credit=$cashRegisterMapper->getMonthCreditsByConsumerId($consumer_id,$month_no[$n],$year_val);
							  							 
							 $debit_count=count($debit);
							 $credit_count=count($credit);
							 
							 $debit[0]["cr_amount"];
							 if($debit_count<=1&& $credit_count<=1){							
									$consumerdata.= "<td><a href='#tips' onclick='check(".$debit[0]["transaction_id"].")'> ".$debit[0]["cr_amount"]."
											</a> </td>
							  						 <td> <a href='#tips' onclick='check(".$credit[0]["transaction_id"].")'>".$credit[0]["cr_amount"]."
							  						 	</a>	 </td>";
									$totalDebit=$totalDebit+$debit[0]["cr_amount"];
									$totalCredit=$totalCredit+$credit[0]["cr_amount"];
							 }else{
							 	$max_loop=max($debit_count,$credit_count);
							 	$consumerdata.= "<td >";
							 	for($k=0;$k<$debit_count;$k++){
							 		$consumerdata.= "<a href='#tips' onclick='check(".$debit[$k]["transaction_id"].")'>".$debit[$k]["cr_amount"]." </a>| ";
							 		$totalDebit=$totalDebit+$debit[$k]["cr_amount"];
							 		 
							 	}
							 	$consumerdata.= "</td>
							 			<td>";
							 	for($k=0;$k<$credit_count;$k++){
							 		$consumerdata.= "<a href='#tips' onclick='check(".$credit[$k]["transaction_id"].")'>".$credit[$k]["cr_amount"]." </a>| ";
							 		
							 		$totalCredit=$totalCredit+$credit[$k]["cr_amount"];
							 	}
							 	$consumerdata.="</td>";
							 }											  							if($month_no[$n]==$month){
											  							break;
											  					}
											  					}
											  					$year_balance=$totalDebit - $totalCredit;
											  				    $year_balance=	$oustanding-$year_balance;
											  					$consumerdata.= "<td align='center'>". $year_balance."</td>
											  					</tr> ";
											  					}
											  					}
											  					$consumerdata.=  "</tbody> </table>
											  					</div>
											  							</div>
											  							</div>
											  							</div> ";
											  					
         	 		 $filename="YTD Sheet Report for ".$site->__get("site_name")." ".date('d M Y hi A').'.pdf';
				$files=(string)$filename;
				//	$checkPdf=$this->pdfGenerateAction($consumerdata,$files);
				}
			}
			echo $consumerdata;exit;
											  		$auth=new My_Auth('user');
											  		$email_id=$auth->getIdentity()->user_email;
											  		$this->sendMailStateWiseAction($StateName_Array,$email_id);
											  		break;
			case 'Ytd_Sheet_25_cycle_san':
											  		$date = new Zend_Date();
											  		$date->setTimezone("Asia/Calcutta");
											  		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
											  		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
											  		$day=$trans_date["day"];
											  		$month= $trans_date["month"];
											  		//$month= 8;$year=2016;
											  		$year= $trans_date["year"];
											  		$stateMapper=new Application_Model_StatesMapper();
											  		$clusterMapper=new Application_Model_ClustersMapper();
											  		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
											  		$siteMapper=new Application_Model_SitesMapper();
											  		$consumerMapper=new Application_Model_ConsumersMapper();
											  		$packageMapper=new Application_Model_PackagesMapper();
				
											  		$StateName_Array=array();
											  		$sites=$siteMapper->getAllSites();
											  		$counters=0;
											  		$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
											  		$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
				$consumerdata="<script>
						function check(txnId){
								document.getElementById('txn_id').value=txnId;
						}
						</script>
						<div><h4 id='tips'>Transaction Id:</h4> <input type='text' id='txn_id'></div>";											  		foreach ($sites as $site)
											  			{
											  					
											  							if($site->__get("site_id")!=1){
				
					 $consumerdata.= " <h4><center>File created on ".date('jS F, Y')." at ".date('h:i A')."  <br>
			  		YTD OS 25th Cycle Collection Sheet for ".date('F, Y')."</center></h4>";
							  			
							  		$stateName=$stateMapper->getStateById($site->__get("state_id"));
			  $consumerdata.= "<div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>
	                <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
                        <tbody>
                         <tr>
							  				<th colspan='29' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
				
			  $consumerdata.= "<tr>
                                <th width='50px' colspan='5'></th>";
				
				                                for($m=0 ; $m<sizeof($months);$m++)
				                                {
				
				                                $year_vals=$year;
				                                if($month >=4)
				                                	$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
				                                			$year_vals=($month_no[$m] >=4)?$year-1:$year;
				                                	}
				                                	$consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
				                <th width='50px'>S.No</th>
				                <th width='150px'>Connection ID</th>
				 				<th width='130px'>Consumers Name</th>
				                <th width='130px'>Mobile No.</th>
							    <th width='130px'>Outstanding</th>";
							   
				
				                                			for($m=0 ; $m<sizeof($months);$m++)
							{
					
									$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
													if($month_no[$m]==$month){
				                                			break;
				                                			}
				                                			}
				                                			$consumerdata.= "
				                                			<th  width='80px'>Balance</th>
				                                			</tr>";
				
				                                			$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"),true);
				
				                                					if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
							  					foreach ($consumers as $consumer){
							  					$year_balance=0;
							  							$totalDebit=0;$totalCredit=0;
							  							$consumer_id=$consumer->__get("consumer_id");
							  							$oustanding=$cashRegisterMapper->getOutByConsumerId($consumer_id,$month,$year,true);
							  							$count= $count+1;
							  							$consumerdata.= "  <tr>
							  							<td>".$count."</td>
							  							<td><a href='http://taraurja.in/consumers/view/id/".$consumer->__get('consumer_id')."' target='_blank'>".$consumer->__get("consumer_connection_id")."</a></td>
											  				
							  							<td>".$consumer->__get("consumer_name")."</td>
							  							<td>".$consumer->__get("consumer_code")."</td>
							  							<td>".$oustanding."</td>";
							  							
							  							for($n=0 ; $n<sizeof($month_no);$n++)
							  							{
				
							  							$year_val=$year;
							  							if($month >=4)
							  									$year_val=($month_no[$n] >=4)?$year:$year+1;
							  							else {
							  							$year_val=($month_no[$n] >=4)?$year-1:$year;
							  							}
							  							$debit=$cashRegisterMapper->getMonthlyDebitsByConsumerId($consumer_id,$month_no[$n],$year_val);
							  							$credit=$cashRegisterMapper->getMonthlyCreditsByConsumerId($consumer_id,$month_no[$n],$year_val);
							 $debit_count=count($debit);
							 $credit_count=count($credit);
							 
							 $debit[0]["cr_amount"];
							 if($debit_count<=1&& $credit_count<=1){							
									$consumerdata.= "<td><a href='#tips' onclick='check(".$debit[0]["transaction_id"].")'> ".$debit[0]["cr_amount"]."
											</a> </td>
							  						 <td> <a href='#tips' onclick='check(".$credit[0]["transaction_id"].")'>".$credit[0]["cr_amount"]."
							  						 	</a>	 </td>";
									$totalDebit=$totalDebit+$debit[0]["cr_amount"];
									$totalCredit=$totalCredit+$credit[0]["cr_amount"];
							 }else{
							 	$max_loop=max($debit_count,$credit_count);
							 	$consumerdata.= "<td >";
							 	for($k=0;$k<$debit_count;$k++){
							 		$consumerdata.= "<a href='#tips' onclick='check(".$debit[$k]["transaction_id"].")'>".$debit[$k]["cr_amount"]." </a>| ";
							 		$totalDebit=$totalDebit+$debit[$k]["cr_amount"];
							 		 
							 	}
							 	$consumerdata.= "</td>
							 			<td>";
							 	for($k=0;$k<$credit_count;$k++){
							 		$consumerdata.= "<a href='#tips' onclick='check(".$credit[$k]["transaction_id"].")'>".$credit[$k]["cr_amount"]." </a>| ";
							 		
							 		$totalCredit=$totalCredit+$credit[$k]["cr_amount"];
							 	}
							 	$consumerdata.="</td>";
							 }
									if($month_no[$n]==$month){
										break;
									}
								}
									
								$year_balance=$totalDebit - $totalCredit;
								$year_balance=$oustanding - $year_balance;
			                     $consumerdata.= "<td align='center'>".$year_balance."</td>
				                </tr> ";
			  				}
				  	 }
				  	 
				
				  
				  	 
			  $consumerdata.=  "</tbody> </table> </div></div></div></div>
							  <h4><center>Banned Consumers</center></h4>
							  							<div>
							  							<div class='panel-body'>
            	<div class='table-scrollable'>
							  							<div class='adv-table'>
							  							<table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
							  							<tbody>
							  							<tr>
							  							<th colspan='29' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			
							  									$consumerdata.= "<tr>
							  											<th width='50px' colspan='4'>S.No</th>";
							  												
							  											for($m=0 ; $m<sizeof($months);$m++)
							  							{
											  	  $year_vals=$year;
								  if($month >=4)
								  	$year_vals=($month_no[$m] >=4)?$year:$year+1;
								  else {
								  	$year_vals=($month_no[$m] >=4)?$year-1:$year;
											  		}
											  		$consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>";
								  if($month_no[$m]==$month){
											  				break;
												  }
												  }
												  	$consumerdata.= "</tr>";
												  	$consumerdata.= "<tr>
							  		<th width='50px'>S.No</th>
							  		<th width='150px'>Connection ID</th>
											  		<th width='130px'>Consumers Name</th>
											  		<th width='130px'>Mobile No.</th>
											  		 <th width='130px'>Outstanding</th>";
											  			
											  		for($m=0 ; $m<sizeof($months);$m++)
											  		{
											  		$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
											  			if($month_no[$m]==$month){
											  			break;
											  		}
											  		}
											  		$consumerdata.= "
											  		<th  width='80px'>Balance</th>
											  		</tr>";
					
											  				$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"));
					
											  						if($consumers){
											  								$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
											  								foreach ($consumers as $consumer){
											  								$year_balance=0;
											  								$totalDebit=0;$totalCredit=0;
											  								
											  								$consumer_id=$consumer->__get("consumer_id");
											  								$oustanding=$cashRegisterMapper->getOutByConsumerId($consumer_id,$month,$year,true);
											  								
											  								$count= $count+1;
											  								$consumerdata.= "  <tr>
											  								<td>".$count."</td>
											  								<td><a href='http://taraurja.in/consumers/view/id/".$consumer->__get('consumer_id')."' target='_blank'>".$consumer->__get("consumer_connection_id")."</a></td>
											  								<td>".$consumer->__get("consumer_name")."</td>
											  								<td>".$consumer->__get("consumer_code")."</td>
											  								<td>".$oustanding."</td>";
											  										for($n=0 ; $n<sizeof($month_no);$n++)
											  						{
											  							$year_val=$year;
											  							if($month >=4)
											  								$year_val=($month_no[$n] >=4)?$year:$year+1;
											  							else {
											  							$year_val=($month_no[$n] >=4)?$year-1:$year;
											  								}
							/*$debit=$cashRegisterMapper->getMonthlyDebitByConsumerId($consumer_id,$month_no[$n],$year_val);
							$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
							$consumerdata.= "<td>".$debit."</td>
								<td>".$credit."</td>";*/
							$debit=$cashRegisterMapper->getMonthlyDebitsByConsumerId($consumer_id,$month_no[$n],$year_val);
							  							$credit=$cashRegisterMapper->getMonthlyCreditsByConsumerId($consumer_id,$month_no[$n],$year_val);
							 $debit_count=count($debit);
							 $credit_count=count($credit);
							 
							 $debit[0]["cr_amount"];
							 if($debit_count<=1&& $credit_count<=1){							
									$consumerdata.= "<td><a href='#tips' onclick='check(".$debit[0]["transaction_id"].")'> ".$debit[0]["cr_amount"]."
											</a> </td>
							  						 <td> <a href='#tips' onclick='check(".$credit[0]["transaction_id"].")'>".$credit[0]["cr_amount"]."
							  						 	</a>	 </td>";
									$totalDebit=$totalDebit+$debit[0]["cr_amount"];
									$totalCredit=$totalCredit+$credit[0]["cr_amount"];
							 }else{
							 	$max_loop=max($debit_count,$credit_count);
							 	$consumerdata.= "<td >";
							 	for($k=0;$k<$debit_count;$k++){
							 		$consumerdata.= "<a href='#tips' onclick='check(".$debit[$k]["transaction_id"].")'>".$debit[$k]["cr_amount"]." </a>| ";
							 		$totalDebit=$totalDebit+$debit[$k]["cr_amount"];
							 		 
							 	}
							 	$consumerdata.= "</td>
							 			<td>";
							 	for($k=0;$k<$credit_count;$k++){
							 		$consumerdata.= "<a href='#tips' onclick='check(".$credit[$k]["transaction_id"].")'>".$credit[$k]["cr_amount"]." </a>| ";
							 		
							 		$totalCredit=$totalCredit+$credit[$k]["cr_amount"];
							 	}
							 	$consumerdata.="</td>";
							 }							 if($month_no[$n]==$month){
											  											break;
											  		}
											  		}
											  		$year_balance=$totalDebit - $totalCredit;
											  		$year_balance=$oustanding - $year_balance;
											  		$consumerdata.= "<td align='center'>". $year_balance."</td>
											  		</tr> ";
							  							}
							  							}
									$consumerdata.=  "</tbody> </table>
								                		</div>
								           			</div>
										   		</div>
								         	</div> ";
							 
				         	 		 $filename="YTD OS 25th Cycle for ".$site->__get("site_name")." ".date('d M Y hi A').'.pdf';
				         	 		 $files=(string)$filename;
				         	 		 //$checkPdf=$this->pdfGenerateAction($consumerdata,$files);
				         	 		// array_push($StateName_Array, $files);
				         	 		
								}
							  }
							  echo  $consumerdata;exit;

							$auth=new My_Auth('user');
						    $email_id= $auth->getIdentity()->user_email;
							$this->sendMailStateWiseAction($StateName_Array,$email_id,true);
								break;
			
			case "Rate_avg_consumer" :
										
									$year = $request->getParam("year");
									$month_val = $request->getParam("month");
									$month = date("m", strtotime($month_val));
									$file_type = $request->getParam("file_type");
									
									$stateMapper=new Application_Model_StatesMapper();
									$cashRegisterMapper = new Application_Model_CashRegisterMapper();
									$packageMapper=new Application_Model_PackagesMapper();
									$siteMapper=new Application_Model_SitesMapper();
									$consumerMapper=new Application_Model_ConsumersMapper();
									$meterReadingMapper=new Application_Model_MeterReadingsMapper();
									$consumerPackages=new Application_Model_ConsumersPackageMapper();
									$wattageMapper=new Application_Model_WattageMapper();
									$feedarMapper=new Application_Model_SiteMasterMeterMapper();
									$consumerdata="";
									$consumerdata.= " <h4 align='center'>Rate Avg Consumer Report for ".$month_val."-".$year."</h4>";
									$consumerdata.= "<div>
									<table border=1 align='center'><tbody>";
										
									$consumerdata.= "<tr>
												<th>State Name</th>
												<th>Site Name</th>
												<th>Prepaid Customer</th>
												<th>Avg Rate</th>
												<th>Metered Customer</th>
												<th>Avg Rate</th>
												<th>Total Customer</th>
												<th>Avg Rate</th>";
								
									$consumerdata.="</tr>";
									$roleSession = new Zend_Session_Namespace('roles');
									$role_sites_id=$roleSession->site_id;
									$sites=$siteMapper->getAllSites(null,null,$role_sites_id);
									$sort_arr = array();
									foreach ($sites as $key => $row)
									{
										$sort_arr[$key] = $row->__get('state_id');
									}
									array_multisort($sort_arr, SORT_DESC ,$sites);
										
									if(sites){
										$total_con=0;$total_Pre_con=0;$total_Mtr_con=0;$total_avg=0;$total_Pre_avg=0;$total_Mtr_avg=0;
										foreach ($sites as $site){
										if($site->__get("site_id")!=1){
											$consumers=$consumerMapper->getSiteWiseConsumerTypeDetails($site->__get("site_id"),$month,$year);
								
											$totalMtrUnit=0;
											$consumer_pre=$consumerMapper->getConsumerByTypeDetails($site->__get("site_id"),'1,2',$month,$year);
											if($consumer_pre){
												foreach ($consumer_pre as $consumer_pres){
													$meterReading=$meterReadingMapper->getTotalUnitSiteWise($consumer_pres["consumer_id"],$month,$year);
													if($meterReading){
														 $result=floatval($meterReading["curr_reading"])-floatval($meterReading["last_reading"]);
														 if($result<0){
															$result=0;
														 }
														$totalMtrUnit=$totalMtrUnit+$result;
													}
												}
											}
												 
											$totalFixedUnit=0;
											$consumer_pre=$consumerMapper->getConsumerByTypeDetails($site->__get("site_id"),'0',$month,$year);
											if($consumer_pre){
												foreach ($consumer_pre as $consumer_pres){
													$conpackages=$consumerPackages->getPackageByConsumerId($consumer_pres["consumer_id"]);
													if($conpackages){
														$wattage_value=0;
														foreach ($conpackages as $conpackage){
															$wattage=$wattageMapper->getWattageBywattId($conpackage["wattage"]);
															if($wattage){
																$wattage_value=$wattage_value+$wattage->__get("wattage");
															}
														}
													}
								
													$feedars=$feedarMapper->getMMById($consumer_pres["site_meter_id"]);
													if($feedars){
														if($feedars->__get("is_24_hr")==1) {
															$hours= 24;
														}elseif($feedars->__get("start_time") >$feedars->__get("end_time")){
															$hours=24+($feedars->__get("end_time") - $feedars->__get("start_time"));
														}else{
															$hours=$feedars->__get("end_time") - $feedars->__get("start_time");
														}
													}
								
													$totalFixedUnit=$totalFixedUnit+(($wattage_value*$hours*cal_days_in_month(CAL_GREGORIAN, $month, $year))/1000);
								
												}
											}
								
											$states=$stateMapper->getStateById($site->__get("state_id"));
											$totalCon = $consumers['fixed']+$consumers['metered'];
											$totalAvg = $totalFixedUnit+$totalMtrUnit;
											if($consumers){
								
												$totalCon=$consumers['fixed']+$consumers['metered'];
												$total_con=$total_con+$totalCon;
												$total_Pre_con=$total_Pre_con+$consumers['fixed'];
												$total_Mtr_con=$total_Mtr_con+$consumers['metered'];
												$total_Pre_avg=$total_Pre_avg+$totalFixedUnit;
												$total_Mtr_avg=$total_Mtr_avg+$totalMtrUnit;
												$total_avg=$total_avg+$totalAvg;
												$consumerdata.= "<tr>
																	<td>".$states->__get("state_name")."</td>
																	<td>".$site->__get("site_name")."</td>
																	<td>".$consumers['fixed']."</td>
																	<td>".$totalFixedUnit."</td>
																	<td>".$consumers['metered']."</td>
																	<td>".$totalMtrUnit."</td>
																	<td>".$totalCon."</td>
																	<td>".$totalAvg."</td>";
								
												$consumerdata.= "</tr>";
											}
										}
										}
										$consumerdata.= "<tr>
																	<th></th>
																	<th>Grand Total</th>
																	<td>".$total_Pre_con."</td>
																	<td>".$total_Pre_avg."</td>
																	<td>".$total_Mtr_con."</td>
																	<td>".$total_Mtr_avg."</td>
																	<td>".$total_con."</td>
																	<td>".$total_avg."</td>";
									}
										
									$consumerdata.=  "</tbody> </table>
						</div>";
							  
								 
									if($file_type=='PDF'){
										$filename='Rate Avg Consumer Report for '.$month_val."-".$year.'.pdf';
										$files=(string)$filename;
										$checkPdf=$this->pdfGeneratesAction(null,$consumerdata,$files);
									}else{
										$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
										$cacheSettings = array( 'memoryCacheSize' => '8192MB');
										PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
										
										$objPHPExcel=null;
									  
										$filename = 'Rate Avg Consumer Report for '.$month_val."-".$year.'.xlsx'; 
										$files=(string)$filename;
										$inputFileType = 'Excel2007';
										
										$tmpfile = tempnam("/html/excelBackup/", 'html');
										
										file_put_contents($tmpfile, $consumerdata);
										
										$objPHPExcel = new PHPExcel();
										$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
										$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
										$objPHPExcel->getActiveSheet()->setTitle('Collection Sheet');
										
										$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
										$objWriter->save($files);
										$objPHPExcel->disconnectWorksheets();
										unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings);
										gc_enable() ;gc_collect_cycles();
									}
									  
										
									$auth=new My_Auth('user');
									$email_id=$auth->getIdentity()->user_email;
									//$this->sendMailForSingleFileAction($files,$email_id);
									echo "http://taraurja.in/".$files;
									break;
									
			case 'spi_monthly_report':
											
									 	$month_val = $request->getParam("month");
										$month = date("m", strtotime($month_val));
										$year= $request->getParam("year");
									  	
										$stateMapper=new Application_Model_StatesMapper();
										$clusterMapper=new Application_Model_ClustersMapper();
										$cashRegisterMapper = new Application_Model_CashRegisterMapper();
										$siteMapper=new Application_Model_SitesMapper();
										$consumerMapper=new Application_Model_ConsumersMapper();
										$packageMapper=new Application_Model_PackagesMapper();
										$MMmapper=new Application_Model_SiteMasterMeterMapper();
										$meterReadingMapper=new Application_Model_MeterReadingsMapper();
									 
										$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
									
										$StateName_Array=array();
										$roleSession = new Zend_Session_Namespace('roles');
										$role_sites_id=$roleSession->site_id;
										
										$states=$stateMapper->getAllStates();
										$consumers_type=$consumerMapper->getConsumerType();
									 if($states){
											foreach ($states as $state){
												$consumerdata="";
												$consumerdata.= " <h4>SPI Monthly Report for ".$month_val.", ".$year."</h4>";
												$consumerdata.= " <h4>".$state->__get("state_name")."</h4>";
											 $sites=$siteMapper->getSitesForStateId($state->__get("state_id"),$role_sites_id);
												 if($sites){
													$consumerdata.= " <table border=1>";
												                       		
													foreach ($sites as $site)
													{
														$consumerdata.= "<tr></tr><tr> <th></th> <th>".$site["site_name"]."</th></tr><tr></tr>";
														$consumerdata.= "<tr><th></th><th></th><th></th><th></th><th></th><th>Billing</th><th></th><th></th><th>Collection</th><th></th><th></th></tr>";
														$consumerdata.= "<tr><td>Categories</td>
																			 <td>Total Consumers</td>
																 			 <td>New Consumers</td>
																			 <td>Tariff</td>
																			 <td>Billing</td>
																			 <td>EED/MED</td>
																			 <td>Scheme (Energy)</td> 
																			 <td>Collection</td>
																			 <td>EED/MED</td>
																			 <td>Scheme (Discount)</td>
																	 		 <td>Daily Unit Sold</td></tr>";
														if($consumers_type){
															$total_consumer=0;$total_tariff=0;$total_billed=0;$total_collected=0;$total_daily_unit=0;
															$total_billed_eed=0;$total_bill_scheme=0; 
															$total_coll_eed=0;$total_coll_dis=0; 
															foreach ($consumers_type as $consumers_types){
															if($consumers_types["consumer_type_id"]!=11 && $consumers_types["consumer_type_id"]!=12){
																$consumerdata.= "<tr><td>".$consumers_types["consumer_type_name"]."</td>";
																$consumer_totals=$consumerMapper->getConsumerTotalConnection($consumers_types["consumer_type_id"],$month,$year,$site["site_id"]);
																if($consumer_totals){
																	
																	$total_consumer_data=($consumer_totals["total_consumer"]!=0)?$consumer_totals["total_consumer"]:NULL;
																	$new_consumer_data=($consumer_totals["new_consumer"]!=0)?$consumer_totals["new_consumer"]:NULL;
																	
																	$consumerdata.= "<td>".$total_consumer_data."</td>
																					 <td>".$new_consumer_data."</td>";
																}else{
																	$consumerdata.= "<td>".null."</td><td>".null."</td>";
																}
																$total_con=$consumer_totals["new_consumer"]+$consumer_totals["total_consumer"];
																$total_consumer=$total_consumer+$total_con;
																
																$billing_collected=$cashRegisterMapper->getTotalTariffAmount($consumers_types["consumer_type_id"],$month,$year,$site["site_id"]);
																if($billing_collected){
																	if($total_con!=0){
																		$tariff= sprintf("%.2f", $billing_collected["billed"]/$total_con);
																	}else{
																		$tariff=0;
																	}
																	
																	$daily_unit_total=0;
																	$consumer_details=$consumerMapper->getConsumerWattageByType($consumers_types["consumer_type_id"],$month,$year,$site["site_id"]);
																	if($consumer_details){
																		foreach ($consumer_details as $consumer_detail){
																			 
																			if($consumer_detail["is_postpaid"]==0){
																				$feedarHrs=0;
																				$site_meter_ids=explode(",", $consumer_detail['site_meter_id']);
																				$countFeedar=0;
																				if(($site_meter_ids)>0){
																					for ($k=0;$k<count($site_meter_ids);$k++){
																						$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
																						if($feedarDetail){
																							$countFeedar=$countFeedar+1;
																							if($feedarDetail->__get("is_24_hr")==1){
																								$feedarHr=24;
																							}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
																								$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
																							}else{
																								$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
																							}
																							$feedarHrs=$feedarHrs+$feedarHr;
																						}
																					}
																					if($countFeedar>1){
																						$feedarHrs=10;
																					}
																				}
																				$dailyUnit=sprintf("%.2f",($consumer_detail["wattage"]*$feedarHrs)/1000);
																				 
																			}else{
																	
																				$meterReading=$meterReadingMapper->getTotalUnitSiteWise($consumer_detail["consumer_id"],$month,$year);
																				if($meterReading){
																					if($meterReading["curr_reading"]!=NULL){
																						$totalMtrUnit=(floatval($meterReading["curr_reading"])-floatval($meterReading["last_reading"]));
																						$dailyUnit=sprintf("%.2f",$totalMtrUnit/$total_days_in_month);
																					}else{
																						$dailyUnit=0;
																					}
																					
																				}else{
																					$totalMtrUnit=0;
																					$dailyUnit=0;
																				}
																				 
																			}
																			$daily_unit_total=$daily_unit_total+ $dailyUnit;
																		}
																	}
																	$schemeConsumer=$cashRegisterMapper->getSchemeConsumers($month,$year,$site["site_id"],$consumers_types["consumer_type_id"]);
																	
																	if($schemeConsumer){
																		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
																		foreach ($schemeConsumer as $schemeConsumers){
																				$detailsPackage=$consumerSchemeMapper->getschemePackageByConsumerID($schemeConsumers["consumer_id"]);
																				if($detailsPackage){
																					foreach ($detailsPackage as $detailsPackages){
																						$dailyUnit=sprintf("%.2f",($detailsPackages["wattage"]*$detailsPackages["feeder_hours"])/1000);
																						$daily_unit_total=$daily_unit_total+ $dailyUnit;
																					}
																				}
																		}
																		
																	}
																	$tariff_data=($tariff!=0)?$tariff:NULL;
																	$billing_data=($billing_collected["billed"]!=0)?$billing_collected["billed"]:NULL;
																	$bill_eed=($billing_collected["bill_eed"]!=0)?$billing_collected["bill_eed"]:NULL;
																	$bill_scheme=($billing_collected["bill_scheme"]!=0)?$billing_collected["bill_scheme"]:NULL;
																	$collected_data=($billing_collected["collected"]!=0)?$billing_collected["collected"]:NULL;
																	$coll_eed=($billing_collected["coll_eed"]!=0)?$billing_collected["coll_eed"]:NULL;
																	$coll_dis=($billing_collected["coll_dis"]!=0)?$billing_collected["coll_dis"]:NULL;
																	
																	$consumerdata.= "<td>".floatval($tariff_data)."</td>
																					 <td>".floatval($billing_data)."</td>
																					  <td>".floatval($bill_eed)."</td>
																					   <td>".floatval($bill_scheme)."</td>
																					 <td>".floatval($collected_data)."</td>
																					  <td>".floatval($coll_eed)."</td>
																					   <td>".floatval($coll_dis)."</td>
																					 <td>".sprintf("%.2f", $daily_unit_total)."</td>";
																	
																	$total_tariff=$total_tariff+$tariff;
																	$total_billed=$total_billed+$billing_collected["billed"];
																	$total_collected=$total_collected+$billing_collected["collected"];
																	$total_daily_unit=$total_daily_unit+$daily_unit_total;
																	$total_billed_eed=$total_billed_eed+$billing_collected["bill_eed"];
																	$total_bill_scheme=$total_bill_scheme+$billing_collected["bill_scheme"]; 
																	$total_coll_eed=$total_coll_eed+$billing_collected["coll_eed"];
																	$total_coll_dis=$total_coll_dis+$billing_collected["coll_dis"]; 
																}else{
																	$consumerdata.= "<td>".null."</td><td>".null."</td><td>".null."</td>";
																}
																
																$consumerdata.= "</tr>";
															}
															}
															$total_tarrif_val=sprintf("%.2f", floatval($total_billed/$total_consumer));
															$consumerdata.= "<tr><td>Grand Total</td>
																			 <td>".$total_consumer."</td>
																 			 <td>".NULL."</td>
																			 <td>".$total_tarrif_val."</td> 
																			 <td>".floatval($total_billed)."</td>
																			 <td>".floatval($total_billed_eed)."</td>
																			 <td>".floatval($total_bill_scheme)."</td>
																			 <td>".floatval($total_collected)."</td>
																			 <td>".floatval($total_coll_eed)."</td>
																			 <td>".floatval($total_coll_dis)."</td>
																	 		 <td>".sprintf("%.2f", $total_daily_unit)."</td></tr>";
														}
													}
													$consumerdata.= "<tr> </tr>";
													$consumerdata.= " </table>";
												 }
												 
												 $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
												 $cacheSettings = array( 'memoryCacheSize' => '8192MB');
												 PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
												 	
												 $objPHPExcel=null;
												 	
												 $filename = "SPI Monthly Report for ".$state->__get("state_name")." ".$month_val.", ".$year.'.xlsx';
												 $files=(string)$filename;
												 $inputFileType = 'Excel2007';
													  																								 
												 $tmpfile = tempnam("/html/excelBackup/", 'html');
												        
												 file_put_contents($tmpfile, $consumerdata);
												        
												 $objPHPExcel = new PHPExcel();
												 $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
												 $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
												 $objPHPExcel->getActiveSheet()->setTitle("SPI Report ".$state->__get("state_name")."");
												        
											   	 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
												 $objWriter->save($files);
												 $objPHPExcel->disconnectWorksheets();
												 unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings);
												 gc_enable() ;gc_collect_cycles();
												 array_push($StateName_Array, $files);
											}
										} 
										
			 $consumers_type=$consumerMapper->getConsumerType();
			 if($states){
			 	$consumerdata="<table border=1 align='center'>";
			 	
					$consumerdata.="<tr><td>State</td>
							<td>Sites</td>
							<td>type of me</td>
							<td>Week End date</td>
							<td>07-".$month."-".$year."</td>
							<td>14-".$month."-".$year."</td>
							<td>21-".$month."-".$year."</td>
							<td>28-".$month."-".$year."</td>
							<td></td>
							<td>07-".$month."-".$year."</td>
							<td>14-".$month."-".$year."</td>
							<td>21-".$month."-".$year."</td>
							<td>28-".$month."-".$year."</td>
						 </tr>";
					 
					$consumerdata.="<tr><td>State</td>
							<td></td><td>Segment</td>
							<td>Live customers</td>
							<td></td>
							<td>Customers Added</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Customers Disconnected</td>
							<td></td>
							<td></td>
						 </tr>";
					$new_01_total=0;$new_07_total=0;$new_14_total=0;$new_21_total=0;$new_28_total=0;
					$banned_07_total=0;$banned_14_total=0;$banned_21_total=0;$banned_28_total=0;
					foreach ($states as $state){
						
					$consumerdata.= "<tr><td>".$state->__get("state_name")."</td>";
					$state_count=0;
					$sites=$siteMapper->getSitesForStateId($state->__get("state_id"),$role_sites_id);
					if($sites){
						foreach ($sites as $site){
							$site_count=0;
							if($state_count!=0){
								$consumerdata.= "<tr><td>".NULL."</td>";
							}
							$state_count=$state_count+1;
							$consumerdata.= "<td>".$site["site_name"]."</td>";
							if($consumers_type){
								foreach ($consumers_type as $consumers_types){
								if($consumers_types["consumer_type_id"]!=11 && $consumers_types["consumer_type_id"]!=12){
									$consumer_totals=$consumerMapper->getConsumerTotalConnection($consumers_types["consumer_type_id"],$month,$year,$site["site_id"]);
									if($consumer_totals){
										if($site_count!=0){
											$consumerdata.= "<tr><td>".NULL."</td><td>".NULL."</td>";
										}
										$site_count=$site_count+1;
										$new_consumer_01=($consumer_totals["total_consumer_all"]!=0)?$consumer_totals["total_consumer_all"]:NULL;
										$new_consumer_07=($consumer_totals["new_consumer_07"]!=0)?$consumer_totals["new_consumer_07"]:NULL;
										$new_consumer_14=($consumer_totals["new_consumer_14"]!=0)?$consumer_totals["new_consumer_14"]:NULL;
										$new_consumer_21=($consumer_totals["new_consumer_21"]!=0)?$consumer_totals["new_consumer_21"]:NULL;
										$new_consumer_28=($consumer_totals["new_consumer_28"]!=0)?$consumer_totals["new_consumer_28"]:NULL;
										
										$banned_consumer_07=($consumer_totals["banned_consumer_07"]!=0)?$consumer_totals["banned_consumer_07"]:NULL;
										$banned_consumer_14=($consumer_totals["banned_consumer_14"]!=0)?$consumer_totals["banned_consumer_14"]:NULL;
										$banned_consumer_21=($consumer_totals["banned_consumer_21"]!=0)?$consumer_totals["banned_consumer_21"]:NULL;
										$banned_consumer_28=($consumer_totals["banned_consumer_28"]!=0)?$consumer_totals["banned_consumer_28"]:NULL;
										
										$new_01_total=$new_01_total+$new_consumer_01;
										$new_07_total=$new_07_total+$new_consumer_07;
										$new_14_total=$new_14_total+$new_consumer_14;
										$new_21_total=$new_21_total+$new_consumer_21;
										$new_28_total=$new_28_total+$new_consumer_28;
										
										$banned_07_total=$banned_07_total+$banned_consumer_07;
										$banned_14_total=$banned_14_total+$banned_consumer_14;
										$banned_21_total=$banned_21_total+$banned_consumer_21;
										$banned_28_total=$banned_28_total+$banned_consumer_28;
										
										$consumerdata.= "<td>".$consumers_types["consumer_type_name"]."</td>
														 <td>".$new_consumer_01."</td>
														 <td>".$new_consumer_07."</td>
														 <td>".$new_consumer_14."</td>
														 <td>".$new_consumer_21."</td>
														 <td>".$new_consumer_28."</td>
														 <td> </td>
														 <td>".$banned_consumer_07."</td>
														 <td>".$banned_consumer_14."</td>
														 <td>".$banned_consumer_21."</td>
														 <td>".$banned_consumer_28."</td></tr>";
										
										
									}
								}
								}
							}					
						}
					}
				}
				$consumerdata.= "<tr><td> </td>
								 <td>Grand Total</td>
								 <td></td>
								 <td>".$new_01_total."</td>
								 <td>".$new_07_total."</td>
								 <td>".$new_14_total."</td>
								 <td>".$new_21_total."</td>
								 <td>".$new_28_total."</td>
								 <td> </td>
								 <td>".$banned_07_total."</td>
								 <td>".$banned_14_total."</td>
								 <td>".$banned_21_total."</td>
								 <td>".$banned_28_total."</td></tr>";
				$consumerdata.= "</table>";
				//echo $consumerdata;exit;
				$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
				$cacheSettings = array( 'memoryCacheSize' => '8192MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
				
				$objPHPExcel=null;
				
				$filename = "Weekly Enrollment for ".$month_val.", ".$year.'.xlsx';
				$files=(string)$filename;
				$inputFileType = 'Excel2007';
				
				$tmpfile = tempnam("/html/excelBackup/", 'html');
				
				file_put_contents($tmpfile, $consumerdata);
				
				$objPHPExcel = new PHPExcel();
				$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
				$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
				$objPHPExcel->getActiveSheet()->setTitle("Weekly Enrollment");
				
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save($files);
				$objPHPExcel->disconnectWorksheets();
				unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings);
				gc_enable() ;gc_collect_cycles();
				array_push($StateName_Array, $files);
			} 
			
			$zipname = 'SPI Monthly Report '.$month_val.', '.$year. '.zip';
			$zip = new ZipArchive();
			defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
			if(file_exists(PUBLIC_PATH . "/html/".$zipname)) {
				unlink (PUBLIC_PATH . "/html/".$zipname);
			}
			if ($zip->open(PUBLIC_PATH . "/html/".$zipname, ZIPARCHIVE::CREATE) != TRUE) {
				die ("Could not open archive");
			}
			
			foreach ($StateName_Array as $file) {
				$zip->addFile($file);
			}
				
			$zip->close();
				
			$arr = array(
					"data" => "http://taraurja.in/".$zipname
			);
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;exit;
								/* $auth=new My_Auth('user');
								$email_id=$auth->getIdentity()->user_email;
								$this->sendMailStateWiseAction($StateName_Array,$email_id,'ytd'); */
							break;
							
			case 'sales_data':
								
							$month_val = $request->getParam("month");
							$month = date("m", strtotime($month_val));
							$year= $request->getParam("year");
						
							$stateMapper=new Application_Model_StatesMapper();
							$clusterMapper=new Application_Model_ClustersMapper();
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$siteMapper=new Application_Model_SitesMapper();
							$consumerMapper=new Application_Model_ConsumersMapper();
							$packageMapper=new Application_Model_PackagesMapper();
							$MMmapper=new Application_Model_SiteMasterMeterMapper();
							$meterReadingMapper=new Application_Model_MeterReadingsMapper();
							$wattageMapper=new Application_Model_WattageMapper();
							$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
							
							date_default_timezone_set('Asia/Kolkata');
							$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
							$date_val=	$year."-".$month."-".$total_days_in_month;
							$StateName_Array=array();
							$roleSession = new Zend_Session_Namespace('roles');
							$role_sites_id=$roleSession->site_id;
						
							$states=$stateMapper->getAllStates();
							$consumers_type=$consumerMapper->getConsumerType();
						    $date_today=$total_days_in_month."-".$month."-".$year;
						   	if($states){
								$consumerdata="<table border=1 align='center'>";
									
								$consumerdata.="<tr> 
												<td>Plant</td>
												<td>Date</td>
												<td>Customer Segment</td>
												<td>No of prepaid customer</td>
												<td>Package Tariff (INR) for prepaid</td>
												<td>Packaged Units Sold (kWh)- Prepaid</td>
												<td>Amount Billed (INR) for Prepaid	</td>
												<td>No of postpaid customer</td>
												<td>Unit Tariff (INR) - Post Paid</td>
												<td>Metered Units Sold (kWh) - Post Paid</td>
												<td>Amount Billed (INR) - Post Paid</td>
												<td>Connection Charges (INR) Billed</td>
												<td>Other Charges (INR) Billed</td>
												<td>Amount Collected (INR)</td>
												<td>Arrears Collected (INR)</td>
											 </tr>";
						
								
								foreach ($states as $state){
						 			$sites=$siteMapper->getSitesForStateId($state->__get("state_id"),$role_sites_id);
									if($sites){
										foreach ($sites as $site){
											if($consumers_type){
												foreach ($consumers_type as $consumers_types){
												if($consumers_types["consumer_type_id"]!=11 && $consumers_types["consumer_type_id"]!=12){
													$month_unit_pre_total=0;$month_unit_post_total=0;$unitTariffPostpaid=0;
													$cashRegister=$cashRegisterMapper->getTotalBilledAmountBySegment($consumers_types["consumer_type_id"],$month,$year,$site["site_id"]);
													$OTPAndOthers=$cashRegisterMapper->getTotalOTPAndOthersBySegment($consumers_types["consumer_type_id"],$month,$year,$site["site_id"]);
													
													$consumer_total_pre=$consumerMapper->getTotalConsumerBySegmentMonth($consumers_types["consumer_type_id"],$month,$year,$site["site_id"],true);
													$package_tariff_pre=NULL;
													 $totalPrePaid=array();
													if($consumer_total_pre){
														
														$consumer_count=array();
														foreach ($consumer_total_pre as $consumer_detail){
															//echo " c".$consumer_detail['consumer_id']." t".$consumer_detail['type']. " ";
															if($consumer_detail['type']==0){
																$consumer_count[]=$consumer_detail['consumer_id'];
															 }  
																 	$feedarHrs=0;
																	$site_meter_ids=explode(",", $consumer_detail['site_meter_id']);
																	if(count($site_meter_ids)>0){
																		for ($k=0;$k<count($site_meter_ids);$k++){
																			$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
																			if($feedarDetail){
																				if($feedarDetail->__get("is_24_hr")==1){
																					$feedarHr=24;
																				}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
																					$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
																				}else{
																					$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
																				}
																				$feedarHrs=$feedarHrs+$feedarHr;
																			}
																		}
																	}
																	$watt_total=$wattageMapper->getWattageBywattId($consumer_detail["wattage"]);
																	if($watt_total){
																		$wattage=$watt_total->__get("wattage");
																	}else{
																		$wattage=0;
																	}
																	
																	$MonthlyUnit_pre=($wattage*$feedarHrs*$total_days_in_month)/1000;
																 
																$month_unit_pre_total=$month_unit_pre_total+ $MonthlyUnit_pre;
																
																$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer_detail['consumer_id']);
									
																if($consumer_scheme){
																	foreach($consumer_scheme as $consumer_schemes){
																		$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
																		$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
																		
																		$discount_month=$consumer_schemes["discount_month"];
																		$feeder_hours=$consumer_schemes["feeder_hours"];
																		$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
																		$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month")); 
																		
																		if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
																			$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]*$feeder_hours*$total_days_in_month)/1000);
																			$month_unit_pre_total=$month_unit_pre_total + $dailyUnit;
																		}
																	}
																}
															} 
														 $totalPrePaid  = array_keys(array_flip($consumer_count));
														// print_r($consumer_count);exit;
														$unique_con_total_pre = array_keys(array_flip($consumer_count));
														if(count($consumer_total_pre)>0){
															$package_tariff_pre=$cashRegister["billed_pre"]/count($unique_con_total_pre);
														}
													}
													
													$consumer_total_post=$consumerMapper->getTotalConsumerBySegmentMonth($consumers_types["consumer_type_id"],$month,$year,$site["site_id"],NULL);
													//print_r($consumer_total_post);  
													$totalPostPaid=array();
													if($consumer_total_post){ 
														foreach ($consumer_total_post as $consumer_detail){
															$totalPostPaid[]=$consumer_detail["consumer_id"];
															
																	/*$feedarHrs=0;
																	$site_meter_ids=explode(",", $consumer_detail['site_meter_id']);
																	if(count($site_meter_ids)>0){
																		for ($k=0;$k<count($site_meter_ids);$k++){
																			$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
																			if($feedarDetail){
																				if($feedarDetail->__get("is_24_hr")==1){
																					$feedarHr=24;
																				}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
																					$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
																				}else{
																					$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
																				}
																				$feedarHrs=$feedarHrs+$feedarHr;
																			}
																		}
																	}
																	$watt_total=$wattageMapper->getWattageBywattId($consumer_detail["wattage"]);
																	if($watt_total){
																		$wattage=$watt_total->__get("wattage");
																	}else{
																		$wattage=0;
																	}*/
																	 
																	$monthly_unit_value=$consumer_detail["unit"];//($wattage*$feedarHrs*$total_days_in_month)/1000;
																	
															$meterReading=$meterReadingMapper->getTotalUnitSiteWise($consumer_detail["consumer_id"],$month,$year);
															if($meterReading){
																if(floatval($meterReading["curr_reading"])>0){
												 					$totalMtrUnit=(floatval($meterReading["curr_reading"])-floatval($meterReading["last_reading"]));
																	if($monthly_unit_value<$totalMtrUnit){
																		$monthly_Unit_post=$totalMtrUnit;
																	}else{
																		$monthly_Unit_post=$monthly_unit_value;
																	}
																	
																}else{
																	$monthly_Unit_post=$monthly_unit_value; 
																}
															}else{
																$monthly_Unit_post=$monthly_unit_value;
															}
															
															$month_unit_post_total=$month_unit_post_total+ $monthly_Unit_post;
														} 
													 	  $totalPostPaid  = array_keys(array_flip($totalPostPaid));
														  
														$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer_detail['consumer_id']); 
														$month_scheme_pre_total=0;
														if($consumer_scheme){
															foreach($consumer_scheme as $consumer_schemes){
																$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
																$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
																
																$discount_month=$consumer_schemes["discount_month"];
																$feeder_hours=$consumer_schemes["feeder_hours"];
																$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
																$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month")); 
																
																if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
																	$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]*$feeder_hours*$total_days_in_month)/1000);
																	$month_scheme_pre_total=$month_scheme_pre_total + $dailyUnit;
																}
															}
														} 
														$month_unit_post_total=$month_unit_post_total+$month_scheme_pre_total;
														 
														if($month_unit_post_total>0){
															$unitTariffPostpaid=intval($cashRegister["billed_post"])/$month_unit_post_total;
														}else{ $unitTariffPostpaid=0;}
													}
												 	
													$arrear=(intval($cashRegister["billed_pre"])+intval($cashRegister["billed_post"]))-intval($cashRegister["collected"]);
													if($arrear<0){
														$total_arrear=abs($arrear);
													}else{$total_arrear=0 ;}
													  
													$consumerdata.= "<tr><td>".$site["site_name"]."</td>
																		 <td>".$date_today."</td>
																		 <td>".$consumers_types["consumer_type_name"]."</td>
																		 <td>".count($totalPrePaid)."</td>
																		 <td>".sprintf("%.2f",$package_tariff_pre)."</td>
																		 <td>".sprintf("%.2f",$month_unit_pre_total)."</td>
																		 <td>".intval($cashRegister["billed_pre"])."</td>
																		  <td>".count($totalPostPaid)."</td>
																		 <td>".sprintf("%.2f",$unitTariffPostpaid)."</td>
																		 <td>".sprintf("%.2f",$month_unit_post_total)."</td>
																		 <td>".intval($cashRegister["billed_post"])."</td>
																		 <td>".intval($OTPAndOthers["billed_OTP"])."</td>
																		 <td>".intval($OTPAndOthers["billed_OTHERS"])."</td>
																		 <td>".intval($cashRegister["collected"])."</td>
																		 <td>".$total_arrear."</td>
																	</tr>";
						 						} 
											}
											}
										}
									}
								}
							 
								$consumerdata.= "</table>";
								//echo $consumerdata;exit;  
								$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
								$cacheSettings = array( 'memoryCacheSize' => '8192MB');
								PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
						
								$objPHPExcel=null;
						
								$filename = "Monthly Sales Data for ".$month_val.", ".$year.'.xlsx';
								$files=(string)$filename;
								$inputFileType = 'Excel2007';
						
								$tmpfile = tempnam("/html/excelBackup/", 'html');
						
								file_put_contents($tmpfile, $consumerdata);
						        
								$objPHPExcel = new PHPExcel();
								$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
								$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
								$objPHPExcel->getActiveSheet()->setTitle("Monthly Sales Data");
						
								$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
								$objWriter->save($files);
								$objPHPExcel->disconnectWorksheets();
								unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings);
								gc_enable() ;gc_collect_cycles();
								 
								$arr = array(
									"data" => "http://taraurja.in/".$filename
								); 
								$json = json_encode($arr, JSON_PRETTY_PRINT);
								echo $json; 
								
								$auth=new My_Auth('user');
								$email_id=$auth->getIdentity()->user_email;
								$this->sendMailForSingleFileAction($files,$email_id); 
							}
								
							 
							
							break;
	  	case 'consumer_detail_data':
							
								$month_val = $request->getParam("month");
								$month = date("m", strtotime($month_val));
								$year= $request->getParam("year");
							
								$stateMapper=new Application_Model_StatesMapper();
								$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
								$cashRegisterMapper = new Application_Model_CashRegisterMapper();
								$siteMapper=new Application_Model_SitesMapper();
								$consumerMapper=new Application_Model_ConsumersMapper();
								$packageMapper=new Application_Model_PackagesMapper();
								$MMmapper=new Application_Model_SiteMasterMeterMapper();
								$meterReadingMapper=new Application_Model_MeterReadingsMapper();
								$wattageMapper=new Application_Model_WattageMapper();
									
								date_default_timezone_set('Asia/Kolkata');
								$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
							
								$StateName_Array=array();
								$roleSession = new Zend_Session_Namespace('roles');
								$role_sites_id=$roleSession->site_id;
								 
								$states=$stateMapper->getAllStates();
								$consumers_type=$consumerMapper->getConsumerType();
								$date_today=date('d-m-Y');
								if($states){
									$consumerdata="<table border=1 align='center'>";
									
									$consumerdata.="<tr>
												<td>Plant</td>
												<td>Customer Segment</td>
												<td>Year</td>
												<td>Month</td>
												<td>No. of Consumers</td>
												<td>Consumer Added</td>
												<td>Consumers Dropped</td>
												<td>Consumers Upgraded</td>
												<td>Consumers Downgraded</td>
												<td>Consumer Defaulters</td>
												<td>No. of Consumers (Month Target)</td>
											  </tr>";
							
							
									foreach ($states as $state){
										$sites=$siteMapper->getSitesForStateId($state->__get("state_id"),$role_sites_id);
										if($sites){
											foreach ($sites as $site){
												if($consumers_type){ 
													foreach ($consumers_type as $consumers_types){
														if($consumers_types["consumer_type_id"]!=11  && $consumers_types["consumer_type_id"]!=12){
														$consumer_total_pre=$consumerMapper->getConsumerForConsumerDataReport($consumers_types["consumer_type_id"],$month,$year,$site["site_id"],true);
														
														$ConsumerPackHis=$packageHistoryMapper->getpackageDetailByTypeOfMe($consumers_types["consumer_type_id"],$month,$year,$site["site_id"]);
														$upgrade=0;$downgrade=0;
														if($ConsumerPackHis){
															foreach ($ConsumerPackHis as $ConsumerPackHist){
																$new_package_id=$ConsumerPackHist["new_package"];
																$New_Pack=$packageMapper->getPackageById($new_package_id);
																
																$last_package_id=$ConsumerPackHist["last_package"];
																$Last_Pack=$packageMapper->getPackageById($last_package_id);
																
																$new_package_cost=$New_Pack->__get("package_cost");
																$curr_package_cost=$Last_Pack->__get("package_cost");
																
																$new_postpaid_value=$New_Pack->__get("is_postpaid");
																$cur_postpaid_value=$Last_Pack->__get("is_postpaid");
																 
																if($cur_postpaid_value==0 && $new_postpaid_value!=0){
																	$packageStatus=1;
																}else if($cur_postpaid_value==0 && $new_postpaid_value==0){
																	if($new_package_cost>$curr_package_cost){
																		$packageStatus=1;
																	}else{
																		$packageStatus=0;
																	}
																}else if($cur_postpaid_value!=0 && $new_postpaid_value==0){
																	$packageStatus=0;
																}else if($cur_postpaid_value!=0 && $new_postpaid_value!=0){
																	if($new_package_cost>$curr_package_cost){
																		$packageStatus=1;
																	}else{
																		$packageStatus=0;
																	}
																}else{
																	$packageStatus=0;
																}
																
																if($packageStatus==1){
																	$upgrade=$upgrade+1;
																}else{
																	$downgrade=$downgrade+1;
																}
																
															}
														}
														$all_consumer=(intval($consumer_total_pre["all_consumer"])+intval($consumer_total_pre["new_consumer"]))-intval($consumer_total_pre["banned_consumer"]);
														 
														$cashEntry=$cashRegisterMapper->getConsumerDefaulter($consumers_types["consumer_type_id"],$month,$year,$site["site_id"]);
														if($cashEntry){
															if(intval($all_consumer)>count($cashEntry)){
																$defaulter=intval($all_consumer)-count($cashEntry);
															}else{
																$defaulter=0;
															}
														}else{
															$defaulter=intval($all_consumer);
														}
														
														$consumerdata.= "<tr><td>".$site["site_name"]."</td>
																		  	 <td>".$consumers_types["consumer_type_name"]."</td>
																			 <td>".$month_val."</td>
																			 <td>".$year."</td>
																			 <td>".intval($consumer_total_pre["all_consumer"])."</td>
																			 <td>".intval($consumer_total_pre["new_consumer"])."</td>
																			 <td>".intval($consumer_total_pre["banned_consumer"])."</td>
																			 <td>".$upgrade."</td>
																			 <td>".$downgrade."</td>
																			 <td>".$defaulter."</td>
																			 <td>-</td>
																	     </tr>";
													}
													}
												}
											}
										}
									}
									 
									$consumerdata.= "</table>";
									//echo $consumerdata;exit;
									$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
									$cacheSettings = array( 'memoryCacheSize' => '8192MB');
									PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
							
									$objPHPExcel=null;
							
									$filename = "Monthly Consumer Data for ".$month_val.", ".$year.'.xlsx';
									$files=(string)$filename;
									$inputFileType = 'Excel2007';
							
									$tmpfile = tempnam("/html/excelBackup/", 'html');
							
									file_put_contents($tmpfile, $consumerdata);
							
									$objPHPExcel = new PHPExcel();
									$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
									$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
									$objPHPExcel->getActiveSheet()->setTitle("Monthly Consumer Data");
							
									$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
									$objWriter->save($files);
									$objPHPExcel->disconnectWorksheets();
									unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings);
									gc_enable() ;gc_collect_cycles();
										
									$arr = array(
											"data" => "http://taraurja.in/".$filename
									);
									$json = json_encode($arr, JSON_PRETTY_PRINT);
									echo $json;
							
									$auth=new My_Auth('user');
									$email_id=$auth->getIdentity()->user_email;
									$this->sendMailForSingleFileAction($files,$email_id);
								}
							 break;
							 
			case 'daily_plant_data':
							 		
							 	$month_val = $request->getParam("month");
							 	$month = date("m", strtotime($month_val));
							 	$year= $request->getParam("year");
							 		
							 	$stateMapper=new Application_Model_StatesMapper();
							 	$mpptMapper=new Application_Model_MpptsMapper();
							 	$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
							 	$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							 	$siteMapper=new Application_Model_SitesMapper();
							 	$consumerMapper=new Application_Model_ConsumersMapper();
							 	$packageMapper=new Application_Model_PackagesMapper();
							 	$MMmapper=new Application_Model_SiteMasterMeterMapper();
							 	$meterReadingMapper=new Application_Model_MeterReadingsMapper();
							 	$wattageMapper=new Application_Model_WattageMapper();
							 		
							 	date_default_timezone_set('Asia/Kolkata');
							 	$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
							 		
							 	$StateName_Array=array();
							 	$roleSession = new Zend_Session_Namespace('roles');
							 	$role_sites_id=$roleSession->site_id;
							 		
							 	$states=$stateMapper->getAllStates();
							 	$consumers_type=$consumerMapper->getConsumerType();
							 	$date_today=date('d-m-Y');
							 	if($states){
							 		$consumerdata="<table border=1 align='center'>";
							  
							 		$consumerdata.="<tr>
												<td>Plant</td>
												<td>Date</td>
												<td>Day time Consumption (Units/kWh)</td>
												<td>Night time Consumption (Units/kWh)</td>
												<td>24 hours Consumption (Units/kWh)</td>
												<td>Internal Consumption (Units/kWh)</td>
												<td>Solar Units Generated (Units/kWh)</td>
												<td>Biomass Units Generated (Units/kWh)</td>
												<td>DG Units Generated (Units/kWh)</td>
												<td>DG Run Hours (Units/kWh)</td>
												<td>Diesel Cosumptions(Lts)</td>
							 					<td>Power Outage Hours</td>
											  </tr>";
							 			
							 			
							 		foreach ($states as $state){
							 			$sites=$siteMapper->getSitesForStateId($state->__get("state_id"),$role_sites_id);
							 			if($sites){
							 				foreach ($sites as $site){
							 					 for ($i=1;$i<=$total_days_in_month;$i++){
							 						 $timestamp=$year."-".$month."-".$i;
							 						$feederDetails=$MMmapper->getMasterMeterReadingByFeederType($timestamp,$site["site_id"]); 	
							 						if($feederDetails){
							 							$DD_Unit=$feederDetails["DD_Unit"];
							 							$NN_Unit=$feederDetails["NN_Unit"];
							 							$DN_Unit=$feederDetails["DN_Unit"];
							 							$Plant_Unit=$feederDetails["Plant_Unit"];
							 						}else{
							 							$DD_Unit=NULL; $NN_Unit=NULL; $DN_Unit=NULL; $Plant_Unit=NULL;
							 						}
							 						$mpptDetails=$mpptMapper->getMasterMeterReadingByFeederType($timestamp,$site["site_id"]);
							 						if($mpptDetails){
							 							$solar_Unit=$mpptDetails["Solar"];
							 							$Bio_Unit=$mpptDetails["BIOGAS"];
							 							$DG_Unit=$mpptDetails["DG"];
							 							$DG_page=$mpptDetails["DG_page"];
							 							$site_status=$mpptDetails["site_status"];
							 						}else{
							 							$solar_Unit=NULL; $Bio_Unit=NULL; $DG_Unit=NULL;$DG_page=NULL;$site_status=NULL;
							 						}
							 						
							 						$consumerdata.= "<tr><td>".$site["site_name"]."</td>
																		 <td>".$timestamp."</td>
																		 <td>".$DD_Unit."</td>
																		 <td>".$NN_Unit."</td>
																		 <td>".$DN_Unit."</td>
																		 <td>".$Plant_Unit."</td>
																		 <td>".$solar_Unit."</td>
																	     <td>".$Bio_Unit."</td>
																		 <td>".$DG_Unit."</td>
																		 <td>-</td>
																		 <td>".$DG_page."</td>
																		 <td>".$site_status."</td>
																	 </tr>"; 
							 					}
							 				}
							 			}
							 		}
							 
							 		$consumerdata.= "</table>";
							 		 
							 		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
							 		$cacheSettings = array( 'memoryCacheSize' => '8192MB');
							 		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
							 			
							 		$objPHPExcel=null;
							 			
							 		$filename = "Daily Plant Data for ".$month_val.", ".$year.'.xlsx';
							 		$files=(string)$filename;
							 		$inputFileType = 'Excel2007';
							 			
							 		$tmpfile = tempnam("/html/excelBackup/", 'html');
							 			
							 		file_put_contents($tmpfile, $consumerdata);
							 			
							 		$objPHPExcel = new PHPExcel();
							 		$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
							 		$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
							 		$objPHPExcel->getActiveSheet()->setTitle("Daily Plant Data");
							 			
							 		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
							 		$objWriter->save($files);
							 		$objPHPExcel->disconnectWorksheets();
							 		unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings);
							 		gc_enable() ;gc_collect_cycles();
							 
							 		$arr = array(
							 				"data" => "http://taraurja.in/".$filename
							 		);
							 		$json = json_encode($arr, JSON_PRETTY_PRINT);
							 		echo $json;
							 			
							 		$auth=new My_Auth('user');
							 		$email_id=$auth->getIdentity()->user_email;
							 		$this->sendMailForSingleFileAction($files,$email_id);
							 	}
							 	break;
							    
						 	case "recancellation_report" :
							 	
							 		$stateMapper=new Application_Model_StatesMapper();
							 		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							 		$logMapper=new Application_Model_LogsMapper();
							 		$packageMapper=new Application_Model_PackagesMapper();
							 		$siteMapper=new Application_Model_SitesMapper();
							 		$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
							 		$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
							 		$userMapper=new Application_Model_UsersMapper();
							 		$consumerMapper=new Application_Model_ConsumersMapper();
							 		$MMmapper=new Application_Model_SiteMasterMeterMapper();
							 		$wattageMapper=new Application_Model_WattageMapper();
							 		$currDate = $request->getParam("secondDate");
							 		$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
							 		$currDate = $zendDates->toString("yyyy-MM-dd");
							 		$conPackageMapper=new Application_Model_ConsumersPackageMapper();
							 		$collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
							 		$bankDepositMapper=new Application_Model_CollectionsMapper();
							 		$lastDate = $request->getParam("firstDate");
							 		$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
							 		$lastDate = $zendDate->toString("yyyy-MM-dd");
							 	
							 		$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL: $request->getParam("state_id") ;
							 		$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL: $request->getParam("site_id");
							 		$file_type = ($request->getParam("file_type")=="" || $request->getParam("file_type")=='undefined' )?NULL: $request->getParam("file_type");
							 			
									
							 		$roleSession = new Zend_Session_Namespace('roles');
							 		$role_sites_id=$roleSession->site_id;
							 		$sites=$siteMapper->getSiteByStateId($state_id,$role_sites_id,$site_id); 
							 		$sites_arr=array();
							 		
										$overAllColl=0;
										//	$consumerdata.= "<h4 align='center'>Reconciliation Report for ".$site->__get("site_name")." from ".$request->getParam("firstDate")." to ".$request->getParam("secondDate")."</h4>";
											$consumerdata.= "<h4 align='center'>Reconciliation Report from ".$request->getParam("firstDate")." to ".$request->getParam("secondDate")."</h4>";
											
											$consumerdata.= "<div> 
																<table border=1 align='center'><tbody>";
											 
											$consumerdata.= "<tr>
														<th>Site Name</th>
														<th>Agent Name</th>
														<th>CHG</th>
														<th>ACT</th>
														<th>EED</th>
														<th>MED</th>
														<th>WATER</th>
														<th>OTHERS</th>
														<th>Security Deposit</th>
														<th>Total </th></tr>";
											$sites_arr=array();$opening_bal=0;
											if ($sites){
												foreach ($sites as $site){
													$sites_arr[]= $site->__get("site_id");
													$opening_bal=$opening_bal+intval($site->__get("opening_bal"));
													//$collection_agents=$collectionAgentMapper->getAgentBySiteId($site->__get("site_id"));
													//if($collection_agents){
														//foreach ($collection_agents as $collection_agent){
															$cashReg=$cashRegisterMapper->getTotalCollectionByCollectionId($currDate,$lastDate,$site->__get("site_id"),$collection_agent["collection_agent_id"],$collection_agent["user_id"]);
															$totalColl=intval($cashReg["CHG"])+intval($cashReg["ACT"])+intval($cashReg["EED"])+
															intval($cashReg["MED"])+intval($cashReg["WATER"])+intval($cashReg["OTHERS"])+intval($cashReg["SD"]);
															$overAllColl=$overAllColl+$totalColl;
															$consumerdata.= "<tr>
																<td>".$site->__get("site_name")."</td>
																<td>".$collection_agent["agent_fname"]." ".$collection_agent["agent_lname"]."</td>
																<td>".intval($cashReg["CHG"])."</td>
																<td>".intval($cashReg["ACT"])."</td>
																<td>".intval($cashReg["EED"])."</td>
																<td>".intval($cashReg["MED"])."</td>
																<td>".intval($cashReg["WATER"])."</td>
																<td>".intval($cashReg["OTHERS"])."</td>
																<td>".intval($cashReg["SD"])."</td>
																<td>".$totalColl."</td></tr>";
														//}
													//}
													}
													//$sites_arr=array($site->__get("site_id"));
												
													$bankDeposit=$bankDepositMapper->getAllDepositBySiteData($sites_arr,$currDate,$lastDate);
										 
													$opening=$cashRegisterMapper->getOpeningBalanceData($currDate,$lastDate,$sites_arr);
													$opening_bal=$opening_bal + (intval($opening["collection"])-intval($opening["bankdeposit"]));  
													
													$remainingBal=(intval($opening_bal)+$overAllColl)-intval($bankDeposit);
													$consumerdata.= "<tr></tr>
															<tr>
															<td>Total Collection</td>
															<td> </td> <td> </td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
															<td>".intval($overAllColl)."</td> 
														</tr>";
															 
													$consumerdata.= "<tr></tr>
															<tr>
															<td>Opening Balance</td>
															<td> </td> <td> </td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
															<td>".intval($opening_bal)."</td>
														</tr>";
													$consumerdata.= "<tr></tr>
															<tr>
															<td>Total Bank Deposit</td>
															<td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
															<td>".intval($bankDeposit)."</td>
														</tr>";
													$consumerdata.= "<tr></tr>
															<tr>
															<td>Remaining Balance</td>
															<td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
															<td>".$remainingBal."</td>
														</tr>";
													
											
												$consumerdata.=  "</tbody> </table>
													</div> <pagebreak />"; 
												}
										  // }  
							 			
							 		
									//echo $consumerdata;exit; 
							 		if($file_type=='PDF'){
										$filename='Reconciliation Report from '.$request->getParam("firstDate")." - ".$request->getParam("secondDate").'.pdf';
										$files=(string)$filename;
										$checkPdf=$this->pdfGeneratesAction(null,$consumerdata,$files);
									}else{
										$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
										$cacheSettings = array( 'memoryCacheSize' => '8192MB');
										PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings); $objPHPExcel=null;
										$filename = 'Reconciliation Report from '.$request->getParam("firstDate")." - ".$request->getParam("secondDate").'.xlsx';
										$files=(string)$filename; $inputFileType = 'Excel2007';
										 
										$tmpfile = tempnam("/html/excelBackup/", 'html'); file_put_contents($tmpfile, $consumerdata);
										
										$objPHPExcel = new PHPExcel(); $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
										$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
										$objPHPExcel->getActiveSheet()->setTitle('Reconciliation Report');
										
										$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
										$objWriter->save($files); $objPHPExcel->disconnectWorksheets();
										unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings); gc_enable() ;gc_collect_cycles();
									}
									  
										
									$auth=new My_Auth('user');
									$email_id=$auth->getIdentity()->user_email;
									$this->sendMailForSingleFileAction($files,$email_id);
									break;
									
									
					case "billing_comparision" :
											
										$stateMapper=new Application_Model_StatesMapper();
										$cashRegisterMapper = new Application_Model_CashRegisterMapper();
										$logMapper=new Application_Model_LogsMapper();
										$packageMapper=new Application_Model_PackagesMapper();
										$siteMapper=new Application_Model_SitesMapper();
										$consumerMapper=new Application_Model_ConsumersMapper();
										$conPackageMapper=new Application_Model_ConsumersPackageMapper();
										$meterReadingMapper=new Application_Model_MeterReadingsMapper();
										 
										$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL: $request->getParam("state_id") ;
										$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL: $request->getParam("site_id");
										$entry_type = ($request->getParam("bill_type")=="" || $request->getParam("bill_type")=='undefined' )?NULL: $request->getParam("bill_type");
										$file_type = ($request->getParam("file_type")=="" || $request->getParam("file_type")=='undefined' )?NULL: $request->getParam("file_type");
										$month_val = $request->getParam("month");
										
										$month = date("m", strtotime($month_val));
										$year = $request->getParam("year");
										
										$dateObj   = DateTime::createFromFormat('!m', $month);
										$MonthName = $dateObj->format('M');
										
										if($month==1){
											$lastMonth=12;
											$lastYear=$year-1;
										}else{
											$lastMonth=$month-1;
											$lastYear=$year;
										}
										$dateObj   = DateTime::createFromFormat('!m', $lastMonth);
										$lastMonthName = $dateObj->format('M');
										
										if($lastToLastMonth==1){
											$lastToLastMonth=12;
											$lastToLastyear=$lastYear-1;
										}else{
											$lastToLastMonth=$lastMonth-1;
											$lastToLastyear=$lastYear;
										}
										$dateObj   = DateTime::createFromFormat('!m', $lastToLastMonth);
										$lastToLastMonthName = $dateObj->format('M');
										
										$roleSession = new Zend_Session_Namespace('roles');
										$role_sites_id=$roleSession->site_id;
										
										$sites=$siteMapper->getSiteByStateId($state_id,$role_sites_id,$site_id);
										$sites_arr=array();
										if ($sites){
											foreach ($sites as $site){
												$siteID=$site->__get("site_id");
												$overAllColl=0;
												$consumerdata.= "<h4 align='center'>Billing Comparision Report for ".$site->__get("site_name")." of ".$MonthName.", ".$year."</h4>";
												$consumerdata.= "<div>
																<table border=1 align='center'><tbody>";
									
												$consumerdata.= "<tr>
															<th>Consumer ID</th>
															<th>Consumer Name</th>
															<th>Consumer connection id</th>
															<th>Status</th>
															<th>Active/Banned date</th>
															<th>Package Name</th>
															<th>Package Type</th>
															<th>Unit</th>";
																 
												$consumerdata.= "<th>Prepaid ".$MonthName."-".$year."</th> 
																 <th>Prepaid ".$lastMonthName."-".$lastYear."</th>";
												$consumerdata.= "<th>Prepaid Difference</th>";
																
												$consumerdata.= "<th>Postpaid ".$lastMonthName."-".$lastYear."</th>
																 <th>Postpaid ".$lastToLastMonthName."-".$lastToLastyear."</th>";
												$consumerdata.= "<th>Postpaid Difference</th>";
												
												$consumerdata.= "</tr>";
												$consumers=$consumerMapper->getConsumersByColumnValue("site_id", $siteID,true,null,null,null,true);
												if($consumers){
													foreach ($consumers as $consumer){
														$cashRegister=$cashRegisterMapper->getBillingComparision($consumer->__get("consumer_id"),$entry_type,$month,$year,$lastMonth,$lastYear,$lastToLastMonth,$lastToLastyear);
														$prepaidDifference=(intval($cashRegister["curr_month_pre"])-intval($cashRegister["last_month_pre"]));
														$postpaidDifference=(intval($cashRegister["last_month_post"])-intval($cashRegister["last_to_last_month_post"]));
														if($consumer->__get("consumer_status")=='banned'){
															$date=date("d-M-Y",strtotime($consumer->__get("suspension_date")));
														}else{
															$date=date("d-M-Y",strtotime($consumer->__get("consumer_act_date")));
														}
														if(($consumer->__get("consumer_status")=='banned' && (intval($cashRegister["curr_month_pre"])!=0  || intval($cashRegister["last_month_pre"])!=0  || intval($cashRegister["last_month_post"])!=0  || intval($cashRegister["last_to_last_month_post"])!=0))
															|| $consumer->__get("consumer_status")=='active' || $consumer->__get("consumer_status")=='new'){

															$consumerpackage=$conPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
															$is_postpaid=array();$packageName=array();$package_id_val=0;
															if($consumerpackage){
																foreach($consumerpackage as $package){
																	$packageName[]=$package["package_name"];
																	$is_postpaid[]=(($package["is_postpaid"]==0)?'PrePaid':(($package["is_postpaid"]==2)?'MixMode':'Postpaid'));  
																	if($is_postpaid!=0){
																		$package_id_val=$package["package_id"];
																	}
																}
															}
															
															$meterReadingUnit=sprintf("%.2f",($meterReadingMapper->getTotalUnitByConsumerId($consumer->__get("consumer_id"),$package_id_val,$lastMonth,$lastYear)));
															$consumerdata.= "<tr>
																<td>".$consumer->__get("consumer_id")."</td>
																<td>".$consumer->__get("consumer_name")."</td>
																<td>".$consumer->__get("consumer_connection_id")."</td>
																<td>".ucfirst($consumer->__get("consumer_status"))."</td>
																<td>".$date."</td>	
																<td>".implode(',',$packageName)."</td>
																<td>".implode(',',$is_postpaid)."</td>
																<td>".$meterReadingUnit."</td> 
																<td>".intval($cashRegister["curr_month_pre"])."</td>
																<td>".intval($cashRegister["last_month_pre"])."</td>
																<td>".intval($prepaidDifference)."</td>
																<td>".intval($cashRegister["last_month_post"])."</td>
																<td>".intval($cashRegister["last_to_last_month_post"])."</td>
																<td>".intval($postpaidDifference)."</td></tr>"; 
														}
													}
												} 
												$consumerdata.=  "</tbody> </table>
											</div> <pagebreak />";
											}
										}
										//echo $consumerdata;exit;
										if($file_type=='PDF'){
											$filename='Billing comparision report of '.$MonthName."-".$year.'.pdf';
											$files=(string)$filename;
											$checkPdf=$this->pdfGeneratesAction(null,$consumerdata,$files);
										}else{
											$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
											$cacheSettings = array( 'memoryCacheSize' => '8192MB');
											PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings); $objPHPExcel=null;
											$filename='Billing comparision report of '.$MonthName."-".$year.'.xlsx';
											$files=(string)$filename; $inputFileType = 'Excel2007';
												
											$tmpfile = tempnam("/html/excelBackup/", 'html'); file_put_contents($tmpfile, $consumerdata);
									
											$objPHPExcel = new PHPExcel(); $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('Billing comparision');
									
											$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
											$objWriter->save($files); $objPHPExcel->disconnectWorksheets();
											unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings); gc_enable() ;gc_collect_cycles();
										}
										
									
										$auth=new My_Auth('user');
										$email_id=$auth->getIdentity()->user_email;
										$this->sendMailForSingleFileAction($files,$email_id);
										break;
										
					case "diesel_report" :
							 	
							 		$stateMapper=new Application_Model_StatesMapper();
							 		$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							 		$logMapper=new Application_Model_LogsMapper();
							 		$packageMapper=new Application_Model_PackagesMapper();
							 		$siteMapper=new Application_Model_SitesMapper();
							 		$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
							 		$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
							 		$userMapper=new Application_Model_UsersMapper();
							 		$consumerMapper=new Application_Model_ConsumersMapper();
							 		$MMmapper=new Application_Model_SiteMasterMeterMapper();
							 		$wattageMapper=new Application_Model_WattageMapper();
							 		$currDate = $request->getParam("secondDate");
							 		$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
							 		$currDate = $zendDates->toString("yyyy-MM-dd");
							 		$conPackageMapper=new Application_Model_ConsumersPackageMapper();
							 		$collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
							 		$bankDepositMapper=new Application_Model_CollectionsMapper();
							 		$lastDate = $request->getParam("firstDate"); 
							 		$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
							 		$lastDate = $zendDate->toString("yyyy-MM-dd");
							 	
							 		$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL: $request->getParam("state_id") ;
							 		$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL: $request->getParam("site_id");
							 		$file_type = ($request->getParam("file_type")=="" || $request->getParam("file_type")=='undefined' )?NULL: $request->getParam("file_type");
							 			
									
							 		$roleSession = new Zend_Session_Namespace('roles');
							 		$role_sites_id=$roleSession->site_id;
							 		$sites=$siteMapper->getSiteByStateId($state_id,$role_sites_id,$site_id); 
							 		$sites_arr=array();$consumerdata="";
							 		if ($sites){
										$overAllDieselCost=0;$overAllDieselGen=0;$overAllDieselUnit=0; $overAllDieselltr=0;
										$consumerdata.= "<h4 align='center'>Diesel Report from ".$request->getParam("firstDate")." to ".$request->getParam("secondDate")."</h4>";
											$consumerdata.= "<div> 
																<table border=1 align='center'><tbody>";
											 
											$consumerdata.= "<tr>
														<th>Site Name</th>
														<th>Diesel Cost</th>
														<th>Diesel (Liter)</th> 
														<th>Estimated DG Unit</th>
														<th>Actual Generation</th>
														<th>Difference</th></tr>";
											
							 			foreach ($sites as $site){
												
							 				$dieselDetails=$mpptReadingMapper->getDieselReport($site->__get("site_id"),NULL,NULL,$currDate,$lastDate);
							 				if($dieselDetails){
											
							 					    $overAllDieselGen=$overAllDieselGen+$dieselDetails["DG"];
													
													$overAllDieselCost=$overAllDieselCost+$dieselDetails["DG_Cost"];
													$overAllDieselltr=$overAllDieselltr+$dieselDetails["DG_unit"];
													$overAllDieselUnit=$overAllDieselUnit+($dieselDetails["DG_unit"]*$site->__get("diesel_factor"));
													
													$diff=($dieselDetails["DG_unit"]*$site->__get("diesel_factor"))-$dieselDetails["DG"];
													
							 						$consumerdata.= "<tr>
														<td>".$site->__get("site_name")."</td>
														<td>".sprintf("%.2f",($dieselDetails["DG_Cost"]))."</td>
														<td>".sprintf("%.2f",($dieselDetails["DG_unit"]))."</td> 
														<td>".sprintf("%.2f",($dieselDetails["DG_unit"])*$site->__get("diesel_factor"))."</td> 
														<td>".sprintf("%.2f",($dieselDetails["DG"]))."</td> 
														<td>".sprintf("%.2f",($diff))."</td> </tr>";
							 				}
										}
										$totalDiff=$overAllDieselUnit-$overAllDieselGen;
							 				$consumerdata.= "<tr></tr>
													<tr> 
													<td>Total Collection</td>
													<td>".sprintf("%.2f",($overAllDieselCost))."</td> 
													<td>".sprintf("%.2f",($overAllDieselltr))."</td>
													<td>".sprintf("%.2f",($overAllDieselUnit))."</td>
													<td>".sprintf("%.2f",($overAllDieselGen))."</td>
													<td>".sprintf("%.2f",($totalDiff))."</td>
												</tr>"; 
										 
									
										$consumerdata.=  "</tbody> </table>
											</div> "; 
										
								   }
							 			
							 		
									//echo $consumerdata;exit; 
							 		if($file_type=='PDF'){
										$filename='Diesel Report from '.$request->getParam("firstDate")." - ".$request->getParam("secondDate").'.pdf';
										$files=(string)$filename;
										$checkPdf=$this->pdfGeneratesAction(null,$consumerdata,$files);
									}else{
										$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
										$cacheSettings = array( 'memoryCacheSize' => '8192MB');
										PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings); $objPHPExcel=null;
										$filename = 'Diesel Report from '.$request->getParam("firstDate")." - ".$request->getParam("secondDate").'.xlsx';
										$files=(string)$filename; $inputFileType = 'Excel2007';
										 
										$tmpfile = tempnam("/html/excelBackup/", 'html'); file_put_contents($tmpfile, $consumerdata);
										
										$objPHPExcel = new PHPExcel(); $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
										$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
										$objPHPExcel->getActiveSheet()->setTitle('Reconciliation Report');
										
										$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
										$objWriter->save($files); $objPHPExcel->disconnectWorksheets();
										unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings); gc_enable() ;gc_collect_cycles();
									}
									  
										
									$auth=new My_Auth('user');
									$email_id=$auth->getIdentity()->user_email;
									$this->sendMailForSingleFileAction($files,$email_id);
									break;
									
				 default:
                        //$result = "Invalid request";
                        $result = "Error. Kripya kuch der baad try karein."; 
                        break;		 
	}
		
}
		 
	public function pdfGenerateAction($data,$filename){
			 
			date_default_timezone_set('Asia/Kolkata');
			$pdf=new HTML2FPDF('P','mm',"a2");
			$pdf->AddPage();
			 
			$strContent=$data;
			$pdf->WriteHTML($strContent);
			 
			$pdf->Output($filename);
			return $filename;
	}	
	public function pdfGenerateCheckAction($data,$filename){
	
		date_default_timezone_set('Asia/Kolkata');
		$pdf=new HTML2FPDF('P','mm',"a1"); 
		$pdf->AddPage();
	
		$strContent=$data;
		$pdf->WriteHTML($strContent);
	 
		$pdf->Output($filename);
		return $filename;
      }	

     public function pdfGeneratesAction($site_name,$data,$files){
      
      	$mpdf=new mPDF('c','A4-L','','',15,15,16,16,9,9,'L');
      	$mpdf->AddPage();
      	$stylesheet = "
		body { font-family: DejaVuSansCondensed; font-size: 11pt;  }
		p { 	text-align: justify; margin-bottom: 4pt; margin-top:0pt;  }

		table {font-family: DejaVuSansCondensed; font-size: 9pt; line-height: 1.2;
			margin-top: 2pt; margin-bottom: 5pt;
			border-collapse: collapse;  }

		thead {	font-weight: bold; vertical-align: bottom; }
		tfoot {	font-weight: bold; vertical-align: top; }
		thead td { font-weight: bold; }
		tfoot td { font-weight: bold; }

		thead td, thead th, tfoot td, tfoot th { font-variant: small-caps; }

		.headerrow td, .headerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
		.footerrow td, .footerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }

		th {	font-weight: bold; 
			vertical-align: top; 
			text-align:left; 
			padding-left: 2mm; 
			padding-right: 2mm; 
			padding-top: 0.5mm; 
			padding-bottom: 0.5mm; 
		 }

		td {	padding-left: 2mm; 
			vertical-align: top; 
			text-align:left; 
			padding-right: 2mm; 
			padding-top: 0.5mm; 
			padding-bottom: 0.5mm;
		 }

		th p { text-align: left; margin:0pt;  }
		td p { text-align: left; margin:0pt;  }

		table.widecells td {
			padding-left: 5mm;
			padding-right: 5mm;
		}
		table.tallcells td {
			padding-top: 3mm;
			padding-bottom: 3mm; 
		}

		hr {	width: 70%; height: 1px; 
			text-align: center; color: #999999; 
			margin-top: 8pt; margin-bottom: 8pt; }

		a {	color: #000066; font-style: normal; text-decoration: underline; 
			font-weight: normal; }

		ul {	text-indent: 5mm; margin-bottom: 9pt; }
		ol {	text-indent: 5mm; margin-bottom: 9pt; }

		pre { font-family: DejaVuSansMono; font-size: 9pt; margin-top: 5pt; margin-bottom: 5pt; }

		h1 {	font-weight: normal; font-size: 26pt; color: #000066; 
			font-family: DejaVuSansCondensed; margin-top: 4pt; margin-bottom: 6pt; 
			border-top: 0.075cm solid #000000; border-bottom: 0.075cm solid #000000; 
			text-align: ; page-break-after:avoid; }
		h2 {	font-weight: bold; font-size: 12pt; color: #000066; 
			font-family: DejaVuSansCondensed; margin-top: 4pt; margin-bottom: 6pt; 
			border-top: 0.07cm solid #000000; border-bottom: 0.07cm solid #000000; 
			text-align: ;  text-transform:uppercase; page-break-after:avoid; }
		h3 {	font-weight: normal; font-size: 26pt; color: #000000; 
			font-family: DejaVuSansCondensed; margin-top: 0pt; margin-bottom: 6pt; 
			border-top: 0; border-bottom: 0; 
			text-align: ; page-break-after:avoid; }
		h4 {	font-weight: ; font-size: 13pt; color: #9f2b1e; 
			font-family: DejaVuSansCondensed; margin-top: 4pt; margin-bottom: 7pt; 
			font-variant: small-caps;
			text-align: ;  margin-collapse:collapse; page-break-after:avoid; }
		h5 {	font-weight: bold; font-style:italic; ; font-size: 11pt; color: #000044; 
			font-family: DejaVuSansCondensed; margin-top: 2pt; margin-bottom: 4pt; 
			text-align: ;  page-break-after:avoid; }
		h6 {	font-weight: bold; font-size: 9.5pt; color: #333333; 
			font-family: DejaVuSansCondensed; margin-top: 4pt; margin-bottom: ; 
			text-align: ;  page-break-after:avoid; }

		.breadcrumb {
			text-align: right; font-size: 8pt; font-family: DejaVuSerifCondensed; color: #666666;
			font-weight: bold; font-style: normal; margin-bottom: 6pt; }

		.evenrow td, .evenrow th { background-color: #f5f8f5; } 
		.oddrow td, .oddrow th { background-color: #e3ece4; } 

		.bpmTopic {	background-color: #e3ece4; }
		.bpmTopicC { background-color: #e3ece4; }
		.bpmNoLines { background-color: #e3ece4; }
		.bpmNoLinesC { background-color: #e3ece4; }
		.bpmClear {		}
		.bpmClearC { text-align: center; }
		.bpmTopnTail { background-color: #e3ece4; topntail: 0.02cm solid #495b4a;}
		.bpmTopnTailC { background-color: #e3ece4; topntail: 0.02cm solid #495b4a;}
		.bpmTopnTailClear { topntail: 0.02cm solid #495b4a; }
		.bpmTopnTailClearC { topntail: 0.02cm solid #495b4a; }

		.bpmTopicC td, .bpmTopicC td p { text-align: center; }
		.bpmNoLinesC td, .bpmNoLinesC td p { text-align: center; }
		.bpmClearC td, .bpmClearC td p { text-align: center; }
		.bpmTopnTailC td, .bpmTopnTailC td p { text-align: center;  }
		.bpmTopnTailClearC td, .bpmTopnTailClearC td p {  text-align: center;  }

		.pmhMiddleCenter { text-align:center; vertical-align:middle; }
		.pmhMiddleRight {	text-align:right; vertical-align:middle; }
		.pmhBottomCenter { text-align:center; vertical-align:bottom; }
		.pmhBottomRight {	text-align:right; vertical-align:bottom; }
		.pmhTopCenter {	text-align:center; vertical-align:top; }
		.pmhTopRight {	text-align:right; vertical-align:top; }
		.pmhTopLeft {	text-align:left; vertical-align:top; }
		.pmhBottomLeft {	text-align:left; vertical-align:bottom; }
		.pmhMiddleLeft {	text-align:left; vertical-align:middle; }

		.infobox { margin-top:10pt; background-color:#DDDDBB; text-align:center; border:1px solid #880000; }

		.bpmTopic td, .bpmTopic th  {	border-top: 1px solid #FFFFFF; }
		.bpmTopicC td, .bpmTopicC th  {	border-top: 1px solid #FFFFFF; }
		.bpmTopnTail td, .bpmTopnTail th  {	border-top: 1px solid #FFFFFF; }
		.bpmTopnTailC td, .bpmTopnTailC th  {	border-top: 1px solid #FFFFFF; }";
       
      		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
      		$mpdf->WriteHTML($data,2);
      		$mpdf->Output($files);
	     		return $files;
      }
	public function sendMailStateWiseAction($filename,$email_id=NULL,$ytd=NULL,$ytd_cycle=NULL)
	{
			date_default_timezone_set('Asia/Kolkata');
			$filenames=array();
			defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
	
			$mail = new Zend_Mail();
			$config = array('auth' => 'login',
					'username' => 'postmaster@iwsspl.com',
					'ssl' => 'ssl',
					'port' => 465,
					'password' => '68b1f5b3204ae4f4072c4afb59e241c0');
	
			$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
	
			for($i=0;$i<count($filename);$i++){
				$filenames[$i] = PUBLIC_PATH . "/html/".$filename[$i];
				$content = file_get_contents($filenames[$i]);
				$attachment = new Zend_Mime_Part($content);
				$attachment->type = 'application/pdf';
				$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
				$attachment->encoding = Zend_Mime::ENCODING_BASE64;
				$attachment->filename = $filename[$i];
					
				$mail->addAttachment($attachment);
			}
			if(count($filename)>0){
				 
				$mail->setFrom("postmaster@iwsspl.com", "TARAUrja.in CRON");
				  
	  			
				if($ytd==NULL){
					//$mail->addTo(array('paggarwal@devalt.org','adhawan@devalt.org','skumar1@devalt.org'), 'recipient');
					$mail->addTo($email_id, 'recipient');
					$mail->addCc('taraurja@gmail.com');
					$mail->setSubject("Customers Account Sheet ". date('d M Y hi A')); 
	  				 
				}else{
					$mail->addTo($email_id, 'recipient');
					if($ytd_cycle!=NULL){
						$mail->setSubject("Collection Sheet (Old Cycle) ". date('d M Y hi A')); 
					}else{
						$mail->setSubject("Collection Sheet ". date('d M Y hi A')); 
					}   
				} 
				$mail->setBodyHtml("PFA"); 
				 
				if ($mail->send($transport)) {
						 
					echo "Mail sent successfully..."; 
				}
				else {
					echo "Mail could not be sent...";  
				}
			}
	
		}
	public function sendMailForSingleFileAction($filename=NULL,$email_id=NULL)
		{
			date_default_timezone_set('Asia/Kolkata');
			$filenames=array();
			defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
		
			$mail = new Zend_Mail();
			$config = array('auth' => 'login',
					'username' => 'postmaster@iwsspl.com',
					'ssl' => 'ssl',
					'port' => 465,
					'password' => '68b1f5b3204ae4f4072c4afb59e241c0');
		
				$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
		
			     
				$filenames = PUBLIC_PATH . "/html/".$filename;
				$content = file_get_contents($filenames);
				$attachment = new Zend_Mime_Part($content);
				$attachment->type = 'application/pdf';
				$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
				$attachment->encoding = Zend_Mime::ENCODING_BASE64;
				$attachment->filename = $filename;
					
				$mail->addAttachment($attachment);
	 
			 	$mail->setFrom("postmaster@iwsspl.com", "TARAUrja.in CRON");
		        $mail->addTo($email_id, 'recipient');
				$mail->setSubject($filename);
					 
				 
				$mail->setBodyHtml("PFA");
					
				if ($mail->send($transport)) {
						
					//echo "Mail sent successfully...";
				}
				else {
					//echo "Mail could not be sent...";
				}
			 
		
		}
		
 public function sendMailForCollectionAction($filename=NULL,$email_id=NULL)
		{
			date_default_timezone_set('Asia/Kolkata');
			$filenames="";
			defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
		
			$mail = new Zend_Mail();
			$config = array('auth' => 'login',
					'username' => 'postmaster@iwsspl.com',
					'ssl' => 'ssl',
					'port' => 465,
					'password' => '68b1f5b3204ae4f4072c4afb59e241c0');
		
				$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
		
			 
				$filenames = PUBLIC_PATH . "/html/".$filename;
				$content = file_get_contents($filenames); 
				$attachment = new Zend_Mime_Part($content);
				$attachment->type = 'application/pdf';
				$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
				$attachment->encoding = Zend_Mime::ENCODING_BASE64;
				$attachment->filename = $filename;
					
				$mail->addAttachment($attachment);
	 
			 	$mail->setFrom("postmaster@iwsspl.com", "TARAUrja.in CRON");
		        $mail->addTo($email_id, 'recipient');
				$mail->setSubject($filename);
					 
				 
				$mail->setBodyHtml("PFA");
					
				if ($mail->send($transport)) {
					 echo "Mail sent successfully...";
				} else {
					 echo "Mail could not be sent...";
				}
			 
		
		}
}



 

