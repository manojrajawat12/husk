<?php

class Admin_AjaxController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction() {
        $consumersMapper = new Application_Model_ConsumersMapper();
        print_r($consumersMapper->getTotalConsumersBySite(17));
    }
	
	public function appInitAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$mpptsMapper=new Application_Model_MpptsMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    	$stateMapper=new Application_Model_StatesMapper();
		$equipmentMapper=new Application_Model_EquipmentMapper();
    	
    	$request = $this->getRequest();
    	date_default_timezone_set('Asia/Kolkata');
    	$device_token=$request->getParam("device_token");
    	$device_type=$request->getParam("device_type");
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$parameter=array(
    			"device_token"=>$device_token,
    			"device_type"=>$device_type,
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    	);
    	
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
			 
    	$files=array();	$success=0;
    	$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
    	if($collectionAgents){
    		if($collectionAgents->__get("collection_agent_id")!=0){
    			$sites=$sitesMapper->getSitesByCollectionSite(explode(",",$collectionAgents->__get("site_id")));
				$permissions=explode(',', $collectionAgents->__get("permission")) ;
				$menu_permission=array(
						'dashboard'=>in_array("1", $permissions)?1:0,
						'cash_register'=>in_array("2", $permissions)?1:0,
						'customer'=>in_array("3", $permissions)?1:0,
						'plant'=>in_array("4", $permissions)?1:0); 
																	
				$subMenu_permission=array(
						'add_customer'=>in_array("5", $permissions)?1:0,
						'add_meter_reading'=>in_array("6", $permissions)?1:0,
						'add_transaction'=>in_array("7", $permissions)?1:0,
						'customer_list'=>in_array("8", $permissions)?1:0,
						'package'=>in_array("9", $permissions)?1:0,
						'scheme'=>in_array("10", $permissions)?1:0,
						'feeder'=>in_array("11", $permissions)?1:0,
						'feeder_data'=>in_array("12", $permissions)?1:0,
						'plant_status'=>in_array("13", $permissions)?1:0,
						'mppts'=>in_array("14", $permissions)?1:0,
						'mppts_data'=>in_array("15", $permissions)?1:0,
						'diesel_generator'=>in_array("16", $permissions)?1:0,
						'add_instant_consumer'=>in_array("17", $permissions)?1:0,
						'cash_ledger'=>in_array("18", $permissions)?1:0,
						'temporary_disconnect'=>in_array("19", $permissions)?1:0,
						'change_feeder_meter'=>in_array("20", $permissions)?1:0,
						'bank_deposit'=>in_array("21", $permissions)?1:0,
						'edit_consumer_profile'=>in_array("22", $permissions)?1:0,
						'consumer_activity'=>in_array("23", $permissions)?1:0,
						'change_consumer_meter'=>in_array("24", $permissions)?1:0,
						'unrelease_meter'=>in_array("25", $permissions)?1:0);
    		}else{
				$menu_permission=array();$subMenu_permission=array();
    			$sites=$sitesMapper->getSitesByCollectionSite();
    		}
			$site_array_val=array();
			foreach($sites as $site){
				$site_array_val[]=$site["site_id"];
			}
    		$success=1;
    		$state_arr=array();
    		$states=$stateMapper->getAllStates();
    		if($states){
    			foreach($states as $state){
					$state_data=array("state_id"=>$state->__get("state_id"),
						"state_name"=>$state->__get("state_name"),
					);
					$state_arr[]=$state_data;	
    			}
    		}
    		$site_arr=array();
    		if($sites){
    			foreach($sites as $site){
    				$consumer_arr=array();$MM_arr=array();$data=array();
    				
    				$otp_dues=0;
    				$Otp_due=$cashRegisterMapper->getTotalOtpDueSiteWise($site["site_id"]);
    				if($Otp_due){
						$otp_dues=$Otp_due["Bill_ACT"]-$Otp_due["ACT"];
    				}
    				$siteMasters=$siteMasterMapper->getSiteMasterById($site["site_id"]);
    				if($siteMasters){
    					foreach($siteMasters as $siteMaster){
    						$siteMasterMeters=$siteMasterMapper->getMasterMeterReadingByMeterId($siteMaster->__get("id"));
    						if($siteMasterMeters){
    							$siteMasterMeter=$siteMasterMeters[0];
    							$last_reading=$siteMasterMeter->__get("reading");
    							$reading_on = $siteMasterMeter->__get("reading_date");
    						}else{
    							$last_reading=0;
    							$reading_on=0;
    						}
    						$packages=$packagesMapper->getPackagesByFeederId($siteMaster->__get("id"));
    						$packages_arr=array();
    						if($packages){
    							foreach($packages as $package){
    								$watt_total=0;
    								$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
    								if($watt){
    									$watt_total=$watt->__get("wattage");
    								}
    								$pack_data=array(
    										"package_id"=>$package["package_id"],
    										"package_name"=>$package["package_name"],
    										"package_cost"=>$package["package_cost"],
    										"is_postpaid"=>$package["is_postpaid"],
											
											"max_wattage_allowed"=>$watt_total,
    										"min_amount"=>$package["package_cost"],
    										"free_units"=>$package["unit"],
    										"rate_after_free_units"=>$package["extra_charges"],
											"min_unit_rate"=>$package["min_charges"],
    										"max_unit_rate"=>$package["max_charges"],
											
    										"wattage"=>$watt_total,
    										"status"=>$package["status"],
    								);
    								$packages_arr[]=$pack_data;
    							}
    						}
    							
    						$MM_detail=array(
    								"feeder_id"=>$siteMaster->__get("id"),
    								"meter_name"=>$siteMaster->__get("description"),
    								"last_reading" =>$last_reading,
    								"reading_on" =>$reading_on,
    								"meter_keyword"=>$siteMaster->__get("meter_keyword"),
    								"description" =>$siteMaster->__get("description"),
    								"start_time" =>$siteMaster->__get("start_time"),
    								"end_time"=>$siteMaster->__get("end_time"),
    								"is_24_hr"=>$siteMaster->__get("is_24_hr"),
    								"packages"=>$packages_arr
    						);
    							
    							
    						$MM_arr[]=$MM_detail;
    					}
    				}
					//Equipment Name, Down Payment, Emi Amount , Duration, Package Name, Discount Amount & Validity
    				$scheme_arr=array();
    				$schems=$schemeMapper->getSchemeBySiteId($site["site_id"]);
    				if($schems){
    					foreach ($schems as $scheme){
						$packages_data=$packagesMapper->getPackageById($scheme["package_id"]);
						 
						 $start_date = $scheme["start_date"];
						 $end_date = $scheme["end_date"];
						 
						 $date = new Zend_Date();
						 $date->setTimezone("Asia/Kolkata");
						 $current_date = $date->toString("yyyy-MM-dd");
						  
						if(strtotime($current_date)>=strtotime($start_date) && strtotime($current_date)<=strtotime($end_date)){
								$equipment=($scheme["celamed_id"]=="NULL" || $scheme["celamed_id"]=='undefined' || $scheme["celamed_id"]==NULL)?0:$scheme["celamed_id"];
								
								$equipments=$equipmentMapper->getEquipmentById($equipment);
								$equip_name="";
								if($equipments){
									$equip_name=$equipments->__get("name");
								}
								$scheme_data=array(
										"scheme_id"=>($scheme["scheme_id"]=="NULL" || $scheme["scheme_id"]=='undefined' || $scheme["scheme_id"]==NULL)?'':$scheme["scheme_id"],
										"scheme_name"=> ($scheme["scheme"]=="NULL" || $scheme["scheme"]=='undefined' || $scheme["scheme"]==NULL)?'':$scheme["scheme"],
										"down_payment"=> ($scheme["down_payment"]=="NULL" || $scheme["down_payment"]=='undefined' || $scheme["down_payment"]==NULL)?'':$scheme["down_payment"],
										"emi_amt"=> ($scheme["emi_amt"]=="NULL" || $scheme["emi_amt"]=='undefined' || $scheme["emi_amt"]==NULL)?'':$scheme["emi_amt"],
										"emi_duration"=> ($scheme["emi_duration"]=="NULL" || $scheme["emi_duration"]=='undefined' || $scheme["emi_duration"]==NULL)?'':$scheme["emi_duration"],
										"discount_amt"=> ($scheme["discount_amt"]=="NULL" || $scheme["discount_amt"]=='undefined' || $scheme["discount_amt"]==NULL)?'':$scheme["discount_amt"],
										"package_id"=>$packages_data->__get("package_name"),
										"duration"=> ($scheme["discount_month"]=="NULL" || $scheme["discount_month"]=='undefined' || $scheme["discount_month"]==NULL)?'':$scheme["discount_month"],
										"from_date"=> ($scheme["start_date"]=="NULL" || $scheme["start_date"]=='undefined' || $scheme["start_date"]==NULL)?'':$scheme["start_date"],
										"to_date"=>($scheme["end_date"]=="NULL" || $scheme["end_date"]=='undefined' || $scheme["end_date"]==NULL)?'':$scheme["end_date"],
										"package_continue"=> ($scheme["package_continue"]=="NULL" || $scheme["package_continue"]=='undefined' || $scheme["package_continue"]==NULL)?'':$scheme["package_continue"],
										"scheme_continue"=> ($scheme["scheme_continue"]=="NULL" || $scheme["scheme_continue"]=='undefined' || $scheme["scheme_continue"]==NULL)?'':$scheme["scheme_continue"],
										
										"advance_payment_for_month"=>($scheme["advance_payment"]=="NULL" || $scheme["advance_payment"]=='undefined' || $scheme["advance_payment"]==NULL)?'':$scheme["advance_payment"],
										"discount_for_month"=>($scheme["advance_dis_month"]=="NULL" || $scheme["advance_dis_month"]=='undefined' || $scheme["advance_dis_month"]==NULL)?'':$scheme["advance_dis_month"],
										"flat_discount"=>($scheme["flat_dis"]=="NULL" || $scheme["flat_dis"]=='undefined' || $scheme["flat_dis"]==NULL)?'':$scheme["flat_dis"],
										"percentage_discount"=>($scheme["per_dis"]=="NULL" || $scheme["per_dis"]=='undefined' || $scheme["per_dis"]==NULL)?'':$scheme["per_dis"],
										"equipment"=>$equip_name,
								);  
								$scheme_arr[]=$scheme_data;
							}
    					}
    				}
    				$mpppt_arr=array();
    				$mpppts=$mpptsMapper->getMpptsBySiteId($site["site_id"]);
    				if($mpppts){
    					foreach ($mpppts as $mpppt){
    						$scheme_data=array(
    								"mppt_id"=>$mpppt->__get("id"),
    								"mppt_name"=>$mpppt->__get("mppt_name"),
    								"mppt_keyword"=>$mpppt->__get("mppt_keyword"),
    								"description"=>$mpppt->__get("description"),
    						);
    						$mpppt_arr[]=$scheme_data;
    					}
    				}
					$charges_arr=array();
					$optCharges=$sitesMapper->getChargesbyID($site["otp_act_id"]);
					$meterCharges=$sitesMapper->getChargesbyID($site["otp_mtr_id"]);
					$charges=array(
							"label"=>"OTP Charges","amount"=>intval($optCharges[0]["charges"]),'imput_field_lable'=>'Amount Collected','id'=>1
					);
					$charges_arr[]=$charges;
					$charges=array(
							"label"=>"Meter Charges","amount"=>intval($meterCharges[0]["charges"]),'imput_field_lable'=>'Amount Collected','id'=>2
					);
					$charges_arr[]=$charges;
					$charges=array(
							"label"=>"Security Deposit","amount"=>0,'imput_field_lable'=>'Amount Collected','id'=>3
					);
					$charges_arr[]=$charges;
					$charges=array(
							"label"=>"Energy Charges","amount"=>0,'imput_field_lable'=>'Amount Collected','id'=>4
					);  
					$charges_arr[]=$charges;
					$charges=array(
							"label"=>"Other Charges","amount"=>0,'imput_field_lable'=>'Other Collected','id'=>5
					);  
					$charges_arr[]=$charges;
    				$site_data=array(
							"state_id"=>$site["state_id"],
    						"site_id"=>$site["site_id"],
    						"site_name"=>$site["site_name"]." (".$site["site_code"].")", 
    						"site_code"=>$site["site_code"],
    						"feeder"=>$MM_arr,
    						"mppt"=>$mpppt_arr,
    						"scheme"=>$scheme_arr,
							"charges"=>$charges_arr,
    						"otp_due"=>$otp_dues,
							"otp_charges"=>intval($optCharges[0]["charges"]),
							"meter_charges"=>intval($meterCharges[0]["charges"]),
							"helpLine_no"=>'0000-0000-0000'
    		
    				);
    				$site_arr[]=$site_data;  
    			}
    		}
    			
    		$consumer_type_arr=array();
    		$consumer_types=$consumersMapper->getConsumerType();
    		if($consumer_types){
    			foreach ($consumer_types as $consumer_type_main){
				
					$sub_consumer_type_arr=array();
					$sub_consumer_types=$consumersMapper->getConsumerType($consumer_type_main["consumer_type_id"]);
					if($sub_consumer_types){
						foreach ($sub_consumer_types as $consumer_type){
							$data_val=array(
									"consumer_type_id"=>$consumer_type["consumer_type_id"],
									"consumer_type_name"=>$consumer_type["consumer_type_name"],
									"consumer_type_code"=>$consumer_type["consumer_type_code"],
							);
							$sub_consumer_type_arr[]=$data_val;
						}
					}
			
    				$data_val=array(
    						"consumer_type_id"=>$consumer_type_main["consumer_type_id"],
    						"consumer_type_name"=>$consumer_type_main["consumer_type_name"],
    						"consumer_type_code"=>$consumer_type_main["consumer_type_code"],
							"customer_category"=>$sub_consumer_type_arr,
    				);
    				$consumer_type_arr[]=$data_val;
    			}
    		}
    			
    		$charges=array();
    		$charges_arr=$packagesMapper->getAllCharges();
    		if($charges_arr){
    			foreach ($charges_arr as $charge){
    				$data_val=array(
    						"charge_id"=>$charge["charge_id"],
    						"charge_name"=>$charge["charge_name"],
    						"charge_parameter"=>$charge["charge_parameter"],
    						"hindi_text"=> $charge["hindi_text"],
    						"charge_code"=>$charge["charge_code"],
    				);
    				$charges[]=$data_val;
    			}
    		}
    		$reasons_arr=array();
    		$reasons=$packagesMapper->getReasonRemark();
    		if($reasons){
    			foreach($reasons as $reason){
    				$reason_data=array(
    						"id"=>$reason["id"],
    						"reason"=>$reason["reason"],
    				);
    				$reasons_arr[]=	$reason_data;
    			}
    		}
    		/*$sub_consumer_type_arr=array();
    		$sub_consumer_types=$consumersMapper->getConsumerType(true);
    		if($sub_consumer_types){
    			foreach ($sub_consumer_types as $consumer_type){
    				$data_val=array(
    						"consumer_type_id"=>$consumer_type["consumer_type_id"],
    						"consumer_type_name"=>$consumer_type["consumer_type_name"],
    						"consumer_type_code"=>$consumer_type["consumer_type_code"],
    				);
    				$sub_consumer_type_arr[]=$data_val;
    			}
    		}*/
			
    		$data=array(
    				"state"=>$state_arr,
    				"site"=>$site_arr,
    				"customer_type"=>$consumer_type_arr,
					//"customer_category"=>$sub_consumer_type_arr,
    				"charges"=>$charges,
    				"reason_for_remark"=>$reasons_arr,
					"menu_permission"=>$menu_permission,
					"subMenu_permission"=>$subMenu_permission,
					"customer_service_no"=>'0000-0000-0000',
					"reporting_manager_no"=>$collectionAgents->__get("line_manager_no"),
					"bank_deposit_calendar"=>'7',
					"diesel_generator_calendar"=>'7',
					"plant_status_calendar"=>'30',
					"mppt_date_calendar"=>'30',
					"feeder_date_calendar"=>'30',
					"cashRegister_date_calendar"=>'30',
					"version_code"=>'4',
				);
    		$result="Data is loading successfully.";
    			 
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    
    		$appMapper=new Application_Model_AppDetailMapper();
    		$AppDetail=new Application_Model_AppDetail();
    		$AppDetail->__set("user_id", $user_id);
    		$AppDetail->__set("user_type", NULL);
    		$AppDetail->__set("device_token", $device_token);
    		$AppDetail->__set("device_type", $device_type);
    		$AppDetail->__set("timestamp", $timestamp);
    		$appDetails=$appMapper->addNewCollections($AppDetail);
    	}else{
    		$result = "Error. You are not an Agent.";
    	}
    		
    	$input=array('api'=>'App Init',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    
    	exit;
    }
	
	public function loginAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$mpptsMapper=new Application_Model_MpptsMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		$stateMapper=new Application_Model_StatesMapper();
    	$equipmentMapper=new Application_Model_EquipmentMapper();
		
		$request = $this->getRequest();
		date_default_timezone_set('Asia/Kolkata');
        $username=$request->getParam("username");
		$password=$request->getParam("password");
		$device_token=$request->getParam("device_token");
		$device_type=$request->getParam("device_type");
        $parameter=array(
			"device_token"=>$device_token,
			"device_type"=>$device_type,
			"username"=>$username,
			"password"=>$password,
		);
		 
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		 
		$files=array();	$success=0; 
		$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
		$collectionAgent=$collectionAgentsMapper->loginAgent(sha1(strtoupper($password)),$username);
                     								
		if($collectionAgent["collection_agent_id"] !=null || $collectionAgent["collection_agent_id"] !="")
		{
				$success=1;
		$permissions=explode(',', $collectionAgent["permission"]) ;
		$menu_permission=array(
				'dashboard'=>in_array("1", $permissions)?1:0,
				'cash_register'=>in_array("2", $permissions)?1:0,
				'customer'=>in_array("3", $permissions)?1:0,
				'plant'=>in_array("4", $permissions)?1:0); 
                     										
		$subMenu_permission=array(
				'add_customer'=>in_array("5", $permissions)?1:0,
				'add_meter_reading'=>in_array("6", $permissions)?1:0,
				'add_transaction'=>in_array("7", $permissions)?1:0,
				'customer_list'=>in_array("8", $permissions)?1:0,
				'package'=>in_array("9", $permissions)?1:0,
				'scheme'=>in_array("10", $permissions)?1:0,
				'feeder'=>in_array("11", $permissions)?1:0,
				'feeder_data'=>in_array("12", $permissions)?1:0,
				'plant_status'=>in_array("13", $permissions)?1:0,
				'mppts'=>in_array("14", $permissions)?1:0,
				'mppts_data'=>in_array("15", $permissions)?1:0,
				'diesel_generator'=>in_array("16", $permissions)?1:0,
				'add_instant_consumer'=>in_array("17", $permissions)?1:0,
				'cash_ledger'=>in_array("18", $permissions)?1:0,
				'temporary_disconnect'=>in_array("19", $permissions)?1:0,
				'change_feeder_meter'=>in_array("20", $permissions)?1:0,
				'bank_deposit'=>in_array("21", $permissions)?1:0,
				'edit_consumer_profile'=>in_array("22", $permissions)?1:0, 
				'consumer_activity'=>in_array("23", $permissions)?1:0,
				'change_consumer_meter'=>in_array("24", $permissions)?1:0,
				'unrelease_meter'=>in_array("25", $permissions)?1:0);
				  
																 
			$sites=$sitesMapper->getSitesByCollectionSite(explode(",",$collectionAgent["site_id"]));
            $site_array_val=array();
			foreach($sites as $site){
				$site_array_val[]=$site["site_id"];
			}         	 											 
             $state_arr=array();
    		$states=$stateMapper->getAllStates();
    		if($states){
    			foreach($states as $state){
					$state_data=array(
						"state_id"=>$state->__get("state_id"),
						"state_name"=>$state->__get("state_name"),
					);
					$state_arr[]=$state_data;	
    			}
    		}
			
			$site_arr=array();
    		if($sites){
    			foreach($sites as $site){
    				$consumer_arr=array();$MM_arr=array();$data=array();
    				 
    				$otp_dues=0;
    				$Otp_due=$cashRegisterMapper->getTotalOtpDueSiteWise($site["site_id"]);
    				if($Otp_due){
						$otp_dues=$Otp_due["Bill_ACT"]-$Otp_due["ACT"];
    				}
    				$siteMasters=$siteMasterMapper->getSiteMasterById($site["site_id"]);
    				if($siteMasters){ 
    					foreach($siteMasters as $siteMaster){
    						$siteMasterMeters=$siteMasterMapper->getMasterMeterReadingByMeterId($siteMaster->__get("id"));
    						if($siteMasterMeters){
    							$siteMasterMeter=$siteMasterMeters[0];
    							$last_reading=$siteMasterMeter->__get("reading");
    							$reading_on = $siteMasterMeter->__get("reading_date");
    						}else{
    							$last_reading=0;
    							$reading_on=0;
    						}
    						$packages=$packagesMapper->getPackagesByFeederId($siteMaster->__get("id"));
    						$packages_arr=array();
    						if($packages){
    							foreach($packages as $package){
    								$watt_total=0;
    								$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
    								if($watt){
    									$watt_total=$watt->__get("wattage");
    								}
    								$pack_data=array(
    										"package_id"=>$package["package_id"],
    										"package_name"=>$package["package_name"],
    										"package_cost"=>$package["package_cost"],
    										"is_postpaid"=>$package["is_postpaid"],
											
											"max_wattage_allowed"=>$watt_total,
    										"min_amount"=>$package["package_cost"],
    										"free_units"=>$package["unit"],
    										"rate_after_free_units"=>$package["extra_charges"],
											"min_unit_rate"=>$package["min_charges"],
    										"max_unit_rate"=>$package["max_charges"],
											
    										"wattage"=>$watt_total,
    										"status"=>$package["status"],
    								);
    								$packages_arr[]=$pack_data;
    							}
    						}
    							
    						$MM_detail=array(
    								"feeder_id"=>$siteMaster->__get("id"),
    								"meter_name"=>$siteMaster->__get("description"),
    								"last_reading" =>$last_reading,
    								"reading_on" =>$reading_on,
    								"meter_keyword"=>$siteMaster->__get("meter_keyword"),
    								"description" =>$siteMaster->__get("description"),
    								"start_time" =>$siteMaster->__get("start_time"),
    								"end_time"=>$siteMaster->__get("end_time"),
    								"is_24_hr"=>$siteMaster->__get("is_24_hr"),
    								"packages"=>$packages_arr
    						); 
    							
    							
    						$MM_arr[]=$MM_detail;
    					}
    				}
					//Equipment Name, Down Payment, Emi Amount , Duration, Package Name, Discount Amount & Validity
    				$scheme_arr=array();
    				$schems=$schemeMapper->getSchemeBySiteId($site["site_id"]);
    				if($schems){
    					foreach ($schems as $scheme){
						$packageMapper = new Application_Model_PackagesMapper();
					 	$packages_data=$packagesMapper->getPackageById($scheme["package_id"]);
    					 $start_date = $scheme["start_date"];
						 $end_date = $scheme["end_date"];
						 
						 $date = new Zend_Date();
						 $date->setTimezone("Asia/Kolkata");
						 $current_date = $date->toString("yyyy-MM-dd");
						 
							if(strtotime($current_date)>=strtotime($start_date) && strtotime($current_date)<=strtotime($end_date)){
								$equipment=($scheme["celamed_id"]=="NULL" || $scheme["celamed_id"]=='undefined' || $scheme["celamed_id"]==NULL)?0:$scheme["celamed_id"];
								$equipments=$equipmentMapper->getEquipmentById($equipment); 
								$equip_name="";
								if($equipments){
									$equip_name=$equipments->__get("name");
								}
								$scheme_data=array(
										"scheme_id"=>($scheme["scheme_id"]=="NULL" || $scheme["scheme_id"]=='undefined' || $scheme["scheme_id"]==NULL)?'':$scheme["scheme_id"],
										"scheme_name"=> ($scheme["scheme"]=="NULL" || $scheme["scheme"]=='undefined' || $scheme["scheme"]==NULL)?'':$scheme["scheme"],
										"down_payment"=> ($scheme["down_payment"]=="NULL" || $scheme["down_payment"]=='undefined' || $scheme["down_payment"]==NULL)?'':$scheme["down_payment"],
										"emi_amt"=> ($scheme["emi_amt"]=="NULL" || $scheme["emi_amt"]=='undefined' || $scheme["emi_amt"]==NULL)?'':$scheme["emi_amt"],
										"emi_duration"=> ($scheme["emi_duration"]=="NULL" || $scheme["emi_duration"]=='undefined' || $scheme["emi_duration"]==NULL)?'':$scheme["emi_duration"],
										"discount_amt"=> ($scheme["discount_amt"]=="NULL" || $scheme["discount_amt"]=='undefined' || $scheme["discount_amt"]==NULL)?'':$scheme["discount_amt"],
										"package_id"=>$packages_data->__get("package_name"),
										"duration"=> ($scheme["discount_month"]=="NULL" || $scheme["discount_month"]=='undefined' || $scheme["discount_month"]==NULL)?'':$scheme["discount_month"],
										"from_date"=> ($scheme["start_date"]=="NULL" || $scheme["start_date"]=='undefined' || $scheme["start_date"]==NULL)?'':$scheme["start_date"],
										"to_date"=>($scheme["end_date"]=="NULL" || $scheme["end_date"]=='undefined' || $scheme["end_date"]==NULL)?'':$scheme["end_date"],
										"package_continue"=> ($scheme["package_continue"]=="NULL" || $scheme["package_continue"]=='undefined' || $scheme["package_continue"]==NULL)?'':$scheme["package_continue"],
										"scheme_continue"=> ($scheme["scheme_continue"]=="NULL" || $scheme["scheme_continue"]=='undefined' || $scheme["scheme_continue"]==NULL)?'':$scheme["scheme_continue"],
										
										"advance_payment_for_month"=>($scheme["advance_payment"]=="NULL" || $scheme["advance_payment"]=='undefined' || $scheme["advance_payment"]==NULL)?'':$scheme["advance_payment"],
										"discount_for_month"=>($scheme["advance_dis_month"]=="NULL" || $scheme["advance_dis_month"]=='undefined' || $scheme["advance_dis_month"]==NULL)?'':$scheme["advance_dis_month"],
										"flat_discount"=>($scheme["flat_dis"]=="NULL" || $scheme["flat_dis"]=='undefined' || $scheme["flat_dis"]==NULL)?'':$scheme["flat_dis"],
										"percentage_discount"=>($scheme["per_dis"]=="NULL" || $scheme["per_dis"]=='undefined' || $scheme["per_dis"]==NULL)?'':$scheme["per_dis"],
										"equipment"=>$equip_name,
								);  
								$scheme_arr[]=$scheme_data; 
							}
    					}
    				}
    				$mpppt_arr=array();
    				$mpppts=$mpptsMapper->getMpptsBySiteId($site["site_id"]);
    				if($mpppts){
    					foreach ($mpppts as $mpppt){
    						$mppt_data=array(
    								"mppt_id"=>$mpppt->__get("id"),
    								"mppt_name"=>$mpppt->__get("mppt_name"),
    								"mppt_keyword"=>$mpppt->__get("mppt_keyword"),
    								"description"=>$mpppt->__get("description"),
    						);
    						$mpppt_arr[]=$mppt_data;
    					}
    				}
					$optCharges=$sitesMapper->getChargesbyID($site["otp_act_id"]);
					$meterCharges=$sitesMapper->getChargesbyID($site["otp_mtr_id"]);
					$charges_arr=array();
    				$charges=array(
							"label"=>"OTP Charges","amount"=>intval($optCharges[0]["charges"]),'imput_field_lable'=>'OTP Amount','id'=>1
					);
					$charges_arr[]=$charges;
					$charges=array(
							"label"=>"Meter Charges Collected","amount"=>intval($meterCharges[0]["charges"]),'imput_field_lable'=>'Meter Amount','id'=>2
					);
					$charges_arr[]=$charges;
					$charges=array(
							"label"=>"Security Amount Collected","amount"=>0,'imput_field_lable'=>'Security Amount','id'=>3
					);
					$charges_arr[]=$charges;
					$charges=array(
							"label"=>"Energy Charges","amount"=>0,'imput_field_lable'=>'Energy Amount','id'=>4
					);
					$charges_arr[]=$charges;
						$charges=array(
							"label"=>"Other Charges","amount"=>0,'imput_field_lable'=>'Other Collected','id'=>5
					);  
					$charges_arr[]=$charges; 
    				$site_data=array(
							"state_id"=>$site["state_id"],
    						"site_id"=>$site["site_id"],
    						"site_name"=>$site["site_name"]." (".$site["site_code"].")",
    						"site_code"=>$site["site_code"],
    						"feeder"=>$MM_arr,
    						"mppt"=>$mpppt_arr,
    						"scheme"=>$scheme_arr,
							"charges"=>$charges_arr,
    						"otp_due"=>$otp_dues,
							"otp_charges"=>intval($optCharges[0]["charges"]),
							"meter_charges"=>intval($meterCharges[0]["charges"]),
							"helpLine_no"=>'0000-0000-0000'
    		
    				);
    				$site_arr[]=$site_data;
    			}
    		}
                     											
            $consumer_type_arr=array();
    		$consumer_types=$consumersMapper->getConsumerType();
    		if($consumer_types){
    			foreach ($consumer_types as $consumer_type_main){
				
					$sub_consumer_type_arr=array();
					$sub_consumer_types=$consumersMapper->getConsumerType($consumer_type_main["consumer_type_id"]);
					if($sub_consumer_types){
						foreach ($sub_consumer_types as $consumer_type){
							$data_val=array(
									"consumer_type_id"=>$consumer_type["consumer_type_id"],
									"consumer_type_name"=>$consumer_type["consumer_type_name"],
									"consumer_type_code"=>$consumer_type["consumer_type_code"],
							);
							$sub_consumer_type_arr[]=$data_val;
						}
					}
			
    				$data_val=array(
    						"consumer_type_id"=>$consumer_type_main["consumer_type_id"],
    						"consumer_type_name"=>$consumer_type_main["consumer_type_name"],
    						"consumer_type_code"=>$consumer_type_main["consumer_type_code"],
							"customer_category"=>$sub_consumer_type_arr,
    				);
    				$consumer_type_arr[]=$data_val;
    			}
    		}
                     											
                     												$charges=array();
                     												$charges_arr=$packagesMapper->getAllCharges();
                     												if($charges_arr){
                     													foreach ($charges_arr as $charge){
                     														$data_val=array("charge_id"=>$charge["charge_id"],
                     																"charge_name"=>$charge["charge_name"],
                     																"charge_parameter"=>$charge["charge_parameter"],
                     																"hindi_text"=> $charge["hindi_text"],
																					"charge_code"=>$charge["charge_code"],
                     														);
                     														$charges[]=$data_val;
                     													}
                     												}
                     											
                     												$reasons_arr=array();	
																			$reasons=$packagesMapper->getReasonRemark();		
																				if($reasons){
																					foreach($reasons as $reason){
																						$reason_data=array(
																							"id"=>$reason["id"],
																							"reason"=>$reason["reason"],
																						);
																						$reasons_arr[]=	$reason_data;			
																					}
																				}
																													
			/*$sub_consumer_type_arr=array();
    		$sub_consumer_types=$consumersMapper->getConsumerType(true);
    		if($sub_consumer_types){
    			foreach ($sub_consumer_types as $consumer_type){
    				$data_val=array(
    						"consumer_type_id"=>$consumer_type["consumer_type_id"],
    						"consumer_type_name"=>$consumer_type["consumer_type_name"],
    						"consumer_type_code"=>$consumer_type["consumer_type_code"],
    				);
    				$sub_consumer_type_arr[]=$data_val;
    			}
    		}*/
				$data=array(
					"agent_id" => $collectionAgent["collection_agent_id"],
					"user_type"=>"csa", 
					"mobile" =>$collectionAgent["agent_mobile"],
					"agent_name" =>$collectionAgent["agent_fname"]." ".$collectionAgent["agent_lname"],
					"menu_permission"=>$menu_permission,
					"subMenu_permission"=>$subMenu_permission,
					"state"=>$state_arr,
					"site"=>$site_arr, 
					"customer_type"=>$consumer_type_arr,
					//"customer_category"=>$sub_consumer_type_arr,
					"charges"=>$charges,
					"reason_for_remark"=>$reasons_arr,
					"customer_service_no"=>'0000-0000-0000',
					"reporting_manager_no"=>$collectionAgent["line_manager_no"],
					"bank_deposit_calendar"=>'7',
					"diesel_generator_calendar"=>'7',
					"plant_status_calendar"=>'30',
					"mppt_date_calendar"=>'30',
					"feeder_date_calendar"=>'7',
					"version_code"=>'4',
				);  
					$result = "Login Successfully";
                     										
				$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
                							
				$appMapper=new Application_Model_AppDetailMapper();
				$AppDetail=new Application_Model_AppDetail();
				$AppDetail->__set("user_id", NULL);
				$AppDetail->__set("user_type", NULL);
				$AppDetail->__set("device_token", $device_token);
				$AppDetail->__set("device_type", $device_type);
				$AppDetail->__set("timestamp", $timestamp);
				$appDetails=$appMapper->addNewCollections($AppDetail);
			}else{
				$result="Incorrect Username or Password";
				$data=array();
			}        
							
			$input=array('api'=>'login',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		
	}
	
	public function dashboardAction(){
	
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		$request = $this->getRequest();
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$site_id=$request->getParam("site_id");
		$start_month=$request->getParam("start_month");
		$start_year=$request->getParam("start_year");
		$end_month=$request->getParam("end_month");
		$end_year=$request->getParam("end_year");
		 
		$parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"site_id"=>$site_id,
			"start_month"=>sprintf("%02d",$start_month),
			"start_year"=>$start_year,
			"end_month"=>sprintf("%02d",$end_month),
			"end_year"=>$end_year,
		);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		
		$files=array();	$success=0;
		$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
            									
		/*if($collectionAgents->__get("collection_agent_id")!=0){
			$sites=$sitesMapper->getSitesByCollectionSite(explode(",",$collectionAgents->__get("site_id")),$site_id);
		}else{
			$sites=$sitesMapper->getSitesByCollectionSite(NULL,$site_id);
		}*/
        $site_arr=array();    
		if($site_id!=NULL && $site_id!=""){
			 if(count(explode(",", $site_id))>1){
				$site_name="Selected Plants";
			 }else{
				$site = $siteMapper->getSiteById($site_id);
				$site_name=$site->__get("site_name");	
			}
			$site_arr=explode(",",$site_id);
		
		 }else{
			$site_name="All Plants";
			$site_arr=explode(",",$collectionAgents->__get("site_id"));
		 }
		
		
		/*$site_arr=array();
		if($sites){
			$success=1;
			foreach($sites as $site){
				$site_arr[]=$site["site_id"];
			}
		}*/
		
		$zendDate = new Zend_Date();
		$zendDate->setTimezone("Asia/Calcutta");
        $month = $zendDate->toString("MM");
        $year = intval($zendDate->toString("yyyy"));
					
		$start_date=NULL;$end_date=NULL;
		if(count($site_arr)>0){
		$success=1;
			if($start_month!=NULL && $start_year!=NULL){
				$start_date=$start_year."-".$start_month."-01";
			}else{
				$start_month=$month;
				$start_year=$year;
			}
			if($end_month!=NULL && $end_year!=NULL){
				$prepaid_key='Prepaid Billing';
				$postpaid_key='Postpaid Billing';
				$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);
				$end_date=$end_year."-".$end_month."-".$total_days_in_month; 
			}else{
				$prepaid_key='Prepaid Current Month';
				$postpaid_key='Postpaid Previous Month';
				$end_month=$month;
				$end_year=$year;
			} 
			$ytd_dues=0;
			$Bill_colls=$cashRegisterMapper->getOutstandingBySiteIdAndFilter($site_arr,$start_date,$end_date,$user_id);
			if($Bill_colls){
				$ytd_dues=$cashRegisterMapper->getOutstandingDataByConsumerId($site_arr);
				$pre_dues=$cashRegisterMapper->getOutstandingDataByConsumerId($site_arr,null,null,'0'); 
				$post_dues=$cashRegisterMapper->getOutstandingDataByConsumerId($site_arr,null,null,'1');
	        }
			  
			 $otherCollection=intval($Bill_colls["CREDIT"])-intval($Bill_colls["My_CREDIT"]);
			 $dateObj   = DateTime::createFromFormat('!m', sprintf("%02d",$start_month));
			 $startmonthName = $dateObj->format('M'); 
				
			 $dateObj   = DateTime::createFromFormat('!m', sprintf("%02d",$end_month));
			 $endmonthName = $dateObj->format('M'); 
			$data=array(
				"site"=> $site_name,
				"total_billed"=> $this->formatMoney(intval($Bill_colls["DEBIT"]+$Bill_colls["Pre_DEBIT_SD"]+$Bill_colls["MTR_DEBIT_SD"]),false), 
				"total_collection"=> $this->formatMoney(intval($Bill_colls["CREDIT"]),false),
				"my_collection"=> $this->formatMoney(intval($Bill_colls["My_CREDIT"]),false),
				"other_collection"=>$this->formatMoney(intval($otherCollection),false),
				"Prepaid_bill"=> $this->formatMoney(intval($Bill_colls["Pre_DEBIT"]+$Bill_colls["Pre_DEBIT_SD"]),false),
				"postpaid_bill"=> $this->formatMoney(intval($Bill_colls["MTR_DEBIT"]+$Bill_colls["MTR_DEBIT_SD"]),false),
				"ytd_outstanding"=> $this->formatMoney(abs($ytd_dues),false) ,
				"ytd_prepaid_outstanding"=> $this->formatMoney(abs($pre_dues),false) ,
				"ytd_postpaid_outstanding"=> $this->formatMoney(abs($post_dues),false) ,
				"start_month"=>$startmonthName,
				"start_year"=>$start_year,
				"end_month"=>$endmonthName,
				"end_year"=>$end_year,
				"prepaid_key"=>$prepaid_key,
				"postpaid_key"=>$postpaid_key 
			);
			$result="Data is loading successfully.";
		}else{
			$data=array();
			$result="You do not have this site permission.";
		}
		
			$input=array('api'=>'dashboard',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		
	}
	
	public function formatMoney($num) {
		$explrestunits = "" ;
		if(strlen($num)>3) {
			$lastthree = substr($num, strlen($num)-3, strlen($num));
			$restunits = substr($num, 0, strlen($num)-3); 
			$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);
			for($i=0; $i<sizeof($expunit); $i++) {  
				// creates each of the 2's group and adds a comma to the end
				if($i==0) {
					$explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
				} else {
					$explrestunits .= $expunit[$i].",";
				}
			}
			$thecash = $explrestunits.$lastthree;
		} else {
			$thecash = $num;
		}
		return $thecash; // writes the final format where $currency is the currency symbol.
	}

    public function consumerListAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		$request = $this->getRequest();
		$site_id=$request->getParam("site_id");
		$skip_count=$request->getParam("skip_count");
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$filter_type=$request->getParam("filter_type"); 
		
		$parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"site_id"=>$site_id,
			"skip_count"=>$skip_count,
		);
		 
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		 
		$files=array();	$success=0;     											
        $collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
                     										
        if($site_id!=NULL){
			$success=1;
			$sites=$sitesMapper->getSiteById($site_id);
			$site_name=$sites->__get("site_name");
			$site_id_arr=array();
			 
			$site_id_arr[]=$site_id;
				 
		}elseif($collectionAgents->__get("collection_agent_id")!=0){
			$sites=$sitesMapper->getSitesByCollectionSite(explode(",",$collectionAgents->__get("site_id")));
			$site_name="All Plants";
			$site_id_arr=array();
			if($sites){
				$success=1;
				foreach($sites as $site){
					$site_id_arr[]=$site["site_id"];
				}
			}
		}else{
			$sites=$sitesMapper->getSitesByCollectionSite();
			$site_name="All Plants";
			$site_id_arr=array();
			if($sites){
				$success=1;
				foreach($sites as $site){
					$site_id_arr[]=$site["site_id"];
				}
			}
		}
             											 
        $consumer_arr=array();$MM_arr=array();$total_consumer=0;
        $consumers=$consumersMapper->getConsumersBySiteAndFilter($site_id_arr,null,$skip_count,$filter_type); 
		if($consumers){
			foreach ($consumers as $consumer){
                     															 
				 $package_name_arr=array();
			 													 
				$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer->__get("consumer_id"));
				$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
    						$package_arr=array();
							$is_post='0';
    						if($consumerpackage){
    							foreach($consumerpackage as $package){
    								$watt_total=0;
    								$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
    								if($watt){
    									$watt_total=$watt->__get("wattage");
    								}
    								if($package["is_postpaid"]==0){
    									$reading=0;
										$letest_meter_reading_update=0;
										$meter_no=0;
    								}else{ 
										$is_post=$package["is_postpaid"];
    									$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    									$latestReading=$meterReadingMapper->getLatestReadingByConsumerId($consumer->__get("consumer_id"),$package["package_id"]);
    									if($latestReading){ 
    										$reading=floatval($latestReading->__get("meter_reading"));
											$letest_meter_reading_update=date("d-M-Y g:i a",strtotime($latestReading->__get("timestamp")));
											$meter_no=$latestReading->__get("meter_no");
    									}else{$reading=0;
											$letest_meter_reading_update=0;
											$meter_no=0; 
										}
    								}
									$feederName=$siteMasterMapper->getMMById($consumer->__get("site_meter_id"));
									$feeder_names='';$feeder_id='';
									if($feederName){
										$feeder_names=$feederName->__get('meter_name')." (".$feederName->__get('description').")"." (".$feederName->__get('feeder_type').")";
										$feeder_id=$consumer->__get("site_meter_id");
									}
    								$pack_data=array(
    										"package_id"=>$package["package_id"],
    										"package_name"=>$package["package_name"],
    										"package_cost"=>$package["package_cost"],
    										"is_postpaid"=>$package["is_postpaid"],
											
											"max_wattage_allowed"=>$watt_total,
    										"min_amount"=>$package["package_cost"],
    										"free_units"=>$package["unit"],
    										"rate_after_free_units"=>$package["extra_charges"],
											"min_unit_rate"=>$package["min_charges"],
    										"max_unit_rate"=>$package["max_charges"],
											
    										"wattage"=>$watt_total,
    										"status"=>$package["status"],
    										"meter_reading"=>$reading,
											
											"updated_date"=>$letest_meter_reading_update,
											"meter_no"=>$meter_no,
											"feeder_names"=>$feeder_names,
											"feeder_id"=>$feeder_id,  
    								);
									
									$package_name_arr[]=$package["package_name"];
    								$package_arr[]=$pack_data;
    							}
    						}
						$data = array(
							'consumer_id' => $consumer->__get("consumer_id"),
							'consumer_code' => $consumer->__get("consumer_code"),
							"dob"=>$consumer->__get("dob"),
							'site_id' => $consumer->__get("site_id"),
							'consumer_name' =>strtoupper($consumer->__get("consumer_name")),
							'consumer_father_name' => $consumer->__get("consumer_father_name"),
							'email_id' => $consumer->__get("email_id"),
							'consumer_status' => $consumer->__get("consumer_status"), 
							'consumer_act_date' => date("d-M-Y",strtotime($consumer->__get("consumer_act_date"))),
							'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
							'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
							'type_of_me' => $consumer->__get("type_of_me"),
							'feeder_id' => $consumer->__get("site_meter_id"),
							'package_name'=>implode(',',$package_name_arr),
							"oustanding"=>$oustanding,
							"latest_meter_reading"=>$reading, 
							"updated_date"=>$letest_meter_reading_update,
							"meter_no"=>$meter_no,
							"is_postpaid"=>$is_post,
							"packages"=>$package_arr
							
						);
						$consumer_arr[]=$data;
			}
		}
        $consumers_count=$consumersMapper->getConsumersCountBySiteAndFilter($site_id_arr,null,null,$filter_type);             												 
		
			$data=array(
				"consumers"=>$consumer_arr,
				"Site_name"=>$site_name,
				"total_consumer"=>$consumers_count
			);
			
			$result="Data is loading successfully.";
                     											 
			$input=array('api'=>'consumer_list',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function consumerSearchListAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		$request = $this->getRequest();
		$site_id=$request->getParam("site_id");
		$skip_count=$request->getParam("skip_count");
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$data_value=$request->getParam("search_data"); 
		$filter_type=$request->getParam("filter_type"); 
		$parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"site_id"=>$site_id,
			"skip_count"=>$skip_count,
		);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		 
		$files=array();	$success=0;     											
        $collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
                     										
        if($site_id!=NULL){
			$success=1;
			$sites=$sitesMapper->getSiteById($site_id);
			$site_name=$sites->__get("site_name");
			$site_id_arr=array();
			 
			$site_id_arr[]=$site_id;
				 
		}elseif($collectionAgents->__get("collection_agent_id")!=0){
			$sites=$sitesMapper->getSitesByCollectionSite(explode(",",$collectionAgents->__get("site_id")));
			$site_name="All Plants";
			$site_id_arr=array();
			if($sites){
				$success=1;
				foreach($sites as $site){
					$site_id_arr[]=$site["site_id"];
				}
			}
		}else{
			$sites=$sitesMapper->getSitesByCollectionSite();
			$site_name="All Plants";
			$site_id_arr=array();
			if($sites){
				$success=1;
				foreach($sites as $site){
					$site_id_arr[]=$site["site_id"];
				}
			}
		}
        if($data_value!=NULL && $data_value!='' && $data_value!='undefined'){ 									 
        $consumer_arr=array();$MM_arr=array();$total_consumer=0;
        $consumers=$consumersMapper->getConsumerDetailBySearch($data_value,$site_id_arr,true,$filter_type); 
		
			if($consumers){
				$total_consumer=count($consumers);
				foreach ($consumers as $consumer){
																					 
					 $package_name_arr=array();
																	 
					$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer["consumer_id"]);
					$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer["consumer_id"]);
								$package_arr=array();
								$is_post='0';
								if($consumerpackage){
									foreach($consumerpackage as $package){
										$watt_total=0;
										$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
										if($watt){
											$watt_total=$watt->__get("wattage");
										}
										if($package["is_postpaid"]==0){
											$reading="";
											$letest_meter_reading_update="";
											$meter_no="";
										}else{ 
											$is_post=$package["is_postpaid"];
											$meterReadingMapper=new Application_Model_MeterReadingsMapper();
											$latestReading=$meterReadingMapper->getLatestReadingByConsumerId($consumer["consumer_id"],$package["package_id"]);
											if($latestReading){
												$reading=$latestReading->__get("meter_reading");
												$letest_meter_reading_update=date("d-M-Y g:i a",strtotime($latestReading->__get("timestamp")));
												$meter_no=$latestReading->__get("meter_no");
											}else{$reading="";
												$letest_meter_reading_update="";
												$meter_no=""; 
											}
										}
										$feederName=$siteMasterMapper->getMMById($consumer["site_meter_id"]);
										$feeder_names='';$feeder_id='';
										if($feederName){
											$feeder_names=$feederName->__get('meter_name')." (".$feederName->__get('description').")"." (".$feederName->__get('feeder_type').")";
											$feeder_id=$consumer["site_meter_id"];
										}
										$pack_data=array(
												"package_id"=>$package["package_id"],
												"package_name"=>$package["package_name"],
												"package_cost"=>$package["package_cost"],
												"is_postpaid"=>$package["is_postpaid"],
												
												"max_wattage_allowed"=>$watt_total,
												"min_amount"=>$package["package_cost"],
												"free_units"=>$package["unit"],
												"rate_after_free_units"=>$package["extra_charges"],
												"min_unit_rate"=>$package["min_charges"],
												"max_unit_rate"=>$package["max_charges"],
												
												"wattage"=>$watt_total,
												"status"=>$package["status"],
												"meter_reading"=>$reading,
												
												"updated_date"=>$letest_meter_reading_update,
												"meter_no"=>$meter_no,
												"feeder_names"=>$feeder_names,
												"feeder_id"=>$feeder_id,  
										);
										
										$package_name_arr[]=$package["package_name"];
										$package_arr[]=$pack_data;
									}
								} 
							$data = array(
								'consumer_id' => $consumer["consumer_id"],
								'consumer_code' => $consumer["consumer_code"],
								'site_id' => $consumer["site_id"],
								'consumer_name' =>strtoupper($consumer["consumer_name"]),
								'consumer_father_name' => $consumer["consumer_father_name"],
								'email_id' => $consumer["email_id"], 
								'consumer_status' => $consumer["consumer_status"],
								'consumer_act_date' =>date("d-M-Y",strtotime($consumer["consumer_act_date"])),
								'consumer_act_charge' => $consumer["consumer_act_charge"],
								'consumer_connection_id' => $consumer["consumer_connection_id"],
								'type_of_me' => $consumer["type_of_me"],
								'feeder_id' => $consumer["site_meter_id"],
								'package_name'=>implode(',',$package_name_arr),
								"oustanding"=>$oustanding,
								"latest_meter_reading"=>$reading, 
								"updated_date"=>$letest_meter_reading_update,
								"meter_no"=>$meter_no,
								"is_postpaid"=>$is_post,
								"packages"=>$package_arr
								
							);
							$consumer_arr[]=$data;
							$result="Data is loading successfully.";
				}
			}else{ 
					$success=0;
					$consumer_arr=array();
					$result="No detail found.";
			}  
        }else{
			$success=0;
			$consumer_arr=array();
			$result="No detail found.";
		}  												 
		$data=array(
				"consumers"=>$consumer_arr,
				"Site_name"=>$site_name,
				"total_consumer"=>$total_consumer
			);
		
                     											 
           $input=array('api'=>'consumer_search_list',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function homeDataAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$mpptsMapper=new Application_Model_MpptsMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		$request = $this->getRequest();
		$user_id=$request->getParam("user_id");
        $user_type=$request->getParam("user_type");
                     												
		
		$parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
		);
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$files=array();	$success=0;     											
        	date_default_timezone_set('Asia/Kolkata');
                     												 
                     												$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
                     											
                     												if($collectionAgents->__get("collection_agent_id")!=0){
                     													$sites=$sitesMapper->getSitesByCollectionSite(explode(",",$collectionAgents->__get("site_id")));
                     												}else{
                     													$sites=$sitesMapper->getSitesByCollectionSite();
                     												}
                     												 
                     												$site_arr=array();
                     												if($sites){
                     													$success=1;
                     													foreach($sites as $site){
                     											
                     														$consumer_arr=array();$MM_arr=array();
                     														$consumers=$consumersMapper->getConsumersByColumnValue("site_id", $site["site_id"],true);
                     														if($consumers){
                     															foreach ($consumers as $consumer){
                     																 
                     																$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
                     																$package_arr=array();
                     																if($consumerpackage){
                     											
                     																	foreach($consumerpackage as $package){
                     																		$watt_total=0;
                     																		$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
                     																		if($watt){
                     																			$watt_total=$watt->__get("wattage");
                     																			 
                     																		}
                     																		if($package["is_postpaid"]==0){
																								$reading="";
																							}else{
																								$meterReadingMapper=new Application_Model_MeterReadingsMapper();
																								$latestReading=$meterReadingMapper->getLatestReadingByConsumerId($consumer->__get("consumer_id"),$package["package_id"]);
																								if($latestReading){
																									$reading=$latestReading->__get("meter_reading");
																								}else{$reading="";}
																							}
																							$pack_data=array(
																									"package_id"=>$package["package_id"],
																									"package_name"=>$package["package_name"],
																									"package_cost"=>$package["package_cost"],
																									"is_postpaid"=>$package["is_postpaid"],
																									
																									"max_wattage_allowed"=>$watt_total,
																									"min_amount"=>$package["package_cost"],
																									"free_units"=>$package["unit"],
																									"rate_after_free_units"=>$package["extra_charges"],
																									"min_unit_rate"=>$package["min_charges"],
																									"max_unit_rate"=>$package["max_charges"],
																									
																									"wattage"=>$watt_total,
																									"status"=>$package["status"],
																									"meter_reading"=>$reading
																							);
                     																		$package_arr[]=$pack_data;
                     																	}
                     																}
                     																 
                     																$data = array(
                     																		'consumer_id' => $consumer->__get("consumer_id"),
                     																		'consumer_code' => $consumer->__get("consumer_code"),
                     																		'consumer_name' =>strtoupper($consumer->__get("consumer_name")),
                     																		'consumer_father_name' => $consumer->__get("consumer_father_name"),
                     																		'consumer_status' => $consumer->__get("consumer_status"),
                     																		'consumer_act_date' => $consumer->__get("consumer_act_date"),
                     																		'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
                     																		'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
                     																		'type_of_me' => $consumer->__get("type_of_me"),
																							'feeder_id' => $consumer->__get("site_meter_id"),
                     																		"packages"=>$package_arr
                     																);
                     																$consumer_arr[]=$data;
                     															}
                     														}
                     														$otp_dues=0;
                     														$Otp_due=$cashRegisterMapper->getTotalOtpDueSiteWise($site["site_id"]);
                     														if($Otp_due){
                     															$otp_dues=$Otp_due["Bill_ACT"]-$Otp_due["ACT"];
                     														}
                     														$siteMasters=$siteMasterMapper->getSiteMasterById($site["site_id"]);
                     														if($siteMasters){
                     															foreach($siteMasters as $siteMaster){
                     																$siteMasterMeters=$siteMasterMapper->getMasterMeterReadingByMeterId($siteMaster->__get("id"));
                     																if($siteMasterMeters){
                     																	$siteMasterMeter=$siteMasterMeters[0];
                     																	$last_reading=$siteMasterMeter->__get("reading");
                     																	$reading_on = $siteMasterMeter->__get("reading_date");
                     																}else{
                     																	$last_reading=0;
                     																	$reading_on=0;
                     																}
																					
																						$packages=$packagesMapper->getPackagesByFeederId($siteMaster->__get("id"));
																						$packages_arr=array();
																						if($packages){
																							foreach($packages as $package){
																								$watt_total=0;
																								$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
																								if($watt){
																									$watt_total=$watt->__get("wattage");
																								}
																								$pack_data=array(
																									"package_id"=>$package["package_id"],
																									"package_name"=>$package["package_name"],
																									"package_cost"=>$package["package_cost"],
																									"is_postpaid"=>$package["is_postpaid"],
																									
																									"max_wattage_allowed"=>$watt_total,
																									"min_amount"=>$package["package_cost"],
																									"free_units"=>$package["unit"],
																									"rate_after_free_units"=>$package["extra_charges"],
																									"min_unit_rate"=>$package["min_charges"],
																									"max_unit_rate"=>$package["max_charges"],
																									
																									"wattage"=>$watt_total,
																									"status"=>$package["status"],
																								);
																								$packages_arr[]=$pack_data;
																							}
																						}
																						
                     																$MM_detail=array(
                     																		"meter_id"=>$siteMaster->__get("id"),
                     																		"meter_name"=>$siteMaster->__get("meter_name"),
                     																		"last_reading" =>$last_reading,
                     																		"reading_on" =>$reading_on,
                     																		"meter_keyword"=>$siteMaster->__get("meter_keyword"),
                     																		"description" =>$siteMaster->__get("description"),
                     																		"start_time" =>$siteMaster->__get("start_time"),
                     																		"end_time"=>$siteMaster->__get("end_time"),
                     																		"is_24_hr"=>$siteMaster->__get("is_24_hr"),
																							"packages"=>$packages_arr
                     																);
                     																$MM_arr[]=$MM_detail;
                     															}
                     														}
                     														$scheme_arr=array();
                     														$schems=$schemeMapper->getSchemeBySiteId($site["site_id"]);
                     														if($schems){
                     															foreach ($schems as $scheme){
                     																$scheme_data=array(
                     																		"scheme_id"=>$scheme["scheme_id"],
                     																		"scheme_name"=>$scheme["scheme"],
                     																		"package_continue"=>$scheme["package_continue"],
																							"scheme_continue"=>$scheme["scheme_continue"],
                     																);
                     																$scheme_arr[]=$scheme_data;                  													
																				}
                     														}
                     														 
                     														$site_data=array(
                     																"site_id"=>$site["site_id"],
                     																"site_name"=>$site["site_name"],
                     																"site_code"=>$site["site_code"],
                     																"consumers"=>$consumer_arr,
                     																"feeder"=>$MM_arr,
                     																"scheme"=>$scheme_arr,
                     																"otp_due"=>$otp_dues
                     														);
                     														$site_arr[]=$site_data;
                     													}
                     												}
                     											
                     												$consumer_type_arr=array();
                     												$consumer_types=$consumersMapper->getConsumerType();
                     												if($consumer_types){
                     													foreach ($consumer_types as $consumer_type){
                     														$data_val=array("consumer_type_id"=>$consumer_type["consumer_type_id"],
                     																"consumer_type_name"=>$consumer_type["consumer_type_name"],
                     																"consumer_type_code"=>$consumer_type["consumer_type_code"],
                     																 
                     														);
                     														$consumer_type_arr[]=$data_val;
                     													}
                     												}
                     											
                     												$charges=array();
                     												$charges_arr=$packagesMapper->getAllCharges();
                     												if($charges_arr){
                     													foreach ($charges_arr as $charge){
                     														$data_val=array("charge_id"=>$charge["charge_id"],
                     																"charge_name"=>$charge["charge_name"],
                     																"charge_parameter"=>$charge["charge_parameter"],
                     																"hindi_text"=> $charge["hindi_text"],
																					"charge_code"=>$charge["charge_code"],
                     														);
                     														$charges[]=$data_val;
                     													}
                     												}
                     											
                     												$reasons_arr=array();	
																			$reasons=$packagesMapper->getReasonRemark();		
																				if($reasons){
																					foreach($reasons as $reason){
																						$reason_data=array(
																							"id"=>$reason["id"],
																							"reason"=>$reason["reason"],
																						);
																						$reasons_arr[]=	$reason_data;			
																					}
																				}
																													
																	$data=array(
																		"site"=>$site_arr,
																		"customer_type"=>$consumer_type_arr,
																		"charges"=>$charges,
																		"reason_for_remark"=>$reasons_arr,
																	);
                     												$result="Data is loading successfully.";
                     												 
                											 
           $input=array('api'=>'home_data',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function consumerDetailAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    	$equipmentMapper=new Application_Model_EquipmentMapper();
		$stateMapper=new Application_Model_StatesMapper();
		
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$consumer_id=$request->getParam("consumer_id");
    
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"consumer_id"=>$consumer_id,
    	);
    		
    	$files=array();	$success=0;
    	$total_reading=0;$total_amt=0;$unit_rate=0;
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$consumer_arr=array();
    	$consumer = $consumersMapper->getConsumerById($consumer_id);
    	if($consumer){
    		$success=1;
    		$transaction_historys=array();
    		$consumer_mobile=$consumer->__get("consumer_code");
    		$consumer_connection_id=$consumer->__get("consumer_connection_id");
    		$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
    		$package_arr=array();
    		if($consumerpackage){
    
    			foreach($consumerpackage as $package){
    				$watt_total=0;
    				$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
    				if($watt){
    					$watt_total=$watt->__get("wattage");
    				}
    				if($package["is_postpaid"]==0){
    					$reading="";
    				}else{
    					$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    					$latestReading=$meterReadingMapper->getLatestReadingByConsumerId($consumer_id,$package["package_id"]);
    					if($latestReading){
    						$reading=$latestReading->__get("meter_reading");
							$meter_no=$latestReading->__get("meter_no");
    					}else{$reading="";$meter_no="";}
    				}
    				$pack_data=array(
    						"package_id"=>$package["package_id"],
    						"package_name"=>$package["package_name"],
    						"package_cost"=>$package["package_cost"],
    						"is_postpaid"=>$package["is_postpaid"],
    							
    						"max_wattage_allowed"=>$watt_total,
    						"min_amount"=>$package["package_cost"],
    						"free_units"=>$package["unit"],
    						"rate_after_free_units"=>$package["extra_charges"],
    						"unit_rate"=>$package["extra_charges"],
    						"min_unit_rate"=>$package["min_charges"],
    						"max_unit_rate"=>$package["max_charges"],
    							
    						"wattage"=>$watt_total,
    						"status"=>$package["status"],
    						"meter_reading"=>$reading,
							"meterNumber"=>$meter_no 
    				);
    				$package_arr[]=$pack_data;
    			}
    		}
    		$type_of_me=$consumer->__get("type_of_me");
    		$consumer_type_vals=$consumersMapper->getConsumerTypeById($type_of_me);
    		$consumer_types=NULL;
    		if($consumer_type_vals){
    			$consumer_types=$consumer_type_vals["consumer_type_name"];
    		}
			$sub_type_of_me=$consumer->__get("sub_type_of_me");
    		$sub_type_of_me_vals=$consumersMapper->getConsumerTypeById($sub_type_of_me);
    		$sub_consumer_types=NULL;
    		if($sub_type_of_me_vals){
    			$sub_consumer_types=$sub_type_of_me_vals["consumer_type_name"];
    		}
    		$cashRe=$cashRegisterMapper->calculateActivationAmount('ACTIVATION',$consumer->__get("consumer_id"));
    		$actiCharges=$cashRe;
    		if($actiCharges==false){$actiCharges=0;}
    		 
    		//$outstanding=$consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
    		$activation= $consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
    		$sites_values=$siteMapper->getSiteById($consumer->__get("site_id"));
			$states=$stateMapper->getStateById($sites_values->__get("state_id"));
    		$meter_name=$siteMasterMapper->getMMById($consumer->__get("site_meter_id"));
    		$MM="";
    		if($meter_name){
    			$MM=$meter_name->__get("meter_name")." (".$meter_name->__get("description").")"; 
    		}
    		$total=$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer->__get("consumer_id")); //$outstanding+$activation;
    		if($consumer->__get("mobile_no")==NULL || $consumer->__get("mobile_no")==""){
    			$non_primary=array();
    		}else{
    			$non_primary=explode(",",$consumer->__get("mobile_no"));
    		}
    		$mob=array(
    				"mobile_no"=>$consumer->__get("consumer_code"),
    				"is_primary"=>1
    		);
    		$mob_total[]=$mob;
    			
    		if(count($non_primary)>0){
    			for($k=0;$k<count($non_primary);$k++){
    				$mob=array(
    						"mobile_no"=>$non_primary[$k],
    						"is_primary"=>0
    				);
    				$mob_total[]=$mob;
    			}
    		}
			$con_scheme_arr=array();
    		$con_schems=$consumerSchemeMapper->getConsumerSchemeDetailByConsumerId($consumer->__get("consumer_id"));
    
    		if($con_schems){
    			foreach ($con_schems as $con_schem){
    
    				$schemes=$schemeMapper->getSchemeById($con_schem["consumer_scheme"]);
    
    				$equipment=($schemes->__get("celamed_id")=="NULL" || $schemes->__get("celamed_id")=='undefined' || $schemes->__get("celamed_id")==NULL)?'':$schemes->__get("celamed_id");
								$equipments=$equipmentMapper->getEquipmentById($equipment); 
								$equip_name="";
								if($equipments){
									$equip_name=$equipments->__get("name");
								} 
    				$scheme_data=array(
						"scheme_id"=>($schemes->__get("scheme_id")=="NULL" || $schemes->__get("scheme_id")=='undefined' || $schemes->__get("scheme_id")==NULL)?'':$schemes->__get("scheme_id"),
						"scheme_name"=> ($schemes->__get("scheme")=="NULL" || $schemes->__get("scheme")=='undefined' || $schemes->__get("scheme")==NULL)?'':$schemes->__get("scheme"),
						"down_payment"=> ($schemes->__get("down_payment")=="NULL" || $schemes->__get("down_payment")=='undefined' || $schemes->__get("down_payment")==NULL)?'':$schemes->__get("down_payment"),
						"emi_amt"=> ($schemes->__get("emi_amt")=="NULL" || $schemes->__get("emi_amt")=='undefined' || $schemes->__get("emi_amt")==NULL)?'':$schemes->__get("emi_amt"),
						"emi_duration"=> ($schemes->__get("emi_duration")=="NULL" || $schemes->__get("emi_duration")=='undefined' || $schemes->__get("emi_duration")==NULL)?'':$schemes->__get("emi_duration"),
						"discount_amt"=> ($schemes->__get("discount_amt")=="NULL" || $schemes->__get("discount_amt")=='undefined' || $schemes->__get("discount_amt")==NULL)?'':$schemes->__get("discount_amt"),
						"duration"=> ($schemes->__get("discount_month")=="NULL" || $schemes->__get("discount_month")=='undefined' || $schemes->__get("discount_month")==NULL)?'':$schemes->__get("discount_month"),
						"from_date"=> ($schemes->__get("start_date")=="NULL" || $schemes->__get("start_date")=='undefined' || $schemes->__get("start_date")==NULL)?'':date("d-M-Y",strtotime($schemes->__get("start_date"))),
						"to_date"=>($schemes->__get("end_date")=="NULL" || $schemes->__get("end_date")=='undefined' || $schemes->__get("end_date")==NULL)?'':date("d-M-Y",strtotime($schemes->__get("end_date"))),
						"package_continue"=> ($con_schem["package_continue"]=="NULL" || $con_schem["package_continue"]=='undefined' || $con_schem["package_continue"]==NULL)?'':$con_schem["package_continue"],
						"scheme_continue"=> ($con_schem["scheme_continue"]=="NULL" || $con_schem["scheme_continue"]=='undefined' || $con_schem["scheme_continue"]==NULL)?'':$con_schem["scheme_continue"],
										
						"advance_payment_for_month"=>($schemes->__get("advance_payment")=="NULL" || $schemes->__get("advance_payment")=='undefined' || $schemes->__get("advance_payment")==NULL)?'':$schemes->__get("advance_payment"),
						"discount_for_month"=>($schemes->__get("advance_dis_month")=="NULL" || $schemes->__get("advance_dis_month")=='undefined' || $schemes->__get("advance_dis_month")==NULL)?'':$schemes->__get("advance_dis_month"),
						"flat_discount"=>($schemes->__get("flat_dis")=="NULL" || $schemes->__get("flat_dis")=='undefined' || $schemes->__get("flat_dis")==NULL)?'':$schemes->__get("flat_dis"),
						"percentage_discount"=>($schemes->__get("per_dis")=="NULL" || $schemes->__get("per_dis")=='undefined' || $schemes->__get("per_dis")==NULL)?'':$schemes->__get("per_dis"),
						"equipment"=>$equip_name,
					);
    				$con_scheme_arr[]=$scheme_data;
    			}  
    		} 
    		$consumer_details=array(
    				"plant"=>$sites_values->__get("site_name"),
    				"feeder_name"=>$MM,
    				"consumer_name"=>$consumer->__get("consumer_name"),
					"dob"=>$consumer->__get("dob"),
    				"father_name"=>$consumer->__get("consumer_father_name"),
					'consumer_status' => $consumer->__get("consumer_status"), 
    				"act_date"=>date("d-M-Y",strtotime($consumer->__get("consumer_act_date"))),
    				"email_id"=>$consumer->__get("email_id"), 
					"primary_mob_no"=>$consumer->__get("consumer_code"),
					"state_name"=>$states->__get("state_name"),
    				"outstanding"=>$total,
    				"otp_due"=>$activation,
    				"package"=>$package_arr,
    				"mobile"=>$mob_total, 
					"scheme"=>$con_scheme_arr,
    		);
    		$casRegisters=$cashRegisterMapper->getCashRegisterDetailsByConsumerId($consumer->__get("consumer_id"),true);
			
    		if($casRegisters){
    			foreach ($casRegisters as $casRegister){
					$receipts_no=$cashRegisterMapper->getCashRegisterTransactionByReceiptNo($consumer->__get("consumer_id"),$casRegister->__get("receipt_number"),$casRegister->__get("user_id"));
    				 
					 if($receipts_no){
						$entry_type=array();$amount_type=array();$hindi=array();$charges_arr=array();$total_amount=0;$cr_entry_status=array();
						$reciept_no=$sites_values->__get("site_code").$casRegister->__get("user_id").$casRegister->__get("receipt_number");
						$receipt_count=$casRegister->__get("receipt_count");
						$user_id=$casRegister->__get("user_id");
						foreach($receipts_no as $receipt_no){
						
							$entry_type[]=($receipt_no["entry_status"]=='CHG'?'ENERGY':$receipt_no["entry_status"]);
							$amount_type[]=$receipt_no["cr_amount"];
							$cr_entry_status[]=$receipt_no["cr_entry_type"];
							$total_amount=$total_amount+$receipt_no["cr_amount"];
							$hindi_text=NULL;
							$charges=$packagesMapper->getChargesByparameter($receipt_no["entry_status"]);
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$hindi[]=$hindi_text;
							$data_charges=array(
								"hindi_text"=>$hindi_text,
								"amount"=>$receipt_no["cr_amount"],
								"transaction"=>$receipt_no["cr_amount"], 
								"transaction_id"=>$receipt_no["transaction_id"],
							);
							$charges_arr[]=$data_charges; 
							
							$date_val=date("d-M-Y g:i a",strtotime($receipt_no["timestamp"]));
						}
															
							$transaction_history=array(
									"date"=>$date_val,
									"amount"=> implode(",",$amount_type),
									"transaction_type"=>implode(",",$cr_entry_status),
									"entry_status"=>implode(",",$entry_type),
									"transaction_type_color"=>NULL,
									"is_print"=>1,
									"hindi_text"=>implode(",",$hindi),
									"total_collection" => $total_amount,
									"receipt_no" => $reciept_no,
									"receipt_count"=>$receipt_count,
									"receipt_user_id"=>$user_id, 
									"charges"=>$charges_arr, 
							);
						$transaction_historys[]=$transaction_history;
					}
    				
					 
    			}
    		}else{
    			$transaction_historys=array();
    		}
    		 
    		
    		   
    		$meterVal=array();
    		$meter_nos=$meterReadingsMapper->getAllMeterOnconsumerId($consumer->__get("consumer_id"));
			if($consumer->__get("site_id")!=62 && $consumer->__get("site_id")!=31){
				if($meter_nos){
					foreach ($meter_nos as $meter_no){
						if($meter_no["meter_no"]!='' && $meter_no["meter_no"]!=NULL && $meter_no["meter_no"]!='undefined' && $meter_no["meter_no"]!='null'){
						$meterValue=array();
						$meter=$meterReadingsMapper->getMeterReadingByMeterNo($meter_no["meter_no"],$consumer->__get("consumer_id"));
						
						if($meter){
							foreach($meter as $meters){	
								$packageName=$packagesMapper->getPackageById($meters["package_id"]);
								$packName=$packageName->__get("package_name");
								$meter_val=array(
										"datetime"=>date("d-M-Y g:i a",strtotime($meters["timestamp"])),
										"package_id"=>$meters["package_id"],
										"package_name"=>$packageName->__get("package_name"),
										"opening_reading"=>floatval($meters["meter_reading"]),
										"closing_reading"=>(floatval($meters["unit"])),
								);
								$meterValue[]=$meter_val;
							 }  
						}
						$data=array(
								"meter_no"=>$meter_no["meter_no"]." (".$packName.")",
								"reading_details"=>$meterValue
						);
						$meterVal[]=$data;
						}
					} 
				}
			}else{
				if($meter_nos){
					foreach ($meter_nos as $meter_no){
						if($meter_no["meter_no"]!='' && $meter_no["meter_no"]!=NULL && $meter_no["meter_no"]!='undefined' && $meter_no["meter_no"]!='null'){
						$meterValue=array();
						$meter=$meterReadingsMapper->getReadingByMeterNoCustomerData($consumer->__get("consumer_id"),$meter_no["meter_no"]);
						if($meter){
							foreach($meter as $meters){
								
								$packageName=$packagesMapper->getPackageById($meters["package_id"]);
								$packName=$packageName->__get("package_name");
								 
								$current_date = date_parse_from_format("Y-m-d", $meters["timestamp"]);
								$month = sprintf("%02d", $current_date["month"]);
								$year = $current_date["year"];
								$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
								$timestamp=$year."-".$month."-".$total_days_in_month;
								if($meters["start_reading"]==0){
									$reading_vals=$meterReadingsMapper->getConsumerMeterReading($consumer->__get("consumer_id"),$timestamp,$month,$year,1);
								}else{
									$reading_vals=$meterReadingsMapper->getConsumerMeterReading($consumer->__get("consumer_id"),$meters["timestamp"],$month,$year,0);
								}
								
								$meter_val=array(
										"datetime"=>date("d-M-Y g:i a",strtotime($reading_vals["timestamp"])),
										"package_id"=>$meters["package_id"],
										"package_name"=>$packageName->__get("package_name"),
										"opening_reading"=>floatval($reading_vals["meter_reading"]),
										"closing_reading"=>(floatval($reading_vals["unit"])),
								);
								$meterValue[]=$meter_val;
							 } 
						}
						$data=array(
								"meter_no"=>$meter_no["meter_no"]." (".$packName.")",
								"reading_details"=>$meterValue
						);
						$meterVal[]=$data;
						}
					} 
				}
			}
			$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer->__get("consumer_id"));
    		
			$data=array(
    				'connectionID'=>$consumer_connection_id,
    				"reading_history"=>$meterVal,
    				"consumer_details"=>$consumer_details,
    				"ActivationCharge"=>$actiCharges,
    				"customerType"=> $consumer_types ,
					"consumerSubType"=>$sub_consumer_types,
    				"CustomerCode"=>$consumer_mobile,
    				"transaction_historys"=>$transaction_historys,
    				
    				"helpLine_no"=>'0000-0000-0000',  
					"outstanding"=>intval($oustanding)
                  
    		);
    		$result="Customer Details Found";
    	}else{
    		$result="Customer Not Found";
    		$data=array();
    	}
    	 
    	$input=array('api'=>'consumer_detail',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function addConsumerSchemeAction(){ 
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		$request = $this->getRequest();
		date_default_timezone_set('Asia/Kolkata');
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$consumer_id=$request->getParam("consumer_id");
		$scheme_id=$request->getParam("scheme_id");
		$package_continue=$request->getParam("package_continue");
		$scheme_continue=$request->getParam("scheme_continue");
		 
		 $parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"consumer_id"=>$consumer_id,
			"scheme_id"=>$scheme_id,
		);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		
		 $data=array();
		$files=array();	$success=0;     											
         
			                     						$consumers=$consumersMapper->getConsumerById($consumer_id);
			                     						$date = new Zend_Date();
			                     						$date->setTimezone("Asia/Kolkata");
			                     						$cur_date=$date->toString("yyyy-MM-dd");
			                     						$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
			                     						if($consumers){
			                     							 	$equip_month_dis=NULL;
																$scheme=$schemeMapper->getSchemeById($scheme_id);
			                     							 	if($scheme->__get("scheme_type")=="equipment"){
			                     									$total_month=$scheme->__get("emi_duration");
																	$equip_month_dis=$scheme->__get("discount_month");
			                     								}elseif ($scheme->__get("gen_scheme_type")=="advance_pay"){
			                     									$total_month=$scheme->__get("advance_payment")+$scheme->__get("advance_dis_month");
			                     								}
			                     								$sites=$siteMapper->getSiteById($consumers->__get("site_id"));
			                     								$cluster_id=$sites->__get("cluster_id");
			                     								$consumer_schemes = new Application_Model_ConsumerScheme();
			                     								$consumer_schemes->__set("consumer_scheme", $scheme_id);
			                     								$consumer_schemes->__set("consumer_id", $consumer_id);
			                     								$consumer_schemes->__set("cluster_id", $cluster_id);
			                     								$consumer_schemes->__set("site_id", $consumers->__get("site_id"));
			                     								$consumer_schemes->__set("total_month", $total_month);  
																$consumer_schemes->__set("equip_month_dis", $equip_month_dis);
																$consumer_schemes->__set("package_continue", $package_continue);  
																$consumer_schemes->__set("scheme_continue", $scheme_continue);
																$consumer_schemes->__set("timestamp", $cur_date);  
																$consumer_schemes->__set("assign_date", $cur_date);
																 
			                     								$consumer_schemes_id = $consumerSchemeMapper->addNewConsumerScheme($consumer_schemes);
			                     								if($consumer_schemes_id){
			                     									if($scheme->__get("scheme_type")=="equipment"){
			                     										$consumerPack=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
			                     										$down_payment=$scheme->__get("down_payment");
			                     										$date = new Zend_Date();
			                     										$date->setTimezone("Asia/Kolkata");
			                     										$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
			                     							
			                     										$cr_amount = str_pad($down_payment, 4, "0", STR_PAD_LEFT);
			                     										$transaction_id = $timestamp . "-" . $cr_amount;
			                     										$cashRegisterMapper = new Application_Model_CashRegisterMapper();
			                     										$cashRegister = new Application_Model_CashRegister();
			                     										$cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
			                     										$cashRegister->__set("consumer_id", $consumer_id);
			                     										$cashRegister->__set("cr_entry_type", "DEBIT");
			                     										$cashRegister->__set("cr_amount", $down_payment);
			                     										$cashRegister->__set("receipt_number", "CA-" . $collectionAgent->__get("collection_agent_id"));
			                     										$cashRegister->__set("transaction_id", $transaction_id);
			                     										$cashRegister->__set("transaction_type", "(M)");
			                     										$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
			                     										$cashRegister->__set("entry_status", "EED");
			                     										$cashRegister->__set("remark", "Downpayment");
			                     										$cashRegister->__set("package_id", $consumerPack[0]["package_id"]);
			                     										$cashRegisterMapper->addNewCashRegister($cashRegister);
			                     									}
			                     								}
																$success=1;
																$result="Scheme assigned successfully.";
			                     							 
			                     						}else{
			                     							$result="Customer Not Found";
			                     							$data=array();
			                     						} 
                     											 
           $input=array('api'=>'add_consumer_scheme',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function addTransactionAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		$stateMapper=new Application_Model_StatesMapper();
	
		$request = $this->getRequest();
		date_default_timezone_set('Asia/Kolkata');
		
		$user_id=$request->getParam("user_id");
        $user_type=$request->getParam("user_type");
		$consumer_id=$request->getParam("consumer_id");
		$charges=json_decode($request->getParam("charges"));
		 
					 						 	
		$parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"consumer_id"=>$consumer_id,
		);
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$files=array();	$success=0;     											
        date_default_timezone_set('Asia/Kolkata');
                     						
                     							$tran_type=array();$tran_amt=array();
												foreach($charges as $charge){
														$tran_type[]= $charge->charge_type;
														$tran_amt[] = $charge->amount;
												}
                     							//$tran_type=array("ACT","CHG","EED","MED","OTHERS","WATER");
                     							//$tran_amt=array($activation,$monthly,$eed,$med,$others,$water);
                     						 
                     							$checkData=0;
                     							$act_transaction=0;$chg_transaction=0;$consumer_name='';$consumer_father_name='';
                     							//$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                     							//$consumer = $consumers[0];
												$consumer=$consumersMapper->getConsumerById($consumer_id);
			                     				$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
                     							$allDetails_tran=array();
                     							$charges=array();
												$totalCharges=0;
												$receipt_id=intval($cashRegisterMapper->getMaxUserNumber($user_id));
														
                     							for($i=0;$i<count($tran_type);$i++){
                     								if($tran_amt[$i]!=NULL && $tran_amt[$i]!="" && $tran_amt[$i]!='undefined'){
														$date = new Zend_Date();
														$date->setTimezone("Asia/Kolkata");
                     									//$result = $this->rechargeConsumer($sender,$collectionAgent, $tran_type[$i], $tran_amt[$i], $customer_code);
														$timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));

														$tcr_amount = str_pad($tran_amt[$i], 4, "0", STR_PAD_LEFT);
														$transaction_id = $timestamp . "-" . $tcr_amount;
														$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
														$cashRegister = new Application_Model_CashRegister();
														$cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
														$cashRegister->__set("cr_entry_type", "CREDIT");
														$cashRegister->__set("cr_amount", $tran_amt[$i]);
														$cashRegister->__set("transaction_id", $transaction_id);
														$cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
														$cashRegister->__set("receipt_number", ($receipt_id+1));
														$cashRegister->__set("entry_status", $tran_type[$i]); 
														$cashRegister->__set("package_id", $consumerPackages[0]["package_id"]); 
														$cashRegister->__set("transaction_type", "(M)" );
														$totalCharges=$totalCharges+$tran_amt[$i];
														if ($cashRegisterMapper->addNewCashRegister($cashRegister)) {
															
															if($tran_type[$i]=='MED' || $tran_type[$i]=='OTHERS'){
																$tcr_amount = str_pad($tran_amt[$i], 4, "0", STR_PAD_LEFT);
																$transaction_id = $timestamp . "-" . $tcr_amount;
																$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
																
																$cashRegister = new Application_Model_CashRegister();
																$cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
																$cashRegister->__set("cr_entry_type", "DEBIT");
																$cashRegister->__set("cr_amount", $tran_amt[$i]);
																$cashRegister->__set("transaction_id", $transaction_id);
																$cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
																$cashRegister->__set("entry_status", $tran_type[$i]); 
																$cashRegister->__set("package_id", $consumerPackages[0]["package_id"]); 
																$cashRegister->__set("transaction_type", "(M)" );
																$cashRegisterMapper->addNewCashRegister($cashRegister);
															}
                     										$success=1;$checkData=1;
                     										 
                     										$details_tran=array(
                     												$tran_type[$i]."Amount"=>$tran_amt[$i],
                     												$tran_type[$i]."TransactionID"=>$transaction_id
                     										);
                     						
                     										array_push($allDetails_tran,$details_tran);
                     										 
                     										$charges_data=$packagesMapper->getChargesByparameter($tran_type[$i]);
                     										$charges_hindi=NULL;
                     										if($charges_data){
                     											$charges_hindi=$charges_data["hindi_text"];
                     										}
                     										$data_charges=array(
                     												"hindi_text"=>$charges_hindi,
                     												"amount"=>$tran_amt[$i],
                     												"transaction"=>$act_transaction, 
																	"transaction_id"=>$transaction_id
                     										);
                     										$charges[]=$data_charges;
                     										 
                     									}
                     								}
                     							} 
                     							if(count($allDetails_tran)==6){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2],$allDetails_tran[3],$allDetails_tran[4],$allDetails_tran[5]);
                     							}elseif (count($allDetails_tran)==5){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2],$allDetails_tran[3],$allDetails_tran[4]);
                     							}elseif (count($allDetails_tran)==4){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2],$allDetails_tran[3]);
                     							}elseif (count($allDetails_tran)==3){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2]);
                     							}elseif (count($allDetails_tran)==2){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1]);
                     							}else{
                     								$allDetails_tran=$allDetails_tran[0];
                     							}
                     						     
                     							 if($checkData==1){
                     								$charge=array(
                     										"charges"=>$charges,
															
                     								);
													//state, primary mobile no, receipt_no, helpline no,plant_name
													$sites=$siteMapper->getSiteById($consumer->__get("site_id"));
													 
													$state_id=$sites->__get("state_id");
													$states=$stateMapper->getStateById($state_id);
													$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer->__get("consumer_id"));
                     								$resultdata=array('connectionID'=>$consumer->__get("consumer_connection_id"),
                     										'consumerName'=> $consumer->__get("consumer_name"),
                     										'consumerFatherName'=>$consumer->__get("consumer_father_name"),
                     										'timestamp'=>date("d-M-Y g:i a"),  
															"state"=>$states->__get("state_name"),
															"plant"=>$sites->__get("site_name"),
															"primary_mob_no"=>$consumer->__get("consumer_code"),
															"helpLine_no"=>'0000-0000-0000', 
															"receipt_number"=>$sites->__get("site_name").$user_id.$receipt_id,
															"total_amount"=>$totalCharges,
															"oustanding"=>$oustanding
                     								);
                     								$data=array_merge($resultdata,$allDetails_tran,$charge);
													$success=1;
                     								$result="Transactions have been inserted successfully.";
                     							}else{
                     								$data=new stdClass();
                     							} 
                     							 
                     											 
           $input=array('api'=>'add_transaction',
						 'parameters'=>$parameter);
		 	$commandResult = array( 
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function updatePrintCountAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
			$request = $this->getRequest();
			$user_id=$request->getParam("user_id");
			$user_type=$request->getParam("user_type");
			$receipt_no=$request->getParam("receipt_no");
			$receipt_count=$request->getParam("receipt_count");
			$receipt_user_id=$request->getParam("receipt_user_id");
			$consumer_id=$request->getParam("consumer_id");
			
		 $parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"receipt_no"=>$receipt_no,
			"receipt_count"=>$receipt_count,
		);
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$files=array();	$success=0;     											
		$receipt_nos=explode($receipt_user_id,$receipt_no);
		$receipt_count=intval($receipt_count)+1;
		
		if($cashRegisterMapper->updateReceiptCountByTransactionId( $receipt_nos[1] ,$receipt_count,$consumer_id )){
			$success=1;
			$timestamp=array();
				$data=array(
					"receipt_count"=>$receipt_count
				);
				$arr=array(
					"success"=>$success,
					"data"=>$data
				);
				$result="receipt count updated successfully.";
		} else{
			$success=0;
			$result= "receipt count Can not be updated.";
		}
                     											 
           $input=array('api'=>'update_print_count',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function consumerSchemeListAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		//site_id, skip_count, from, to, status	site_id =  blank for all sites,status = -1 -all, 0 - inactive, 1 - active
		
		date_default_timezone_set('Asia/Kolkata');
		$request = $this->getRequest();
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$site_id=$request->getParam("site_id");
		$skip_count=$request->getParam("skip_count");
		$scheme_id=$request->getParam("scheme_id");
		$from=$request->getParam("from");
		$to=$request->getParam("to");
		$status=$request->getParam("status");
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		 $parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"site_id"=>$site_id,
			"skip_count"=>$skip_count,
			"from"=>$from,
			"to"=>$to,
			"status"=>$status,
		);
		 
		$files=array();	$success=0;     											
		
		$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
		$sites=$collectionAgent->__get("site_id");
		$site_ids=explode(",", $sites);
		if ($site_id!="" && $site_id!=NULL){
			$site_ids=array($site_id);
			$sites=$siteMapper->getSiteById($site_id);
			$siteName=$sites->__get("site_name");
		}else{
			$siteName="All Plants";
		}
		$data_val=array();
		if($from==NULL && $to==NULL){
			$zendDate = new Zend_Date();
			$zendDate->setTimezone("Asia/Calcutta");
			$month = $zendDate->toString("MM");
			$year = intval($zendDate->toString("yyyy"));
			$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$from=$year."-".$month."-01";
			$to=$year."-".$month."-".$total_days_in_month;
		}
		if($status=="" || $status==NULL){
			$status_return="All";
		}elseif($status==1){
			$status_return="Active";
		}else{
			$status_return="Inactive";
		}
		if ($consumer_schemes = $consumerSchemeMapper->getAllConsumerSchemeSiteWise($site_ids,$skip_count,$from,$to,$status,$scheme_id)) {
			$success=1;     
			foreach ($consumer_schemes as $consumer_scheme) { 
				$consumers = $consumersMapper->getConsumerById($consumer_scheme->__get("consumer_id"));
				$consumer_schemes_name=$schemeMapper->getSchemeById($consumer_scheme->__get("consumer_scheme"));
				$outstanding=$consumersMapper->getConsumerOutstanding($consumer_scheme->__get("consumer_id"));
				$activation= $consumersMapper->getRemainingActivation($consumer_scheme->__get("consumer_id"));
				//date('d-M-Y', strtotime(date('d-M-Y')."+".$equip_month_dis." month")),
				$total=$outstanding+$activation;
					$scheme_val=$schemeMapper->getSchemeById($consumer_scheme->__get("consumer_scheme"));
					$datas = array(
						"consumer_scheme_id" => $consumer_scheme->__get("consumer_scheme"),
						"consumer_id" => $consumer_scheme->__get("consumer_id"),
						"consumer_name" => $consumers->__get("consumer_name"),
						"consumer_code" => $consumers->__get("consumer_code"),
						"consumer_scheme_name" => $consumer_schemes_name->__get("scheme"),
						"cluster_id" => $consumer_scheme->__get("cluster_id"),
						"site_id" => $consumer_scheme->__get("site_id"),
						"timestamp" => $consumer_scheme->__get("timestamp"),
						"connection_id" => $consumers->__get("consumer_connection_id"),
						"package_continue"=> $consumer_scheme->__get("package_continue"),
						"scheme_continue"=> $consumer_scheme->__get("scheme_continue"),
						"outstanding_amount"=> $total, 
						"from_date"=>date('d-M-Y',strtotime( $consumer_scheme->__get("timestamp"))),
						"to_date"=>date('d-M-Y', strtotime($consumer_scheme->__get("timestamp")."+".$scheme_val->__get("discount_month")." month")), 
					);
				$data_val[] = $datas;
				$result="Schemes are loading successfully.";
			}
				
		}else{
			$data_val=array(); 
			$result="No scheme Found";
			 
		}  	
		$schemes_data=$schemeMapper->getAllSchemes(null,true);
		if($schemes_data){
			foreach($schemes_data as $scheme){
					$scheme_data=array(
								"scheme_id"=>($scheme->__get("scheme_id")=="NULL" || $scheme->__get("scheme_id")=='undefined' || $scheme->__get("scheme_id")==NULL)?'':$scheme->__get("scheme_id"),
								"scheme_name"=> ($scheme->__get("scheme")=="NULL" || $scheme->__get("scheme")=='undefined' || $scheme->__get("scheme")==NULL)?'':$scheme->__get("scheme"),
								"down_payment"=> ($scheme->__get("down_payment")=="NULL" || $scheme->__get("down_payment")=='undefined' || $scheme->__get("down_payment")==NULL)?'':$scheme->__get("down_payment"),
								"emi_amt"=> ($scheme->__get("emi_amt")=="NULL" || $scheme->__get("emi_amt")=='undefined' || $scheme->__get("emi_amt")==NULL)?'':$scheme->__get("emi_amt"),
								"emi_duration"=> ($scheme->__get("emi_duration")=="NULL" || $scheme->__get("emi_duration")=='undefined' || $scheme->__get("emi_duration")==NULL)?'':$scheme->__get("emi_duration"),
								"discount_amt"=> ($scheme->__get("discount_amt")=="NULL" || $scheme->__get("discount_amt")=='undefined' || $scheme->__get("discount_amt")==NULL)?'':$scheme->__get("discount_amt"),
								"duration"=> ($scheme->__get("discount_month")=="NULL" || $scheme->__get("discount_month")=='undefined' || $scheme->__get("discount_month")==NULL)?'':$scheme->__get("discount_month"),
								"from_date"=>date('d-M-Y',strtotime( $scheme->__get("start_date"))),
								"to_date"=>date('d-M-Y', strtotime( $scheme->__get("end_date"))), 
								"package_continue"=> ($scheme->__get("package_continue")=="NULL" || $scheme->__get("package_continue")=='undefined' || $scheme->__get("package_continue")==NULL)?'':$scheme->__get("package_continue"),
								"scheme_continue"=> ($scheme->__get("scheme_continue")=="NULL" || $scheme->__get("scheme_continue")=='undefined' || $scheme->__get("scheme_continue")==NULL)?'':$scheme->__get("scheme_continue"),
								"advance_payment_for_month"=>($scheme->__get("advance_payment")=="NULL" || $scheme->__get("advance_payment")=='undefined' || $scheme->__get("advance_payment")==NULL)?'':$scheme->__get("advance_payment"),
								"discount_for_month"=>($scheme->__get("advance_dis_month")=="NULL" || $scheme->__get("advance_dis_month")=='undefined' || $scheme->__get("advance_dis_month")==NULL)?'':$scheme->__get("advance_dis_month"),
								"flat_discount"=>($scheme->__get("flat_dis")=="NULL" || $scheme->__get("flat_dis")=='undefined' || $scheme->__get("flat_dis")==NULL)?'':$scheme->__get("flat_dis"),
								"percentage_discount"=>($scheme->__get("per_dis")=="NULL" || $scheme->__get("per_dis")=='undefined' || $scheme->__get("per_dis")==NULL)?'':$scheme->__get("per_dis"),
								"equipment"=>$equip_name,
							);  
							$scheme_arr[]=$scheme_data; 
			}
		}	
					$data=array(
						"plant"=>$siteName,
						"from" => date("d-M-Y",strtotime($from)), 
						"to" => date("d-M-Y",strtotime($to)),
						"status"=>$status_return,
						"scheme"=>$data_val,
						"scheme_all"=>$scheme_arr
						
					);		
           $input=array('api'=>'consumer_scheme_list',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function addConsumerPackageAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		$request = $this->getRequest();
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$consumer_id=$request->getParam("consumer_id");
		$package_id=$request->getParam("package_id");
		$meter_reading=$request->getParam("meter_reading");
		$unit_rate=$request->getParam("unit_rate");
		$meter_name=$request->getParam("meter_name");
		
		 $parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"consumer_id"=>$consumer_id,
			"package_id"=>$package_id,
			"meter_reading"=>$meter_reading,
			"unit_rate"=>$unit_rate,
			"meter_name"=>$meter_name,
		);
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$files=array();	$success=0;     $data=array();									
        
                 
                     									
                     										$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
                     										$packages=$packagesMapper->getPackageById($package_id);
                     										$consumerPackageMapper = new Application_Model_ConsumersPackageMapper();
											    			$package= new Application_Model_ConsumersPackage();
											    			$package->__set("package_id",$package_id);
											    			$package->__set("consumer_id",$consumer_id);
											    			$package->__set("type",NULL);
											    			$package->__set("charges",$unit_rate);
											    			
											    			$date = new Zend_Date();
											    			$date->setTimezone("Asia/Calcutta");
											    			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
											    				
											    			$consumerPackage=$consumerPackageMapper->addNewConsumersPackage($package);
											    				if($consumerPackage)
											    				{ 
																$success=1; 
											    					if($packages->__get("is_postpaid")!=0){
											    						$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
											    						$meterReading = new Application_Model_MeterReadings();
											    						$meterReading->__set("consumer_id",$consumer_id);
											    						$meterReading->__set("meter_reading",$meter_reading);
											    						$meterReading->__set("timestamp",$timestamp);
											    						$meterReading->__set("package_id",$package_id);
											    						$meterReading->__set("start_reading",1);
																		$meterReading->__set("meter_name",$meter_name);
																		$meterReading->__set("entry_by",$user_id); 
																		$meterReading->__set("entry_type",'M');  
											    						$meterReadingsMapper->addNewMeterReading($meterReading);
											    					}
											    					
											    					$packageMapper= new Application_Model_PackagesMapper();
											    					$packages=$packageMapper->getPackageById($package_id);
											    					$package_cost = $packages->__get("package_cost");
											    					 
											    					$status="Package Updated";
											    					$package= new Application_Model_PackageHistory();
											    					$package->__set("package_id",$package_id);
											    					$package->__set("consumer_id",$consumer_id);
											    					$package->__set("package_cost",$package_cost);
											    					$package->__set("change_by",$user_id);
											    					$package->__set("status",$status);
											    					$package->__set("last_package",NULL);
											    					 
											    					$packageHistorymapper = new Application_Model_PackageHistoryMapper();
											    					$packageHistory= $packageHistorymapper->addNewPackageHistory($package);
																	$result = "Package assign successfully.";
											    				}else{
											    					$result = "Error. Kripya kuch der baad try karein.";
											    				}
											    			    											 
           $input=array('api'=>'add_consumer_package',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function addMeterReadingAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		 $request = $this->getRequest();
		 $user_id=$request->getParam("user_id");
		 $user_type=$request->getParam("user_type");
		 $consumer_id=$request->getParam("consumer_id");
         $package_id=$request->getParam("package_id");
		 $end_reading=$request->getParam("new_reading");
		 $meter_no=$request->getParam("meter_no"); 
		 
		 $parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"consumer_id"=>$consumer_id,
			"package_id"=>$package_id,
			"end_reading"=>$end_reading,
			"meter_no"=>$meter_no,
		);
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$files=array();	$success=0; $data=array(); 											
         
                     											$date = new Zend_Date();
                     											$date->setTimezone("Asia/Calcutta");
                     											$timestamp_date = $date->toString("yyyy-MM-dd HH:mm:ss");
													//if($meter_no!="" &&  $meter_no!=NULL &&  $meter_no!='undefined' &&  $meter_no!='null'){
                     											$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
                     											$packages=$packagesMapper->getPackageById($package_id);
                     											$consumers=$consumersMapper->getConsumerById($consumer_id);
                     											if($packages->__get("is_postpaid")!=0){
                     											  
                     												$latestReading = $meterReadingsMapper->getLatestReadingByConsumerId($consumer_id,$package_id);
                     												$start_reading = 0;
                     												if($latestReading){
                     													$start_reading = abs($latestReading->__get("meter_reading"));
                     												}
                     												if($end_reading>=$start_reading)
                     												{
																		$checkMeter = $meterReadingsMapper->checkMeterNo($meter_no,$consumer_id); 
																		//if($checkMeter){
																		$unit=sprintf("%.2f",floatval($end_reading-$start_reading));
																		if($unit<1500) { 
																			$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
																			$meterReading = new Application_Model_MeterReadings();
																			$meterReading->__set("consumer_id",$consumer_id);
																			$meterReading->__set("meter_reading",sprintf("%.2f",floatval($end_reading)));
																			$meterReading->__set("unit",sprintf("%.2f",floatval($end_reading-$start_reading)));
																			$meterReading->__set("timestamp",$timestamp_date);
																			$meterReading->__set("package_id",$package_id);
																			$meterReading->__set("start_reading",0);
																			$meterReading->__set("meter_no",$meter_no); 
																			$meterReading->__set("entry_by",$user_id); 
																			$meterReading->__set("entry_type",'M');  
																			$meterReadingsMapper->addNewMeterReading($meterReading);
																			 
																			if($packages->__get("is_postpaid")==1) {
																				$date = new Zend_Date();
																				$date->setTimezone("Asia/Kolkata");
																				
																				$timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));
																				$amount=round(($end_reading-$start_reading)*$consumers->__get("micro_entrprise_price"));
																				$tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
																				$transaction_id = $timestamp . "-" . $tcr_amount;
																				
																				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
																				$cashRegister = new Application_Model_CashRegister();
																				$cashRegister->__set("consumer_id", $consumer_id);
																				$cashRegister->__set("cr_entry_type", "DEBIT");
																				$cashRegister->__set("entry_status", "CHG");
																				$cashRegister->__set("timestamp", $timestamp_date);
																				$cashRegister->__set("cr_amount", round($amount)); 
																				$cashRegister->__set("transaction_id", $transaction_id);
																				$cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
																				$cashRegister->__set("receipt_number", "CA-" . $collectionAgent->__get("collection_agent_id"));
																				$cashRegister->__set("mtr_entry", '1');
																				$cashRegister->__set("transaction_type", "(M)" );
																				$cashRegister->__set("package_id", $package_id );
																				
																				$cashRegisterMapper->addNewCashRegister($cashRegister);
																			}
																			$success=1;
																			$result="Meter Reading is added successfully.";
																		} else {
																			$success=0;
																			$result = "Error. Your unit is more then 500, Please check";
																		} 
																	/*}else{
                     													$success=0;
                     													$result = "Error. Please enter correct meter number.";
                     												} 	*/
																	}else{
                     													$success=0;
                     													$result = "Error. Previous Reading for ".$consumers->__get("consumer_connection_id")." was ".$start_reading.". New reading must be bigger. Please check and try again.";
                     												} 
                     											}else{
                     												$success=0;
                     												$result="Connection is not Metered.";
                     											} 
														/*}else{
															$success=0;
															$result="Error. Please add meter number to consumer.";
														}*/ 
                     										        											 
           $input=array('api'=>'add_meter_reading',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function cashRegisterAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		$request = $this->getRequest();
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$site_id=$request->getParam("site_id");
		$start_date=$request->getParam("start_date");
		$end_date=$request->getParam("end_date");
		$collection_type=$request->getParam("collection_type");
		$skip_count=$request->getParam("skip_count");
		
		 $parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"site_id"=>$site_id,
			"start_date"=>$start_date,
			"end_date"=>$end_date,
			"collection_type"=>$collection_type,
			"skip_count"=>$skip_count,
		);
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$files=array();	$success=0;     											
        
		/*if($site_id!=NULL && $site_id!=""){
			$sites_vals=$sitesMapper->getSiteById($site_id);
			$site_id_name=$sites_vals->__get("site_name");
		}else{
			$site_id_name="ALL Plants";
		}*/
		
		if($start_date==NULL && $end_date==NULL){
			$zendDate = new Zend_Date();
			$zendDate->setTimezone("Asia/Calcutta");
			$zendDate->toString("yyyy-MM-dd HH:mm:ss");
			$day = $zendDate->toString("dd");
			$month = $zendDate->toString("MM");
			$year = intval($zendDate->toString("yyyy"));
			 
			$start_date=$year."-".$month."-01";
			$end_date=$year."-".$month."-".$day;
			
			$zendDate = new Zend_Date($start_date,"yyyy-MM-dd");
			$from=$zendDate->toString("dd-MMM-yyyy");
			
			$zendDates = new Zend_Date($end_date,"yyyy-MM-dd");
			$to=$zendDates->toString("dd-MMM-yyyy");
			
		}else{
		  
			$zendDate = new Zend_Date($start_date,"yyyy-MM-dd");
			$from=$zendDate->toString("dd-MMM-yyyy");
			
			$zendDates = new Zend_Date($end_date,"yyyy-MM-dd");
			$to=$zendDates->toString("dd-MMM-yyyy");
		}
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp_date = $date->toString("yyyy-MM-dd HH:mm:ss");
		$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
		if ($collectionAgent){
			$site_arr=array();    
			if($site_id!=NULL && $site_id!=""){
				 if(count(explode(",", $site_id))>1){
					$site_id_name="Selected Plants";
				 }else{
					$site = $siteMapper->getSiteById($site_id);
					$site_id_name=$site->__get("site_name");	
				}
				$site_arr=explode(",",$site_id);
			
			 }else{
				$site_id_name="All Plants";
				$site_arr=explode(",",$collectionAgent->__get("site_id")); 
			 }
		 
			$data=array();
			if($collection_type!=NULL && $collection_type!=""){
				if($collection_type==1){
					$coll_type=1;
					$users=$user_id;
					$userVal=NULL;
				}elseif($collection_type==2){
					$coll_type=2;
					$users='Other';
					$userVal=$user_id;
				}else{
					$coll_type=0;
					$users=NULL;
					$userVal=NULL;
				}
			}else{
				$coll_type=0;
				$users=NULL;
			}
			$datas=array();
			$consumers_data=$cashRegisterMapper->getCashRegister($site_arr,NULL,$start_date,$end_date,$users,$skip_count,$userVal);
			
			if($consumers_data){
				foreach ($consumers_data as $consumer_data) {
					$consumers=$consumersMapper->getConsumerById($consumer_data->__get("consumer_id"));
					$sites_val=$sitesMapper->getSiteById($consumers->__get("site_id"));
					$transaction_history = array(
						"consumer_name" => $consumers->__get("consumer_name"),
						"consumer_id" => $consumers->__get("consumer_id"),
						"connection_id" => $consumers->__get("consumer_connection_id"),
						"site_name" => $sites_val->__get("site_name"), 
						"transaction_id" => $consumer_data->__get("transaction_id"),
						"amount" => $consumer_data->__get("cr_amount"),
						"cr_entry_type" => $consumer_data->__get("cr_entry_type"),
						"entry_status" => $consumer_data->__get("entry_status"),
						"receipt_no" => $sites_val->__get("site_code").$consumer_data->__get("user_id").$consumer_data->__get("receipt_number"), 
						"total_collection" => $consumer_data->__get("cr_amount"),
						"date" => date("d-M-Y g:i a",strtotime($consumer_data->__get("timestamp"))),
					);
					$datas[]=$transaction_history;
				}
				$result = "All transaction are loaded successfully.";
				$success=1;
			}else{
				$result = "Error. Kripya kuch der baad try karein.";
			}
			
		$consumers_data=$cashRegisterMapper->getBillsByRangeSiteId($start_date,$end_date,$site_arr,NULL,$users,$userVal);
		
		$total= intval($consumers_data["ACT"])+intval($consumers_data["CHG"])+intval($consumers_data["EED"])+intval($consumers_data["MED"])+intval($consumers_data["WATER"])+intval($consumers_data["OTHERS"])+intval($consumers_data["SD"])+intval($consumers_data["METER"]); 
		//echo intval($consumers_data["ACT"])." ".intval($consumers_data["CHG"])." ".intval($consumers_data["EED"])." ".intval($consumers_data["MED"])." ".intval($consumers_data["WATER"])." ".intval($consumers_data["OTHERS"])." ".intval($consumers_data["SD"])." ".intval($consumers_data["METER"]);				   
					if(intval($consumers_data["CHG"])>0){
						$charges=array(
							"label"=>"CHG","amount"=>intval($consumers_data["CHG"])
						);
						$charges_arr[]=$charges;
					}
					if(intval($consumers_data["ACT"])>0){
						$charges=array(
								"label"=>"ACT","amount"=>intval($consumers_data["ACT"])
							);
							$charges_arr[]=$charges;
					}
					if(intval($consumers_data["WATER"])>0){
						$charges=array(
								"label"=>"WATER","amount"=>intval($consumers_data["WATER"])
						);
						$charges_arr[]=$charges;
					}
					if(intval($consumers_data["OTHERS"])>0){
						$charges=array(
								"label"=>"OTHERS","amount"=>intval($consumers_data["OTHERS"])
						);
						$charges_arr[]=$charges;
					}
					if(intval($consumers_data["EED"])>0){
						$charges=array(
								"label"=>"EED","amount"=>intval($consumers_data["EED"])
						);
						$charges_arr[]=$charges;
					}
					if(intval($consumers_data["MED"])>0){
						$charges=array(
								"label"=>"MED","amount"=>intval($consumers_data["MED"])
						);
						$charges_arr[]=$charges;
					}
					if(intval($consumers_data["SD"])>0){
						$charges=array(
								"label"=>"Security Deposit","amount"=>intval($consumers_data["SD"])
						);
						$charges_arr[]=$charges;
					}
					if(intval($consumers_data["METER"])>0){
						$charges=array(
								"label"=>"METER","amount"=>intval($consumers_data["METER"])
						);
						$charges_arr[]=$charges;
					}
			$cash_data_count=$cashRegisterMapper->getCashRegisterCount($site_arr,NULL,$start_date,$end_date,$users,$skip_count,$userVal);	
			$data=array(
				"cash_register"=>$datas,
				"charges"=>$charges_arr, 
				"site_name"=>$site_id_name,
				"collection_type"=>$coll_type,
				'from'=>$from,
				'to'=>$to,
				"TotalCollection"=>intval($total), 
				"cashRegister_date_calendar"=>'30',
				"total_cash_register"=>intval($cash_data_count) 
			); 
		}else{
			$result = "Error. You are not an Agent.";
			$data=array();
		}
           $input=array('api'=>'cash_register',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function changeConsumerPackageAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		$request = $this->getRequest();
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$consumer_id=$request->getParam("consumer_id");
		$old_package_id=$request->getParam("old_package_id");
		$new_package=$request->getParam("new_package_id");
		$meter_reading=$request->getParam("meter_reading");
		$unit_rate=$request->getParam("unit_rate");
		$meter_no=$request->getParam("meter_no");
		$remark=$request->getParam("reason_for_change");
		
		$parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"consumer_id"=>$consumer_id,
			"old_package_id"=>$old_package_id,
			"new_package_id"=>$new_package,
			"meter_reading"=>$meter_reading,
			"unit_rate"=>$unit_rate,
			"meter_no"=>$meter_no,
			"reason_for_change"=>$remark,
		);
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$files=array();	$success=0; $data=array();    											
        	  
                     												$date = new Zend_Date();
                     												$date->setTimezone("Asia/Calcutta");
                     												$timestamp_date = $date->toString("yyyy-MM-dd HH:mm:ss");
                     											
                     												$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
                     												$packages=$packagesMapper->getPackageById($new_package);
                     												$consumers=$consumersMapper->getConsumerById($consumer_id);
                     												$cp=$consumerPackageMapper->getPackageByConsumerId($consumer_id,$old_package_id);
																	if($cp){
														            $consumerPackage=$consumerPackageMapper->updatePackageId($consumer_id,$new_package,$old_package_id);
														    		
														    		$is_postpaid=$packages->__get("is_postpaid");
														    		if($is_postpaid!=0){
														    			$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
														    			$meterReading = new Application_Model_MeterReadings();
														    			$date = new Zend_Date();
														    			$date->setTimezone("Asia/Calcutta");
														    			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
														    			$meterReading->__set("consumer_id",$consumer_id);
														    			$meterReading->__set("meter_reading",$meter_reading);
														    			$meterReading->__set("timestamp",$timestamp);
														    			$meterReading->__set("package_id",$old_package_id);
														    			$meterReading->__set("start_reading",1);
																		$meterReading->__set("meter_no",$meter_no);
																		$meterReading->__set("entry_by",$user_id); 
																		$meterReading->__set("entry_type",'M');
														    			$meterReadingsMapper->addNewMeterReading($meterReading);
														    		}
														    		if ($consumerPackage){
														    			$new_packages=$packagesMapper->getPackageById($new_package);
														    			$curr_packages=$packagesMapper->getPackageById($old_package_id);
														    			
														    			$new_package_cost=$new_packages->__get("package_cost");
														    			$curr_package_cost=$curr_packages->__get("package_cost");
														    			
															    			if($curr_package_cost<$new_package_cost)
															    			{
															    				$date = new Zend_Date();
															    				$date->setTimezone("Asia/Kolkata");
															    				$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
															    				$current_date = date_parse_from_format("Y-m-d", $curr_date);
															    				$day = $current_date["day"];
															    				$month = sprintf("%02d", $current_date["month"]);
															    				$year = $current_date["year"];
															    				$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
															    				
															    				$consumers=$consumersMapper->getConsumerById($consumer_id);
															    				$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
															    				$Act_day = $Act_date["day"];
															    				$Act_month = sprintf("%02d", $Act_date["month"]);
															    				$Act_year = $Act_date["year"];
															    				
															    				$lastDebit=$cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year);
															    				if($month==$Act_month && $year==$Act_year){
															    					$lastAmount=($lastDebit/$total_days_in_month)*($day-$Act_day);
															    				}else{
															    					$lastAmount=($lastDebit/$total_days_in_month)*$day;
															    				}
															    				if($is_postpaid==0){
																    				$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
																    				$package_val=($newAmount+$lastAmount)-$lastDebit;
															    				}else {
															    					$unit=$curr_packages->__get("unit");
															    					$extra_charges=$curr_packages->__get("extra_charges");
															    					
															    					$usedUnit=($unit/$total_days_in_month)*($total_days_in_month-$day);
															    					if($reading>$usedUnit){
															    						$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
															    						$unit_amt=$newAmount+(($reading-$usedUnit)*$extra_charges);
															    						$package_val=($unit_amt+$lastAmount)-$lastDebit;
															    					}else{
															    						$newAmount=($new_package_cost/$total_days_in_month)*($total_days_in_month-$day);
															    						$package_val=($newAmount+$lastAmount)-$lastDebit;
															    					}
															    				}
															    				
															    			    $timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
															    				$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
															    				$transaction_id = $timestamp . "-" . $cr_amount;
															    			
															    				$cashRegister = new Application_Model_CashRegister();
															    				$cashRegister->__set("user_id", 0);
															    				$cashRegister->__set("consumer_id", $consumer_id);
															    				$cashRegister->__set("cr_entry_type", "DEBIT");
															    				$cashRegister->__set("cr_amount", intval($package_val));
															    				$cashRegister->__set("receipt_number", 0);
															    				$cashRegister->__set("transaction_id", $transaction_id);
															    				$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
															    				$cashRegister->__set("entry_status", "CHG");
															    				$cashRegister->__set("remark", $remark);
															    				$cashRegister->__set("package_id", $new_package);
															    				$cashRegisterMapper->addNewCashRegister($cashRegister);
															    				$status="Package Updated";
															    			}else{
															    				$status="Package Request Received";
															    			}
														    			
															    			$packages=$packagesMapper->getPackageById($new_package);
															    			$package_cost = $packages->__get("package_cost");
															    			 
															    			$package= new Application_Model_PackageHistory();
															    			$package->__set("package_id",$new_package);
															    			$package->__set("consumer_id",$consumer_id);
															    			$package->__set("package_cost",$package_cost);
															    			$package->__set("change_by",$user_id);
															    			$package->__set("status",$status);
															    			$package->__set("change_description", $remark);
															    			$package->__set("last_package",$old_package_id);
															    			
															    			$packageHistorymapper = new Application_Model_PackageHistoryMapper();
															    			$packageHistory= $packageHistorymapper->addNewPackageHistory($package);
															    				$success=1; 
															    			$result = "Package has been changed successfully.";
														    		}else{
														    			$result = "Error. Kripya kuch der baad try karein.";
														    		}
															}else{
														    			$result = "Error. You do not have this package.";
														    		}
			
           $input=array('api'=>'change_consumer_package',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function addConsumerAction(){
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
		$stateMapper=new Application_Model_StatesMapper();
		$equipmentMapper=new Application_Model_EquipmentMapper();
		$meterInventoryMapper=new Application_Model_MeterInventoryMapper();
		
    	date_default_timezone_set('Asia/Kolkata'); 
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$feeder_id=$request->getParam("feeder_id");
    	$consumer_pole=$request->getParam("consumer_pole");
    	$consumer_name=$request->getParam("name");
    	$father_name=$request->getParam("father_name");
    	$email_id=$request->getParam("email_id");
		$dob=$request->getParam("dob"); 
    	$mobile_number= json_decode($request->getParam("mobile_number"));
     	$consumer_type=$request->getParam("consumer_type");
		 $sub_type_of_me=$request->getParam("consumer_category");  
    	/*$otp_collected=$request->getParam("otp_collected");
		$security_deposit=$request->getParam("security_deposit");
    	$monthly_amount=$request->getParam("monthly_amount");*/
		$charges_arr= json_decode($request->getParam("charges"));
		
		$packages_arr= json_decode($request->getParam("packages"));
    	$schemes= json_decode($request->getParam("schemes"));
    	 
    	$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"feeder_id"=>$feeder_id,
    			"consumer_pole"=>$consumer_pole,
    			"name"=>$consumer_name,
    			"father_name"=>$father_name,
    			"otp_collected"=>$otp_collected,
    			"consumer_type"=>$consumer_type,
    			"monthly_amount"=>$monthly_amount,
    			"email_id"=>$email_id,
				"security_deposit"=>$security_deposit
    	);
		
    	$data=new stdClass(); $files=array(); $success=0;
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");

    	$site = $siteMapper->getSiteById($site_id);
    	$states=$stateMapper->getStateById($site->__get("state_id"));
		
    	$pole_no=sprintf("%03d",$consumer_pole);
    	$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
    	$consumer_connection=$consumersMapper->getConnectionId($site->__get("site_code"),$pole_no);
    	if($consumer_connection){
    		$connection_no = $consumer_connection +1;
    	}else{
    		$connection_no='01';
    	}
    	$consumer_connection_id=$site->__get("site_code").$pole_no.sprintf("%02d",$connection_no);
    	
    	$settingMapper=new Application_Model_SettingsMapper();
    	$setings=$settingMapper->getSettingById(30);
    	if($setings->__get("setting_value")=="no"){
    		$consumer_first_status="use";
    	}else{
    		$consumer_first_status="new";
    	}
    	
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$activation_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
		
    	$consumer_code=0;$mobile_no=array();$mobile_arr=array();
    	if(count($mobile_number)>0){
    		foreach ($mobile_number as $mobile_numbers){
    			if($mobile_numbers->is_primary==1){
    				$consumer_code=$mobile_numbers->mobile_number;
    			}else{
    				$mobile_no[]=$mobile_numbers->mobile_number;
    			}
				$mobile_ar=array(
					"is_primary"=>$mobile_numbers->is_primary,
					"mobile"=>$mobile_numbers->mobile_number
				);
					$mobile_arr[]=$mobile_ar;
    		}
    	}
		$consumer_code_check = $consumersMapper->getMobileNumber($consumer_code);
	if(!$consumer_code_check){
    	$consumer = new Application_Model_Consumers();
      
    	$consumer->__set("site_id",$site_id);
    	$consumer->__set("package_id",0);
    	$consumer->__set("consumer_name",$consumer_name);
    	$consumer->__set("consumer_connection_id",$consumer_connection_id);
    	$consumer->__set("consumer_father_name",$father_name);
    	$consumer->__set("consumer_status","new");
    	$consumer->__set("consumer_code",$consumer_code);
    	$consumer->__set("mobile_no",implode(",", $mobile_no));
    	$consumer->__set("consumer_act_date",$activation_date);
    	$consumer->__set("is_micro_enterprise","0");
    	$consumer->__set("sms_opt_in","1");
    	$consumer->__set("site_meter_id", $feeder_id);
    	$consumer->__set("suspension_date",NULL);
    	$consumer->__set("type_of_me",$consumer_type);
		$consumer->__set("sub_type_of_me",$sub_type_of_me);    
    	$consumer->__set("consumer_first_status",$consumer_first_status);
    	$consumer->__set("email_id",$email_id); 
		$consumer->__set("dob",$dob);  
    	$consumer->__set("created_by",$collectionAgent->__get("collection_agent_id"));
    	$consumer->__set("create_type","(M)");
		
        $consumer_id = $consumersMapper->addNewConsumer($consumer);
    	if($consumer_id){ 
		$package_arr=array(); 
		$scheme_arr=array();
    		$prepaid=0;$postpaid=0; $is_micro_enterprise=0;$micro_price=0;
			$prePaid_con=0;$postPaid_con=0;
    		if(count($packages_arr)>0){
	    	    foreach ($packages_arr as $package){
		    		$packages=$packagesMapper->getPackageById($package->package_id);
		    		$package_data= new Application_Model_ConsumersPackage();
		    		$package_data->__set("package_id",$package->package_id);
		    		$package_data->__set("consumer_id",$consumer_id);
		    		$package_data->__set("type",NULL);
		    		$package_data->__set("charges",$package->unit_rate);
		    		
		    		$consumerPackage=$consumerPackageMapper->addNewConsumersPackage($package_data);
					if($consumerPackage){
						if($packages->__get("is_postpaid")==0){
							$prePaid_con=1;
						}
						if($packages->__get("is_postpaid")!=0){
							$postPaid_con=1;
						}
						
						$watt_total=0;
						$watt=$wattageMapper->getWattageBywattId($packages->__get("wattage"));
						if($watt){
							$watt_total=$watt->__get("wattage");
						}
							//state, mobile_no, transaction_date, connection_id,
							$pack_detail=array(
								"package_id"=>$package->package_id,
								"package_name"=>$packages->__get("package_name"),
								"is_postpaid"=>$packages->__get("is_postpaid"),
								"package_cost"=>$packages->__get("package_cost"),
    							"max_wattage_allowed"=>$watt_total,
    							"min_amount"=>$packages->__get("package_cost"),
    							"free_units"=>$packages->__get("unit"),
    							"rate_after_free_units"=>$packages->__get("extra_charges"),
								"min_unit_rate"=>$packages->__get("min_charges"),
    							"max_unit_rate"=>$packages->__get("max_charges"),
								"state"=>$states->__get("state_name"),
								"connection_id"=>$consumer_connection_id,
								"transaction_date"=>date("d-M-Y g:i a",strtotime($timestamp)),
								"meterNumber"=>$package->meter_no,
								"meterReading"=>$package->meter_reading,
								"mobile_no"=>$mobile_arr,
						 		
							);
							$package_arr[]=$pack_detail;
							
		    			if($packages->__get("is_postpaid")!=0){
		    				$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
		    				$meterReading = new Application_Model_MeterReadings();
		    				$meterReading->__set("consumer_id",$consumer_id);
		    				$meterReading->__set("meter_reading",$package->meter_reading);
		    				$meterReading->__set("timestamp",$timestamp);
		    				$meterReading->__set("package_id",$package->package_id);
							$meterReading->__set("unit",0);
		    				$meterReading->__set("start_reading",1);
		    				$meterReading->__set("meter_no",$package->meter_no); 
							$meterReading->__set("entry_by",$user_id); 
							$meterReading->__set("entry_type",'M');
		    				$meterReadingsMapper->addNewMeterReading($meterReading);
		    				$inventory_last=$meterInventoryMapper->updateMeterStatus($package->meter_no,1,NULL,$consumer_id,$timestamp);
		    				$micro_price=$package->unit_rate;
		    				$is_micro_enterprise=1;
		    				$postpaid=1;
		    			}else{
		    				$prepaid=1;
		    			}
		    			$package_cost = $packages->__get("package_cost");
		    				
		    			$status="Package Added";
		    			$package_data= new Application_Model_PackageHistory();
		    			$package_data->__set("package_id",$package->package_id);
		    			$package_data->__set("consumer_id",$consumer_id);
		    			$package_data->__set("package_cost",$package_cost);
		    			$package_data->__set("change_by",$user_id);
		    			$package_data->__set("status",$status);
		    			$package_data->__set("last_package",NULL);
		    			$packageHistorymapper = new Application_Model_PackageHistoryMapper();
		    			$packageHistory= $packageHistorymapper->addNewPackageHistory($package_data);
		    			 
		    		}
		    	}
	    	
		    	if (count($schemes)>0){
		    		foreach ($schemes as $scheme_val){
						$cur_date = $zendDate->toString("yyyy-MM-dd");
						//$check=$schemeMapper->checkDate($scheme_val->scheme_id,$cur_date)
			    		if ($scheme=$schemeMapper->getSchemeById($scheme_val->scheme_id)) {
							$equip_month_dis=NULL;
			    			
			    			if($scheme->__get("scheme_type")=="equipment"){
			    				$total_month=$scheme->__get("emi_duration");
								$equip_month_dis=$scheme->__get("discount_month");
			    			}elseif ($scheme->__get("gen_scheme_type")=="advance_pay"){
			    				$total_month=$scheme->__get("advance_payment")+$scheme->__get("advance_dis_month");
								$equip_month_dis=$scheme->__get("advance_payment");
			    			}
							$packageMapper = new Application_Model_PackagesMapper();
							$packages_data=$packagesMapper->getPackageById($scheme->__get("package_id"));
							$equipment=($scheme->__get("celamed_id")=="NULL" || $scheme->__get("celamed_id")=='undefined' || $scheme->__get("celamed_id")==NULL)?'':$scheme->__get("celamed_id");
							$equipments=$equipmentMapper->getEquipmentById($equipment); 
							$equip_name="";
							if($equipments){
								$equip_name=$equipments->__get("name");
							}    
							$scheme_data=array(
								"scheme_id"=>($scheme->__get("scheme_id")=="NULL" || $scheme->__get("scheme_id")=='undefined' || $scheme->__get("scheme_id")==NULL)?'':$scheme->__get("scheme_id"),
								"scheme_name"=> ($scheme->__get("scheme")=="NULL" || $scheme->__get("scheme")=='undefined' || $scheme->__get("scheme")==NULL)?'':$scheme->__get("scheme"),
								"down_payment"=> ($scheme->__get("down_payment")=="NULL" || $scheme->__get("down_payment")=='undefined' || $scheme->__get("down_payment")==NULL)?'':$scheme->__get("down_payment"),
								"emi_amt"=> ($scheme->__get("emi_amt")=="NULL" || $scheme->__get("emi_amt")=='undefined' || $scheme->__get("emi_amt")==NULL)?'':$scheme->__get("emi_amt"),
								"emi_duration"=> ($scheme->__get("emi_duration")=="NULL" || $scheme->__get("emi_duration")=='undefined' || $scheme->__get("emi_duration")==NULL)?'':$scheme->__get("emi_duration"),
								"discount_amt"=> ($scheme->__get("discount_amt")=="NULL" || $scheme->__get("discount_amt")=='undefined' || $scheme->__get("discount_amt")==NULL)?'':$scheme->__get("discount_amt"),
								"duration"=> ($scheme->__get("discount_month")=="NULL" || $scheme->__get("discount_month")=='undefined' || $scheme->__get("discount_month")==NULL)?'':$scheme->__get("discount_month"),
								"from_date"=>date('d-M-Y'),
								"to_date"=>date('d-M-Y', strtotime(date('d-M-Y')."+".$equip_month_dis." month")),
								"package_continue"=> $scheme_val->package_continue,
								"scheme_continue"=> $scheme_val->scheme_continue,
								"package_id"=>$packages_data->__get("package_name"),				
								"advance_payment_for_month"=>($scheme->__get("advance_payment")=="NULL" || $scheme->__get("advance_payment")=='undefined' || $scheme->__get("advance_payment")==NULL)?'':$scheme->__get("advance_payment"),
								"discount_for_month"=>($scheme->__get("advance_dis_month")=="NULL" || $scheme->__get("advance_dis_month")=='undefined' || $scheme->__get("advance_dis_month")==NULL)?'':$scheme->__get("advance_dis_month"),
								"flat_discount"=>($scheme->__get("flat_dis")=="NULL" || $scheme->__get("flat_dis")=='undefined' || $scheme->__get("flat_dis")==NULL)?'':$scheme->__get("flat_dis"),
								"percentage_discount"=>($scheme->__get("per_dis")=="NULL" || $scheme->__get("per_dis")=='undefined' || $scheme->__get("per_dis")==NULL)?'':$scheme->__get("per_dis"),
								"equipment"=>$equip_name,
							);  
							$scheme_arr[]=$scheme_data; 
							
			    			$sites=$siteMapper->getSiteById($site_id);
			    			$cluster_id=$sites->__get("cluster_id");
			    			$consumer_schemes = new Application_Model_ConsumerScheme();
			    			$consumer_schemes->__set("consumer_scheme", $scheme->scheme_id);
			    			$consumer_schemes->__set("consumer_id", $consumer_id);
			    			$consumer_schemes->__set("cluster_id", $cluster_id);
			    			$consumer_schemes->__set("site_id", $site_id);
			    			$consumer_schemes->__set("total_month", $total_month);
							$consumer_schemes->__set("equip_month_dis", $equip_month_dis);
							$consumer_schemes->__set("package_continue", $scheme_val->package_continue);
							$consumer_schemes->__set("scheme_continue",$scheme_val->scheme_continue);
							$consumer_schemes->__set("timestamp",$timestamp);
							$consumer_schemes->__set("assign_date",$timestamp); 
			    				
			    			$consumer_schemes_id = $consumerSchemeMapper->addNewConsumerScheme($consumer_schemes);
			    			if($consumer_schemes_id){
			    				if($scheme->__get("scheme_type")=="equipment"){
			    					$consumerPack=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
			    					$down_payment=$scheme->__get("down_payment");
			    					$date = new Zend_Date();
			    					$date->setTimezone("Asia/Kolkata");
			    					$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
			    					 
			    					$cr_amount = str_pad($down_payment, 4, "0", STR_PAD_LEFT);
			    					$transaction_id = $timestamp . "-" . $cr_amount;
			    					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
			    					$cashRegister = new Application_Model_CashRegister();
			    					$cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
			    					$cashRegister->__set("consumer_id", $consumer_id);
			    					$cashRegister->__set("cr_entry_type", "DEBIT");
			    					$cashRegister->__set("cr_amount", $down_payment);
			    					$cashRegister->__set("receipt_number", "CA-" . $collectionAgent->__get("collection_agent_id"));
			    					$cashRegister->__set("transaction_id", $transaction_id);
			    					$cashRegister->__set("transaction_type", "(M)");
			    					$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
			    					$cashRegister->__set("entry_status", "EED");
			    					$cashRegister->__set("remark", "Downpayment");
			    					$cashRegister->__set("package_id", $consumerPack[0]["package_id"]);
			    					$cashRegisterMapper->addNewCashRegister($cashRegister);
			    				}
			    			}
			    			$result="Scheme assigned successfully.";
			    		} else {
			    			$result="Scheme Not Found";
			    		}
		    		}
		    	}
				 
		    	$otp_act=$siteMapper->getChargesbyID($site->__get("otp_act_id"));
		    	$otp_mtr=$siteMapper->getChargesbyID($site->__get("otp_mtr_id"));
		    	 
		    	if(intval($otp_act[0]["charges"])>0){
					$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"DEBIT","ACT",intval($otp_act[0]["charges"]),$consumer_id);
				}
				if(intval($otp_mtr[0]["charges"])>0 && $postPaid_con==1){
					$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"DEBIT","METER",intval($otp_mtr[0]["charges"]),$consumer_id);
				}
				
		    	$totalOTP=0;
		    	if($prepaid==1){$totalOTP=$totalOTP+$otp_act[0]["charges"];}
		    	if($postpaid==1){$totalOTP=$totalOTP+$otp_mtr[0]["charges"];}
		    	 
		    	$consumer->__set("micro_entrprise_price",$micro_price);
		    	$consumer->__set("is_micro_enterprise",$is_micro_enterprise);
		    	$consumer->__set("consumer_act_charge",$totalOTP);
		    	$consumer->__set("consumer_id",$consumer_id);
		    	$consumer->__set("enterprise_type",'FlatUnit');
		    	$consumersMapper->updateConsumer($consumer);
				$total_amount=0; 
				$total_amount_arr=array();
				$otp_transaction_id="";$monthly_transaction_id="";$security_transaction_id="";
				
				$receipt_id=intval($cashRegisterMapper->getMaxUserNumber($collectionAgent->__get("collection_agent_id")))+1;
				if(count($charges_arr)>0){
					foreach ($charges_arr as $charges_val){
						if($charges_val->id==1 && intval($charges_val->amount)>0){
							$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"ACTIVATION","ACT",$charges_val->amount,$consumer_id,$receipt_id);
							$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('ACT');
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$otp_transaction_id=$transaction->__get("transaction_id");	 
							$receipt_number=$transaction->__get("receipt_number");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								"charge_label"=>"OTP Collected",
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>1
							);
							$total_amount_arr[]=$arr;
						}
					
						if($charges_val->id==2 && intval($charges_val->amount)>0){
							$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"CREDIT","METER",$charges_val->amount,$consumer_id,$receipt_id);
							$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('ACT');
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$otp_transaction_id=$transaction->__get("transaction_id");	
							$receipt_number=$transaction->__get("receipt_number");							
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								"charge_label"=>"Meter Collected",
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>1
							);
							$total_amount_arr[]=$arr;
						}
					
						if($charges_val->id==3 && intval($charges_val->amount)>0){
							$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"CREDIT","SD",$charges_val->amount,$consumer_id,$receipt_id);
							$total_amount=$total_amount+intval($charges_val->amount);
							$charges=$packagesMapper->getChargesByparameter('SD'); 
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$security_transaction_id=$transaction->__get("transaction_id");
							$receipt_number=$transaction->__get("receipt_number");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								
								"charge_label"=>"Security Deposit",
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>3
							);
							$total_amount_arr[]=$arr; 
						}
						
						if($charges_val->id==4 && intval($charges_val->amount)>0){ 
							$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"CREDIT","CHG",$charges_val->amount,$consumer_id,$receipt_id);
							$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('CHG');
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$monthly_transaction_id=$transaction->__get("transaction_id");
							$receipt_number=$transaction->__get("receipt_number");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								
								"charge_label"=>"Energy Amount", 
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>4
							);
							$total_amount_arr[]=$arr;
						}
						if($charges_val->id==5 && intval($charges_val->amount)>0){
							$transactionBill=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"CREDIT","OTHERS",$charges_val->amount,$consumer_id,$receipt_id);
							$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"DEBIT","OTHERS",$charges_val->amount,$consumer_id);
							
							$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('CHG');
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$monthly_transaction_id=$transaction->__get("transaction_id");
							$receipt_number=$transaction->__get("receipt_number");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								
								"charge_label"=>"Energy Amount", 
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>4
							);
							$total_amount_arr[]=$arr;
						}
					}
				}
				$packages= json_decode($request->getParam("packages"));
				$schemes= json_decode($request->getParam("schemes"));
				
				$sites=$siteMapper->getSiteById($site_id);
				//state, cluster,and current_date
				$cluster_id=$sites->__get("cluster_id");
			    $state_id=$sites->__get("state_id");
				$states=$stateMapper->getStateById($state_id);
				$data=array(
						"plant"=>$sites->__get("site_name"),
						"state"=>$states->__get("state_name"),
						"feeder_id"=>$feeder_id,
						"consumer_id"=>$consumer_id,
						"consumer_connection_id"=>$consumer_connection_id,
						"consumer_type"=>$consumer_type,
						"primary_mob_no"=>$consumer_code,
						"name"=>$consumer_name,
						"father_name"=>$father_name,
						"consumer_type"=>$consumer_type,
						"consumer_act_charge"=>$totalOTP,
						"otp_collected"=>$otp_collected,
						"monthly_amount"=>$monthly_amount,
						"security_deposit"=>$security_deposit,
						"otp_transaction_id"=>$otp_transaction_id,
						"monthly_transaction_id"=>$monthly_transaction_id,
						"security_transaction_id"=>$security_transaction_id,
						"total_amount"=>$total_amount, 
						"email_id"=>$email_id,
						"datetime"=>date('d-M-Y h:i:s'), 
						"helpLine_no"=>'0000-0000-0000',  
						"receipt_number"=>$sites->__get("site_code").$collectionAgent->__get("collection_agent_id").$receipt_id, 
						"packages"=>$package_arr, 
						"dob"=>$dob, 
						"schemes"=>$scheme_arr,
						"amount"=>$total_amount_arr
						
				);
				$success=1;
		    	$result = "Consumer added successfully";
    		} 
    	}else{
			$result = "Error. Kripya kuch der baad try karein";	
		}
    	}else{
			$result = "Error. Consumer's mobile number is already exist.";	 
		} 
    	$input=array('api'=>'add_consumer',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function addInstantCustomerAction(){
        
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
		$stateMapper=new Application_Model_StatesMapper();
    
    	date_default_timezone_set('Asia/Kolkata');
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$activation_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    	
    	$request = $this->getRequest();
		$user_id=$request->getParam("user_id");
		$user_type=$request->getParam("user_type");
		$site_id=$request->getParam("site_id");
		$name=$request->getParam("name");
		$father_name=$request->getParam("father_name");
		$mobile_number=$request->getParam("mobile_number");
		//$otp_collected=$request->getParam("otp_collected");
		$charges_arr= json_decode($request->getParam("charges"));
		 
		$consumer = new Application_Model_Consumers();
		$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$consumer->__set("site_id",$site_id);
		$consumer->__set("consumer_name",$name);
		$consumer->__set("consumer_status","new");
		$consumer->__set("consumer_code",$mobile_number);
		$consumer->__set("consumer_act_date",$activation_date);
		$consumer->__set("father_name",$father_name); 
  		$consumer->__set("consumer_first_status","new");
	    $consumer->__set("created_by",$collectionAgent->__get("collection_agent_id"));
		$consumer->__set("create_type","(M)");
		$consumer->__set("sms_opt_in","1");
		$consumer_id = $consumersMapper->addNewConsumer($consumer);
		
			if($consumer_id){
				$result="Consumer Added successfully.";
			}else{
				$result="Error. Kripya kuch der baad try karein.";
			}
			$receipt_id=intval($cashRegisterMapper->getMaxUserNumber($collectionAgent->__get("collection_agent_id")))+1;
			if(count($charges_arr)>0){
				foreach ($charges_arr as $charges_val){
					if($charges_val->id==1 && intval($charges_val->amount)>0){
						$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"ACTIVATION","ACT",$charges_val->amount,$consumer_id,$receipt_id);
						$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('ACT');
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$monthly_transaction_id=$transaction->__get("transaction_id");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								
								"charge_label"=>"OTP Collected",
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>1
							);
							$total_amount_arr[]=$arr;
					}
					if($charges_val->id==2 && intval($charges_val->amount)>0){
						$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"CREDIT","METER",$charges_val->amount,$consumer_id,$receipt_id);
						$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('METER');
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$monthly_transaction_id=$transaction->__get("transaction_id");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								
								"charge_label"=>"METER Collected",
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>2
							);
							$total_amount_arr[]=$arr;
					}
					if($charges_val->id==3 && intval($charges_val->amount)>0){
						$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"CREDIT","SD",$charges_val->amount,$consumer_id,$receipt_id);
						$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('SD');
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$monthly_transaction_id=$transaction->__get("transaction_id");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								
								"charge_label"=>"Security Deposit",
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>3
							);
							$total_amount_arr[]=$arr;
					}
					if($charges_val->id==4 && intval($charges_val->amount)>0){
						$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"CREDIT","CHG",$charges_val->amount,$consumer_id,$receipt_id);
						$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('CHG');
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$monthly_transaction_id=$transaction->__get("transaction_id");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								
								"charge_label"=>"Energy Amount", 
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>4
							);
							$total_amount_arr[]=$arr;
					}
					if($charges_val->id==5 && intval($charges_val->amount)>0){
							$transactionBill=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"CREDIT","OTHERS",$charges_val->amount,$consumer_id,$receipt_id);
							$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"DEBIT","OTHERS",$charges_val->amount,$consumer_id);
							
							$total_amount=$total_amount+intval($charges_val->amount);
							$hindi_text="";
							$charges=$packagesMapper->getChargesByparameter('CHG');
							if($charges){ 
								$hindi_text=$charges["hindi_text"];
							}
							$monthly_transaction_id=$transaction->__get("transaction_id");
							$arr=array(
								"transaction_id"=>$transaction->__get("transaction_id"),
								"receipt_number"=>$transaction->__get("receipt_number"),
								"transaction_date"=>date('Y-m-d h:i:s'), 
								
								"charge_label"=>"Energy Amount", 
								"hindi_text"=>$hindi_text,
								"amount"=>$charges_val->amount,
								"serial_number"=>4
							);
							$total_amount_arr[]=$arr;
						}
				}
				$sites = $siteMapper->getSiteById($site_id);
				$otp_act=$siteMapper->getChargesbyID($sites->__get("otp_act_id"));
		    	$otp_mtr=$siteMapper->getChargesbyID($sites->__get("otp_mtr_id"));
		    	
		    	if(intval($otp_act[0]["charges"])>0){
					$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"DEBIT","ACT",intval($otp_act[0]["charges"]),$consumer_id);
				} 
				/*if(intval($otp_mtr[0]["charges"])>0){
					$transaction=$this->rechargeConsumerByType($collectionAgent->__get("collection_agent_id"),"DEBIT","METER",intval($otp_mtr[0]["charges"]),$consumer_id);
				}*/
			} 
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"name"=>$name,
    			"mobile_number"=>$mobile_number,
    			"otp_collected"=>$otp_collected,
    	);
    		
    	$files=array();	$success=1;$data=array();$package_arr=array();$scheme_arr=array();
		
				$sites = $siteMapper->getSiteById($site_id);
				$cluster_id=$sites->__get("cluster_id");
			    $state_id=$sites->__get("state_id");
				$states=$stateMapper->getStateById($state_id);
				
				$data=array(
						"plant"=>$sites->__get("site_name"),
						"state"=>$states->__get("state_name"),
						"feeder_id"=>"",
						"consumer_id"=>$consumer_id,
						"consumer_connection_id"=>"",
						"consumer_type"=>"",
						"primary_mob_no"=>$mobile_number,
						"name"=>$name,
						"father_name"=>$father_name, 
						"consumer_act_charge"=>"",
						"otp_collected"=>"",
						"monthly_amount"=>"",
						"security_deposit"=>"",
						"otp_transaction_id"=>"",
						"monthly_transaction_id"=>"",
						"security_transaction_id"=>"",
						"datetime"=>date('d-M-Y h:i:s'), 
						"helpLine_no"=>'0000-0000-0000', 
						"receipt_number"=>$sites->__get("site_code").$collectionAgent->__get("collection_agent_id").$receipt_id,
						"total_amount"=>$total_amount, 
						"packages"=>$package_arr, 
						"schemes"=>$scheme_arr,
						"amount"=>$total_amount_arr,
				 );
    	
    	$input=array('api'=>'add_instant_customer',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function getCustomerOutstandingAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$activation_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    	 
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$consumer_id=$request->getParam("customer_id");
    	$files=array();	$success=1;
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"customer_id"=>$consumer_id,
    			 
    	);
    	$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
    	$Totaloustanding =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id); 
    	$Totaloustanding_CHG =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"CHG");
    	$Totaloustanding_EED =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"EED");
    	$Totaloustanding_MED =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"MED");
    	$Totaloustanding_WATER =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"WATER");
    	$Totaloustanding_ACT =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"ACT");
		$Totaloustanding_METER =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"METER");
    	$Totaloustanding_OTHERS =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"OTHERS");
        
    	$result="Outstaning data is loaded successfully.";
		$CHG_arr=array(
			"entry_type"=>"ENERGY",
			"outstanding_amount"=>$Totaloustanding_CHG,
		);
		$EED_arr=array(
			"entry_type"=>"EED",
			"outstanding_amount"=>$Totaloustanding_EED,
		);
		$MED_arr=array(
			"entry_type"=>"MED",
			"outstanding_amount"=>$Totaloustanding_MED,
		);
		$ACT_arr=array(
			"entry_type"=>"ACT",
			"outstanding_amount"=>$Totaloustanding_ACT,
		);
		$OTHER_arr=array(
			"entry_type"=>"OTHERS",
			"outstanding_amount"=>$Totaloustanding_OTHERS,
		);
		$WATER_arr=array(
			"entry_type"=>"WATER",
			"outstanding_amount"=>$Totaloustanding_WATER,
		);
		$METER_arr=array(
			"entry_type"=>"METER",
			"outstanding_amount"=>$Totaloustanding_METER,
		);
		$Outstaning=array();
		array_push($Outstaning,$CHG_arr,$EED_arr,$MED_arr,$ACT_arr,$OTHER_arr,$WATER_arr,$METER_arr);
    	 $data=array(
    	 		"Outstanding"=>$Outstaning,
    	 		"totalOutstanding"=>$Totaloustanding
    	 );
       
    	$input=array('api'=>'get_customer_outstanding',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function rechargeConsumerByType($collection_id,$cr_entry_type,$entry_type,$amount,$consumer_id,$receipt_id=NULL){
    	  
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	
		$date = new Zend_Date();
    	$date->setTimezone("Asia/Kolkata");
    	
    	$timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));
    	
    	$tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
    	$transaction_id = $timestamp . "-" . $tcr_amount;
    	$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
		$receipt_ids=NULL;
    	if($cr_entry_type!='DEBIT'){
			$receipt_ids=$receipt_id; 
		}
    	$cashRegister = new Application_Model_CashRegister();
    	$cashRegister->__set("consumer_id", $consumer_id);
    	$cashRegister->__set("cr_entry_type", $cr_entry_type);
    	$cashRegister->__set("cr_amount", $amount);
    	$cashRegister->__set("transaction_id", $transaction_id);
    	$cashRegister->__set("user_id", $collection_id);
    	$cashRegister->__set("receipt_number", $receipt_ids);  
    	$cashRegister->__set("entry_status", $entry_type); 
    	$cashRegister->__set("package_id", $consumerPackages[0]["package_id"]);
    	$cashRegister->__set("transaction_type", "(M)" );
    	
		$date_val = new Zend_Date();
		$date_val->setTimezone("Asia/Calcutta");
		$timestamp_val = $date_val->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp_val,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	if ($cashRegisterMapper->addNewCashRegister($cashRegister)) {
    		return $cashRegister;
    	}else{
    		return false;
    	}
    }
	
	public function getFeederListAction(){
    
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
		$userMapper=new Application_Model_UsersMapper();
		
		$siteMapper=new Application_Model_SitesMapper();
    	
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$from=$request->getParam("from");
    	$to=$request->getParam("to");
    	$skip_count=$request->getParam("skip_count");
    	
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"from"=>$from,
    			"to"=>$to,
    			"skip_count"=>$skip_count,
    	);
    	if($from!=NULL && $to!=NULL){
    		$startDate=$from;
    		$endDate=$to;
    	}else{
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    		
    		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
    		$day=$trans_date["day"];
    		$month= $trans_date["month"];
    		$year= $trans_date["year"];
    		
    		//$startDate=$year."-".$month."-01";
    		//$endDate=$year."-".$month."-".$day; 
			$endDate=$year."-".$month."-".$day;
    		$startDate = date('Y-m-d', strtotime($endDate."-4 day"));
    	}
    	$files=array();	$success=0;
			 if($site_id!=NULL && $site_id!="" && $site_id!='undefined'){
				$sites_val=array($site_id);
				$sites=$siteMapper->getSiteById($site_id);
				$siteName=$sites->__get("site_name");
			 }else{
				$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
				$sites_val=explode(",",$collectionAgents->__get("site_id"));
				$siteName="All Plants";
			 }
			 
    		$sitesMappers=new Application_Model_SitesMapper();
    		$siteMeter=new Application_Model_SiteMasterMeterMapper(); 
    		//$sites=$sitesMappers->getAllSites(null,null,$sites_val);
			$value_arr=array();
    		if(count($sites_val)>0){
    			$success=1;
    			  
    				 $meter_values=array();
    				 $secondNext=$endDate;
	    				while(strtotime($secondNext)>=strtotime($startDate)){
							for($k=0;$k<count($sites_val);$k++){
							$site_arr_val=array($sites_val[$k]);
    						$siteMasterMeters=$siteMeter->getMasterMeterReadingByDate($secondNext,$site_arr_val);
							 
    						$feeder=array();$total_readig=0;
    						if($siteMasterMeters){
    							foreach ($siteMasterMeters as $siteMasterMeter){
    								$total_readig=$total_readig+$siteMasterMeter["reading"];
    								$zendDate = new Zend_Date($siteMasterMeter["reading_date"],"yyyy-MM-dd");
    								$onlytime = $zendDate->toString("HH:mm:ss");
									$sites_value=$siteMapper->getSiteById($siteMasterMeter["site_id"]);
									$siteNames=$sites_value->__get("site_name");
									
										if ($siteMasterMeter["entry_type"]=='(W)') {
										   $users = $userMapper->getUserById($siteMasterMeter["entry_by"]);
										   $Created_By= $users->__get("user_fname") . " " . $users->__get("user_lname");
										} else if($sites_value->__get("spark")!=1)  {
											   $collectionAgent = $collectionAgentsMapper->getCollectionAgentById($siteMasterMeter["entry_by"]);
											   $Created_By=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
										}else{
											$Created_By="Server";
										}
										
    									$data=array(
    										"meter_description" => $siteMasterMeter["description"],
    										"meter_name"=>$siteMasterMeter["meter_name"]."(".$siteMasterMeter["feeder_type"].")(".$siteMasterMeter["description"].")",
			    							"reading"=>round($siteMasterMeter["actual_reading"]),
			    							"reading_time" => $onlytime,
											"entry_by"=>$Created_By,
											"meter_no"=>$siteMasterMeter["meter_no"],
											"unit_con"=>round($siteMasterMeter["reading"]),
											"site_name"=>$siteNames,
											);
    									$feeder[]=$data;
    								}
									
									$zendDate = new Zend_Date($secondNext,"yyyy-MM-dd");
									$date_time_format = $zendDate->toString("dd-MMM-YYYY"); 
    								$data_read=array(
    										"date"=>$date_time_format,
    										"meters" => $feeder,
											"site_name"=>$siteNames,
    										"total_reading"=>round($total_readig)
    								);
									 
    								$meter_values[]=$data_read;
    							}
							}
    						$secondNext = date('Y-m-d', strtotime($secondNext."-1 day"));
								
    					}
    						  
							  $zendDate = new Zend_Date($startDate,"yyyy-MM-dd");
									$startDate = $zendDate->toString("dd-MMM-YYYY"); 
    						$zendDate = new Zend_Date($endDate,"yyyy-MM-dd");
									$endDate = $zendDate->toString("dd-MMM-YYYY"); 
    						
							$data=array(
    							"pant_name"=>$siteName,
    							"from"=>$startDate,
								"to"=>$endDate,
    							"meters"=>$meter_values,
								"feeder_date_calendar"=>'30'
							);
    						 
    			
    			$result="Site Details Found";
    			 
    		}else{
    			$result="Site Details Not Found";
    			 
    			$data=array();
    		}
    	 
    	 
    	$input=array('api'=>'get_feeder_list',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function getFeederListByPlantAction(){
    
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$userMapper=new Application_Model_UsersMapper();
    
    	$siteMapper=new Application_Model_SitesMapper();
    	 
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$from=$request->getParam("from");
    	$to=$request->getParam("to");
    	$skip_count=$request->getParam("skip_count");
		
    	$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"from"=>$from,
    			"to"=>$to,
    			"skip_count"=>$skip_count,
    	);
    	if($from!=NULL && $to!=NULL){
    		$startDate=$from;
    		$endDate=$to;
    	}else{
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    
    		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
    		$day=$trans_date["day"];
    		$month= $trans_date["month"];
    		$year= $trans_date["year"];
    		
    		$endDate=$year."-".$month."-".$day;
    		$startDate = date('Y-m-d', strtotime($curr_date."-1 month"));
    	}
    	$files=array();	$success=0;
    	if($site_id!=NULL && $site_id!="" && $site_id!='undefined'){
    		$sites_val=$site_id;
    		$sites=$siteMapper->getSiteById($site_id);
    		$siteName=$sites->__get("site_name");
    	}else{
    		$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
    		$sites_val=explode(",",$collectionAgents->__get("site_id"));
    		$siteName="All Plants";
    	}
    
    	$sitesMappers=new Application_Model_SitesMapper();
    	$siteMeter=new Application_Model_SiteMasterMeterMapper();
    	//$sites=$sitesMappers->getAllSites(null,null,$sites_val);
    	$value_arr=array();
		
    	if(count($sites_val)>0){
    		$success=1;
    			
    		$meter_values=array();
    		$secondNext=$startDate;
		 
    		while(strtotime($secondNext)<=strtotime($endDate)){
			     
    			$siteMasterMeters=$siteMeter->getMasterMeterReadingByDate($secondNext,array($sites_val));
    
    			$feeder=array();$total_readig=0;
    			//if($siteMasterMeters){
				 
    				$feederBySite=$siteMeter->getSiteMasterById($sites_val);
					//echo " ".count($feederBySite)." ".count($siteMasterMeters)." ";
    				if(count($feederBySite)!=count($siteMasterMeters)){
    				 
						$zendDate = new Zend_Date($secondNext,"yyyy-MM-dd");
						$date_time_format = $zendDate->toString("dd-MMM-YYYY");
						$data_read=array(
								"date"=>$date_time_format,
						);
						
						$meter_values[]=$data_read;
					}
    			//}
    			  $secondNext = date('Y-m-d', strtotime($secondNext."+1 day"));
   
    		}
    
    		$zendDate = new Zend_Date($startDate,"yyyy-MM-dd");
    		$startDate = $zendDate->toString("dd-MMM-YYYY");
    		$zendDate = new Zend_Date($endDate,"yyyy-MM-dd");
    		$endDate = $zendDate->toString("dd-MMM-YYYY");
    
    		$data=array(
    				"pant_name"=>$siteName,
    				"from"=>$startDate,
    				"to"=>$endDate,
    				"meters"=>$meter_values
    		);
    			
    		 
    		$result="Site Details Found";
    
    	}else{
    		$result="Site Details Not Found";
    
    		$data=array();
    	}
    
    
    	$input=array('api'=>'get_feeder_list_By_plant',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function getFeederListByDateAction(){
    
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$userMapper=new Application_Model_UsersMapper();
    
    	$siteMapper=new Application_Model_SitesMapper();
    	 
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$date=$request->getParam("date");
		
    	$date_val = new Zend_Date();
		$date_val->setTimezone("Asia/Calcutta");
		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile'); 
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"date"=>$date,
    	);
    	 
    	$files=array();	$success=0;
    	 
    	$sites=$siteMapper->getSiteById($site_id);
    	$siteName=$sites->__get("site_name");
    	 
    	$sitesMappers=new Application_Model_SitesMapper();
    	$siteMeter=new Application_Model_SiteMasterMeterMapper();
    	
		$value_arr=array();
		$success=1;
    	$meter_values=array();
    	$feeder=array();$total_readig=0;
    			 
				 $feederBySite=$siteMeter->getSiteMasterById($site_id);
    				if($feederBySite){
    				
    				foreach ($feederBySite as $siteMasterMeter){
							$check=$siteMeter->getMasterMeterRecord($siteMasterMeter->__get("id"),$date);
							if($check){
								$status=1; 
								$reading=sprintf("%.2f",$check["actual_reading"]);
								$last_update=$check["reading_date"];
							}else{
								$status=0;
								
								$checks=$siteMeter->getMasterMeterRecord($siteMasterMeter->__get("id"),$date,true); 
								if($checks){
									$reading=sprintf("%.2f",$checks["actual_reading"]);
									$last_update=$checks["reading_date"];
								}else{
									$reading="";
									$last_update="";
								}
							} 
							if($last_update!="" && $last_update!=NULL){
								$zendDate = new Zend_Date($last_update,"yyyy-MM-dd");
								$last_update = $zendDate->toString("dd-MMM-YYYY");
							}
							
							$data_read=array(
									"meter_id"=>$siteMasterMeter->__get("id"),
									"meter_name"=>$siteMasterMeter->__get("meter_name")."(".$siteMasterMeter->__get("feeder_type").")(".$siteMasterMeter->__get("description").") ( ".$siteMasterMeter->__get("start_time")." to ".$siteMasterMeter->__get("end_time").")",
									"last_update"=>$last_update,
									"reading"=>$reading,
									"status"=>$status
							);
							
							$meter_values[]=$data_read;
						}
					}
						
					 
    			 
    
    		$zendDate = new Zend_Date($date,"yyyy-MM-dd");
    		$startDate = $zendDate->toString("dd-MMM-YYYY");
    		 
    		$data=array(
    				"pant_name"=>$siteName,
    				"date"=>$startDate,
    				"meters"=>$meter_values
    		);
    			
    		 
    		$result="Site Details Found";
    
    	 
    	$input=array('api'=>'get_feeder_list_By_plant',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function addFeederDataAction(){
    
    	$sitesMappers=new Application_Model_SitesMapper();
        $siteMeter=new Application_Model_SiteMasterMeterMapper();
        $siteReadingMapper=new Application_Model_SiteMeterReadingMapper();
    	 
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$date=$request->getParam("date");
    	$feeder_readings=json_decode($request->getParam("feeder_readings"));
		
		$sites=$sitesMappers->getSiteById($site_id);
		$siteName=$sites->__get("site_name");
    	$parameter=array(
    		"user_id"=>$user_id,
    		"user_type"=>$user_type,
    		"site_id"=>$site_id,
    		"date"=>$date,
    		"feeder_readings"=>$feeder_readings
    	);
    	 
    	$date_val = new Zend_Date();
		$date_val->setTimezone("Asia/Calcutta");
		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	 
    	$files=array();	$success=0;$data=array();
    	$result=""; 
    	if(count($feeder_readings)>0){
    		foreach ($feeder_readings as $feeder_reading){
    		        $validDateMeterReading=$siteReadingMapper->getDateSiteReadingByMeterId($feeder_reading->feeder_id,$date,$site_id);
					 
					if($validDateMeterReading){
							$siteMeterRead=new Application_Model_SiteMeterReading();
							$meter_name=$siteReadingMapper->getMeterReadingByMeterId($feeder_reading->feeder_id);
							if(!$meter_name){
								
								$siteMeterRead->__set("site_id",$site_id);
								$siteMeterRead->__set("meter_id",$feeder_reading->feeder_id);
								$siteMeterRead->__set("reading_date",$date);
								$siteMeterRead->__set("reading",0);
								$siteMeterRead->__set("actual_reading",$feeder_reading->new_reading);
								$siteMeterRead->__set("entry_by",$user_id);
								$siteMeterRead->__set("entry_type",'(M)'); 
								$Site_MM=$siteReadingMapper->addNewSiteMeterReading($siteMeterRead);
								if($Site_MM){
									$success=1;
									$result .="Site Master Meter Reading updated successfully.";
								}else{
									$result.="Error ".$feeder_reading->feeder_id;
								}
							}elseif(floatval($feeder_reading->new_reading)>=floatval($meter_name[0]->__get("actual_reading"))){
								
								$siteMeterRead->__set("site_id",$site_id);
								$siteMeterRead->__set("meter_id",$feeder_reading->feeder_id);
								$siteMeterRead->__set("reading_date",$date);
								$siteMeterRead->__set("reading",($feeder_reading->new_reading-$meter_name[0]->__get("actual_reading")));
								$siteMeterRead->__set("actual_reading",$feeder_reading->new_reading);
								$siteMeterRead->__set("entry_by",$user_id);
								$siteMeterRead->__set("entry_type",'(M)'); 
								
								$Site_MM=$siteReadingMapper->addNewSiteMeterReading($siteMeterRead);
							 
								if($Site_MM){
									$success=1;
									$result .="Site Master Meter Reading updated successfully.";
								}else{
									$result.="Error ".$feeder_reading->feeder_id;
								}
								 
							 }else{
								$result.="Error ".$feeder_reading->feeder_id." , New reading must be greater then old reading.";
							 }
			      }else{
				    	 $result.="Meter ID ".$feeder_reading->feeder_id. " ki ".$date." ki entry server par already daali jaa chuki hai.";
				  } 
			 }
			 
			$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			
			$trans_date=date_parse_from_format("Y-m-d", $timestamp);
    		$day=$trans_date["day"];
    		$month= $trans_date["month"];
    		$year= $trans_date["year"];
    		
    		$endDate=$year."-".$month."-".$day;
    		$startDate = date('Y-m-d', strtotime($endDate."-1 month"));
			
			$meter_values=array();
    		$secondNext=$startDate; 
		 
    		while(strtotime($secondNext)<=strtotime($endDate)){
			     
    			$siteMasterMeters=$siteMeter->getMasterMeterReadingByDate($secondNext,array($site_id));
    
    			$feeder=array();$total_readig=0;
    			//if($siteMasterMeters){
				 
    				$feederBySite=$siteMeter->getSiteMasterById($site_id);
					
    				if(count($feederBySite)!=count($siteMasterMeters)){
    				 
						$zendDate = new Zend_Date($secondNext,"yyyy-MM-dd");
						$date_time_format = $zendDate->toString("dd-MMM-YYYY");
						$data_read=array(
								"date"=>$date_time_format,
						);
						
						$meter_values[]=$data_read;
					}
    			//}
    			  $secondNext = date('Y-m-d', strtotime($secondNext."+1 day"));
   
    		}
			
			$zendDate = new Zend_Date($startDate,"yyyy-MM-dd");
    		$startDate = $zendDate->toString("dd-MMM-YYYY");
    		$zendDate = new Zend_Date($endDate,"yyyy-MM-dd");
    		$endDate = $zendDate->toString("dd-MMM-YYYY");
			
    		$data=array(
    				"pant_name"=>$siteName,
    				"from"=>$startDate,
    				"to"=>$endDate,
    				"meters"=>$meter_values
    		);
			
    	}else{
    		$result="Please enter feeder details.";
    	}
    	$input=array('api'=>'add_feeder_data',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function getMpptsListAction(){
    
    $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    $siteMapper=new Application_Model_SitesMapper();
	$userMapper=new Application_Model_UsersMapper();
		
    		
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$from=$request->getParam("from");
    	$to=$request->getParam("to");
    	$skip_count=$request->getParam("skip_count");
    	
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"from"=>$from,
    			"to"=>$to,
    			"skip_count"=>$skip_count,
    	);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		
    	if($from!=NULL && $to!=NULL){
    		$startDate=$from;
    		$endDate=$to;
    	}else{
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    		
    		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
    		$day=$trans_date["day"];
    		$month= $trans_date["month"];
    		$year= $trans_date["year"];
    		
    		//$startDate=$year."-".$month."-01";
    		$endDate=$year."-".$month."-".$day;
			$startDate = date('Y-m-d', strtotime($endDate."-4 day"));
    	}
			$files=array();	$success=0;
			 if($site_id!=NULL && $site_id!="" && $site_id!='undefined'){
				$sites_val=array($site_id);
				$sites=$siteMapper->getSiteById($site_id);
				$siteName=$sites->__get("site_name");
			 }else{
				$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
				$sites_val=explode(",",$collectionAgents->__get("site_id"));
				$siteName="All Plants";
			 }
			 
    		$sitesMappers=new Application_Model_SitesMapper();
    		$mpptMeter=new Application_Model_MpptsReadingMapper(); 
    		 
    		if(count($sites_val)>0){
    			$success=1;
					$mppt_values=array();
    				 $secondNext=$endDate;
					 //echo $secondNext." ".$endDate;exit;
	    				while(strtotime($secondNext)>=strtotime($startDate)){
							
							for($k=0;$k<count($sites_val);$k++){
							$site_arr_val=array($sites_val[$k]);
							  
    						$siteMasterMeters=$mpptMeter->getMpptsReadingByDate($secondNext,$site_arr_val); 
    						$feeder=array();$total_readig=0;
    						if($siteMasterMeters){
    							foreach ($siteMasterMeters as $siteMasterMeter){
    								$total_readig=$total_readig+$siteMasterMeter["reading"];
    								$zendDate = new Zend_Date($siteMasterMeter["reading_date"],"yyyy-MM-dd");
    								$onlytime = $zendDate->toString("HH:mm:ss");
									$sites_value=$siteMapper->getSiteById($siteMasterMeter["site_id"]);
									$siteNames=$sites_value->__get("site_name");
									
									if ($siteMasterMeter["entry_type"]=='(W)') {
                                        $users = $userMapper->getUserById($siteMasterMeter["entry_by"]);
                                        $Created_By= $users->__get("user_fname") . " " . $users->__get("user_lname");
									} else if($siteMasterMeter["site_id"]!=46){
										$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($siteMasterMeter["entry_by"]);
										$Created_By=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
									}else{
										$Created_By= "Server";
									}
									
									 
    									$data=array(
    										"mppt_description" => $siteMasterMeter["description"],
    										"mppt_name"=>$siteMasterMeter["mppt_name"],
			    							"reading"=>$siteMasterMeter["reading"],
			    							"reading_time" => $onlytime,
											"updated_by"=>$Created_By,
											"site_name"=>$siteNames,
										);
    									$feeder[]=$data;
								}
									$zendDate = new Zend_Date($secondNext,"yyyy-MM-dd");
									$date_time_format = $zendDate->toString("dd-MMM-YYYY"); 
    								
    								$data_read=array(
    										"date"=>$date_time_format,
    										"mppt" => $feeder,
											"site"=>$siteNames,
    										"total_reading"=>$total_readig
    											
    								);
    								$mppt_values[]=$data_read;
    							}
							  }
    						 $secondNext = date('Y-m-d', strtotime($secondNext."-1 day")); 
							
    						}
    						
							$zendDate = new Zend_Date($startDate,"yyyy-MM-dd");
							$startDate = $zendDate->toString("dd-MMM-YYYY"); 
    						$zendDate = new Zend_Date($endDate,"yyyy-MM-dd");
							$endDate = $zendDate->toString("dd-MMM-YYYY"); 
    								
							$data=array(
    							"pant_name"=>$siteName,
    							"from"=>$startDate,
								"to"=>$endDate,
    							"mppts"=>$mppt_values,
								"mppt_date_calendar"=>'30',
							);
    						 
    				 
    				 
    			
    			$result="Site Details Found";
    			 
    		}else{
    			$result="Site Details Not Found";
    			 
    			$data=array();
    		}
    	 
    	 
    	$input=array('api'=>'get_mppt_list',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function getMpptListByPlantAction(){
    
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$userMapper=new Application_Model_UsersMapper();
    
    	$siteMapper=new Application_Model_SitesMapper();
    	 
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$from=$request->getParam("from");
    	$to=$request->getParam("to");
    	$skip_count=$request->getParam("skip_count");
    	
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"from"=>$from,
    			"to"=>$to,
    			"skip_count"=>$skip_count,
    	);
		
    	if($from!=NULL && $to!=NULL){
    		$startDate=$from;
    		$endDate=$to;
    	}else{
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    
    		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
    		$day=$trans_date["day"];
    		$month= $trans_date["month"];
    		$year= $trans_date["year"];
    		
    		$endDate=$year."-".$month."-".$day;
    		$startDate = date('Y-m-d', strtotime($endDate."-1 month"));
    	}
    	$files=array();	$success=0;
    	if($site_id!=NULL && $site_id!="" && $site_id!='undefined'){
    		$sites_val=$site_id;
    		$sites=$siteMapper->getSiteById($site_id);
    		$siteName=$sites->__get("site_name");
    	}else{
    		$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
    		$sites_val=explode(",",$collectionAgents->__get("site_id"));
    		$siteName="All Plants";
    	}
    
    	$sitesMappers=new Application_Model_SitesMapper();
    	$mpptMeter=new Application_Model_MpptsReadingMapper(); 
    	//$sites=$sitesMappers->getAllSites(null,null,$sites_val);
    	$value_arr=array();
		
    	if(count($sites_val)>0){
    		$success=1;
    			
    		$meter_values=array();
    		$secondNext=$startDate;
		 
    		while(strtotime($secondNext)<=strtotime($endDate)){
			     
    			$siteMasterMeters=$mpptMeter->getMpptsReadingByDate($secondNext,array($sites_val));
    
    			$feeder=array();$total_readig=0;
    			//if($siteMasterMeters){
				  
    				$feederBySite=$mpptMeter->getMpptsById($sites_val);
					
    				if(count($feederBySite)!=count($siteMasterMeters)){
    				 
						$zendDate = new Zend_Date($secondNext,"yyyy-MM-dd");
						$date_time_format = $zendDate->toString("dd-MMM-YYYY");
						$mppt=array(); 
						if($feederBySite){
							foreach($feederBySite as $feederBySites){
								$mppts=$mpptMeter->getMpptsReadingByDateID($feederBySites->__get("id"));
								if($mppts){
									$last_update=$mppts["reading_date"];
									if($mppts["mppt_name"]=='DG' || $mppts["mppt_name"]=='BIOGAS'){
										$reading=$mppts["actual_reading"];
									}else{
										$reading=$mppts["reading"];
									}
									//$reading=$mppts["reading"];
								}else{
									$last_update="";
									$reading="";
								}
							 
									if ($mppts["entry_type"]=='(W)') {
                                        $users = $userMapper->getUserById($mppts["entry_by"]);
                                        $Created_By= $users->__get("user_fname") . " " . $users->__get("user_lname");
									} else if($mppts["site_id"]!=46){
										$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($mppts["entry_by"]);
										$Created_By=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
									}else{
										$Created_By= "Server";
									}
									
							if($last_update!=""){
								$zendDate = new Zend_Date($last_update,"yyyy-MM-dd");
								$last_update = $zendDate->toString("dd-MMM-YYYY");
							}
							 
							$mppts=array(
								"mppt_id"=>$mppts["mppt_id"],
								"mppt_name"=>$mppts["mppt_name"],
								"last_update"=>$last_update,
								"last_reading"=>$reading, 
								"Created_By"=>$Created_By,
								
							);
							$mppt[]=$mppts;
							}
						}
						$data_read=array(
								"date"=>$date_time_format,
								"mppt"=>$mppt
						);
						
						$meter_values[]=$data_read;
					}
    			//}
    			  $secondNext = date('Y-m-d', strtotime($secondNext."+1 day"));
   
    		}
    
    		$zendDate = new Zend_Date($startDate,"yyyy-MM-dd");
    		$startDate = $zendDate->toString("dd-MMM-YYYY");
    		$zendDate = new Zend_Date($endDate,"yyyy-MM-dd");
    		$endDate = $zendDate->toString("dd-MMM-YYYY");
            
    		$data=array(
    				"pant_name"=>$siteName,
    				"from"=>$startDate,
    				"to"=>$endDate,
    				"mppts"=>$meter_values
    		);
    			
    		 
    		$result="Site Details Found";
    
    	}else{
    		$result="Site Details Not Found";
    
    		$data=array();
    	}
    
    
    	$input=array('api'=>'get_mppt_list_By_plant',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function addMpptsDataAction(){
    
    	$sitesMappers=new Application_Model_SitesMapper();
        $mppts=new Application_Model_MpptsMapper();
        $mpptReadingMappers=new Application_Model_MpptsReadingMapper();
    	 $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	 $userMapper=new Application_Model_UsersMapper();
		 
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$date=$request->getParam("date");
    	$mppt_readings=json_decode($request->getParam("mppt_readings"));
		
		$date_val = new Zend_Date();
		$date_val->setTimezone("Asia/Calcutta");
		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$parameter=array(
    		"user_id"=>$user_id,
    		"user_type"=>$user_type,
    		"site_id"=>$site_id,
    		"date"=>$date,
    		"mppt_readings"=>$mppt_readings
    	);
    	
    	$files=array();	$success=0;$data=array();
    	$result="";
		$temp=0;
    	if(count($mppt_readings)>0){
			foreach ($mppt_readings as $mppt_reading){
				$meter_name=$mppts->getMpptsById($mppt_reading->mppt_id);
				if($meter_name->__get("mppt_name")=='DG' || $meter_name->__get("mppt_name")=='BIOGAS'){
					$mppts_val=$mpptReadingMappers->getMpptsReadingByDateID($mppt_reading->mppt_id);
					if($mppts_val){
						if(floatval($mppt_reading->new_reading)<floatval($mppts_val["actual_reading"])){
							$temp=1;
						}
					}
				}
			} 
			if($temp==0){
				foreach ($mppt_readings as $mppt_reading){
    		       	$validDateMeterReading=$mpptReadingMappers->getDateSiteReadingByMpptId($mppt_reading->mppt_id,$date,$site_id);
    			
					if($validDateMeterReading){ 
						$siteMeterRead=new Application_Model_MpptsReading();
						$meter_name=$mppts->getMpptsById($mppt_reading->mppt_id);
						$siteName=$sitesMappers->getSiteById($site_id);
						
						if($meter_name->__get("mppt_name")=='DG' || $meter_name->__get("mppt_name")=='BIOGAS'){
							$mppts_val=$mpptReadingMappers->getMpptsReadingByDateID($mppt_reading->mppt_id);
							if($mppts_val){
								$actualReading=$mppt_reading->new_reading;
								$unit=floatval($mppt_reading->new_reading)-floatval($mppts_val["actual_reading"]);
							}else{
								$actualReading=$mppt_reading->new_reading;
								$unit=$mppt_reading->new_reading;
							}
						}else{
							$actualReading=0;
							$unit=$mppt_reading->new_reading;
						}
						 
						$siteMeterRead->__set("site_id",$site_id);
						$siteMeterRead->__set("mppt_id",$mppt_reading->mppt_id);
						$siteMeterRead->__set("reading_date",$date);
						$siteMeterRead->__set("reading",$unit);
						$siteMeterRead->__set("entry_by",$user_id);
						$siteMeterRead->__set("entry_type",'(M)');
						$siteMeterRead->__set("actual_reading",$actualReading);
						$Site_MM=$mpptReadingMappers->addNewMpptsReading($siteMeterRead);  
							
						if($Site_MM){
							$success=1; 
							$result ="Site MPPTs Reading updated successfully.";
						}else{
							$result.="Error ".$mppt_reading->mppt_id;
						}
					}else{
						$result.="MPPTs ID ".$mppt_reading->mppt_id. " ki ".$date." ki entry server par already daali jaa chuki hai.";
		
					}
				} 
			}else{ 
				$success=0; 
				$result.="Error. Please check DG and Biogas reading.";
			}
					$date = new Zend_Date();
					$date->setTimezone("Asia/Calcutta");
					$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
					
					$trans_date=date_parse_from_format("Y-m-d", $timestamp);
					$day=$trans_date["day"];
					$month= $trans_date["month"];
					$year= $trans_date["year"];
					
					$endDate=$year."-".$month."-".$day;
					$startDate = date('Y-m-d', strtotime($endDate."-1 month"));
					$siteMapper=new Application_Model_SitesMapper();
					$mpptMeter=new Application_Model_MpptsReadingMapper(); 
					$mppt_values=array();
					
    				 $secondNext=$startDate; 
	    				while(strtotime($secondNext)<=strtotime($endDate)){
							
							$siteMasterMeters=$mpptMeter->getMpptsReadingByDate($secondNext,array($site_id));
				
							$feeder=array();$total_readig=0;
							//if($siteMasterMeters){
							  
								$feederBySite=$mpptMeter->getMpptsById($site_id);
								//echo count($feederBySite).count($siteMasterMeters);
								if(count($feederBySite)!=count($siteMasterMeters)){
								 
									$zendDate = new Zend_Date($secondNext,"yyyy-MM-dd");
									$date_time_format = $zendDate->toString("dd-MMM-YYYY");
									$mppt=array(); 
									if($feederBySite){
										foreach($feederBySite as $feederBySites){
											$mppts=$mpptMeter->getMpptsReadingByDateID($feederBySites->__get("id"));
											if($mppts){
												$last_update=$mppts["reading_date"];
												$reading=$mppts["reading"];
											}else{
												$last_update="";
												$reading="";
											}
										
									
											if ($mppts["entry_type"]=='(W)') {
												$users = $userMapper->getUserById($mppts["entry_by"]);
												$Created_By= $users->__get("user_fname") . " " . $users->__get("user_lname");
											} else {
												$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($mppts["entry_by"]);
												$Created_By=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
											}
											
											if($last_update!=""){
												$zendDate = new Zend_Date($last_update,"yyyy-MM-dd");
												$last_update = $zendDate->toString("dd-MMM-YYYY");
											}
											
											$mppts_arr=array(
												"mppt_id"=>$mppts["mppt_id"],
												"mppt_name"=>$mppts["mppt_name"],
												"last_update"=>$last_update,
												"last_reading"=>$reading,
												"Created_By"=>$Created_By,
												
											);
											$mppt[]=$mppts_arr;
										}
									}
									$data_read=array(
											"date"=>$date_time_format,
											"mppt"=>$mppt
									);
									
									$mppt_values[]=$data_read;
								}
							//}
							  $secondNext = date('Y-m-d', strtotime($secondNext."+1 day"));
			   
						}
			 
			$zendDate = new Zend_Date($startDate,"yyyy-MM-dd");
    		$startDate = $zendDate->toString("dd-MMM-YYYY");
    		$zendDate = new Zend_Date($endDate,"yyyy-MM-dd");
    		$endDate = $zendDate->toString("dd-MMM-YYYY");
			
    		$data=array(
    				"pant_name"=>$siteName,
    				"from"=>$startDate,
    				"to"=>$endDate,
    				"mppts"=>$mppt_values 
    		);
			 
    	}else{
    		$result="Please enter Mppt details."; 
    	}
    	$input=array('api'=>'add_feeder_data',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function getCustomerCashLedgerAction(){
    
    	/*$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$userMapper=new Application_Model_UsersMapper();  
    	
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$customer_id=$request->getParam("customer_id");*/    
    	 
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"customer_id"=>$customer_id,
    	);
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$userMapper=new Application_Model_UsersMapper();  
    	
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$customer_id=$request->getParam("customer_id");
    	 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"customer_id"=>$customer_id,
    	);
    		
    	$files=array();	$success=0;$ledger=0;
    	$consumer = $consumersMapper->getConsumerById($customer_id);
    	 
    	if($consumer){
		
			$sites=$sitesMapper->getSiteById($consumer->__get("site_id"));
			if($sites){
				$siteName=$sites->__get("site_name");
			}else{
				$siteName="";
			}
			$MM=$siteMasterMapper->getMMById($consumer->__get("site_meter_id"));
			if($MM){
				$MMName=$MM->__get("meter_name")." (".$MM->__get("description").")"; 
			}else{
				$MMName="";
			}
			$type_of_me=$consumer->__get("type_of_me");
    		$consumer_type_vals=$consumersMapper->getConsumerTypeById($type_of_me);
    		$consumer_types=NULL;
    		if($consumer_type_vals){
    			$consumer_types=$consumer_type_vals["consumer_type_name"];
    		}
			$sub_type_of_me=$consumer->__get("sub_type_of_me");
    		$sub_type_of_me_vals=$consumersMapper->getConsumerTypeById($sub_type_of_me);
    		$sub_consumer_types=NULL;
    		if($sub_type_of_me_vals){
    			$sub_consumer_types=$sub_type_of_me_vals["consumer_type_name"];
    		}
    	
			$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer->__get("consumer_id"));
			$otp_due=$consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
		
    		$success=1;
    		$cashRegister = $cashRegisterMapper->getAllEntryByConsumerId($consumer->__get("consumer_id"));
			$typesArray = array("CREDIT", "DEBIT", "DISCOUNT", "ACTIVATION", "PENALTY");
			$cashRegister = $cashRegisterMapper->applyTypeFilter($cashRegister, $typesArray);
			$cashRegister_data=array();$ledger=array();
			
			if($cashRegister){
				$number=0;
				foreach($cashRegister as $cash_register){
					if($cash_register->__get("cr_status")!='INACTIVE' && $cash_register->__get("entry_status")!='SD'){
						$dr=0;$cr=0;
						if($cash_register->__get("cr_entry_type") == "DEBIT"){
							$dr=$cash_register->__get("cr_amount");
							$debit  = $debit + $cash_register->__get("cr_amount");
						}else{
							$cr=$cash_register->__get("cr_amount");
							$credit = $credit + $cash_register->__get("cr_amount");
						}
						$balance = ($debit - $credit);
						
						$trans_date=date_parse_from_format("Y-m-d", $cash_register->__get("timestamp"));
						$day=$trans_date["day"];
						$month= $trans_date["month"];
						$year= $trans_date["year"];
																	
						$dateObj   = DateTime::createFromFormat('!m', $month);
						$monthName = $dateObj->format('M');
						if($cash_register->__get("cr_entry_type")!='DEBIT'){
							$perticulars="Collection-".$monthName.",".$year."-".$cash["entry_status"];
						}else{
							if($cash_register->__get("entry_status")=='CHG'){
								$packageMapper=new Application_Model_PackagesMapper();
								$packages=$packageMapper->getPackageById($cash_register->__get("package_id"));
								if($packages){
									$perticulars="CHG-".$monthName.",".$year."-".$packages->__get("package_name");
								}else{
									$perticulars="CHG-".$monthName.",".$year."-Server";
								}
							}elseif($cash_register->__get("entry_status")=='EED'){
								$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
								$consumerScheme=$consumerSchemeMapper->getConsumerSchemeByConsumerData($month,$year,$cash_register->__get("consumer_id"));
								if($consumerScheme){
									$totalDiscount=$consumerScheme["discount_month"];
									$RemaingDiscount=$consumerScheme["discount_month"]-$consumerScheme["equip_month_dis"];
								}
								$perticulars="EED-".$monthName.",".$year."-".$RemaingDiscount."/".$totalDiscount;
							}else{
								$perticulars=$cash_register->__get("entry_status")."-".$monthName.",".$year;
							}
						}
																 
						$meter_reading=0;$unit=0;$meter_reading_data=array();
						if($cash_register->__get("cr_entry_type") == "DEBIT" && $cash_register->__get("entry_status")=='CHG'){
							$readingMapper=new Application_Model_MeterReadingsMapper();
							$readingCheck=$readingMapper->checkMonthMeterReadingsByDateConsumerId($cash_register->__get("consumer_id"),NULL,$cash_register->__get("timestamp"));
							if($readingCheck){
								$reading=$readingMapper->getMeterReadingsByConsumerId($cash_register->__get("consumer_id"),NULL,$cash_register->__get("timestamp")); 
								if($reading){
									$meter_reading = sprintf("%.2f",($reading["meter_reading"]));
									$Act_date = date_parse_from_format("Y-m-d", $cash_register->__get("timestamp"));
									$Act_day = $Act_date["day"];
									$Act_month = sprintf("%02d", $Act_date["month"]); 
									$Act_year = $Act_date["year"]; 
									$units=$readingMapper->getTotalUnitByConsumerId($cash_register->__get("consumer_id"),$cash_register->__get("package_id"),$Act_month,$Act_year);
									$unit=sprintf("%.2f", $units);
								}
							}
						}
						if ($cash["cr_entry_type"]!='DEBIT' || $cash["transaction_type"]=='(W)') {
							if ($cash["transaction_type"]=='(W)') {
								$users = $userMapper->getUserById($cash_register->__get("user_id"));
								$createdBy= $users->__get("user_fname") . " " . $users->__get("user_lname");
							} else {
								$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cash_register->__get("user_id"));
								$createdBy= $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
							} 
						}else{
							$createdBy= "Server";
						}													
						$data_value=array(
								"transaction_date"=>date("d-M-Y g:i a",strtotime($cash_register->__get("timestamp"))), 
								"Particulars"=>$perticulars,
								"entry_type"=>$cash_register->__get("entry_status"),
								"created_by"=>$createdBy,
								"credit"=>$cr, 
								"debit"=>$dr,
								"balance"=>$balance,
								"cr_entry_type"=>$cash_register->__get("cr_entry_type"),
								"transaction_status"=>$cash_register->__get("cr_status"),
								"number"=>$number++,
								"unit_count"=>$unit,
								"meter_reading"=>$meter_reading,
						);
						$ledger[]=$data_value;
					}
        	 
    			}
				
				$sort_arr = array();
				foreach ($ledger as $key => $row)
				{
					$sort_arr[$key] = $row['number'];
				}
				array_multisort($sort_arr, SORT_DESC ,$ledger);
				
				$ledgers=array(); 
				foreach($ledger as $cash_registers_val){
					$data_value=array(
								"transaction_date"=>$cash_registers_val["transaction_date"], 
								"Particulars"=>$cash_registers_val["Particulars"], 
								"entry_type"=>$cash_registers_val["entry_type"], 
								"created_by"=>$cash_registers_val["created_by"], 
								"credit"=>$cash_registers_val["credit"], 
								"debit"=>$cash_registers_val["debit"], 
								"balance"=>$cash_registers_val["balance"], 
								"cr_entry_type"=>$cash_registers_val["cr_entry_type"], 
								"transaction_status"=>$cash_registers_val["transaction_status"], 
								"number"=>$cash_registers_val["number"], 
								"unit_count"=>$cash_registers_val["unit_count"], 
						);
					$ledgers[]=$data_value;
				}
				$data=array(
					"ledger"=>$ledgers,
					"plant_name"=>$siteName,
					"feeder_name"=>$MMName,
					"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
					"consumer_status"=>$consumer->__get("consumer_status"),
					"consumer_name"=>$consumer->__get("consumer_name"),
					"consumer_father_name"=>$consumer->__get("consumer_father_name"),
					"mobile_no"=>$consumer->__get("consumer_code"),
					"email"=>($consumer->__get("email_id")==NULL)?"":$consumer->__get("email_id"),
					"consumer_act_date"=>date("d-M-Y",strtotime($consumer->__get("consumer_act_date"))),
					"Consumer_type"=>$consumer_types,
					"consumerSubType"=>$sub_consumer_types,
					"outstanding"=>$oustanding,
					"otp_due"=>$otp_due
				);
    			$result="Data is loading successfully.";
    		}else{
    			$result="Error. Kripya kuch der baad try karein.";
    		}
    	}else{
    		$result="Consumer not found.";
    	}
    	
    	$input=array('api'=>'get_customer_cash_ledger',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }

	public function updatePlantStatusAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$feeder_id=$request->getParam("feeder_id");
    	$down_time=json_decode($request->getParam("down_time"));
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"down_time"=>$down_time,
    	);
    		
    	$files=array();	$success=0;
    		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$siteStatusMapper=new Application_Model_SiteStatusMapper();
    	$MMs=$siteMasterMapper->getMMById($feeder_id);
    	if($MMs){
    		$site_id=$MMs->__get("site_id");
    		$sites=$sitesMapper->getSiteById($site_id);
    		$start_time=$MMs->__get("start_time");
    		$end_time=$MMs->__get("end_time");
    		$is_24_hr=$MMs->__get("is_24_hr");
    	
    	 
    		$maxDates = $siteStatusMapper->getActiveByFeedarId($feeder_id);
    		if($maxDates){
    			$maxDate=$maxDates["timestamp"];
    		}else{
    			$maxDate="0000-00-00";
    		}
    		$temp=0;$allerror=array();
    		foreach($down_time as $down_times){
    			$allHours=explode(",", $down_times->hours);
    			for($j=0;$j<count($allHours);$j++){
	    			  if($down_times->date >= $maxDate){
	    					$siteStatus=new Application_Model_SiteStatus();
	    					$siteStatus->__set("site_id",$site_id);
	    					$siteStatus->__set("status","INACTIVE");
	    					$siteStatus->__set("timestamp",$down_times->date);
	    					$siteStatus->__set("feedar_id",$feeder_id);
	    					$siteStatus->__set("start_date",$allHours[$j]);
							$siteStatus->__set("entry_by",$user_id);
							$siteStatus->__set("entry_type",'Mobile');
							$siteStatus->__set("entry_date",$timestamp);
	    					$statsite = $siteStatusMapper->addNewSiteStatus($siteStatus);
	    					$temp=1;
	    				}else{
	    					$allerror[]=$down_times->date;
	    				}
    				}
    			}
    		if ($temp==1)
    		{
				$statsiteData = $siteStatusMapper->getSiteStatusByFeedarId($feeder_id);
				if($statsiteData){
					$timeDate_val=date('Y-m-d', strtotime($statsiteData[0]["timestamp"]."+1 day"));
				
					$date = new Zend_Date();
					$date->setTimezone("Asia/Calcutta");
					$ValidTimestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
										
					$siteStatus=new Application_Model_SiteStatus();
					$siteStatus->__set("site_id",$site_id);
					$siteStatus->__set("status","ACTIVE");
					$siteStatus->__set("timestamp",$timeDate_val);
					$siteStatus->__set("feedar_id",$feeder_id);
					$siteStatus->__set("start_date",0);
					$siteStatus->__set("entry_by",$user_id);
					$siteStatus->__set("entry_type",'Mobile');
					$siteStatus->__set("entry_date",$ValidTimestamp);
					$statsiteValid = $siteStatusMapper->addNewSiteStatus($siteStatus);
					
					$success=1;
					$result="Records inserted successfully.";
					$data = array(
							'Site_id' => $site_id,
							'Site Name' => $sites->__get("site_name"),
							'feeder_id' => $feeder_id,
					);
				}else{
					$result = "Error. Please try after some time.";
					$data=new stdClass();
				}
    		}else{
    			$result = "Error. Invalid dates ".implode(",", array_unique($allerror));
    			$data=new stdClass();
    		}
    	
    	} else{
    		$result="SiteId is not found, Please check";
    		$data=new stdClass();
    	} 
    	
    	$input=array('api'=>'update-plant-status',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function getPlantStatusDataAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
		$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$feeder_id=$request->getParam("feeder_id");
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"feeder_id"=>$feeder_id,
    	);
    	$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');	
    	$files=array();	$success=0;
    	$data=array();
    	$dates=array();
    	$MMs=$siteMasterMapper->getMMById($feeder_id);
    	if($MMs){
    		$site_id=$MMs->__get("site_id");
    		$start_time=$MMs->__get("start_time");
    		$end_time=$MMs->__get("end_time");
    		$is_24_hr=$MMs->__get("is_24_hr");
    		 
    		$siteStatusMapper=new Application_Model_SiteStatusMapper();
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
    		$day= $trans_date["day"];
    		$month= $trans_date["month"];
    		$year= $trans_date["year"];
    	
    		$statsite = $siteStatusMapper->getSiteStatusByFeedarId($feeder_id);
    		if($statsite){
    			$success=1;
    	
    			$maxDates = $siteStatusMapper->getActiveByFeedarId($feeder_id);
    			$maxDate=$maxDates["timestamp"];
    			if($maxDates){
    				$maxDate=$maxDates["timestamp"];
    			}else{
    				$maxDate="0000-00-00";
    			}
    			foreach ($statsite as $statsites)
    			{
    				$AllDates[]=$statsites["timestamp"];
    			}
    	
    			$totalDaysInMonth=cal_days_in_month(CAL_GREGORIAN,$month,$year);
    	
    			for($i=1; $i<=$totalDaysInMonth; $i++){
    				$Alltimes=array();
    				$start_date=$year."-".sprintf("%02d",$month)."-".sprintf("%02d",$i);
    	
    				$daystat = $siteStatusMapper->getdayStatusByFeedarId($feeder_id,$start_date);
    				if($daystat){
    					foreach ($daystat as $daystats)
    					{
    						$Alltimes[]=$daystats["start_date"];
    					}
    				}
    				$times_val=array();
    				if($is_24_hr!=1){
    					for($j=intval($start_time);$j!=intval($end_time);$j++){
    						$start_date_time=$start_date." ".sprintf("%02d",$j).":00:00";
    	
    						if($start_date_time<=$maxDate){
    							$set=1;
    						}else{
    							$set=0;
    						}
    						if(in_array($start_date, $AllDates)){
    							if(in_array($j, $Alltimes)){
    								$active=0;
    							}else{
    								$active=1;
    							}
    						}else{
    							$active=1;
    						}
    						$times=array(
    								'hr' => sprintf("%02d",$j),
    								'active' => $active,
    								'set'=> $set,
    						);
    						$times_val[]=$times;
    						if($j==23){
    							$j=-1;
    						}
    					}
    				}
    	
    	
    				$result="Feedar Detail Found Successfully.";
    				$data_val = array(
    						'date' => $start_date,
    						'time'=>$times_val
    				);
    				$data[]=$data_val;
    			}
    			$dates=array( "dates"=>$data 	);
    		}else{
    			$success=1;
    			$totalDaysInMonth=cal_days_in_month(CAL_GREGORIAN,$month,$year);
    				
    			$maxDates = $siteStatusMapper->getActiveByFeedarId($feeder_id);
    			$maxDate=$maxDates["timestamp"];
    			if($maxDates){
    				$maxDate=$maxDates["timestamp"];
    			}else{
    				$maxDate="0000-00-00";
    			}
    			for($i=1; $i<=$totalDaysInMonth; $i++){
    				$times_val=array();
    				$start_date=$year."-".sprintf("%02d",$month)."-".sprintf("%02d",$i);
    				if($is_24_hr!=1){
    					for($j=$start_time;$j!=$end_time;$j++){
    						$start_date_time=$start_date." ".sprintf("%02d",$j).":00:00";
    	
    						if($start_date_time<=$maxDate){
    							$set=1;
    						}else{
    							$set=0;
    						}
    						$times=array(
    								'hr' => sprintf("%02d",$j),
    								'active' => 1,
    								'set'=> $set,
    						);
    						$times_val[]=	$times;
    						if($j==23){
    							$j=-1;
    						}
    	
    					}
    				}
    				$result="Feedar Detail Found Successfully.";
    				$data_val = array(
    						'date' => $start_date,
    						'time'=>$times_val
    						 
    				);
    				$data[]=$data_val;
    			}
    			$dates=array( 	"dates"=>$data 	);
    		}
    		 
    	} else{
    		$result="Feedar Id is not found, Please check";
    		$data=new stdClass();
    	}
    	 
    	$input=array('api'=>'get-plant-status-data',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function addDieselConsumptionDataAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    	$dgMapper=new Application_Model_DgMapper();
    	 
    	//user_id, user_type, purchase_date, total_amount, rate, unit, (image) for $_Files parameter
    	
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$purchase_date=$request->getParam("purchase_date");
    	$total_amount=$request->getParam("total_amount");
    	$rate=$request->getParam("rate");
    	$unit=$request->getParam("unit");
		$invoice_no=$request->getParam("invoice_no");
    	$upload_dir = "angular_app/components/dg/images/";
    	$fileName=NULL;
		 $temp=0;
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	if(isset($_FILES["file"]["type"]))
    	{
    		$fileName=$_FILES["file"]["name"];
    		$validextensions = array("jpeg", "jpg", "png", "gif");
    		$temporary = explode(".", $_FILES["file"]["name"]);
    		$file_extension = end($temporary);
    		if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg")) && in_array($file_extension, $validextensions)) {
    			if ($_FILES["file"]["error"] > 0){
    					$_FILES["file"]["error"];
    			} else {
    				if (file_exists($upload_dir.$_FILES["file"]["name"])) {
    					 $temp=0;
    				} else {
    					$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
    					$filename = $_FILES['file']['name'];
    					$targetPath = $upload_dir.$filename; // Target path where file is to be stored
    					move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
    				 
    				}
    			}
    		}
    	} 
    	 
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"purchase_date"=>$purchase_date,
    			"total_amount"=>$total_amount,
    			"rate"=>$rate,
    			"unit"=>$unit,
				"invoice_no"=>$invoice_no
    	);
    
    	$files=array($_FILES["file"]);	$success=0;$data=array();
    
    	$DgMapper=new Application_Model_DgMapper();
    	$Dg= new Application_Model_Dg();
    	$Dg->__set("site_id",$site_id);
    	$Dg->__set("date_of_purchase",$purchase_date);
    	$Dg->__set("rate",$rate);
    	$Dg->__set("amount",$total_amount);
    	$Dg->__set("unit",$total_amount/$rate);    
    	$Dg->__set("bill_attach",$fileName);
		$Dg->__set("invoice_no",$invoice_no);
    	if($temp==0){ 
			if($DgId=$DgMapper->addNewDG($Dg)){
				$success=1;
				$result="Deisel genretaion is added successfully.";
			}else{
				$result="Error. Kripya kuch der baad try karein.";
			}
    	 }else{
			$result="Error. File already exist.";
		 }
    
    	$input=array('api'=>'add-diesel-consumption-list',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data,
    			 
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function getDieselConsumptionListAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    	$dgMapper=new Application_Model_DgMapper();
    	
    	
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$from=$request->getParam("from");
    	$to=$request->getParam("to");
    	$skip_count=$request->getParam("skip_count");
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"from"=>$from,
    			"to"=>$to,
    			"skip_count"=>$skip_count,
    	);
		if($from==NULL && $to==NULL){
			
			$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			
		 	$trans_date=date_parse_from_format("Y-m-d", $timestamp);
			$day = $trans_date["day"];
		 	$month = $trans_date["month"];
		 	$year= $trans_date["year"];
			
			$to=$year."-".$month."-".$day;
    		$from = date('Y-m-d', strtotime($to."-1 month"));
		} 
    	$files=array();	$success=0;
    
    	$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
    	if($collectionAgents){
		if($site_id==NULL || $site_id ==""){
    		if($collectionAgents->__get("collection_agent_id")!=0){
    			$sites=$sitesMapper->getSitesByCollectionSite(explode(",",$collectionAgents->__get("site_id")));
    		}else{
    			$sites=$sitesMapper->getSitesByCollectionSite();
    		}
    		$site_ids=array();
    		if($sites){
				foreach($sites as $site){
					$site_ids[]= $site["site_id"];
    			}
    		}
			}else{
				$site_ids =  array($site_id);
			}
    		$data_all=array();$total_unit=0;
    		if(count($site_ids)>0){
    			$dgs=$dgMapper->getAllDgBySiteId($site_ids,$from,$to,$skip_count);
    			if($dgs){
    				foreach ($dgs as $dg){
						$total_unit=$total_unit+$dg["unit"];
    					$sites_data=$sitesMapper->getSiteById($dg["site_id"]);
    					$data_val=array(
    							"image_url,"=>"http://taraurja.in/angular_app/components/dg/images/".$dg["bill_attach"],
    							"plant_name"=>$dg["site_name"],
    							"purchase_date"=>date("d-M-Y g:i a",strtotime($dg["date_of_purchase"])),
								"entry_date"=>date("d-M-Y g:i a",strtotime($dg["timestamp"])),
    							"amount"=>$dg["amount"],
    							"unit"=>$dg["unit"],
    							"rate"=>$dg["rate"],
								"invoice_no"=>$dg["invoice_no"]
    					);
    					$data_all[]=$data_val;
    				}
    			}
    		}
			
			 
    		if($site_id!=null && $site_id!=""){
				$sites_data=$sitesMapper->getSiteById($site_id);
				$site_name=$sites_data->__get("site_name"); 
			}else{
				$site_name="All Plants";
			}
			    
    		$data=array(
    				"Plants"=>$data_all,
    				"site_id"=>$site_id,
					"site_name"=>$site_name,
    				"from"=>date("d-M-Y",strtotime($from)),
    				"to"=>date("d-M-Y",strtotime($to)),
    				"skip_count"=>$skip_count,
					"total_unit"=>$total_unit,
					"dg_range_calender"=>"30"
    		);
    		$success=1;
    		$result = "Data is loaded successfully.";
    	}else{
    		$data=array();
    		$result = "Error. You are not an Agent.";
    	}
    
    	$input=array('api'=>'get-diesel-consumption-list',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data,
    			
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
     
    public function getBankDepositListAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    	$CollectionMapper=new Application_Model_CollectionsMapper();
    	 
    	 
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$from=$request->getParam("from");
    	$to=$request->getParam("to");
    	$skip_count=$request->getParam("skip_count");
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"from"=>$from,
    			"to"=>$to,
    			"skip_count"=>$skip_count,
    	);
    $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$files=array();	$success=0;
		
		if($from==NULL && $to==NULL){
			 
			$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
			
		 	$trans_date=date_parse_from_format("Y-m-d", $timestamp);
			$day = $trans_date["day"];
		 	$month = $trans_date["month"];
		 	$year= $trans_date["year"];
			
			$to=$year."-".$month."-".$day;
    		$from = date('Y-m-d', strtotime($to."-1 month"));
		}
		
    	$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
    	if($collectionAgents){
			if($site_id==NULL || $site_id ==""){
				if($collectionAgents->__get("collection_agent_id")!=0){
					$sites=$sitesMapper->getSitesByCollectionSite(explode(",",$collectionAgents->__get("site_id")));
				}else{
					$sites=$sitesMapper->getSitesByCollectionSite();
				}
				$site_ids=array();
				if($sites){
					foreach($sites as $site){
						$site_ids[]= $site["site_id"];
					}
				}
			}else{
				$site_ids=array($site_id);   
			}
    		$data_all=array();$totalColl=0;
    		if(count($site_ids)>0){
    			$collections=$CollectionMapper->getAllDepositBySiteId($site_ids,$from,$to,$skip_count);
    			if($collections){
    				foreach ($collections as $collection){
    					$sites_data=$sitesMapper->getSiteById($collection["site_id"]);
						$totalColl=$totalColl+$collection["amount"];
    					$data_val=array(
    							"image_url,"=>"http://taraurja.in/angular_app/components/collections/images/".$collection["attachment"],
    							"plant_name"=>$collection["site_name"],
    							"timestamp"=>date("d-M-Y g:i a",strtotime($collection["timestamp"])),
								"entry_date"=>date("d-M-Y g:i a",strtotime($collection["curr_timestamp"])),
    							"amount"=>$collection["amount"],
    							"deposit_by"=>$collection["deposit_by"],
    							"deposit_location"=>$collection["deposit_location"],
    					);
    					$data_all[]=$data_val;
    				}
    			}
    		}
			
			if($site_id!=null && $site_id!=""){
				$sites_data=$sitesMapper->getSiteById($site_id);
				$site_name=$sites_data->__get("site_name"); 
			}else{
				$site_name="All Plants";
			}
			  
    		$data=array(
    				"Plants"=>$data_all,
    				"site_id"=>$site_id,
					"site_name"=>$site_name, 
    				"from"=>date("d-M-Y",strtotime($from)),
    				"to"=>date("d-M-Y",strtotime($to)),
    				"skip_count"=>$skip_count,
					"total_amount"=>$totalColl,
					"bank_deposit_range_calender"=>"30"					
    		);
    		$success=1;
    		$result = "Data is loaded successfully.";
    	}else{
    		$data=array();
    		$result = "Error. You are not an Agent.";
    	}
    
    	$input=array('api'=>'get-bank-deposit-list',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data,
    			 
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function addBankDepositListAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    	$dgMapper=new Application_Model_DgMapper();
    
    	 
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$deposit_date=$request->getParam("deposit_date");
    	$deposit_amount=$request->getParam("deposit_amount");
    	$deposit_by=$request->getParam("deposit_by");
    	$deposit_location=$request->getParam("deposit_location");
    	$upload_dir = "angular_app/components/collections/images/"; 
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$fileName=NULL;
		 $temp=0;
    	if(isset($_FILES["file"]["type"]))
    	{
    		$fileName=$_FILES["file"]["name"];
    		$validextensions = array("jpeg", "jpg", "png", "gif");
    		$temporary = explode(".", $_FILES["file"]["name"]);
    		$file_extension = end($temporary);
    		if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg")) && in_array($file_extension, $validextensions)) {
    			if ($_FILES["file"]["error"] > 0){
    				$_FILES["file"]["error"];
    			} else {
    				if (file_exists($upload_dir.$_FILES["file"]["name"])) {
						$temp=0; 
    				} else {
    					$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
    					$filename = $_FILES['file']['name'];
    					$targetPath = $upload_dir.$filename; // Target path where file is to be stored
    					move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
    						
    				}
    			}
    		}
    	}
    	 
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"deposit_date"=>$deposit_date,
    			"deposit_amount"=>$deposit_amount,
    			"deposit_by"=>$deposit_by,
    			"deposit_location"=>$deposit_location,
    	);
    
    	$files=array($_FILES["file"]);	$success=0;$data=array();
    
    	$collectionMapper=new Application_Model_CollectionsMapper();
    	$coll= new Application_Model_Collections();
    	$coll->__set("site_id",$site_id);
    	$coll->__set("timestamp",$deposit_date);
    	$coll->__set("amount",$deposit_amount);
    	$coll->__set("deposit_by",$deposit_by);
    	$coll->__set("deposit_location",$deposit_location);
    	$coll->__set("attachment",$fileName);
		$coll->__set("user_id",$user_id);
    	if($temp==0){
			if($CollId=$collectionMapper->addNewCollections($coll)){
				$success=1;
				$result="Bank deposit is added successfully.";
			}else{
				$result="Error. Kripya kuch der baad try karein.";
			}
		}else{
			$result="Error. File already exist.";
		}
    
    	$input=array('api'=>'get-diesel-consumption-list',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data,
    
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
   
	public function changeFeederMeterAction(){
    
      	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$feeder_id=$request->getParam("feeder_id");
    	$old_meter_reading=$request->getParam("old_meter_reading");
    	$new_meter_no=$request->getParam("new_meter_no");
    	$new_meter_reading=$request->getParam("new_meter_reading");
    	 
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"feeder_id"=>$feeder_id,
    			"old_meter_reading"=>$old_meter_reading,
    			"new_meter_no"=>$new_meter_no,
    			"new_meter_reading"=>$new_meter_reading,
    	);
    	
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$files=array();	$success=0;
    	$siteReadingMapper=new Application_Model_SiteMeterReadingMapper();
    	$feederMapper=new Application_Model_SiteMasterMeterMapper();
		$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
    	 if($collectionAgents){
    	 	$date = new Zend_Date(); 
    	 	$date->setTimezone("Asia/Calcutta");
    	 	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    	 	
    	 	$feeder_val=$feederMapper->getMMById($feeder_id);
    	 	
    			$feeder=new Application_Model_SiteMeterReading();
    			$feeder->__set("site_id",$feeder_val->__get("site_id"));
             	$feeder->__set("meter_id",$feeder_id);
             	$feeder->__set("reading_date",$timestamp);
             	$feeder->__set("reading",$new_meter_reading);
             	$feeder->__set("meter_no",$new_meter_no);
             	$feeder->__set("reset",1);
             	
	    	$siteMeter=$siteReadingMapper->addNewSiteMeterReading($feeder);
	    	$success=1;
			$data=array();
	    	$result="Data is loading successfully.";
    	 }else {
    	 	$data=array();
    	 	$result="You Are not agent.";
    	 }
    	
    	 
    	$input=array('api'=>'update_feeder_meter',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function consumerTempDisconnectAction(){
    
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	 
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	$active_consumers=$request->getParam("active_consumers");
    	$inactive_consumers=$request->getParam("inactive_consumers");
    	
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    			"active_consumers"=>$active_consumers,
    			"inactive_consumers"=>$inactive_consumers,
    	);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$files=array();	$success=0;
    	$consumerDisconnectMapper=new Application_Model_ConsumerDisconnectionMapper();
    	
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    	 
    	if($active_consumers!=NULL && $active_consumers!="" && $active_consumers!='undefined'){
    		
    		$consumerDisconnect=new Application_Model_ConsumerDisconnection();
    		$allActiveCon=explode(",", $active_consumers);
    		if(count($allActiveCon)>0){
    			$updateConsumer=$consumerDisconnectMapper->updateConsumerTempStatus(0,$allActiveCon);
    			if($updateConsumer){
    				$success=1;
		    		for ($i=0;$i<count($allActiveCon);$i++){
		    			$consumerDisconnect->__set("consumer_id", $allActiveCon[$i]);
		    			$consumerDisconnect->__set("status", 'active');
		    			$consumerDisconnect->__set("user_id", $user_id);
		    			$consumerDisconnect->__set("user_type", "(M)");
		    			$consumerDisconnect->__set("timestamp", $timestamp);
		    			$consumerDisconnectMapper->addConsumerDisconnection($consumerDisconnect);
		    		}
    			} 
    		}
    		 
    	}
    	if($inactive_consumers!=NULL && $inactive_consumers!="" && $inactive_consumers!='undefined'){
    		
    		$consumerDisconnect=new Application_Model_ConsumerDisconnection();
    		$allInactiveCon=explode(",", $inactive_consumers);
    		if(count($allInactiveCon)>0){
    			$updateConsumer=$consumerDisconnectMapper->updateConsumerTempStatus(1,$allInactiveCon); 
    			if($updateConsumer){
    				$success=1;
	    			for ($i=0;$i<count($allInactiveCon);$i++){
	    				$consumerDisconnect->__set("consumer_id", $allInactiveCon[$i]);
	    				$consumerDisconnect->__set("status", 'inactive');
	    				$consumerDisconnect->__set("user_id", $user_id);
	    				$consumerDisconnect->__set("user_type", "(M)");
	    				$consumerDisconnect->__set("timestamp", $timestamp);
	    				$consumerDisconnectMapper->addConsumerDisconnection($consumerDisconnect);
	    			}
    			}
    		}
    	}
    	 
    	if($success==1){
			$data=array();
    		$result="Data is inserted successfully.";
    	}else {
    		$data=array();
    		$result="Something went wrong.";
    	}
    	 
    
    	$input=array('api'=>'consumer_temp_disconnect',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function getTemporaryDisconnectConsumersAction(){
    
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
    	  
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"site_id"=>$site_id,
    	);
    	$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$files=array();	$success=0;
    	$consumerDisconnectMapper=new Application_Model_ConsumerDisconnectionMapper();
    	$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    	
    	$consumerDisconnects=$consumerDisconnectMapper->getConsumerTempConnection($site_id);
    	$consumer_arr=array();
    	if($consumerDisconnects){
    		foreach ($consumerDisconnects as $consumer){
    			$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer["consumer_id"]);
    			$package_arr=array();
    			if($consumerpackage){
    			
    				foreach($consumerpackage as $package){
    					$watt_total=0;
    					$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
    					if($watt){
    						$watt_total=$watt->__get("wattage");
    					}
    					if($package["is_postpaid"]==0){
    						$reading="";
    					}else{
    						$latestReading=$meterReadingMapper->getLatestReadingByConsumerId($consumer["consumer_id"],$package["package_id"]);
    						if($latestReading){
    							$reading=$latestReading->__get("meter_reading");
    						}else{$reading="";}
    					}
    					$pack_data=array(
    							"package_id"=>$package["package_id"],
    							"package_name"=>$package["package_name"],
    							"package_cost"=>$package["package_cost"],
    							"is_postpaid"=>$package["is_postpaid"],
    								
    							"max_wattage_allowed"=>$watt_total,
    							"min_amount"=>$package["package_cost"],
    							"free_units"=>$package["unit"],
    							"rate_after_free_units"=>$package["extra_charges"],
    							"min_unit_rate"=>$package["min_charges"],
    							"max_unit_rate"=>$package["max_charges"],
    								
    							"wattage"=>$watt_total,
    							"status"=>$package["status"],
    							"meter_reading"=>$reading
    					);
    					$package_arr[]=$pack_data;
    				}
    			}
    			if($consumer["temp_dis"]==0){
					$status='active';
				}else{
					$status='banned';
				}
				
    			$data = array(
    					'consumer_id' => $consumer["consumer_id"],
    					'mobile_no' => $consumer["consumer_code"],
    					'consumer_name' =>strtoupper($consumer["consumer_name"]),
    					'consumer_father_name' => $consumer["consumer_father_name"],
    					'consumer_status' => $status,
    					'consumer_act_date' => $consumer["consumer_act_date"],
    					'consumer_act_charge' => $consumer["consumer_act_charge"],
    					'consumer_connection_id' => $consumer["consumer_connection_id"],
    					'type_of_me' => $consumer["type_of_me"],
    					'feeder_id' => $consumer["site_meter_id"],
    					"packages"=>$package_arr
    			);
    			$consumer_arr[]=$data;
    			
    			$success=1;
    		}
    	}
    	if($success==1){
    		$data=$consumer_arr;
    		$result="Data is loaded successfully.";
    	}else {
    		$data=array();
    		$result="No Record found.";
    	}
    
    
    	$input=array('api'=>'get_temp_disconnect',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function updateConsumerTypeAction(){
    
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$consumer_id=$request->getParam("consumer_id");
    	$consumer_category=$request->getParam("consumer_category");
		$consumer_type=$request->getParam("consumer_type");
    	
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"consumer_id"=>$consumer_id,
    			"consumer_type"=>$consumer_type,
				"consumer_category"=>$consumer_category,
    	);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$files=array();	$success=0;
    	$consumerMapper=new Application_Model_ConsumersMapper();
    	 
    	 if($consumer_id!=NULL && $consumer_id!="" && $consumer_id!='undefined'){
     	 	
    	 	$updateConsumer=$consumerMapper->updateConsumerType($consumer_type,$consumer_category,$consumer_id); 
    		if($updateConsumer){
				$success=1; 
    			$data=array();
    			$result="Consumer Type is updated successfully.";
    		}else {
    			$data=array();
    			$result="Something went wrong.";
    		}
    	 }else {
    		$data=array();
    		$result="Something went wrong.";
    	}
    	 
     
    
    	$input=array('api'=>'consumer_temp_disconnect',
    			'parameters'=>$parameter); 
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function updateConsumerMobAction(){
      
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$consumer_id=$request->getParam("consumer_id");
    	$mob_no=$request->getParam("mob_no");
    	
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"consumer_id"=>$consumer_id,
    			"mob_no"=>$mob_no,
    	);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$files=array();	$success=0;
    	$consumerMapper=new Application_Model_ConsumersMapper();
    	 
    	 if($consumer_id!=NULL && $consumer_id!="" && $consumer_id!='undefined'){
     	 	
    	 	$updateConsumer=$consumerMapper->updatePrimaryNoStatus($consumer_id,$mob_no);
    		if($updateConsumer){
				$success=1; 
    			$data=array();
    			$result="Primary no is updated successfully.";
    		}else {
    			$data=array();
    			$result="Something went wrong.";
    		}
    	 }else {
    		$data=array();
    		$result="Something went wrong.";
    	}
    	 
      
    	$input=array('api'=>'update_consumer_mob',
    			'parameters'=>$parameter); 
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function addConsumerActivityAction(){
    
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    
		$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$consumer_id=$request->getParam("consumer_id");
    	$site_id=$request->getParam("site_id");
    	$activity=$request->getParam("activity");
    	$instant=$request->getParam("instant");
    	$date=$request->getParam("date");
    	$opening_reading=$request->getParam("opening_reading");
    	$closing_reading=$request->getParam("closing_reading");
    	$packages= json_decode($request->getParam("packages"));
		
		$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
		
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"consumer_id"=>$consumer_id,
    			"site_id"=>$site_id,
				"activity"=>$activity,
    			"instant"=>$instant,
				"date"=>$date,
    			"opening_reading"=>$opening_reading,
				"closing_reading"=>$closing_reading,
    	);
		
		$date_value = new Zend_Date();
		$date_value->setTimezone("Asia/Calcutta");
		$timestamp = $date_value->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$files=array();	$success=0;
    	
			$date_val = new Zend_Date();
    		$date_val->setTimezone("Asia/Calcutta");
    		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
    		
    		if($date!=NULL && $date!="" && $date!='undefined'){
    			$zendDate = new Zend_Date($date,"dd-MM-yyyy");
    			$effective_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    		}else{
    			$effective_date =$date_val->toString("yyyy-MM-dd");
    		}
			
    		$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
			$collectionAgents=$collectionAgentsMapper->getCollectionAgentById($user_id);
    		
    		$changePackageMapper=new Application_Model_ConsumerActivityMapper();
    		$changePackage=new Application_Model_ConsumerActivity();
    		
    		$changePackage->__set("consumer_id", $consumer_id);
    		$changePackage->__set("site_id", $site_id);
    		$changePackage->__set("curr_package_id", $curr_package_id);
    		$changePackage->__set("new_package_id", $new_package_id);
    		$changePackage->__set("activity", $activity);
    		$changePackage->__set("instant", $instant);
    		$changePackage->__set("effective_date", $effective_date);
    		$changePackage->__set("user_id", $collectionAgents->__get("collection_agent_id"));
    		$changePackage->__set("user_type", "(M)"); 
    		$changePackage->__set("timestamp", $timestamp);
    		$changePackage->__set("opening_reading", 0);
    		$changePackage->__set("closing_reading", 0);
				if(count($packages)>0){
					foreach ($packages as $package){
						$package_id=$package->package_id;
						$reading=$package->meter_reading;
						$meter_no=$package->meter_no;
						$last_reading=$package->last_reading;
						if($activity=='banned'){
							$status=0;
						}else{
							$status=1;
						}
						
						$MeterReadings= new Application_Model_MeterReadings();
						$MeterReadings->__set("consumer_id",$consumer_id);
						$MeterReadings->__set("meter_reading",$reading);
						$MeterReadings->__set("timestamp",$timestamp);
						$MeterReadings->__set("package_id",$package_id);
						$MeterReadings->__set("start_reading",$status);
						$MeterReadings->__set("meter_no",$meter_no);
						$MeterReadings->__set("unit",(floatval($reading)-floatval($last_reading)));
						$MeterReadings->__set("serial",$meter_no);
						$MeterReadings->__set("entry_by",$user_id); 
						$MeterReadings->__set("entry_type",'M');
						$reading_value=$meterReadingsMapper->addNewMeterReading($MeterReadings);
						if($reading_value){
							$meterInventoryMapper=new Application_Model_MeterInventoryMapper();
							$check=$updateInventory=$meterInventoryMapper->getDetailByMeterNo($meter_no);
							if($check){ 
								$inventory_new=$meterInventoryMapper->updateInventory($meter_no,$status,$consumer_id); 
							}
						}
					}
				}
			
    		$changePackages=$changePackageMapper->addConsumerActivity($changePackage);
    		if($changePackages){
				$success=1;
				$data=array();
    			$result="Data is added successfully.";
			}else {
				$data=array();
				$result="Something went wrong.";
			}
    	
    	$input=array('api'=>'add_consumer_activity',
    			'parameters'=>$parameter); 
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function addPackageActivityAction(){
    
    	$request=$this->getRequest();
			$user_id=$request->getParam("user_id");
    		$consumer_id=$request->getParam("consumer_id");
    		$site_id=$request->getParam("site_id");
    		$curr_package_id=$request->getParam("current_package_id");
    		$new_package_id=$request->getParam("new_package_id");
    		$assign_package=$request->getParam("assign_package");
    		$activity=$request->getParam("activity");
    		$instant=$request->getParam("instant");
    		$date=$request->getParam("date");
    		$opening_reading=$request->getParam("opening_reading");
    		$closing_reading=$request->getParam("closing_reading");
			$meter_no=$request->getParam("meter_no");
    		
			$date_value = new Zend_Date();
			$date_value->setTimezone("Asia/Calcutta");
			$timestamp = $date_value->toString("yyyy-MM-dd HH:mm:ss");
		 
			$usersMapper= new Application_Model_UsersMapper();
			$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
			
    		$date_val = new Zend_Date();
    		$date_val->setTimezone("Asia/Calcutta");
    		$timestamp = $date_val->toString("yyyy-MM-dd HH:mm:ss");
    		
    		if($date!=NULL && $date!="" && $date!='undefined'){
    			$zendDate = new Zend_Date($date,"dd-MM-yyyy");
    			$effective_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    		}else{
    			$effective_date =$date_val->toString("yyyy-MM-dd");
    		}
    		$files=array();	$success=0;
    		$changePackageMapper=new Application_Model_PackageChangeMapper();
    		$changePackage=new Application_Model_PackageChange();
    		
    		$changePackage->__set("consumer_id", $consumer_id);
    		$changePackage->__set("site_id", $site_id);
    		$changePackage->__set("curr_package_id", $curr_package_id);
    		$changePackage->__set("new_package_id", $new_package_id);
    		$changePackage->__set("assign_package", $assign_package);
    		$changePackage->__set("activity", $activity);
    		$changePackage->__set("instant", $instant);
    		$changePackage->__set("effective_date", $effective_date);
    		$changePackage->__set("user_id", $user_id);
    		$changePackage->__set("user_type", "(M)");
    		$changePackage->__set("timestamp", $timestamp);
    		$changePackage->__set("opening_reading", $opening_reading);
    		$changePackage->__set("closing_reading", $closing_reading);
			$changePackage->__set("meter_no", $meter_no);
    		 
    		$changePackages=$changePackageMapper->addPackageChange($changePackage);
    		if($changePackages){
				$success=1;
    			$data=array();
    			$result="Data is added successfully.";
    		}else {
				$data=array();
				$result="Something went wrong.";
			}
    	 
     
    
    	$input=array('api'=>'consumer_temp_disconnect',
    			'parameters'=>$parameter); 
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }

	public function changeMeterStatusAction(){
		
		$consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
		$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
		$wattageMapper=new Application_Model_WattageMapper();
		$schemeMapper=new Application_Model_SchemesMapper();
		$cashRegisterMapper=new Application_Model_CashRegisterMapper();
		$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$siteMapper=new Application_Model_SitesMapper();
		
		date_default_timezone_set('Asia/Kolkata');
		 $request = $this->getRequest();
		 $user_id=$request->getParam("user_id");
		 $user_type=$request->getParam("user_type");
		 $consumer_id=$request->getParam("consumer_id");
		 $package_id=$request->getParam("package_id");
		 $new_reading=$request->getParam("new_reading");
		 $closing_reading=$request->getParam("closing_reading");
		 $meter_no=$request->getParam("meter_no");
		 $last_meter_no=$request->getParam("last_meter_no"); 
		 $usable=$request->getParam("usable");
		 $release=$request->getParam("release");
		 
		$parameter=array(
			"user_id"=>$user_id,
			"user_type"=>$user_type,
			"consumer_id"=>$consumer_id,
			"package_id"=>$package_id,
			"meter_no"=>$meter_no,
			"new_reading"=>$new_reading,
			"closing_reading"=>$closing_reading,
			"last_meter_no"=>$last_meter_no,
			"usable"=>$usable,
			"release"=>$release,
		);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		$files=array();	$success=0; $data=array(); 											
         
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
        $lastTimestamp= date("Y-m-d H:i:s", strtotime($timestamp) - 60);             											
		$collectionAgent=$collectionAgentsMapper->getCollectionAgentById($user_id);
		 
		$meter_no_closing=NULL;$meter_reading=0;
			$reading=$meterReadingsMapper->getTotalReadingByPreviousDate($consumer_id,$timestamp,$package_id,true); 
			if($reading){
				$meter_no_closing=$reading["meter_no"];
				$meter_reading=$reading["meter_reading"];
			}
			 
    		if(intval($closing_reading)>0 && $closing_reading>=$meter_reading){
					$MeterReadings= new Application_Model_MeterReadings();
					$MeterReadings->__set("consumer_id",$consumer_id);
					$MeterReadings->__set("meter_reading",$closing_reading);
					$MeterReadings->__set("timestamp",$lastTimestamp);
					$MeterReadings->__set("package_id",$package_id);
					$MeterReadings->__set("start_reading",0);
					$MeterReadings->__set("meter_no",$last_meter_no);
					$MeterReadings->__set("unit",floatval($closing_reading-$meter_reading));
					$MeterReadings->__set("serial",$last_meter_no);
					$MeterReadings->__set("entry_by",$user_id); 
					$MeterReadings->__set("entry_type",'M');
					$readings=$meterReadingsMapper->addNewMeterReading($MeterReadings);
					if($readings){
						$MeterReadings= new Application_Model_MeterReadings();
						$MeterReadings->__set("consumer_id",$consumer_id);
						$MeterReadings->__set("meter_reading",$new_reading);
						$MeterReadings->__set("timestamp",$timestamp);
						$MeterReadings->__set("package_id",$package_id);
						$MeterReadings->__set("start_reading",1);
						$MeterReadings->__set("meter_no",$meter_no);
						$MeterReadings->__set("unit",0);
						$MeterReadings->__set("serial",$meter_no);
						$MeterReadings->__set("entry_by",$user_id); 
						$MeterReadings->__set("entry_type",'M');
						$reading_value=$meterReadingsMapper->addNewMeterReading($MeterReadings);
						if($reading_value){
							$meterInventoryMapper=new Application_Model_MeterInventoryMapper();
							$inventory_new=$meterInventoryMapper->updateInventory($meter_no,1,$consumer_id); 
							if($usable==2 || $usable==3){
								$inventory_last=$meterInventoryMapper->updateInventory($last_meter_no,$usable,$consumer_id);
							}elseif($release==0){
								$inventory_last=$meterInventoryMapper->updateInventory($last_meter_no,$release,$consumer_id); 
							}
						}
					}	
					$success=1;
					$data=array();
					$result="Meter has been changed successfully.";
			}else{
				$success=0;
    			$data=array();
    			$result="New reading should be greater then last reading.";
			}
					
			$input=array('api'=>'change_meter',
						 'parameters'=>$parameter);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
						 "files" => $files,
		                 "commandResult" => $commandResult,
		                );
		 
			$json = json_encode($arr, JSON_PRETTY_PRINT);
			echo $json;
		    exit;		          											
	}
	
	public function getPendingMeterListAction(){
    
    	$MeterInventoryMapper = new Application_Model_MeterInventoryMapper();
    	$consumersMapper = new Application_Model_ConsumersMapper();
		
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$site_id=$request->getParam("site_id");
		
		$meters=$MeterInventoryMapper->getMeterNoByStatus(0,'disable',$site_id);
		$meter_arr=array(); 
    	if($meters){
			 foreach($meters as $meter){
				$consumer_name=NULL;$consumer_connection_id=NULL;
				$consumer = $consumersMapper->getConsumerById($meter["consumer_id"]);
				if($consumer){
					$consumer_name=$consumer->__get("consumer_name");
					$consumer_connection_id=$consumer->__get("consumer_connection_id");
				}
				 $data=array(
					"meter_no"=>$meter["meter_no"],
					"brand"=>$meter["brand"],
					"reading"=>$meter["reading"],
					"date"=>date("d-M-Y g:i a",strtotime($meter["remove_timestamp"])), 
					"consumer_name"=>$consumer_name,
					"consumer_id"=>$meter["consumer_id"],
					"consumer_connection_id"=>$consumer_connection_id, 
				 );
				 $meter_arr[]=$data;
			 }    
				$success=1;
				$result="Meter List have been accessed successfully.";
			 
		}else{
			$success=0;
			$result="No pending meter exist.";
		}
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
    	$input=array('api'=>'get-diesel-consumption-list',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$meter_arr,
    
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function updatePendingMeterNoAction(){
      
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    
    	$user_id=$request->getParam("user_id");
    	$user_type=$request->getParam("user_type");
    	$meter_no=$request->getParam("meter_no");
		$consumer_id=$request->getParam("consumer_id");
		$status=$request->getParam("status");
    	$parameter=array(
    			"user_id"=>$user_id,
    			"user_type"=>$user_type,
    			"meter_no"=>$meter_no,
    	);
		
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$usersMapper= new Application_Model_UsersMapper();
		$details=$usersMapper->addLogindetails($user_id,$timestamp,'Login',$_SERVER['REMOTE_ADDR'],'Mobile');
		
    	$files=array();	$success=0;
    	$MeterInventoryMapper = new Application_Model_MeterInventoryMapper();
    	 
		if($meter_no!=NULL && $meter_no!="" && $meter_no!='undefined'){
     	 	if($status==3){
				$meter_status='delete';
			}elseif($status==2){
				$meter_status='enable';
			}else{
				$meter_status='disable';
			}
			
    	 	$updateInventory=$MeterInventoryMapper->updateMeterStatus($meter_no,$status,$meter_status,$consumer_id);
    		if($updateInventory){
				$success=1; 
    			$data=array();
    			$result="Meter Number is updated successfully.";
    		}else {
				$success=0; 
    			$data=array();
    			$result="Something went wrong.";
    		}
    	}else {
			$success=0; 
    		$data=array();
    		$result="Please enter correct meter no.";
    	}
    	 
      
    	$input=array('api'=>'update_meter_no',
    			'parameters'=>$parameter); 
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	/* Start Consumer Application API */ 
     
    public function loginConsumerAction(){
    
    	$stateMapper=new Application_Model_StatesMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
		$collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
    	$sliderImageMapper=new Application_Model_SliderImageMapper(); 
		$userMapper=new Application_Model_UsersMapper();
		$ConsumerComplainMapper=new Application_Model_ConsumerComplainMapper(); 
		
		$request = $this->getRequest();
    	date_default_timezone_set('Asia/Kolkata');
    	
    	$number=$request->getParam("number");
    	$parameter=array("number"=>$number);
    	$success=0;
    	
    	$consumers=$consumersMapper->getConsumerByMobileNo($number);
    	if($consumers)
    	{
    		$success=1;
    	    
    		$state_arr=array();
    		$states=$stateMapper->getAllStates();
    		if($states){
    			foreach($states as $state){
    				$state_data=array(
    						"state_id"=>$state->__get("state_id"),
    						"state_name"=>$state->__get("state_name"),
    				);
    				$state_arr[]=$state_data;
    			}
    		}
    		
    		$site_arr=array();
    		$sites=$sitesMapper->getAllSites();
    		if($sites){
    			foreach($sites as $site){
    				$states=$stateMapper->getStateById($site->__get("state_id"));
    				$site_data=array(
    						"state_id"=>$site->__get("state_id"),
    						"site_id"=>$site->__get("site_id"),
    						"site_name"=>$site->__get("site_name"),
    						"site_code"=>$site->__get("site_code"),
    				 );
    				$site_arr[]=$site_data;
    			}
    		}
     		
    		$outstanding=$consumersMapper->getConsumerOutstanding($consumers["consumer_id"]);
    		$activation= $consumersMapper->getRemainingActivation($consumers["consumer_id"]);
    		
    		$total_due=$outstanding+$activation;
    		
    		$otp=rand(1,9).rand(0,9).rand(0,9).rand(0,9);
    		//$sms="Your generated otp is ".$otp.", Please enter this otp.";
			$sms=$otp." is your OTP for TARAurja App. Please do not share OTP with anyone.";
    		$this->_smsNotification($number, $sms);
    		//$otp=1234; 
    		$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumers["consumer_id"]);
    		$package_arr=array();
    		$is_post='0';
    		if($consumerpackage){
    			foreach($consumerpackage as $package){
    				$watt_total=0;
    				$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
    				if($watt){
    					$watt_total=$watt->__get("wattage");
    				}
    				if($package["is_postpaid"]==0){
    					$reading="";
    					$letest_meter_reading_update="";
    					$meter_no="";
    				}else{
    					$is_post=$package["is_postpaid"];
    					$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    					$latestReading=$meterReadingMapper->getLatestReadingByConsumerId($consumers["consumer_id"],$package["package_id"]);
    					if($latestReading){
    						$reading=$latestReading->__get("meter_reading");
    						$letest_meter_reading_update=date("d-M-Y g:i a",strtotime($latestReading->__get("timestamp")));
    						$meter_no=$latestReading->__get("meter_no");
    					}else{$reading="";
    						$letest_meter_reading_update="";
    						$meter_no="";
    					}
    				}
    				
    				$pack_data=array(
    						"package_id"=>$package["package_id"],
    						"package_name"=>$package["package_name"],
    						"package_cost"=>$package["package_cost"],
    						"is_postpaid"=>$package["is_postpaid"],
    							
    						"max_wattage_allowed"=>$watt_total,
    						"min_amount"=>$package["package_cost"],
    						"free_units"=>$package["unit"],
    						"rate_after_free_units"=>$package["extra_charges"],
    						"min_unit_rate"=>$package["min_charges"],
    						"max_unit_rate"=>$package["max_charges"],
    							
    						"wattage"=>$watt_total,
    						"status"=>$package["status"],
    						"meter_reading"=>$reading,
    							
    						"updated_date"=>$letest_meter_reading_update,
    						"meter_no"=>$meter_no,
    				);
    				$package_name_arr[]=$package["package_name"];
    				$package_arr[]=$pack_data;
    			}
    		}
			
    		$complainRemarks_arr=array();
    		$complainRemarks=$consumersMapper->getComplainRemark();
    		if($complainRemarks){
    			foreach($complainRemarks as $complainRemark){
    				$data=array(
    						"id"=>$complainRemark["id"],
    						"remark"=>$complainRemark["remark"],
							"service_id"=>$complainRemark["service_id"],
    				);
    				$complainRemarks_arr[]=$data;
    			}
    		}
    		
    		$ServiceRequest_arr=array();
    		$ServiceRequests=$consumersMapper->getServiceRequest();
    		if($ServiceRequests){
    			foreach($ServiceRequests as $ServiceRequest){
    				$data=array(
    						"id"=>$ServiceRequest["id"],
    						"service"=>$ServiceRequest["service"],
    				);
    				$ServiceRequest_arr[]=$data;
    			}
    		}
    		$collectionAgents=$collectionAgentMapper->getCollBySiteId($consumers["site_id"]);
			if($collectionAgents){
				$line_manager_no=$collectionAgents[0]["agent_mobile"];
				$collectionAgent_no=$collectionAgents[0]["line_manager_no"];
			}else{
				$line_manager_no=0;
				$collectionAgent_no=0;
			}
			
			$sliderImage_arr=array();
				$sliderImages=$sliderImageMapper->getAllSliderImage($consumers["site_id"]);
				if($sliderImages){
					foreach($sliderImages as $sliderImage){
						$data=array(
								"id"=>$sliderImage->__get("id"),
								"name"=>$sliderImage->__get("name"),
								"url"=>"http://taraurja.in/angular_app/components/slider-image/slider/".$sliderImage->__get("name"),
								"timestamp"=>date("d-M-Y g:i a",strtotime($sliderImage->__get("timestamp"))),
						);
						$sliderImage_arr[]=$data;
					}
				}
			
				$pendingRating_arr=array();
				$pendingRatings=$ConsumerComplainMapper->getConsumerComplain($consumers["consumer_id"],true);
				if($pendingRatings){
					foreach($pendingRatings as $pendingRating){
						
						$complainRemarks_name=array();$complainRemarks_id=array();
						$remarks_id=explode(",",$pendingRating["remark_id"]);
						if(count($remarks_id)>0){
							for($i=0;$i<count($remarks_id);$i++){
								$complainRemarks=$consumersMapper->getComplainRemark($remarks_id[$i]);
								if($complainRemarks){ 
									$complainRemarks_name[]=$complainRemarks[0]["remark"];
									$complainRemarks_id[]=$remarks_id[$i];
								}
							}
						}
					
						$ServiceRequests=$consumersMapper->getServiceRequest($pendingRating["service_id"]);
						if($ServiceRequests){
							$ServiceRequests_name=$ServiceRequests[0]["service"];
						}
						if($pendingRating["approve_type"]=='M'){
							$collectionAgent = $collectionAgentMapper->getCollectionAgentById($pendingRating["approve_by"]);
							$Created_By=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
						}else{
							$users = $userMapper->getUserById($pendingRating["approve_by"]);
							$Created_By= $users->__get("user_fname") . " " . $users->__get("user_lname");
						}
						
						
						
						$data=array(
								"consumer_id"=>$pendingRating["consumer_id"],
								"consumer_name"=>$consumers["consumer_name"],
								"consumer_connection_id"=>$consumers["consumer_connection_id"],
								"service_id"=>$pendingRating["service_id"],
								"service"=>$ServiceRequests_name,
								"remark_id"=>implode(",",$complainRemarks_id), 
								"remark"=>implode(",",$complainRemarks_name), 
								"entry_date"=>date("d-M-Y g:i a",strtotime($pendingRating["entry_date"])),
								"approve_by"=>$Created_By,
								"approve_date"=>date("d-M-Y g:i a",strtotime($pendingRating["approve_date"])),
						);
						$pendingRating_arr[]=$data;
					}
				}
				  
    		$data=array( 
    				"states"=>$state_arr,
    				"site"=>$site_arr,
    				"packages"=>$package_arr,
    				"remark"=>$complainRemarks_arr,
    				"service"=>$ServiceRequest_arr,
					"banners"=>$sliderImage_arr, 
					'complete_service'=>$pendingRating_arr, 
    				'consumer_id' => $consumers["consumer_id"],
    				'consumer_code' => $consumers["consumer_code"],
    				'site_id' => $consumers["site_id"],
    				'consumer_name' =>strtoupper($consumers["consumer_name"]),
    				'consumer_father_name' => $consumers["consumer_father_name"],
    				'consumer_status' => $consumers["consumer_status"],
    				'consumer_act_date' => $consumers["consumer_act_date"],
    				'consumer_act_charge' => $consumers["consumer_act_charge"],
    				'consumer_connection_id' => $consumers["consumer_connection_id"],
    				'email_id' => $consumers["email_id"],
    				'type_of_me' => $consumers["type_of_me"],
    				'feeder_id' => $consumers["site_meter_id"],
    				'package_name'=>implode(',',$package_name_arr),
    				"is_postpaid"=>$is_post,
    				"otp"=>$otp,
    				"oustanding"=>$total_due,
    				"helpLine_no"=>'0000-0000-0000',
					"version_code"=>'3',
					"line_manager_no"=>$line_manager_no,
					"collectionAgent_no"=>$collectionAgent_no
    		);
    		$result = "Login Successfully";
    		 
    	}else{
    		$result="Please enter correct mobile number.";
    		$data=array();
    	}
    		
    	$input=array('api'=>'login-consumer',
    				'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
	public function consumerMasterDataAction(){
    
    	$stateMapper=new Application_Model_StatesMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
		$sliderImageMapper=new Application_Model_SliderImageMapper(); 
		$ConsumerComplainMapper=new Application_Model_ConsumerComplainMapper(); 
		$userMapper=new Application_Model_UsersMapper();
		
    	$request = $this->getRequest();
    	date_default_timezone_set('Asia/Kolkata');
    	
    	$device_type=$request->getParam("device_type");
		$device_token=$request->getParam("device_token");
		$consumer_id=$request->getParam("consumer_id");
    	
		$parameter=array();
    	 	$success=1;
    	    $pendingRating_arr=array();
    		$state_arr=array();
    		$states=$stateMapper->getAllStates();
    		if($states){
    			foreach($states as $state){
    				$state_data=array(
    						"state_id"=>$state->__get("state_id"),
    						"state_name"=>$state->__get("state_name"),
    				);
    				$state_arr[]=$state_data;
    			}
    		}
    		
    		$site_arr=array();
    		$sites=$sitesMapper->getAllSites();
    		if($sites){
    			foreach($sites as $site){
    				$states=$stateMapper->getStateById($site->__get("state_id"));
    				$site_data=array(
    						"state_id"=>$site->__get("state_id"),
    						"site_id"=>$site->__get("site_id"),
    						"site_name"=>$site->__get("site_name"),
    						"site_code"=>$site->__get("site_code"),
    				 );
    				$site_arr[]=$site_data;
    			}
    		}
     		
    	 
			
    		$complainRemarks_arr=array();
    		$complainRemarks=$consumersMapper->getComplainRemark();
    		if($complainRemarks){
    			foreach($complainRemarks as $complainRemark){
    				$data=array(
    						"id"=>$complainRemark["id"],
    						"remark"=>$complainRemark["remark"],
							"service_id"=>$complainRemark["service_id"],
    				);
    				$complainRemarks_arr[]=$data;
    			}
    		}
    		
    		$ServiceRequest_arr=array();
    		$ServiceRequests=$consumersMapper->getServiceRequest();
    		if($ServiceRequests){
    			foreach($ServiceRequests as $ServiceRequest){
    				$data=array(
    						"id"=>$ServiceRequest["id"],
    						"service"=>$ServiceRequest["service"],
    				);
    				$ServiceRequest_arr[]=$data;
    			}
    		}
			$line_manager_no=0;$collectionAgent_no=0;$total_due=0;$sliderImage_arr=array();
			if($consumer_id!="" && $consumer_id!=NULL && $consumer_id!='NULL' && $consumer_id!='undefined' && $consumer_id!=0){
				$consumers=$consumersMapper->getConsumerById($consumer_id);
				if($consumers)
				{
					$outstanding=$consumersMapper->getConsumerOutstanding($consumers->__get("consumer_id"));
					$activation= $consumersMapper->getRemainingActivation($consumers->__get("consumer_id"));
				
					$total_due=$outstanding+$activation;
					
					$collectionAgents=$collectionAgentMapper->getCollBySiteId($consumers->__get("site_id"));
					if($collectionAgents){
						$line_manager_no=$collectionAgents[0]["agent_mobile"];
						$collectionAgent_no=$collectionAgents[0]["line_manager_no"];
					}
				}
				
				$sliderImage_arr=array();
				$sliderImages=$sliderImageMapper->getAllSliderImage($consumers->__get("site_id"));
				if($sliderImages){
					foreach($sliderImages as $sliderImage){
						$data=array(
								"id"=>$sliderImage->__get("id"),
								"name"=>$sliderImage->__get("name"),
								"url"=>"http://taraurja.in/angular_app/components/slider-image/slider/".$sliderImage->__get("name"),
								"timestamp"=>date("d-M-Y g:i a",strtotime($sliderImage->__get("timestamp"))),
						);
						$sliderImage_arr[]=$data;
					}
				}
				
				$pendingRating_arr=array();
				$pendingRatings=$ConsumerComplainMapper->getConsumerComplain($consumers->__get("consumer_id"),true);
				if($pendingRatings){
					foreach($pendingRatings as $pendingRating){
						
						$complainRemarks_name=array();$complainRemarks_id=array();
						$remarks_id=explode(",",$pendingRating["remark_id"]);
						if(count($remarks_id)>0){
							for($i=0;$i<count($remarks_id);$i++){
								$complainRemarks=$consumersMapper->getComplainRemark($remarks_id[$i]);
								if($complainRemarks){ 
									$complainRemarks_name[]=$complainRemarks[0]["remark"];
									$complainRemarks_id[]=$remarks_id[$i];
								}
							}
						}
					
						$ServiceRequests=$consumersMapper->getServiceRequest($pendingRating["service_id"]);
						if($ServiceRequests){
							$ServiceRequests_name=$ServiceRequests[0]["service"];
						}
						if($pendingRating["approve_type"]=='M'){
							$collectionAgent = $collectionAgentMapper->getCollectionAgentById($pendingRating["approve_by"]);
							$Created_By=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
						}else{
							$users = $userMapper->getUserById($pendingRating["approve_by"]);
							$Created_By= $users->__get("user_fname") . " " . $users->__get("user_lname");
						}
						
						
						
						$data=array(
								"consumer_id"=>$pendingRating["consumer_id"],
								"consumer_name"=>$consumers->__get("consumer_name"),
								"consumer_connection_id"=>$consumers->__get("consumer_connection_id"),
								"service_id"=>$pendingRating["service_id"],
								"service"=>$ServiceRequests_name,
								"remark_id"=>implode(",",$complainRemarks_id), 
								"remark"=>implode(",",$complainRemarks_name), 
								"entry_date"=>date("d-M-Y g:i a",strtotime($pendingRating["entry_date"])),
								"approve_by"=>$Created_By,
								"approve_date"=>date("d-M-Y g:i a",strtotime($pendingRating["approve_date"])),
						);
						$pendingRating_arr[]=$data;
					}
				}
				
			} 
			
			
			
			$consumer_type_arr=array();
    		$consumer_types=$consumersMapper->getConsumerType();
    		if($consumer_types){
    			foreach ($consumer_types as $consumer_type_main){
				
					$sub_consumer_type_arr=array();
					$sub_consumer_types=$consumersMapper->getConsumerType($consumer_type_main["consumer_type_id"]);
					if($sub_consumer_types){
						foreach ($sub_consumer_types as $consumer_type){
							$data_val=array(
									"consumer_type_id"=>$consumer_type["consumer_type_id"],
									"consumer_type_name"=>$consumer_type["consumer_type_name"],
									"consumer_type_code"=>$consumer_type["consumer_type_code"],
							);
							$sub_consumer_type_arr[]=$data_val;
						}
					}
			
    				$data_val=array(
    						"consumer_type_id"=>$consumer_type_main["consumer_type_id"],
    						"consumer_type_name"=>$consumer_type_main["consumer_type_name"],
    						"consumer_type_code"=>$consumer_type_main["consumer_type_code"],
							"customer_category"=>$sub_consumer_type_arr,
    				);
    				$consumer_type_arr[]=$data_val;
    			}
    		}
			
    		$data=array(
    				"states"=>$state_arr,
    				"site"=>$site_arr,
    				"remark"=>$complainRemarks_arr,
    				"service"=>$ServiceRequest_arr, 
					"banners"=>$sliderImage_arr, 
					"customer_type"=>$consumer_type_arr,
					'complete_service'=>$pendingRating_arr, 
    				"helpLine_no"=>'0000-0000-0000',
					"oustanding"=>$total_due, 
    				"version_code"=>'3',
					"line_manager_no"=>$line_manager_no,
					"collectionAgent_no"=>$collectionAgent_no
    		);
    		$result = "Data loaded Successfully"; 
    		 
    	 
    		
    	$input=array('api'=>'consumer-master-data',
    				'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
    public function resendOtpAction(){
		
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	 
    	$request = $this->getRequest();
    	  
    	$number=$request->getParam("number");
		$filter=$request->getParam("filter");
		
    	$parameter=array("number"=>$number);
    	$success=0;
    	if($filter=="" || $filter =="NULL" || $filter==NULL || $filter=='undefined'){ 
			$consumers=$consumersMapper->getConsumerByMobileNo($number);
		}else{
			$consumers=true;
		}
    	if($consumers)
    	{
    		$success=1;
    		$otp=rand(1,9).rand(0,9).rand(0,9).rand(0,9);
    		$sms=$otp." is your OTP for TARAurja App. Please do not share OTP with anyone.";
    		$this->_smsNotification($number, $sms);
			
    		$data=array(
    				"otp"=>$otp,
    		);
    		$result = "Login Successfully";
    		 
    	}else{
    		$result="Please enter correct mobile number.";
    		$data=array();
    	}
    
    	$input=array('api'=>'login-consumer',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function consumerDetailAppAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    	$equipmentMapper=new Application_Model_EquipmentMapper();
		$stateMapper=new Application_Model_StatesMapper();
		$collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$consumer_id=$request->getParam("consumer_id");
        
    	$parameter=array(
    			"consumer_id"=>$consumer_id,
    	);
    		
    	$files=array();	$success=0;
    	$total_reading=0;$total_amt=0;$unit_rate=0;
    
    	$consumer_arr=array();
    	$consumer = $consumersMapper->getConsumerById($consumer_id);
    	if($consumer){
    		$success=1;
    		$transaction_historys=array();
    		$consumer_mobile=$consumer->__get("consumer_code");
    		$consumer_connection_id=$consumer->__get("consumer_connection_id");
    		$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
    		$package_arr=array();
    		if($consumerpackage){
    
    			foreach($consumerpackage as $package){
    				$watt_total=0;
    				$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
    				if($watt){
    					$watt_total=$watt->__get("wattage");
    				}
    				if($package["is_postpaid"]==0){
    					$reading="";
    				}else{
    					$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    					$latestReading=$meterReadingMapper->getLatestReadingByConsumerId($consumer_id,$package["package_id"]);
    					if($latestReading){
    						$reading=$latestReading->__get("meter_reading");
							$meter_no=$latestReading->__get("meter_no");
    					}else{$reading="";$meter_no="";}
    				}
    				$pack_data=array(
    						"package_id"=>$package["package_id"],
    						"package_name"=>$package["package_name"],
    						"package_cost"=>$package["package_cost"],
    						"is_postpaid"=>$package["is_postpaid"],
    							
    						"max_wattage_allowed"=>$watt_total,
    						"min_amount"=>$package["package_cost"],
    						"free_units"=>$package["unit"],
    						"rate_after_free_units"=>$package["extra_charges"],
    						"unit_rate"=>$package["extra_charges"],
    						"min_unit_rate"=>$package["min_charges"],
    						"max_unit_rate"=>$package["max_charges"],
    							
    						"wattage"=>$watt_total,
    						"status"=>$package["status"],
    						"meter_reading"=>$reading,
							"meterNumber"=>$meter_no 
    				);
    				$package_arr[]=$pack_data;
    			}
    		}
    		$type_of_me=$consumer->__get("type_of_me");
    		$consumer_type_vals=$consumersMapper->getConsumerTypeById($type_of_me);
    		$consumer_types=NULL;
    		if($consumer_type_vals){
    			$consumer_types=$consumer_type_vals["consumer_type_name"];
    		}
			$sub_type_of_me=$consumer->__get("sub_type_of_me");
    		$sub_type_of_me_vals=$consumersMapper->getConsumerTypeById($sub_type_of_me);
    		$sub_consumer_types=NULL;
    		if($sub_type_of_me_vals){
    			$sub_consumer_types=$sub_type_of_me_vals["consumer_type_name"];
    		}
    		$cashRe=$cashRegisterMapper->calculateActivationAmount('ACTIVATION',$consumer->__get("consumer_id"));
    		$actiCharges=$cashRe;
    		if($actiCharges==false){$actiCharges=0;}
    		 
    		//$outstanding=$consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
    		$activation= $consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
    		$sites_values=$siteMapper->getSiteById($consumer->__get("site_id"));
			$states=$stateMapper->getStateById($sites_values->__get("state_id"));
    		$meter_name=$siteMasterMapper->getMMById($consumer->__get("site_meter_id"));
    		$MM="";
    		if($meter_name){
    			$MM=$meter_name->__get("meter_name")." (".$meter_name->__get("description").")"; 
    		}
    		$total=$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer->__get("consumer_id")); //$outstanding+$activation;
    		if($consumer->__get("mobile_no")==NULL || $consumer->__get("mobile_no")==""){
    			$non_primary=array();
    		}else{
    			$non_primary=explode(",",$consumer->__get("mobile_no"));
    		}
    		$mob=array(
    				"mobile_no"=>$consumer->__get("consumer_code"),
    				"is_primary"=>1
    		);
    		$mob_total[]=$mob;
    			
    		if(count($non_primary)>0){
    			for($k=0;$k<count($non_primary);$k++){
    				$mob=array(
    						"mobile_no"=>$non_primary[$k],
    						"is_primary"=>0
    				);
    				$mob_total[]=$mob;
    			}
    		}
			$con_scheme_arr=array();
    		$con_schems=$consumerSchemeMapper->getConsumerSchemeDetailByConsumerId($consumer->__get("consumer_id"));
    
    		if($con_schems){
    			foreach ($con_schems as $con_schem){
    
    				$schemes=$schemeMapper->getSchemeById($con_schem["consumer_scheme"]);
    
    				$equipment=($schemes->__get("celamed_id")=="NULL" || $schemes->__get("celamed_id")=='undefined' || $schemes->__get("celamed_id")==NULL)?'':$schemes->__get("celamed_id");
								$equipments=$equipmentMapper->getEquipmentById($equipment); 
								$equip_name="";
								if($equipments){
									$equip_name=$equipments->__get("name");
								} 
    				$scheme_data=array(
						"scheme_id"=>($schemes->__get("scheme_id")=="NULL" || $schemes->__get("scheme_id")=='undefined' || $schemes->__get("scheme_id")==NULL)?'':$schemes->__get("scheme_id"),
						"scheme_name"=> ($schemes->__get("scheme")=="NULL" || $schemes->__get("scheme")=='undefined' || $schemes->__get("scheme")==NULL)?'':$schemes->__get("scheme"),
						"down_payment"=> ($schemes->__get("down_payment")=="NULL" || $schemes->__get("down_payment")=='undefined' || $schemes->__get("down_payment")==NULL)?'':$schemes->__get("down_payment"),
						"emi_amt"=> ($schemes->__get("emi_amt")=="NULL" || $schemes->__get("emi_amt")=='undefined' || $schemes->__get("emi_amt")==NULL)?'':$schemes->__get("emi_amt"),
						"emi_duration"=> ($schemes->__get("emi_duration")=="NULL" || $schemes->__get("emi_duration")=='undefined' || $schemes->__get("emi_duration")==NULL)?'':$schemes->__get("emi_duration"),
						"discount_amt"=> ($schemes->__get("discount_amt")=="NULL" || $schemes->__get("discount_amt")=='undefined' || $schemes->__get("discount_amt")==NULL)?'':$schemes->__get("discount_amt"),
						"duration"=> ($schemes->__get("discount_month")=="NULL" || $schemes->__get("discount_month")=='undefined' || $schemes->__get("discount_month")==NULL)?'':$schemes->__get("discount_month"),
						"from_date"=> ($schemes->__get("start_date")=="NULL" || $schemes->__get("start_date")=='undefined' || $schemes->__get("start_date")==NULL)?'':date("d-M-Y",strtotime($schemes->__get("start_date"))),
						"to_date"=>($schemes->__get("end_date")=="NULL" || $schemes->__get("end_date")=='undefined' || $schemes->__get("end_date")==NULL)?'':date("d-M-Y",strtotime($schemes->__get("end_date"))),
						"package_continue"=> ($con_schem["package_continue"]=="NULL" || $con_schem["package_continue"]=='undefined' || $con_schem["package_continue"]==NULL)?'':$con_schem["package_continue"],
						"scheme_continue"=> ($con_schem["scheme_continue"]=="NULL" || $con_schem["scheme_continue"]=='undefined' || $con_schem["scheme_continue"]==NULL)?'':$con_schem["scheme_continue"],
										
						"advance_payment_for_month"=>($schemes->__get("advance_payment")=="NULL" || $schemes->__get("advance_payment")=='undefined' || $schemes->__get("advance_payment")==NULL)?'':$schemes->__get("advance_payment"),
						"discount_for_month"=>($schemes->__get("advance_dis_month")=="NULL" || $schemes->__get("advance_dis_month")=='undefined' || $schemes->__get("advance_dis_month")==NULL)?'':$schemes->__get("advance_dis_month"),
						"flat_discount"=>($schemes->__get("flat_dis")=="NULL" || $schemes->__get("flat_dis")=='undefined' || $schemes->__get("flat_dis")==NULL)?'':$schemes->__get("flat_dis"),
						"percentage_discount"=>($schemes->__get("per_dis")=="NULL" || $schemes->__get("per_dis")=='undefined' || $schemes->__get("per_dis")==NULL)?'':$schemes->__get("per_dis"),
						"equipment"=>$equip_name,
					);
    				$con_scheme_arr[]=$scheme_data;
    			}  
    		} 
    		$consumer_details=array(
    				"plant"=>$sites_values->__get("site_name"),
    				"feeder_name"=>$MM,
    				"consumer_name"=>$consumer->__get("consumer_name"),
					"dob"=>date("d-M-Y",strtotime($consumer->__get("dob"))),
    				"father_name"=>$consumer->__get("consumer_father_name"),
					'consumer_status' => $consumer->__get("consumer_status"), 
    				"act_date"=>date("d-M-Y",strtotime($consumer->__get("consumer_act_date"))),
    				"email_id"=>$consumer->__get("email_id"), 
					"primary_mob_no"=>$consumer->__get("consumer_code"),
					"state_name"=>$states->__get("state_name"),
    				"outstanding"=>$total,
    				"otp_due"=>$activation,
    				"package"=>$package_arr,
    				"mobile"=>$mob_total, 
					"scheme"=>$con_scheme_arr,
    		);
    		$casRegisters=$cashRegisterMapper->getCashRegisterDetailsByConsumerId($consumer->__get("consumer_id"),true,true);
			
    		if($casRegisters){
    			foreach ($casRegisters as $casRegister){ 
					$receipts_no=$cashRegisterMapper->getCashRegisterTransactionByReceiptNo($consumer->__get("consumer_id"),$casRegister->__get("receipt_number"),$casRegister->__get("user_id"));
    				  
					 if($receipts_no){
						$entry_type=array();$amount_type=array();$hindi=array();$charges_arr=array();$total_amount=0;$cr_entry_status=array();
						$reciept_no=$sites_values->__get("site_code").$casRegister->__get("user_id").$casRegister->__get("receipt_number");
						$receipt_count=$casRegister->__get("receipt_count");
						$user_id=$casRegister->__get("user_id");
						foreach($receipts_no as $receipt_no){
						
							$entry_type[]=($receipt_no["entry_status"]=='CHG'?'ENERGY':$receipt_no["entry_status"]);
							$amount_type[]=$receipt_no["cr_amount"];
							$cr_entry_status[]=$receipt_no["cr_entry_type"];
							$total_amount=$total_amount+$receipt_no["cr_amount"];
							$hindi_text=NULL;
							$charges=$packagesMapper->getChargesByparameter($receipt_no["entry_status"]);
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$hindi[]=$hindi_text;
							$data_charges=array(
								"hindi_text"=>$hindi_text,
								"amount"=>$receipt_no["cr_amount"],
								"transaction"=>$receipt_no["cr_amount"], 
								"transaction_id"=>$receipt_no["transaction_id"],
							);
							$charges_arr[]=$data_charges; 
							
							$date_val=date("d-M-Y g:i a",strtotime($receipt_no["timestamp"]));
						}
															
							$transaction_history=array(
									"date"=>$date_val,
									"amount"=> implode(",",$amount_type),
									"transaction_type"=>implode(",",$cr_entry_status),
									"entry_status"=>implode(",",$entry_type),
									"transaction_type_color"=>NULL,
									"is_print"=>1,
									"hindi_text"=>implode(",",$hindi),
									"total_collection" => $total_amount,
									"receipt_no" => $reciept_no,
									"receipt_count"=>$receipt_count,
									"receipt_user_id"=>$user_id, 
									"charges"=>$charges_arr, 
							);
						$transaction_historys[]=$transaction_history;
					}
    			}
    		}else{
    			$transaction_historys=array();
    		}
    		 
    		
    		   
    		$meterVal=array();
    		$meter_nos=$meterReadingsMapper->getAllMeterOnconsumerId($consumer->__get("consumer_id"));
			if($consumer->__get("site_id")!=62 && $consumer->__get("site_id")!=31){
				if($meter_nos){
					
					foreach ($meter_nos as $meter_no){
						if($meter_no["meter_no"]!='' && $meter_no["meter_no"]!=NULL && $meter_no["meter_no"]!='undefined' && $meter_no["meter_no"]!='null'){
							$meter=$meterReadingsMapper->getMeterReadingByMeterNo($meter_no["meter_no"],$consumer->__get("consumer_id"));
							$meterValue=array();
							if($meter){
								foreach($meter as $meters){
									$packageName=$packagesMapper->getPackageById($meters["package_id"]);
									//echo $packageName->__get("is_postpaid");exit;
									if(intval($packageName->__get("is_postpaid"))!=0){
										$packName=$packageName->__get("package_name");
										$meter_val=array(
												"datetime"=>date("d-M-Y g:i a",strtotime($meters["timestamp"])),
												"package_id"=>$meters["package_id"],
												"package_name"=>$packageName->__get("package_name"),
												"opening_reading"=>floatval($meters["meter_reading"]),
												"closing_reading"=>(floatval($meters["unit"])),
										);
										
										$meterValue[]=$meter_val;
									} 
									 
								}  
							}
							
							$data=array(
									"meter_no"=>$meter_no["meter_no"]." (".$packName.")",
									"reading_details"=>$meterValue
							);
							$meterVal[]=$data;
						}
					} 
				}
			}else{
				if($meter_nos){
					$meterValue=array();
					foreach ($meter_nos as $meter_no){
						if($meter_no["meter_no"]!='' && $meter_no["meter_no"]!=NULL && $meter_no["meter_no"]!='undefined' && $meter_no["meter_no"]!='null'){
						$meterValue=array();
						$meter=$meterReadingsMapper->getReadingByMeterNoCustomerData($consumer->__get("consumer_id"),$meter_no["meter_no"]);
						if($meter){
							foreach($meter as $meters){
								
								$packageName=$packagesMapper->getPackageById($meters["package_id"]);
								$packName=$packageName->__get("package_name");
								 
								$current_date = date_parse_from_format("Y-m-d", $meters["timestamp"]);
								$month = sprintf("%02d", $current_date["month"]);
								$year = $current_date["year"];
								$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
								$timestamp=$year."-".$month."-".$total_days_in_month;
								if($meters["start_reading"]==0){
									$reading_vals=$meterReadingsMapper->getConsumerMeterReading($consumer->__get("consumer_id"),$timestamp,$month,$year,1);
								}else{
									$reading_vals=$meterReadingsMapper->getConsumerMeterReading($consumer->__get("consumer_id"),$meters["timestamp"],$month,$year,0);
								}
								
								$meter_val=array(
										"datetime"=>date("d-M-Y g:i a",strtotime($reading_vals["timestamp"])),
										"package_id"=>$meters["package_id"],
										"package_name"=>$packageName->__get("package_name"),
										"opening_reading"=>floatval($reading_vals["meter_reading"]),
										"closing_reading"=>(floatval($reading_vals["unit"])),
								);
								$meterValue[]=$meter_val;
							 } 
						}
						if(count($meterValue)>0){
							$sort_arr = array();
							foreach ($meterValue as $key => $row)
							{
								$sort_arr[$key] = $row['datetime'];
							}
							array_multisort($sort_arr, SORT_DESC ,$meterValue);
						}
						$data=array(
								"meter_no"=>$meter_no["meter_no"]." (".$packName.")",
								"reading_details"=>$meterValue
						);
						$meterVal[]=$data;
						}
					} 
				}
			}
			$collectionAgents=$collectionAgentMapper->getCollBySiteId($consumer->__get("site_id"));
			if($collectionAgents){
				$line_manager_no=$collectionAgents[0]["agent_mobile"];
				$collectionAgent_no=$collectionAgents[0]["line_manager_no"];
			}else{
				$line_manager_no=0;
				$collectionAgent_no=0;
			}
			$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer->__get("consumer_id"));
    		$casRegistersCount=count($cashRegisterMapper->getCashRegisterDetailsByConsumerId($consumer->__get("consumer_id"),true));
			$data=array(
    				'connectionID'=>$consumer_connection_id,
    				"reading_history"=>$meterVal,
    				"consumer_details"=>$consumer_details,
    				"ActivationCharge"=>$actiCharges,
    				"customerType"=> $consumer_types ,
					"consumerSubType"=>$sub_consumer_types,
    				"CustomerCode"=>$consumer_mobile,
    				"transaction_historys"=>$transaction_historys,
    				
    				"helpLine_no"=>'0000-0000-0000',
					"outstanding"=>intval($oustanding),
					"transactionCount"=>$casRegistersCount,
					"version_code"=>'2',
					"line_manager_no"=>$line_manager_no,
					"collectionAgent_no"=>$collectionAgent_no
                  
    		);
    		$result="Customer Details Found";
    	}else{
    		$result="Customer Not Found";
    		$data=array();
    	}
    	 
    	$input=array('api'=>'consumer_detail',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit; 
    }
    
	public function consumerTransactionAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    	$equipmentMapper=new Application_Model_EquipmentMapper();
		$stateMapper=new Application_Model_StatesMapper();
		
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$consumer_id=$request->getParam("consumer_id");
		$order=$request->getParam("order");
         
    	$parameter=array(
    			"consumer_id"=>$consumer_id,
				"order"=>$order,
    	);
    		
    	$files=array();	$success=0;
    	$total_reading=0;$total_amt=0;$unit_rate=0;
    
    	$consumer_arr=array();
    	$consumer = $consumersMapper->getConsumerById($consumer_id);
    	if($consumer){
    		$success=1;
    		$data=array();
    		 
    		$casRegisters=$cashRegisterMapper->getCashRegisterDetailsByConsumerId($consumer->__get("consumer_id"),true,$order);
			$sites_values=$siteMapper->getSiteById($consumer->__get("site_id"));
			$states=$stateMapper->getStateById($sites_values->__get("state_id"));
    		if($casRegisters){
    			foreach ($casRegisters as $casRegister){
					$receipts_no=$cashRegisterMapper->getCashRegisterTransactionByReceiptNo($consumer->__get("consumer_id"),$casRegister->__get("receipt_number"),$casRegister->__get("user_id"));
    				  
					 if($receipts_no){
						$entry_type=array();$amount_type=array();$hindi=array();$charges_arr=array();$total_amount=0;$cr_entry_status=array();
						$reciept_no=$sites_values->__get("site_code").$casRegister->__get("user_id").$casRegister->__get("receipt_number");
						$receipt_count=$casRegister->__get("receipt_count");
						$user_id=$casRegister->__get("user_id");
						foreach($receipts_no as $receipt_no){
						
							$entry_type[]=($receipt_no["entry_status"]=='CHG'?'ENERGY':$receipt_no["entry_status"]);
							$amount_type[]=$receipt_no["cr_amount"];
							$cr_entry_status[]=$receipt_no["cr_entry_type"];
							$total_amount=$total_amount+$receipt_no["cr_amount"];
							$hindi_text=NULL;
							$charges=$packagesMapper->getChargesByparameter($receipt_no["entry_status"]);
							if($charges){
								$hindi_text=$charges["hindi_text"];
							}
							$hindi[]=$hindi_text;
							$data_charges=array(
								"hindi_text"=>$hindi_text,
								"amount"=>$receipt_no["cr_amount"],
								"transaction"=>$receipt_no["cr_amount"], 
								"transaction_id"=>$receipt_no["transaction_id"],
							);
							$charges_arr[]=$data_charges; 
							
							$date_val=date("d-M-Y g:i a",strtotime($receipt_no["timestamp"]));
						}
															
							$transaction_history=array(
									"date"=>$date_val,
									"amount"=> implode(",",$amount_type),
									"transaction_type"=>implode(",",$cr_entry_status),
									"entry_status"=>implode(",",$entry_type),
									"transaction_type_color"=>NULL,
									"is_print"=>1,
									"hindi_text"=>implode(",",$hindi),
									"total_collection" => $total_amount,
									"receipt_no" => $reciept_no,
									"receipt_count"=>$receipt_count,
									"receipt_user_id"=>$user_id, 
									"charges"=>$charges_arr, 
							);
						$data[]=$transaction_history;
					}
    			}
    		}else{
    			$data=array();
    		}
    		  
			 
    		$result="Customer Details Found";
    	}else{
    		$result="Customer Not Found";
    		$data=array();
    	}
    	 
    	$input=array('api'=>'consumer_transaction',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit; 
    }
	
    public function getTotalOutstandingByConsumerAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$activation_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
    
    	$request = $this->getRequest();
    	$consumer_id=$request->getParam("customer_id");
    	$files=array();	$success=1;
    	$parameter=array(
    			"customer_id"=>$consumer_id,
    	);
    	 
    	$Totaloustanding =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id);
    	$Totaloustanding_CHG =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"CHG");
    	$Totaloustanding_EED =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"EED");
    	$Totaloustanding_MED =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"MED");
    	$Totaloustanding_WATER =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"WATER");
    	$Totaloustanding_ACT =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"ACT");
    	$Totaloustanding_OTHERS =$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer_id,"OTHERS");
    
    	$result="Outstaning data is loaded successfully.";
    	$CHG_arr=array(
    			"entry_type"=>"Energy",
    			"outstanding_amount"=>$Totaloustanding_CHG,
    	);
    	$ACT_arr=array(
    			"entry_type"=>"Connection charges",
    			"outstanding_amount"=>$Totaloustanding_ACT,
    	);
    	$WATER_arr=array(
    			"entry_type"=>"WATER",
    			"outstanding_amount"=>$Totaloustanding_WATER,
    	);
    	$OTHER_arr=array(
    			"entry_type"=>"OTHERS",
    			"outstanding_amount"=>$Totaloustanding_OTHERS,
    	);
    	$EED_arr=array(
    			"entry_type"=>"EED",
    			"outstanding_amount"=>$Totaloustanding_EED,
    	);
    	$MED_arr=array(
    			"entry_type"=>"MED",
    			"outstanding_amount"=>$Totaloustanding_MED,
    	);
    	
    	 
    	$Outstaning=array();
    	array_push($Outstaning,$CHG_arr,$EED_arr,$MED_arr,$ACT_arr,$OTHER_arr,$WATER_arr);
    	$data=array(
    			"Outstanding"=>$Outstaning,
    			"totalOutstanding"=>$Totaloustanding
    	);
    	 
    	$input=array('api'=>'get_customer_outstanding',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }

    public function requestForConnectionAction(){
    
    	$ConnectionRequestMapper = new Application_Model_ConnectionRequestMapper();
    
    	$request = $this->getRequest();
    	 
    	$name=$request->getParam("name");
    	$mob_no=$request->getParam("mob_no");
    	$ref_name=$request->getParam("ref_name");
    	$ref_no=$request->getParam("ref_no");
    	$state=$request->getParam("state");
    	$site=$request->getParam("site");
    	$status=$request->getParam("status");
    	
    	$parameter=array("name"=>$name,
    			"mob_no"=>$mob_no,
    			"ref_name"=>$ref_name,
    			"ref_no"=>$ref_no,
    			"state"=>$state,
    			"site"=>$site,
    			"status"=>$status,
    	);
    	$success=0;
    
    	if($mob_no!=NULL || $ref_no!=NULL)
    	{
    		/*$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    		
    		$ConnectionRequest=new Application_Model_ConnectionRequest();
    		$ConnectionRequest->__set("name", $name);
    		$ConnectionRequest->__set("mob_no", $mob_no);
    		$ConnectionRequest->__set("ref_name", $ref_name);
    		$ConnectionRequest->__set("ref_no", $ref_no);
    		$ConnectionRequest->__set("state", $state);
    		$ConnectionRequest->__set("site", $site);
    		$ConnectionRequest->__set("status", $status);
    		$ConnectionRequest->__set("track_status", "Pending");
    		$ConnectionRequest->__set("entry_date", $timestamp);
    		
    		$ConnectionRequests=$ConnectionRequestMapper->addConnectionRequest($ConnectionRequest);*/
    		if(true){
    			$success=1;
    			 
				$otp=rand(1,9).rand(0,9).rand(0,9).rand(0,9);
	    		$sms=$otp." is your OTP for TARAurja App. Please do not share OTP with anyone.";
				$this->_smsNotification($mob_no, $sms);
	    
	    		$data=array(
	    				"otp"=>$otp,
	    				"name"=>$name,
	    				"mob_no"=>$mob_no,
	    				"ref_name"=>$ref_name,
	    				"ref_no"=>$ref_no,
	    				"state"=>$state,
	    				"site"=>$site,
	    				"status"=>$status,
	    		);
	    		$result = "Your request has been send.";
    		}else{
    			$result="Please enter correct values.";
    			$data=array();
    		}
    	}else{
    		$result="Please enter correct values.";
    		$data=array();
    	}
    
    	$input=array('api'=>'request-for-connection',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function requestForConnectionSuccessAction(){
		
    	$ConnectionRequestMapper = new Application_Model_ConnectionRequestMapper();
    
    	$request = $this->getRequest();
    	 
    	$name=$request->getParam("name");
    	$mob_no=$request->getParam("mob_no");
    	$ref_name=$request->getParam("ref_name");
    	$ref_no=$request->getParam("ref_no");
    	$state=$request->getParam("state");
    	$site=$request->getParam("site");
    	$status=$request->getParam("status");
    	
    	$parameter=array("name"=>$name,
    			"mob_no"=>$mob_no,
    			"ref_name"=>$ref_name,
    			"ref_no"=>$ref_no,
    			"state"=>$state,
    			"site"=>$site,
    			"status"=>$status,
    	);
    	$success=0;
    
    	if($mob_no!=NULL || $ref_no!=NULL)
    	{
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    		
    		$ConnectionRequest=new Application_Model_ConnectionRequest();
    		$ConnectionRequest->__set("name", $name);
    		$ConnectionRequest->__set("mob_no", $mob_no);
    		$ConnectionRequest->__set("ref_name", $ref_name);
    		$ConnectionRequest->__set("ref_no", $ref_no);
    		$ConnectionRequest->__set("state", $state);
    		$ConnectionRequest->__set("site", $site);
    		$ConnectionRequest->__set("status", $status);
    		$ConnectionRequest->__set("track_status", "Pending");
    		$ConnectionRequest->__set("entry_date", $timestamp);
    		
    		$ConnectionRequests=$ConnectionRequestMapper->addConnectionRequest($ConnectionRequest);
    		if($ConnectionRequests){
    			$success=1;
    			
	    		 
	    		$data=array(
	    				"name"=>$name,
	    				"mob_no"=>$mob_no,
	    				"ref_name"=>$ref_name,
	    				"ref_no"=>$ref_no,
	    				"state"=>$state,
	    				"site"=>$site,
	    				"status"=>$status,
	    		);
	    		$result = "Your request has been send.";
    		}else{
    			$result="Please enter correct values.";
    			$data=array();
    		}
    	}else{
    		$result="Please enter correct values.";
    		$data=array();
    	}
    
    	$input=array('api'=>'request-for-connection',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }

    public function getPackageByConsumerAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    
    	$consumer_id=$request->getParam("consumer_id");
    
    	$parameter=array(
    			"consumer_id"=>$consumer_id,
    	);
    
    	$success=0;
    	$total_reading=0;$total_amt=0;$unit_rate=0;
    
    	$consumer_arr=array();
    	$consumer = $consumersMapper->getConsumerById($consumer_id);
    	if($consumer){
    		$success=1;
    		$transaction_historys=array();
    		$consumer_mobile=$consumer->__get("consumer_code");
    		$consumer_connection_id=$consumer->__get("consumer_connection_id");
    		$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
    		$package_arr=array();
			$meter_name=$siteMasterMapper->getMMById($consumer->__get("site_meter_id"));
    		$MM="";
    		if($meter_name){
    			$MM=$meter_name->__get("meter_name")." (".$meter_name->__get("description").")"; 
    		}
    		if($consumerpackage){
    
    			foreach($consumerpackage as $package){
    				$watt_total=0;
    				$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
    				if($watt){
    					$watt_total=$watt->__get("wattage");
    				}
    				if($package["is_postpaid"]==0){
    					$reading="";$meter_no="";
    				}else{
    					$meterReadingMapper=new Application_Model_MeterReadingsMapper();
    					$latestReading=$meterReadingMapper->getLatestReadingByConsumerId($consumer_id,$package["package_id"]);
    					if($latestReading){
    						$reading=$latestReading->__get("meter_reading");
							$meter_no=$latestReading->__get("meter_no");
    					}else{$reading="";$meter_no="";}
    				}
    				$pack_data=array( 
							"feeder_name"=>$MM,
    						"package_id"=>$package["package_id"],
    						"package_name"=>$package["package_name"],
    						"package_cost"=>$package["package_cost"],
    						"is_postpaid"=>$package["is_postpaid"],
    							
    						"max_wattage_allowed"=>$watt_total,
    						"min_amount"=>$package["package_cost"],
    						"free_units"=>$package["unit"],
    						"rate_after_free_units"=>$package["extra_charges"],
    						"unit_rate"=>$package["extra_charges"],
    						"min_unit_rate"=>$package["min_charges"],
    						"max_unit_rate"=>$package["max_charges"],
    							
    						"wattage"=>$watt_total,
    						"status"=>$package["status"],
    						"meter_reading"=>$reading,
							"meterNumber"=>$meter_no  
    				);
    				$package_arr[]=$pack_data;
    			}
    		}
    		
    		$data=array(
    				'consumer_id'=>$consumer_id,
    				"packages"=>$package_arr,
    		);
    		$result="Package Details Found";
    	}else{
    		$result="Package Not Found";
    		$data=array();
    	}
    
    	$input=array('api'=>'get_package_by_consumer',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function getUpperPackageByPackageAndConsumerAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    
    	$consumer_id=$request->getParam("consumer_id");
    	$package_id=$request->getParam("package_id");
    
    	$parameter=array(
    			"consumer_id"=>$consumer_id,
    			"package_id"=>$package_id,
    	);
    
    	$success=0;
    	$total_reading=0;$total_amt=0;$unit_rate=0;
    
    	$consumer_arr=array();
    	$consumer = $consumersMapper->getConsumerById($consumer_id);
    	if($consumer){
    		$success=1;
    		$transaction_historys=array();
    		$consumer_mobile=$consumer->__get("consumer_code");
    		$consumer_connection_id=$consumer->__get("consumer_connection_id");
    		
    		$packae_value=$packagesMapper->getPackageById($package_id);
    		$curr_package_cost=$packae_value->__get("package_cost");
    		$curr_package_type=$packae_value->__get("is_postpaid");
    		
    		$package_arr=array();
    		 
    			$packages=$packagesMapper->getPackagesForFilter($consumer->__get("site_id"),null,$consumer->__get("site_meter_id"));
    			if($packages){
	    			foreach($packages as $package){
	    				$temp=0;
	    				$package_cost=$package["package_cost"];
	    				$is_postpaid=$package["is_postpaid"];
	    				
	    				if($curr_package_type==0 && ($is_postpaid==1 || $is_postpaid==2)){
	    					$temp=1;
	    				}elseif (($curr_package_type==0 && $is_postpaid==0) && ($curr_package_cost<$package_cost)){
	    					$temp=1;
	    				}elseif ($curr_package_type==1 && ($is_postpaid==1 || $is_postpaid==2)){
	    					$temp=1;
	    				}elseif ($curr_package_type==2 && ($is_postpaid==1)){
	    					$temp=1;
	    				}elseif (($curr_package_type==2 && $is_postpaid==2) && ($curr_package_cost<$package_cost)){
	    					$temp=1;
	    				}
	    				
	    				if($temp==1){
		    				$watt_total=0;
		    				$watt=$wattageMapper->getWattageBywattId($package["wattage"]);
		    				if($watt){
		    					$watt_total=$watt->__get("wattage");
		    				}
		    				 
		    				$pack_data=array(
		    						"package_id"=>$package["package_id"],
		    						"package_name"=>$package["package_name"],
		    						"package_cost"=>$package["package_cost"],
		    						"is_postpaid"=>$package["is_postpaid"],
		    						"max_wattage_allowed"=>$watt_total,
		    						"min_amount"=>$package["package_cost"],
		    						"free_units"=>$package["unit"],
		    						"rate_after_free_units"=>$package["extra_charges"],
		    						"unit_rate"=>$package["extra_charges"],
		    						"wattage"=>$watt_total,
		    						 
		    				);
		    				$package_arr[]=$pack_data;
	    				}
	    			}
    		}
    
    		$data=array(
    				'consumer_id'=>$consumer_id,
    				"packages"=>$package_arr,
    		);
    		$result="Package Details Found";
    	}else{
    		$result="Package Not Found";
    		$data=array();
    	}
    
    	$input=array('api'=>'get_package_by_consumer',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }

    public function changePackageForConsumerAction(){
    
    	$packageChangeMapper=new Application_Model_PackageChangeMapper();
    	$consumerMapper=new Application_Model_ConsumersMapper();
    	
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	 
    	$consumer_id=$request->getParam("consumer_id");
    	$curr_package_id=$request->getParam("curr_package_id");
    	$new_package=$request->getParam("new_package_id");
    	 
    
    	$parameter=array(
    		    "consumer_id"=>$consumer_id,
    			"curr_package_id"=>$curr_package_id,
    			"new_package_id"=>$new_package,
    	);
    		
    	$files=array();	$success=0; $data=array();
    	 
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp_date = $date->toString("yyyy-MM-dd HH:mm:ss");
     
    	$consumers=$consumerMapper->getConsumerById($consumer_id);
    	if($consumers){
				$packageChange=new Application_Model_PackageChange();
				
				$packageChange->__set("site_id", $consumers->__get("site_id"));
				$packageChange->__set("consumer_id", $consumer_id);
				$packageChange->__set("curr_package_id", $curr_package_id);
				$packageChange->__set("new_package_id", $new_package);
				$packageChange->__set("timestamp", $timestamp_date);
    		 	$packageChanges=$packageChangeMapper->addPackageChange($packageChange);
    			if($packageChanges){
	    			$success=1;
	    			$result = "Your request for package change has been completed.";
	    		}else{
	    			$result = "Error. Kripya kuch der baad try karein.";
	    		}
    	}else{
    		$result = "Error. You do not have this package.";
    	}
    		
    	$input=array('api'=>'change_consumer_package',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function cashRegisterForCustomerAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$siteMapper=new Application_Model_SitesMapper();
    
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	$consumer_id=$request->getParam("consumer_id");
    	
    	$parameter=array(
    			"consumer_id"=>$consumer_id
    	);
    		
    	$files=array();	$success=0;
    
    	 
     
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp_date = $date->toString("yyyy-MM-dd");
		$start_date=date('Y-m-d', strtotime($timestamp_date."-1 year"));
		$end_date =$timestamp_date;
		 
    	$consumers=$consumersMapper->getConsumerById($consumer_id);
    	if ($consumers){
    		$site_arr=array($consumers->__get("site_id"));
    		
    		$datas=array();
    		$consumers_data=$cashRegisterMapper->getCashRegister($site_arr,NULL,$start_date,$end_date,null,$skip_count,null,$consumer_id);
    			
    		if($consumers_data){
    			foreach ($consumers_data as $consumer_data) {
    				 
    				$sites_val=$sitesMapper->getSiteById($consumers->__get("site_id"));
					$reciept_no=$sites_val->__get("site_code").$consumer_data->__get("user_id").$consumer_data->__get("receipt_number");
    				
					if($consumer_data->__get("entry_status")=='CHG'){
						$entry_status='Energy';
					}elseif($consumer_data->__get("entry_status")=='ACT'){
						$entry_status='Connection charges';
					}else{
						$entry_status=$consumer_data->__get("entry_status");
					}
					$transaction_history = array(
    						"consumer_name" => $consumers->__get("consumer_name"),
    						"consumer_id" => $consumers->__get("consumer_id"),
    						"connection_id" => $consumers->__get("consumer_connection_id"),
    						"site_name" => $sites_val->__get("site_name"),
    						"transaction_id" => $consumer_data->__get("transaction_id"),
    						"amount" => $consumer_data->__get("cr_amount"),
    						"cr_entry_type" => $consumer_data->__get("cr_entry_type"),
    						"entry_status" => $entry_status,
    						"receipt_no" => $reciept_no,
    						"total_collection" => $consumer_data->__get("cr_amount"),
    						"date" => date("d-M-Y g:i a",strtotime($consumer_data->__get("timestamp"))),
    				);
    				$datas[]=$transaction_history;
    			 
    			}
    			$result = "All transaction are loaded successfully.";
    			$success=1;
    		}else{
    			$result = "No transaction found.";
    		}
    			
    		$consumers_data=$cashRegisterMapper->getBillsByRangeSiteId($start_date,$end_date,$site_arr,NULL,NULL,NULL,$consumer_id);
    
    		$total=$consumers_data["EED"]+$consumers_data["CHG"]+$consumers_data["MED"]+$consumers_data["WATER"]+$consumers_data["ACT"]+$consumers_data["OTHERS"];
    		$charges_arr=array();
    		if(intval($consumers_data["CHG"])>0){
    			$charges=array(
    					"label"=>"Energy","amount"=>intval($consumers_data["CHG"])
    			);
    			$charges_arr[]=$charges;
    		}
    		if(intval($consumers_data["ACT"])>0){
    			$charges=array(
    					"label"=>"Connection charges","amount"=>intval($consumers_data["ACT"])
    			);
    			$charges_arr[]=$charges;
    		}
    		if(intval($consumers_data["WATER"])>0){
    			$charges=array(
    					"label"=>"WATER","amount"=>intval($consumers_data["WATER"])
    			);
    			$charges_arr[]=$charges;
    		}
    		if(intval($consumers_data["OTHERS"])>0){
    			$charges=array(
    					"label"=>"OTHERS","amount"=>intval($consumers_data["OTHERS"])
    			);
    			$charges_arr[]=$charges;
    		}
    		if(intval($consumers_data["EED"])>0){
    			$charges=array(
    					"label"=>"EED","amount"=>intval($consumers_data["EED"])
    			);
    			$charges_arr[]=$charges;
    		}
    		if(intval($consumers_data["MED"])>0){
    			$charges=array(
    					"label"=>"MED","amount"=>intval($consumers_data["MED"])
    			);
    			$charges_arr[]=$charges;
    		}
    		 
    		$sites=$sitesMapper->getSiteById($consumers->__get("site_id"));
    		$data=array(
    				"cash_register"=>$datas,
    				"charges"=>$charges_arr,
    				"site_name"=>$sites->__get("site_name"),
    				"TotalCollection"=>intval($total),
    				 
    		);
    	}else{
    		$result = "Error. You are not an Consumer.";
    		$data=array();
    	}
    	$input=array('api'=>'cash_register',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
    public function addServiceRequestAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$consumerComplainMapper=new Application_Model_ConsumerComplainMapper();
    	$request = $this->getRequest();
    	 
    	$consumer_id=$request->getParam("consumer_id");
    	$service_id=$request->getParam("service_id");
    	$remark=$request->getParam("remarks");
    	
    	$parameter=array("consumer_id"=>$consumer_id,
    			"service_id"=>$service_id,
    			"remark"=>$remark
    			
    	);
    	$success=0;
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    	
    	$consumers=$consumersMapper->getConsumerById($consumer_id);
    	if($consumers)
    	{
    		$success=1; 
    		//$remarks=explode(",", $remark);
    		//for ($i=0;$i<count($remarks);$i++){
	    		$consumerComplains=new Application_Model_ConsumerComplain();
	    		$consumerComplains->__set("consumer_id", $consumer_id);
	    		$consumerComplains->__set("service_id", $service_id);
	    		$consumerComplains->__set("remark_id", $remark);
	    		$consumerComplains->__set("consumer_id", $consumer_id);
	    		$consumerComplains->__set("entry_date", $timestamp);
	    		$consumerComplains->__set("status", "Pending");
	    		$consumerComplain=$consumerComplainMapper->addConsumerComplain($consumerComplains);
    		//}
    		$data=array();
    		$result = "Your Request has been submitted.";
    		 
    	}else{
    		$result="Please enter correct data.";
    		$data=array();
    	}
    
    	$input=array('api'=>'add-Service-Request',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }

    public function updateRatingAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$consumerComplainMapper=new Application_Model_ConsumerComplainMapper();
    	$request = $this->getRequest();
    
    	$consumer_id=$request->getParam("consumer_id");
    	$service_id=$request->getParam("service_id");
    	$rating=$request->getParam("rating");
    	 
    	$parameter=array("consumer_id"=>$consumer_id,
    			"service_id"=>$service_id,
    			"rating"=>$rating
    			 
    	);
    	$success=0;
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    	 
    	$consumers=$consumersMapper->getConsumerById($consumer_id);
    	if($consumers)
    	{
    		$success=1;
    		$ratings=$consumerComplainMapper->updateRatingByReqId($consumer_id,$service_id,$rating);
    		$data=array();
    		$result = "Your Feedback has been submitted.";
    		 
    	}else{
    		$result="Please enter correct data.";
    		$data=array();
    	}
    
    	$input=array('api'=>'update-rating',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }

    public function getAllServiceRequestAction(){
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$consumerComplainMapper=new Application_Model_ConsumerComplainMapper();
		
		$collectionAgentMapper=new Application_Model_CollectionAgentsMapper();
		$userMapper=new Application_Model_UsersMapper();
    	$request = $this->getRequest();
    
    	$consumer_id=$request->getParam("consumer_id");
    	 
    	$parameter=array("consumer_id"=>$consumer_id);
    	$success=0;
    	$service_arr=array();
    	$consumers=$consumersMapper->getConsumerById($consumer_id);
    	if($consumers)
    	{
    		$success=1;
    		$consumerComplains=$consumerComplainMapper->getConsumerComplain($consumer_id);
    		 
    		if($consumerComplains){
    			foreach ($consumerComplains as $consumerComplain){
					$complainRemarks_name=array();$complainRemarks_id=array();
					$remarks_id=explode(",",$consumerComplain["remark_id"]);
					if(count($remarks_id)>0){
						for($i=0;$i<count($remarks_id);$i++){
							$complainRemarks=$consumersMapper->getComplainRemark($remarks_id[$i]);
							if($complainRemarks){ 
								$complainRemarks_name[]=$complainRemarks[0]["remark"];
								$complainRemarks_id[]=$remarks_id[$i];
							}
						}
					}
    				$ServiceRequests=$consumersMapper->getServiceRequest($consumerComplain["service_id"]);
    				if($ServiceRequests){
    					$ServiceRequests_name=$ServiceRequests[0]["service"];
    				}
					if($consumerComplain["status"]!='Pending'){
						if($consumerComplain["approve_type"]=='M'){
							$collectionAgent = $collectionAgentMapper->getCollectionAgentById($consumerComplain["approve_by"]);
							$Created_By=  $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
						}else{
							$users = $userMapper->getUserById($consumerComplain["approve_by"]);
							$Created_By= $users->__get("user_fname") . " " . $users->__get("user_lname");
						}
						$approve_date=date("d-M-Y g:i a",strtotime($consumerComplain["approve_date"]));
					}else{
						$Created_By="";
						$approve_date="";
					}
					
						 
    				$data=array(
    					"consumer_id"=>$consumer_id,
    					"consumer_name"=>$consumers->__get("consumer_name"),
    					"remark"=>implode(",",$complainRemarks_name), 
    					"service"=>$ServiceRequests_name,
    					"remark_id"=>implode(",",$complainRemarks_id), 
    					"service_id"=>$ServiceRequests[0]["id"],
    					"status"=>$consumerComplain["status"],
    					"complain_id"=>$consumerComplain["id"],
    					"request_on_date"=>date("d-M-Y g:i a",strtotime($consumerComplain["entry_date"])),
    					"rating"=>($consumerComplain["rating"]==NULL?"":$consumerComplain["rating"]),
						"approve_by"=>$Created_By,
						"approve_date"=>$approve_date,
    				);
    				$service_arr[]=$data;
    			}
    		}
    		 
    		$result = "Data has been Loaded.";
    		 
    	}else{
    		$result="Please enter correct data.";
    		$data=array();
    	}
    
    	$input=array('api'=>'update-rating',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$service_arr
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
    
	public function getCustomerCashLedgerAppAction(){ 
    
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$sitesMapper=new Application_Model_SitesMapper();
    	$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
    	$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
    	$packagesMapper=new Application_Model_PackagesMapper();
    	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
    	$wattageMapper=new Application_Model_WattageMapper();
    	$schemeMapper=new Application_Model_SchemesMapper();
    	$cashRegisterMapper=new Application_Model_CashRegisterMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$userMapper=new Application_Model_UsersMapper();  
    	
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    	 
    	$customer_id=$request->getParam("customer_id");
    	 
    	$parameter=array(
			"customer_id"=>$customer_id,
    	);
    		
    	$files=array();	$success=0;$ledger=0;
    	$consumer = $consumersMapper->getConsumerById($customer_id);
    	 
    	if($consumer){
		
			$sites=$sitesMapper->getSiteById($consumer->__get("site_id"));
			if($sites){
				$siteName=$sites->__get("site_name");
			}else{
				$siteName="";
			}
			$MM=$siteMasterMapper->getMMById($consumer->__get("site_meter_id"));
			if($MM){
				$MMName=$MM->__get("meter_name")." (".$MM->__get("description").")"; 
			}else{
				$MMName="";
			}
			$type_of_me=$consumer->__get("type_of_me");
    		$consumer_type_vals=$consumersMapper->getConsumerTypeById($type_of_me);
    		$consumer_types=NULL;
    		if($consumer_type_vals){
    			$consumer_types=$consumer_type_vals["consumer_type_name"];
    		}
			$sub_type_of_me=$consumer->__get("sub_type_of_me");
    		$sub_type_of_me_vals=$consumersMapper->getConsumerTypeById($sub_type_of_me);
    		$sub_consumer_types=NULL;
    		if($sub_type_of_me_vals){
    			$sub_consumer_types=$sub_type_of_me_vals["consumer_type_name"];
    		}
    	
			$oustanding=$cashRegisterMapper->getOutstandingDataByConsumerId(NULL,$consumer->__get("consumer_id"));
			$otp_due=$consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
		
    		$success=1;
    		$cashRegister = $cashRegisterMapper->getAllEntryByConsumerId($consumer->__get("consumer_id"));
			$typesArray = array("CREDIT", "DEBIT", "DISCOUNT", "ACTIVATION", "PENALTY");
			$cashRegister = $cashRegisterMapper->applyTypeFilter($cashRegister, $typesArray);
			$cashRegister_data=array();$ledger=array();
			
			if($cashRegister){
				$number=0;
				foreach($cashRegister as $cash_register){
					if($cash_register->__get("cr_status")!='INACTIVE' && $cash_register->__get("entry_status")!='SD'){
						$dr=0;$cr=0;
						if($cash_register->__get("cr_entry_type") == "DEBIT"){
							$dr=$cash_register->__get("cr_amount");
							$debit  = $debit + $cash_register->__get("cr_amount");
						}else{
							$cr=$cash_register->__get("cr_amount");
							$credit = $credit + $cash_register->__get("cr_amount");
						}
						$balance = ($debit - $credit);
						
						$trans_date=date_parse_from_format("Y-m-d", $cash_register->__get("timestamp"));
						$day=$trans_date["day"];
						$month= $trans_date["month"];
						$year= $trans_date["year"];
																	
						$dateObj   = DateTime::createFromFormat('!m', $month);
						$monthName = $dateObj->format('M');
						if($cash_register->__get("cr_entry_type")!='DEBIT'){
							$perticulars="Collection-".$monthName.",".$year."-".$cash["entry_status"];
						}else{
							if($cash_register->__get("entry_status")=='CHG'){
								$packageMapper=new Application_Model_PackagesMapper();
								$packages=$packageMapper->getPackageById($cash_register->__get("package_id"));
								if($packages){
									$perticulars="CHG-".$monthName.",".$year."-".$packages->__get("package_name");
								}else{
									$perticulars="CHG-".$monthName.",".$year."-Server";
								}
							}elseif($cash_register->__get("entry_status")=='EED'){
								$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
								$consumerScheme=$consumerSchemeMapper->getConsumerSchemeByConsumerData($month,$year,$cash_register->__get("consumer_id"));
								if($consumerScheme){
									$totalDiscount=$consumerScheme["discount_month"];
									$RemaingDiscount=$consumerScheme["discount_month"]-$consumerScheme["equip_month_dis"];
								}
								$perticulars="EED-".$monthName.",".$year."-".$RemaingDiscount."/".$totalDiscount;
							}else{
								$perticulars=$cash_register->__get("entry_status")."-".$monthName.",".$year;
							}
						}
																 
						$meter_reading=0;$unit=0;$meter_reading_data=array();
						if($cash_register->__get("cr_entry_type") == "DEBIT" && $cash_register->__get("entry_status")=='CHG'){
							$readingMapper=new Application_Model_MeterReadingsMapper();
							$readingCheck=$readingMapper->checkMonthMeterReadingsByDateConsumerId($cash_register->__get("consumer_id"),NULL,$cash_register->__get("timestamp"));
							if($readingCheck){
								$reading=$readingMapper->getMeterReadingsByConsumerId($cash_register->__get("consumer_id"),NULL,$cash_register->__get("timestamp")); 
								if($reading){
									$meter_reading = sprintf("%.2f",($reading["meter_reading"]));
									$Act_date = date_parse_from_format("Y-m-d", $cash_register->__get("timestamp"));
									$Act_day = $Act_date["day"];
									$Act_month = sprintf("%02d", $Act_date["month"]); 
									$Act_year = $Act_date["year"]; 
									$units=$readingMapper->getTotalUnitByConsumerId($cash_register->__get("consumer_id"),$cash_register->__get("package_id"),$Act_month,$Act_year);
									$unit=sprintf("%.2f", $units);
								}
							}
						}
						if ($cash["cr_entry_type"]!='DEBIT' || $cash["transaction_type"]=='(W)') {
							if ($cash["transaction_type"]=='(W)') {
								$users = $userMapper->getUserById($cash_register->__get("user_id"));
								$createdBy= $users->__get("user_fname") . " " . $users->__get("user_lname");
							} else {
								$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cash_register->__get("user_id"));
								$createdBy= $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
							} 
						}else{
							$createdBy= "Server";
						}													
						$data_value=array(
								"transaction_date"=>date("d-M-Y g:i a",strtotime($cash_register->__get("timestamp"))), 
								"Particulars"=>$perticulars,
								"entry_type"=>$cash_register->__get("entry_status"),
								"created_by"=>$createdBy,
								"credit"=>$cr, 
								"debit"=>$dr,
								"balance"=>$balance,
								"cr_entry_type"=>$cash_register->__get("cr_entry_type"),
								"transaction_status"=>$cash_register->__get("cr_status"),
								"number"=>$number++,
								"unit_count"=>$unit,
								"meter_reading"=>$meter_reading,
						);
						$ledger[]=$data_value;
					}
        	 
    			}
				
				$sort_arr = array();
				foreach ($ledger as $key => $row)
				{
					$sort_arr[$key] = $row['number'];
				}
				array_multisort($sort_arr, SORT_DESC ,$ledger);
				
				$ledgers=array(); 
				foreach($ledger as $cash_registers_val){
					$data_value=array(
								"transaction_date"=>$cash_registers_val["transaction_date"], 
								"Particulars"=>$cash_registers_val["Particulars"], 
								"entry_type"=>$cash_registers_val["entry_type"], 
								"created_by"=>$cash_registers_val["created_by"], 
								"credit"=>$cash_registers_val["credit"], 
								"debit"=>$cash_registers_val["debit"], 
								"balance"=>$cash_registers_val["balance"], 
								"cr_entry_type"=>$cash_registers_val["cr_entry_type"], 
								"transaction_status"=>$cash_registers_val["transaction_status"], 
								"number"=>$cash_registers_val["number"], 
								"unit_count"=>$cash_registers_val["unit_count"], 
						);
					$ledgers[]=$data_value;
				}
				$data=array(
					"ledger"=>$ledgers,
					"plant_name"=>$siteName,
					"feeder_name"=>$MMName,
					"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
					"consumer_status"=>$consumer->__get("consumer_status"),
					"consumer_name"=>$consumer->__get("consumer_name"),
					"consumer_father_name"=>$consumer->__get("consumer_father_name"),
					"mobile_no"=>$consumer->__get("consumer_code"),
					"email"=>($consumer->__get("email_id")==NULL)?"":$consumer->__get("email_id"),
					"consumer_act_date"=>date("d-M-Y",strtotime($consumer->__get("consumer_act_date"))),
					"Consumer_type"=>$consumer_types,
					"consumerSubType"=>$sub_consumer_types,
					"outstanding"=>$oustanding,
					"otp_due"=>$otp_due
				);
    			$result="Data is loading successfully.";
    		}else{
    			$result="Error. Kripya kuch der baad try karein.";
    		}
    	}else{
    		$result="Consumer not found.";
    	}
    	
    	$input=array('api'=>'get_customer_cash_ledger',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"files" => $files,
    			"commandResult" => $commandResult,
    	);
    		
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    }
	
	public function checkMeterNoAction(){
    
    	$inventoryMapper=new Application_Model_MeterInventoryMapper();
    		 
    	date_default_timezone_set('Asia/Kolkata');
    	$request = $this->getRequest();
    
    	$meter_no=$request->getParam("meter_no");
    
    	$parameter=array(
    			"meter_no"=>$meter_no,
    	);
    
    	$success=0;
    	
    	$consumer_arr=array();
    	$meter_nos = $inventoryMapper->getMeterNoByNo($meter_no);
    	if($meter_nos){
    		$success=1;
    		$data=array(
    				'meter_no'=>$meter_no,
    				"reading"=>$meter_nos["reading"],
    		);
    		$result="Meter Number Details Found";
    	}else{
    		$result="Meter Number Not Found";
    		$data=array();
    	}
    
    	$input=array('api'=>'check_meter_no',
    			'parameters'=>$parameter);
    	$commandResult = array(
    			"success"=>$success,
    			"message" =>$result,
    			"data"=>$data
    	);
    	$arr = array(
    			"input" => $input,
    			"commandResult" => $commandResult,
    	);
    
    	$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;
    	exit;
    } 
    /* End Consumer Application API*/
	
    public function longCodeAction() {
		/*$file = 'test.txt';
        $current = file_get_contents($file);
        $current .= "check";
        file_put_contents($file, $current);*/
	

        $request = $this->getRequest();
        $who = $request->getParam("who"); 
        $what = $request->getParam("what");
        $sender = $request->getParam("sender");
        $what_string = $what;
        $number = $who ;
		
		$success=0;
        $data=new stdClass(); 
      	if($sender=="mobile"){
      		
			if($who=="" || $who==null){
				$who='';
			}
			if($what=="" || $what==null){
				$what_string='';
			}	
			if(true){	
      			$input=array('what'=>$what_string,
						 'who'=>$who);
				$commandResult = array(
						 "success"=>$success,
						 "message" =>"This Application will not work. Please use new Application",
						 "data"=>$data
			              );
				$arr = array(
		                 "input" => $input,
		                 "commandResult" => $commandResult,
		                );
				$json = json_encode($arr, JSON_PRETTY_PRINT);
				echo $json;
				exit;		
      		}
      	}

        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $collectionAgent = $collectionAgentsMapper->getCollectionAgentByPhone($number);
		$sitesMapper=new Application_Model_SitesMapper();
        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $consumersMapper = new Application_Model_ConsumersMapper();
		$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
		$packagesMapper=new Application_Model_PackagesMapper();
        $what = explode(" ", $what);
        $type = $what[1];
	
        $zendDate = new Zend_Date();
        $zendDate->setTimezone("Asia/Calcutta");
		
        if($type=="PICO" or $type=="AP1" or $type=="AP2" or $type=="AP3" or $type=="TMTS1"  or $type=="TMTS2" or $type=="MINI")
        {
		
            $type = strtolower($type);
            $string = "Number - ".$who;
            $string .= "|";
            $string .= "Message - ".$what[2];
            $string .= "|";
            $zendDate = new Zend_Date();
            $zendDate->setTimezone("Asia/Calcutta");
            $string .= "Time - ".$zendDate->toString();
            $file = $type.'.txt';
            $current = file_get_contents($file);
            $current .= $string . "\r\n";
            file_put_contents($file, $current);
            $type = strtoupper($type); 
        }
	if($type=="MINI"){
		$number = $who;
                   			$message = $what[2];
                   			$siteMasterMappers =new Application_Model_SiteMasterMeterMapper();
                   			$siteMapper=new Application_Model_SitesMapper();
                   			$devices=$siteMasterMappers->getMasterMeterByNumber($number);
							
							if($devices){
                   			$value_array=explode(";", $message);
						 	
							 
                   			if(count($value_array)==6){ 
                   			//$site_code = preg_replace('/-/','',$value_array[0]);
                   			//$meter_id=$value_array[1];
                   			//$timestamps=$value_array[0]." ".$value_array[1];
                   			//$zendDate = new Zend_Date($timestamps,"YY/MM/dd HH:mm");
                   		   // $timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
                   		    
							
                   				if($value_array[0]=='$MTR1'){
                   					$feedar='FEEDER1';
                   				}elseif ($value_array[0]=='$MTR2'){
                   					$feedar='FEEDER2';
                   				}else{
                   					$feedar='FEEDER3';
                   				}
                   				
                   		    $site_id=$devices["site_id"];
                   		    
                   		    $sites=$siteMapper->getSiteById($site_id);
                   		    $meter_keyword=strtoupper($sites->__get("site_code").$feedar);
                   		    
                   			$ins_vol=$value_array[1];
                   			$ins_cur=$value_array[2];
                   			$pow_fac="";
                   			$ins_freq="";
                   			$act_eng=$value_array[3];
                   			$app_eng=$value_array[4];
                   			
                   			$siteMasterMappers =new Application_Model_SiteMasterMeterMapper();
                   			$siteMasterReadingMapper=new Application_Model_SiteMeterReadingMapper();
                   			$siteMaster=new Application_Model_SiteMeterReading();
                   			$checkMeter=$siteMasterMappers->getMasterMeterByMeterKey($meter_keyword);
                   			 
                   			$date = new Zend_Date();
                   			$date->setTimezone("Asia/Calcutta");
                   			$timestamp_del = $date->toString("yyyy-MM-dd HH:mm:ss");
                   			
                   			$trans_date=date_parse_from_format("Y-m-d h:i:s", $timestamp_del);
                   			$hour=$trans_date["hour"];
                   			$min=$trans_date["minute"];
                   			$correcthm=floatval($hour.".".$min); 
							
							$tempval=0;
                   			if($checkMeter){
                   				foreach ($checkMeter as $checkMeters){
								$start_t= floatval($checkMeters["start_time"]);
								$end_t= floatval($checkMeters["end_time"]);
							 	
	                   			  if(($correcthm>=$start_t && $correcthm<=$end_t) || ($correcthm<=$start_t && $correcthm>=$end_t)){
								  $site_id=$checkMeters["site_id"];
		                   			$meter_id=$checkMeters["id"]." ";
									
		                   			$val=$siteMasterReadingMapper->getMeterReadingByMeterId($meter_id);
									
									if(count($val)>0){
	                   			  		$reading_val=preg_replace('/KWH/','',$act_eng)-($val[0]->__get("reading"));
	                   			  	}else{
	                   			  		$reading_val=preg_replace('/KWH/','',$act_eng);
	                   			  	}
	                   			  		                    			
		                   			$siteMaster->__set("site_id",$site_id);
		                   			$siteMaster->__set("meter_id",$meter_id);
		                   			$siteMaster->__set("reading_date",$timestamp_del);
		                   			$siteMaster->__set("reading",$reading_val);
		                   			
		                   			$Site_MM=$siteMasterReadingMapper->addNewSiteMeterReading($siteMaster);
		                   			
		                   			$MeterDataMapper=new Application_Model_MeterDataMapper();
		                   			$meterData=new Application_Model_MeterData();
		                   			$meterData->__set("site_id",$site_id);
		                   			$meterData->__set("meter_id",$meter_id);
		                   			$meterData->__set("ins_vol",$ins_vol);
		                   			$meterData->__set("ins_cur",$ins_cur);
		                   			$meterData->__set("pow_fac",$pow_fac);
		                   			$meterData->__set("ins_freq",$ins_freq);
		                   			$meterData->__set("act_eng",$act_eng);
		                   			$meterData->__set("app_eng",$app_eng);
		                   			 
		                   			$MeterDataValue=$MeterDataMapper->addNewSiteMaster($meterData);
		                   			
			                   			if($Site_MM){
											$tempval=1;
				                   		 } 
		                   			} 
                   			  }
								if($tempval==1){
											$success=1;
												$data=array(
														"Message"=>$message,
												);
												$result = "Data has been inserted into database.";
								}else{
											$success=0;
											$data=array();
											$result = "Invalid Feeder Time.";
								}
	                   			}else{
	                   				$success=0;
	                   				$data=array();
	                   				$result = "Invalid Meter.";
	                   			}
                   			}else{
                   				$success=0;
                   				$data=array();
                   				$result = "Invalid request.";
                   			}
                   			}else{
                   				$success=0;
                   				$data=array();
                   				$result = "Invalid request.";
                   			}
							 
		$input=array('what'=>$what_string,
        			'who'=>$who);
        	$commandResult = array(
        			"success"=>$success,
        			"message" =>$result,
        			"data"=>$data
        	);
        	$arr = array(
        			"input" => $input,
        			"commandResult" => $commandResult,
        	);
        	 
        
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        	exit;
                     	 
	}
	
	 
	 if($type=="UPDATEFEEDER"){
	  
			
			
        	$siteStatusMapper=new Application_Model_SiteStatusMapper();
        	$feedar_id = $what[2];
        	$MMs=$siteMasterMapper->getMMById($feedar_id);
        	if($MMs){
        		$site_id=$MMs->__get("site_id");
        		$sites=$sitesMapper->getSiteById($site_id);
        		$start_time=$MMs->__get("start_time");
        		$end_time=$MMs->__get("end_time");
        		$is_24_hr=$MMs->__get("is_24_hr");
        		
	        	$stringfeedar=" ".$feedar_id." ";
	        	$records=explode($stringfeedar,$what_string);
	        	$timestamp=$records[1];
	        	$eachdata=explode(",",$timestamp);
	        	
	        	$maxDates = $siteStatusMapper->getActiveByFeedarId($feedar_id);
	        	if($maxDates){
	        		$maxDate=$maxDates["timestamp"];
	        	}else{
	        		$maxDate="0000-00-00";
	        	}
	        	$temp=0;$allerror=array();
        			for($i=0;$i<count($eachdata);$i++){
        				$invalidStarttime=explode(" ",$eachdata[$i]);
        				$dateofday=$invalidStarttime[0];
        				$invaildhr=$invalidStarttime[1];
        				$invalidTime=explode(":",$invaildhr);
        				for ($j=0;$j<count($invalidTime);$j++){
        					if($dateofday>=$maxDate){
		        				$siteStatus=new Application_Model_SiteStatus();
		        				$siteStatus->__set("site_id",$site_id);
		        				$siteStatus->__set("status","INACTIVE");
		        				$siteStatus->__set("timestamp",$dateofday);
		        				$siteStatus->__set("feedar_id",$feedar_id);
		        				$siteStatus->__set("start_date",$invalidTime[$j]);
		        				$statsite = $siteStatusMapper->addNewSiteStatus($siteStatus);
		        				$temp=1;
        					}else{
        						$allerror[]=$dateofday;
        					}
        				}
        			}
        			if ($temp==1)
        			{
        			$date = new Zend_Date();
        			$date->setTimezone("Asia/Calcutta");
        			$ValidTimestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
        			$siteStatus=new Application_Model_SiteStatus();
        			$siteStatus->__set("site_id",$site_id);
        			$siteStatus->__set("status","ACTIVE");
        			$siteStatus->__set("timestamp",$ValidTimestamp);
        			$siteStatus->__set("feedar_id",$feedar_id);
        			$siteStatus->__set("start_date",0);
        			$statsiteValid = $siteStatusMapper->addNewSiteStatus($siteStatus);
        			
        				$success=1;
        				$result="Records inserted successfully.";
        				$data = array(
        						'Site_id' => $site_id,
        						'Site Name' => $sites->__get("site_name"),
        						'feedar_id' => $feedar_id,
        				);
        			}else{
        				$result = "Error. Invalid dates ".implode(",", array_unique($allerror));
        				$data=new stdClass();
        			}
        				
        		} else{
        			$result="SiteId is not found, Please check";
        			$data=new stdClass();
        		}
        	 
        	$input=array('what'=>$what_string,
        			'who'=>$who);
        	$commandResult = array(
        			"success"=>$success,
        			"message" =>$result,
        			"data"=>$data
        	);
        	$arr = array(
        			"input" => $input,
        			"commandResult" => $commandResult,
        	);
        	 
        
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        	exit;
        }   			
        
		if($type=="FEEDERDATA"){
		//$siteStatusMapper=new Application_Model_SiteStatusMapper();
        	$feedar_id = $what[2]; 
        	$data=array();
        	$dates=array();
        	$MMs=$siteMasterMapper->getMMById($feedar_id);
        	if($MMs){
        		$site_id=$MMs->__get("site_id");
        		$start_time=$MMs->__get("start_time");
        		$end_time=$MMs->__get("end_time");
        		$is_24_hr=$MMs->__get("is_24_hr");
        		 
        		$siteStatusMapper=new Application_Model_SiteStatusMapper();
        		$date = new Zend_Date();
        		$date->setTimezone("Asia/Calcutta");
        		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
        		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
        		$day= $trans_date["day"];
        		$month= $trans_date["month"];
        		$year= $trans_date["year"];
        
        		$statsite = $siteStatusMapper->getSiteStatusByFeedarId($feedar_id);
        		if($statsite){
        			$success=1;
        				
        			$maxDates = $siteStatusMapper->getActiveByFeedarId($feedar_id);
        			$maxDate=$maxDates["timestamp"];
					if($maxDates){
						$maxDate=$maxDates["timestamp"];
					}else{
						$maxDate="0000-00-00";
					}
        			foreach ($statsite as $statsites)
        			{
        				$AllDates[]=$statsites["timestamp"];
        			}
        			 
        			$totalDaysInMonth=cal_days_in_month(CAL_GREGORIAN,$month,$year);
        			 
        			for($i=1; $i<=$totalDaysInMonth; $i++){
        				$Alltimes=array();
        				$start_date=$year."-".sprintf("%02d",$month)."-".sprintf("%02d",$i);
        				
        				$daystat = $siteStatusMapper->getdayStatusByFeedarId($feedar_id,$start_date);
        				if($daystat){
	        				foreach ($daystat as $daystats)
	        				{
	        					$Alltimes[]=$daystats["start_date"];
	        				}
        				}
        				$times_val=array();
        				if($is_24_hr!=1){
	        				for($j=intval($start_time);$j!=intval($end_time);$j++){
	        					    $start_date_time=$start_date." ".sprintf("%02d",$j).":00:00"; 
		        					 
		        					if($start_date_time<=$maxDate){
		        						$set=1;
		        					}else{
		        						$set=0;
		        					}
		        					if(in_array($start_date, $AllDates)){
		        						if(in_array($j, $Alltimes)){
		        							$active=0;
		        						}else{
		        							$active=1;
		        						}
		        					}else{
		        						$active=1;
		        					} 
		        					$times=array(
		        						'hr' => sprintf("%02d",$j),
		        						'active' => $active,
		        						'set'=> $set,
	        					 	);
		        					$times_val[]=$times;
		        					if($j==23){
		        						$j=-1;
		        					}
	        				}
        				}
        				
        				
        				$result="Feedar Detail Found Successfully.";
        				$data_val = array(
        						'date' => $start_date,
        						'time'=>$times_val
        				);
        				$data[]=$data_val;
        			}
        			$dates=array( "dates"=>$data 	);
        		}else{
        			$success=1;
        			$totalDaysInMonth=cal_days_in_month(CAL_GREGORIAN,$month,$year);
					
					$maxDates = $siteStatusMapper->getActiveByFeedarId($feedar_id);
        			$maxDate=$maxDates["timestamp"];
					if($maxDates){
						$maxDate=$maxDates["timestamp"];
					}else{
						$maxDate="0000-00-00";
					}
        			for($i=1; $i<=$totalDaysInMonth; $i++){
        				$times_val=array();
        				$start_date=$year."-".sprintf("%02d",$month)."-".sprintf("%02d",$i);
	        			if($is_24_hr!=1){
	        				for($j=$start_time;$j!=$end_time;$j++){
	        				  $start_date_time=$start_date." ".sprintf("%02d",$j).":00:00"; 
		        					 
		        					if($start_date_time<=$maxDate){
		        						$set=1;
		        					}else{
		        						$set=0;
		        					}
	        						$times=array(
		        						'hr' => sprintf("%02d",$j),
		        						'active' => 1,
		        						'set'=> $set,
	        					 	);
		        					$times_val[]=	$times;
		        					if($j==23){
		        						$j=-1;
		        					}
		        					 
	        					}
	        				}
        				$result="Feedar Detail Found Successfully.";
        				$data_val = array(
        						'date' => $start_date,
        						'time'=>$times_val
        						 
        				);
        				$data[]=$data_val;
        			}
        			$dates=array( 	"dates"=>$data 	);
        		}
        			
        	} else{
        		$result="Feedar Id is not found, Please check";
        		$data=new stdClass();
        	}
        
        	$input=array('what'=>$what_string,
        			'who'=>$who);
        	$commandResult = array(
        			"success"=>$success,
        			"message" =>$result,
        			"data"=>$dates
        	);
        	$arr = array(
        			"input" => $input,
        			"commandResult" => $commandResult,
        	);
        	 
        
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        	exit;
        }
		  
		if($type=="SITEMETER"){
		
		$agentSite=array();
			$collectionMapper=new Application_Model_CollectionAgentsMapper();
			$Agents=$collectionMapper->getCollectionAgentByPhone($number);
			if($Agents){
				$Sites_details=$Agents->__get("site_id"); 
				$agentSite=explode(",", $Sites_details);
			}
			 
			
        	$sitesMappers=new Application_Model_SitesMapper();
        	$siteMeter=new Application_Model_SiteMasterMeterMapper();
        	$sites=$sitesMappers->getAllSites();
        	if($sites){
        		$success=1;
				/*/$collectionMapper=new Application_Model_CollectionAgentsMapper();
				$Agents=$collectionMapper->getCollectionAgentByPhone($number);
				if($Agents){
					$Sites_details=$Agents->__get("site_id"); 
					$agentSite=explode(",", $Sites_details);
				}else{
					$agentSite=array();
				}*/
        		foreach ($sites as $site){
        		
					if(in_array($site->__get("site_id"), $agentSite)){
						$sites_reading=$sitesMappers->getAllSitesForMeterReading($site->__get("site_id"));
						$meter_values=array();
						if($sites_reading){
							foreach ($sites_reading as $sites_readings){
								$siteMasterMeters=$siteMeter->getMasterMeterReadingByMeterId($sites_readings["id"]);
								 
								 if($siteMasterMeters){
									$siteMasterMeter=$siteMasterMeters[0];
									$last_reading=$siteMasterMeter->__get("reading");
									$reading_on = $siteMasterMeter->__get("reading_date");
								 }else{
									$last_reading=0;
									$reading_on=0;
								 }
								
								$starttime=($sites_readings["start_time"]!=null)?$sites_readings["start_time"]:"";
								 $endtime=($sites_readings["end_time"]!=null)?$sites_readings["end_time"]:"";
								$data_read=array(
										"meter_id" => $sites_readings["id"],
										"last_reading" =>$last_reading,
										"reading_on" =>$reading_on,
										"meter_description" => $sites_readings["description"],
										"meter_name"=>$sites_readings["meter_name"],
										"start_time"=>$starttime,
										"end_time"=>$endtime,
										"is_24_hr"=>$sites_readings["is_24_hr"]
								);
								$meter_values[]=$data_read;
							}
						}
						$sites=array(
								"site_id"=>$site->__get("site_id"),
								"site_name"=>$site->__get("site_name"),
								"site_code"=>$site->__get("site_code"),
								"meters"=>$meter_values
						);
						
						$sites_detail[]=$sites;
					}
        		}
        		$data=array(
        				"sites"=>$sites_detail
        		);
        		$result="Site Details Found";
        		 
        	}else{
        		$result="Site Details Not Found";
        		 
        		$data=array();
        	}
        	$input=array('what'=>$what_string,
        			'who'=>$who);
        	$commandResult = array(
        			"success"=>$success,
        			"message" =>$result,
        			"data"=>$data
        	);
        	$arr = array(
        			"input" => $input,
        			"commandResult" => $commandResult,
        	);
        	
        	 
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        	exit;
        }	
		
		
	  if($type=='SETMMREADING'){
         
        	
       if(count($what)==5){
        		
        	$reading_date=$what[2];
        	$site_code=$what[3];
        	$meter_detail=$what[4];
        	$date = new Zend_Date($reading_date,"yyyy-MM-dd HH:mm:ss");
            $timestamp = $date->toString("MMM dd, yyyy");

        	$sitesMapper=new Application_Model_SitesMapper();
        	$siteReading=new Application_Model_SiteMeterReadingMapper();
        	$siteRead=new Application_Model_SiteMeterReading();
        	$siteCode=$sitesMapper->getSiteBySiteCode($site_code);
        	if($siteCode->__get("site_name")!="---Deleted---"){
        		$site_id=$siteCode->__get("site_id");
				$site_name=$siteCode->__get("site_name");
				$agent_id=$collectionAgent->__get("collection_agent_id");
        		$result_val= $this->updateSiteMeterReading($site_id,$meter_detail,$reading_date,$agent_id);
        		$results_value=$result_val;
        	    if (strpos($result_val, 'Error') !== false || strpos($result_val, 'Meter name') !== false)
        		   {
	        		   	
	        		   	$results=explode("Error", $result_val);
	        		   	$data_vals=array();
	        		   	for($j=1;$j<count($results);$j++){
		        		    $data_vals[]=$results[$j];
	        		   	}
	        		   	if(count($data_vals)>=3){
	        		   		$success=0;
	        		   		$result="All Master Meter Reading are not updated, please try again";
	        		   		$data=array();
	        		   	}else{
	        		   		$success=2;
		        		   	$data=array("meter_id"=>$data_vals);
		        		   	$result=$results_value;
		        		   	$result.=" Master Meter".implode(",", $data_vals)." Reading not updated, please try again";
	        		   	}
        		   }else{
	        		   	$success=1;
	        		   	$result="All Master Meter Readings for ".$site_name." udpated for ".$timestamp;
        		   } 
        	}else{
        		$result="Site not found";
        		$data=array();
        	 }
        	}else{
        		$result="Parameter Missing.";
        		$data=array();
        	}
        	$input=array('what'=>$what_string,
        			'who'=>$who);
        	$commandResult = array(
        			"success"=>$success,
        			"message" =>$result,
        			"data"=>$data
        	);
        	$arr = array(
        			"input" => $input,
        			"commandResult" => $commandResult,
        	);
        	
        	 
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        	exit;
        }
 if($type=="SITEMPPT"){
  
			$agentSite=array();
			$collectionMapper=new Application_Model_CollectionAgentsMapper();
			$Agents=$collectionMapper->getCollectionAgentByPhone($number);
			if($Agents){
				$Sites_details=$Agents->__get("site_id"); 
				$agentSite=explode(",", $Sites_details);
			}
			 
        	$sitesMappers=new Application_Model_SitesMapper();
        	$siteMeters=new Application_Model_MpptsReadingMapper();
        	$sites=$sitesMappers->getAllSites();
        	if($sites){
        		$success=1;
        		foreach ($sites as $site){
        		if(in_array($site->__get("site_id"), $agentSite)){
						$sites_reading=$sitesMappers->getAllSitesForMpptReading($site->__get("site_id"));
						$meter_values=array();
						if($sites_reading){
							foreach ($sites_reading as $sites_readings){
								$siteMasterMeters=$siteMeters->getMpptsReadingByMeterId($sites_readings["id"]);
			
								if($siteMasterMeters){
									$siteMasterMeter=$siteMasterMeters[0];
									$last_reading=$siteMasterMeter->__get("reading");
									$reading_on = $siteMasterMeter->__get("reading_date");
								}else{
									$last_reading=0;
									$reading_on=0;
								}
								 
								$data_read=array(
										"mppt_id" => $sites_readings["id"],
										"last_reading" =>$last_reading,
										"reading_on" =>$reading_on,
										"mppt_description" => $sites_readings["description"],
										"mppt_name"=>$sites_readings["mppt_name"]
								);
								$meter_values[]=$data_read;
							}
						}
						$sites=array(
								"site_id"=>$site->__get("site_id"),
								"site_name"=>$site->__get("site_name"),
								"site_code"=>$site->__get("site_code"),
								"mppts"=>$meter_values
						);
						$sites_detail[]=$sites;
        			}
        			
        		}
				
        		$data=array(
        				"sites"=>$sites_detail
        		);
        		$result="Site Details Found";
        		 
        	}else{
        		$result="Site Details Not Found";
        		 
        		$data=array();
        	}
        	$input=array('what'=>$what_string,
        			'who'=>$who);
        	$commandResult = array(
        			"success"=>$success,
        			"message" =>$result,
        			"data"=>$data
        	);
        	$arr = array(
        			"input" => $input,
        			"commandResult" => $commandResult,
        	);
        	 
        
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        	exit;
        }
        if($type=='SETMPPTREADING'){
        	 
        	 
        	if(count($what)==5){
        
        		$reading_date=$what[2];
        		$site_code=$what[3];
        		$meter_detail=$what[4];
        		$date = new Zend_Date($reading_date,"yyyy-MM-dd HH:mm:ss");
        		$timestamp = $date->toString("MMM dd, yyyy");
        		$sitesMapper=new Application_Model_SitesMapper();
        		$siteReading=new Application_Model_MpptsReadingMapper();
        		$siteRead=new Application_Model_SiteMeterReading();
        		$siteCode=$sitesMapper->getSiteBySiteCode($site_code);
        		if($siteCode->__get("site_name")!="---Deleted---"){
        			$site_id=$siteCode->__get("site_id");
        			$site_name=$siteCode->__get("site_name");
					$agent_id=$collectionAgent->__get("collection_agent_id");
        			$result_val= $this->updateSiteMpptReading($site_id,$meter_detail,$reading_date,$agent_id);
        			$results_value=$result_val;
        			if (strpos($result_val, 'Error') !== false || strpos($result_val, 'MPPTs ID') !== false)
        			{
        				 
        				$results=explode("Error", $result_val);
        				$data_vals=array();
        				for($j=1;$j<count($results);$j++){
        					$data_vals[]=$results[$j];
        				}
        				if(count($data_vals)>=3){
        					$success=0;
        					$result="All MPPTs Reading are not updated, please try again";
        					$data=array();
        				}else{
        					$success=2;
        					$data=array("mppt_id"=>$data_vals);
        					$result=$results_value;
        					$result.=" MPPTs ".implode(",", $data_vals)." Reading not updated, please try again";
        				}
        			}else{
        				$success=1;
        				$result="All MPPTs Readings for ".$site_name." udpated for ".$timestamp;
        			}
        		}else{
        			$result="Site not found";
        			$data=array();
        		}
        	}else{
        		$result="Parameter Missing.";
        		$data=array();
        	}
        	$input=array('what'=>$what_string,
        			'who'=>$who);
        	$commandResult = array(
        			"success"=>$success,
        			"message" =>$result,
        			"data"=>$data
        	);
        	$arr = array(
        			"input" => $input,
        			"commandResult" => $commandResult,
        	);
        	 
        
        	$json = json_encode($arr, JSON_PRETTY_PRINT);
        	echo $json;
        	exit;
        }

        if ($type == "BAL") {
            $result = $this->getConsumerBalance($number);
        } else {
            if ($collectionAgent) {
                $customer_code = $what[2];
                $con = $consumersMapper->getConsumersByColumnValue("consumer_connection_id", $customer_code);

                switch ($type) {
                    case "CID2PKG":
                        if($con)
                        {
                            $package_id = $con[0]->__get("package_id");
                            $result = "Package id of the customer is ".$package_id;
                        }
                        else
                        {
                            $result = "Connection id not found";
                        }
                        break;
                    case "CABAL":
                        $amount = abs($what[3]);
                        $b = $collectionAgent->__get("agent_balance");
                        $name = $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");;
                        $result = "Dear " . $name . ", your current Account Balance is Rs. " . $b . ".";
                        break;
                    case "ACT":
					date_default_timezone_set('Asia/Kolkata');
                         $amount = abs($what[3]);
			$checkData=0;$act_transaction=0;$consumer_name='';$consumer_father_name='';
                        $result = $this->rechargeConsumer($sender,$collectionAgent, $type, $amount, $customer_code);
						 $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                        $consumer = $consumers[0];
                        if (strpos($result, 'Connection ID') !== false) 
						{
							$success=1;$checkData=1;
							$allData = explode(" ", $result);
							$act_tran = explode(".", $allData[5]);
							$act_transaction=$act_tran[0];
							$consumer_father_name= $consumer->__get("consumer_father_name");
							$consumer_name=  $consumer->__get("consumer_name");
						}
						if($checkData==1){
						$data=array('connectionID'=>$customer_code,
								'amount'=> $amount,
								'transactionID'=>$act_transaction,
								'consumerName'=>$consumer_name,
								'consumerFatherName'=>$consumer_father_name,
								'timestamp'=>date("d/m/Y g:i a"),
							);
						}else{
							$data=new stdClass();
						}
                        break;
                    case "CHG":
					date_default_timezone_set('Asia/Kolkata');
                        $amount = abs($what[3]);
			$checkData=0;$act_transaction=0;$consumer_name='';$consumer_father_name='';
                        $result = $this->rechargeConsumer($sender,$collectionAgent, $type, $amount, $customer_code);
			$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                        $consumer = $consumers[0];
                        if (strpos($result, 'Connection ID') !== false) 
						{
							$success=1;$checkData=1;
							$allData = explode(" ", $result);
							$chg_tran = explode(".", $allData[5]);
							$chg_transaction=$chg_tran[0];
							$consumer_father_name= $consumer->__get("consumer_father_name");
							$consumer_name=  $consumer->__get("consumer_name");
						}
						if($checkData==1){
						$data=array('connectionID'=>$customer_code,
								'amount'=> $amount,
								'transactionID'=>$chg_transaction,
								'consumerName'=>$consumer_name,
								'consumerFatherName'=>$consumer_father_name,
								'timestamp'=>date("d/m/Y g:i a"),
							);
						}else{
							$data=new stdClass();
						}
                        break;

		    case "ACTCHG":
			date_default_timezone_set('Asia/Kolkata');
			$amounts=explode(':',$what[3] ) ;
                        $actAmount = abs($amounts[0]);
				$chgAmount = abs($amounts[1]);
				$checkData=0;
				$act_transaction=0;$chg_transaction=0;$consumer_name='';$consumer_father_name='';
                        $result = $this->rechargeConsumer($sender,$collectionAgent, 'ACT', $actAmount, $customer_code);
                        $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                        $consumer = $consumers[0];
                        if (strpos($result, 'Connection ID') !== false) 
						{
							$success=1;$checkData=1;
							$allData = explode(" ", $result);
							$act_tran = explode(".", $allData[5]);
							$act_transaction=$act_tran[0];
							$consumer_father_name= $consumer->__get("consumer_father_name");
							$consumer_name=  $consumer->__get("consumer_name");
						}
						
                        $result = $this->rechargeConsumer($sender,$collectionAgent, 'CHG', $chgAmount, $customer_code);
						if (strpos($result, 'Connection ID') !== false) 
						{
							$success=1;$checkData=1;
							$allData = explode(" ", $result);
							$chg_transac = explode(".", $allData[5]);
							$chg_transaction=$chg_transac[0];
						}
						if($checkData==1){
						$data=array('connectionID'=>$customer_code,
								'actamount'=>$actAmount,
								'chgamount'=>$chgAmount,
								'acttransactionID'=>$act_transaction,
								'chgtransactionID'=>$chg_transaction,	
								'consumerName'=>$consumer_name,
								'consumerFatherName'=>$consumer_father_name,
								'timestamp'=>date("d/m/Y g:i a"),
							);
						}else{
							$data=new stdClass();
						}
						
                        break;

                    case "CNC":
                        $amount = abs($what[3]);
                        $result = $this->addFollowup($collectionAgent, $type, $amount, $customer_code,$sender);
                        break;
                    case "MTR":
					
                         if(!$con)
                        {
                            $result = "Error. Customer ID ".$customer_code." not found. Please check.";
                        }
                        elseif($con[0]->__get("type")==0) 
                        {
                            $packagesMapper = new Application_Model_PackagesMapper();
							$siteMapper=new Application_Model_SitesMapper();
                            $package_id = $con[0]->__get("package_id");
                            $package = $packagesMapper->getPackageById($package_id);
                            $package_name = $package->__get("package_name");
							$site_id = $con[0]->__get("site_id");
							$sites=$siteMapper->getSiteById($site_id);
							$site_name=$sites->__get("site_name");
                            $result = "Connection ID ".$customer_code." is not a Metered Connection. Please check Connection ID and try again. Connection ID ".$customer_code." belongs to ".$site_name." , s/o ".$con[0]->__get("consumer_father_name")." with Package ".$package_name.".";
                        }
                        else
                        {
                            $latestReading = $meterReadingsMapper->getLatestReadingByConsumerId($con[0]->__get("consumer_id"));
                            $start_reading = 0;
                            if($latestReading)
                            {
                                $start_reading = $amount = abs($latestReading->__get("meter_reading"));
                            }
                            $end_reading = $amount = abs($what[3]);
                            if($end_reading>=$start_reading) 
                            {
							
                                $result = $this->addMTR($collectionAgent, $start_reading, $end_reading, $customer_code,$sender);
                            }
                            else
                            {
                                $connection_id = $con[0]->__get("consumer_connection_id");
                                $result = "Error. Previous Reading for ".$connection_id." was ".$start_reading.". New reading must be bigger. Please check and try again.";
                            }
                        }
                        $total_reading=0;$total_amt=0;$unit_rate=0;
                        $packagesMapper=new Application_Model_PackagesMapper();
                        $cashMapper=new Application_Model_CashRegisterMapper();
                         
                        $consumer_arr=array();
                        $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                        if($consumers){
                        	$transaction_historys=array();
                        	$consumer=$consumers[0];
                        	$package=$packagesMapper->getPackageById($consumer->__get("package_id"));
                        	 $cashRe=$cashMapper->calculateActivationAmount('ACTIVATION',$consumer->__get("consumer_id"));
                        	$actiCharges=$cashRe;
                        	if($actiCharges==false){$actiCharges=0;}

                        	$outstanding=$consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
                        	$activation= $consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
                        	$total=$outstanding+$activation;
                        	$consumer_details=array(
                        			"consumer_name"=>$consumer->__get("consumer_name"),
									"father_name"=>$consumer->__get("consumer_father_name"),
                        			"mobile"=>$consumer->__get("consumer_code"),
                        			"act_date"=>$consumer->__get("consumer_act_date"),
                        			"outstanding"=>$total,
                        			"current_package"=> $package->__get("package_name"),
									"otp_due"=>$activation
                        	);
                        	$casRegisters=$cashMapper->getCashRegisterDetailsByConsumerId($consumer->__get("consumer_id"));
                        	 
                        	foreach ($casRegisters as $casRegister){
								$Type=$casRegister->__get("cr_entry_type");
                        		$transaction_type = ($Type=='DEBIT') ? "#ed6b75" : (($Type=='CREDIT') ? "#36c6d3" : "#659be0");
                        		$hindi_text=NULL;
 								$charges=$packagesMapper->getChargesByparameter($casRegister->__get("entry_status"));
 								if($charges){
 									$hindi_text=$charges["hindi_text"];
 								}
                        		$transaction_history=array(
                        				"transaction_id"=> $casRegister->__get("transaction_id"),
                        				"date"=>$casRegister->__get("timestamp"),
                        				"amount"=> $casRegister->__get("cr_amount"),
										"transaction_type"=>$casRegister->__get("cr_entry_type"),
                        				"transaction_type_color"=>$transaction_type,
										"is_print"=>1,
                        				"hindi_text"=>$hindi_text
                        		);
                        		$transaction_historys[]=$transaction_history;
                        	}
                        
                        	$meterVal=array();
                        	$meter=$meterReadingsMapper->getMTRReadingByConsumerId($consumer->__get("consumer_id"));
                        	 
							if($meter!=false){
								foreach ($meter as $meters){
									$meter_val=array(
											"reading"=>$meters["meter_reading"],
											//"amount"=>$meters["cr_amount"],
											"datetime"=>$meters["timestamp"]
									);
									$meterVal[]=$meter_val;
								}
							}
                        }
                        if (strpos($result, 'ka bill') !== false)
                        {
                        	$success=1;
                        	$total_unit = explode("Total Unit", $result);
                        	$totalUnit = explode(".", $total_unit[1]);
                        	$total_reading = $totalUnit[0];
                        	
                        	$amt = explode("amount Rs.", $result);
                        	$r_amount = explode("hai", $amt[1]);
                        	$total_amt = $r_amount[0];
                        	
                        	$rate = explode("Rate Rs", $result);
                        	$rates = explode("/unit", $rate[1]);
                        	$unit_rate = $rates[0];
                        	
//                         $data_val=	$this->getConsumerDetails($customer_code);
                        	 
                        	
                        	
                        $data=array('connectionID'=>$customer_code,
                        		"amount"=>$total_amt,
								"oldReading"=>$start_reading,
								"newReading"=>$end_reading,
								"totalUnit"=>$total_reading,
								"rate"=>$unit_rate,
								"ActivationCharge"=>$actiCharges,
                        		"reading_history"=>$meterVal,
                        		"consumer_details"=>$consumer_details,
                        		"transaction_historys"=>$transaction_historys
                        		
                        );
                     
                    
                        }elseif(strpos($result, 'ki Old Meter Reading') !== false){
                        	$success=1;
                        	 $data=array('connectionID'=>$customer_code,
                        		"amount"=>$total_amt,
								"oldReading"=>$start_reading,
								"newReading"=>$end_reading,
								"totalUnit"=>$total_reading,
								"rate"=>$unit_rate,
								"ActivationCharge"=>$actiCharges,
                        		"reading_history"=>$meterVal,
                        		"consumer_details"=>$consumer_details,
                        		"transaction_historys"=>$transaction_historys
                        		
                        );
                        }
                        else{
                        	$data=array();
                        }                        
                        break;
                       
                
                   case "ENROL":
                        //TARA ENROL BHFAKT:ARJUN_DHAWAN:SANJAY_DHAWAN:9650408880:012:0600:0120
						//BHBHLD:99996:TEST_USER:TEST_FATHER:9953300332:193,0:00:0000000:10:10:0:Shops_Advanced:23
                    	$packagesMapper = new Application_Model_PackagesMapper();
                    	 
                        $data = explode(":",$customer_code);
						 $duplicate = false;
						   
                        if(count($data)!=13)
                        {
                            $result = "Invalid request";
							$data=new stdClass();
                            break;
                        }
                        else 
                        {
                        	$check=0;
                            $site_code = $data[0];
                            $generated_connection_id = $data[1];
                            $consumer_name = ucwords(strtolower(str_replace("_", " ", $data[2])));
                            $consumer_father_name = ucwords(strtolower(str_replace("_", " ", $data[3])));
                            $consumer_code = $data[4];
                            $package_id = $data[5];
                            $micro_price = intval($data[6]);
                            $meter_start_reading = intval($data[7]);
                            $activation_charge = intval($data[8]);
                            $activation_amount = intval($data[9]);
                            $collection_amount = intval($data[10]);
			    			$type_of_consumer = $data[11];
			    			$site_meter_id = $data[12]; 
					
			    		$packagesDetail=explode(",", $package_id);
			    		$package_count=count($packagesDetail);
			    		$package_id=$packagesDetail[0];
			    		$packages= $packagesMapper->getPackageById($package_id);
			    		if($packages){
			    			if ($packages->__get("is_postpaid")==1 && $package_count==3){
			    				$meter_start_reading=$packagesDetail[1];
			    				$micro_price=$packagesDetail[2];
			    				$check=1;
			    			}elseif ($packages->__get("is_postpaid")==2 && $package_count==2){
			    				$check=1;
			    			}elseif ($packages->__get("is_postpaid")==0 && $package_count==1){
			    				$check=1;
			    			}
			    		}
			    		
			    		if($check==1){
                            $sitesMapper = new Application_Model_SitesMapper();
                            $site = $sitesMapper->getSiteBySiteCode($site_code);
                            if($site->__get("site_name")=="---Deleted---")
                            {
                                $result = "Site not found";
                                break;
                            }
                            $zendDate = new Zend_Date();
                            $zendDate->setTimezone("Asia/Calcutta");
                            $activation_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
                            $site_id = $site->__get("site_id");
                            $consumersMapper = new Application_Model_ConsumersMapper();
                            $consumer = new Application_Model_Consumers();
                            
                            $package = $packagesMapper->getPackageById($package_id);
                            $package_name = "N/A";
                            if($package)
                            {
                                $package_name = $package->__get("package_name");
								$is_postPaid =  $package->__get("is_postpaid");
                            }
							
							$consumer_type=$consumersMapper->getConsumerTypeByName(str_replace('_', ' ', $type_of_consumer));
                            $type_name=1;
                            if($consumer_type){
                            	$type_name=$consumer_type["consumer_type_id"];
                            }
							
                            $consumer->__set("site_id",$site_id);
                            $consumer->__set("package_id",$package_id);
                            $consumer->__set("consumer_code",$consumer_code);
                            $consumer->__set("consumer_name",$consumer_name);
                            $consumer->__set("consumer_father_name",$consumer_father_name);
                            $consumer->__set("consumer_status","new");
                            $consumer->__set("consumer_act_charge",$activation_charge);
                            $consumer->__set("consumer_act_date",$activation_date);
                            $consumer->__set("micro_entrprise_price",$micro_price);
                            $consumer->__set("is_micro_enterprise","0");
                            $consumer->__set("sms_opt_in","1");
                            $consumer->__set("site_meter_id", $site_meter_id);
							$consumer->__set("suspension_date",NULL);
							$consumer->__set("created_by",$name = $collectionAgent->__get("collection_agent_id"));
							$consumer->__set("create_type","(M)");
							$consumer->__set("type_of_me",$type_name);
							$settingMapper=new Application_Model_SettingsMapper();
							
							$setings=$settingMapper->getSettingById(30);
							
							if($setings->__get("setting_value")=="no"){
								$consumer_first_status="use";
							}else{
								$consumer_first_status="new";
							}
							$consumer->__set("consumer_first_status",$consumer_first_status);
                            if($micro_price!=0)
                            {
                                $consumer->__set("is_micro_enterprise","1");
                                $consumer->__set("meter_start_reading",$meter_start_reading);
                            }
                            
                            $consumer_id = $consumersMapper->addNewConsumer($consumer);
							if($consumer_id){
                            	$cpMapper=new Application_Model_ConsumersPackageMapper();
                            	$cp=new Application_Model_ConsumersPackage();
                            	$cp->__set("package_id",$package_id);
                            	$cp->__set("consumer_id",$consumer_id);
                            	$cpMapper->addNewConsumersPackage($cp);
                            }
                            $connection_id = $site_code.$consumer_id;
                            if($generated_connection_id!="00000")
                            {
                                $new_connection_id = $site_code.$generated_connection_id;
                                $getConsumer = $consumersMapper->getConsumerByConnectionID($new_connection_id);
                                if($getConsumer->__get("consumer_name")=="")
                                {
                                    $connection_id=$new_connection_id;
                                    $duplicate = false;
                                }
                                else
                                {
                                    $result = "Duplicate Connection ID found (".$getConsumer->__get("consumer_name")." s/o ".$getConsumer->__get("consumer_father_name").", ID - ".$new_connection_id."). Generating Temporary ID ".$connection_id.". Please update correct ID with HQ as soon as possible.";
                                    //$this->_smsNotification($number, $result);
                                    $duplicate = true;
                                }
                            }
                            $packages= $packagesMapper->getPackageById($package_id);
                            if($packages){
                            	if($packages->__get("is_postpaid")==0){  
                            		$package_id=$packagesDetail[0];
                            	}elseif ($packages->__get("is_postpaid")==1){
                            		 
                            		$date = new Zend_Date();
                            		$date->setTimezone("Asia/Calcutta");
                            		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
                            		$meterReading=new Application_Model_MeterReadings();
                            		$meterReading->__set("meter_reading",$packagesDetail[1]);
                            		$meterReading->__set("consumer_id",$consumer_id);
                            		$meterReading->__set("timestamp",$timestamp);
									$meterReading->__set("package_id",$package_id);
									$meterReading->__set("start_reading",1);
									 
                            		$meterReadingsMapper->addNewMeterReading($meterReading);
                            		
                            
                            		$consumer->__set("micro_entrprise_price",$packagesDetail[2]);
                            		$meter_start_reading=$packagesDetail[1];
                            		$micro_price=$packagesDetail[2];
                            		
                            	}elseif ($packages->__get("is_postpaid")==2){
                            		$date = new Zend_Date();
                            		$date->setTimezone("Asia/Calcutta");
                            		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
                            		$meterReading=new Application_Model_MeterReadings();
                            		$meterReading->__set("meter_reading",$packagesDetail[1]);
                            		$meterReading->__set("consumer_id",$consumer_id);
                            		$meterReading->__set("timestamp",$timestamp);
									$meterReading->__set("package_id",$package_id);
									$meterReading->__set("start_reading",1);
									 
                            		$meterReadingsMapper->addNewMeterReading($meterReading);
                            		
                            	}
                            }
                            $consumer->__set("consumer_connection_id",$connection_id);
                            $consumer->__set("consumer_id",$consumer_id);
							$consumer->__set("enterprise_type",'FlatUnit');
                            $consumersMapper->updateConsumer($consumer);
                            $result = "Consumer added successfully";
                            if($consumer_id)
                            {
								$success=1;
								
								$cashRegisterMapper=new Application_Model_CashRegisterMapper();
								$sitesMapper = new Application_Model_SitesMapper();
								$site = $sitesMapper->getSiteById($site_id);
							    $receipt_no=$cashRegisterMapper->getCashRegisterTransactionByReceiptNo($consumer_id,NULL,$collectionAgent->__get("collection_agent_id"),true,'CREDIT');
								$invoice_no=$cashRegisterMapper->getCashRegisterTransactionByReceiptNo($consumer_id,NULL,$collectionAgent->__get("collection_agent_id"),true,'DEBIT',$site->__get("site_code"));
                     							
                     			$receiptNo=intval($receipt_no[0]["receipt_number"])+1;
								$invoice_no=intval($invoice_no[0]["receipt_number"])+1; 
								
                                $result = $this->rechargeConsumer($sender,$collectionAgent, "ACT", $activation_amount, $connection_id,false,$receiptNo,$invoice_no);
                                $result = $this->rechargeConsumer($sender,$collectionAgent, "CHG", $collection_amount, $connection_id,false,$receiptNo,$invoice_no);
								
                                /*  if($is_postPaid!=0){
                                	$meterReading = new Application_Model_MeterReadings();
                                	$meterReading->__set("consumer_id",$consumer_id);
                                	$meterReading->__set("meter_reading",$meter_start_reading);
                                	$meterReading->__set("timestamp",$activation_date);
                                	$meterReadingsMapper->addNewMeterReading($meterReading);
                                } */
                                if($package_id==5)
                                {
                                    $sms = "Customer ".$consumer_name." s/o ".$consumer_father_name.". Mobile No. ".$consumer_code.".  Connection ID ".$connection_id.". Metered Connection. Total OTP Due ".$activation_charge.". OTP In ".$activation_amount.". Advance Amt. ".$collection_amount.". Meter Start Reading ".$meter_start_reading.". Rate Rs.".$micro_price." per unit.";
                                    //$this->_smsNotification($number, $sms);
                                }
                                else
                                {
                                    if($duplicate)
                                    {
                                        $result = "Customer ".$consumer_name." s/o ".$consumer_father_name.". Mobile No. ".$consumer_code.". Temp. Connection ID ".$connection_id.". Package ".$package_name.". Total OTP ".$activation_charge.". OTP In ".$activation_amount.". Advance Amt. ".$collection_amount.".";
                                    }
                                    else
                                    {
                                        $result = "Customer ".$consumer_name." s/o ".$consumer_father_name.". Mobile No. ".$consumer_code.". Connection ID ".$connection_id.". Package ".$package_name.". Total OTP ".$activation_charge.". OTP In ".$activation_amount.". Advance Amt. ".$collection_amount.".";
                                    }
                                }
                            }
							 $data=new stdClass();
                        }else{
    						$result="Error. Please enter correct parameter.";
    						 
    						} 
                        }
 			break;
			case "CHGPKG":
                		$package_id = $what[3];
                		$old_package='NULL';$new_package='NULL';$live_from='NULL';
                		$result=$this->changePackage($collectionAgent, $package_id, $customer_code);
				$consumerMapper=new Application_Model_ConsumersMapper();
                		$consumers = $consumerMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                		if($consumers){
                			$consumer = $consumers[0];
                			$consumer_name=$consumer->__get("consumer_name");
                			$consumer_father_name=$consumer->__get("consumer_father_name");
                			 
                		}
                		if (strpos($result, 'Package change request received') !== false)
                		{
                			$success=1;
                			$oldpack = explode("Old Package", $result);
                			$oldpackage = explode(".", $oldpack[1]);
                			$old_package = $oldpackage[0];
                			
                			$newpack = explode("New Package", $result);
                			$newpackage = explode("will", $newpack[1]);
                			$new_package = $newpackage[0];
                			
                			$live = explode("live from", $result);
                			$livedate = explode(".", $live[1]);
                			$live_from = $livedate[0];
                		$data=array(
						"consumerName"=>$consumer_name,
                				"fatherName"=>$consumer_father_name,
                				"connectionID"=>$customer_code,
                				"oldPackage"=>$old_package,
                				"newPackage"=>$new_package,
                				"liveDate"=>$live_from
                			);
                		}
                		else{
                			$data=new stdClass();
                		}
                		
                	break;	
                	
                	case "CHKPKG":
                		$result=$this->checkPackage($collectionAgent,$customer_code);
                		$packageName='NULL';$packagesID='NULL';
				$consumerMapper=new Application_Model_ConsumersMapper();
                		$consumers = $consumerMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                		if($consumers){
                			$consumer = $consumers[0];
                			$consumer_name=$consumer->__get("consumer_name");
                			$consumer_father_name=$consumer->__get("consumer_father_name");
                			 
                		}
                		if (strpos($result, 'Package Name') !== false)
                		{
                			$success=1;
                			$pack_name = explode("Package Name", $result);
                			$package_name = explode("aur", $pack_name[1]);
                			$packageName = $package_name[0];
                			 
                			$package_id = explode("Package Id", $result);
                			$packageId = explode("hai", $package_id[1]);
                			$packagesID = $packageId[0];
                		
							$data=array(
						"consumerName"=>$consumer_name,
                				"fatherName"=>$consumer_father_name,
                				"connectionID"=>$customer_code,
                				"packageName"=>$packageName,
						"PackageID"=>$packagesID
							);
                		}
                		else{
                			$data=new stdClass();
                		}
                		break; 
					
			case "CHGOTP":
				$amount = abs($what[3]);
                        	$result = $this->updateConsumerAct($amount, $customer_code);
				$consumerMapper=new Application_Model_ConsumersMapper();
                        	$consumers = $consumerMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                        	if($consumers){
                        		$consumer = $consumers[0];
                        		$consumer_name=$consumer->__get("consumer_name");
                        		$consumer_father_name=$consumer->__get("consumer_father_name");
                        		 
                        	}
				if (strpos($result, 'Activation Charge') !== false)
                        	{
                        		$success=1;
                        	
                        		$data=array(
							"consumerName"=>$consumer_name,
                        				"fatherName"=>$consumer_father_name,
                        				"connectionID"=>$customer_code,
                        				"ActivationCharge"=>$amount
                        				
                        		);
                        	}
                        	else{
                        		$data=array();
                        	}

                        	break;
			
				case "AGENTLOGIN":
                        		$agent_email =$what[2];
                        		$password=$what[3];
								$sender="mobile";
								
								$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
							 	$collectionAgent=$collectionAgentsMapper->loginAgent(sha1(strtoupper($password)),$agent_email);
								
								if($collectionAgent["collection_agent_id"] !=null || $collectionAgent["collection_agent_id"] !="")
								{
									$success=1;
									$permissions=explode(',', $collectionAgent["permission"]) ;
									$permission=array('enrol'=>in_array("1", $permissions)?1:0,
											'pkg'=>in_array("2", $permissions)?1:0,
											'read'=>in_array("3", $permissions)?1:0,
											'txn'=>in_array("4", $permissions)?1:0,
											'otp'=>in_array("5", $permissions)?1:0,
											'info'=>in_array("6", $permissions)?1:0,
											'mm'=>in_array("7", $permissions)?1:0,
											'mppts'=>in_array("8", $permissions)?1:0,
											'plantstat'=>in_array("9", $permissions)?1:0,);	 

											$data=array(
									"agent_id" => $collectionAgent["collection_agent_id"],
					            	 "mobile" =>$collectionAgent["agent_mobile"],
					            	 "permission"=>$permission
									 );
					            	$result = "Login Successfully";
								}
								else{
									$result="Incorrect Username or Password";
								}
                        		break;
                        	case "GETOTP":
                        			$consumersMapper = new Application_Model_ConsumersMapper();
                        			$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                        			if ($consumers) {
                        				$consumer = $consumers[0];
                        				$consumer_id=$consumer->__get("consumer_id");
                        				$consumer_name=$consumer->__get("consumer_name");
							$consumer_father_name=$consumer->__get("consumer_father_name");
                        				$consumer_act_charge=$consumer->__get("consumer_act_charge");
                        				$success=1;
                        				$result= "Customer ID ".$customer_code." ka Activation Charge ".$consumer_act_charge." hai.";
                        				$data=array(
									"consumerName"=>$consumer_name,
                        						"fatherName"=>$consumer_father_name,
                        						"connectionID"=>$customer_code,
                        						"ActivationCharge"=>$consumer_act_charge
                        				
                        				);
                        			}else {
                        				$data=array();
							    	$result = "Error. Customer ID ".$customer_code." not found. Please check.";
							    	}
                        			 
                        			break;
			 case "RECEIPTCOUNT":
                        	
                        	     $transaction_ids =explode(":", $what[2]);
                        		 $receipt_count =explode(":", $what[3]);
                        		/*if(count($what)==5)
                        		{
                        			if($what["4"] =="mobile")
                        				$sender=$what["4"];
                        		}*/
                        		$cashRegisterMapper= new Application_Model_CashRegisterMapper();
                        		date_default_timezone_set('Asia/Kolkata');
								 
								if(count($transaction_ids) == count($receipt_count))
								{
								 
									if($cashRegisterMapper->updateReceiptCountByTransactionId( $transaction_ids ,$receipt_count )){
                        			
										$success=1;
										$timestamp=array();
										for($i=0;$i< count($transaction_ids);$i++){
											 $cashEntry=$cashRegisterMapper->getCr_pendingByTxnId($transaction_ids[$i]);
											 $timestamp[]=date("d/m/Y g:i a", strtotime($cashEntry["timestamp"]));
										}
										
										$data=array(
												"transaction_id" => implode(",", $transaction_ids),
												"receipt_count" => implode(",", $receipt_count),
												"timestamp"=>implode(",", $timestamp),
										);
										
										$arr=array(
												"success"=>$success,
												"data"=>$data
												
										);
										$result="Transaction Ids :". implode(',', $transaction_ids) ." receipt count updated successfully.";
									} else{
										$success=0;
										$result= "Transaction Ids :". implode(',', $transaction_ids) ."  receipt count Can not be updated.";
									}
								}
								else{
									$success=0;
                        			$result= "Transaction Ids and receipt counts number not matched.";
								}
                        		
                        		
                        	
                        break;
			
			case "INFO":
                        $total_reading=0;$total_amt=0;$unit_rate=0;
                        $consumersMapper=new Application_Model_ConsumersMapper();
                        $packagesMapper=new Application_Model_PackagesMapper();
                        $cashMapper=new Application_Model_CashRegisterMapper();
                         
                        $consumer_arr=array();
                        $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                        if($consumers){
                        	$success=1;
                        	$transaction_historys=array();
                        	$consumer=$consumers[0];
				$consumer_mobile=$consumer->__get("consumer_code");
                        	$package=$packagesMapper->getPackageById($consumer->__get("package_id"));
                        	$consumer_mobile=$consumer->__get("consumer_code");
                        	$type_of_me=$consumer->__get("type_of_me");
							$consumer_type_vals=$consumersMapper->getConsumerTypeById($type_of_me);
                        	$consumer_types=NULL;
                        	if($consumer_type_vals){
                        		 $consumer_types=$consumer_type_vals["consumer_type_name"];
                        	}
				$cashRe=$cashMapper->calculateActivationAmount('ACTIVATION',$consumer->__get("consumer_id"));
                        	$actiCharges=$cashRe;
				if($actiCharges==false){$actiCharges=0;} 
	
                        	$consumer_mobile=$consumer->__get("consumer_code");
                        	$outstanding=$consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
                        	$activation= $consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
                        	$total=$outstanding+$activation;
                        	$consumer_details=array(
                        			"consumer_name"=>$consumer->__get("consumer_name"),
                        			"father_name"=>$consumer->__get("consumer_father_name"),
                        			"mobile"=>$consumer->__get("consumer_code"),
                        			"act_date"=>$consumer->__get("consumer_act_date"),
                        			"outstanding"=>$total,
                        			"current_package"=> $package->__get("package_name"),
						"otp_due"=>$activation
                        	);
                        	$casRegisters=$cashMapper->getCashRegisterDetailsByConsumerId($consumer->__get("consumer_id"));
                        	 if($casRegisters){
				foreach ($casRegisters as $casRegister){
                                $Type=$casRegister->__get("cr_entry_type");
 
								$hindi_text=NULL;
 								$charges=$packagesMapper->getChargesByparameter($casRegister->__get("entry_status"));
 								if($charges){
 									$hindi_text=$charges["hindi_text"];
 								}
								 
	                        	$transaction_type = ($Type=='DEBIT') ? "#ed6b75" : (($Type=='CREDIT') ? "#36c6d3" : "#659be0");
                        		$transaction_history=array(
                        				"transaction_id"=> $casRegister->__get("transaction_id"),
                        				"date"=>date("d/m/Y g:i a",strtotime($casRegister->__get("timestamp"))),
                        				"amount"=> $casRegister->__get("cr_amount"),
										"transaction_type"=>$casRegister->__get("cr_entry_type"),
	                        			"transaction_type_color"=>$transaction_type,
                        				"is_print"=>1,
                        				"hindi_text"=>$hindi_text
                        		);
								 
                        		$transaction_historys[]=$transaction_history;
                        	}
				}else{
					$transaction_historys=array();
				}     
 			
                        	$meterVal=array();
                        	$meter=$meterReadingsMapper->getMTRReadingByConsumerId($consumer->__get("consumer_id"));
							if($meter){
								foreach ($meter as $meters){
									$meter_val=array(
											"reading"=>$meters["meter_reading"],
											"datetime"=>$meters["timestamp"]
									);
									$meterVal[]=$meter_val;
								}
                        	}else{
								$meterVal=array();
							}
                       	
                        $data=array('connectionID'=>$customer_code,
	                        		"reading_history"=>$meterVal,
	                        		"consumer_details"=>$consumer_details,
									"ActivationCharge"=>$actiCharges,
                        			"customerType"=> $consumer_types,
                        			"CustomerCode"=>$consumer_mobile,
	                        		"transaction_historys"=>$transaction_historys
                        		
                        );
						 
                     $result="Customer Details Found";
                       
                        
                        }
                        else{
 			$result="Customer Not Found";

                        	$data=array();
                        }
                        
                        break;
			case "CHGCCODE" :   
                     		$amount = abs($what[3]);
                     		$result = $this->updateConsumerCode($amount, $customer_code);
                     		$consumerMapper=new Application_Model_ConsumersMapper();
                     		$consumers = $consumerMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                     		if($consumers){
                     			$consumer = $consumers[0];
                     			$consumer_name=$consumer->__get("consumer_name");
                     			$consumer_father_name=$consumer->__get("consumer_father_name");
                     			 
                     		}
                     		if (strpos($result, 'consumer_code') !== false)
                     		{
                     			$success=1;
                     			 
                     			$data=array(
                     					"consumerName"=>$consumer_name,
                     					"fatherName"=>$consumer_father_name,
                     					"connectionID"=>$customer_code,
                     					"consumerCode"=>$amount
                     		
                     			);
                     		}
                     		else{
                     			$data=array();
                     		} 
				break;

				case "CHGCTYPE" :  
                     		$amount = $what[3];
                     		$result = $this->updateConsumerTypeOfConsumer($amount, $customer_code);
                     		$consumerMapper=new Application_Model_ConsumersMapper();
                     		$consumers = $consumerMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                     		if($consumers){
                     			$consumer = $consumers[0];
                     			$consumer_name=$consumer->__get("consumer_name");
                     			$consumer_father_name=$consumer->__get("consumer_father_name");
                     			 
                     		}
                     		if (strpos($result, 'consumer Type') !== false)
                     		{
                     			$success=1;
                     			 
                     			$data=array(
                     					"consumerName"=>$consumer_name,
                     					"fatherName"=>$consumer_father_name,
                     					"connectionID"=>$customer_code,
                     					"consumerType"=>$amount
                     					 
                     			);
                     		}
                     		else{
                     			$data=array();
                     		}
                     		break;
					/*case "MINI" :
                  			//BH-NGNJ;12;16/11/08;17:07;240.2V;12.86A;0.98;50.0HZ;019280.9KWH;949560.9KVAH
                  			//04/07/16;11:42;MTR3;231.9V;00.00A;000001.7KWH;000001.7KVAH|
                   			$message = $what[2];
                   			
                   			$value_array=explode(";", $message);
                   			if(count($value_array)==7){
                   			//$site_code = preg_replace('/-/','',$value_array[0]);
                   			//$meter_id=$value_array[1];
                   			$timestamps=$value_array[0]." ".$value_array[1];
                   			$zendDate = new Zend_Date($timestamps,"YY/MM/dd HH:mm");
                   		    $timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
                   		    $meter_keyword=$value_array[2];
                   			$ins_vol=$value_array[3];
                   			$ins_cur=$value_array[4];
                   			$pow_fac="";
                   			$ins_freq="";
                   			$act_eng=$value_array[5];
                   			$app_eng=$value_array[6];
                   			
                   			$siteMasterMappers =new Application_Model_SiteMasterMeterMapper();
                   			$siteMasterReadingMapper=new Application_Model_SiteMeterReadingMapper();
                   			$siteMaster=new Application_Model_SiteMeterReading();
                   			$checkMeter=$siteMasterMappers->getMasterMeterByMeterKey($meter_keyword);
                   			
                   			if($checkMeter){
	                   			$site_id=$checkMeter["site_id"];
	                   			$meter_id=$checkMeter["id"];
	                   			$siteMaster->__set("site_id",$site_id);
	                   			$siteMaster->__set("meter_id",$meter_id);
	                   			$siteMaster->__set("reading_date",$timestamp);
	                   			$siteMaster->__set("reading",preg_replace('/KWH/','',$act_eng));
	                   			
	                   			$Site_MM=$siteMasterReadingMapper->addNewSiteMeterReading($siteMaster);
	                   			
	                   			$MeterDataMapper=new Application_Model_MeterDataMapper();
	                   			$meterData=new Application_Model_MeterData();
	                   			$meterData->__set("site_id",$site_id);
	                   			$meterData->__set("meter_id",$meter_id);
	                   			$meterData->__set("ins_vol",$ins_vol);
	                   			$meterData->__set("ins_cur",$ins_cur);
	                   			$meterData->__set("pow_fac",$pow_fac);
	                   			$meterData->__set("ins_freq",$ins_freq);
	                   			$meterData->__set("act_eng",$act_eng);
	                   			$meterData->__set("app_eng",$app_eng);
	                   			 
	                   			$MeterDataValue=$MeterDataMapper->addNewSiteMaster($meterData);
	                   			if($Site_MM){
		                   			$success=1;
		                   			$data=array(
		                   					"Message"=>$message,
		                   			);
		                   			$result = "Data has been inserted into database.";
	                   			}else{
	                   				$success=0;
	                   				$data=array();
	                   				$result = "Error. Kripya kuch der baad try karein.";
	                   				
	                   			}
	                   			}else{
	                   				$success=0;
	                   				$data=array();
	                   				$result = "Invalid Meter.";
	                   			}
                   			}else{
                   				$success=0;
                   				$data=array();
                   				$result = "Invalid request.";
                   			}
                     		break;*/
							
							
				/*	 case "MINI" :
                  			//BH-NGNJ;12;16/11/08;17:07;240.2V;12.86A;0.98;50.0HZ;019280.9KWH;949560.9KVAH
                  			//04/07/16;11:42;MTR3;231.9V;00.00A;000001.7KWH;000001.7KVAH|
                  			$number = $who;
                   			$message = $what[2];
                   			$deviceMapper=new Application_Model_DeviceMapper();
                   			$siteMapper=new Application_Model_SitesMapper();
                   			$devices=$deviceMapper->getDeviceBySimNo($number);
                   			if($devices){
                   			$value_array=explode(";", $message);
                   			if(count($value_array)==7){
                   			//$site_code = preg_replace('/-/','',$value_array[0]);
                   			//$meter_id=$value_array[1];
                   			$timestamps=$value_array[0]." ".$value_array[1];
                   			$zendDate = new Zend_Date($timestamps,"YY/MM/dd HH:mm");
                   		    $timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
                   		    
                   		    $site_id=$devices->__get("site_id");
                   		    
                   		    $sites=$siteMapper->getSiteById($site_id);
                   		    $meter_keyword=strtoupper($sites->__get("site_code").$value_array[2]);
                   		   
                   			$ins_vol=$value_array[3];
                   			$ins_cur=$value_array[4];
                   			$pow_fac="";
                   			$ins_freq="";
                   			$act_eng=$value_array[5];
                   			$app_eng=$value_array[6];
                   			
                   			$siteMasterMappers =new Application_Model_SiteMasterMeterMapper();
                   			$siteMasterReadingMapper=new Application_Model_SiteMeterReadingMapper();
                   			$siteMaster=new Application_Model_SiteMeterReading();
                   			$checkMeter=$siteMasterMappers->getMasterMeterByMeterKey($meter_keyword);
                   			
                   			if($checkMeter){
	                   			$site_id=$checkMeter["site_id"];
	                   			$meter_id=$checkMeter["id"];
	                   			$siteMaster->__set("site_id",$site_id);
	                   			$siteMaster->__set("meter_id",$meter_id);
	                   			$siteMaster->__set("reading_date",$timestamp);
	                   			$siteMaster->__set("reading",preg_replace('/KWH/','',$act_eng));
	                   			
	                   			$Site_MM=$siteMasterReadingMapper->addNewSiteMeterReading($siteMaster);
	                   			
	                   			$MeterDataMapper=new Application_Model_MeterDataMapper();
	                   			$meterData=new Application_Model_MeterData();
	                   			$meterData->__set("site_id",$site_id);
	                   			$meterData->__set("meter_id",$meter_id);
	                   			$meterData->__set("ins_vol",$ins_vol);
	                   			$meterData->__set("ins_cur",$ins_cur);
	                   			$meterData->__set("pow_fac",$pow_fac);
	                   			$meterData->__set("ins_freq",$ins_freq);
	                   			$meterData->__set("act_eng",$act_eng);
	                   			$meterData->__set("app_eng",$app_eng);
	                   			 
	                   			$MeterDataValue=$MeterDataMapper->addNewSiteMaster($meterData);
	                   			if($Site_MM){
		                   			$success=1;
		                   			$data=array(
		                   					"Message"=>$message,
		                   			);
		                   			$result = "Data has been inserted into database.";
	                   			}else{
	                   				$success=0;
	                   				$data=array();
	                   				$result = "Error. Kripya kuch der baad try karein.";
	                   				
	                   			}
	                   			}else{
	                   				$success=0;
	                   				$data=array();
	                   				$result = "Invalid Meter.";
	                   			}
                   			}else{
                   				$success=0;
                   				$data=array();
                   				$result = "Invalid request.";
                   			}
                   			}else{
                   				$success=0;
                   				$data=array();
                   				$result = "Invalid request.";
                   			}
                     		break;*/
							case "EED":
                     			date_default_timezone_set('Asia/Kolkata');
                     			$amount = abs($what[3]);
                     			$checkData=0;$act_transaction=0;$consumer_name='';$consumer_father_name='';
                     			$result = $this->rechargeConsumer($sender,$collectionAgent, $type, $amount, $customer_code);
                     			$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                     			$consumer = $consumers[0];
                     			if (strpos($result, 'Connection ID') !== false)
                     			{
                     				$success=1;$checkData=1;
                     				$allData = explode(" ", $result);
                     				$chg_tran = explode(".", $allData[5]);
                     				$chg_transaction=$chg_tran[0];
                     				$consumer_father_name= $consumer->__get("consumer_father_name");
                     				$consumer_name=  $consumer->__get("consumer_name");
                     			}
                     			if($checkData==1){
                     				$data=array('connectionID'=>$customer_code,
                     						'amount'=> $amount,
                     						'transactionID'=>$chg_transaction,
                     						'consumerName'=>$consumer_name,
                     						'consumerFatherName'=>$consumer_father_name,
                     						'timestamp'=>date("d/m/Y g:i a"),
                     				);
                     			}else{
                     				$data=new stdClass();
                     			}
                     			break;
                     			
                     			case "MED":
                     				date_default_timezone_set('Asia/Kolkata');
                     				$amount = abs($what[3]);
                     				$checkData=0;$act_transaction=0;$consumer_name='';$consumer_father_name='';
                     				$result = $this->rechargeConsumer($sender,$collectionAgent, $type, $amount, $customer_code);
                     				$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                     				$consumer = $consumers[0];
                     				if (strpos($result, 'Connection ID') !== false)
                     				{
                     					$success=1;$checkData=1;
                     					$allData = explode(" ", $result);
                     					$chg_tran = explode(".", $allData[5]);
                     					$chg_transaction=$chg_tran[0];
                     					$consumer_father_name= $consumer->__get("consumer_father_name");
                     					$consumer_name=  $consumer->__get("consumer_name");
                     				}
                     				if($checkData==1){
                     					$data=array('connectionID'=>$customer_code,
                     							'amount'=> $amount,
                     							'transactionID'=>$chg_transaction,
                     							'consumerName'=>$consumer_name,
                     							'consumerFatherName'=>$consumer_father_name,
                     							'timestamp'=>date("d/m/Y g:i a"),
                     					);
                     				}else{
                     					$data=new stdClass();
                     				}
                     				break;
                     				
                     				case "OTHERS":
                     					date_default_timezone_set('Asia/Kolkata');
                     					$amount = abs($what[3]);
                     					$checkData=0;$act_transaction=0;$consumer_name='';$consumer_father_name='';
                     					$result = $this->rechargeConsumer($sender,$collectionAgent, $type, $amount, $customer_code);
                     					$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                     					$consumer = $consumers[0];
                     					if (strpos($result, 'Connection ID') !== false)
                     					{
                     						$success=1;$checkData=1;
                     						$allData = explode(" ", $result);
                     						$chg_tran = explode(".", $allData[5]);
                     						$chg_transaction=$chg_tran[0];
                     						$consumer_father_name= $consumer->__get("consumer_father_name");
                     						$consumer_name=  $consumer->__get("consumer_name");
                     					}
                     					if($checkData==1){
                     						$data=array('connectionID'=>$customer_code,
                     								'amount'=> $amount,
                     								'transactionID'=>$chg_transaction,
                     								'consumerName'=>$consumer_name,
                     								'consumerFatherName'=>$consumer_father_name,
                     								'timestamp'=>date("d/m/Y g:i a"),
                     						);
                     					}else{
                     						$data=new stdClass();
                     					}
                     					break;
									
									case "WATER":
                     						date_default_timezone_set('Asia/Kolkata');
                     						$amount = abs($what[3]);
                     						$checkData=0;$act_transaction=0;$consumer_name='';$consumer_father_name='';
                     						$result = $this->rechargeConsumer($sender,$collectionAgent, $type, $amount, $customer_code);
                     						$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                     						$consumer = $consumers[0];
                     						if (strpos($result, 'Connection ID') !== false)
                     						{
                     							$success=1;$checkData=1;
                     							$allData = explode(" ", $result);
                     							$chg_tran = explode(".", $allData[5]);
                     							$chg_transaction=$chg_tran[0];
                     							$consumer_father_name= $consumer->__get("consumer_father_name");
                     							$consumer_name=  $consumer->__get("consumer_name");
                     						}
                     						if($checkData==1){
                     							$data=array('connectionID'=>$customer_code,
                     									'amount'=> $amount,
                     									'transactionID'=>$chg_transaction,
                     									'consumerName'=>$consumer_name,
                     									'consumerFatherName'=>$consumer_father_name,
                     									'timestamp'=>date("d/m/Y g:i a"),
                     							);
                     						}else{
                     							$data=new stdClass();
                     						}
                     						break;
											
									case "CHARGES":
                     							date_default_timezone_set('Asia/Kolkata');
                     							
                     							$activation=$request->getParam("activation"); 
										        $monthly=$request->getParam("monthly");
										        $eed=$request->getParam("eed");
										        $med=$request->getParam("med");
										        $others=$request->getParam("others");
										        $water=$request->getParam("water");
												$SD=$request->getParam("security");
										          
										        $tran_type=array("ACT","CHG","EED","MED","OTHERS","WATER",'SD'); 
										        $tran_amt=array($activation,$monthly,$eed,$med,$others,$water,$SD);
										         
                     							$checkData=0;
                     							$act_transaction=0;$chg_transaction=0;$consumer_name='';$consumer_father_name='';
                     							$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
                     							$consumer = $consumers[0];
                     							$allDetails_tran=array();
                     							$charges=array();
												$cashRegisterMapper=new Application_Model_CashRegisterMapper();
												$sitesMapper = new Application_Model_SitesMapper();
												
												$site = $sitesMapper->getSiteById($consumer->__get("site_id"));
												$receipt_no=$cashRegisterMapper->getCashRegisterTransactionByReceiptNo($consumer->__get("consumer_id"),NULL,$collectionAgent->__get("collection_agent_id"),true,'CREDIT');
												$invoice_no=$cashRegisterMapper->getCashRegisterTransactionByReceiptNo($consumer->__get("consumer_id"),NULL,$collectionAgent->__get("collection_agent_id"),true,'DEBIT',$site->__get("site_code"));
                     							
                     							$receiptNo=intval($receipt_no[0]["receipt_number"])+1;
												$invoice_no=intval($invoice_no[0]["receipt_number"])+1; 
											//	echo $receiptNo." ".$invoice_no;exit;
												for($i=0;$i<count($tran_type);$i++){
                     								if($tran_amt[$i]!=NULL && $tran_amt[$i]!="" && $tran_amt!='undefined'){
                     									
		                     							$result = $this->rechargeConsumer($sender,$collectionAgent, $tran_type[$i], $tran_amt[$i], $customer_code,null,$receiptNo,$invoice_no);
		                     							
		                     							if (strpos($result, 'Connection ID') !== false)
		                     							{
		                     								$success=1;$checkData=1;
		                     								$allData = explode(" ", $result);
		                     								$act_tran = explode(".", $allData[5]);
		                     								$act_transaction=$act_tran[0];
		                     								
		                     								$details_tran=array(
		                     										$tran_type[$i]."Amount"=>$tran_amt[$i],
		                     										$tran_type[$i]."TransactionID"=>$act_transaction
		                     										);
		                     								 
		                     								array_push($allDetails_tran,$details_tran);
		                     								
		                     								$charges_data=$packagesMapper->getChargesByparameter($tran_type[$i]);
		                     								$charges_hindi=NULL;
		                     								if($charges_data){
		                     									$charges_hindi=$charges_data["hindi_text"];
		                     								}
		                     								$data_charges=array(
		                     										"hindi_text"=>$charges_hindi,
		                     										"amount"=>$tran_amt[$i],
		                     										"transaction"=>$act_transaction,
		                     								);
		                     								$charges[]=$data_charges;
		                     								
		                     							}
                     								}
                     							}
												if(count($allDetails_tran)==7){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2],$allDetails_tran[3],$allDetails_tran[4],$allDetails_tran[5],$allDetails_tran[6]);
                     							}
                     							elseif(count($allDetails_tran)==6){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2],$allDetails_tran[3],$allDetails_tran[4],$allDetails_tran[5]);
                     							}elseif (count($allDetails_tran)==5){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2],$allDetails_tran[3],$allDetails_tran[4]);
                     							}elseif (count($allDetails_tran)==4){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2],$allDetails_tran[3]);
                     							}elseif (count($allDetails_tran)==3){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1],$allDetails_tran[2]);
                     							}elseif (count($allDetails_tran)==2){
                     								$allDetails_tran=array_merge($allDetails_tran[0],$allDetails_tran[1]);
                     							}else{
													$allDetails_tran=$allDetails_tran[0]; 
												}
                     							
                     							if($checkData==1){
                     								$charge=array(
                     										"charges"=>$charges
                     								);
                     								$resultdata=array('connectionID'=>$customer_code,
                     										'consumerName'=> $consumer->__get("consumer_name"),
                     										'consumerFatherName'=>$consumer->__get("consumer_father_name"),
                     										'timestamp'=>date("d/m/Y g:i a"),
                     								);
                     								$data=array_merge($resultdata,$allDetails_tran,$charge);
                     								$result="Transactions have been inserted successfully.";
                     							}else{
                     								$data=new stdClass();
                     							}
                     						
                     							break;
												
								case "BANaaNEDs":
	                     			$consumer_connection_id = $what[2];
	                     			$consumerPackageMapper = new Application_Model_ConsumersPackageMapper();
	                     			$consumerMapper=new Application_Model_ConsumersMapper();
	                     			$meterReadingMapper=new Application_Model_MeterReadingsMapper();
	                     			
	                     			
	                     			$consumers = $consumerMapper->getConsumerByConnectionID($consumer_connection_id);
                     				if($consumers){
                     					$consumer_id=$consumers->__get("consumer_id");
                     					$consumer_status=$consumers->__get("consumer_status");
                     					$checkPackage=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
                     					if($checkPackage){
                     						$temp=0;
                     						 
                     					foreach ($checkPackage as $checkPackages){
                     						if($checkPackages["is_postpaid"]!=0){
                     							$temp=1;
                     						}
                     					}
                     					
                     					
                     					if($consumer_status=='banned'){
                     						$result="Customer ID ".$consumer_connection_id." is alredy BANNED.";
                     					}elseif($temp==0){
                     						
                     						$status='banned';
                     						$date = new Zend_Date();
                     						$date->setTimezone("Asia/Calcutta");
                     						$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
                     						
                     						foreach ($checkPackage as $checkPackages){
                     							$date = new Zend_Date();
									    		$date->setTimezone("Asia/Kolkata");
									    		$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
									    		$current_date = date_parse_from_format("Y-m-d", $curr_date);
									    		$day = $current_date["day"];
									    		$month = sprintf("%02d", $current_date["month"]);
									    		$year = $current_date["year"];
									    		$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
									    		 
									    		$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
									    		$Act_day = $Act_date["day"];
									    		$Act_month = sprintf("%02d", $Act_date["month"]);
									    		$Act_year = $Act_date["year"];
									    		 
									    		$lastDebit=$cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$checkPackages["package_id"]);
									    		if($month==$Act_month && $year==$Act_year){
									    			$lastAmount=($lastDebit/$total_days_in_month)*($day-$Act_day);
									    		}else{
									    			$lastAmount=($lastDebit/$total_days_in_month)*$day;
									    		}
									    		 
									    		$newAmount=($package_cost/$total_days_in_month)*($total_days_in_month-$day);
									    		$package_val=($newAmount+$lastAmount)-$lastDebit;
									    		 
									    		$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
									    		$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
									    		$transaction_id = $timestamp . "-" . $cr_amount;
									    		
									    		$cashRegister = new Application_Model_CashRegister();
									    		$cashRegister->__set("user_id", 0);
									    		$cashRegister->__set("consumer_id", $consumer_id);
									    		$cashRegister->__set("cr_entry_type", "DEBIT");
									    		$cashRegister->__set("cr_amount", intval($package_val));
									    		$cashRegister->__set("receipt_number", 0);
									    		$cashRegister->__set("transaction_id", $transaction_id);
									    		$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
									    		$cashRegister->__set("entry_status", "CHG");
									    		$cashRegister->__set("remark", "");
									    		$cashRegister->__set("package_id", $current_package);
									    		$cashRegisterMapper->addNewCashRegister($cashRegister);
                     						}
                     						
                     						$consumerStatus = $consumerMapper->updateStatus($consumer_connection_id,$status,$timestamp);
                     						 
	                     						if($consumerStatus){
	                     							$result="Consumer Id ".$consumer_connection_id." ko banned kar diya gaya hai.";
	                     						}else{
	                     							$result="Error. Kripya kuch der baad try karein.";
	                     						}
	                     					}elseif($temp==1 && (count($what)==3 || count($what)==4)){
	                     							$result = "Error. Please enter package id and meter reading for consumer.";
	                     					}elseif($temp==1 && (count($what)==5)){
	                     							$package_id = $what[3];
	                     							$reading_val = $what[4];
	                     							$status='banned';
	                     							$checkPackage=$consumerPackageMapper->getPackageByConsumerId($consumer_id,$package_id);
	                     							if($checkPackage){
	                     							$checkReadings=$meterReadingMapper->getLatestReadingByPackageConsumerId($consumer_id,$package_id);
	                     							if($checkReadings){
	                     								if($reading_val>$checkReadings->__get("reading")){
	                     									$reading=$reading_val-$checkReadings->__get("reading");
	                     								}else{
	                     									$result = "Error. Previous Reading for ".$consumer_connection_id." was ".$checkReadings->__get("reading").". New reading must be bigger. Please check and try again.";
	                     									 break;
	                     								}
	                     							}else{
	                     								$reading=$reading_val;
	                     							}
                     								$date = new Zend_Date();
                     								$date->setTimezone("Asia/Kolkata");
                     								$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
                     								$current_date = date_parse_from_format("Y-m-d", $curr_date);
                     								$day = $current_date["day"];
                     								$month = sprintf("%02d", $current_date["month"]);
                     								$year = $current_date["year"];
                     								$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
                     							
                     								$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
                     								$Act_day = $Act_date["day"];
                     								$Act_month = sprintf("%02d", $Act_date["month"]);
                     								$Act_year = $Act_date["year"];
                     							
                     								$lastDebit=$cashRegisterMapper->getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$package_id);
                     								if($month==$Act_month && $year==$Act_year){
                     									$lastAmount=($lastDebit/$total_days_in_month)*($day-$Act_day);
                     								}else{
                     									$lastAmount=($lastDebit/$total_days_in_month)*$day;
                     								}
                     								$packages=$packagesMapper->getPackageById($package_id);
			                     					$unit=$packages->__get("unit");
									    			$extra_charges=$packages->__get("extra_charges");
									    			$package_cost=$packages->__get("package_cost");
									    			$usedUnit=($unit/$total_days_in_month)*($total_days_in_month-$day);
									    			if($reading>$usedUnit){
									    				$newAmount=($package_cost/$total_days_in_month)*($total_days_in_month-$day);
									    				$unit_amt=$newAmount+(($reading-$usedUnit)*$extra_charges);
									    				$package_val=($unit_amt+$lastAmount)-$lastDebit;
									    			}else{
									    				$newAmount=($package_cost/$total_days_in_month)*($total_days_in_month-$day);
									    				$package_val=($newAmount+$lastAmount)-$lastDebit;
									    			}
                     									
                     								$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
                     								$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
                     								$transaction_id = $timestamp . "-" . $cr_amount;
                     								
                     								$cashRegister = new Application_Model_CashRegister();
                     								$cashRegister->__set("user_id", 0);
                     								$cashRegister->__set("consumer_id", $consumer_id);
                     								$cashRegister->__set("cr_entry_type", "DEBIT");
                     								$cashRegister->__set("cr_amount", intval($package_val));
                     								$cashRegister->__set("receipt_number", 0);
                     								$cashRegister->__set("transaction_id", $transaction_id);
                     								$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
                     								$cashRegister->__set("entry_status", "CHG");
                     								$cashRegister->__set("remark", "");
                     								$cashRegister->__set("package_id", $current_package);
                     								$cashRegisterMapper->addNewCashRegister($cashRegister);
                     							 
                     							 
                     							$consumerStatus = $consumerMapper->updateStatus($consumer_connection_id,$status,$timestamp);
	                     					}else{
                     						$result ="Error. Package ".$package_id." not found. Please check.";
                     					}
                     					}else{
                     						$result ="Error. Please enter correct data.";
                     					}
                     					
                     					}else{
                     						$result ="Error. Package not found for this Customer ID ".$consumer_connection_id.". Please check.";
                     					}
                     				}else{
                     					$result = "Error. Customer ID ".$consumer_connection_id." not found. Please check.";
                     				}
                     				 
                     			break;
                     		
                     	    case "REACTIVE":
                     								$consumer_connection_id = $what[2];
                     								 
                     								$consumerMapper=new Application_Model_ConsumersMapper();
                     								$consumers = $consumerMapper->getConsumerByConnectionID($consumer_connection_id);
                     								if($consumers){
                     									$consumer_status=$consumers->__get("consumer_status");
														$agent_id = $collectionAgent->__get("collection_agent_id");
														$agent_fname = $collectionAgent->__get("agent_fname");
                     									if($consumer_status=='active'){
                     										$result="Customer is alredy ACTIVE.";
                     									}else {
                     										$status=$consumer_status;
															$remark='Request for Re-active by '.$agent_fname;
															$create_type='(S)';
															$date = new Zend_Date();
															$date->setTimezone("Asia/Kolkata");
															$timestamp=$date->toString("yyyy-MM-dd HH:mm:ss");
															
                     										$consumerStatus = $consumerMapper->updateStatus($consumer_connection_id,$status,$timestamp,$remark,$agent_id,$create_type);
                     										if($consumerStatus){
                     											$result="Customer is Successfully REACTIVE.";
                     										}else{
                     											$result="Error. Kripya kuch der baad try karein.";
                     										}
                     									}
                     									 
                     								}else{
                     									$result = "Error. Customer ID ".$consumer_connection_id." not found. Please check.";
                     								}
                     							 
                     								break;
								case "BANNED": 
                     								$consumer_connection_id = $what[2];
													
                     								$consumerMapper=new Application_Model_ConsumersMapper();
                     								$consumers = $consumerMapper->getConsumerByConnectionID($consumer_connection_id);
                     								if($consumers){
                     									$consumer_status=$consumers->__get("consumer_status");
														$agent_id = $collectionAgent->__get("collection_agent_id");
														$agent_fname = $collectionAgent->__get("agent_fname");
                     									if($consumer_status=='banned'){
                     										$result="Customer is alredy BANNED.";
                     									}else {
                     										$status=$consumer_status;
															$remark='Request for BANNED by '.$agent_fname;
															$create_type='(S)';
															$date = new Zend_Date();
															$date->setTimezone("Asia/Kolkata");
															$timestamp=$date->toString("yyyy-MM-dd HH:mm:ss");
															 
                     										$consumerStatus = $consumerMapper->updateStatus($consumer_connection_id,$status,$timestamp,$remark,$agent_id,$create_type);
                     										if($consumerStatus){
                     											$result="Customer is Successfully Banned.";
                     										}else{
                     											$result="Error. Kripya kuch der baad try karein.";
                     										}
                     									}
                     								}else{
                     									$result = "Error. Customer ID ".$consumer_connection_id." not found. Please check.";
                     								}
                     							 
                     								break;	
                    default:
                        //$result = "Invalid request";
                        $result = "Error. Kripya kuch der baad try karein.";
                        break;

                    default:
                        //$result = "Invalid request";
                        $result = "Error. Kripya kuch der baad try karein.";
                        break;
                }
            } else {
                //$result = "Error, you are not a valid POS! Please call up your cluster manager to resolve this issue.";
                $result = "Error. You are not an Agent.";
            }
        }
	if($sender=="mobile")
		{
			/*if($type=="AGENTLOGIN")
			 {
				$input=array('username'=>$agent_email,
							'password'=>$password);
			 }else{
				$input=array('what'=>$what_string,
							 'who'=>$who);
			 }
			$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
			$arr = array(
		                 "input" => $input,
		                 "commandResult" => $commandResult,
		                );*/
						
		if($type=="AGENTLOGIN")
		 {
		 	$commandResult = array(
						 "success"=>$success,
						// "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "commandResult" => $commandResult,
		                );
		 }else{
		 	
			$input=array('what'=>$what_string,
						 'who'=>$who);
		 	$commandResult = array(
						 "success"=>$success,
						 "message" =>$result,
						 "data"=>$data
			              );	
		 	$arr = array(
		                 "input" => $input,
		                 "commandResult" => $commandResult,
		                );
		 }
		$json = json_encode($arr, JSON_PRETTY_PRINT);
		echo $json;
		//echo json_encode($arr);
		if($type=="AGENTLOGIN")
		 {
		 	//echo $this->_smsNotification($who, json_encode($arr));
		 }
   
        $file = 'people.txt';
        $current = file_get_contents($file);
        $current .= "IN(".$who."):".$what_string." ---- Out:".$result . " ---- Time - ".$zendDate->toString()."\r\n";
        file_put_contents($file, $current);
		exit;				
		}
       $text= NULL; //$this->_smsNotification($number, $result);
        $file = 'people.txt';
        $current = file_get_contents($file);
        $current .= "IN(".$who."):".$what_string." ---- Out:".$result . " ---- Time - ".$zendDate->toString().$text."\r\n";
        //echo $current;exit;
        file_put_contents($file, $current);
		
	/*$file = 'test.txt';
        $current = file_get_contents($file);
        $current .="".$result."".$text."-".$zendDate->toString() . "\r\n";
        file_put_contents($file, $current);*/
    }

    public function sendWarningSmsAction(){
        
        $connections_arr = array("AMANTEST","ARJUN","GCV","SID","KVL","VIC","PAT","ZEN","FNG","SDA","KRITI","KAN");
        
        $consumerMapper = new Application_Model_ConsumersMapper();
        foreach($connections_arr as $connection_id){
                $consumers = $consumerMapper->getConsumersByConnectionId($connection_id);
               
                foreach($consumers as $consumer){
                    print_r($consumer);
                    $number = $consumer->__get("consumer_code");
                    $sms = "Dear ".$consumer->__get("consumer_name").", your payment is overdue. Please pay your local TARAoorja agent immediately to enjoy uninterrupted services. ";
                    
                    $result = NULL; //$this->_smsNotification($number, $sms);
                   
                       echo $result;
                   
                }
        }
    }
    
	private function addMTR($collectionAgent, $start_reading, $end_reading, $customer_code,$sender) {
        $consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$enterprisePriceMappper = new Application_Model_EnterprisePriceMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
    	$packagesMapper = new Application_Model_PackagesMapper();
		$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
		$meterReading = new Application_Model_MeterReadings();
		$consumerPackageMapper=new Application_Model_ConsumersPackageMapper(); 
    	$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
    	if ($consumers) {
    		$consumer = $consumers[0];
    		$reading = $end_reading-$start_reading;
			$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
			$package_id=NULL;
			if($consumerPackages){	
				foreach($consumerPackages as $consumerPackage){
					if($consumerPackage["is_postpaid"]!=0){
						$package_id=$consumerPackage["package_id"];
					}
				}
			}
    		$consumer_id=$consumer->__get("consumer_id");
    		$consumer_number=$consumer->__get("consumer_code");
    		$unit_price = $consumer->__get("micro_entrprise_price");
			$previousOutstanding = $consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
    		$amount = $reading*$unit_price;
    		$micro_price=$unit_price;
    		$enterprise_type=$consumer->__get("enterprise_type");
    		
    		$package = $packagesMapper->getPackageById($package_id);
    		$is_postPaid =  $package->__get("is_postpaid");
			
    		/*if($package_id ==5 && $enterprise_type =="Scheme")
    		{
    			$unit_price="";
    			$tot_amount=0;
    			$consumerSchemes=$consumerSchemeMapper->getConsumerschemeByConsumerId($consumer_id);
    			$scheme_id=1;
    			if($consumerSchemes)
    			{
    				$scheme_id=$consumerSchemes->__get("consumer_scheme");
    			}
    			$schemeUnitPrices=$enterprisePriceMappper->getEnterprisePriceBySchemeId($scheme_id);
    			$cal_amount=0;$last_unit_price=0;
    			$cal_reading=$reading;
    			$amount=$tot_amount;
    			$unit_price_val = array();
				if($schemeUnitPrices){
					foreach ($schemeUnitPrices as $schemeUnitPrice) {
						$unit_from = $schemeUnitPrice->__get("unit_from");
						$unit_to = $schemeUnitPrice->__get("unit_to");
						$enterprise_price =$schemeUnitPrice->__get("enterprise_price");
		
						$unit_between=$unit_to-($unit_from-1);
						if($cal_reading>0){
							$unit_between=($cal_reading>=$unit_between)?$unit_between:$cal_reading;
							$cal_reading=$cal_reading-$unit_between;
							$cal_amount=$unit_between*$enterprise_price;
							$tot_amount=$tot_amount+$cal_amount;
							$last_unit_price=$enterprise_price;
							$unit_price.=($unit_price== "")?$unit_from."-".$unit_to." Rs.".$enterprise_price."/unit": " ".$unit_from."-".$unit_to." Rs.".$enterprise_price."/unit";
						}
						
					}
					if($cal_reading >0)
					{
						$cal_amount=$cal_reading*$last_unit_price;
						$tot_amount=$tot_amount+$cal_amount;
					}
				}
    		}*/
			
			if($is_postPaid==2){
				
				$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				$meterReading->__set("meter_reading",$end_reading);
				$meterReading->__set("consumer_id",$consumer->__get("consumer_id"));
				$meterReading->__set("timestamp",$timestamp);
				$meterReading->__set("package_id", $package_id); 
				$meterReading->__set("start_reading",0);
				$meterReading->__set("unit",$reading); 
				$meterReadingsMapper->addNewMeterReading($meterReading);
				
				$result = "Connection ID ".$consumer->__get("consumer_connection_id")." ki Old Meter Reading ".$start_reading." New Meter Reading ".$end_reading." hai.";
				 
				
			}elseif ($collectionAgent->__get("agent_balance") >= $amount && $is_postPaid==1) {
				 $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $recentCR = $cashRegisterMapper->getLastestEntryByConsumerId($consumer->__get("consumer_id"));
                $date = new Zend_Date();
                $date->setTimezone("Asia/Kolkata");
                
                $timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));
				$amount=round($amount);
                $tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
                $transaction_id = $timestamp . "-" . $tcr_amount;
				
                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $cashRegister = new Application_Model_CashRegister();
                $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
                $cashRegister->__set("cr_entry_type", "DEBIT");
                $cashRegister->__set("cr_amount", $amount);
				$cashRegister->__set("entry_status", "CHG");
                $cashRegister->__set("transaction_id", $transaction_id);
                $cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
                $cashRegister->__set("receipt_number", "CA-" . $collectionAgent->__get("collection_agent_id"));
				$cashRegister->__set("mtr_entry", '1');
				$cashRegister->__set("package_id", $package_id);  
				if($sender=="mobile"){
					$cashRegister->__set("transaction_type", "(M)" );
				}else{
					$cashRegister->__set("transaction_type", "(S)");
				}
                if ($cashRegisterMapper->addNewCashRegister($cashRegister)) {
                    $currentOutstanding = $consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
                    $caTransactionsMapper = new Application_Model_CaTransactionsMapper();
                    $caTransactions = new Application_Model_CaTransactions();
                    $caTransactions->__set("collection_agent_id", $collectionAgent->__get("collection_agent_id"));
                    $caTransactions->__set("transaction_type", "MINUS");
                    $caTransactions->__set("transaction_amount", $amount);
                    $caTransactions->__set("remarks", "Consumer recharge for Consumer Code : " . $consumer->__get("consumer_code"));
                    $ca_amount = $collectionAgent->__get("agent_balance");
                    $balance = $ca_amount - $amount;
                    $collectionAgent->__set("agent_balance", $balance);
                    $collectionAgentsMapper->updateCollectionAgent($collectionAgent);
                    if (true) { 
					    $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
                        $meterReading = new Application_Model_MeterReadings();
                        $date = new Zend_Date();
						$date->setTimezone("Asia/Calcutta");
						$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
                        $meterReading->__set("meter_reading",$end_reading);
                        $meterReading->__set("consumer_id",$consumer->__get("consumer_id"));
						$meterReading->__set("timestamp",$timestamp);
						$meterReading->__set("start_reading",0);
						$meterReading->__set("package_id", $package_id); 
						$meterReading->__set("unit",$reading);
                        $meterReadingsMapper->addNewMeterReading($meterReading);
                        //$result = "Connection ID ".$consumer->__get("consumer_connection_id")." charged successfully with Rs.".$amount.". Your balance is ".$balance.". Transaction ID ".$transaction_id.".";
                        //$result = "Connection ID ".$consumer->__get("consumer_connection_id")." - Rs.".$amount.". ".$transaction_id.".";
                        if($package_id==5  && $enterprise_type =="Scheme"){
						$result = "Connection ID ".$consumer->__get("consumer_connection_id")." ka bill Rs.".$amount." hai. Old Meter Reading ".$start_reading.". New Meter Reading ".$end_reading.". Total Unit ".$reading.". Rate for ".$unit_price." hai.";
                        }
						/*elseif($is_postPaid==2){
						$result = "Connection ID ".$consumer->__get("consumer_connection_id")." ka bill Rs.".$amount." hai. Old Meter Reading ".$start_reading.". New Meter Reading ".$end_reading.". Total Unit ".$reading.". Rate for ".$unit_price." hai.";
                       
						}*/
                        else{
 							$result = "Connection ID ".$consumer->__get("consumer_connection_id")." ka abhi ka bill amount Rs.".$amount." hai. Pichli Meter Reading ".$start_reading." . Abhi ki Meter Reading ".$end_reading." . Total Unit ".$reading." . Rate Rs".$unit_price."/unit. Customer ke pichley Rs.".$previousOutstanding." abhi bhi bakaya hain. Customer se total Rs.".$currentOutstanding." collect kariye.";
                        }
                        //$result = "Customer account has been recharged successfully. Transaction id " . $transaction_id . ". Your balance is Rs. " . $balance;
                        $name = $consumer->__get("consumer_name");
                        $sms = "Namastey ".$name."! Aapkey TARAurja Connection ".$consumer->__get("consumer_connection_id")." ke liye hamein Rs.".$amount." prapt ho gaye hain. ".$transaction_id.".";
                        $sms1 = "Namastey ".$name."! Aapkey TARAurja Connection ".$consumer->__get("consumer_connection_id")." ka bill Rs.".$amount." hai. Yeh bill pichli reading ".$start_reading." aur ab ki reading ".$end_reading." ke hisaab se ".$reading." unit ka bill hai. Aapki bijli ka daam Rs.".$unit_price."/unit hai.";
                        $sms2 = "Namastey ".$name."! Aapkey TARAurja Connection ".$consumer->__get("consumer_connection_id")." ka bill Rs.".$amount." hai. Yeh bill pichli reading ".$start_reading." aur ab ki reading ".$end_reading." ke hisaab se ".$reading." unit ka bill hai. Aapkey pichley bill ke anusaar, Rs.".$previousOutstanding." bakaaya hai. Kripya TARAurja Agent ko total Rs.".$currentOutstanding." ki payment karein.";
                        //$sms = "Dear " . $name . ", Rs." . $amount . " credited to your account " . $consumer->__get("consumer_connection_id") . ". Transaction ID " . $transaction_id . ".";
                        if(true)//$consumer->__get("sms_opt_in")==1)
                        {
                               // $this->_smsNotification($consumer->__get("consumer_code"), $sms2);
                        }
                        else
                        { 
                            //$result = "Consumer has opted out of SMS Facility";
                        }
                    } else {
                        $result = "Error. Not enough balance.";
                        //$result = "Error. Kripya kuch der baad try karein.";
                    }
                } else {
                    $result = "Error. Kripya kuch der baad try karein.";
                    //$result = "Error. Kripya kuch der baad try karein.";
                }
            } else {
            	//$result = "Error, not enough cash balance, please recharge your CA Account to continue.";
            	$result = "Error. Not enough balance.".$reading." ".$amount;
            }
        } else {
        	//$result = "Customer not found. Please check the Customer ID and try again.";
        	$result = "Error. Customer ID ".$customer_code." not found. Please check.";
           
        }
        return $result;
    }
   
    private function addFollowup($collectionAgent, $t_type, $amount, $customer_code,$sender){
        
		$consumersMapper = new Application_Model_ConsumersMapper();
        $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
        if ($consumers) {
            $consumer = $consumers[0];
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $date = new Zend_Date();
            $date->setTimezone("Asia/Kolkata");
            $timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));

            $tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
            $transaction_id = $timestamp . "-" . $tcr_amount;

            $cashRegister = new Application_Model_CashRegister();
            $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
            $cashRegister->__set("cr_entry_type", "FOLLOWUP");
            $cashRegister->__set("cr_amount", $amount);
            $cashRegister->__set("transaction_id", $transaction_id);
            $cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
            $cashRegister->__set("receipt_number", "CA-" . $collectionAgent->__get("collection_agent_id"));
            if($sender=="mobile"){
            	$cashRegister->__set("transaction_type", "(M)" );
            }else{
            	$cashRegister->__set("transaction_type", "(S)");
            }
            $id = $cashRegisterMapper->addNewCashRegister($cashRegister);
            echo $id;exit;
            if ($id) {
                $result = "Follow up added";
            } else {
                $result = "Technical Error, Could not add transaction. Please try again";
                //$result = "Error. Kripya kuch der baad try karein.";
            }
        } else {
            //$result = "Customer not found. Please check the Customer ID and try again.";
            $result = "Error. Customer ID ".$customer_code." not found. Please check.";
        }
        
        return $result;
    }
   
    private function rechargeConsumer($sender,$collectionAgent, $t_type, $amount, $customer_code,$send_sms=true,$receiptNo=NULL,$invoice_no=NULL) {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
		$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
        if ($collectionAgent->__get("agent_balance") >= $amount) {
            $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
            if ($consumers) {
                $consumer = $consumers[0];
                if ($t_type == "ACT") {
                    $transaction_type = "ACTIVATION";
                }
                elseif($t_type == "CNC"){
                    $transaction_type = "FOLLOWUP";
                }
                else {
                    $transaction_type = "CREDIT";
                }
                
                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $recentCR = $cashRegisterMapper->getLastestEntryByConsumerId($consumer->__get("consumer_id"));
                $date = new Zend_Date();
                $date->setTimezone("Asia/Kolkata");
                if(false)//$recentCR)
                {
                    if($recentCR->__get("cr_entry_type")==$transaction_type)
                    {
                        $entryDate = new Zend_Date($recentCR->__get("timestamp"),"yyyy-MM-dd HH:mm:ss");
                        $entryDate->addDay(1);
                        if($entryDate->isLater($date))
                        {
                            return "Error. Duplicate Entry. Please try again.";
                        }
                    }
                }
                
                $timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));

                $tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
                $transaction_id = $timestamp . "-" . $tcr_amount;
				$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
				
                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $cashRegister = new Application_Model_CashRegister();
                $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
                $cashRegister->__set("cr_entry_type", $transaction_type);
                $cashRegister->__set("cr_amount", $amount);
                $cashRegister->__set("transaction_id", $transaction_id);
                $cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
                $cashRegister->__set("receipt_number", intval($receiptNo));
				$cashRegister->__set("entry_status", $t_type); 
				$cashRegister->__set("package_id", $consumerPackages[0]["package_id"]); 
                if($sender=="mobile"){
                	$cashRegister->__set("transaction_type", "(M)");
                }else{
                	$cashRegister->__set("transaction_type", "(S)");
                }
                if ($cashRegisterMapper->addNewCashRegister($cashRegister)) {
					
					if($t_type=='OTHERS' || $t_type=='MED'){
						$date_val = new Zend_Date();
						$date_val->setTimezone("Asia/Kolkata");
						
						$cashRegister_val = new Application_Model_CashRegister();
						$cashRegister_val->__set("consumer_id", $consumer->__get("consumer_id"));
						$cashRegister_val->__set("cr_entry_type", 'DEBIT');
						$cashRegister_val->__set("cr_amount", $amount);
						$cashRegister_val->__set("transaction_id", $transaction_id);
						$cashRegister_val->__set("user_id", $collectionAgent->__get("collection_agent_id"));
						$cashRegister_val->__set("receipt_number", $invoice_no);
						$cashRegister_val->__set("entry_status", $t_type); 
						$cashRegister_val->__set("package_id", $consumerPackages[0]["package_id"]);
						$cashRegister_val->__set("timestamp", $date_val->toString("yyyy-MM-dd 04:05:30"));
						if($sender=="mobile"){
							$cashRegister_val->__set("transaction_type", "(M)");
						}else{
							$cashRegister_val->__set("transaction_type", "(S)");
						}
						$cashRegisterMapper->addNewCashRegister($cashRegister_val); 
					}
					
					if($consumer->__get("site_id")==62 || $consumer->__get("site_id")==31){
						$meterReadingMapper=new Application_Model_MeterReadingsMapper();
						$meters=$meterReadingMapper->getLatestReadingByConsumerId($consumer->__get("consumer_id"));
						$serial=$meters->__get("meter_no");
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$siteOutstaning=$cashRegisterMapper->getMonthlyDueBySiteId($consumer->__get("site_id"),null,null,null,$consumer->__get("consumer_id"));
						$outstanding=intval($siteOutstaning["debit_amount"])-intval($siteOutstaning["credit_amount"]);
						
						//$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode"; 
						if($consumer->__get("site_id")==62){
							$url="https://sparkcloud-tara-bheldih.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode"; 
						}else{
							$url="https://sparkcloud-tara-dumarsan.herokuapp.com/api/v0/meter/".$serial."/set-operating-mode";
						} 	
						if(intval($consumer->__get("credit_limit")<=intval($outstanding))){
								$activitys='off'; 
								$param="state=".$activitys."";
								if($consumer->__get("site_id")==62){ 
									$destination=array(
										'Authentication-Token: .eJwFwcsRwCAIBcBeOIcZlSdqLZkc-Ej_JWT3Jal9bVfjdk0ZFZMNGQy3Y64xZlx6aOZCbgkZhewCO7JQ6jCVqN7o-wEZRhSK.DcwoVA.GIS1jju7M0zPSMjcDrGOBOW7Rp8'
									);
								}else{
									$destination=array(
										'Authentication-Token: .eJwNx7kNwDAIAMBdqGOJ13FmiVIAhv1HiK-7F3bKY-w4fEoPRd4j8NRs3tKVUbngAuvgJC9qjS7KjakktoqVvLnh-wH_yhSf.DgLTZg.K0MSUEm8_CSi_aQQde7UGW8Ikx4'
									);
								} 
								$result=$this->CallCURL($url,'POST',$param,$destination);
									
								$data=(json_decode($result,true));
						}else{    
								$activitys='on'; 
								$param="state=".$activitys."";
								if($consumer->__get("site_id")==62){
									$destination=array(
										'Authentication-Token: .eJwFwcsRwCAIBcBeOIcZlSdqLZkc-Ej_JWT3Jal9bVfjdk0ZFZMNGQy3Y64xZlx6aOZCbgkZhewCO7JQ6jCVqN7o-wEZRhSK.DcwoVA.GIS1jju7M0zPSMjcDrGOBOW7Rp8'
									);
								}else{
									$destination=array(
										'Authentication-Token: .eJwNx7kNwDAIAMBdqGOJ13FmiVIAhv1HiK-7F3bKY-w4fEoPRd4j8NRs3tKVUbngAuvgJC9qjS7KjakktoqVvLnh-wH_yhSf.DgLTZg.K0MSUEm8_CSi_aQQde7UGW8Ikx4'
									);
								}
								$result=$this->CallCURL($url,'POST',$param,$destination);
									
								$data=(json_decode($result,true));
						}
					}
                    $caTransactionsMapper = new Application_Model_CaTransactionsMapper();
                    $caTransactions = new Application_Model_CaTransactions();
                    $caTransactions->__set("collection_agent_id", $collectionAgent->__get("collection_agent_id"));
                    $caTransactions->__set("transaction_type", "MINUS");
                    $caTransactions->__set("transaction_amount", $amount);
                    $caTransactions->__set("remarks", "Consumer recharge for Consumer Code : " . $consumer->__get("consumer_code"));
                    $ca_amount = $collectionAgent->__get("agent_balance");
                    $balance = $ca_amount - $amount;
                    $collectionAgent->__set("agent_balance", $balance);
                    $collectionAgentsMapper->updateCollectionAgent($collectionAgent);
                    if ($caTransactionsMapper->addNewCaTransaction($caTransactions)) {
                        //$result = "Connection ID ".$consumer->__get("consumer_connection_id")." charged successfully with Rs.".$amount.". Your balance is ".$balance.". Transaction ID ".$transaction_id.".";
                        //$result = "Connection ID ".$consumer->__get("consumer_connection_id")." - Rs.".$amount.". ".$transaction_id.".";
                        $result = "Connection ID ".$consumer->__get("consumer_connection_id")." - Rs.".$amount.". ".$transaction_id.". ".$consumer->__get("consumer_name")." s/o ".$consumer->__get("consumer_father_name").".";
                        //$result = "Customer account has been recharged successfully. Transaction id " . $transaction_id . ". Your balance is Rs. " . $balance;
                        $name = $consumer->__get("consumer_name");
                        $sms = "Namastey ".$name."! Aapkey TARAurja Connection ".$consumer->__get("consumer_connection_id")." ke liye hamein Rs.".$amount." prapt ho gaye hain. ".$transaction_id.".";
                        //$sms = "Dear " . $name . ", Rs." . $amount . " credited to your account " . $consumer->__get("consumer_connection_id") . ". Transaction ID " . $transaction_id . ".";
                        if($consumer->__get("sms_opt_in")==1 and $send_sms)
                        {
                           // $this->_smsNotification($consumer->__get("consumer_code"), $sms);
                        }
                        else
                        {
                            //$result = "Consumer has opted out of SMS Facility";
                        }
                    } else {
                        $result = "Technical Error, Could not deduct balance from CA Account";
                        //$result = "Error. Kripya kuch der baad try karein.";
                    }
                } else {
                    $result = "Technical Error, Could not add transaction. Please try again";
                    //$result = "Error. Kripya kuch der baad try karein.";
                }
            } else {
                //$result = "Customer not found. Please check the Customer ID and try again.";
                $result = "Error. Customer ID ".$customer_code." not found. Please check.";
            }
        } else {
            //$result = "Error, not enough cash balance, please recharge your CA Account to continue.";
            $result = "Error. Not enough balance.";
        }
        return $result;
    }
	
	function CallCURL($url, $method = 'GET', $params = null, $destination = []){
		//echo $params;exit;
    	if (count($destination) > 0) {
    		foreach ($destination as $item) {
    			$headers[] = $item;
    		}
    	}
    
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	if ($method == 'GET') {
    		if (count($params) > 0) {
    			$url .= '?' . http_build_query($params);
    		}
    	} else {
    		curl_setopt($ch, CURLOPT_POST, true);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    	}
    	curl_setopt($ch, CURLOPT_URL, $url);
    
    	$result = curl_exec($ch);
    	curl_close($ch);
    	return $result;
    }
	
    private function getConsumerBalance($number) {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $currentConsumer = $consumersMapper->searchByColumnValue("consumer_code", $number);
        if (!$currentConsumer) {
            //$result = "Invalid request";
            $result = "Error. Kripya kuch der baad try karein.";
        } else {
            $currentConsumer = $currentConsumer[0];
            $consumer_id = $currentConsumer->__get("consumer_id");
            $outstanding = $consumersMapper->getConsumerOutstanding($consumer_id);
            $activation = $consumersMapper->getRemainingActivation($consumer_id);
            $total = $outstanding + $activation;
            $result = "Dear " . $currentConsumer->__get("consumer_name") . ", your TOTAL outstanding amount is Rs." . $total . " including Rs." . $activation . " of connection charges.";
        }
        return $result;
    }

    public function getSiteByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $site_id = $request->getParam("site_id");
            $sitesMapper = new Application_Model_SitesMapper();
            $site = $sitesMapper->getSiteById($site_id);
            if ($site) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "site_id" => $site->__get("site_id"),
                    "site_name" => $site->__get("site_name"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getMappingClusterByIdAction() {
        $request = $this->getRequest();
        
            $id = $request->getParam("id");
            $mappingMapper = new Application_Model_MappingClusterSitesMapper();
            $mapping = $mappingMapper->getMappingClusterSiteById($id);
            if ($mapping) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "id" => $mapping->__get("mapping_cluster_site_id"),
                    "site_id" => $mapping->__get("site_id"),
                    "cluster_manager_id" => $mapping->__get("cluster_manager_id"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
       
    }

    public function getPackageByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $packagesMapper = new Application_Model_PackagesMapper();
            $package = $packagesMapper->getPackageById($id);
            if ($package) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "package_id" => $package->__get("package_id"),
                    "package_name" => $package->__get("package_name"),
                    "package_cost" => $package->__get("package_cost"),
                    "package_details" => $package->__get("package_details"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getConsumerByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumer = $consumersMapper->getConsumerById($id);
            if ($consumer) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $actDate = $consumer->__get("consumer_act_date");
                if (strlen($actDate) > 2) {
                    $zendDate = new Zend_Date($actDate, "yyyy-MM-dd HH:mm:ss");
                    $date = $zendDate->toString("dd/MM/yyyy");
                } else {
                    $date = "";
                }
                $data = array(
                    "consumer_id" => $consumer->__get("consumer_id"),
                    "consumer_code" => $consumer->__get("consumer_code"),
                    "consumer_name" => $consumer->__get("consumer_name"),
                    "consumer_father_name" => $consumer->__get("consumer_father_name"),
                    "site_id" => $consumer->__get("site_id"),
                    "package_id" => $consumer->__get("package_id"),
                    "consumer_status" => $consumer->__get("consumer_status"),
                    "consumer_act_date" => $date,
                    "consumer_act_charge" => $consumer->__get("consumer_act_charge"),
                    "consumer_connection_id" => $consumer->__get("consumer_connection_id"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getAdminByIdAction() {
        $request = $this->getRequest();
        
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $adminsMapper = new Application_Model_AdminsMapper();
            $admin = $adminsMapper->getAdminById($id);
            if ($admin) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "admin_id" => $admin->__get("admin_id"),
                    "username" => $admin->__get("username"),
                    "admin_fname" => $admin->__get("admin_fname"),
                    "admin_lname" => $admin->__get("admin_lname"),
                    "admin_email" => $admin->__get("admin_email"),
                    "admin_role" => $admin->__get("admin_role"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getClusterManagerByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $clusterMapper = new Application_Model_ClusterManagersMapper();
            $clusterManager = $clusterMapper->getClusterManagerById($id);
            if ($clusterManager) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "id" => $clusterManager->__get("cluster_manager_id"),
                    "name" => $clusterManager->__get("cluster_manager_name"),
                    "phone" => $clusterManager->__get("cluster_manager_phone"),
                    "email" => $clusterManager->__get("cluster_manager_email")
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getUserByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $usersMapper = new Application_Model_UsersMapper();
            $user = $usersMapper->getUserById($id);
            if ($user) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "user_id" => $user->__get("user_id"),
                    "username" => $user->__get("username"),
                    "user_fname" => $user->__get("user_fname"),
                    "user_lname" => $user->__get("user_lname"),
                    "user_email" => $user->__get("user_email"),
                    "phone" => $user->__get("phone"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getEntryTypeByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $cashRegister = $cashRegisterMapper->getCashRegisterById($id);
            if ($cashRegister) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "cr_id" => $cashRegister->__get("cr_id"),
                    "consumer_id" => $cashRegister->__get("consumer_id"),
                    "user_id" => $cashRegister->__get("user_id"),
                    "receipt_number" => $cashRegister->__get("receipt_number"),
                    "cr_entry_type" => $cashRegister->__get("cr_entry_type"),
                    "cr_amount" => $cashRegister->__get("cr_amount"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function generateBackupAction() {
      
        $db = My_Database::getDetails();
        $host = $db["host"];
        $user = $db["username"];
        $pass = $db["password"];
        $name = $db["dbname"];
        $tables = '*';
        $handle = $this->generateSQLFile($host, $user, $pass, $name, $tables);
        print_r($handle); die;
        if ($handle) {
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $data = array();
        } else {
            $meta = array(
                "code" => 500,
                "message" => "ERROR"
            );
            $data = array();
        }
        $array = array(
            "meta" => $meta,
        );
        $json = json_encode($array);
        echo $json;
    }

    public function importBackupAction() {
        $db = My_Database::getDetails();
        $filename = 'db-backup.sql';
        $mysql_host = $db["host"];
        $mysql_username = $db["username"];
        $mysql_password = $db["password"];
        $mysql_database = $db["dbname"];
        mysql_connect($mysql_host, $mysql_username, $mysql_password) or die('Error connecting to MySQL server: ' . mysql_error());
        mysql_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysql_error());
        $templine = '';
        $lines = file($filename);
        foreach ($lines as $line) {
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;
            $templine .= $line;
            if (substr(trim($line), -1, 1) == ';') {
                mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
                $templine = '';
                $flag = false;
            }
        }
        if ($flag) {
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $data = array();
        } else {
            $meta = array(
                "code" => 500,
                "message" => "ERROR"
            );
            $data = array();
        }
        $array = array(
            "meta" => $meta,
        );
        $json = json_encode($array);
        echo $json;
    }

    public function generateSQLFile($host, $user, $pass, $name, $tables) {
        $link = mysqli_connect($host, $user, $pass);
        mysql_select_db($name, $link);

        //get all of the tables
        if ($tables == '*') {
            $tables = array();
            $result = mysql_query('SHOW TABLES');
            while ($row = mysql_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }
        $return = "";
        //cycle through
        foreach ($tables as $table) {
            $result = mysql_query('SELECT * FROM ' . $table);
            $num_fields = mysql_num_fields($result);

            $return.= 'DROP TABLE ' . $table . ';';
            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
       
            $return.= "\n\n" . $row2[1] . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysql_fetch_row($result)) {
                    $return.= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $return.= '"' . $row[$j] . '"';
                        } else {
                            $return.= '""';
                        }
                        if ($j < ($num_fields - 1)) {
                            $return.= ',';
                        }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }

        //save file
        $handle = fopen('db-backup.sql', 'w+');
        fwrite($handle, $return);
        fclose($handle);
        return $handle;
    }

    public function getAllConsumersAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumers = $consumersMapper->getAllConsumers();

            if ($consumers) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );

                foreach ($consumers as $consumer) {
                    $data1 = array(
                        "consumer_id" => $consumer->__get("consumer_id"),
                        "consumer_name" => $consumer->__get("consumer_name"),
                    );
                    $data[] = $data1;
                }
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }

            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getAllUsersAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $usersMapper = new Application_Model_UsersMapper();
            $users = $usersMapper->getAllUsers();

            if ($users) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );

                foreach ($users as $user) {
                    $data1 = array(
                        "user_id" => $user->__get("user_id"),
                        "user_fname" => $user->__get("user_fname"),
                        "user_lname" => $user->__get("user_lname"),
                    );
                    $data[] = $data1;
                }
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }

            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function searchConsumerByCodeAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $code = $request->getParam("code");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumers = $consumersMapper->searchByColumnValue("consumer_code", $code);
            if ($consumers) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array();
                foreach ($consumers as $consumer) {
                    $data[] = array(
                        "consumer_id" => $consumer->__get("consumer_id"),
                        "consumer_code" => $consumer->__get("consumer_code"),
                        "consumer_name" => $consumer->__get("consumer_name"),
                        "consumer_father_name" => $consumer->__get("consumer_father_name"),
                    );
                }
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    /*protected function _smsNotification($number, $sms) {
        $number = substr($number, 0, 10);
        $sms = urlencode($sms);
        //$url = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=TARA&pwd=921118&to=91" . $number . "&sid=TARAMU&msg=" . $sms . "&fl=0&gwid=2";
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=2000140530&auth_scheme=plain&password=wlVYuapVg&v=1.1&format=text";
        //echo $url;exit;
        $text = file_get_contents($url);
        //echo $text;
        //$text = "";
        return $text;
    }*/
	
	protected function _smsNotification($number, $sms) {
        $number = substr($number, 0, 10);
        $sms = urlencode($sms);
        // $url = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=TARA&pwd=921118&to=91" . $number . "&sid=TARAMU&msg=". $sms . "&fl=0&gwid=2";
        //$url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=2000140530&auth_scheme=plain&password=wlVYuapVg&v=1.1&format=text";
        
		$smsMapper =new Application_Model_SmsGatewayMapper();
        $smsgateway = $smsMapper->getAllSmsGateway();
        $user_name=$smsgateway[0]->__get("user_name");
        $password=$smsgateway[0]->__get("password");
 	
          $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=".$user_name."&auth_scheme=plain&password=".$password."&v=1.1&format=text";
		 
		$text = file_get_contents($url);
        return $text;
    }

    public function getCollectionAgentAction() {

        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $collectionagentsMapper = new Application_Model_CollectionAgentsMapper();
            $collectionagent = $collectionagentsMapper->getCollectionAgentById($id);

            if ($collectionagent) {
                $meta = array(
                    "code" => "200",
                    "message" => "SUCCESS"
                );
                $data = array(
                    'agent_mobile' => $collectionagent->__get("agent_mobile"),
                    'agent_fname' => $collectionagent->__get("agent_fname"),
                    'agent_lname' => $collectionagent->__get("agent_lname"),
                    'agent_status' => $collectionagent->__get("agent_status"),
                    'agent_balance' => $collectionagent->__get("agent_balance"),
                    'id' => $id
                );
            } else {
                $meta = array(
                    "code" => "404",
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getCaTransactionAction() {

        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $catransactionsMapper = new Application_Model_CaTransactionsMapper();
            $catransaction = $catransactionsMapper->getCaTransactionById($id);
            $collectionagentsMapper = new Application_Model_CollectionAgentsMapper();
            $collectionagent = $collectionagentsMapper->getCollectionAgentById($catransaction->__get("collection_agent_id"));

            if ($catransaction) {
                $meta = array(
                    "code" => "200",
                    "message" => "SUCCESS"
                );
                $data = array(
                    'collection_agent_id' => $collectionagent->__get("agent_fname"),
                    'transaction_type' => $catransaction->__get("transaction_type"),
                    'transaction_amount' => $catransaction->__get("transaction_amount"),
                    'timestamp' => $catransaction->__get("timestamp"),
                    'remarks' => $catransaction->__get("remarks"),
                    'id' => $id
                );
            } else {
                $meta = array(
                    "code" => "404",
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }
  
    public function changePackage($collectionAgent, $package_id, $customer_code){
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$enterprisePriceMappper = new Application_Model_EnterprisePriceMapper();
    	$packageHistoryMapper= new Application_Model_PackageHistoryMapper();
    	$packageMapper= new Application_Model_PackagesMapper();
    	$packageHistory=new Application_Model_PackageHistory();
    	$meterReadingsMapper=new Application_Model_MeterReadingsMapper();
    	$check=0;$checkresult=0;
    	$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
    	if ($consumers) {
    		$consumer = $consumers[0];
    		$agent_id=$collectionAgent->__get("collection_agent_id");
    		$consumer_id=$consumer->__get("consumer_id");
    		$package_Id=$consumer->__get("package_id");
    		$consumer_number=$consumer->__get("consumer_code");
    		$consumer_name=$consumer->__get("consumer_name");
    		$package_val=explode(":", $package_id);
    		$package_count=count($package_val);
    		
    		$packages= $packageMapper->getPackageById($package_val[0]);
    		 
    		if($packages)
    		{
    			
    			$package_id=$package_val[0];
    			$package_Names=$packages->__get("package_name");
    			$package_Costs=$packages->__get("package_cost");
    			if($package_Id!=$package_id){
    
    				/* $consumer_id=$consumer->__get("consumer_id");
    				 $consumer_number=$consumer->__get("consumer_code");
    				 $consumer_name=$consumer->__get("consumer_name"); */
    				if($packages->__get("is_postpaid")==2){
    					if($package_count==2){
    						$latestReadings = $meterReadingsMapper->getLatestReadingByConsumerId($consumer_id);
    						if($latestReadings){
    							$latestReading=abs($latestReadings->__get("meter_reading"));
						}else{
							$latestReading=0;
						}
    						$start_reading = 0;
    						 $start_reading = abs($package_val[1]);
    							if($start_reading<=$latestReading)
    							{
    								$result = "Error. Previous Reading for ".$customer_code." was ".$latestReading.". New reading must be bigger. Please check and try again.";
    								$checkresult=1;
    							}else{
    								$date = new Zend_Date();
    								$date->setTimezone("Asia/Calcutta");
    								$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    								$meterReading=new Application_Model_MeterReadings();
    								$meterReading->__set("meter_reading",$start_reading);
    								$meterReading->__set("consumer_id",$consumer_id);
    								$meterReading->__set("timestamp",$timestamp);
									$meterReading->__set("start_reading",0);
									$meterReading->__set("package_id",$package_Id);
    								$meterReadingsMapper->addNewMeterReading($meterReading);
    								$check=1;
    							}
    							
    						 
    					}
    					
    				}elseif($packages->__get("is_postpaid")==1){
    					if($package_count==3){
    						$latestReadings = $meterReadingsMapper->getLatestReadingByConsumerId($consumer_id);
						if($latestReadings){
    							$latestReading=abs($latestReadings->__get("meter_reading"));
						}else{
							$latestReading=0;
						}
    						$start_reading = 0;
    						 
    						 $start_reading = abs($package_val[1]);
    							if($start_reading<=$latestReading)
    							{
    							    $result = "Error. Previous Reading for ".$customer_code." was ".$latestReading.". New reading must be bigger. Please check and try again.";
    							    $checkresult=1;
    							}else{
    								$date = new Zend_Date();
    								$date->setTimezone("Asia/Calcutta");
    								$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    								$meterReading=new Application_Model_MeterReadings();
    								$meterReading->__set("meter_reading",$start_reading);
    								$meterReading->__set("consumer_id",$consumer_id);
    								$meterReading->__set("timestamp",$timestamp);
									$meterReading->__set("start_reading",0);
									$meterReading->__set("package_id",$package_Id);
									 
    								$meterReadingsMapper->addNewMeterReading($meterReading);
    								$check=2;
    							}
    							
    						 
    						$consumersupdate=$consumersMapper->updateConsumerMicroPriseByConnectionId($package_val[2], $consumer_id);
    						 
    					}
    				}else{
    					$check=3;
    				}
    				if($checkresult==0){ 
    				if(($check==1 && $package_count==2)||($check==2 && $package_count==3)||($check==3 && $package_count==1)){ 
    				$con_packages=$con_packageMapper->getPackageByConsumerId($consumer_id,$package_Id);
					$updateConsumer=false;
	    				if($con_packages){
	    					$consumer_package=$con_packages[0];
	    					$consumer_package->__set("package_id",$package_id);
	    					$consumer_package->__set("consumer_id",$consumer_id);
	    					$updateConsumer=$con_packageMapper->updatePackageByConsumerID($consumer_package);
	    				}
    				if($updateConsumer){
		
							if($packagehistorys=$packageHistoryMapper->getpackageHistoryByConsumerId($consumer_id)){
									
								$packages_history=$packagehistorys[0];
								$packageId=$packages_history->__get('package_id');
								$consumerId=$packages_history->__get('consumer_id');
								$changeBy=$packages_history->__get('change_by');
								$packageCost=$packages_history->__get('package_cost');
								$packageChangeDate=$packages_history->__get('package_change_date');
								$changeDescription=$packages_history->__get('change_description');
		
								$packages_Name=$packageMapper->getPackageById($packageId);
								$packageName=$packages_Name->__get("package_name");
									
								$packages_history->__set("package_id",$package_id);
								$packages_history->__set("consumer_id",$consumer_id);
								$packages_history->__set("change_by",$agent_id);
								$packages_history->__set("package_cost",$package_Costs);
								if($packageCost<$package_Costs)
								{
									$packages_history->__set("change_description","Package upgraded from ".$packageName."(Rs.".$packageCost.") to " .$package_Names."(Rs.".$package_Costs.")");
								}else{
									$packages_history->__set("change_description","Package downgraded from ".$packageName."(Rs.".$packageCost.") to " .$package_Names."(Rs.".$package_Costs.")");
								}
								$packages_history->__set("status","Package Updated");
								$packages_history->__set("last_package",$packageId);
								/*$file = 'test.txt';
								$current = file_get_contents($file);
								$current .= $package_Names."## ".$package_Costs . "\r\n";
								file_put_contents($file, $current);*/
								$packageHistoryMapper->addNewPackageHistory($packages_history);
									
								$date=new Zend_Date();
								$date->setTimezone("Asia/Kolkata");
								$currentdate=$date->toString("yyyy-MM-dd");
								$day=intval($date->toString("dd"));
								$year=intval($date->toString("yyyy"));
								$month=intval($date->toString("MM"));
								$nextDate=$date->add(1,Zend_Date::MONTH);
								$nextYear=intval($nextDate->toString("yyyy"));
								$nextMonth=intval($nextDate->toString("MM"));
								$liveDate=($day <8)?$year."-".$month."-08":(($day >=8 && $day<15)?$year."-".$month."-15":(($day >=15 && $day<22)?$year."-".$month."-22":$nextYear."-".$nextMonth."-01"));
								/* $result="Package ID ".$package_id." has been updated for Customer ID ".$customer_code."."; */
		
								$sms= "Namastey ".$consumer_name."! Package change request darj kar li hai. Aapka package ".$package_Names." ko change kar diya jayega";
								$result="Package change request received. Connection ID ".$customer_code.". Old Package ". $packageName.". New Package ".$package_Names." will be live from ".$liveDate.".";
								//$this->_smsNotification($consumer_number, $sms);
								
							}
						} else{
							$result="Error. Kripya kuch der baad try karein.";
						}
    				}else{
    					$result="Error. Please enter correct parameter.";
    				} 
    			}  
    			}
    			else{
    				$result="Error. ".$customer_code." already has ". $packages->__get("package_name")." assigned. Check and try again.";
    				//"Package ".$package_id." has already assigned. Please check.";
    			}
    		}
    		else{
    			$result ="Error. Package ".$package_id." not found. Please check.";
    		}
    	}else {
    		$result = "Error. Customer ID ".$customer_code." not found. Please check.";
    	}
    	return $result;
    }
    
    public function checkPackage($collectionAgent, $customer_code){
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$packageMapper= new Application_Model_PackagesMapper();
    	$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
    	if ($consumers) {
    		$consumer=$consumers[0];
    		
    		$package_id=$consumer->__get('package_id');
    		$packages= $packageMapper->getPackageById($package_id);
    		if($packages)
    		{
    			$package_name=$packages->__get("package_name");
    			$result="Customer ID ".$customer_code." ka Package Name ".$package_name." aur Package Id ".$package_id." hai.";
    		}
    	}else {
    		$result = "Error. Customer ID ".$customer_code." not found. Please check.";
    	}
    	return $result;
    }

	public function updateConsumerAct($amount, $customer_code){
    	
    	$consumerMapper=new Application_Model_ConsumersMapper();
    	$consumers = $consumerMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
    	if ($consumers) {
    		$consumer_all = $consumers[0];
    		$consumer_id=$consumer_all->__get("consumer_id");
    		$Act_amt=$consumer_all->__get("consumer_act_charge");
    		$consumer=$consumerMapper->updateConsumerByConnectionId($amount, $consumer_id);
    		if($consumer){
    			$result = "Activation Charge of Customer ID ".$customer_code." has changed from ".$Act_amt." to ".$amount;
    		}
    		else{
    			$result="Error. Kripya kuch der baad try karein.";
    		}
    	}else {
    		$result = "Error. Customer ID ".$customer_code." not found. Please check.";
    	}
    	return $result;
    }
 
	public function updateConsumerCode($amount, $customer_code){
    	 
    	$consumerMapper=new Application_Model_ConsumersMapper();
    	$consumers = $consumerMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
    	if ($consumers) {
    		$consumer_all = $consumers[0];
    		$consumer_id=$consumer_all->__get("consumer_id");
    		$Act_amt=$consumer_all->__get("consumer_code");
    		$consumer=$consumerMapper->updateConsumerCodeByCustomer_id('consumer_code',$amount, $consumer_id);
    		if($consumer){
    			$result = "consumer_code of Customer ID ".$customer_code." has changed from ".$Act_amt." to ".$amount;
    		}
    		else{
    			$result="Error. Kripya kuch der baad try karein.";
    		}
    	}else {
    		$result = "Error. Customer ID ".$customer_code." not found. Please check.";
    	}
    	return $result;
    }

	public function updateConsumerTypeOfConsumer($consumer_type_val, $customer_code){
    
    	$consumerMapper=new Application_Model_ConsumersMapper();
    	$consumers = $consumerMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
    	if ($consumers) {
    		$consumer_all = $consumers[0];
    		$consumer_id=$consumer_all->__get("consumer_id");
    		$last_type=$consumer_all->__get("type_of_me");
			$last_consumer_type=$consumerMapper->getConsumerTypeById($last_type);
			$last_type_name="null";
            if($last_consumer_type){
				$last_type_name=$last_consumer_type["consumer_type_name"];
			}
			$consumer_types= str_replace('_', ' ', $consumer_type_val);
			$consumers_types=(string)$consumer_types;
			//$allTypeOFConsumer=array('HH Basic ', 'HH Advanced ', 'Shops Basic ', 'Shops Advanced ', 'Commercial- Appliance (Existing)', 'Commercial- Appliance (New)', 'Commercial- Motor', 'New Micro-enterprise', 'CDKN - DGO', 'Institutional Load (New)', 'Institutional Load (Existing)', 'Telecom Tower', 'HH Basic', 'HH Advanced', 'Shops Basic', 'Shops Advanced');
    		$consumer_type=$consumerMapper->getConsumerTypeByName($consumers_types);
			$type_name=NULL;
            if($consumer_type){
				$type_name=$consumer_type["consumer_type_id"];
            
				$consumer=$consumerMapper->updateConsumerCodeByCustomer_id('type_of_me',$type_name, $consumer_id);
				if($consumer){
					$result = "consumer Type of Customer ID ".$customer_code." has changed from ".$last_type_name." to ".$consumer_types;
				} else{
					$result="Error. Kripya kuch der baad try karein.";
				}
			}else{
				$result="Error. Consumer Type ".$consumer_type." not found.";
			}
    	}else {
    		$result = "Error. Customer ID ".$customer_code." not found. Please check.";
    	}
    	return $result;
    }
   
	public function updateSiteMeterReading($site_id,$meter_detail,$reading_date,$agent_id){
    	
    	$siteReadingMapper=new Application_Model_SiteMeterReadingMapper();
    	$siteMeterMaster=new Application_Model_SiteMasterMeterMapper();
    	$siteReading=new Application_Model_SiteMeterReading();
    	$siteMapper=new Application_Model_SitesMapper();
    	$date = new Zend_Date($reading_date,"yyyy-MM-dd HH:mm:ss");
    	$timestamp = $date->toString("dd MMM,yyyy");
    	$meter_val=explode(":", $meter_detail);
    	
    	$meter_id=$meter_val[0];
    	$meter_ids=explode(",", $meter_id);
    	
    	$meter_read=$meter_val[1];
    	$meter_reads=explode(",", $meter_read);
    	$result="";
    	for($i=0;$i<count($meter_ids);$i++){
			$meter_names=$siteMeterMaster->getMMById($meter_ids[$i]);
			$feederName=$meter_names->__get("meter_name");
    		$checkMeterReading=$siteReadingMapper->getSiteReadingByMeterId($meter_ids[$i],$reading_date,$site_id);
    		if($checkMeterReading){
    			$validDateMeterReading=$siteReadingMapper->getDateSiteReadingByMeterId($meter_ids[$i],$reading_date,$site_id);
				if($validDateMeterReading){
				
				$validDateMeterReadings=$siteReadingMapper->getLastReadingByMeterId($meter_ids[$i],$reading_date,$site_id);
				if($validDateMeterReadings){
					$lastReading=$validDateMeterReadings["actual_reading"];  
					if(floatval($lastReading)<=floatval($meter_reads[$i])){ 
						$newUnit=floatval($meter_reads[$i])-floatval($lastReading); 
					
					
						$siteMeterRead=new Application_Model_SiteMeterReading();
						$meter_name=$siteMeterMaster->getMMById($meter_ids[$i]);
						$siteName=$siteMapper->getSiteById($site_id);
						$siteMeterRead->__set("site_id",$site_id);
						$siteMeterRead->__set("meter_id",$meter_ids[$i]);
						$siteMeterRead->__set("reading_date",$reading_date);
						$siteMeterRead->__set("reading",$meter_reads[$i]);
						$siteMeterRead->__set("reading",$newUnit);
						$siteMeterRead->__set("actual_reading",$meter_reads[$i]);
						$siteMeterRead->__set("entry_by",$agent_id);
						$siteMeterRead->__set("entry_type",'(M)');
						$Site_MM=$siteReadingMapper->addNewSiteMeterReading($siteMeterRead);
						
						if($Site_MM){
							$results ="Site Master Meter Reading updated successfully.";
						}else{
							$result.="Error ".$feederName;
						}
				
					}else{
						$result.="Meter name ".$feederName. " - New reading ".floatval($meter_reads[$i])." must be greater then last reading ".floatval($lastReading).",";
			    	} 
				}else{
			    	 $result.="Meter name ".$feederName. " ki ".$timestamp." ki entry server par already daali jaa chuki hai.";
			    	  
		    	 } 
				
				}else{
			    	 $result.="Meter name ".$feederName. " ki ".$timestamp." ki entry server par already daali jaa chuki hai.";
			    	  
		    	 } 
			} else{
				$result.="Error ".$feederName;
			}
	     } 
    	 return $result; 
    }

	public function updateSiteMpptReading($site_id,$meter_detail,$reading_date,$agent_id){
    	 
    	$siteReadingMappers=new Application_Model_MpptsReadingMapper();
    	$siteMeterMaster=new Application_Model_MpptsMapper();
    	 $siteMapper=new Application_Model_SitesMapper();
    	$date = new Zend_Date($reading_date,"yyyy-MM-dd HH:mm:ss");
    	$timestamp = $date->toString("dd MMM,yyyy");
    	$meter_val=explode(":", $meter_detail);
    	 
    	$meter_id=$meter_val[0];
    	$meter_ids=explode(",", $meter_id);
    	 
    	$meter_read=$meter_val[1];
    	$meter_reads=explode(",", $meter_read);
    	$result="";
		 
    	for($i=0;$i<count($meter_ids);$i++){
    		$checkMeterReading=$siteReadingMappers->getSiteReadingByMpptsId($meter_ids[$i],$reading_date,$site_id);
    		if($checkMeterReading){
    			$validDateMeterReading=$siteReadingMappers->getDateSiteReadingByMpptId($meter_ids[$i],$reading_date,$site_id);
    			
    			if($validDateMeterReading){
    				$siteMeterRead=new Application_Model_MpptsReading();
    				$meter_name=$siteMeterMaster->getMpptsById($meter_ids[$i]);
    				$siteName=$siteMapper->getSiteById($site_id);
    				$siteMeterRead->__set("site_id",$site_id);
    				$siteMeterRead->__set("mppt_id",$meter_ids[$i]);
    				$siteMeterRead->__set("reading_date",$reading_date);
    				$siteMeterRead->__set("reading",$meter_reads[$i]); 
					$siteMeterRead->__set("entry_by",$agent_id);
    				$siteMeterRead->__set("entry_type",'(M)'); 
    				$Site_MM=$siteReadingMappers->addNewMpptsReading($siteMeterRead);
    					
    				if($Site_MM){
    					$results ="Site MPPTs Reading updated successfully.";
    				}else{
    					$result.="Error ".$meter_ids[$i];
    				}
    			}else{
    				$result.="MPPTs ID ".$meter_ids[$i]. " ki ".$timestamp." ki entry server par already daali jaa chuki hai.";
    
    			}
    		}
    		else{
    			$result.="Error ".$meter_ids[$i];
    		}
    	}
    	return $result;
    }

}
