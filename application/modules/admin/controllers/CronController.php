<?php
ini_set('display_errors',0);
require_once '../PHPExcel/PHPExcel/IOFactory.php';
gc_enable() ;gc_collect_cycles();
require '../aws/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use Aws\Firehose\FirehoseClient;

class Admin_CronController extends Zend_Controller_Action {
	
    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true); 
	}
	
	public function sparkIntegrationAction() {
		
		 $firehose = new FirehoseClient([
				'version' => 'latest',
				'region' => 'us-west-2',
				'credentials' => [ // OR set AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY env vars
					'key' => 'AKIAIBHUVYYMOYVX2EPA',
					'secret' => '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' 
				],
			]);
		date_default_timezone_set('Asia/Kolkata');
		$SparkDataMapper=new Application_Model_SparkDataMapper();
		$url="https://sparkcloud-husk-kathkuiyan.herokuapp.com/api/v0/customers";
		$spark_auth=".eJwFwckNwDAIBMBeeAfJdlgLaony4DD9l-CZjxbgVh2cIcGyCmzow12C1y1QNemhoeE6do3dlTqXmsztJ_pAIxP0Xw5IFMo.DqIV3Q.VefPeFj0la7AxEhAk7Qxc5l3-JQ";
		$param=array(); $destination=array( "Authentication-Token: ".$spark_auth );
		$result=$this->CallCURL($url,'GET',$param,$destination);
		$data=(json_decode($result,true));
		 
		$BulkConsumer=array();
		if($data["status"]=='success'){
			$countConsumer =  count($data["customers"]);
			if($countConsumer>0){
				$consumers =  $data["customers"];
				foreach($consumers as $consumer){
					$code=$consumer["code"];
					$credit_balance=$consumer["credit_balance"];
					$debt_balance=$consumer["debt_balance"]; 
					$ground_id=$consumer["ground"]["id"];
					$ground_name=$consumer["ground"]["name"];
					$customer_id=$consumer["id"];
					$customer_name=$consumer["name"];
					$phone_number=$consumer["phone_number"];
					$phone_number_verified=$consumer["phone_number_verified"];
					$active=$consumer["meters"][0]["active"];
					$address=$consumer["meters"][0]["address"];
					$coords=$consumer["meters"][0]["coords"]; 
					$current_tariff_name=$consumer["meters"][0]["current_tariff_name"];
					$is_running_plan=$consumer["meters"][0]["is_running_plan"];
					$last_energy=$consumer["meters"][0]["last_energy"];
					$last_energy_datetime=$consumer["meters"][0]["last_energy_datetime"];
					$operating_mode=$consumer["meters"][0]["operating_mode"];
					$plan_balance=$consumer["meters"][0]["plan_balance"];
					$serial=$consumer["meters"][0]["serial"];
					$total_cycle_energy=$consumer["meters"][0]["total_cycle_energy"];
								
					$latestReading = $SparkDataMapper->getLatestReadingByConsumerId($consumer["id"]);
					
					$start_reading = 0;$unit=$consumer["meters"][0]["last_energy"];
					if(count($latestReading)>0) {
						$start_reading = $latestReading["last_energy"];
						$unit=$meterReading_val-$start_reading;
					}
					if(floatval(sprintf("%.2f",$unit))>0){
						$Spark = new Application_Model_SparkData();
						$Spark->__set("code",$code);
						$Spark->__set("debt_balance",$debt_balance);
						$Spark->__set("ground_id",$ground_id);
						$Spark->__set("ground_name",$ground_name);
						$Spark->__set("customer_id",$customer_id);
						$Spark->__set("customer_name",$customer_name);
						$Spark->__set("phone_number",$phone_number);
						$Spark->__set("phone_number_verified",$phone_number_verified);	
						$Spark->__set("active",$active);
						$Spark->__set("address",$address);
						$Spark->__set("credit_balance",$credit_balance);
						$Spark->__set("coords",$coords);
						$Spark->__set("current_tariff_name",$current_tariff_name);
						$Spark->__set("is_running_plan",$is_running_plan);
						$Spark->__set("last_energy",$last_energy);
						$Spark->__set("unit",sprintf("%.2f",$unit));  
						$Spark->__set("last_energy_datetime",date('Y-m-d h:i:s',strtotime($last_energy_datetime)));
						$Spark->__set("operating_mode",$operating_mode);	
						$Spark->__set("plan_balance",$plan_balance);
						$Spark->__set("serial",$serial);
						$Spark->__set("total_cycle_energy",$total_cycle_energy);
						$Spark->__set("timestamp",date('Y-m-d h:i:s'));	
												
						$spark_id=$SparkDataMapper->addSparkData($Spark);
						if($spark_id){
							$data_val=array(
								"id"=>intval($spark_id),
								"ground_id"=>$ground_id,
								"ground_name"=>$ground_name,
								"customer_id"=>$customer_id,
								"customer_name"=>$customer_name,
								"phone_number"=>$phone_number,
								"active"=>$active,
								"address"=>$address,
								"current_tariff_name"=>$current_tariff_name,
								"last_energy"=>"".$last_energy."",
								"unit"=>"".$unit."",
								"total_cycle_energy"=>"".$total_cycle_energy."",
								"is_running_plan"=>$is_running_plan,
								"last_energy_datetime"=>$last_energy_datetime,
								"operating_mode"=>$operating_mode,
								"plan_balance"=>"".$plan_balance."",
								"serial"=>$serial,
								 
							);
							if(floatval($unit)>0){
								$json = json_encode($data_val, JSON_PRETTY_PRINT);
								$newArray=array('Data'=> $json);
								$records=array('DeliveryStreamName' => 'SparkMeter','Record'=>$newArray);  
								$resultssss=$firehose->putRecord($records);  
							}
						}
					}
				}
				
				/*$records=array('DeliveryStreamName' => 'SparkMeter','Record'=>$BulkConsumer);  
				$resultssss=$firehose->putRecord($records);  
				print_r($resultssss);exit;*/  
			}
		}
	}

	function CallCURL($url, $method = 'GET', $params = [], $destination = []) {
		if (count($destination) > 0) {
			foreach ($destination as $item) {
				$headers[] = $item;
			}
		}
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if ($method == 'GET') {
			if (count($params) > 0) {
				$url .= '?' . http_build_query($params);
			}
		} else {
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		
		curl_setopt($ch, CURLOPT_URL, $url);
	
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	
}

class ReadFilter implements PHPExcel_Reader_IReadFilter {

	public function __construct($fromColumn, $toColumn) {
		$this->columns = array();
		$toColumn++;
		while ($fromColumn !== $toColumn) {
			$this->columns[] = $fromColumn++;
		}
	}

	public function readCell($column, $row, $worksheetName = '') {
		if (in_array($column, $this->columns)) {
			return true;
		}
		return false;
	}
}


