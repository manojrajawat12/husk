
<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('memory_limit', '5000M');
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

include('MPDF/mpdf.php'); 
require('html2fpdf.php');
require('font/makefont/makefont.php');

/** PHPExcel_IOFactory */

require_once '../PHPExcel/PHPExcel/IOFactory.php';
gc_enable() ;gc_collect_cycles();

class Admin_GenerateExcelController extends Zend_Controller_Action
{
	public function init()
	{
		 $this->_helper->layout()->disableLayout();
         	 $this->_helper->viewRenderer->setNoRender(true);
		
	}
	
	public function indexAction()
	{
	
		$request = $this->getRequest();
		$type = $request->getParam("type");
		$from = $request->getParam("cron");
		$daily = $request->getParam("daily");
		$year = $request->getParam("year");
		$month_val = $request->getParam("month");
		$month = date("m", strtotime($month_val));
		$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
		if($from !=null && $from!=="")
		{
			//$date = date(); 
			$next_date = date('Y-m-d', strtotime($date .' +1 day'));
			$day=date('d', strtotime($next_date));
			if($daily=='yes'){
				$day=1;
			}
			if($day!=1){   
				exit;
			} 
		}

		$newfile='';
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '8192MB'); 
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		$objPHPExcel=null;
		date_default_timezone_set('Asia/Kolkata');
		switch ($type)
		{
			case 'BKS_Defaulter':
				$newfile = 'BKS - Defaulters'.date('d M,Y h-i A').'.xlsm';
				copy('ExcelFiles/BKS - Defaulters - AR ON (txnGroup).xlsm', $newfile);
				$inputFileType = 'Excel2007';
				$inputFileName = $newfile;
				
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				$objPHPExcel->setActiveSheetIndex(0);
				
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
				$rowCount = 2;
				
				foreach(array_reverse($cashRegisters) as $cashRegister)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
					$rowCount++;
				}
				$objPHPExcel->setActiveSheetIndex(1);
				$consumerMapper = new Application_Model_ConsumersMapper();
				$consumers = $consumerMapper->getAllConsumers(false);
				$rowCount = 2;
				
				foreach(array_reverse($consumers) as $consumer)
				{
						
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
					$rowCount++;
				}	
			break;	
			
			case 'BKS_Monthly':
				
				$newfile = 'BKS - Monthly Village Revenue'.date('d M,Y h-i A').'.xlsm';
				copy('ExcelFiles/BKS - Monthly Village Revenue - AR ON.xlsm', $newfile);
				$inputFileType = 'Excel2007';
				$inputFileName = $newfile;
			
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				$objPHPExcel->setActiveSheetIndex(0);
			
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
				$rowCount = 2;
			
				foreach(array_reverse($cashRegisters) as $cashRegister)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
					$rowCount++;
				}
				$objPHPExcel->setActiveSheetIndex(1);
				$consumerMapper = new Application_Model_ConsumersMapper();
				$consumers = $consumerMapper->getAllConsumers(false);
				$rowCount = 2;
			
				foreach(array_reverse($consumers) as $consumer)
				{
			
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
					$rowCount++;
				}
				break;
				
				case 'CM_Current':
					$newfile = 'CM - Current Month Defaulters'.date('d M,Y h-i A').'.xlsm';
					copy('ExcelFiles/CM - Current Month Defaulters - AR ON (TxnGroup).xlsm', $newfile);
					$inputFileType = 'Excel2007';
					$inputFileName = $newfile;
						
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					$objPHPExcel->setActiveSheetIndex(0);
						
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
					$rowCount = 2;
						
					foreach(array_reverse($cashRegisters) as $cashRegister)
					{
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
						$rowCount++;
					}
					$objPHPExcel->setActiveSheetIndex(1);
					$consumerMapper = new Application_Model_ConsumersMapper();
					$consumers = $consumerMapper->getAllConsumers(false);
					$rowCount = 2;
						
					foreach(array_reverse($consumers) as $consumer)
					{
							
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
						$rowCount++;
					}
					break;
					
					case 'CM_Next':
						$newfile = 'CM - Next Month Collection Report'.date('d M,Y h-i A').'.xlsm';
						copy('ExcelFiles/CM - Next Month Collection Report - AR ON.xlsm', $newfile);
						$inputFileType = 'Excel2007';
						$inputFileName = $newfile;
					
						$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
						$objPHPExcel->setActiveSheetIndex(0);
					
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
						$rowCount = 2;
					
						foreach(array_reverse($cashRegisters) as $cashRegister)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("cr_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("consumer_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("transaction_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_entry_type"));
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("cr_amount"));
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $cashRegister->__get("timestamp"));
							$rowCount++;
						}
						$objPHPExcel->setActiveSheetIndex(1);
						$consumerMapper = new Application_Model_ConsumersMapper();
						$consumers = $consumerMapper->getAllConsumers(false);
						$rowCount = 2;
					
						foreach(array_reverse($consumers) as $consumer)
						{
							$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
							foreach ($consumerPackages as $consumerPackage){
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumerPackage["package_id"]);
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("site_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_name"));
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("consumer_status"));
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("consumer_act_charge"));
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $consumer->__get("consumer_connection_id"));
								$rowCount++;
							}
						}
						$objPHPExcel->setActiveSheetIndex(2);
						$packagesMapper = new Application_Model_PackagesMapper();
						$packages = $packagesMapper->getAllPackages(true);
						$rowCount = 2;
							
						foreach(array_reverse($packages) as $package)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_cost"));
							$rowCount++;
						}
				break;
				
					case 'SD_Kwh':
					$newfile = '2SD - KwH daily'.date('d M,Y h-i A').'.xlsm';
					copy('ExcelFiles/2 SD - KwH daily - AR ON.xlsm', $newfile);
					$inputFileType = 'Excel2007';
					$inputFileName = $newfile;
						
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					$objPHPExcel->setActiveSheetIndex(0);
						
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
					$rowCount = 2;
						
					foreach(array_reverse($cashRegisters) as $cashRegister)
					{
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
						$rowCount++;
					}
					$objPHPExcel->setActiveSheetIndex(1);
					$consumerMapper = new Application_Model_ConsumersMapper();
					$consumers = $consumerMapper->getAllConsumers(false);
					$rowCount = 2;
						
					foreach(array_reverse($consumers) as $consumer)
					{
						$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
						foreach ($consumerPackages as $consumerPackage){
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumerPackage["package_id"]);
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_name"));
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_status"));
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("wattage"));
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("micro_entrprise_price"));
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $consumer->__get("consumer_connection_id"));
							$rowCount++;
						}
					}
					$objPHPExcel->setActiveSheetIndex(2);
					$packagesMapper = new Application_Model_PackagesMapper();
					$packages = $packagesMapper->getAllPackages(true);
					$rowCount = 2;
						
					foreach(array_reverse($packages) as $package)
					{
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_name"));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $package->__get("package_cost"));
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $package->__get("package_details"));
						$rowCount++;
					}
					break;
					
					case 'Server_Collection':
						$newfile = '2 Server Collection List'.date('d M,Y h-i A').'.xlsm';
						copy('ExcelFiles/2 Server Collection List- AR ON.xlsm', $newfile);
						$inputFileType = 'Excel2007';
						$inputFileName = $newfile;
					
						$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
						$objPHPExcel->setActiveSheetIndex(0);
					
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
						$rowCount = 2;
					
						foreach(array_reverse($cashRegisters) as $cashRegister)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("user_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_amount"));
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("transaction_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("cr_entry_type"));
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $cashRegister->__get("timestamp"));
							$rowCount++;
						}
						$objPHPExcel->setActiveSheetIndex(2);
						$consumerMapper = new Application_Model_ConsumersMapper();
						$consumers = $consumerMapper->getAllConsumers(false);
						$rowCount = 2;
					
						foreach(array_reverse($consumers) as $consumer)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_connection_id"));
							$rowCount++;
						}
						 $objPHPExcel->setActiveSheetIndex(3);
						$agentsMapper = new Application_Model_CollectionAgentsMapper();
						$agents = $agentsMapper->getAllCollectionAgents();
						$rowCount = 2;
					
						foreach(array_reverse($agents) as $agent)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $agent->__get("collection_agent_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $agent->__get("agent_fname"));
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $agent->__get("agent_lname"));
							$rowCount++;
						} 
						break;
						
						case 'VN_Monthly_Revenue':
							$newfile = '2 VN - Customer Monthly Revenue'.date('d M,Y h-i A').'.xlsm';
							copy('ExcelFiles/2 VN - Customer Monthly Revenue - AR ON.xlsm', $newfile);
							$inputFileType = 'Excel2007';
							$inputFileName = $newfile;
								
							$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
							$objPHPExcel->setActiveSheetIndex(0);
								
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
							$rowCount = 2;
								
							foreach(array_reverse($cashRegisters) as $cashRegister)
							{
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
								$rowCount++;
							}
							$objPHPExcel->setActiveSheetIndex(1);
							$consumerMapper = new Application_Model_ConsumersMapper();
							$consumers = $consumerMapper->getAllConsumers(false);
							$rowCount = 2;
								
							foreach(array_reverse($consumers) as $consumer)
							{
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_act_charge"));
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("consumer_connection_id"));
								$rowCount++;
							}
							
					break;
					case "RF_Monthly" :
							$newfile = 'RF - Monthly report'.date('d M,Y h-i A').'.xlsm';
							copy('ExcelFiles/RF - Monthly report.xlsm', $newfile);
							$inputFileType = 'Excel2007';
							$inputFileName = $newfile;
							
							$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
							$objPHPExcel->setActiveSheetIndex(0);
							
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
							$rowCount = 2;
							$sheet=$objPHPExcel->getActiveSheet();
							$num_rows = count($cashRegisters);
							$row = 2;
							$sheet->insertNewRowBefore($row, $num_rows);
							
							foreach(array_reverse($cashRegisters) as $cashRegister)
							{
								$sheet->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
								$sheet->setCellValue('B'.$rowCount, $cashRegister->__get("cr_entry_type"));
								$sheet->setCellValue('C'.$rowCount, $cashRegister->__get("cr_amount"));
								$sheet->setCellValue('D'.$rowCount, $cashRegister->__get("timestamp"));
								$rowCount++;
							}
							$objPHPExcel->setActiveSheetIndex(1);
							$consumerMapper = new Application_Model_ConsumersMapper();
							if($from !=null && $from!=="")
								{
									$consumers = $consumerMapper->getAllConsumers(false,null,null,null,'true');
								}else{
									$consumers = $consumerMapper->getAllConsumers(false);
								}
							
							$rowCount = 2;
							$sheet=$objPHPExcel->getActiveSheet();
							$num_rows = count($consumers);
							$row = 2;
							$sheet->insertNewRowBefore($row, $num_rows);
							foreach(array_reverse($consumers) as $consumer)
							{
								$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
								foreach ($consumerPackages as $consumerPackage){
								$sheet->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
								$sheet->setCellValue('B'.$rowCount, $consumerPackage["package_id"]);
								//$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_name"));
								$sheet->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
								$sheet->setCellValue('D'.$rowCount, $consumer->__get("wattage"));
								$sheet->setCellValue('E'.$rowCount, $consumer->__get("consumer_act_date"));
								$sheet->setCellValue('F'.$rowCount, $consumer->__get("consumer_connection_id"));
								$sheet->setCellValue('G'.$rowCount, $consumer->__get("type_of_me"));
								$rowCount++;
								}
							}
							$objPHPExcel->setActiveSheetIndex(2);
							$packagesMapper = new Application_Model_PackagesMapper();
							$packages = $packagesMapper->getAllPackages(true);
							$rowCount = 2;
							$sheet=$objPHPExcel->getActiveSheet();
							$num_rows = count($packages);
							$row = 2;
							$sheet->insertNewRowBefore($row, $num_rows);
							foreach(array_reverse($packages) as $package)
							{
								$sheet->setCellValue('A'.$rowCount, $package->__get("package_id"));
								$sheet->setCellValue('B'.$rowCount, $package->__get("package_name"));
								$sheet->setCellValue('C'.$rowCount, $package->__get("package_cost"));
								$sheet->setCellValue('D'.$rowCount, $package->__get("package_details"));
								$rowCount++;
							}
							break;
				
							case 'CM_XL07':
								$newfile = 'CM XL07 Defaulters'.date('d M,Y h-i A').'.xlsm';
								copy('ExcelFiles/CM XL07 Defaulters.xlsm', $newfile);
								$inputFileType = 'Excel2007';
								$inputFileName = $newfile;
							
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								$objPHPExcel->setActiveSheetIndex(0);
							
								$cashRegisterMapper = new Application_Model_CashRegisterMapper();
								$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
								$rowCount = 2;
							
								foreach(array_reverse($cashRegisters) as $cashRegister)
								{
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
									$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
								
									$rowCount++;
								}
								$objPHPExcel->setActiveSheetIndex(1);
								$consumerMapper = new Application_Model_ConsumersMapper();
								$consumers = $consumerMapper->getAllConsumers(false);
								$rowCount = 2;
							
								foreach(array_reverse($consumers) as $consumer)
								{
										
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
									$rowCount++;
								}
								break;
					case 'SD_KwhEnroll':
									$newfile = 'SD - KwH & Enrollment'.date('d M,Y h-i A').'.xlsm';
									copy('ExcelFiles/SD - KwH & Enrollment.xlsm', $newfile);
									$inputFileType = 'Excel2007';
									$inputFileName = $newfile;
								
									$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
									$objPHPExcel->setActiveSheetIndex(0);
								
									$cashRegisterMapper = new Application_Model_CashRegisterMapper();
									$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
									$rowCount = 2;
								
									foreach(array_reverse($cashRegisters) as $cashRegister)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
										$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
										$rowCount++;
									}
									$objPHPExcel->setActiveSheetIndex(1);
									$consumerMapper = new Application_Model_ConsumersMapper();
									$consumers = $consumerMapper->getAllConsumers(false);
									$rowCount = 2;
								
									foreach(array_reverse($consumers) as $consumer)
									{
										$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
										foreach ($consumerPackages as $consumerPackage){
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumerPackage["package_id"]);
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_name"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_status"));
										$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("wattage"));
										$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("micro_entrprise_price"));
										$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $consumer->__get("consumer_act_date"));
										$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowCount, $consumer->__get("consumer_connection_id"));
										$rowCount++;
										}
									}
									$objPHPExcel->setActiveSheetIndex(2);
									$meterReadingMapper = new Application_Model_MeterReadingsMapper();
									$meterReading = $meterReadingMapper->getAllMeterReadings();
									$rowCount = 2;
									
									foreach(array_reverse($meterReading) as $meter)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $meter->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $meter->__get("meter_reading"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $meter->__get("timestamp"));
								
										$rowCount++;
									}
									$objPHPExcel->setActiveSheetIndex(5);
									$packagesMapper = new Application_Model_PackagesMapper();
									$packages = $packagesMapper->getAllPackages(true);
									$rowCount = 2;
								
									foreach(array_reverse($packages) as $package)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_name"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $package->__get("package_cost"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $package->__get("package_details"));
										$rowCount++;
									}
						break;
				case 'RF_WeeklyEnroll':
										$newfile = 'RF - Weekly Enrollment'.date('d M,Y h-i A').'.xlsm';
										copy('ExcelFiles/RF - Weekly Enrollment.xlsm', $newfile);
										$inputFileType = 'Excel2007';
										$inputFileName = $newfile;
									
										$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
										$objPHPExcel->setActiveSheetIndex(0);
									
										$consumerMapper = new Application_Model_ConsumersMapper();
										$consumers = $consumerMapper->getAllConsumers(false);
										$rowCount = 2;
									
										foreach(array_reverse($consumers) as $consumer)
										{
											
											$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
											foreach ($consumerPackages as $consumerPackage){
											$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumerPackage["package_id"]);
											$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_status"));
											$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_act_date"));
											$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
											$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("suspension_date"));
											$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("type_of_me"));

											$rowCount++;
											}
										}
										$objPHPExcel->setActiveSheetIndex(1);
										$packagesMapper = new Application_Model_PackagesMapper();
										$packages = $packagesMapper->getAllPackages(true);
										$rowCount = 2;
									
										foreach(array_reverse($packages) as $package)
										{
											$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
											$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_name"));
											$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $package->__get("package_cost"));
											$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $package->__get("package_details"));
											$rowCount++;
										}
										break;
					case "SPI_Monthly" :
								$newfile = 'SPI Monthly Report'.date('d M,Y h-i A').'.xlsm';
								copy('ExcelFiles/SPI Monthly Report.xlsm', $newfile);
								$inputFileType = 'Excel2007';
								$inputFileName = $newfile;
									
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								$objPHPExcel->setActiveSheetIndex(0);
								$rowCount = 2;

								$consumerMapper = new Application_Model_ConsumersMapper();
								if($from !=null && $from!=="")
								{
									$consumers = $consumerMapper->getAllConsumers(false,null,null,null,'true');
								}else{
									$consumers = $consumerMapper->getAllConsumers(false);
								}
								$rowCount = 2;
							  
								foreach(array_reverse($consumers) as $consumer)
								{
									$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
									foreach ($consumerPackages as $consumerPackage){
									//$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumerPackage["package_id"]);
									//$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_name"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_status"));
									//$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("wattage"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_act_date"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("suspension_date"));
									$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("type_of_me"));
							        $rowCount++;
									
									}
								}
								$objPHPExcel->setActiveSheetIndex(1);
								$packagesMapper = new Application_Model_PackagesMapper();
								$packages = $packagesMapper->getAllPackages(true);
								$rowCount = 2;
									
								foreach(array_reverse($packages) as $package)
								{
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_name"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $package->__get("package_cost"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $package->__get("package_details"));
									
									$rowCount++;
								}
								break;
								
		case "Debtor_Aging" :
								$newfile = 'Debtor Aging Sheet'.date('d M,Y h-i A').'.xlsm';
								copy('ExcelFiles/Debtor Aging Sheet.xlsm', $newfile);
								$inputFileType = 'Excel2007';
								$inputFileName = $newfile;
									
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								$objPHPExcel->setActiveSheetIndex(0);
								$rowCount = 2;

								$consumerMapper = new Application_Model_ConsumersMapper();
								$consumers = $consumerMapper->getAllConsumers(false);
								$rowCount = 2;
									
								foreach(array_reverse($consumers) as $consumer)
								{
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_connection_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("site_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_status"));
									$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("consumer_act_date"));
									$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("consumer_act_charge"));

							
									$rowCount++;
								}
									$objPHPExcel->setActiveSheetIndex(1);
									$cashRegisterMapper = new Application_Model_CashRegisterMapper();
									$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
									$rowCount = 2;
								
									foreach(array_reverse($cashRegisters) as $cashRegister)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("cr_entry_type"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_amount"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("timestamp"));
									//	$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("cr_status"));
										$rowCount++;
									}
									
								/*	$objPHPExcel->setActiveSheetIndex(2);
									$siteMapper = new Application_Model_SitesMapper();
									$sites=$siteMapper->getAllSites();
									$rowCount = 2;
								
									foreach(array_reverse($sites) as $site)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $site->__get("site_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $site->__get("site_name"));
										$rowCount++;
									}*/
								break;	
								case "Socket8001" : 		
								$newfile = 'Report template '.date('d M,Y h-i A').'.xlsm';
								copy('ExcelFiles/Report_template.xlsm', $newfile);
								$inputFileType = 'Excel2007';
								$inputFileName = $newfile;
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								break; 

case "MED_Revenue" :
									$newfile = 'MED Revenue data.xlsm'; 
									copy('ExcelFiles/MED Revenue data.xlsm', $newfile);
									$inputFileType = 'Excel2007';
									$inputFileName = $newfile;
										
									$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
									$objPHPExcel->setActiveSheetIndex(0);
									$rowCount = 2;
								
									$consumerMapper = new Application_Model_ConsumersMapper();
									$consumers = $consumerMapper->getAllConsumers(false);
									$rowCount = 2;
										
									foreach(array_reverse($consumers) as $consumer)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_connection_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_act_date"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_status"));
										 
								
											
										$rowCount++;
									}
									$objPHPExcel->setActiveSheetIndex(1);
									$cashRegisterMapper = new Application_Model_CashRegisterMapper();
									$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
									$rowCount = 2;
								
									foreach(array_reverse($cashRegisters) as $cashRegister)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("cr_entry_type"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_amount"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("timestamp"));
										 
										$rowCount++;
									}
										
									$objPHPExcel->setActiveSheetIndex(2);
									 $siteMapper = new Application_Model_SitesMapper();
									 $sites=$siteMapper->getAllSites();
									 $rowCount = 2;
								
									 foreach(array_reverse($sites) as $site)
									 {
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $site->__get("site_id"));
									 	$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $site->__get("site_name"));
									 	
									 	$rowCount++;
									 }
									 $objPHPExcel->setActiveSheetIndex(3);
									 $CelamedMapper = new Application_Model_CelamedMapper();
									 $Celameds=$CelamedMapper->getAllCelameds();
									 $rowCount = 2;
									 
									 foreach(array_reverse($Celameds) as $Celamed)
									 {
									 	$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $Celamed->__get("consumer_id"));
									 	$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $Celamed->__get("intervention_type"));
									 		
									 	$rowCount++;
									 }
									break;							
			 case "YTD_Sheet_exl" :
		 		$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
				$trans_date=date_parse_from_format("Y-m-d", $timestamp);
				$day=$trans_date["day"];
			 	$month= $trans_date["month"];
				 
				$year= $trans_date["year"];
				$stateMapper=new Application_Model_StatesMapper();
				$clusterMapper=new Application_Model_ClustersMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$siteMapper=new Application_Model_SitesMapper();
				$consumerMapper=new Application_Model_ConsumersMapper();
				$packageMapper=new Application_Model_PackagesMapper();
				
		 		$StateName_Array=array();
			  	$sites=$siteMapper->getAllSites(); 
				$counters=0;
				$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
				$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
			  $consumerdata="";
			 	
			  
			  $consumerdata.= "<div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>
	                <table  border=1 align='center'>
                        <tbody>
                         <tr> ";
			 
			  $consumerdata.= "<tr>
							<th></th>  <th></th>  <th></th>
							<th></th>  <th></th>  <th></th> 
							<th></th>  <th></th>  <th></th>
							<th></th>  <th></th>  <th></th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
								
									$year_vals=$year;
									if($month >=4)
										$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
										$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th width='120px' colspan='5'>".$months[$m].",".$year_vals."</th>
											<th></th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
                                <th width='50px'>S.No</th>
							  	<th width='50px'>State Name</th>
							  	<th width='50px'>Site Name</th>
                                <th width='150px'>Connection ID</th>
				 				<th width='130px'>Consumers Name</th> 
							  	<th width='130px'>Mobile No.</th>
							  	<th width='50px'>Consumer Status</th>
							  	<th width='50px'>Date</th>
							  	<th width='130px'>ACT</th>
							  	<th width='130px'>OTHERS</th>	
							  	<th width='130px'>Type</th>
							  	<th>Opening Balance</th>";
								
							for($m=0 ; $m<sizeof($months);$m++)
							{
								    $consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "
                                <th  width='80px'>Balance</th>
							 </tr>";
			foreach ($sites as $site)
			{
			  	$consumerdata.="";
			  	if($site->__get("site_id")!=1){  
			  		 
					 	
			  			$stateName=$stateMapper->getStateById($site->__get("state_id"));
			  				$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"),true);
			  				
			  				if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			  				foreach ($consumers as $consumer){
							 	
			  					$consumer_id=$consumer->__get("consumer_id");	
				  				$count= $count+1;
				  				$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
				  				$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
				  				$meteres=array();
				  				if($consumerPackages){
				  					foreach ($consumerPackages as $consumerPackage){
					  					$sites_val=$packageMapper->getPackageById($consumerPackage["package_id"]);
					  					 
					  					if($sites_val){
					  						if($sites_val->__get("is_postpaid")==2){
					  							$meteres[]="MM";
					  						}elseif ($sites_val->__get("is_postpaid")==1){
					  							$meteres[]="ME";
					  						}else{
					  							$meteres[]="FIXED";
					  						}
					  					}
				  					}
				  				}
				  				$meteres=" (".implode(",", $meteres).")";
				  				
				  				$ACT_value=$cashRegisterMapper->getOTPInByConsumerId($consumer_id);
				  				$other_value=$cashRegisterMapper->getOtherAmtByConsumerId($consumer_id);
				  				
				  				$ACT_value=$consumer->__get("consumer_act_charge")-$ACT_value;
				  				$consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$stateName->__get("state_name")."</td>
		                                    <td>".$site->__get("site_name")."</td>
		                                    <td>".$consumer->__get("consumer_connection_id").$metered."</td>
										    <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
											<td>".$consumer->__get("consumer_code")."</td>
											<td>".$consumer->__get("consumer_status")."</td>
											<td>".$consumer->__get("consumer_act_date")."</td>
											<td>".$ACT_value."</td>
											<td>".$other_value."</td>";
										 
											
								
				  				$total_type=array("CHG","EED","MED","WATER");
								$counter=0;
								for ($j=0;$j<count($total_type);$j++){
									$year_balance=0;
									$totalDebit=0;$totalCredit=0;
									$datetime=$year."-04-01";
									$checkStatus=$cashRegisterMapper->checkEntryAmtByConsumerId($consumer_id,$total_type[$j],$datetime);
									if($checkStatus){
										if($counter!=0){
											$consumerdata.="<tr><td>".NULL."</td><td>".NULL."</td><td>".NULL."</td><td>".NULL."</td>
														<td>".NULL."</td><td>".NULL."</td><td>".NULL."</td><td>".NULL."</td>
														<td>".NULL."</td> <td>".NULL."</td>";
										}
										$counter=$counter+1;
										$outstanding_value=$cashRegisterMapper->getOutByConsumerId($consumer_id,$month,$year,null,null,$total_type[$j]);
										$consumerdata.="<td>".$total_type[$j]."</td>
														<td>".$outstanding_value."</td>";
										for($n=0 ; $n<sizeof($month_no);$n++)
										{
											$year_val=$year;
											if($month >=4)
												$year_val=($month_no[$n] >=4)?$year:$year+1;
											else {
												$year_val=($month_no[$n] >=4)?$year-1:$year;
											}
											$debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$n],$year_val,null,$total_type[$j]);
						  					$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val,null,$total_type[$j]);
											
											$consumerdata.= "<td>".$debit."</td>
												    		<td>".$credit."</td>";
											$totalDebit=$totalDebit+$debit;
											$totalCredit=$totalCredit+$credit;
											if($month_no[$n]==$month){
												break;
											}
										}		    
										$year_balances=$totalDebit - $totalCredit;
										 
										$year_balance=$year_balances+($outstanding_value);
					                     $consumerdata.= "<td align='center'>". $year_balance."</td>
						                </tr> ";
				                     
									}
								}
			                }
							 
				  		 }
			  	 
			 	
			  $consumerdata.=  " 
			 
			  			<tr>";
			  			 
						 
							  $consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"));
							  
			  	if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			  				foreach ($consumers as $consumer){
								
			  					$consumer_id=$consumer->__get("consumer_id");	
				  				$count= $count+1;
				  				$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
				  				$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer_id);
				  				$meteres=array();
				  				if($consumerPackages){
				  					foreach ($consumerPackages as $consumerPackage){
					  					$sites_val=$packageMapper->getPackageById($consumerPackage["package_id"]);
					  					 
					  					if($sites_val){
					  						if($sites_val->__get("is_postpaid")==2){
					  							$meteres[]="MM";
					  						}elseif ($sites_val->__get("is_postpaid")==1){
					  							$meteres[]="ME";
					  						}else{
					  							$meteres[]="FIXED";
					  						}
					  					}
				  					}
				  				}
				  				$meteres=" (".implode(",", $meteres).")";
				  				
				  				$ACT_value=$cashRegisterMapper->getOTPInByConsumerId($consumer_id);
				  				$other_value=$cashRegisterMapper->getOtherAmtByConsumerId($consumer_id);
				  				
				  				$ACT_value=$consumer->__get("consumer_act_charge")-$ACT_value; 
				  				$consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$stateName->__get("state_name")."</td>
		                                    <td>".$site->__get("site_name")."</td>
		                                    <td>".$consumer->__get("consumer_connection_id").$metered."</td>
										    <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
											<td>".$consumer->__get("consumer_code")."</td>
											<td>".$consumer->__get("consumer_status")."</td>
											<td>".$consumer->__get("suspension_date")."</td>
											<td>".$ACT_value."</td>
											<td>".$other_value."</td>";
										 
											
								
				  				$total_type=array("CHG","EED","MED","WATER");
								$counter=0; 
								for ($j=0;$j<count($total_type);$j++){
									$year_balance=0;
									$totalDebit=0;$totalCredit=0;
									$datetime=$year."-04-01";
									$checkStatus=$cashRegisterMapper->checkEntryAmtByConsumerId($consumer_id,$total_type[$j],$datetime);
									if($checkStatus){
										if($counter!=0){
											$consumerdata.="<tr><td>".NULL."</td><td>".NULL."</td><td>".NULL."</td><td>".NULL."</td>
														<td>".NULL."</td><td>".NULL."</td><td>".NULL."</td><td>".NULL."</td>
														<td>".NULL."</td> <td>".NULL."</td>";
										}
										$counter=$counter+1;
										$outstanding_value=$cashRegisterMapper->getOutByConsumerId($consumer_id,$month,$year,null,null,$total_type[$j]);
										$consumerdata.="<td>".$total_type[$j]."</td>
														<td>".$outstanding_value."</td>";
										for($n=0 ; $n<sizeof($month_no);$n++)
										{
											$year_val=$year;
											if($month >=4)
												$year_val=($month_no[$n] >=4)?$year:$year+1;
											else {
												$year_val=($month_no[$n] >=4)?$year-1:$year;
											}
											$debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$n],$year_val,null,$total_type[$j]);
						  					$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val,null,$total_type[$j]);
											
											$consumerdata.= "<td>".$debit."</td>
												    		<td>".$credit."</td>";
											$totalDebit=$totalDebit+$debit;
											$totalCredit=$totalCredit+$credit;
											if($month_no[$n]==$month){
												break;
											}
										}  
										$year_balances=$totalDebit - $totalCredit;
										$year_balance=$year_balances+($outstanding_value);
					                     $consumerdata.= "<td align='center'>". $year_balance."</td>
						                </tr> ";
				                     
									}
								}
			                }
							 
				  		 }
					 
					$consumerdata.=  "</tbody> </table>
				                		</div>
				           			</div>
						   		</div>
				         	</div>"; 
				  
         	   
				}
			} 
			//echo $consumerdata;exit;
						$newfile = 'Collection Sheet '.date('d M,Y h-i A').'.xlsx';
						$inputFileType = 'Excel2007';
						$tmpfile = tempnam(sys_get_temp_dir(), 'html');
						file_put_contents($tmpfile, $consumerdata);
						
						$objPHPExcel = new PHPExcel();
						$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
						$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('Collection Sheet');
						break; 

		case "YTD_Sheet_old_exl" :
				$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
				$trans_date=date_parse_from_format("Y-m-d", $timestamp);
				$day=$trans_date["day"];
			 	$month= $trans_date["month"];
				//$month= 8;$year=2016;
				$year= $trans_date["year"];
				$stateMapper=new Application_Model_StatesMapper();
				$clusterMapper=new Application_Model_ClustersMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$siteMapper=new Application_Model_SitesMapper();
				$consumerMapper=new Application_Model_ConsumersMapper();
				$packageMapper=new Application_Model_PackagesMapper();
				
		 		$StateName_Array=array(); 
			  	$sites=$siteMapper->getAllSites(); 
				$counters=0;
				$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
				$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
				$consumerdata="";
			  foreach ($sites as $site)
			  {
			  	$consumerdata.="";
			  	if($site->__get("site_id")!=1){
			  		 
					 $consumerdata.= " <h4 align='center'>Collection Sheet (Old Cycle) for ".date('F, Y')."</h4>";
			  
			  $stateName=$stateMapper->getStateById($site->__get("state_id"));
			  $consumerdata.= "<div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>
	                <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
                        <tbody>
                         <tr>
						  <th colspan='29' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			 
			  $consumerdata.= "<tr>
                                <th></th>
						  		<th></th>
						  		<th></th>  
						  		<th></th>";

							for($m=0 ; $m<sizeof($months);$m++)
							{
								
									$year_vals=$year;
									if($month >=4)
										$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
										$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>
											<th></th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
                                <th width='50px'>S.No</th>
                                <th width='150px'>Connection ID</th>
				 				<th width='130px'>Consumers Name</th> 
							  	<th width='130px'>Mobile No.</th>";
								
							for($m=0 ; $m<sizeof($months);$m++)
							{
								 
									$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>
											<th></th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "
                                <th  width='80px'>Balance</th>
							 </tr>";

			  				$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"),true);
			  				 
			  				if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			  				foreach ($consumers as $consumer){
			  					$year_balance=0;
			  					$totalDebit=0;$totalCredit=0;
			  					$consumer_id=$consumer->__get("consumer_id");	
				  				$count= $count+1;
				  				$sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM)</b>";
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				}
								}
								$consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$consumer->__get("consumer_connection_id").$metered."</td>
										    <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
											<td>".$consumer->__get("consumer_code")."</td>";
								for($n=0 ; $n<sizeof($month_no);$n++)
								{
									 
									$year_val=$year;
									if($month >=4)
										$year_val=($month_no[$n] >=4)?$year:$year+1;
									else {
										$year_val=($month_no[$n] >=4)?$year-1:$year;
									}
									$debit=$cashRegisterMapper->getMonthlyDebitByConsumerId($consumer_id,$month_no[$n],$year_val);
				  					$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
									$consumerdata.= "<td>".$debit."</td>
										    <td>".$credit."</td>";
									$totalDebit=$totalDebit+$debit;
									$totalCredit=$totalCredit+$credit;
									if($month_no[$n]==$month){
										break;
									}
								}		    
								$year_balance=$totalDebit - $totalCredit;
			                     $consumerdata.= "<td align='center'>". $year_balance."</td>
				                </tr> ";
			  				}
				  	 }
			  		  
			  		 
				 
			  $consumerdata.=  "</tbody> </table> </div></div></div></div>
			  <h4 align='center'>Banned Consumers</h4>
			  <div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>		
			  		<table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
			  			<tbody>
			  			<tr>
			  			<th colspan='29' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			  
						$consumerdata.= "<tr>
							<th></th>
						  		<th></th>
						  		<th></th>
						  		<th></th>
								<th></th>";
							  
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
							  	  $year_vals=$year;
								  if($month >=4)
								  	$year_vals=($month_no[$m] >=4)?$year:$year+1;
								  else {
								  	$year_vals=($month_no[$m] >=4)?$year-1:$year;
								  }
								  $consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>
								  		<th></th>";
								  if($month_no[$m]==$month){
								  	break;
								  }
							  }
							  $consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
							  		<th width='50px'>S.No</th>
							  		<th width='150px'>Connection ID</th>
							  		<th width='130px'>Consumers Name</th>
							  		<th width='130px'>Mobile No.</th>
							  		<th width='130px'>Banned Date</th>";
							  
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
							  		$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
							  		if($month_no[$m]==$month){
							  			break;
							 		}
							  }
							  $consumerdata.= "
							  	<th  width='80px'>Balance</th>
							  </tr>";
							  
							  $consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"));
							  
							  if($consumers){
							  $count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
							  foreach ($consumers as $consumer){
							  $year_balance=0;
							  $totalDebit=0;$totalCredit=0;
							  $consumer_id=$consumer->__get("consumer_id");
							  $count= $count+1;
							  $sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
							  $DODate=$consumerMapper->getConsumerDODatebyConnectionId($consumer->__get("consumer_connection_id"));
							  
				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM)</b>";
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				}}
							  $consumerdata.= "  <tr>
							  <td>".$count."</td>
							  <td>".$consumer->__get("consumer_connection_id").$metered."</td>
							  <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
							  		<td>".$consumer->__get("consumer_code")."</td>
							  				<td>".$DODate["timestamp"]."</td>";
							  		for($n=0 ; $n<sizeof($month_no);$n++)
							  		{
							  			$year_val=$year;
							  			if($month >=4)
							  				$year_val=($month_no[$n] >=4)?$year:$year+1;
							  			else {
							  				$year_val=($month_no[$n] >=4)?$year-1:$year;
							  			}
							  			$debit=$cashRegisterMapper->getMonthlyDebitByConsumerId($consumer_id,$month_no[$n],$year_val);
							  			$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
							  			$consumerdata.= "<td>".$debit."</td>
							  			<td>".$credit."</td>";
							  			$totalDebit=$totalDebit+$debit;
							  			$totalCredit=$totalCredit+$credit;
							  			if($month_no[$n]==$month){
							  					break;
							  		}
							  }
							  		$year_balance=$totalDebit - $totalCredit;
							  		$consumerdata.= "<td align='center'>". $year_balance."</td>
							  		</tr> ";
							  }
							}
					$consumerdata.=  "</tbody> </table>
				                		</div>
				           			</div>
						   		</div>
				         	</div> 
						<h5 align='right'><i>File created on ".date('jS F, Y')." at ".date('h:i A')." </i></h5>";
					
         	 		 
         	 		
				}
			  }
						$newfile = 'Collection Sheet (Old Cycle) '.date('d M,Y h-i A').'.xlsx';
						$inputFileType = 'Excel2007';
						$tmpfile = tempnam(sys_get_temp_dir(), 'html');
						file_put_contents($tmpfile, $consumerdata);
						
						$objPHPExcel = new PHPExcel();
						$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
						$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('Collection Sheet (Old Cycle)');
							
				break;
		case "Monthly_revenue_report" :
					$date = new Zend_Date();
					$date->setTimezone("Asia/Calcutta");
					$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
					$trans_date=date_parse_from_format("Y-m-d", $timestamp);
					$day=$trans_date["day"];
					$month= $trans_date["month"];
					$years= $trans_date["year"];
					 
					$stateMapper=new Application_Model_StatesMapper();
					$clusterMapper=new Application_Model_ClustersMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
				
					$StateName_Array=array();
					$sites=$siteMapper->getAllSites();
					$counters=0;
					$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
					$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
					$consumerdata="";
					$states=$stateMapper->getAllStates();
					$lastyear=$year+1;
					
					$consumerdata.= " <h4><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>Monthly Revenue Report for Year ".$year."-".$lastyear."</h4>";
					
					$entry_status=array("'CHG'","'ACT'");
					 $roleSession = new Zend_Session_Namespace('roles');
					$role_sites_id=$roleSession->site_id;			
							 
			 $consumerdata.= "<div>
			  
	                <table border=1 align='center'>
                        <tbody>";
				
		     	$consumerdata.= "<tr><th></th>";
				
							for($m=0 ; $m<sizeof($months);$m++)
							{
				
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th></th>
											<th>".$months[$m].",".$year_vals."</th>
											<th></th>";
									if($year==$years){
										if($month_no[$m]==$month){
											break;
											}
									}
							}
							$consumerdata.= "</tr><tr><th>Site Name</th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
							
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th> Billed Amt.</th>
											<th>Collected Amt.</th>
											<th>Outstanding Amt.</th>";
									if($year==$years){
										if($month_no[$m]==$month){
											break;
											}
									}
							}
							$consumerdata.= "</tr>";
								 
							foreach ($states as $state){
							 
							 $consumerdata.= "<tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th><h4>".$state->__get("state_name")."</h4></th>
								</tr>";
					$sites=$siteMapper->getSiteByStateId($state->__get("state_id"));
					foreach ($sites as $site)
					 {
							 		
						if($site->__get("site_id")!=1){
							 
							 $consumerdata.= "<tr><td>".$site->__get("site_name")."</td>";
							 
							 for($m=0 ; $m<sizeof($months);$m++)
							 {
								
								$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
								else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
								}
									//$balance_debit=$cashRegisterMapper->getBalanceBySite($site->__get("site_id"),$month_no[$m],$year,"DEBIT");
									//$balance_credit=$cashRegisterMapper->getBalanceBySite($site->__get("site_id"),$month_no[$m],$year,"CREDIT");
									$total_days_in_month=cal_days_in_month(CAL_GREGORIAN,$month_no[$m], $year_vals);
									$curr_date=$year_vals."-".$month_no[$m]."-".$total_days_in_month; 
									 $last_date=$year_vals."-".$month_no[$m]."-01";   
									$balance_debit = $cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site->__get("site_id"),"DEBIT",$last_date,null,null,null,$entry_status);
									$balance_credit=$cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site->__get("site_id"),"CREDIT",$last_date,null,null,null,array("'CHG'"));
									$balance_act=$cashRegisterMapper->getTotalCollectedByMonth($curr_date,$site->__get("site_id"),"ACTIVATION",$last_date,null,null,null,array("'ACT'"));
									$balance_credit=intval($balance_credit)+intval($balance_act);

											$consumerdata.= "<td> ".$balance_debit."</td>
													<td>".$balance_credit."</td>
													<td>".($balance_debit-$balance_credit)."</td>";
											if($year==$years){
												if($month_no[$m]==$month){
													break;
													}
											}
							 }
					 	$consumerdata.= "</tr> ";
					 
			  			 	}
					 }
					 
					 $consumerdata.= "<tr><th><b>Total</b></th>"; 
					 for($m=0 ; $m<sizeof($months);$m++)
					 {
					 	
					 $year_vals=$year;
					 	if($month >=4)
					 		$year_vals=($month_no[$m] >=4)?$year:$year+1; 
					 		else {
					 		$year_vals=($month_no[$m] >=4)?$year-1:$year;
					 		}
					 		//$Total_balance_debit=$cashRegisterMapper->getBalanceByState($state->__get("state_id"),$month_no[$m],$year,"DEBIT");
					 		//$Total_balance_credit=$cashRegisterMapper->getBalanceByState($state->__get("state_id"),$month_no[$m],$year,"CREDIT");
							$total_days_in_month=cal_days_in_month(CAL_GREGORIAN,$month_no[$m], $year_vals);
							  $curr_date=$year_vals."-".$month_no[$m]."-".$total_days_in_month;
							  $last_date=$year_vals."-".$month_no[$m]."-01";  
					 		$Total_balance_debit = $cashRegisterMapper->getTotalCollectedByMonth($curr_date,null,"DEBIT",$last_date,$state->__get("state_id"),$role_sites_id,null,$entry_status);
					 		$Total_balance_credit=$cashRegisterMapper->getTotalCollectedByMonth($curr_date,null,"CREDIT",$last_date,$state->__get("state_id"),$role_sites_id,null,array("'CHG'"));
					 		$Total_balance_act=$cashRegisterMapper->getTotalCollectedByMonth($curr_date,null,"ACTIVATION",$last_date,$state->__get("state_id"),$role_sites_id,null,array("'ACT'"));
					 		 $Total_balance_credit=intval($Total_balance_credit)+intval($Total_balance_act);
					 			$consumerdata.= "<td> ".$Total_balance_debit."</td>
												 <td>".$Total_balance_credit."</td>
												 <td>".($Total_balance_debit-$Total_balance_credit)."</td>";
												 if($year==$years){
													if($month_no[$m]==$month){
														break;
														}
												 } 
							} 
					  $consumerdata.= "</tr>";
					 
							}
										$consumerdata.=  "</tbody> </table>
						
										</div>";
							  
										$newfile = 'Monthly Revenue Report for '.$year.'-'.$lastyear.'.xlsx';
										$inputFileType = 'Excel2007';
												$tmpfile = tempnam(sys_get_temp_dir(), 'html');
												file_put_contents($tmpfile, $consumerdata);
				
												$objPHPExcel = new PHPExcel();
												$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
												$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('Monthly Revenue Report '); 
													
												break;
			case "Monthly_active_consumer" :
													 
						$stateMapper=new Application_Model_StatesMapper();
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$siteMapper=new Application_Model_SitesMapper();
						$consumerMapper=new Application_Model_ConsumersMapper();
						$packageMapper=new Application_Model_PackagesMapper();
						$consumerdata="";
						$StateName_Array=array();
													 	
						$consumerdata.= "<div>
			
	                <table border=1 align='center'>
                        <tbody>";
												
				$consumerdata.= "<tr>
								<th>S.NO.</th>
								<th>Consumer Connection ID</th> 
								<th>Consumer Name</th> 
								<th>Status</th> 
								<th>Type of ME</th> 
								<th>Activation Date</th> 
								<th>Package Type</th> 
								<th>Pkg. Cost (Old)</th> 
								<th>Package Cost</th> 
								<th>Site Name</th> 
								<th>State Name</th> 
								<th>Upgradation Date</th> 							 
						 </tr>";
					$roleSession = new Zend_Session_Namespace('roles');
					$role_sites_id=$roleSession->site_id;
					$consumers=$consumerMapper->getConsumersWithActDate($month,$year,null,null,$role_sites_id);	 
					$Total_cost=0;$Total_cost_old=0;
					$main_summary_array=array();
					  	foreach ($consumers as $consumer)
					 	{
					 		$sites=$siteMapper->getSiteById($consumer->__get("site_id"));
					 		$state=$stateMapper->getStateById($sites->__get("state_id"));
					 		$package=$packageMapper->getPackageById($consumer->__get("package_id"));
					 		$Package_his=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,$consumer->__get("consumer_id"),"active");
					 		$upgradation="";$old_packageCost="";
							if($Package_his){
					 		if($Package_his[0]->__get("old_package_id")!=NULL){
					 			$oldPackage=$packageMapper->getPackageById($Package_his[0]->__get("old_package_id"));
					 			if($oldPackage){
					 				$packageCost=$Package_his[0]->__get("package_cost");
					 				$old_packageCost=$oldPackage->__get("package_cost");
					 				if($old_packageCost<$packageCost)
					 				{
										$Total_cost_old=$Total_cost_old+$old_packageCost;
					 					$upgradation=$Package_his[0]->__get("package_change_date");
					 				}
					 			}
					 		}}
							$Total_cost=$Total_cost+$package->__get("package_cost");
							if($package->__get("is_postpaid")==2){
								$value='MixMode' ;
							}elseif($package->__get("is_postpaid") ==1){
								$value= 'Postpaid';
							}else{
								$value= 'Prepaid' ;
							}
							$datas=array(
									"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
									"consumer_name"=>$consumer->__get("consumer_name"),
									"consumer_status"=>$consumer->__get("consumer_status"),
									"type_of_me"=>$consumer->__get("type_of_me"),
									"consumer_act_date"=>$consumer->__get("consumer_act_date"),
									"ispostpaid"=>$value,
									"oldPackge"=>$old_packageCost,
									"package_cost"=>$package->__get("package_cost"),
									"site_name"=>$sites->__get("site_name"),
									"state_name"=>$state->__get("state_name"),
									"upgradation"=>$upgradation
							);
							$main_summary_array[]=$datas;
						}
						$Package_changes=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,null,"active");
						foreach ($Package_changes as $Package_change){
							if($Package_change->__get("old_package_id")!=NULL){
								$oldPackage=$packageMapper->getPackageById($Package_change->__get("old_package_id"));
								if($oldPackage){
									$packageCost=$Package_change->__get("package_cost");
									$old_packageCost=$oldPackage->__get("package_cost");
									if($old_packageCost<$packageCost)
									{
										$consumers_detail=$consumerMapper->getConsumerById($Package_change->__get("consumer_id"));
										$sites=$siteMapper->getSiteById($consumers_detail->__get("site_id"));
										$state=$stateMapper->getStateById($sites->__get("state_id"));
						
										$Total_cost=$Total_cost+$packageCost;
										$Total_cost_old=$Total_cost_old+$old_packageCost;
										if($Package_change->__get("is_postpaid")==2){
											$value='MixMode' ;
										}elseif($Package_change->__get("is_postpaid") ==1){
											$value= 'Postpaid';
										}else{
											$value= 'Prepaid' ;
										}
										$datas=array(
												"consumer_connection_id"=>$consumers_detail->__get("consumer_connection_id"),
												"consumer_name"=>$consumers_detail->__get("consumer_name"),
												"consumer_status"=>$consumers_detail->__get("consumer_status"),
												"type_of_me"=>$consumers_detail->__get("type_of_me"),
												"consumer_act_date"=>$consumers_detail->__get("consumer_act_date"),
												"ispostpaid"=>$value,
												"oldPackge"=>$old_packageCost,
												"package_cost"=>$packageCost,
												"site_name"=>$sites->__get("site_name"),
												"state_name"=>$state->__get("state_name"),
												"upgradation"=>$Package_change->__get("package_change_date")
										);
										$main_summary_array[]=$datas;
									}
								}
							}
						}
						$sort_arr = array();
						foreach ($main_summary_array as $key => $row)
						{
							$sort_arr[$key] = $row['state_name'];
						}
						array_multisort($sort_arr, SORT_ASC ,$main_summary_array);
						$sort_arr = array();
						foreach ($main_summary_array as $key => $row)
						{
							$sort_arr[$key] = $row['consumer_act_date'];
						}
						array_multisort($sort_arr, SORT_DESC ,$main_summary_array);
						foreach ($main_summary_array as $main){
							$consumerdata.= "<tr>
											<td>".++$counter."</td>
											<td>".$main["consumer_connection_id"]."</td>
											<td>".$main["consumer_name"]."</td>
											<td>".$main["consumer_status"]."</td>
											<td>".$main["type_of_me"]."</td>
											<td>".$main["consumer_act_date"]."</td>
											<td>".$main["ispostpaid"]."</td>
											<td>".$main["oldPackge"]."</td>
											<td>".$main["package_cost"]."</td>
											<td>".$main["site_name"]."</td>
											<td>".$main["state_name"]."</td>
											<td>".$main["upgradation"]."</td>
									 </tr> ";
						}
						
							$consumerdata.= "<tr>
									<th></th><th></th><th></th><th></th><th></th><th></th><th>Total</th>
									<th>".$Total_cost_old."</th>
									<th>".$Total_cost."</th>";
							$consumerdata.= "</tr>";
						 
							$consumerdata.=  "</tbody> </table>
						</div>";
					 																		
					$newfile = 'Increment of Load in Month '.$month_val.','.$year.'.xlsx';
					$inputFileType = 'Excel2007';
					$tmpfile = tempnam(sys_get_temp_dir(), 'html');
					file_put_contents($tmpfile, $consumerdata);
												
					$objPHPExcel = new PHPExcel();
					$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
					$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
					$objPHPExcel->getActiveSheet()->setTitle('Monthly Active Consumer ');
																										
				break;
				
				case "Monthly_banned_consumer" :
				
					$stateMapper=new Application_Model_StatesMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
					$consumerdata="";
					$StateName_Array=array();
						
					$consumerdata.= "<div>
		
	                <table border=1 align='center'>
                        <tbody>";
				
					$consumerdata.= "<tr>
								<th>S.NO.</th>
								<th>Consumer Connection ID</th>
								<th>Consumer Name</th>
								<th>Status</th>
								<th>Type of ME</th>
								<th>suspension_date</th>
								<th>Package Type</th> 
								<th>Pkg. Cost (Old)</th> 
								<th>Package Cost</th>
								<th>Site Name</th>
								<th>State Name</th>
								<th>Downgrade Date</th>
						 </tr>";
					$consumers=$consumerMapper->getBannedConsumersWithActDate($month,$year,null,null,$role_sites_id);
					$Total_cost=0;$Total_cost_old=0;
					foreach ($consumers as $consumer)
					{
							$sites=$siteMapper->getSiteById($consumer->__get("site_id"));
					 		$state=$stateMapper->getStateById($sites->__get("state_id"));
					 		$package=$packageMapper->getPackageById($consumer->__get("package_id"));
					 		$Package_his=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,$consumer->__get("consumer_id"),"banned");
					 		
					 		$downdation="";$old_packageCost="";
							if($Package_his){
					 		if($Package_his[0]->__get("old_package_id")!=NULL){
					 			$oldPackage=$packageMapper->getPackageById($Package_his[0]->__get("old_package_id"));
					 			if($oldPackage){
					 				$packageCost=$Package_his[0]->__get("package_cost");
					 				$old_packageCost=$oldPackage->__get("package_cost");
					 				if($old_packageCost>$packageCost)
					 				{
										$Total_cost_old=$Total_cost_old+$old_packageCost;
					 					$downdation=$Package_his[0]->__get("package_change_date");
					 				}
					 			}
					 		} }
							$Total_cost=$Total_cost+$package->__get("package_cost");
							
							if($package->__get("is_postpaid")==2){
								$value='MixMode' ;
							}elseif($package->__get("is_postpaid") ==1){
								$value= 'Postpaid';
							}else{
								$value= 'Prepaid' ;
							}
							if($consumer->__get("consumer_status")=='active'){
								$suspension="";
							}else{
								$suspension=$consumer->__get("suspension_date");
							}
							$datas=array(
									"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
									"consumer_name"=>$consumer->__get("consumer_name"),
									"consumer_status"=>$consumer->__get("consumer_status"),
									"type_of_me"=>$consumer->__get("type_of_me"),
									"consumer_act_date"=>$suspension,
									"ispostpaid"=>$value,
									"oldPackge"=>$old_packageCost,
									"package_cost"=>$package->__get("package_cost"),
									"site_name"=>$sites->__get("site_name"),
									"state_name"=>$state->__get("state_name"),
									"upgradation"=>$downdation
							);
							$main_summary_array[]=$datas;
					}
					$Package_changes=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,null,null);
					 if($Package_changes){

					foreach ($Package_changes as $Package_change){
					if($Package_change->__get("old_package_id")!=NULL){
						$oldPackage=$packageMapper->getPackageById($Package_change->__get("old_package_id"));
						if($oldPackage){
							$packageCost=$Package_change->__get("package_cost");
							$old_packageCost=$oldPackage->__get("package_cost");
							if($old_packageCost>$packageCost)
							{
								$consumers_detail=$consumerMapper->getConsumerById($Package_change->__get("consumer_id"));
								$sites=$siteMapper->getSiteById($consumers_detail->__get("site_id"));
								$state=$stateMapper->getStateById($sites->__get("state_id"));
								$Total_cost_old=$Total_cost_old+$old_packageCost;
								$Total_cost=$Total_cost+$packageCost;
						
								if($Package_change->__get("is_postpaid")==2){
									$value='MixMode' ;
								}elseif($Package_change->__get("is_postpaid") ==1){
									$value= 'Postpaid';
								}else{
									$value= 'Prepaid' ;
								}
								if($consumers_detail->__get("consumer_status")=='active'){
									$suspension="";
								}else{
									$suspension=$consumers_detail->__get("suspension_date");
								}
								$datas=array(
										"consumer_connection_id"=>$consumers_detail->__get("consumer_connection_id"),
										"consumer_name"=>$consumers_detail->__get("consumer_name"),
										"consumer_status"=>$consumers_detail->__get("consumer_status"),
										"type_of_me"=>$consumers_detail->__get("type_of_me"),
										"consumer_act_date"=>$suspension,
										"ispostpaid"=>$value,
										"oldPackge"=>$old_packageCost,
										"package_cost"=>$packageCost,
										"site_name"=>$sites->__get("site_name"),
										"state_name"=>$state->__get("state_name"),
										"upgradation"=>$Package_change->__get("package_change_date")
								);
								$main_summary_array[]=$datas;
							}
						}
					}
					
					}
					}
					$sort_arr = array();
					foreach ($main_summary_array as $key => $row)
					{
						$sort_arr[$key] = $row['state_name'];
					}
					array_multisort($sort_arr, SORT_ASC ,$main_summary_array);
					$sort_arr = array();
					foreach ($main_summary_array as $key => $row)
					{
						$sort_arr[$key] = $row['consumer_act_date'];
					}
					array_multisort($sort_arr, SORT_DESC ,$main_summary_array);
					$counter=0;
						foreach ($main_summary_array as $main){
							$consumerdata.= "<tr>
											<td>".++$counter."</td>
											<td>".$main["consumer_connection_id"]."</td>
											<td>".$main["consumer_name"]."</td>
											<td>".$main["consumer_status"]."</td>
											<td>".$main["type_of_me"]."</td>
											<td>".$main["consumer_act_date"]."</td>
											<td>".$main["ispostpaid"]."</td>
											<td>".$main["oldPackge"]."</td>
											<td>".$main["package_cost"]."</td>
											<td>".$main["site_name"]."</td>
											<td>".$main["state_name"]."</td>
											<td>".$main["upgradation"]."</td>
									 </tr> ";
					}
					$consumerdata.= "<tr>
									<th></th><th></th><th></th><th></th><th></th><th></th><th>Total</th>
									<th>".$Total_cost_old."</th>
									<th>".$Total_cost."</th>";
					$consumerdata.= "</tr>";
						
					$consumerdata.=  "</tbody> </table>
						</div>";
					 
					$newfile = 'Short Falls of Load in Month '.$month_val.','.$year.'.xlsx';
					$inputFileType = 'Excel2007';
					$tmpfile = tempnam(sys_get_temp_dir(), 'html');
					file_put_contents($tmpfile, $consumerdata);
				
					$objPHPExcel = new PHPExcel();
					$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
					$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
					$objPHPExcel->getActiveSheet()->setTitle('Monthly Banned Consumer ');
				
					break;

case "new_consumer_statics" :
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
					
			$curr_date = $request->getParam("secondDate");
			$zendDates = new Zend_Date($curr_date,"MMMM D, YYYY");
			$curr_date = $zendDates->toString("yyyy-MM-dd");
			$conPackageMapper=new Application_Model_ConsumersPackageMapper();
													
			$last_date = $request->getParam("firstDate");
			$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
			$last_date = $zendDate->toString("yyyy-MM-dd");
														
			$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
			$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
				
			$cal=(strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24);
            $days = round($cal); 
            $m=0;$y=0;
			
            $counter=round($days/30);
            $next='+1 month';
			
			$trans_date=date_parse_from_format("Y-m-d", $timestamp);
			$day=$trans_date["day"];
			$month= $trans_date["month"];
			$years= $trans_date["year"];
					
			$stateMapper=new Application_Model_StatesMapper();
			$clusterMapper=new Application_Model_ClustersMapper();
			$cashRegisterMapper = new Application_Model_CashRegisterMapper();
			$siteMapper=new Application_Model_SitesMapper();
			$consumerMapper=new Application_Model_ConsumersMapper();
			$packageMapper=new Application_Model_PackagesMapper();
			$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
			$logMapper=new Application_Model_LogsMapper();
			 
			$StateName_Array=array();
			 
			$counters=0;
			$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
			$month_no=['04','05','06','07','08','09','10','11','12','01','02','03'];
			$consumerdata="";
			$states=$stateMapper->getAllStates();
			//$preConsumer=$consumerMapper->getNewConsumerDetails(04,2017,null,2,true,null,'0');
			//$preConsumer=$consumerMapper->getNewConsumerDetails(04,2017,null,2,true,null,'0',null,true);
			$lastyear=$year-1;					
			$consumerdata.= " <h4><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>From ".$last_date." to ".$curr_date."</h4>";
								
			$consumerdata.= "<div>
			
	                <table border=1 align='center'> 
                        <tbody>";
					
						$consumerdata.= "<tr><th>Sites</th>
											 <th>Months</th>";
					
						
						$secondNext=$last_date;
						for($i=0;$i<$counter;$i++){
            		
							$trans_date=date_parse_from_format("Y-m-d", $secondNext);
							$month= $trans_date["month"];
							$year= $trans_date["year"];
							
							$dateObj   = DateTime::createFromFormat('!m', $month);
							$monthName = $dateObj->format('M');
							
							$consumerdata.= " 
									<th>".$monthName.",".$year."</th>
									<th></th>";
							$secondNext = date('Y-m-d', strtotime($secondNext.$next));
						}
						
							$consumerdata.= "</tr><tr><th></th><th></th>";
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$trans_date=date_parse_from_format("Y-m-d", $secondNext);
								$month= $trans_date["month"];
								$year= $trans_date["year"];
								
								$dateObj   = DateTime::createFromFormat('!m', $month);
								$monthName = $dateObj->format('M');
										$consumerdata.= "<th> Pre.p</th>
														 <th>Post.p</th>";
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}
					
						$consumerdata.= "</tr>"; 
					foreach ($states as $state){
						if(in_array($state->__get("state_id"), $state_id)){
					
							$site_arr_val=array();
							$sites=$siteMapper->getSiteByStateId($state->__get("state_id"));
							foreach ($sites as $site)
							{ 
								if(in_array($site->__get("site_id"), $site_id) || $site_id==null){
									$site_arr_val[]=$site->__get("site_id");
								}
							}
							 
						$consumerdata.= "<tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>".$state->__get("state_name")."</th></tr>";
							$consumerdata.= "<tr><th>Opening Customer</th><th></th>";
							//$consumerdata.= "</tr><tr><th></th><th></th>";
							
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$trans_date=date_parse_from_format("Y-m-d", $secondNext);
								$month= $trans_date["month"];
								$year= $trans_date["year"];
								
								$dateObj   = DateTime::createFromFormat('!m', $month);
								$monthName = $dateObj->format('M');
									 
								$preConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'0','active','start');
								$postConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'1','active','start');
						
								//$prebannedConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'0','banned','start');
								//$postbannedConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'1','banned','start');
					   		
							//$totalpre=$preConsumer+$prebannedConsumer;
							//$totalpost=$postConsumer+$postbannedConsumer;
								$consumerdata.= "<th>".$preConsumer."</th>
									<th>".$postConsumer."</th>";
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}
							$consumerdata.= "</tr><tr><th>Target for the month</th><th></th>";
							
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$trans_date=date_parse_from_format("Y-m-d", $secondNext);
								$month= $trans_date["month"];
								$year= $trans_date["year"];
								 
									$consumerdata.= "<th> </th>
									<th> </th>";
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}
							$consumerdata.= "</tr><tr><th>New Customer Added</th><th></th>";
							
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$trans_date=date_parse_from_format("Y-m-d", $secondNext);
								$month= $trans_date["month"];
								$year= $trans_date["year"];
							
								$preConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'0','activecur');
								$postConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'1','activecur');
								
								//$prebannedConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'0','bannedcur');
								//$postbannedConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'1','bannedcur');
								
								//$totalpre=$preConsumer+$prebannedConsumer;
								//$totalpost=$postConsumer+$postbannedConsumer;
									$consumerdata.= "<th>".$preConsumer."</th>
										<th>".$postConsumer."</th>";
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}
							
							$consumerdata.= "</tr>";
						
							$Allsites=array();
							$sites=$siteMapper->getSiteByStateId($state->__get("state_id"));
							if($site_id!=NULL){
								foreach ($sites as $site)
								{ 
									if(in_array($site->__get("site_id"), $site_id)){
										$Allsites[]=$site;
									}
								}
							}else{
								$Allsites=$sites;
							}	
						
						foreach ($Allsites as $site)
						{
							if($site->__get("site_id")!=1){
							
							$consumerdata.= "<tr><th>".$site->__get("site_name")."</th><th></th>";
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$trans_date=date_parse_from_format("Y-m-d", $secondNext);
								$month= $trans_date["month"];
								$year= $trans_date["year"];
								
								$preConsumer=$consumerMapper->getNewConsumerDetails($month,$year,array($site->__get("site_id")),null,'0','activecur');
								$postConsumer=$consumerMapper->getNewConsumerDetails($month,$year,array($site->__get("site_id")),null,'1','activecur');
								
								//$prebannedConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,$site->__get("site_id"),'0','bannedcur');
								//$postbannedConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,$site->__get("site_id"),'1','bannedcur');
									
								//$preConsumer1=$preConsumer+$prebannedConsumer;
								//$postConsumer1=$postConsumer+$postbannedConsumer;
									
								$consumerdata.= "<th>".$preConsumer."  </th>
											<th> ".$postConsumer."</th>";
															 
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}
							
							 $consumerdata.= "</tr>";
						  }
					    }
						   $consumerdata.= "<tr><th>Disconnections in Month</th><th></th>";
						   
						   /*$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$trans_date=date_parse_from_format("Y-m-d", $secondNext);
								$month= $trans_date["month"];
								$year= $trans_date["year"];
								
								$prebannedConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'0','bannedcur');
								$postbannedConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'1','bannedcur');
					   		
							
								$consumerdata.= "<th>".$prebannedConsumer."  </th>
											<th> ".$postbannedConsumer."</th>";
							
					   		$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}*/
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
								
								$reactives=$logMapper->getConsumerBannedLogsBydate($secondCurr,$secondNext); 
						
								$reactive_count_pre=0;$reactive_count_post=0;
								if($reactives){
									foreach ($reactives as $reactives){
										$arr=explode("Consumer ID: ", $reactives["message"]);
										$connection=explode(" ",$arr[1]);
										$connection_id=$connection[0];
										$site_arr=implode(",",$site_arr_val);
										$checkReactive=$consumerMapper->checkConnectionId($site_arr,null,null,$connection_id);
										if($checkReactive){
											if($checkReactive["type"]==0){
												$reactive_count_pre=$reactive_count_pre+1;
											}else{
												$reactive_count_post=$reactive_count_post+1;
											}
										}
									}
								}
								
							 	$consumerdata.= "<th>".$reactive_count_pre."</th>
												 <th>".$reactive_count_post."</th>";
							
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}
							
							$consumerdata.= "</tr>";
							foreach ($Allsites as $site)
							{
								
								if($site->__get("site_id")!=1){
						
								$consumerdata.= "<tr><th>".$site->__get("site_name")."</th><th></th>";
							
								$secondNext=$last_date;
								for($i=0;$i<$counter;$i++){
									$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
									
									$reactives=$logMapper->getConsumerBannedLogsBydate($secondCurr,$secondNext); 
							
									$reactive_count_pre=0;$reactive_count_post=0;
									if($reactives){
										foreach ($reactives as $reactives){
											$arr=explode("Consumer ID: ", $reactives["message"]);
											$connection=explode(" ",$arr[1]);
											$connection_id=$connection[0];
											
											$checkReactive=$consumerMapper->checkConnectionId($site->__get("site_id"),null,null,$connection_id);
											if($checkReactive){
												if($checkReactive["type"]==0){
													$reactive_count_pre=$reactive_count_pre+1;
												}else{
													$reactive_count_post=$reactive_count_post+1;
												}
											}
										}
									}
									
									$consumerdata.= "<th>".$reactive_count_pre."</th>
													 <th>".$reactive_count_post."</th>";
								
									$secondNext = date('Y-m-d', strtotime($secondNext.$next));
								}
							
								$consumerdata.= "</tr>";
							 
								}
							}
					   		
							$consumerdata.= "<tr><th>Re-active in Month</th><th></th>";
						   
						    $secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
								
								$reactives=$logMapper->getConsumerReactiveLogsBydate($secondCurr,$secondNext); 
						
								$reactive_count_pre=0;$reactive_count_post=0;
								if($reactives){
									foreach ($reactives as $reactives){
										$arr=explode("Consumer ID: ", $reactives["message"]);
										$connection=explode(" ",$arr[1]);
										$connection_id=$connection[0];
										$site_arr=implode(",",$site_arr_val);
										$checkReactive=$consumerMapper->checkConnectionId($site_arr,null,null,$connection_id);
										if($checkReactive){
											if($checkReactive["type"]==0){
												$reactive_count_pre=$reactive_count_pre+1;
											}else{
												$reactive_count_post=$reactive_count_post+1;
											}
										}
									}
								}
								
							 	$consumerdata.= "<th>".$reactive_count_pre."</th>
												 <th>".$reactive_count_post."</th>";
							
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}
							
							$consumerdata.= "</tr>";
							foreach ($Allsites as $site)
							{
								
								if($site->__get("site_id")!=1){
						
								$consumerdata.= "<tr><th>".$site->__get("site_name")."</th><th></th>";
							
								$secondNext=$last_date;
								for($i=0;$i<$counter;$i++){
									$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
									
									$reactives=$logMapper->getConsumerReactiveLogsBydate($secondCurr,$secondNext); 
							
									$reactive_count_pre=0;$reactive_count_post=0;
									if($reactives){
										foreach ($reactives as $reactives){
											$arr=explode("Consumer ID: ", $reactives["message"]);
											$connection=explode(" ",$arr[1]);
											$connection_id=$connection[0];
											
											$checkReactive=$consumerMapper->checkConnectionId($site->__get("site_id"),null,null,$connection_id);
											if($checkReactive){
												if($checkReactive["type"]==0){
													$reactive_count_pre=$reactive_count_pre+1;
												}else{
													$reactive_count_post=$reactive_count_post+1;
												}
											}
										}
									}
									
									$consumerdata.= "<th>".$reactive_count_pre."</th>
													 <th>".$reactive_count_post."</th>";
								
									$secondNext = date('Y-m-d', strtotime($secondNext.$next));
								}
							
								$consumerdata.= "</tr>";
							 
								}
							}
							
					   		$consumerdata.= "<tr><th>Net Add/Omition in Month</th><th></th>";
					   			$secondNext=$last_date;
								for($i=0;$i<$counter;$i++){
									$trans_date=date_parse_from_format("Y-m-d", $secondNext);
									$month= $trans_date["month"];
									$year= $trans_date["year"];
					   				
					   				$preConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'0','activecur');
									$postConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'1','activecur');
									
									//$prebannedConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'0','bannedcur');
									//$postbannedConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'1','bannedcur');
									 
									$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
									
									$reactives=$logMapper->getConsumerBannedLogsBydate($secondCurr,$secondNext); 
							
									$prebannedConsumer=0;$postbannedConsumer=0;
									if($reactives){
										foreach ($reactives as $reactives){
											$arr=explode("Consumer ID: ", $reactives["message"]);
											$connection=explode(" ",$arr[1]);
											$connection_id=$connection[0];
											$site_arr=implode(",",$site_arr_val);
											$checkReactive=$consumerMapper->checkConnectionId($site_arr,null,null,$connection_id);
											if($checkReactive){
												if($checkReactive["type"]==0){
													$prebannedConsumer=$prebannedConsumer+1;
												}else{
													$postbannedConsumer=$postbannedConsumer+1;
												}
											}
										}
									}
								
							   
									$preConsumer1=$preConsumer-$prebannedConsumer;
									$postConsumer1=$postConsumer-$postbannedConsumer;
									
					   				 	$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
										
										$reactives=$logMapper->getConsumerReactiveLogsBydate($secondCurr,$secondNext); 
								
										$reactive_count_pre=0;$reactive_count_post=0;
										if($reactives){
											foreach ($reactives as $reactives){
												$arr=explode("Consumer ID: ", $reactives["message"]);
												$connection=explode(" ",$arr[1]);
												$connection_id=$connection[0];
												$site_arr=implode(",",$site_arr_val);
												$checkReactive=$consumerMapper->checkConnectionId($site_arr,null,null,$connection_id);
												if($checkReactive){
													if($checkReactive["type"]==0){
														$reactive_count_pre=$reactive_count_pre+1;
													}else{
														$reactive_count_post=$reactive_count_post+1;
													}
												}
											}
										}
									
										/*$consumerdata.= "<th>".$reactive_count_pre."</th>
													 <th>".$reactive_count_post."</th>";*/ 
										
									 	$consumerdata.= "<th>".($preConsumer1+$reactive_count_pre)." </th>
					   									<th>".($postConsumer1+$reactive_count_post)."</th>";
									$secondNext = date('Y-m-d', strtotime($secondNext.$next));
								}
					   			$consumerdata.= "</tr>";
								$consumerdata.= "<tr><th>Closing Customer</th><th></th>";
								
								$secondNext=$last_date;
								for($i=0;$i<$counter;$i++){
									$trans_date=date_parse_from_format("Y-m-d", $secondNext);
									$month= $trans_date["month"];
									$year= $trans_date["year"];
									
									$preConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'0','active','end');
									$postConsumer=$consumerMapper->getNewConsumerDetails($month,$year,$site_arr_val,null,'1','active','end');
									  
									//$totalpre=$preConsumer+$prebannedConsumer;
									//$totalpost=$postConsumer+$postbannedConsumer;
									$consumerdata.= "<th>".$preConsumer."</th>
											<th>".$postConsumer."</th>";
									$secondNext = date('Y-m-d', strtotime($secondNext.$next));
								}
								 
								$consumerdata.= "</tr>";
					}
				}
				//echo $consumerdata;exit;			 	
					$consumerdata.= "<tr></tr><tr><th></th><th></th>";	
						$secondNext=$last_date;
						for($i=0;$i<$counter;$i++){
							
							$trans_date=date_parse_from_format("Y-m-d", $secondNext);
							$month= $trans_date["month"];
							$year= $trans_date["year"];
							
							$dateObj   = DateTime::createFromFormat('!m', $month);
							$monthName = $dateObj->format('M');
							
							$consumerdata.= " 
									<th></th><th>".$monthName.",".$year."</th>
									<th></th>";
							$secondNext = date('Y-m-d', strtotime($secondNext.$next));
						}
						
					$consumerdata.= "</tr><tr><th>Sites</th><th></th>";
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								 
								$monthName = $dateObj->format('M');
										$consumerdata.= "<th>Upgrade</th>
														 <th>Downgrade</th>
														 <th>Re-active</th>";
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
							}
						 
						$Allsites=array();
						$roleSession = new Zend_Session_Namespace('roles');
						$role_sites_id=$roleSession->site_id;
						$sites=$siteMapper->getAllSites(null,null,$role_sites_id);
						if($site_id!=NULL){
							foreach ($sites as $site)
							{ 
								if(in_array($site->__get("site_id"), $site_id)){
									$Allsites[]=$site;
								}
							}
						}else{
							$Allsites=$sites;
						}
						$sites_array=array();
						foreach($Allsites as $site){
							if($site->__get("site_id")!=1){ 
							$sites_array[]=$site->__get("site_id");
							$consumerdata.= "<tr><th>".$site->__get("site_name")."</th><th></th>";
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$secondCurr = date('Y-m-d', strtotime($secondNext.$next));	
								$upgrade=0;$downgrade=0;$reactive=0;
								$Consumers_status=$packageHistoryMapper->getMonthlypackageHistoryStatus($secondCurr,$secondNext,$site->__get("site_id"));
					  
								 if($Consumers_status){
									foreach ($Consumers_status as $Consumer_status){
										if($Consumer_status["ph_package_id"]=="" || $Consumer_status["ph_package_id"]==NULL || $Consumer_status["ph_package_id"]=='NULL'){
											$ph_package_id=0;
										}else{
											$ph_package_id=$Consumer_status["ph_package_id"];
										}
										
										$lastPackage=$packageMapper->getPackageById($ph_package_id);
										if($lastPackage){
											$lastPackage_cost=$lastPackage->__get("package_cost");
										}else{
											$lastPackage_cost=0;
										}
										if($Consumer_status["last_package"]=="" || $Consumer_status["last_package"]==NULL || $Consumer_status["ph_package_id"]=='NULL' ){
											$last_package=0;
										}else{
											$last_package=$Consumer_status["last_package"];
										}
										 
										$newPackage=$packageMapper->getPackageById($last_package);
										
										if($newPackage){
											$newPackage_cost=$newPackage->__get("package_cost");
										}else{
											$newPackage_cost=0;
										}
										if($lastPackage_cost>$newPackage_cost){
											$upgrade=$upgrade+1;
										}elseif($lastPackage_cost<$newPackage_cost){
											$downgrade=$downgrade+1;
										}  
									}
								}
								$reactives=$logMapper->getConsumerReactiveLogsBydate($secondCurr,$secondNext); 
						
								$reactive_count=0;
								if($reactives){
									foreach ($reactives as $reactives){
										$arr=explode("Consumer ID: ", $reactives["message"]);
										$connection=explode(" ",$arr[1]);
										$connection_id=$connection[0];
										$checkReactive=$consumerMapper->checkConnectionId($site->__get("site_id"),null,null,$connection_id);
										if($checkReactive){
											$reactive_count=$reactive_count+1;
										}
									}
								}
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
								$consumerdata.= "<th>".$upgrade."</th><th>".$downgrade."</th><th>".$reactive_count."</th>";
							}
							}
						}
						 
							$consumerdata.= "<tr><th>Total</th><th></th>";
							$secondNext=$last_date;
							for($i=0;$i<$counter;$i++){
								$secondCurr = date('Y-m-d', strtotime($secondNext.$next));	
								$upgrade=0;$downgrade=0;$reactive=0;
								$Consumers_status=$packageHistoryMapper->getMonthlypackageHistoryStatus($secondCurr,$secondNext,null,null,$sites_array);
					  
								 if($Consumers_status){
									foreach ($Consumers_status as $Consumer_status){
										if($Consumer_status["ph_package_id"]=="" || $Consumer_status["ph_package_id"]==NULL || $Consumer_status["ph_package_id"]=='NULL'){
											$ph_package_id=0;
										}else{
											$ph_package_id=$Consumer_status["ph_package_id"];
										}
										
										$lastPackage=$packageMapper->getPackageById($ph_package_id);
										if($lastPackage){
											$lastPackage_cost=$lastPackage->__get("package_cost");
										}else{
											$lastPackage_cost=0;
										}
										if($Consumer_status["last_package"]=="" || $Consumer_status["last_package"]==NULL || $Consumer_status["ph_package_id"]=='NULL' ){
											$last_package=0;
										}else{
											$last_package=$Consumer_status["last_package"];
										}
										 
										$newPackage=$packageMapper->getPackageById($last_package);
										
										if($newPackage){
											$newPackage_cost=$newPackage->__get("package_cost");
										}else{
											$newPackage_cost=0;
										}
										if($lastPackage_cost>$newPackage_cost){
											$upgrade=$upgrade+1;
										}elseif($lastPackage_cost<$newPackage_cost){
											$downgrade=$downgrade+1;
										}  
									}
								}
								$reactives=$logMapper->getConsumerReactiveLogsBydate($secondCurr,$secondNext); 
						
								$reactive_count=0;
								if($reactives){
									foreach ($reactives as $reactives){
										$arr=explode("Consumer ID: ", $reactives["message"]);
										$connection=explode(" ",$arr[1]);
										$connection_id=$connection[0];
										$checkReactive=$consumerMapper->checkConnectionId(null,null,$sites_array,$connection_id);
										if($checkReactive){
											$reactive_count=$reactive_count+1;
										}
									}
								}
								$secondNext = date('Y-m-d', strtotime($secondNext.$next));
								$consumerdata.= "<th>".$upgrade."</th><th>".$downgrade."</th><th>".$reactive_count."</th>";
							 
							}
						$consumerdata.= "</tr>"; 
						
				
				$consumerdata.=  "</tbody> </table></div>";
				 	 	 	 					
						$newfile = 'New Consumer Statistics for '.$year.'.xlsx';
						$inputFileType = 'Excel2007';
						$tmpfile = tempnam(sys_get_temp_dir(), 'html');
						file_put_contents($tmpfile, $consumerdata);
					
						$objPHPExcel = new PHPExcel();
						$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
						$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('New Consumer Statics');
						break;
						
			case "mixed_mode_report" :
				
					$stateMapper=new Application_Model_StatesMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
					$consumerdata="";
					$StateName_Array=array();
					$zendDate = new Zend_Date(); 
					//$zendDate =  new Zend_Date('2017-02-28', 'yyyy-MM-dd'); 
		   			$zendDate->setTimezone("Asia/Calcutta");
					 		
		   				$day= intval($zendDate->toString("dd"));
						   $month=intval( $zendDate->toString("MM"));
		   				$year= intval($zendDate->toString("yyyy"));	
					$consumerdata.= "<div>
		
	                <table border=1 align='center'>
                        <tbody>";
				
					$consumerdata.= "<tr>
										<th>Consumer ID</th>
										<th>Consumer Connection ID</th>
										<th>MM Debit Amt</th>
										<th>Last package Amt</th>
						           </tr>";
					$consumers=$cashRegisterMapper->getMixedModeEntry("mm",$day,$month,$year);
					echo count($consumers);
					$Total_cost_mm=0; $Total_cost_old=0;
					if($consumers){
						foreach ($consumers as $consumer)
						{
								$con=$consumerMapper->getConsumerById($consumer["consumer_id"]);
								$consumers_val=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,$consumer["consumer_id"]);
								if($consumers_val){
	                  				if($consumers_val[0]->__get("old_package_id")!=NULL){
											$oldPackage=$packageMapper->getPackageById($consumers_val[0]->__get("old_package_id"));
											$oldPack=$oldPackage->__get("package_cost");
									}else{
										$oldPack=null;
									}
								}else{
										$oldPack=null;
									}	
								$datas=array(
										"consumer_id"=>$consumer["consumer_id"],
										"consumer_connection_id"=>$con->__get("consumer_connection_id"),
										"cr_amount"=>$consumer["cr_amount"],
										"oldpack"=>$oldPack
								);
								$Total_cost_mm=$Total_cost_mm+$consumer["cr_amount"];
								$Total_cost_old=$Total_cost_old+$oldPack;
								$main_summary_array[]=$datas;
						}
						$sort_arr = array();
						foreach ($main_summary_array as $key => $row)
						{
							$sort_arr[$key] = $row['cr_amount'];
						}
						array_multisort($sort_arr, SORT_DESC ,$main_summary_array); 
						
						foreach ($main_summary_array as $main){
							$consumerdata.= "<tr><td>".$main["consumer_id"]."</td>
												 <td>".$main["consumer_connection_id"]."</td>
												 <td>".$main["cr_amount"]."</td>
												 <td>".$main["oldpack"]."</td>
											</tr> ";
						}
						$consumerdata.= "<tr>
										<th>Total</th>
										<th>".count($main_summary_array)."</th>
										<th>".$Total_cost_mm."</th>
										<th>".$Total_cost_old."</th>";
						$consumerdata.= "</tr>";
					}
					$consumerdata.=  "</tbody> </table>
						</div>";
					//echo $consumerdata;exit; 
					$newfile = 'Mixed Mode Consumers Report for '.date('d M,Y h-i A').'.xlsx';
					$inputFileType = 'Excel2007'; 
					$tmpfile = tempnam(sys_get_temp_dir(), 'html');
					file_put_contents($tmpfile, $consumerdata);
				
					$objPHPExcel = new PHPExcel();
					$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
					$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
					$objPHPExcel->getActiveSheet()->setTitle('Mixed Mode Consumers');
				
					break; 

		case "bill_reduce_report" :
				
					$stateMapper=new Application_Model_StatesMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
					$consumerdata="";
					$StateName_Array=array();
					$zendDate = new Zend_Date(); 
					//$zendDate =  /*new Zend_Date();//*/ new Zend_Date('2017-02-28', 'yyyy-MM-dd');
		   			$zendDate->setTimezone("Asia/Calcutta");
					 		
		   				$day= intval($zendDate->toString("dd"));
						   $month=intval( $zendDate->toString("MM"));
		   				$year= intval($zendDate->toString("yyyy"));	
					$consumerdata.= "<div>
		
	                <table border=1 align='center'>
                        <tbody>";
				
					$consumerdata.= "<tr>
										<th>Consumer ID</th>
										<th>Consumer Connection ID</th>
										<th>Old debit amt</th>
										<th>Update amt</th>
						           </tr>";
					$consumers=$cashRegisterMapper->getMixedModeEntry("bill_reduce",$day,$month,$year);
					$Total_cost_mm=0; $Total_cost_bil=0;
					if($consumers){
						foreach ($consumers as $consumer)
						{
								$con=$consumerMapper->getConsumerById($consumer["consumer_id"]);
								
								$datas=array(
										"consumer_id"=>$consumer["consumer_id"],
										"consumer_connection_id"=>$con->__get("consumer_connection_id"),
										"amt_prompt"=>$consumer["amt_prompt"],
										"cr_amount"=>$consumer["cr_amount"],
										
								);
								$Total_cost_mm=$Total_cost_mm+$consumer["amt_prompt"];
								$Total_cost_bil=$Total_cost_bil+$consumer["cr_amount"];
								$main_summary_array[]=$datas;
						}
						$sort_arr = array();
						foreach ($main_summary_array as $key => $row)
						{
							$sort_arr[$key] = $row['cr_amount'];
						}
						array_multisort($sort_arr, SORT_DESC ,$main_summary_array); 
						
						foreach ($main_summary_array as $main){
							$consumerdata.= "<tr><td>".$main["consumer_id"]."</td>
												 <td>".$main["consumer_connection_id"]."</td>
												 <td>".$main["amt_prompt"]."</td>
												 <td>".$main["cr_amount"]."</td>
												 
											</tr> ";
						}
						$consumerdata.= "<tr>
										<th>Total</th>
										<th>".count($main_summary_array)."</th>
										<th>".$Total_cost_mm."</th>
										<th>".$Total_cost_bil."</th>";
						$consumerdata.= "</tr>";
					}	
					$consumerdata.=  "</tbody> </table>
						</div>";
					 //echo $consumerdata;exit;
					$newfile = 'Bill Reduction Report for '.date('d M,Y h-i A').'.xlsx';
					$inputFileType = 'Excel2007';
					$tmpfile = tempnam(sys_get_temp_dir(), 'html');
					file_put_contents($tmpfile, $consumerdata);    
				
					$objPHPExcel = new PHPExcel();
					$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
					$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
					$objPHPExcel->getActiveSheet()->setTitle('Bill Reduction Consumers');
				
					break;
					
			case "consumer_status_report" :
					
						$state_id = $request->getParam("state_id");
						$site_id = $request->getParam("site_id");
						$package_id = $request->getParam("package_id");
						$pdfFile= $request->getParam("PDF");
						
						$state_id=($state_id=="ALL")?NULL:$state_id;
						$site_id=($site_id=="ALL")?NULL:$site_id;
						$package_id=($package_id=="ALL")?NULL:$package_id;
						$pdf=($pdfFile=="PDF")?true:false;
						
						$logMapper = new Application_Model_LogsMapper();
						$consumerMapper=new Application_Model_ConsumersMapper();
						$consumerdata="";
						$Logs_conn=$logMapper->getAllLogsbyConnectionID(null,$month,$year);
						if($Logs_conn){
						foreach ($Logs_conn as $logCon){
							$connection_id=explode("Consumer ID: ", $logCon["message"]);
							$connectionIDs=explode(" ", $connection_id[1]);
							$Conn_id[]=$connectionIDs[0];
						} 
						$Conn_ids = array_unique($Conn_id);
						$ConnIDs = array_values($Conn_ids);
						}else{ $ConnIDs=array();}
						$consumerdata.= "<div>
					
	                <table border=1 align='center'>
                        <tbody>";
					
						$consumerdata.= "<tr>
										<th>Consumer Name</th>
										<th>Consumer Connection ID</th>
										<th>Timestamp</th>
										<th>Summary</th>
						           </tr>";
						 
						$Total_cost_mm=0; $Total_cost_old=0;
						if(count($ConnIDs)>0){
							for($j=0;$j<count($ConnIDs);$j++)
							{
								$Logs=$logMapper->getAllLogsbyConnectionID(true,$month,$year,$ConnIDs[$j]);
								if($Logs) {
									$consumer=$consumerMapper->geAllConsumerStatusDetails($state_id,$site_id,$ConnIDs[$j],$package_id);
									if($consumer){
										$consumerdata.= "<tr>
												<th>".$consumer["consumer_name"]."</th>
												<th>".$consumer["consumer_connection_id"]."</th>";
										$i=0;
										foreach ($Logs as $log){
										$i++;
										if($i==1){
											$consumerdata.="<th>".$log["timestamp"]."</th>
															<th>".$log["message"]."</th></tr>";
										}else{
											$consumerdata.="<tr><th> </th>
															<th> </th>
															<th>".$log["timestamp"]."</th>
															<th>".$log["message"]."</th></tr>";
											}
										}
									}
							   } 
							}
						}
						 
						$consumerdata.=  "</tbody> </table>
						</div>";
					 
						$newfile = 'Consumers Status Report for '.$month.','.$year.'.xlsx';
						if($pdf){
							$newfile = 'Consumers Status Report for '.$month.','.$year.'.pdf';
							$files=(string)$newfile;
							$checkPdf=$this->pdfGenerateAction($consumerdata,$files);
							$this->sendMailAction($files);
						Exit;
						}
						$inputFileType = 'Excel2007';
						$tmpfile = tempnam(sys_get_temp_dir(), 'html');
						file_put_contents($tmpfile, $consumerdata);
					
						$objPHPExcel = new PHPExcel();
						$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
						$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('Consumers Status Report');
					
						break;
						
			case "daily_transaction_report" :
						
							$stateMapper=new Application_Model_StatesMapper();
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							
							$consumerdata="";
							$StateName_Array=array();
							$zendDate = new Zend_Date();
							$zendDate->setTimezone("Asia/Calcutta");
						
							$day= intval($zendDate->toString("dd"));
							$month=intval( $zendDate->toString("MM"));
							$year= intval($zendDate->toString("yyyy"));
							$states=$stateMapper->getAllStates();
							
							$consumerdata.= "<div>
						
	                <table border=1 align='center'>
                        <tbody>";
						
							$consumerdata.= "<tr>
										<th>State Name</th>
										<th>Site Name</th>
										<th>Consumer Connection ID</th>
										<th>Transaction Id</th>
										<th>Entry Type</th>
										<th>Amount</th>
										<th>Transaction By</th>
										<th>Timestamp</th>
										 
						           </tr>";
							
							if($states){
								foreach ($states as $state)
								{
									$cashRegisters=$cashRegisterMapper->getAllTransactionType($state->__get("state_id"));
									if($cashRegisters){
										foreach ($cashRegisters as $cashRegisters){
											if($cashRegisters['transaction_type']=="(M)"){
												$type="Mobile";
											}else{
												$type="SMS";
											}
										$consumerdata.= "<tr>
											<td>".$state->__get("state_name")."</td>
											<td>".$cashRegisters['site_name']."</td>
											<td>".$cashRegisters['consumer_connection_id']."</td>
											<td>".$cashRegisters['transaction_id']."</td>
											<td>".$cashRegisters['cr_entry_type']."</td>
											<td>".$cashRegisters['cr_amount']."</td>
											<td>".$type."</td>
											<td>".$cashRegisters['timestamp']."</td>";
										$consumerdata.= "</tr>";
										}
									}
								}
								 
							}
							$consumerdata.=  "</tbody> </table>
						</div>";
							 
							$newfile = 'Daily transaction Report for '.$month.','.$year.'.xlsx';
							$inputFileType = 'Excel2007';
							$tmpfile = tempnam(sys_get_temp_dir(), 'html');
							file_put_contents($tmpfile, $consumerdata);
						
							$objPHPExcel = new PHPExcel();
							$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
							$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
							$objPHPExcel->getActiveSheet()->setTitle('Daily transaction Report');
						
							break;
							
			case "daily_consumption_report" :
							
								$stateMapper=new Application_Model_StatesMapper();
								$cashRegisterMapper = new Application_Model_CashRegisterMapper();
								$mpptMapper=new Application_Model_MpptsMapper();
								$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
								$sitesMapper=new Application_Model_SitesMapper();
								$consumerdata="";
								$StateName_Array=array();
								$zendDate = new Zend_Date();
								$zendDate->setTimezone("Asia/Calcutta");
							
								$day= intval($zendDate->toString("dd"));
								$month=intval( $zendDate->toString("MM"));
								$year= intval($zendDate->toString("yyyy"));
								$states=$stateMapper->getAllStates();
									
								$first=$year."-".$month."-01 00:00:00";
								$second=$year."-".$month."-".$day." 23:59:59";
								$consumerdata.= "<div>
							
	                <table border=1>
                        <tbody>";
							
								$consumerdata.= "<tr>
										<th>State Name</th>
										<th>Site Name</th>
										<th>MPPT1</th>
										<th>MPPT2</th>
										<th>MPPT3</th>
										<th>MPPT4</th>
										<th>MPPT5</th>
										<th>MPPT6</th>
										<th>MPPT7</th>
										<th>MPPT8</th>
										<th>Timestamp</th>
						
						           </tr>";
								if($states){
									foreach ($states as $state)
									{
										$sites=$sitesMapper->getSitesByStateId($state->__get("state_id"));
										if($sites){
											foreach ($sites as $site)
											{
												$mppts=$mpptMapper->getMpptsBySiteId($site->__get("site_id"));
												if($mppts){
													foreach ($mppts as $mppt){
														$site_id=$site->__get("site_id");
														$mppt_id=$mppt->__get("id");
														$mppt_name=$mppt->__get("mppt_name");
														$mpptReadings=$mpptReadingMapper->getMpptDataByMpptId($mppt_id,$site_id,$first,$second);
														if($mpptReadings){
															foreach ($mpptReadings as $mpptReading){
																$consumerdata.= "<tr>
																				 <td>".$state->__get("state_name")."</td>
																				 <td>".$site->__get("site_name")."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 1')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 2')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 3')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 4')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 5')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 6')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 7')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 8')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".$mpptReading['timestamp']."</td>";
																$consumerdata.= "</tr>";
															}
														}
													}
												}
											}
									}
									}
								}
								$consumerdata.=  "</tbody> </table>
						</div>";
								 
								$newfile = 'Daily Consumption Report for '.$month.','.$year.'.xlsx';
								$inputFileType = 'Excel2007';
								$tmpfile = tempnam(sys_get_temp_dir(), 'html');
								file_put_contents($tmpfile, $consumerdata);
							
								$objPHPExcel = new PHPExcel();
								$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
								$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
								$objPHPExcel->getActiveSheet()->setTitle('Daily Consumption Report');
							
								break;
								
				case "transaction_report" :
						
							$stateMapper=new Application_Model_StatesMapper();
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$logMapper=new Application_Model_LogsMapper();
							$packageMapper=new Application_Model_PackagesMapper();
							$siteMapper=new Application_Model_SitesMapper();
							$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
							$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
							$userMapper=new Application_Model_UsersMapper();
							$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
							$currDate = $request->getParam("secondDate");
							$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
							$currDate = $zendDates->toString("yyyy-MM-dd");

							$lastDate = $request->getParam("firstDate");
							$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
							$lastDate = $zendDate->toString("yyyy-MM-dd");
											
							$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
							$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
							$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
							$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
							$status = ($request->getParam("status")=="" || $request->getParam("status")=='undefined' )?NULL: explode(",", $request->getParam("status"));
								
							$collection_agent = ($request->getParam("collection_agent")=="" || $request->getParam("collection_agent")=='undefined' )?NULL: explode(",", $request->getParam("collection_agent"));
							$type = ($request->getParam("types")=="" || $request->getParam("types")=='undefined' )?NULL: explode(",", $request->getParam("types"));

							$outstanding = ($request->getParam("outstanding")=="" || $request->getParam("outstanding")=='undefined' )?NULL:  $request->getParam("outstanding");
							$not_paid = ($request->getParam("not_paid")=="" || $request->getParam("type")=='not_paid' )?NULL: $request->getParam("not_paid");
							
							$roleSession = new Zend_Session_Namespace('roles');
							$role_sites_id=$roleSession->site_id;
			
							$consumerdata.= "<div>
						
	                <table border=1 align='center'>
                        <tbody>";
							if(count($status)>0){
								 if(in_array("'INACTIVE'", $status) || in_array("'PENDING'", $status)){
									$cashRegisters_pending=$cashRegisterMapper->getAllTransactionTypeForPending($state_id,$site_id,$package_type,$package_id,$status,$collection_agent,$type,$outstanding,$not_paid,$currDate,$lastDate);
								 }else{
									$cashRegisters_pending=FALSE;
								 }
								
								 if(in_array("'ACTIVE'", $status)){
									$cashRegisters=$cashRegisterMapper->getAllTransactionType($state_id,$site_id,$package_type,$package_id,$status,$collection_agent,$type,$currDate,$lastDate,$role_sites_id);
								
								 }else{
									$cashRegisters=FALSE; 
								 }
							 }else{
									$status=array("'INACTIVE'","'ACTIVE'","'PENDING'");
									$cashRegisters_pending=$cashRegisterMapper->getAllTransactionTypeForPending($state_id,$site_id,$package_type,$package_id,$status,$collection_agent,$type,$outstanding,$not_paid,$currDate,$lastDate);
									$cashRegisters=$cashRegisterMapper->getAllTransactionType($state_id,$site_id,$package_type,$package_id,$status,$collection_agent,$type,$currDate,$lastDate,$role_sites_id);
								
							 }
							$consumerdata.= "<tr>
										<th>State Name</th>
										<th>Site Name</th>
										<th>Consumer Connection ID</th>
										<th>Consumer Name</th>
										<th>Activation Date</th>
										<th>Consumer Status</th>
									    <th>Transaction Id</th>
										<th>Entry Type</th>
										<th>Amount</th>
										<th>Transaction By</th>
										<th>Transaction Date</th>
										<th>Transaction Time</th>
										<th>Transaction Status</th>
										<th>Type</th>
										<th>Collection Agent</th>
										<th>Approve By</th>
										<th>Approve Date</th>
						           </tr>";
							 
							if($cashRegisters){
								foreach ($cashRegisters as $cashRegister)
								{
									$state=$stateMapper->getStateById($cashRegister["state_id"]);
									 
											if($cashRegister['transaction_type']=="(M)" || $cashRegister['transaction_type']=="(S)"){
												if($cashRegister['transaction_type']=="(M)"){
													$type="Mobile";
												}else{
													$type="Server";
												}
												if($cashRegister["user_id"]!=0){
													$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cashRegister["user_id"]);
													$By= $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
												}else{
													$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cashRegister["user_val"]);
													$By= $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
												}
                                             
											}elseif($cashRegister['transaction_type']=="(W)"){
												$type="Website";
												if($cashRegister["user_id"]!=0){
													$users = $userMapper->getUserById($cashRegister["user_id"]);
													$By=$users->__get("user_fname") . " " . $users->__get("user_lname");
												}else{
													$users = $userMapper->getUserById($cashRegister["user_val"]);
													$By=$users->__get("user_fname") . " " . $users->__get("user_lname");
												}
                                                  
											}else{
												$type="Server";
												$By="Server";
											}
										$zendDate = new Zend_Date($cashRegister['timestamp'],"yyyy-MM-dd HH:mm:ss");
										$timestamp_date = $zendDate->toString("dd-MM-yyyy");	 
                                        $timestamp_time = $zendDate->toString("HH:mm:ss");
										$zendDate = new Zend_Date($cashRegister['consumer_act_date'],"yyyy-MM-dd HH:mm:ss");
										$consumer_act_date = $zendDate->toString("dd-MM-yyyy");		

										
												if($cashRegister["cr_status"]!='NEW'){
													$users = $userMapper->getUserById($cashRegister["approved_by"]);
													$approve_by=$users->__get("user_fname") . " " . $users->__get("user_lname");
													
													$zendDate = new Zend_Date($cashRegister['approved_date'],"yyyy-MM-dd HH:mm:ss");
													$approve_date = $zendDate->toString("dd-MM-yyyy");	
												}else{
													$approve_by=NULL;
													$approve_date=NULL;
												}
												
										$consumerdata.= "<tr>
											<td>".$state->__get("state_name")."</td>
											<td>".$cashRegister['site_name']."</td>
											<td>".$cashRegister['consumer_connection_id']."</td>
											<td>".$cashRegister['consumer_name']."</td>
											<td>".$consumer_act_date."</td>
											<td>".$cashRegister['consumer_status']."</td>
											<td>".$cashRegister['transaction_id']."</td>
											<td>".$cashRegister['cr_entry_type']."</td>
											<td>".$cashRegister['cr_amount']."</td>
											<td>".$type."</td>
											<td>".$timestamp_date."</td> 
											<td>".$timestamp_time."</td>
											<td>".$cashRegister['cr_status']."</td>
											<td>".$cashRegister['entry_status']."</td>
											<td>".$By."</td>
											<td>".$approve_by."</td>
											<td>".$approve_date."</td>";
											
										$consumerdata.= "</tr>";
										 
								}
							}
							
							if($cashRegisters_pending){
								foreach ($cashRegisters_pending as $cashRegister)
								{
									$state=$stateMapper->getStateById($cashRegister["state_id"]);
									 
											if($cashRegister['transaction_type']=="(M)" || $cashRegister['transaction_type']=="(S)"){
												if($cashRegister['transaction_type']=="(M)"){
													$type="Mobile";
												}else{
													$type="Server";
												}
												if($cashRegister["user_id"]!=0){
													$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cashRegister["user_id"]);
													$By= $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
												}else{
													$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cashRegister["user_val"]);
													$By= $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
												}
                                             
											}elseif($cashRegister['transaction_type']=="(W)"){
												$type="Website";
												if($cashRegister["user_id"]!=0){
													$users = $userMapper->getUserById($cashRegister["user_id"]);
													$By=$users->__get("user_fname") . " " . $users->__get("user_lname");
												}else{
													$users = $userMapper->getUserById($cashRegister["user_val"]);
													$By=$users->__get("user_fname") . " " . $users->__get("user_lname");
												}
                                                  
											}else{
												$type="Server";
												$By="Server";
											}
											$status_val=($cashRegister['cr_status']=='ACTIVE'?'PENDING':'REJECT');
											$zendDate = new Zend_Date($cashRegister['timestamp'],"yyyy-MM-dd HH:mm:ss");
											$timestamp_date = $zendDate->toString("dd-MM-yyyy");	 
											$timestamp_time = $zendDate->toString("HH:mm:ss");	
											
											if($cashRegister["cr_status"]!='NEW'){
													$users = $userMapper->getUserById($cashRegister["approved_by"]);
													$approve_by=$users->__get("user_fname") . " " . $users->__get("user_lname");
													
													$zendDate = new Zend_Date($cashRegister['approved_date'],"yyyy-MM-dd HH:mm:ss");
													$approve_date = $zendDate->toString("dd-MM-yyyy");	
												}else{
													$approve_by=NULL;
													$approve_date=NULL;
												}
												
											$zendDate = new Zend_Date($cashRegister['consumer_act_date'],"yyyy-MM-dd HH:mm:ss");
											$consumer_act_date = $zendDate->toString("dd-MM-yyyy");		
											$consumerdata.= "<tr>
											<td>".$state->__get("state_name")."</td>
											<td>".$cashRegister['site_name']."</td>
											<td>".$cashRegister['consumer_connection_id']."</td>
										    <td>".$cashRegister['consumer_name']."</td>
											<td>".$consumer_act_date."</td>
											<td>".$cashRegister['consumer_status']."</td>
											<td>".$cashRegister['transaction_id']."</td>
											<td>".$cashRegister['cr_entry_type']."</td>
											<td>".$cashRegister['cr_amount']."</td>
											<td>".$type."</td>
											<td>".$timestamp_date."</td>
											<td>".$timestamp_time."</td>
											<td>".$status_val."</td>
											<td>".$cashRegister['entry_status']."</td>
											<td>".$By."</td>
											<td>".$approve_by."</td>
											<td>".$approve_date."</td>"; 
											$consumerdata.= "</tr>";
										 
								}
							} 
							$consumerdata.=  "</tbody> </table>
						</div>";
						
							$newfile = 'Revenue Report for '.date('Y-m-d H:i:s').'.xlsx';
							$inputFileType = 'Excel2007';
							$tmpfile = tempnam("/html/excelBackup/", 'html');
							file_put_contents($tmpfile, $consumerdata);
						
							$objPHPExcel = new PHPExcel();
							$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
							$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
							$objPHPExcel->getActiveSheet()->setTitle('Transaction Report');  
						
							break;
				
				case "transaction_report_for_out" :
							
								$stateMapper=new Application_Model_StatesMapper();
								$cashRegisterMapper = new Application_Model_CashRegisterMapper();
								$logMapper=new Application_Model_LogsMapper();
								$packageMapper=new Application_Model_PackagesMapper();
								$siteMapper=new Application_Model_SitesMapper();
								$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
								$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
								$userMapper=new Application_Model_UsersMapper();
								$consumerMapper=new Application_Model_ConsumersMapper();
								$currDate = $request->getParam("secondDate");
								$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
								$currDate = $zendDates->toString("yyyy-MM-dd");
								
								$lastDate = $request->getParam("firstDate");
								$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
								$lastDate = $zendDate->toString("yyyy-MM-dd");
								
								$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
								$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
								$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
								$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
								$status = ($request->getParam("status")=="" || $request->getParam("status")=='undefined' )?NULL: explode(",", $request->getParam("status"));
								
								$collection_agent = ($request->getParam("collection_agent")=="" || $request->getParam("collection_agent")=='undefined' )?NULL: explode(",", $request->getParam("collection_agent"));
								$type = ($request->getParam("types")=="" || $request->getParam("types")=='undefined' )?NULL: explode(",", $request->getParam("types"));
								
								$outstanding = ($request->getParam("outstanding")=="" || $request->getParam("outstanding")=='undefined' )?NULL:  $request->getParam("outstanding");
								//$not_paid = ($request->getParam("not_paid")=="" || $request->getParam("type")=='not_paid' )?NULL: $request->getParam("not_paid");
								
								
								$consumerdata.= "<div>
							
										
	                <table border=1 align='center'>
                        <tbody>";
								$consumerdata.= "<tr>
										<th>State Name</th>
										<th>Site Name</th>
										<th>Consumer Name</th>
										<th>Consumer Connection ID</th>
									    <th>DEBIT</th>
										<th>CREDIT</th>
										<th>Outstanding</th>
										<th>Status</th>
								</tr>";
								$consumers=$consumerMapper->getConsumerByFilter($state_id,$site_id,$package_type,$package_id);
								 
								if ($consumers){
									foreach ($consumers as $consumer){
									$cashRegisters=$cashRegisterMapper->getOutstandingByConsumers($consumer["consumer_id"],$currDate,$lastDate);
									 if($cashRegisters){
									    	$credit=intval($cashRegisters["CREDIT"]);
									    	$debit=intval($cashRegisters["DEBIT"]);
									    	$outstanding_val=intval($debit-$credit);
									    	if($outstanding_val==0){
									    		$status="ZERO";
									    	}elseif ($outstanding_val>0){
									    		$status="Increment";
									    	}else{
									    		$status="Decrement";
									    	}
									    	if(($outstanding==0 && $outstanding_val==0) || ($outstanding==1 && $outstanding_val>0) || ($outstanding==2 && $outstanding_val<0)){
												$state=$stateMapper->getStateById($consumer["state_id"]);
												
												$consumerdata.= "<tr>
														<td>".$state->__get("state_name")."</td>
														<td>".$consumer['site_name']."</td>
													    <td>".$consumer['consumer_name']."</td>
														<td>".$consumer['consumer_connection_id']."</td>
														<td>".$debit."</td>
														<td>".$credit."</td>
														<td>".$outstanding_val."</td>
														<td>".$status."</td>";
														 
												$consumerdata.= "</tr>";
											}
										}
									}
								}
								 
								$consumerdata.=  "</tbody> </table>
						</div>";
							 
								$newfile = 'Outstanding Report for '.date('Y-m-d H:i:s').'.xlsx';
								$inputFileType = 'Excel2007';
								$tmpfile = tempnam("/html/excelBackup/", 'html');
								file_put_contents($tmpfile, $consumerdata);
							
								$objPHPExcel = new PHPExcel();
								$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
								$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
								$objPHPExcel->getActiveSheet()->setTitle('Outstanding Report');
							
								break;
				
				case "package_report" :
									
										$stateMapper=new Application_Model_StatesMapper();
											$cashRegisterMapper = new Application_Model_CashRegisterMapper();
											$logMapper=new Application_Model_LogsMapper();
											$packageMapper=new Application_Model_PackagesMapper();
											$siteMapper=new Application_Model_SitesMapper();
											$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
											$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
											$userMapper=new Application_Model_UsersMapper();
											$currDate = $request->getParam("secondDate");
											$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
											$currDate = $zendDates->toString("yyyy-MM-dd");

											$lastDate = $request->getParam("firstDate");
											$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
											$lastDate = $zendDate->toString("yyyy-MM-dd");
											
											$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
											$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
											$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
											$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
												
											$status = ($request->getParam("status")=="" || $request->getParam("status")=='undefined' )?NULL: explode(",", $request->getParam("status"));
											$wattage = ($request->getParam("wattage")=="" || $request->getParam("wattage")=='undefined' )?NULL: explode(",", $request->getParam("wattage"));
											 
										$consumerdata.= "<div>
										
	                <table border=1 align='center'>
                        <tbody>";
									
										$consumerdata.= "<tr>
										<th>Site Name</th>
										<th>Package Name</th>
										<th>Package Cost</th>
										<th>is_postpaid</th>
										<th>wattage</th>
										<th>unit</th>
										<th>extra_charges</th>
										<th>status</th>
										<th>User Name</th>
										 
						
						           </tr>";
										$packages=$packageMapper->getPackagesByDate($state_id,$site_id,$package_type,$package_id,$status,$wattage,$currDate,$lastDate);
										  
										if($packages){
											foreach ($packages as $package)
											{
												$site=$siteMapper->getSiteById($package["site_id"]);
												$user=$userMapper->getUserById($package["user_id"]);
												
												if($package["is_postpaid"]==0){
													$is_post="Prepaid";
												}elseif ($package["is_postpaid"]==1){
													$is_post="Postpaid";
												}else{
													$is_post="MixMode";
												}
										 
												 		$consumerdata.= "<tr>
															<td>".$site->__get("site_name")."</td>
															<td>".$package["package_name"]."</td>
															<td>".$package["package_cost"]."</td>
															<td>".$is_post."</td>
															<td>".$package["wattage"]."</td>
															<td>".$package["unit"]."</td>
															<td>".$package["extra_charges"]."</td>
															<td>".$package["status"]."</td>
															<td>".$user->__get("user_fname")." ".$user->__get("user_lname")."</td>";
														$consumerdata.= "</tr>";
												 
											}
										}
										 
										$consumerdata.=  "</tbody> </table>
						</div>";
										 
										$newfile = 'Package Report for '.date('Y-m-d H:i:s').'.xlsx';
										$inputFileType = 'Excel2007';
										$tmpfile = tempnam("/html/excelBackup/", 'html');
										file_put_contents($tmpfile, $consumerdata);
									 
										$objPHPExcel = new PHPExcel();
										$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
										$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
										$objPHPExcel->getActiveSheet()->setTitle('Package Report');
									
										break;
										
				case "meter_reading_report" :
													
												$stateMapper=new Application_Model_StatesMapper();
												$cashRegisterMapper = new Application_Model_CashRegisterMapper();
												$logMapper=new Application_Model_LogsMapper();
												$packageMapper=new Application_Model_PackagesMapper();
												$siteMapper=new Application_Model_SitesMapper();
												$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
												$meterReadingMapper=new Application_Model_MeterReadingsMapper();
												$consumerMapper=new Application_Model_ConsumersMapper();
												$packageHitoryMapper=new Application_Model_PackageHistoryMapper();
												
												$currDate = $request->getParam("secondDate");
												$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
												$currDate = $zendDates->toString("yyyy-MM-dd");
												
												$trans_date=date_parse_from_format("Y-m-d", $currDate);
												$Curr_month= $trans_date["month"];
												$Curr_year= $trans_date["year"];
												
												$lastDate = $request->getParam("firstDate");
												$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
												$lastDate = $zendDate->toString("yyyy-MM-dd");
													
												$trans_dates=date_parse_from_format("Y-m-d", $lastDate);
												$last_month= $trans_dates["month"];
												$last_year= $trans_dates["year"];
												$package_type_value=[1,2];
												$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
												$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
												$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?$package_type_value:explode(",", $request->getParam("package_type"));
												$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
												$consumer_id = ($request->getParam("consumer_id")=="" || $request->getParam("consumer_id")=='undefined' )?NULL: explode(",", $request->getParam("consumer_id"));
												$listing = ($request->getParam("listing")=="" || $request->getParam("listing")=='undefined' )?NULL: $request->getParam("listing");
													
												
												$consumerdata.= "<div>
												
											
	                <table border=1 align='center'>
                        <tbody>";
												$consumerdata.= "<tr>
													<th>State Name</th>
													<th>Site Name</th>
													<th>Consumer Name</th>
													<th>Father Name</th>
													<th>Consumer Connection ID</th>
												    <th>Activation Date</th>
													<th>Status</th>
													<th>Package Name</th>";
													 
												$cal=(strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24);
												$days = $cal ; 
												$counter=round($days/30);
												$next='+1 month';
												
												$secondNext=$lastDate;
												for($i=0;$i<$counter;$i++){
													$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
														$trans_date=date_parse_from_format("Y-m-d", $secondNext);
														$month= $trans_date["month"];
														$year= $trans_date["year"];
														$dateObj   = DateTime::createFromFormat('!m', $month);
														$monthName = $dateObj->format('M');
														$consumerdata.="<th> Reading_".$monthName.",".$year."</th>";
													$secondNext = date('Y-m-d', strtotime($secondNext.$next));
												} 
												$secondNext=$lastDate;
												for($i=0;$i<$counter;$i++){
													$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
														$trans_date=date_parse_from_format("Y-m-d", $secondNext);
														$month= $trans_date["month"];
														$year= $trans_date["year"];
														$dateObj   = DateTime::createFromFormat('!m', $month);
														$monthName = $dateObj->format('M');
														$consumerdata.="<th>Unit_".$monthName.",".$year."</th>";
													$secondNext = date('Y-m-d', strtotime($secondNext.$next));
												} 
												
													$consumerdata.="</tr>";
													$roleSession = new Zend_Session_Namespace('roles');
													$role_sites_id=$roleSession->site_id;
													
													$consumers=$consumerMapper->getConsumerByFilter($state_id,$site_id,$package_type,$package_id,$consumer_id,null,$role_sites_id,$currDate,$lastDate);
												
													if ($consumers){
														foreach ($consumers as $consumer){
															$package_id=$consumer["package_val"];
															$packages=$packageMapper->getPackageById($package_id);
															$state=$stateMapper->getStateById($consumer["state_id"]);
																		
															if($packages->__get("is_postpaid")!=0){
																if($listing=='month_wise_Consumption'){
																		$consumerdata.= "<tr>
																			<td>".$state->__get("state_name")."</td>
																			<td>".$consumer['site_name']."</td>
																			<td>".$consumer['consumer_name']."</td>
																			<td>".$consumer['consumer_father_name']."</td>
																			<td>".$consumer['consumer_connection_id']."</td>
																			<td>".$consumer['consumer_act_date']."</td>
																			<td>".$consumer['consumer_status']."</td>
																			<td>".$packages->__get("package_name")."</td>";
																			
																			$secondNext=$lastDate;
																			for($i=0;$i<$counter;$i++){
																				$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
																					$trans_date=date_parse_from_format("Y-m-d", $secondNext);
																					$month= $trans_date["month"];
																					$year= $trans_date["year"];
																					$dateObj   = DateTime::createFromFormat('!m', $month);
																					$monthName = $dateObj->format('M');
																					$readings=$meterReadingMapper->getOnlyMeterReadingsByConsumerIdTimestamp($consumer["consumer_id"],$month,$year);
																					
																					if($readings){
																						$consumerdata.="<td>".floatval($readings["meter_reading"])."</td>";
																					}else{
																						$consumerdata.="<td>0</td>";
																					}
																				 
																				    
																				$secondNext = date('Y-m-d', strtotime($secondNext.$next));
																			}
																			 
																			$secondNext=$lastDate;
																			for($i=0;$i<$counter;$i++){
																				$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
																					$trans_date=date_parse_from_format("Y-m-d", $secondNext);
																					$month= $trans_date["month"];
																					$year= $trans_date["year"];
																					$dateObj   = DateTime::createFromFormat('!m', $month);
																					$monthName = $dateObj->format('M');
																					$units=$meterReadingMapper->getTotalUnitByOnlyConsumerId($consumer["consumer_id"],$month,$year);
																					$consumerdata.="<td>".floatval($units)."</td>"; 
																				$secondNext = date('Y-m-d', strtotime($secondNext.$next));
																			}
																		
																	 $consumerdata.= "</tr>";
																}elseif($listing=='not_updated_MR'){
																			$consumerdata.= "<tr>
																			<td>".$state->__get("state_name")."</td>
																			<td>".$consumer['site_name']."</td>
																			<td>".$consumer['consumer_name']."</td>
																			<td>".$consumer['consumer_father_name']."</td>
																			<td>".$consumer['consumer_connection_id']."</td>
																			<td>".$consumer['consumer_act_date']."</td>
																			<td>".$consumer['consumer_status']."</td>
																			<td>".$packages->__get("package_name")."</td>";
																			
																			$secondNext=$lastDate;
																			for($i=0;$i<$counter;$i++){
																				$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
																					$trans_date=date_parse_from_format("Y-m-d", $secondNext);
																					$month= $trans_date["month"];
																					$year= $trans_date["year"];
																					$dateObj   = DateTime::createFromFormat('!m', $month);
																					$monthName = $dateObj->format('M');
																					$readings=$meterReadingMapper->getMeterReadingsByConsumerIdTimestamp($consumer["consumer_id"],$package_id,$month,$year);
																					
																					if($readings){
																						$consumerdata.="<td>".floatval($readings["meter_reading"])."</td>";
																					}else{
																						$consumerdata.="<td>Not Updated</td>";
																					}
																				 
																				    
																				$secondNext = date('Y-m-d', strtotime($secondNext.$next));
																			}
																			
																			$secondNext=$lastDate;
																			for($i=0;$i<$counter;$i++){
																				$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
																					$trans_date=date_parse_from_format("Y-m-d", $secondNext);
																					$month= $trans_date["month"];
																					$year= $trans_date["year"];
																					$dateObj   = DateTime::createFromFormat('!m', $month);
																					$monthName = $dateObj->format('M');
																						$current_unit=$meterReadingMapper->getTotalUnitByConsumerId($consumer["consumer_id"],$package_id,$month,$year);
																						if(intval($current_unit)>0){
																							$consumerdata.="<td>".floatval($current_unit)."</td>"; 
																						}else{
																							$consumerdata.="<td>Not Updated</td>";
																						}
																					$secondNext = date('Y-m-d', strtotime($secondNext.$next));
																			} 
																			$consumerdata.= "</tr>";	
																}
															}
														}
													} 
												
												$consumerdata.=  "</tbody> </table>
						</div>";			 	
												$newfile = 'Meter Reading Report for '.date('Y-m-d H:i:s').'.xlsx';
												$inputFileType = 'Excel2007';
												$tmpfile = tempnam("/html/excelBackup/", 'html');
												file_put_contents($tmpfile, $consumerdata);
													
												$objPHPExcel = new PHPExcel();
												$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
												$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
												$objPHPExcel->getActiveSheet()->setTitle('Meter Reading Report');
													
												break;

												
							case "gen_con_report" :
											$stateMapper=new Application_Model_StatesMapper();
											$cashRegisterMapper = new Application_Model_CashRegisterMapper();
											$logMapper=new Application_Model_LogsMapper();
											$packageMapper=new Application_Model_PackagesMapper();
											$siteMapper=new Application_Model_SitesMapper();
											$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
											$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
											$feedarMapper=new Application_Model_SiteMeterReadingMapper();
											
											$currDate = $request->getParam("secondDate");
											$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
											$currDate = $zendDates->toString("yyyy-MM-dd");

											$lastDate = $request->getParam("firstDate");
											$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
											$lastDate = $zendDate->toString("yyyy-MM-dd");
											
											$state_ids = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
											$site_ids = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
											$mppt = ($request->getParam("mppt")=="" || $request->getParam("mppt")=='undefined' )?NULL:explode(",", $request->getParam("mppt"));
											$feeder = ($request->getParam("feeder")=="" || $request->getParam("feeder")=='undefined' )?NULL:explode(",", $request->getParam("feeder"));

											$mppt_range = ($request->getParam("mppt_range")=="" || $request->getParam("mppt_range")=='undefined' )?NULL: $request->getParam("mppt_range");
											$mppt_range_con = ($request->getParam("mppt_range_con")=="" || $request->getParam("mppt_range_con")=='undefined' )?NULL: $request->getParam("mppt_range_con");
											$feeder_range = ($request->getParam("feeder_range")=="" || $request->getParam("feeder_range")=='undefined' )?NULL: $request->getParam("feeder_range");
											$feeder_range_con = ($request->getParam("feeder_range_con")=="" || $request->getParam("feeder_range_con")=='undefined' )?NULL: $request->getParam("feeder_range_con");

											$days=intval((strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24));
											 $roleSession = new Zend_Session_Namespace('roles');
											$role_sites_id=$roleSession->site_id;
											$consumerdata="";
											$StateName_Array=array();
											$zendDate = new Zend_Date();
											$zendDate->setTimezone("Asia/Calcutta");
										
											$day= intval($zendDate->toString("dd"));
											$month=intval( $zendDate->toString("MM"));
											$year= intval($zendDate->toString("yyyy"));
										
											$consumerdata.= "<div>
													<table border=1 align='center'>
			                        					<tbody><tr>
																<th>State Name</th>
																<th>Site Name</th>
																<th>Date</th>
																<th>MPPT1</th>
																<th>MPPT2</th>
																<th>MPPT3</th>
																<th>MPPT4</th>
																<th>MPPT5</th>
																<th>MPPT6</th>
																<th>MPPT7</th>
																<th>MPPT8</th>
																<th>MPPT9</th>
																<th>MPPT10</th>
																<th>DG</th>
																<th>BIOGAS</th>
																<th>Total</th>
																<th>FEEDAR1 (Plant)</th>
																<th>FEEDAR2 (DD)</th>
																<th>FEEDAR3 (DD)</th>
																<th>FEEDAR4 (NN)</th>
																<th>FEEDAR5 (NN)</th>
																<th>FEEDAR6 (DN)</th>
																<th>FEEDAR7 (DN)</th>
																<th>FEEDAR8 (DND)</th>
																<th>FEEDAR9  (DND)</th>
																<th>FEEDAR10 </th> 
																<th>FEEDAR11 (SP1)</th>
																<th>FEEDAR12 (SP2)</th>
																<th>FEEDAR13 (SP3)</th>
																<th>FEEDAR14 (SP4)</th>
																<th>FEEDAR15 (SP5)</th>    
																<th>Total</th></tr>"; 
							for($j=0;$j<=$days;$j++){
								$next="+".$j." day";
								$secondNext = date('Y-m-d H:i:s', strtotime($lastDate .$next));
							 
								$trans_date=date_parse_from_format("Y-m-d", $secondNext);
								$day=$trans_date["day"];
								$month= $trans_date["month"];
								$year= $trans_date["year"];
							$sites=$siteMapper->getSitesByStateAndSite($site_ids,$state_ids,$role_sites_id);
							 
							if($sites){
								foreach ($sites as $site){
									$site_id=$site["site_id"];
									$site_name=$site["site_name"];
									$state_name=$site["state_name"];
							//$feedar_data_new=$feedarMapper->getTotalFeedarReadingByDate($site_id,$day,$month,$year,$feeder_range,$feeder_range_con);
							//$mpptReading_data_new=$mpptReadingMapper->getTotalMpptReadingByDate($site_id,$day,$month,$year,$mppt_range,$mppt_range_con);
								//if($mpptReading_data_new || $feedar_data_new){
									//$temp=1;
									$mppts=$mpptReadingMapper->getTotalMpptReadingByMpptName($site_id,$day,$month,$year,$mppt_range,$mppt_range_con);
									 
									 
													if($mppts){
														$mpptTotal=floatval($mppts['MPPT1'])+floatval($mppts['MPPT2'])+floatval($mppts['MPPT3'])+floatval($mppts['MPPT4'])+floatval($mppts['MPPT5'])+floatval($mppts['MPPT6'])+floatval($mppts['MPPT7'])+floatval($mppts['MPPT8'])+floatval($mppts['MPPT9'])+floatval($mppts['MPPT10'])+floatval($mppts['DG'])+floatval($mppts['BIOGAS']);				
										 				$consumerdata.= "<tr>
																	<td>".$state_name."</td>
																	<td>".$site_name."</td>
																	<td>".$year."-".$month."-".$day."</td>
																	<td>".floatval($mppts['MPPT1'])."</td>
																	<td>".floatval($mppts['MPPT2'])."</td>
																	<td>".floatval($mppts['MPPT3'])."</td>
																	<td>".floatval($mppts['MPPT4'])."</td>
																	<td>".floatval($mppts['MPPT5'])."</td>
																	<td>".floatval($mppts['MPPT6'])."</td>
																	<td>".floatval($mppts['MPPT7'])."</td>
																	<td>".floatval($mppts['MPPT8'])."</td>
																	<td>".floatval($mppts['MPPT9'])."</td>
																	<td>".floatval($mppts['MPPT10'])."</td>
																	<td>".floatval($mppts['DG'])."</td>
																	<td>".floatval($mppts['BIOGAS'])."</td>
    																<td>".$mpptTotal."</td>";
										 	 
													}else{
														$consumerdata.= "<tr>
																	<td>".$state_name."</td>
																	<td>".$site_name."</td>
																	<td>".$year."-".$month."-".$day."</td>
																	<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
																	<td></td><td></td><td></td><td></td><td></td>";
													}
													$feedars=$feedarMapper->getTotalFeedarReadingByFeedarName($site_id,$day,$month,$year,$feeder_range,$feeder_range_con);
													if($feedars){
														$feedarTotal=floatval($feedars['FEEDER1'])+floatval($feedars['FEEDER2'])+floatval($feedars['FEEDER3'])+floatval($feedars['FEEDER4'])+floatval($feedars['FEEDER5'])+floatval($feedars['FEEDER6'])+floatval($feedars['FEEDER7']) +
																	floatval($feedars['FEEDER8'])+floatval($feedars['FEEDER9'])+floatval($feedars['FEEDER10'])+floatval($feedars['FEEDER11'])+floatval($feedars['FEEDER12'])+floatval($feedars['FEEDER13'])+floatval($feedars['FEEDER14'])+floatval($feedars['FEEDER15']);
													 		$consumerdata.= " 
																					<td>".floatval($feedars['FEEDER1'])."</td>
																					<td>".floatval($feedars['FEEDER2'])."</td>
																					<td>".floatval($feedars['FEEDER3'])."</td>
																					<td>".floatval($feedars['FEEDER4'])."</td>
																					<td>".floatval($feedars['FEEDER5'])."</td>
																					<td>".floatval($feedars['FEEDER6'])."</td>
																					<td>".floatval($feedars['FEEDER7'])."</td>
																					<td>".floatval($feedars['FEEDER8'])."</td>
																					<td>".floatval($feedars['FEEDER9'])."</td>
																					<td>".floatval($feedars['FEEDER10'])."</td>
																					<td>".floatval($feedars['FEEDER11'])."</td>
																					<td>".floatval($feedars['FEEDER12'])."</td>
																					<td>".floatval($feedars['FEEDER13'])."</td>
																					<td>".floatval($feedars['FEEDER14'])."</td>
																					<td>".floatval($feedars['FEEDER15'])."</td>
																					<td>".$feedarTotal."</td></tr>"; 
													
													}else{ 
															$consumerdata.= " <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
													}
								//}

								}
								}
							} 
											
											   
											$consumerdata.=  "</tbody> </table>
											</div>";
										 
											$newfile = 'Generation consumption report for '.$month.','.$year.'.xlsx';
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam("/html/excelBackup/", 'html');
											file_put_contents($tmpfile, $consumerdata);
												
											$objPHPExcel = new PHPExcel();
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('Gen Con report');
												
											break;
											
						case "consumer_profile_report" :
												 
													$stateMapper=new Application_Model_StatesMapper();
													$cashRegisterMapper = new Application_Model_CashRegisterMapper();
													$logMapper=new Application_Model_LogsMapper();
													$packageMapper=new Application_Model_PackagesMapper();
													$siteMapper=new Application_Model_SitesMapper();
													$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
													$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
													$userMapper=new Application_Model_UsersMapper();
													$consumerMapper=new Application_Model_ConsumersMapper();	
													$MMmapper=new Application_Model_SiteMasterMeterMapper();
													$wattageMapper=new Application_Model_WattageMapper();
													$currDate = $request->getParam("secondDate");
													$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
													$currDate = $zendDates->toString("yyyy-MM-dd");
													$conPackageMapper=new Application_Model_ConsumersPackageMapper();
													
													$lastDate = $request->getParam("firstDate");
													$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
													$lastDate = $zendDate->toString("yyyy-MM-dd");
														
													$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
													$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
													$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
													$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
													$status = ($request->getParam("status")=="" || $request->getParam("status")=='undefined' )?NULL: explode(",", $request->getParam("status"));
													$wattage = ($request->getParam("wattage")=="" || $request->getParam("wattage")=='undefined' )?NULL: explode(",", $request->getParam("wattage"));
													$upgrade_downgrade = ($request->getParam("upgrade_downgrade")=="" || $request->getParam("upgrade_downgrade")=='undefined' )?NULL: explode(",", $request->getParam("upgrade_downgrade"));
													$gender = ($request->getParam("gender")=="" || $request->getParam("gender")=='undefined' )?NULL: explode(",", $request->getParam("gender"));
													$feeder = ($request->getParam("feeder")=="" || $request->getParam("feeder")=='undefined' )?NULL: explode(",", $request->getParam("feeder"));
													$equipment_type = ($request->getParam("equipment_type")=="" || $request->getParam("equipment_type")=='undefined' )?NULL: explode(",", $request->getParam("equipment_type"));
													$scheme = ($request->getParam("scheme")=="" || $request->getParam("scheme")=='undefined' )?NULL: explode(",", $request->getParam("scheme"));
													$consumer_type = ($request->getParam("consumer_type")=="" || $request->getParam("consumer_type")=='undefined' )?NULL: explode(",", $request->getParam("consumer_type"));
													
													$no_of_packages = ($request->getParam("no_of_packages")=="" || $request->getParam("no_of_packages")=='undefined' )?NULL:  $request->getParam("no_of_packages");
													
													$consumerdata.= "<div>
														<table border=1 align='center'><tbody>";
													
													$consumerdata.= "<tr>
															<th>State Name</th>
															<th>Site Name</th>
															<th>Consumer Name</th>
															<th>Consumer Connection ID</th>
															<th>Category</th>
														    <th>Package Name</th>
															<th>Is Productive</th>
															<th>Package Cost</th>
															<th>Wattage	</th>
															<th>Feeder Hrs	</th>
															<th>Monthly Estimated Consumption</th>
															<th>Activation Date</th>
															<th>Banned Date</th>
															<th>Status</th>";
													
													
													if(count($upgrade_downgrade)>0){
														$consumerdata.="	<th>Package Status</th>";
													}		 
											           $consumerdata.="</tr>";
													$roleSession = new Zend_Session_Namespace('roles');
													$role_sites_id=$roleSession->site_id;
													$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
													$consumers=$consumerMapper->getConsumerDetailsByFilters($state_id,$site_id,$package_type,$package_id,$status,$wattage,$gender,$feeder,$equipment_type,$scheme,$consumer_type,$currDate,$lastDate,$role_sites_id,$no_of_packages);	
													if($consumers){
														foreach ($consumers as $consumer)
														{
															$state=$stateMapper->getStateById($consumer["state_id"]);
															$package=$packageMapper->getPackageById($consumer["package_val"]);
															
															$Con_type=$consumerMapper->getConsumerTypeById($consumer["type_of_me"]);
															$Con_type_name="";
															if($Con_type){
																$Con_type_name=$Con_type["consumer_type_name"];
															}
															$ConPacks=$conPackageMapper->getPackageByConsumerId($consumer['consumer_id']);
															$light_load="Non-Productive";
															if($ConPacks){
																foreach ($ConPacks as $ConPack){
																	$packages=$packageMapper->getPackageById($ConPack["package_id"]);
																	if($packages->__get("light_load")=='no'){
																		$light_load="Productive";
																	}
																}
															}
															$package_cost=$package->__get("package_cost");
														 
															$feedarNames=array();$feedarHrs=0;
															$site_meter_ids=explode(",", $consumer['site_meter_id']);
															if(($site_meter_ids)>0){
																for ($k=0;$k<count($site_meter_ids);$k++){
																	$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
																	if($feedarDetail){
																		if($feedarDetail->__get("is_24_hr")==1){
																			$feedarHr=24;
																		}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
																			$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
																		}else{
																			$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
																		}
																		$feedarHrs=$feedarHrs+$feedarHr;
																		$feedarNames[]=$feedarDetail->__get("meter_keyword");
																	}
																}
															}
															if(count($feedarNames)>0){
																$feedarName=implode(",", $feedarNames);
															}else{
																$feedarName=NULL;
															}
															
															$watt_total=0;
															$watt=$wattageMapper->getWattageBywattId($package->__get("wattage"));
															if($watt){
																$watt_total=$watt->__get("wattage");
																
															}
															$monthly_estimated=($watt_total*$feedarHrs*30)/1000;
														 if(count($upgrade_downgrade)>0){
															$packageHistorys=$packageHistoryMapper->getLastPackageHistoryDetailsByConumerId($consumer["consumer_id"],$currDate,$lastDate);
															if ($packageHistorys){
															 
																 	
																$ph_package_id=$packageMapper->getPackageById($packageHistorys["ph_package_id"]);
																$newPackage=0;$lastPackage=0;
																if($ph_package_id){
																	$newPackage=$ph_package_id->__get("package_cost");
																	$newpackage_name=$ph_package_id->__get("package_name");
																}
																$last_package=$packageMapper->getPackageById($packageHistorys["last_package"]);
																if($last_package){
																	$lastPackage=$last_package->__get("package_cost");
																	$lastpackage_name=$last_package->__get("package_name");
																}
																$zendDate = new Zend_Date($consumer['consumer_act_date'],"yyyy-MM-dd");
																$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
																
																if($consumer['consumer_status']=='banned'){
																	$zendDate = new Zend_Date($consumer['suspension_date'],"yyyy-MM-dd");
																	$suspension_date = $zendDate->toString("dd-MM-yyyy");		
																}else{
																	$suspension_date = NULL;
																}
																if(in_array("upgrade", $upgrade_downgrade)){
																	if($newPackage>$lastPackage){
																	$consumerdata.= "<tr>
																		<td>".$state->__get("state_name")."</td>
																		<td>".$consumer['site_name']."</td>
																	    <td>".$consumer['consumer_name']."</td>
																		<td>".$consumer['consumer_connection_id']."</td>
																		<td>".$Con_type_name."</td>
																		<td>".$package->__get("package_name")."</td>
																		<td>".$light_load."</td>
																		<td>".$package_cost."</td>
																		<td>".$watt_total."</td>
																		<td>".$feedarHrs."</td>
																		<td>".$monthly_estimated."</td>
																		<td>".$consumer_act_date."</td>
																		<td>".$suspension_date."</td>
																		<td>".$consumer['consumer_status']."</td>
																		<td> UPGRADE </td>";
																			$consumerdata.= "</tr>";
																	} 
																}
																if(in_array("downgrade", $upgrade_downgrade)){
																	if($newPackage<$lastPackage){
																		$consumerdata.= "<tr>
																		<td>".$state->__get("state_name")."</td>
																		<td>".$consumer['site_name']."</td>
																	    <td>".$consumer['consumer_name']."</td>
																		<td>".$consumer['consumer_connection_id']."</td>
																		<td>".$Con_type_name."</td>
																		<td>".$package->__get("package_name")."</td>
																		<td>".$light_load."</td>
																		<td>".$package_cost."</td>
																		<td>".$watt_total."</td>
																		<td>".$feedarHrs."</td>
																		<td>".$monthly_estimated."</td>
																		<td>".$consumer_act_date."</td>
																		<td>".$suspension_date."</td>
																		<td>".$consumer['consumer_status']."</td>
																		<td> DOWNGRADE </td>";
																		$consumerdata.= "</tr>";
																	}
																}
															   
															  }
															}
															//$consumersPack=$consumerPackageMapper->getPackageByConsumerId($consumer['consumer_id']);
															 
															$zendDate = new Zend_Date($consumer['consumer_act_date'],"yyyy-MM-dd");
															$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
															if($consumer['consumer_status']=='banned'){
																$zendDate = new Zend_Date($consumer['suspension_date'],"yyyy-MM-dd");
																$suspension_date = $zendDate->toString("dd-MM-yyyy");		
															}else{
																$suspension_date = NULL;
															} 
																$consumerdata.= "<tr>
																	<td>".$state->__get("state_name")."</td>
																	<td>".$consumer['site_name']."</td>
																    <td>".$consumer['consumer_name']."</td>
																	<td>".$consumer['consumer_connection_id']."</td>
																	<td>".$Con_type_name."</td>
																	<td>".$package->__get("package_name")."</td>
																	<td>".$light_load."</td>
																	<td>".$package_cost."</td>
																	<td>".$watt_total."</td>
																	<td>".$feedarHrs."</td>
																	<td>".$monthly_estimated."</td>
																	<td>".$consumer_act_date."</td>
																	<td>".$suspension_date."</td>
																	<td>".$consumer['consumer_status']."</td>";
																	 
																$consumerdata.= "</tr>";
															  
														}
													}
													 
													if(count($status)>0){
													if(in_array("'reactive'", $status)){
													$logs=$logMapper->getConsumerReactiveLogsBydate($currDate,$lastDate);
													if($logs){
														foreach ($logs as $log)
														{
															$log_msg=$log["message"];
															$msg=explode("banned Consumer ID: ", $log_msg);
															$conn_id=explode(" By", $msg[1]);
															$consumers=$consumerMapper->getConnectionIdDetails($conn_id[0]);
															if($consumers){
															 
																	$site=$siteMapper->getSiteById($consumers["site_id"]);
																	$state=$stateMapper->getStateById($site->__get("state_id"));
																	$ConPacks=$conPackageMapper->getPackageByConsumerId($consumers['consumer_id']);
																	$packageName=array();
																	if($ConPacks){
																		foreach ($ConPacks as $ConPack){
																			$packages=$packageMapper->getPackageById($ConPack["package_id"]);
																			$packageName[]=$packages->__get("package_name");
																		}
																	}
																	$zendDate = new Zend_Date($consumers['consumer_act_date'],"yyyy-MM-dd");
																	$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
																	if($consumer['consumer_status']=='banned'){
																		$zendDate = new Zend_Date($consumers['suspension_date'],"yyyy-MM-dd");
																		$suspension_date = $zendDate->toString("dd-MM-yyyy");		
																	}else{
																		$suspension_date = NULL;
																	}
															
																	$consumerdata.= "<tr>
																		<td>".$state->__get("state_name")."</td>
																		<td>".$site->__get("site_name")."</td>
																	    <td>".$consumers['consumer_connection_id']."</td>
																	    <td>".$Con_type_name."</td>
																		<td>".$consumers['consumer_name']."</td>
																		<td>".implode(",", $packageName)."</td>
																		<td>".$light_load."</td>
																		<td>".$package_cost."</td>
																		<td>".$watt_total."</td>
																		<td>".$feedarHrs."</td>
																		<td>".$monthly_estimated."</td>
																		<td>".$consumer_act_date."</td>
																		<td>".$suspension_date."</td>
																	    <td>".$consumers['consumer_status']."</td>
																		<td> CONSUMER RE-ACTIVE</td>";
																	$consumerdata.= "</tr>";
																}
															 
														}
														//}
													}
													}}
													if(count($status)>0){
													if(in_array("'disabled'", $status)){
												 	$DeletedCons=$consumerMapper->getDeletedConsumerDetails($currDate,$lastDate);
												 	if($DeletedCons){
												 		 
												 		foreach ($DeletedCons as $consumer)
												 		{
												 			$sites=$siteMapper->getSiteById($consumer["site_id"]);
												 			$state=$stateMapper->getStateById($site->__get("state_id"));
												 				
												 			$ConPacks=$conPackageMapper->getPackageByConsumerId($consumer['consumer_id']);
												 			$packageName=array();
												 			if($ConPacks){
												 				foreach ($ConPacks as $ConPack){
												 					$packages=$packageMapper->getPackageById($ConPack["package_id"]);
												 					$packageName[]=$packages->__get("package_name");
												 				}
												 			}
												 			$zendDate = new Zend_Date($consumer['consumer_act_date'],"yyyy-MM-dd");
												 			$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
															
															if($consumer['consumer_status']=='banned'){
																	$zendDate = new Zend_Date($consumer['suspension_date'],"yyyy-MM-dd");
																	$suspension_date = $zendDate->toString("dd-MM-yyyy");		
															}else{
																	$suspension_date = NULL;
															}
															
												 			$consumerdata.= "<tr>
												 				<td>".$state->__get("state_name")."</td>
																<td>".$sites->__get("site_name")."</td>
															    <td>".$consumer['consumer_connection_id']."</td>
															    <td>".$Con_type_name."</td>
																<td>".$consumer['consumer_name']."</td>
																<td>".implode(",", $packageName)."</td>
																<td>".$light_load."</td>
																<td>".$package_cost."</td>
																<td>".$watt_total."</td>
																<td>".$feedarHrs."</td>
																<td>".$monthly_estimated."</td>
																<td>".$consumer_act_date."</td>
																<td>".$suspension_date."</td>
																<td>".$consumer['consumer_status']."</td>
																<td>Deleted CONSUMER</td>";
												 			$consumerdata.= "</tr>";
												 		}
												 	}
													}}
													$consumerdata.=  "</tbody> </table>
						</div>";
											//echo 	$consumerdata;exit;
													$newfile = 'Consumer Profile Report for '.date('Y-m-d H:i:s').'.xlsx';
													$inputFileType = 'Excel2007';
													$tmpfile = tempnam("/html/excelBackup/", 'html');
													file_put_contents($tmpfile, $consumerdata);
												
													$objPHPExcel = new PHPExcel();
													$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
													$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
													$objPHPExcel->getActiveSheet()->setTitle('Consumer Profile Report');
												
													break;
													
					 case "monthly-otp-report":
						 
						$stateMapper = new Application_Model_StatesMapper();
						$clusterMapper = new Application_Model_ClustersMapper();
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$stateMapper=new Application_Model_StatesMapper();
						$siteMapper = new Application_Model_SitesMapper();
						$consumerMapper = new Application_Model_ConsumersMapper();
						$packageMapper = new Application_Model_PackagesMapper();
						
						$StateName_Array = array();
						$states = $stateMapper->getAllStates();
						$counters = 0;
						$months = ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar'];
						$month_no = [4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3];
						 
						$date = new Zend_Date();
						$date->setTimezone("Asia/Calcutta");
						$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
						$trans_date=date_parse_from_format("Y-m-d", $timestamp);
						$month = $trans_date["month"];
						$years= $trans_date["year"];
						
						$roleSession = new Zend_Session_Namespace('roles');
						$role_sites_id=$roleSession->site_id;
						
					    $consumerdata = "";
						$consumerdata .= " <h4 align='center'>Monthly OTP and cash collected report for " . date('F, Y') . "</h4>";
						$consumerdata .= "<div>			
	                	<table border=1 align='center'>
                        	<tbody>";
						  		$consumerdata .= "<tr><th></th><th></th>";
								for($m=0 ; $m<sizeof($months);$m++)
								{
									$year_vals=$year;
									if($month >=4)
										$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
										$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata .= "<th></th><th></th><th></th><th></th><th>" . $months[$m] . "," . $year_vals . "</th><th></th><th></th><th></th><th></th>";
									if($year==$years){
										if($month_no[$m]==$month){
											break;
										}
									}
								}
							  $consumerdata .= "</tr>";
								 
								$consumerdata .= "<tr><th></th><th></th>";
								 
							 
								for($m=0 ; $m<sizeof($months);$m++)
								{
									$year_vals=$year;
									if($month >=4)
										$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
										$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata .= "	<th>BILL</th>
														<th>BILL Scheme</th>
											      		<th>CHG</th>
														<th>CREDIT Note</th>
											      		<th>OTP</th>
													  	<th>EED</th>
														<th>MED</th>
														<th>WATER</th>
														<th>OTHERS</th>";
									if($year==$years){
										if($month_no[$m]==$month){
												break;
										}
									}
								}
								 $consumerdata .= "</tr>";
								  
								if($states){
									$grandTotal=array();
									foreach ($states as $state){
										$consumerdata .= "<tr>	<td>".$state->__get("state_name")."</td></tr>";
										$sites=$siteMapper->getSiteByStateId($state->__get("state_id"));
										if($sites){
											foreach ($sites as $site){
												if (in_array($site->__get("site_id"), $role_sites_id)){
													$consumerdata .= "<tr>	<td></td>	<td>".$site->__get("site_name")."</td> ";
													for($m=0 ; $m<sizeof($months);$m++)
													{
														$year_vals=$year;
														if($month >=4)
															$year_vals=($month_no[$m] >=4)?$year:$year+1;
															else {
															$year_vals=($month_no[$m] >=4)?$year-1:$year;
															}
														$cashRegister=$cashRegisterMapper->getBillsBySiteId($site->__get("site_id"),$month_no[$m],$year_vals,null,$role_sites_id);
														$consumerdata .= "	<th>".intval($cashRegister["BILL"])."</th>
																			<th>".intval($cashRegister["SCHEME"])."</th>
																			<th>".intval($cashRegister["CHG"])."</th>
																			<th>".intval($cashRegister["DISCOUNT"])."</th>
																			<th>".intval($cashRegister["ACT"])."</th>
																			<th>".intval($cashRegister["EED"])."</th>
																			<th>".intval($cashRegister["MED"])."</th>
																			<th>".intval($cashRegister["WATER"])."</th>
																			<th>".intval($cashRegister["OTHERS"])."</th>";
														if($year==$years){
															if($month_no[$m]==$month){
																break;
															}
														}
													}
													$consumerdata .= "</tr>";
												}
											}
										}
										
										$consumerdata .= "<tr>";
										$consumerdata .= "	<th>".$state->__get("state_name")." Total </th>
															<th></th>";
										
												for($m=0 ; $m<sizeof($months);$m++)
												{
													$year_vals=$year;
													if($month >=4)
														$year_vals=($month_no[$m] >=4)?$year:$year+1;
														else {
														$year_vals=($month_no[$m] >=4)?$year-1:$year;
														}
													$cashRegister=$cashRegisterMapper->getBillsBySiteId($site->__get("site_id"),$month_no[$m],$year_vals,$state->__get("state_id"),$role_sites_id);
													$grandTotal[]=$cashRegister;
													$consumerdata .= "	<th>".intval($cashRegister["BILL"])."</th>
															      		<th>".intval($cashRegister["SCHEME"])."</th>
																		<th>".intval($cashRegister["CHG"])."</th>
																		<th>".intval($cashRegister["DISCOUNT"])."</th>
															      		<th>".intval($cashRegister["ACT"])."</th>
																	  	<th>".intval($cashRegister["EED"])."</th>
																		<th>".intval($cashRegister["MED"])."</th>
																		<th>".intval($cashRegister["WATER"])."</th>
																		<th>".intval($cashRegister["OTHERS"])."</th>";
													if($year==$years){
														if($month_no[$m]==$month){
															break;
														}
													}
												}
									}
										$consumerdata .= "</tr>";
										
										$consumerdata .= "<tr></tr><tr>";
										$consumerdata .= "	<th>Grand Total </th>
															<th></th>";
										
										for($m=0 ; $m<sizeof($months);$m++)
										{
										$year_vals=$year;
											if($month >=4)
												$year_vals=($month_no[$m] >=4)?$year:$year+1;
											else {
												$year_vals=($month_no[$m] >=4)?$year-1:$year;
											}
											$cashRegister=$cashRegisterMapper->getBillsBySiteId(null,$month_no[$m],$year_vals,null,$role_sites_id);
													$consumerdata .= "	<th>".intval($cashRegister["BILL"])."</th>
																		<th>".intval($cashRegister["SCHEME"])."</th>
																		<th>".intval($cashRegister["CHG"])."</th>
																		<th>".intval($cashRegister["DISCOUNT"])."</th>
																		<th>".intval($cashRegister["ACT"])."</th>
																		<th>".intval($cashRegister["EED"])."</th>
																		<th>".intval($cashRegister["MED"])."</th>
																		<th>".intval($cashRegister["WATER"])."</th>
																		<th>".intval($cashRegister["OTHERS"])."</th>";
														if($year==$years){
															if($month_no[$m]==$month){
																break;
															}
														}
											}
									$consumerdata .= "</tr>";
									
								}
						
								$consumerdata .= "</tbody> </table> </div>";
      					  		 
								$newfile = 'Monthly Collection Report on '. date('d M Y hi A').'.xlsx';
								$inputFileType = 'Excel2007';
								$tmpfile = tempnam("/html/excelBackup/", 'html');
								file_put_contents($tmpfile, $consumerdata);
												
								$objPHPExcel = new PHPExcel();
								$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
								$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
								$objPHPExcel->getActiveSheet()->setTitle('Monthly Collection Report');
												
					 break;
					 
					 
				case "productive-load-consumer-data":
					 		
					 	$stateMapper = new Application_Model_StatesMapper();
					 	$clusterMapper = new Application_Model_ClustersMapper();
					 	$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					 	$stateMapper=new Application_Model_StatesMapper();
					 	$siteMapper = new Application_Model_SitesMapper();
					 	$consumerMapper = new Application_Model_ConsumersMapper();
					 	$packageMapper = new Application_Model_PackagesMapper();
					 	$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
					 	$conPackageMapper=new Application_Model_ConsumersPackageMapper();
					 	$wattageMapper=new Application_Model_WattageMapper();
					 	$MMmapper=new Application_Model_SiteMasterMeterMapper();
					 	$meterReadingMapper=new Application_Model_MeterReadingsMapper();
						$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
					 	
					 	date_default_timezone_set('Asia/Kolkata');
					 	$StateName_Array = array();
					 	$counters = 0;
					 	$months = ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar'];
					 	$month_no = [4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3];
					 		
					 	$date = new Zend_Date();
					 	$date->setTimezone("Asia/Calcutta");
					 	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
					 	$trans_date=date_parse_from_format("Y-m-d", $timestamp);
					 	$month = $trans_date["month"];
					 	$years= $year;
									
					 	$roleSession = new Zend_Session_Namespace('roles');
					 	$role_sites_id=$roleSession->site_id;
					 
					 	$consumerdata = "";
					 	$consumerdata .= "<div>
	                	<table border=1 align='center'><tbody>";
					 	
					 	$consumerdata .= "<tr>
					 						<th>Load Status</th>
					 						<th>Enterprise Type</th>
					 						<th>State</th>
					 						<th>Site</th>
					 						<th>Consumer ID</th>
					 						<th>Consumer Name</th>
					 						<th>Activation Date</th>
					 						<th>Activation Month</th>
					 						<th>Status</th>
					 						<th>Banned Date</th>
					 						<th>Banned Month</th>
					 						<th>Category</th>
					 						<th>Package Type</th>
					 						<th>Package Name</th>
					 						<th>Cost</th>
					 						<th>Wattage</th>";
					 	for($m=0 ; $m<sizeof($months);$m++)
					 	{
					 		$year_vals=$year;
					 		if($month >=4)
					 			$year_vals=($month_no[$m] >=4)?$year:$year+1;
					 		else {
					 			$year_vals=($month_no[$m] >=4)?$year-1:$year;
					 		}
					 		$consumerdata .= "<th>" . $months[$m] . "-" . $year_vals . "</th>";
					 		/*if($year==$years){
					 			if($month_no[$m]==$month){
					 				break;
					 			}
					 		}*/
					 	}
						
					 	$consumerdata .= "<th></th>";
					 	for($m=0 ; $m<sizeof($months);$m++)
					 	{
					 		$year_vals=$year;
					 		if($month >=4)
					 			$year_vals=($month_no[$m] >=4)?$year:$year+1;
					 		else {
					 			$year_vals=($month_no[$m] >=4)?$year-1:$year;
					 		}
					 		$consumerdata .= "<th>" . $months[$m] . "-" . $year_vals . "</th>";
					 		/*if($year==$years){
					 			if($month_no[$m]==$month){
					 				break;
					 			}
					 		}*/
					 	}
						 
					 	$consumerdata .= "</tr>";
					 	
					 	$roleSession = new Zend_Session_Namespace('roles');
					 	$role_sites_id=$roleSession->site_id;
					 	$consumers=$consumerMapper->getAllConsumerByProductiveLoad($month,$year,$role_sites_id);
						 
						if($consumers){
							foreach ($consumers as $consumer){
								//if($consumer['consumer_id']==3149){
								$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $years);
								 
								$timestamp=$years."-".$month."-".$total_days_in_month;  
								$year_new=$year;
								if($month<=3){
									$year_new=$year-1;
								}
								 $bannedDate=$year_new."-04-01";
								 
								$ConPacks=$packageHistoryMapper->getpackageDetailByConsumerID($consumer['consumer_id'],$year_new,$timestamp);
								$light_load="Non-Productive Load";
								if($ConPacks){
									foreach ($ConPacks as $ConPack){
										$packages=$packageMapper->getPackageById($ConPack["package_id"]);
										if($packages->__get("light_load")=='no'){
											$light_load="Productive Load";
										}
									}
								}else{
									$ConsumerPacks=$conPackageMapper->getPackageByConsumerId($consumer['consumer_id']);
									if($ConsumerPacks){
									foreach ($ConsumerPacks as $ConsumerPack){
											$packages=$packageMapper->getPackageById($ConsumerPack["package_id"]);
											if($packages->__get("light_load")=='no'){
												$light_load="Productive Load";
											}
										}
									
									}
								}
								if($consumer['consumer_status']=='banned' && $consumer['suspension_date']<$bannedDate){
										$light_load="Non-Productive Load";
								}
								
								if($light_load=='Productive Load'){
									$state=$stateMapper->getStateById($consumer["state_id"]);
									$consumerType=$consumerMapper->getConsumerTypeById($consumer["type_of_me"]);
									
									$ConsumerPacks=$conPackageMapper->getPackageByConsumerId($consumer['consumer_id']);
									$packageName=array();$packageType=array();$package_cost=0;$wattage=0;
									if($ConsumerPacks){
										$light_load_now="Non-Productive Load";
										foreach ($ConsumerPacks as $ConsumerPack){
											$packages=$packageMapper->getPackageById($ConsumerPack["package_id"]);
											if($packages->__get("package_name")=="Package Nil"){
												$packageName[]=" EED ";
											} else{ 
												$packageName[]=" ".$packages->__get("package_name")." ";
											}
											if($packages->__get("is_postpaid")==1){
												$packageType[]="ME ";
											}elseif ($packages->__get("is_postpaid")==2){
												$packageType[]="MM ";
											}else{
												$packageType[]="FIXED ";
											}
											$package_cost=$package_cost+$packages->__get("package_cost");
											$watt_total=$wattageMapper->getWattageBywattId($packages->__get("wattage"));
											$wattage=$wattage+$watt_total->__get("wattage");
											
											if($packages->__get("light_load")=='no'){
												$light_load_now="Productive Load";
											}
										}
									}
									$consumer_act_date=NULL;$consumer_banned_date=NULL;
									$consumer_act_month=NULL;$consumer_banned_month=NULL;
									if($consumer["consumer_status"]!='banned'){
										$zendDate = new Zend_Date($consumer['consumer_act_date'],"yyyy-MM-dd");
										$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
										$consumer_act_month=date('F-Y',strtotime($consumer['consumer_act_date']));
									}else{
										$zendDate = new Zend_Date($consumer['suspension_date'],"yyyy-MM-dd");
										$consumer_banned_date = $zendDate->toString("dd-MM-yyyy");
										$consumer_banned_month=date('F-Y',strtotime($consumer['suspension_date']));
									}
									$consumerdata .= "<tr>
												<td>".$light_load_now."</td>
												<td> - </td>
												<td>".$state->__get("state_name")."</td>
												<td>".$consumer["site_name"]."</td>
												<td>".$consumer["consumer_connection_id"]."</td>
												<td>".$consumer["consumer_name"]."</td>
												<td>".$consumer_act_date."</td>
												<td>".$consumer_act_month."</td>
												<td>".$consumer["consumer_status"]."</td>
												<td>".$consumer_banned_date."</td>
												<td>".$consumer_banned_month."</td>
												<td>".$consumerType["consumer_type_name"]."</td>
												<td>".implode(",", $packageType)."</td>
												<td>".implode(",", $packageName)."</td>
												<td>".$package_cost."</td>
												<td>".$wattage."</td>";
									 
									for($m=0 ; $m<sizeof($months);$m++)
									{
										$year_vals=$year;
										if($month >=4)
											$year_vals=($month_no[$m] >=4)?$year:$year+1;
										else {
											$year_vals=($month_no[$m] >=4)?$year-1:$year;
										}
										$cashRegister_amount=$cashRegisterMapper->getMonthDebitByConsumerId($consumer["consumer_id"],$month_no[$m],$year_vals);
										if($cashRegister_amount){
											$consumerdata .= "<td>" .$cashRegister_amount. "</td>";
										}else{
											$consumerdata .= "<td>" .NULL. "</td>";
										}
											/*if($year==$years){
												if($month_no[$m]==$month){
													break;
												}
											}*/
									}
									
									$consumerdata .= "<td></td>";
									
									$consumer_scheme=$consumerSchemeMapper->getschemePackageByConsumerID($consumer['consumer_id']);
									
									for($m=0 ; $m<sizeof($months);$m++)
									{
										$year_vals=$year;
										if($month >=4)
											$year_vals=($month_no[$m] >=4)?$year:$year+1;
										else {
											$year_vals=($month_no[$m] >=4)?$year-1:$year;
										}
										  
										$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_no[$m], $year_vals);
										
										$date_val=$year_vals."-".$month_no[$m]."-".$total_days_in_month;
										$packageHistoryData=$packageHistoryMapper->getpackageHistoryByConsumerAndDate($date_val, $consumer['consumer_id']);
										
										$temp=0;
										 
										$trans_date=date_parse_from_format("Y-m-d", $consumer['suspension_date']);
									    $month_value= $trans_date["month"];
									    $year_value= $trans_date["year"];
										
										$susDate=$year_value."-".$month_value."-".$total_days_in_month; 
										 
										if($consumer['consumer_status']=='banned' && strtotime($susDate)<strtotime($date_val)){
											$temp=1;
										}
										 
										 $totalUnit=0;
										if($temp==0){
											if($packageHistoryData){
												$packages=$packageMapper->getPackageById($packageHistoryData["package_id"]);
												$watt=$wattageMapper->getWattageBywattId($packages->__get("wattage"));
												if($packages->__get("is_postpaid")==0) {
													$feedarHrs=0;
													$site_meter_ids=explode(",", $consumer['site_meter_id']);
													$countFeedar=0;
													if(($site_meter_ids)>0){
														for ($k=0;$k<count($site_meter_ids);$k++){
															$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
															if($feedarDetail){
																$countFeedar=$countFeedar+1;
																if($feedarDetail->__get("is_24_hr")==1){
																	$feedarHr=24;
																}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
																	$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
																}else{
																	$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
																}
																$feedarHrs=$feedarHrs+$feedarHr;
															}
														}
														if($countFeedar>1){
															$feedarHrs=10;
														}
													}
													$dailyUnit=sprintf("%.2f",($watt->__get("wattage")*$feedarHrs*$total_days_in_month)/1000);
														
												}else{
													
													/*$feedarHrs=0;
													$site_meter_ids=explode(",", $consumer['site_meter_id']);
													$countFeedar=0;
													if(($site_meter_ids)>0){
														for ($k=0;$k<count($site_meter_ids);$k++){
															$feedarDetail=$MMmapper->getMMById($site_meter_ids[$k]);
															if($feedarDetail){
																$countFeedar=$countFeedar+1;
																if($feedarDetail->__get("is_24_hr")==1){
																	$feedarHr=24;
																}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
																	$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
																}else{
																	$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
																}
																$feedarHrs=$feedarHrs+$feedarHr;
															}
														}
														if($countFeedar>1){
															$feedarHrs=10;
														}
													}*/
													$Unit_value=$packages->__get("is_postpaid"); // sprintf("%.2f",($watt->__get("wattage")*$feedarHrs*$total_days_in_month)/1000);
															
													$meterReading=$meterReadingMapper->getTotalUnitSiteWise($consumer["consumer_id"],$month_no[$m],$year_vals);
													if($meterReading){
														if(floatval($meterReading["curr_reading"]!=0)){
															$totalMtrUnit=(floatval($meterReading["curr_reading"])-floatval($meterReading["last_reading"]));
															$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month_no[$m], $year_vals);
															if($Unit_value<$totalMtrUnit){
																$dailyUnit=sprintf("%.2f",$totalMtrUnit);
															}else{
																$dailyUnit=sprintf("%.2f",$Unit_value);
															}
														}else{
															$dailyUnit=$Unit_value;
														}
													}else{
														$totalMtrUnit=$Unit_value;
														$dailyUnit=$Unit_value;
													}
												}
												$totalUnit=$totalUnit+$dailyUnit;
											}
											
											if($consumer_scheme){
												foreach($consumer_scheme as $consumer_schemes){ 
													$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
													$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
													//$lastTimestamp=$consumer_schemes["timestamp"];
													$discount_month=$consumer_schemes["discount_month"];
													$feeder_hours=$consumer_schemes["feeder_hours"];
													$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
													$currTimestamp = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month")); 
													  
													if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($currTimestamp)>=strtotime($date_val)){
														 $dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]*$feeder_hours*$total_days_in_month)/1000);
														$totalUnit=$totalUnit+$dailyUnit;
													} 
												}
											}
											 
											if($totalUnit>0){
												$consumerdata .= "<td>" .sprintf("%.2f",$totalUnit). "</td>";
											}else{
												$consumerdata .= "<td>" .NULL. "</td>";
											}
										}else{
											$consumerdata .= "<td>" .NULL. "</td>";
										} 
										/*if($year==$years){
											if($month_no[$m]==$month){
												break;
											}
										}*/
									}
									 
								}
								//} 
							}
						}
					 	$consumerdata .= "</tr>";
					 
					 	 
						$consumerdata .= "</tbody> </table> </div>";
						//echo $consumerdata; exit;   
						$newfile = 'Productive load consumer data on '. date('d M Y hi A').'.xlsx';
					 	$inputFileType = 'Excel2007';
						$tmpfile = tempnam("/html/excelBackup/", 'html');
						file_put_contents($tmpfile, $consumerdata);
					 
						$objPHPExcel = new PHPExcel();
						$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
						$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('Productive Load');
					 
					 break;
				 
				case "generation_consumption_report" :  
											$stateMapper=new Application_Model_StatesMapper();
											$cashRegisterMapper = new Application_Model_CashRegisterMapper();
											$logMapper=new Application_Model_LogsMapper();
											$packageMapper=new Application_Model_PackagesMapper();
											$siteMapper=new Application_Model_SitesMapper();
											$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
											$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
											$feedarMapper=new Application_Model_SiteMeterReadingMapper();
											
											$currDate = $request->getParam("secondDate");
											$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
											$currDate = $zendDates->toString("yyyy-MM-dd");

											$lastDate = $request->getParam("firstDate");
											$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
											$last_date = $zendDate->toString("yyyy-MM-dd");
											
											$state_ids = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
											$site_ids = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
											 
											$cal=intval((strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24));
											$days = $cal; 
											if($counter<=0){
												$counter=1;
											} 
											$counter=intval($days/30);
											
											$next='+1 month';
											
											$roleSession = new Zend_Session_Namespace('roles');
											$role_sites_id=$roleSession->site_id;
											$consumerdata="";
											$StateName_Array=array();
											$zendDate = new Zend_Date();
											$zendDate->setTimezone("Asia/Calcutta");
											
											
											$consumerdata.= "<div>
													<table border=1 align='center'>
			                        					<tbody><tr>
																<th> </th>";
														$secondNext=$last_date;
														for($i=0;$i<$counter;$i++){
													
															$trans_date=date_parse_from_format("Y-m-d", $secondNext);
															$month= $trans_date["month"];
															$year= $trans_date["year"];
															
															$dateObj   = DateTime::createFromFormat('!m', $month);
															$monthName = $dateObj->format('M');
															
															$consumerdata.= " <th></th><th></th>
																	<th></th><th>".$monthName."-".$year."</th>
																	<th></th><th></th><th></th>";
															$secondNext = date('Y-m-d', strtotime($secondNext.$next));
														}
														$consumerdata.= "</tr><tr>";
														
														$secondNext=$last_date;
														for($i=0;$i<$counter;$i++){
													
															$trans_date=date_parse_from_format("Y-m-d", $secondNext);
															$month= $trans_date["month"];
															$year= $trans_date["year"];
															
															$dateObj   = DateTime::createFromFormat('!m', $month);
															$monthName = $dateObj->format('M');
															
															$consumerdata.= "<th></th>
																			 <th>Generation</th>
																			 <th></th><th></th>
																			 <th>Consumption</th>
																			 <th></th><th></th>";
															$secondNext = date('Y-m-d', strtotime($secondNext.$next));
														}
														$consumerdata.= "</tr><tr><th>Sites</th>";
														
														$secondNext=$last_date;
														for($i=0;$i<$counter;$i++){
													
															$trans_date=date_parse_from_format("Y-m-d", $secondNext);
															$month= $trans_date["month"];
															$year= $trans_date["year"];
															
															$dateObj   = DateTime::createFromFormat('!m', $month);
															$monthName = $dateObj->format('M');
															
															$consumerdata.= " <th>PV</th>
																			  <th>DG</th>
																			  <th>BG</th>
																			  <th>DD</th>
																			  <th>NN</th>
																			  <th>DN</th> 
																			  <th>Plant Utilization</th>";
															$secondNext = date('Y-m-d', strtotime($secondNext.$next));
														}
														
											
														$states=$stateMapper->getAllStates();
														if($states){
															foreach ($states as $state){
																if(in_array($state->__get("state_id"), $state_ids)){
																	$site_arr_val=array();
																	$sites=$siteMapper->getSiteByStateId($state->__get("state_id"));
																	foreach ($sites as $site)
																	{	
																		if(in_array($site->__get("site_id"), $site_ids) || $site_ids==null){
																			$consumerdata.= "<tr><th>".$site->__get("site_name")."</th>";
																			$total_gen=0;$total_feeder=0;
																			$secondNext=$last_date;
																			for($i=0;$i<$counter;$i++){
																				$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
																				$dataValue=$mpptReadingMapper->getPlantDataByFilters($site->__get("site_id"),null,null,date('Y-m-d', strtotime($secondCurr."-1 day")),$secondNext); 
																				$gen_pv = $dataValue["mppt"]-$dataValue["DG"]-$dataValue["BIOGAS"];
																				$plantDetails=$siteMapper->getPlantDetailsBySites($site->__get("site_id"),null,null);
																				
																				$trans_date=date_parse_from_format("Y-m-d", $secondNext);
																				$month= $trans_date["month"];
																				$year= $trans_date["year"]; 
																				$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
																				 
																				$genStan=80*round(floatval($plantDetails["Solar_panel_kw"])*floatval($plantDetails["radiation"])*$total_days_in_month)/100; 
																			
																				//$total_gen=$total_gen+$genStan;
																				//$total_feeder=$total_feeder+floatval($dataValue["feeder"]);
																				$utilization=sprintf("%.2f",((floatval($dataValue["feeder"])/floatval($genStan))*100));    
																				 
																				$consumerdata.= "<th>".$gen_pv."</th> 
																								 <th>".$dataValue["DG"]."</th>
																								 <th>".$dataValue["BIOGAS"]."</th>
																								 <th>".$dataValue["feeder_DD"]."</th>
																								 <th>".$dataValue["feeder_NN"]."</th>
																								 <th>".$dataValue["feeder_DN"]."</th>
																								 <th>".$utilization."</th>";
																								 
																				$secondNext = date('Y-m-d', strtotime($secondNext.$next));
																			}
																				
																			$consumerdata.= "</tr>";
																		}
																	}
																}
															}
														}
														//echo $consumerdata;exit;     
										  	
											$consumerdata.=  "</tbody> </table>
											</div>";
										 
											$newfile = 'Generation consumption report from '.$request->getParam("firstDate").','.$request->getParam("secondDate").'.xlsx';
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam("/html/excelBackup/", 'html');
											file_put_contents($tmpfile, $consumerdata);
												
											$objPHPExcel = new PHPExcel();
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('Gen Con report');
												
											break;
				
				case "site_wiseconsumer_report" :
							$date = new Zend_Date();
							$date->setTimezone("Asia/Calcutta");
							$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
									
							$curr_date = $request->getParam("secondDate");
							$zendDates = new Zend_Date($curr_date,"MMMM D, YYYY");
							$curr_date = $zendDates->toString("yyyy-MM-dd");
							$conPackageMapper=new Application_Model_ConsumersPackageMapper();
																	
							$last_date = $request->getParam("firstDate");
							$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
							$last_date = $zendDate->toString("yyyy-MM-dd");
																		
							$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
							$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
								
							$stateMapper=new Application_Model_StatesMapper();
							$clusterMapper=new Application_Model_ClustersMapper();
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$siteMapper=new Application_Model_SitesMapper();
							$consumerMapper=new Application_Model_ConsumersMapper();
							$packageMapper=new Application_Model_PackagesMapper();
							$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
							$logMapper=new Application_Model_LogsMapper();
							
							$StateName_Array=array();
							$counters=0;
							$consumerdata="";
							
							$consumerdata.= "<div>
								<table border=1 align='center'> 
									<tbody>";
								
								$consumerdata.= "<tr><th></th> 
												<th>Consumer #(Start of month)</th>
												<th></th>
												<th>New Added</th>
												<th></th>
												<th>Disconnected</th>
												<th></th>
												<th>Upgrade</th>
												<th></th>
												<th>Downgrade</th>
												<th></th>
												<th>Re-activation</th>
												<th></th>
												<th>Consumer #(End of month)</th>
												<th></th></tr>";
							 
											$consumerdata.="<tr><th>Sites</th> 
												<th>Prepaid</th><th>Postpaid</th><th>Prepaid</th><th>Postpaid</th>
												<th>Prepaid</th><th>Postpaid</th><th>Prepaid</th><th>Postpaid</th>
												<th>Prepaid</th><th>Postpaid</th><th>Prepaid</th><th>Postpaid</th>
												<th>Prepaid</th><th>Postpaid</th></tr>"; 
											
											$states=$stateMapper->getAllStates();
											foreach ($states as $state){
												if(in_array($state->__get("state_id"), $state_id)){
													$site_arr_val=array();
													$sites=$siteMapper->getSiteByStateId($state->__get("state_id"));
													foreach ($sites as $site){
														if(in_array($site->__get("site_id"), $site_id) || $site_id==null){
															$newAdded_prepaid = $consumerMapper->getTotalConsumersCountByMonth(date('Y-m-d',strtotime($curr_date."+1 day")),$last_date,$site->__get("site_id"),NULL,NULL,NULL,NULL,'0');
															$newAdded_postpaid = $consumerMapper->getTotalConsumersCountByMonth(date('Y-m-d',strtotime($curr_date."+1 day")),$last_date,$site->__get("site_id"),NULL,NULL,NULL,NULL,'1,2');
															
															$consumers=$consumerMapper->getActiveConsumerBySite($site->__get("site_id"),NULL,NULL,date('Y-m-d',strtotime($last_date."-1 day")),$curr_date,NULL,NULL,NULL);
															
															$consumer_pre=$consumers["pre_consumer"];
															$consumer_post=$consumers["post_consumer"];
															
															$reactives=$logMapper->getConsumerReactiveLogsBydate($curr_date,$last_date); 
															
															$reactive_count_pre=0;$reactive_count_post=0;
															if($reactives){
																foreach ($reactives as $reactive){
																	$arr=explode("Consumer ID: ", $reactive["message"]); 
																	$connection=explode(" ",$arr[1]);
																	$connection_id=$connection[0]; 
																	$checkReactive=$consumerMapper->checkConnectionId($site->__get("site_id"),NULL,NULL,$connection_id,NULL,NULL);
																	if($checkReactive){
																		if($checkReactive["type"]==0){
																			$reactive_count_pre=$reactive_count_pre+1;
																		}else{
																			$reactive_count_post=$reactive_count_post+1;
																		}
																	}
																}
															}
															
															$Consumers_status=$packageHistoryMapper->getMonthlypackageHistoryStatus($curr_date,$last_date,$site->__get("site_id"),NULL,NULL,NULL,NULL);
															$upgrade_pre=0;$upgrade_post=0;
															$downgrade_pre=0;$downgrade_post=0;
															if($Consumers_status){
																foreach ($Consumers_status as $Consumer_status){
																	if($Consumer_status["ph_package_id"]=="" || $Consumer_status["ph_package_id"]==NULL || $Consumer_status["ph_package_id"]=='NULL'){
																		$ph_package_id=0;
																	}else{
																		$ph_package_id=$Consumer_status["ph_package_id"];
																	}
																	
																	$lastPackage=$packageMapper->getPackageById($ph_package_id);
																	if($lastPackage){
																		$lastPackage_cost=$lastPackage->__get("package_cost");
																		$is_postpaid=$lastPackage->__get("is_postpaid");
																	}else{
																		$lastPackage_cost=0;
																		$is_postpaid=NULL;
																	}
																	if($Consumer_status["last_package"]=="" || $Consumer_status["last_package"]==NULL || $Consumer_status["last_package"]=='NULL' ){
																		$last_package=0;
																	}else{
																		$last_package=$Consumer_status["last_package"];
																	}
																	 
																	$newPackage=$packageMapper->getPackageById($last_package);
																	
																	if($newPackage){
																		$newPackage_cost=$newPackage->__get("package_cost");
																	}else{
																		$newPackage_cost=0;
																	}
																	if($lastPackage_cost>$newPackage_cost){
																		if($is_postpaid==0){
																			$upgrade_pre=$upgrade_pre+1;
																		}else{
																			$upgrade_post=$upgrade_post+1;
																		}
																		
																	}elseif($lastPackage_cost<$newPackage_cost){
																		if($is_postpaid==0){
																			$downgrade_pre=$downgrade_pre+1;
																		}else{
																			$downgrade_post=$downgrade_post+1;
																		}
																	}
																	
																}
															}
															
															$disconnection_con=$logMapper->getConsumerBannedLogsBydate($curr_date,$last_date); 
															$disconnection_pre=0;$disconnection_post=0;
															if($disconnection_con){
																foreach ($disconnection_con as $disconnection_cons){
																	$arr=explode("Consumer ID: ", $disconnection_cons["message"]);
																	$connection=explode(" ",$arr[1]);
																	$connection_id=$connection[0];
																	$checkReactive=$consumerMapper->checkConnectionId($site->__get("site_id"),NULL,NULL,$connection_id,NULL,NULL);
																	if($checkReactive){
																		if($checkReactive["type"]==0){
																			$disconnection_pre=$disconnection_pre+1;
																		}else{
																			$disconnection_post=$disconnection_post+1;
																		}
																	}
																}
															}
															
															$consumers_closing=$consumerMapper->getActiveConsumerBySite($site->__get("site_id"),NULL,NULL,$curr_date,$last_date,NULL,NULL,NULL);
															
															$consumer_pre_closing=$consumers_closing["pre_consumer"];
															$consumer_post_closing=$consumers_closing["post_consumer"];
															
															$consumerdata.="<tr><th>".$site->__get("site_name")."</th>
																<th>".intval($consumer_pre)."</th><th>".intval($consumer_post)."</th>
																<th>".intval($newAdded_prepaid)."</th><th>".intval($newAdded_postpaid)."</th>
																<th>".intval($disconnection_pre)."</th><th>".intval($disconnection_post)."</th>
																<th>".intval($upgrade_pre)."</th><th>".intval($upgrade_post)."</th>
																<th>".intval($downgrade_pre)."</th><th>".intval($downgrade_post)."</th>
																<th>".intval($reactive_count_pre)."</th><th>".intval($reactive_count_post)."</th>
																<th>".intval($consumer_pre_closing)."</th><th>".intval($consumer_post_closing)."</th></tr>"; 
														}
													}
												}
											} 
											//echo $consumerdata;exit;
											
											$consumerdata.= "</tr>"; 
											$consumerdata.=  "</tbody> </table></div>";
																	
											$newfile = 'Monthly consumer statics from '.$request->getParam("firstDate").' to '.$request->getParam("secondDate").'.xlsx';
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam(sys_get_temp_dir(), 'html');
											file_put_contents($tmpfile, $consumerdata);
										
											$objPHPExcel = new PHPExcel();
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('New Consumer Statics');
											break;
											
							case "cash_register_detail_report" :  
											  
											
											$site_id = $request->getParam("site_id", NULL);
											$agent_id = $request->getParam("agent_id", NULL);
											$cluster_id = $request->getParam("cluster_id", NULL);
											$cluster_name = $request->getParam("cluster_name", NULL);
											$entry_status = $request->getParam("entry_status");
											$month = $request->getParam("month", NULL);
											$year = $request->getParam("year", NULL);
											$page = $request->getParam("page", 1);
											$cr_entry_type = $request->getParam("cr_entry_type"); 
											$package_type = $request->getParam("package_type");
											$page_type = $request->getParam("page_type");											
											$auth=new My_Auth('user');
											$user=$auth->getIdentity()->user_role;
											
											$state_id=$auth->getIdentity()->state_id;
								 
										  
											$roleSession = new Zend_Session_Namespace('roles');
											$role_sites_id=$roleSession->site_id;
											
											$curr_date = $request->getParam("curr_dates");
											$last_date = $request->getParam("last_dates");
											  
											$zendDate = new Zend_Date($curr_date,"MMMM D, YYYY");
											$curr_date = $zendDate->toString("yyyy-MM-dd");
											  
											$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
											$last_date=$zendDate->toString("yyyy-MM-dd");
											 
											if ($site_id == "All") {
												$site_id = NULL;
												if($cluster_name=="All")
												{
													$cluster_id = NULL;
												}
											}
											if ($agent_id == "All") {
												$agent_id = NULL;
											}
											 if($entry_status!=NULL && $entry_status!="" && $entry_status!='undefined'){
												$entry_status=explode(",", $entry_status);
											}else{
												$entry_status=NULL;
											}
											
											$total_pages = 0;
											$current_page = 0;
											$crmapper = new Application_Model_CashRegisterMapper();
											$sitesMapper = new Application_Model_SitesMapper();
											$consumersMapper = new Application_Model_ConsumersMapper();
											$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
											$userMapper=new Application_Model_UsersMapper();
											
											$consumerdata = "";
											$consumerdata .= "<div>
											<table border=1 align='center'><tbody>";
											
											$consumerdata .= "<tr>
																<th>Date & Time</th>
																<th>Connection Id</th>
																<th>Consumer Name</th>
																<th>Amount</th>
																<th>Transaction Id</th> 
																<th>Type</th>
																<th>Entry Type</th>
																<th>Transaction Type</th>
																<th>Entry BY</th>
																<th>Site</th>";
														
											$crs = $crmapper->getAllCashRegisterBysiteAndCollectionId($site_id, $agent_id, $curr_date,$cluster_id,$user,$state_id,$role_sites_id,$last_date,$entry_status,$cr_entry_type,$package_type);
											
										    if ($crs) {	
												foreach ($crs as $cash_register) {
													$consumer = $consumersMapper->getConsumerById($cash_register->__get("consumer_id"));
													$date = new Zend_Date($cash_register->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
													$timestamp =  $date->toString("dd-MM-yyyy HH:mm");
													
													if ($cash_register->__get("cr_entry_type")!='DEBIT' || $cash_register->__get("transaction_type")=='(W)') {
														if ($cash_register->__get("transaction_type")=='(W)') {
															$transaction_type="Website";
															$users = $userMapper->getUserById($cash_register->__get("user_id"));
															$entry_by=$users->__get("user_fname") . " " . $users->__get("user_lname");
														} elseif ($cash_register->__get("transaction_type")=='(M)')  {
															$transaction_type="POS";
															$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cash_register->__get("user_id"));
															$entry_by=$collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
														}elseif ($cash_register->__get("transaction_type")=='(S)' && $cash_register->__get("entry_status")!='SCHEME')  {
															$transaction_type="SMS";
															$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cash_register->__get("user_id"));
															$entry_by=$collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
														}else{
															$entry_by="Server";
															$transaction_type="Server";
														} 
													}else{
															$entry_by="Server";
															$transaction_type="Server";
													} 
													
													$site = $sitesMapper->getSiteById($consumer->__get("site_id"));
													$consumerdata .= "<tr>
																		<th>".$timestamp."</th>
																		<th>".$consumer->__get("consumer_connection_id")."</th>
																		<th>".$consumer->__get("consumer_name")."</th>
																		<th>".$cash_register->__get("cr_amount")."</th>
																		<th>".$cash_register->__get("transaction_id")."</th>
																		<th>".$cash_register->__get("cr_entry_type")."</th>
																		<th>".$cash_register->__get("entry_status")."</th>
																		<th>".$transaction_type."</th>
																		<th>".$entry_by."</th>
																		<th>".$site->__get("site_name")."</th></tr>";
												}
											}
											//echo $consumerdata;exit;   
										  	
											$consumerdata.=  "</tbody> </table>
											</div>";
											if($page_type==0){
												$newfile = 'Bill register report from '.$request->getParam("last_dates").','.$request->getParam("curr_dates").'.xlsx';
											}else{
												$newfile = 'Cash register report from '.$request->getParam("last_dates").','.$request->getParam("curr_dates").'.xlsx';
											}
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam("/html/excelBackup/", 'html');
											file_put_contents($tmpfile, $consumerdata);
												
											$objPHPExcel = new PHPExcel();
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											if($page_type==0){
												$objPHPExcel->getActiveSheet()->setTitle('Bill register');
											}else{
												$objPHPExcel->getActiveSheet()->setTitle('Cash register');
											}
											
												
											break;
											
							case "sparkmeter_unit_comparision_report" :
											$stateMapper=new Application_Model_StatesMapper();
											$cashRegisterMapper = new Application_Model_CashRegisterMapper();
											$logMapper=new Application_Model_LogsMapper();
											$meterReadingMapper=new Application_Model_MeterReadingsMapper();
											$siteMapper=new Application_Model_SitesMapper();
											$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
											$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
											$feedarMapper=new Application_Model_SiteMasterMeterMapper();
											
											$currDate = $request->getParam("secondDate");
											$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
											$currDate = $zendDates->toString("yyyy-MM-dd");

											$lastDate = $request->getParam("firstDate");
											$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
											$lastDate = $zendDate->toString("yyyy-MM-dd");
											
											$state_ids = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
											$site_ids = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
											
											
											$days=intval((strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24));
											$roleSession = new Zend_Session_Namespace('roles');
											$role_sites_id=$roleSession->site_id;
											$consumerdata="";
											$StateName_Array=array();
											$zendDate = new Zend_Date();
											$zendDate->setTimezone("Asia/Calcutta");
										
											$day= intval($zendDate->toString("dd"));
											$month=intval( $zendDate->toString("MM"));
											$year= intval($zendDate->toString("yyyy"));
										
											$consumerdata.= "<div>
													<table border=1 align='center'>
			                        					<tbody><tr>
																<th>Site Name</th>
																<th>Feedar Name</th>
																<th>Description</th>
																<th>Consumption unit</th>
																<th>Unit Sold</th> 
																<th>Unit Difference</th> </tr>";
							 
							 
											$sites=$siteMapper->getSitesByStateAndSite($site_ids,$state_ids,$role_sites_id);
											$allsite_feedar=0;$allsite_unit=0;$allsite_difference=0;
											if($sites){
												foreach ($sites as $site){
													$all_feedar=0;$all_unit=0;$all_difference=0;
													$feeders=$feedarMapper->getSiteMasterById($site["site_id"]);
													if($feeders){
														foreach($feeders as $feeder){
															$feeder_unit=sprintf("%.2f",($feedarReadingMapper->getFeedarReadingByFeederIdDate($feeder->__get("id"),$lastDate,$currDate)));
															$meter_unit=sprintf("%.2f",($meterReadingMapper->getTotalUnitByFeederId($feeder->__get("id"),$currDate,$lastDate,$site["site_id"])));
															$diff=sprintf("%.2f",($feeder_unit-$meter_unit));
															$consumerdata.= "<tr>
																				<th>".$site["site_name"]."</th>
																				<th>".$feeder->__get("meter_name")."</th>
																				<th>".$feeder->__get("description")."</th>
																				<th>".$feeder_unit."</th>
																				<th>".$meter_unit."</th> 
																				<th>".$diff."</th> 
																			</tr>";
															$allsite_feedar=$allsite_feedar+$feeder_unit;
															$allsite_unit=$allsite_unit+$meter_unit;
															$allsite_difference=$allsite_difference+$diff;
															
															$all_feedar=$all_feedar+$feeder_unit;
															$all_unit=$all_unit+$meter_unit;
															$all_difference=$all_difference+$diff;
														}
														$consumerdata.= "<tr>
																	<th></th>
																	<th></th>
																	<th>".$site["site_name"]." Total </th>
																	<th>".$all_feedar."</th>
																	<th>".$all_unit."</th> 
																	<th>".$all_difference."</th> 
																</tr>";
													}
												}
												$consumerdata.= "<tr> 
																	<th></th>
																	<th></th>
																	<th> Overall </th>
																	<th>".$allsite_feedar."</th>
																	<th>".$allsite_unit."</th> 
																	<th>".$allsite_difference."</th> 
																</tr>";
											}
											//echo $consumerdata;exit;  
											$consumerdata.=  "</tbody> </table>
														</div>";
											
											$newfile = 'Consumption comparision report from '.$request->getParam("firstDate").' to '.$request->getParam("secondDate").'.xlsx';
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam("/html/excelBackup/", 'html');
											file_put_contents($tmpfile, $consumerdata);
												
											$objPHPExcel = new PHPExcel();
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('Gen Con report');
												
											break;
							
							case "plant_shutdown_report" : 
											$stateMapper=new Application_Model_StatesMapper();
											$cashRegisterMapper = new Application_Model_CashRegisterMapper();
											$logMapper=new Application_Model_LogsMapper();
											$meterReadingMapper=new Application_Model_MeterReadingsMapper();
											$siteMapper=new Application_Model_SitesMapper();
											$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
											$siteStatusMapper=new Application_Model_SiteStatusMapper();
											$feedarMapper=new Application_Model_SiteMasterMeterMapper();
											
											$currDate = $request->getParam("secondDate");
											$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
											$currDate = $zendDates->toString("yyyy-MM-dd");

											$lastDate = $request->getParam("firstDate");
											$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
											$lastDate = $zendDate->toString("yyyy-MM-dd");
											
											$state_ids = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
											$site_ids = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
											$feeder_ids = ($request->getParam("feeder_id")=="" || $request->getParam("feeder_id")=='undefined' )?NULL:explode(",", $request->getParam("feeder_id"));
											
											
											$days=intval((strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24));
											$roleSession = new Zend_Session_Namespace('roles');
											$role_sites_id=$roleSession->site_id;
											$consumerdata="";
											$StateName_Array=array();
											$zendDate = new Zend_Date();
											$zendDate->setTimezone("Asia/Calcutta");
										
											$day= intval($zendDate->toString("dd"));
											$month=intval( $zendDate->toString("MM"));
											$year= intval($zendDate->toString("yyyy"));
										
											$consumerdata.= "<div>
													<table border=1 align='center'>
			                        					<tbody><tr>
																<th>Site Name</th>
																<th>Feedar Name</th>
																<th>Description</th>
																<th>Down Date</th>
																<th>Down Hours</th> 
																</tr>";
							 
							 
											$sites=$siteMapper->getSitesByStateAndSite($site_ids,$state_ids,$role_sites_id);
											$allsite_feedar=0;
											if($sites){
												foreach ($sites as $site){
													$feeders=$feedarMapper->getSiteMasterById($site["site_id"]);
													if($feeders){
														foreach($feeders as $feeder){
															$downTimes = $siteStatusMapper->shutdownTimeCapture($site["site_id"],$feeder->__get("id"),$lastDate,$currDate);
															if($downTimes){
																foreach($downTimes as $downTime){
																		$from_data=$downTime["start_date"]."-". ($downTime["start_date"]+1);
																		$consumerdata.= "<tr>
																				<th>".$site["site_name"]."</th>
																				<th>".$feeder->__get("meter_name")."</th>
																				<th>".$feeder->__get("description")."</th>
																				<th>".date("d-M-Y",strtotime($downTime["timestamp"]))."</th>
																				<th>1</th> 
																				<th>".$from_data."</th>  
																			</tr>";
																			$allsite_feedar=$allsite_feedar+1;
																} 
															}
														}
													 }
												}
												$consumerdata.= "<tr> 
																	<th></th>
																	<th></th>
																	<th></th>
																	<th>Total Down Time </th>
																	<th>".$allsite_feedar."</th> 
																	<th></th> 
																</tr>";
											}
											$consumerdata.=  "</tbody> </table>
														</div>";
											//echo $consumerdata;exit;
											$newfile = 'Plant Shutdown report from '.$request->getParam("firstDate").' to '.$request->getParam("secondDate").'.xlsx';
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam("/html/excelBackup/", 'html');
											file_put_contents($tmpfile, $consumerdata);
												
											$objPHPExcel = new PHPExcel(); 
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('Shutdown Report');
											
											break;
											
							case "scheme_report" :
										$date = new Zend_Date();
										$date->setTimezone("Asia/Calcutta");
										$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
												
										$curr_date = $request->getParam("secondDate");
										$zendDates = new Zend_Date($curr_date,"MMMM D, YYYY");
										$curr_date = $zendDates->toString("yyyy-MM-dd");
										$conPackageMapper=new Application_Model_ConsumersPackageMapper();
																				
										$last_date = $request->getParam("firstDate");
										$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
										$last_date = $zendDate->toString("yyyy-MM-dd");
																					
										$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
										$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
											
										$stateMapper=new Application_Model_StatesMapper();
										$clusterMapper=new Application_Model_ClustersMapper();
										$cashRegisterMapper = new Application_Model_CashRegisterMapper();
										$siteMapper=new Application_Model_SitesMapper();
										$consumerMapper=new Application_Model_ConsumersMapper();
										$packageMapper=new Application_Model_PackagesMapper();
										$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
										$logMapper=new Application_Model_LogsMapper();
										$ConsumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
										$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
										$con_packageMapper=new Application_Model_ConsumersPackageMapper();
										$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
										$wattageMapper=new Application_Model_WattageMapper();
					 	
						
										$cal=intval((strtotime($curr_date)-strtotime($last_date)) / (60 * 60 * 24));
										$days = $cal; 
										
										$counter=intval($days/30);
										if($counter<=0){
											$counter=1;
										} 
										$next='+1 month';
										
										$StateName_Array=array();
										$counters=0;
										$consumerdata="";
										$roleSession = new Zend_Session_Namespace('roles');
										$role_sites_id=$roleSession->site_id;
										$consumerdata.= "<div>
											<table border=1 align='center'> 
												<tbody>";
								
											$consumerdata.= "<tr><th>Connection ID</th> 
														<th>Consumer Name</th>
														<th>Activation Date</th>
														<th>Scheme Date</th>
														<th>No. of EED</th>
														<th>Type of EED</th>
														<th>Scheme Package</th> 
														<th>Current Package</th>";
												$secondNext=$last_date;
												for($i=0;$i<$counter;$i++){
													$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
													$trans_date=date_parse_from_format("Y-m-d", $secondNext);
													$month= $trans_date["month"];
													$year= $trans_date["year"];
													$dateObj   = DateTime::createFromFormat('!m', $month);
													$monthName = $dateObj->format('M');
														$consumerdata.="<th>".$monthName.",".$year."</th>"; 
													$secondNext = date('Y-m-d', strtotime($secondNext.$next));
												}  
												$consumerdata.="<th></th>"; 
												$secondNext=$last_date;
												for($i=0;$i<$counter;$i++){
													$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
													$trans_date=date_parse_from_format("Y-m-d", $secondNext);
													$month= $trans_date["month"];
													$year= $trans_date["year"];
													$dateObj   = DateTime::createFromFormat('!m', $month);
													$monthName = $dateObj->format('M');
														$consumerdata.="<th>".$monthName.",".$year."</th>";  
													$secondNext = date('Y-m-d', strtotime($secondNext.$next));
												}
												
												$consumerdata.= "</tr>"; 
												
												$consumerSchemes=$ConsumerSchemeMapper->getConsumerSchemeByDateRange($site_id,$state_id,$role_sites_id,$curr_date,$last_date);
												if($consumerSchemes){
													foreach($consumerSchemes as $consumerScheme){
														//if($consumerScheme["consumer_connection_id"]=='BHFAKT04509'){
														$scheme_name=array();$package_name=array();$feeder_name=array();$scheme_assign_date=array();
														$consumer_details=$ConsumerSchemeMapper->getSchemesByConsumerID($consumerScheme["consumer_vaue"],$curr_date,$last_date);
														if($consumer_details){
															foreach($consumer_details as $consumer_detail){
																$scheme_name[]=$consumer_detail["scheme"];
																$packages=$packageMapper->getPackageById($consumer_detail["package_id"]);
																if($packages){
																	$package_name[]=$packages->__get("package_name");
																	$feeders=$siteMasterMapper->getMMById($packages->__get("package_name"));
																	if($feeders){
																		$feeder_name[]=$feeders->__get("meter_name")." (".$feeders->__get("description").")";
																	}
																}
																
																$scheme_assign_date[]=date('d-m-Y', strtotime($consumer_detail["timestamp"]));
															}
														}
														$package_name_other=array();$watt_other=0;
														$AllPackages=$con_packageMapper->getPackageByConsumerId($consumerScheme["consumer_vaue"]);
														if ($AllPackages) {
															foreach ($AllPackages as $AllPackage_data){
																$package_name_other[]=$AllPackage_data["package_name"];
																$watt=$wattageMapper->getWattageBywattId($AllPackage_data["wattage"]);
																$watt_other=$watt_other+$watt->__get("wattage");
																
															}
														}
														
														$consumerdata.= "<tr><td>".$consumerScheme["consumer_connection_id"]."</td> 
																			<td>".$consumerScheme["consumer_name"]."</td>
																			<td>".date('d-m-Y', strtotime($consumerScheme["consumer_act_date"]))."</td>
																			<td>".implode(",",$scheme_assign_date)."</td>
																			<td>".count($consumer_details)."</td>
																			<td>".implode(",",$scheme_name)."</td>
																			<td>".implode(",",$package_name)."</td>
																			<td>".implode(",",$package_name_other)."</td> ";
																			$secondNext=$last_date;
																			for($i=0;$i<$counter;$i++){
																				$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
																				$trans_date=date_parse_from_format("Y-m-d", $secondNext);
																				$month= $trans_date["month"];
																				$year= $trans_date["year"];
																				
																				$cashRegister_amount=$cashRegisterMapper->getMonthDebitByConsumerId($consumerScheme["consumer_vaue"],$month,$year);
																				if($cashRegister_amount){
																					$consumerdata .= "<td>" .$cashRegister_amount. "</td>";
																				}else{
																					$consumerdata .= "<td></td>";
																				}
																				$secondNext = date('Y-m-d', strtotime($secondNext.$next));
																			}
																			$consumerdata.="<th></th>"; 
																			$secondNext=$last_date;
																			for($i=0;$i<$counter;$i++){
																				$units=0;
																				$secondCurr = date('Y-m-d', strtotime($secondNext.$next));
																				$trans_date=date_parse_from_format("Y-m-d", $secondNext);
																				$month= $trans_date["month"];
																				$year= $trans_date["year"];
																					
																				if($consumerScheme["site_id"]==62 || $consumerScheme["site_id"]==31){
																					$units=$meterReadingsMapper->getTotalUnitByOnlyConsumerId($consumerScheme["consumer_vaue"],$month,$year);
																				}else{
																					//print_r($AllPackages);exit;
																					$date_val=$year."-".sprintf("%02d",$month)."-01"; 
																					$paymentHistoryMapper=new Application_Model_PackageHistoryMapper();
																					$AllPackage_data=$paymentHistoryMapper->getpackageHistoryForLastEntry($date_val,$consumerScheme["consumer_vaue"]);
																					//print_r($AllPackage_data);exit;
																					if ($AllPackage_data) {
																						//foreach ($allPackages_data as $AllPackage_data){
																							$watt=$wattageMapper->getWattageBywattId($AllPackage_data["wattage"]);
																							if($AllPackage_data["is_postpaid"]!=0){
																								$units_data=$meterReadingsMapper->getTotalUnitByOnlyConsumerId($consumerScheme["consumer_vaue"],$month,$year);
																							}else{
																								$feedarHrs=0;
																								$site_meter_ids=explode(",", $consumerScheme['site_meter_id']);
																								$countFeedar=0;
																								if(($site_meter_ids)>0){
																									for ($k=0;$k<count($site_meter_ids);$k++){
																										$feedarDetail=$siteMasterMapper->getMMById($site_meter_ids[$k]);
																										if($feedarDetail){
																											$countFeedar=$countFeedar+1;
																											if($feedarDetail->__get("is_24_hr")==1){
																												$feedarHr=24;
																											}elseif ($feedarDetail->__get("start_time") >$feedarDetail->__get("end_time")){
																												$feedarHr=24+($feedarDetail->__get("end_time") - $feedarDetail->__get("start_time"));
																											}else{
																												$feedarHr=$feedarDetail->__get("end_time") - $feedarDetail->__get("start_time");
																											}
																											$feedarHrs=$feedarHrs+$feedarHr;
																										}
																									}
																								}
																								$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month,$year);
																								$units=$units+floatval(($watt->__get("wattage")*$feedarHrs*$total_days_in_month)/1000);
																							}
																						//}
																					}
																					
																					$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month,$year);
																					$consumer_scheme=$ConsumerSchemeMapper->getschemePackageByConsumerID($consumerScheme["consumer_vaue"]);
																					if($consumer_scheme){
																						foreach($consumer_scheme as $consumer_schemes){
																							$zendDate = new Zend_Date($consumer_schemes["start_date"],"yyyy-MM-dd");
																							$lastTimestamp = $zendDate->toString("yyyy-MM-dd");
																							$discount_month=$consumer_schemes["discount_month"];
																							$feeder_hours=$consumer_schemes["feeder_hours"];
																							$watt=$wattageMapper->getWattageBywattId($consumer_schemes["wattage"]);
																							$currTimestamps = date('Y-m-d', strtotime($lastTimestamp."+".$discount_month." month")); 
																							$currTimestamp = date('Y-m-d', strtotime($currTimestamps."-1 day")); 
																							if(strtotime($lastTimestamp)<=strtotime($date_val) && strtotime($date_val)<=strtotime($currTimestamp)){
																								$dailyUnit=sprintf("%.2f",($consumer_schemes["wattage"]*$feeder_hours*$total_days_in_month)/1000);
																								$units=$units+$dailyUnit;
																							}  
																							
																						} 
																					}
																					
																				}
																				$consumerdata.="<td>".sprintf("%.2f",($units))."</td>";  
																				$secondNext = date('Y-m-d', strtotime($secondNext.$next));
																			}
																			
																			$consumerdata.= "</tr>";   
													}
														//}
												} 
											
											//echo $consumerdata;exit;
											$consumerdata.=  "</tbody> </table></div>";
																	
											$newfile = 'Scheme report from '.$request->getParam("firstDate").' to '.$request->getParam("secondDate").'.xlsx';
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam(sys_get_temp_dir(), 'html');
											file_put_contents($tmpfile, $consumerdata);
										
											$objPHPExcel = new PHPExcel();
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('Scheme Report');
											break;
								
						case "outstanding_report" :
										$date = new Zend_Date();
										$date->setTimezone("Asia/Calcutta");
										$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
										$trans_date=date_parse_from_format("Y-m-d", $timestamp);
										$month= $trans_date["month"];
										$year= $trans_date["year"];		
										$curr_date = $request->getParam("secondDate");
										$zendDates = new Zend_Date($curr_date,"MMMM D, YYYY");
										$curr_date = $zendDates->toString("yyyy-MM-dd");
										$conPackageMapper=new Application_Model_ConsumersPackageMapper();
																				
										$last_date = $request->getParam("firstDate");
										$zendDate = new Zend_Date($last_date,"MMMM D, YYYY");
										$last_date = $zendDate->toString("yyyy-MM-dd");
																					
										$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL: $request->getParam("state_id");
										$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL: $request->getParam("site_id");
											
										$stateMapper=new Application_Model_StatesMapper();
										$clusterMapper=new Application_Model_ClustersMapper();
										$cashRegisterMapper = new Application_Model_CashRegisterMapper();
										$siteMapper=new Application_Model_SitesMapper();
										$consumerMapper=new Application_Model_ConsumersMapper();
										$packageMapper=new Application_Model_PackagesMapper();
										$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
										$logMapper=new Application_Model_LogsMapper();
										$ConsumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
										$siteMasterMapper=new Application_Model_SiteMasterMeterMapper();
										$con_packageMapper=new Application_Model_ConsumersPackageMapper();
										$meterReadingsMapper = new Application_Model_MeterReadingsMapper();
										$wattageMapper=new Application_Model_WattageMapper();
					 	 
										$StateName_Array=array();
										$counters=0;
										$consumerdata="";
										$roleSession = new Zend_Session_Namespace('roles');
										$role_sites_id=$roleSession->site_id;
										$consumerdata.= "<div>
											<table border=1 align='center'> 
												<tbody>";
								
											$consumerdata.= "<tr><th>Connection ID</th> 
														<th>Consumer Name</th>
														<th>Consumer status</th>
														<th>Total Bill in duration</th>
														<th>CHG</th>
														<th>ACT</th>
														<th>WATER</th> 
														<th>EED</th>
														<th>MED</th>
														<th>OTHERS</th>
														<th>Total Outstanding</th></tr> ";
												 
											$consumers=$consumerMapper->getTotalConsumerBySiteIdAndMonthVal($month,$year,$site_id,$state_id,$role_sites_id);	
											if($consumers){
												foreach($consumers as $consumer){
													 $cashRegs=$cashRegisterMapper->getOutstandingConsumer($last_date,$curr_date,$consumer["consumer_id"]);
													
													 if(!$cashRegs){
														$total_bill=$cashRegisterMapper->getTotalBillConsumerId($last_date,$curr_date,$consumer["consumer_id"]);
														$chg = intval($cashRegisterMapper->getOtherAmtByConsumerId($consumer["consumer_id"],'CHG')) - intval($cashRegisterMapper->getDiscountByConsumerId($consumer["consumer_id"],'CHG'));
														$otp = intval($consumerMapper->getRemainingActivation($consumer["consumer_id"])) - intval($cashRegisterMapper->getDiscountByConsumerId($consumer["consumer_id"],'ACT'));
														$eed = intval($cashRegisterMapper->getOtherAmtByConsumerId($consumer["consumer_id"],'EED')) - intval($cashRegisterMapper->getDiscountByConsumerId($consumer["consumer_id"],'EED'));
														$med = intval($cashRegisterMapper->getOtherAmtByConsumerId($consumer["consumer_id"],'MED')) - intval($cashRegisterMapper->getDiscountByConsumerId($consumer["consumer_id"],'MED'));
														$water = intval($cashRegisterMapper->getOtherAmtByConsumerId($consumer["consumer_id"],'WATER')) - intval($cashRegisterMapper->getDiscountByConsumerId($consumer["consumer_id"],'WATER'));
														$OTHERS = intval($cashRegisterMapper->getOtherAmtByConsumerId($consumer["consumer_id"],'OTHERS')) - intval($cashRegisterMapper->getDiscountByConsumerId($consumer["consumer_id"],'OTHERS'));
														 
														 $consumerdata.= "<tr><td>".$consumer["consumer_connection_id"]."</td> 
																			<td>".$consumer["consumer_name"]."</td>
																			<td>".$consumer["consumer_status"]."</td>
																			<td>".$total_bill."</td>
																			<td>".$chg."</td>
																			<td>".$otp."</td>
																			<td>".$water."</td>
																			<td>".$eed."</td>
																			<td>".$med."</td>
																			<td>".$OTHERS."</td>
																			<td>".($chg+$otp+$otp+$water+$eed+$med+$OTHERS)."</td></tr>";
															
													 }
												}
											}
											
											$consumerdata.=  "</tbody> </table></div>";
											 
											$newfile = 'Outstanding report from '.$request->getParam("firstDate").' to '.$request->getParam("secondDate").'.xlsx';
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam(sys_get_temp_dir(), 'html');
											file_put_contents($tmpfile, $consumerdata);
										
											$objPHPExcel = new PHPExcel();
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('Outstanding Report');
											break;		 					
					}
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		              //$objWriter->setprecalculateformulas(false);
		$objWriter->save($newfile);
		 
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings);
		gc_enable() ;gc_collect_cycles();
		/* $arr = array(
    					"data" => "http://taraurja.in/".$newfile
    			);
		$json = json_encode($arr, JSON_PRETTY_PRINT);
    	echo $json;exit;*/
	 
		

	if($from !=null && $from!=="")
		{
		defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
		  $date = new Zend_Date();
			$mail = new Zend_Mail(); 
			 $config = array('auth' => 'login',
					'username' => 'postmaster@iwsspl.com',
					'ssl' => 'ssl',
					'port' => 465,
					'password' => '68b1f5b3204ae4f4072c4afb59e241c0'); 
				
			$filename =PUBLIC_PATH . "/html/".$newfile;
			$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
            $at = $mail->createAttachment(file_get_contents($filename));
            $date = new Zend_Date();
            $at->filename = $newfile;
			
			$mail->setFrom("postmaster@taraurja.in", "TARAUrja.in CRON");
			 
			if(strpos($newfile, 'RF - Monthly report') !== false){
			
          		$email_user=$this->getEmailsAction('RF - Monthly report');
           		$mail->setSubject("RF Monthly Report ");
				
			}elseif (strpos($newfile, 'SPI Monthly Report') !== false){
			
				$email_user=$this->getEmailsAction('SPI Monthly Report');
				$mail->setSubject("SPI Monthly Report");
				
			}elseif(strpos($newfile, 'Mixed Mode Consumers') !== false){
				
				$email_user=$this->getEmailsAction('Mixed Mode Consumers');
				$mail->setSubject("Mixed Mode Consumers");
				
			}elseif(strpos($newfile, 'Bill Reduction Report') !== false){
				$email_user=$this->getEmailsAction('Bill Reduction Report');
				$mail->setSubject("Bill Reduction Report");
				
			}elseif(strpos($newfile, 'Daily transaction Report') !== false){
			
				$email_user=$this->getEmailsAction('Daily transaction Report');
				$mail->setSubject("Daily transaction Report");
				
			}elseif(strpos($newfile, 'Daily Consumption Report') !== false){
			
				$email_user=$this->getEmailsAction('Daily Consumption Report');
				$mail->setSubject("Daily Consumption Report");
				
			}else{
				$mail->setSubject("Monthly Report");
			}
			//print_r($email_user);exit;
			$mail->addTo($email_user, 'recipient');
			$mail->addCc('taraurja@gmail.com');
			
            $mail->setBodyHtml("PFA");
			
            if ($mail->send($transport)) {
                echo $msg= "SUCCESS";
            } else {
                echo $msg= "ERROR WHILE MAILING";
            }
	  }else{
			
		  	defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
		  	$date = new Zend_Date();
		  	$mail = new Zend_Mail();
		  	$config = array('auth' => 'login',
					'username' => 'postmaster@iwsspl.com',
					'ssl' => 'ssl',
					'port' => 465,
					'password' => '68b1f5b3204ae4f4072c4afb59e241c0');
		  	
		  	$filename =PUBLIC_PATH . "/html/".$newfile;
		  	$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
		  	$at = $mail->createAttachment(file_get_contents($filename));
		  	$date = new Zend_Date();
		  	$at->filename = $newfile;
		  		
		   
		  	$mail->setFrom("postmaster@iwsspl.com", "TARAUrja.in CRON");
		  	$auth=new My_Auth('user');
		  	$email_id=$auth->getIdentity()->user_email;
		  	
		  	$mail->addTo($email_id, 'recipient');
			//$mail->addCc('taraurja@gmail.com'); 
		  	$mail->setSubject($newfile);
		   
		  	$mail->setBodyHtml("PFA");
		  	if ($mail->send($transport)) {
		  		echo $msg= "SUCCESS";
		  	} else {
		  		echo $msg= "ERROR WHILE MAILING";
		  	}
		  }	

	}


	public function getEmailsAction($filename){
	
		$userMapper=new Application_Model_UsersMapper();
		$reportPermissionMapper=new Application_Model_ReportPermissionMapper();
		$rps=$reportPermissionMapper->getReportsByReportName($filename);
		if($rps){
			$rpReport=explode(",", $rps["user_id"]);
			$user_email=array();
			for ($r=0;$r<count($rpReport);$r++){
				$users=$userMapper->getUserById($rpReport[$r]);
				$user_email[]=$users->__get("user_email");
			}
		}
		return $user_email;
	}
	
		public function pdfGenerateAction($data,$filename){
	
		date_default_timezone_set('Asia/Kolkata');
		$pdf=new HTML2FPDF('P','mm',"a2");
		$pdf->AddPage();
	
		$strContent=$data;
		$pdf->WriteHTML($strContent);
	
		$pdf->Output($filename);
		return $filename;
	}
	public function sendMailAction($filename)
	{
	 
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
	
		defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
	
		$mail = new Zend_Mail();
		 $config = array('auth' => 'login',
					'username' => 'postmaster@iwsspl.com',
					'ssl' => 'ssl',
					'port' => 465,
					'password' => '68b1f5b3204ae4f4072c4afb59e241c0');
	
		$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
	
	
		$filenames = PUBLIC_PATH . "/html/" . $filename;
		$content = file_get_contents($filenames);
		$attachment = new Zend_Mime_Part($content);
		$attachment->type = 'application/pdf';
		$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
		$attachment->encoding = Zend_Mime::ENCODING_BASE64;
		$attachment->filename = $filename;
	
		$mail->addAttachment($attachment);
	
		$auth=new My_Auth('user');
		$email_id=$auth->getIdentity()->user_email;
		
		$mail->setFrom("postmaster@iwsspl.com", "TARAUrja.in CRON");
		$mail->addTo($email_id, 'recipient');
		 
		$mail->setSubject($filename);
		$mail->setBodyHtml("PFA");
	
		if ($mail->send($transport)) {
	
			echo "Mail sent successfully...";
		} else {
			echo "Mail could not be sent...";
		}
	}
	
}



class ReadFilter implements PHPExcel_Reader_IReadFilter {

	public function __construct($fromColumn, $toColumn) {
		$this->columns = array();
		$toColumn++;
		while ($fromColumn !== $toColumn) {
			$this->columns[] = $fromColumn++;
		}
	}

	public function readCell($column, $row, $worksheetName = '') {
		if (in_array($column, $this->columns)) {
			return true;
		}
		return false;
	}
}


