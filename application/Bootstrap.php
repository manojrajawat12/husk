<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initErrorControllerSwitcher()
    {
        $this->bootstrap('layout'); 
        $this->bootstrap('frontController'); 
        $layout = $this->getResource('layout'); 
        $front = $this->getResource('frontController');
        $front->registerPlugin(new My_ErrorControllerSwitcher());
    }
    
    protected function _initPageControls()
    {
            Zend_View_Helper_PaginationControl::setDefaultViewPartial('pagination-controls.phtml');
    }
}

