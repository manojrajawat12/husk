<?php
class Application_Model_MeterInventory
{
    private $id;
    private $meter_no;
    private $reading;
    private $status;
    private $brand;
    private $consumer_id;
    private $meter_status;
    private $add_timestamp;
    private $remove_timestamp;

    public function __construct($change_row = null)
    {
        if( !is_null($change_row) && $change_row instanceof Zend_Db_Table_Row ) {

            $this->id = $change_row->id;
            $this->meter_no = $change_row->meter_no;
            $this->reading = $change_row->reading;
            $this->status = $change_row->status;
            $this->brand = $change_row->brand;
            $this->consumer_id = $change_row->consumer_id;
            $this->meter_status = $change_row->meter_status;
            $this->add_timestamp = $change_row->add_timestamp;
            $this->remove_timestamp = $change_row->remove_timestamp;
        }
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}