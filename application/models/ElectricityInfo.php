<?php

class Application_Model_ElectricityInfo
{
	private $id;
    private $node;
    private $version;
    private $device_name;
    private $iccid;
    private $site_id;
	private $votage;
    private $consumer_id;
    private $volt;
    private $info_date; 
 
    public function __construct($electric_row = null)
    {
        if( !is_null($electric_row) && $electric_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $electric_row->id;
                $this->node = $electric_row->node;
                $this->version = $electric_row->version;
                $this->device_name = $electric_row->device_name;
                $this->iccid = $electric_row->iccid;
                $this->site_id = $electric_row->site_id;
				 $this->votage = $electric_row->votage;
                $this->consumer_id = $electric_row->consumer_id;
                $this->info_date = $electric_row->info_date;
                $this->volt = $electric_row->volt;
        }
    }
 	public function __set($name, $value)
    {
    		$this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }
}

