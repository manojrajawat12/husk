<?php

class Application_Model_Query
{
    private $id;
    private $query;
	private $parent_id;

    public function __construct($query_row = null)
    {
        if( !is_null($query_row) && $query_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $query_row->id;
                $this->query = $query_row->query;
				$this->parent_id = $query_row->parent_id;
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "ap_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

