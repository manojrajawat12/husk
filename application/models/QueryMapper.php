<?php
class Application_Model_QueryMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Query();
    }
	
	public function addNewQuery(Application_Model_Query $query)
    {
        $data = array(
    		'query' => $query->__get("query"),
            'parent_id' => $query->__get("parent_id"),
        );
        $result = $this->_db_table->insert($data);

        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }

    public function getQueryById($query_id)
    {
    	
        $result = $this->_db_table->find($query_id);
		
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $query = new Application_Model_Query($row);
        return $query;
    }

    public function getAllQueries()
    {
    	$where = NULL;
        $result = $this->_db_table->fetchAll($where,array('id ASC')); 
        if( count($result) == 0 ) {
                return false;
        }
        $query_object_arr = array();
        foreach ($result as $row)
        {
                $query_object = new Application_Model_Query($row);
                array_push($query_object_arr,$query_object);
        }
        return $query_object_arr;
    } 
	
	 public function getAllParentQueries($parent)
    {
        $query = "SELECT * FROM `queries` WHERE `parent_id`= ".$parent." ";
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();

        if( count($result) == 0 ) {
                return false;
        }else{
            return $result;
        }
    } 

    public function deleteQueryById($query_id)
    {
        $where =  "`id`=".$query_id." OR `parent_id`=".$query_id."";
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function updateQueryById($query_id, $query)
    {
        $where =  "`id`=".$query_id;
        $data = array('query' => $query );
        $result = $this->_db_table->update($data, $where);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
