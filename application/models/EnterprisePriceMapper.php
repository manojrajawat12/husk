<?php
class Application_Model_EnterprisePriceMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_EnterprisePrices();
    }

    public function addNewEnterprisePrice(Application_Model_EnterprisePrice $enterprise)
    {
        $data = array(
			'unit_from' => $enterprise->__get("unit_from"),
			'unit_to' => $enterprise->__get("unit_to"),
			'enterprise_price' => $enterprise->__get("enterprise_price"),
        	'enterprise_scheme'=> $enterprise->__get("enterprise_scheme"),
   		);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return $result;
        }
    }
    public function getEnterprisePriceById($enterprise_price_id)
    {
        $result = $this->_db_table->find($enterprise_price_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $enterprise= new Application_Model_EnterprisePrice($row);
        return $enterprise;
    }
    public function getAllEnterprisePrice()
    {
        $result = $this->_db_table->fetchAll(null,array('enterprise_scheme ASC'));
        $enterprise_object_arr = array();
        if( count($result) == 0 ) {
                return   $enterprise_object_arr ;
        }
       
        foreach ($result as $row)
        {
                $enterprise_object = new Application_Model_EnterprisePrice($row);
                array_push($enterprise_object_arr,$enterprise_object);
        }
        return $enterprise_object_arr;
    }
     public function updateEnterprisePrice(Application_Model_EnterprisePrice $enterpriseprice)
    {
      /*   $data = array(
			'unit_from' => $enterpriseprice->__get("unit_from"),
			'unit_to' => $enterpriseprice->__get("unit_to"),
			'enterprise_price' => $enterpriseprice->__get("enterprise_price"),
        	'enterprise_scheme' => $enterpriseprice->__get("enterprise_scheme")
   		);
        $where = "enterprise_price_id = " . $enterpriseprice->__get("enterprise_price_id");
       
        $result = $this->_db_table->update($data, $where);
      
        if($result==0)
        {
            return false;
        }
        else
        {
            return true;
        } */
        
        $unit_from=  $enterpriseprice->__get("unit_from");
        $unit_to=$enterpriseprice->__get("unit_to");
        $enterprise_price=$enterpriseprice->__get("enterprise_price");
        $enterprise_scheme=$enterpriseprice->__get("enterprise_scheme");
        $enterprise_price_id= $enterpriseprice->__get("enterprise_price_id");
        
        $query ="update enterprise_price set unit_from=".$unit_from.",unit_to=".$unit_to.",enterprise_price=".$enterprise_price." where enterprise_price_id=".$enterprise_price_id;

        $stmt = $this->_db_table->getAdapter()->query($query);
        $result=$stmt->execute();
        
        if($result)
        {
        	return true;
        }
    }    
public function deleteEnterprisePriceById($enterprise_price_id)
    {
        $where = "enterprise_price_id = " . $enterprise_price_id;
        $result = $this->_db_table->delete($where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
  public function  getEnterprisePriceByUnit($consumer_id)
   {
	   	$result = $this->_db_table->fetchAll(null,array('unit_from ASC'));
	   	if( count($result) == 0 ) {
	   		return false;
	   	}
	   	$enterprise_object_arr = array();
	   	foreach ($result as $row)
	   	{
	   		$enterprise_object = new Application_Model_EnterprisePrice($row);
	   		array_push($enterprise_object_arr,$enterprise_object);
	   	}
	   	return $enterprise_object_arr;
   }
   
   public function  deleteEnterprisePriceBySchemeId($scheme_id)
   {
   		$where = "enterprise_scheme = " . $scheme_id;
        $result = $this->_db_table->delete($where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
   }
   public function  getEnterprisePriceBySchemeId($enterprise_scheme)
   {
   	    $query = " SELECT * From enterprise_price where enterprise_scheme ='$enterprise_scheme'";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	//print_r($result);
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$enterprise_scheme_object_arr = array();
    	foreach ($result as $row)
    	{
    		$enterprise_scheme_object = new Application_Model_EnterprisePrice($row);
    		$enterprise_scheme_object->__set("unit_from", $row["unit_from"]);
    		$enterprise_scheme_object->__set("unit_to", $row["unit_to"]);
    		$enterprise_scheme_object->__set("enterprise_price", $row["enterprise_price"]);
    		$enterprise_scheme_object->__set("enterprise_scheme", $row["enterprise_scheme"]);
    		$enterprise_scheme_object->__set("enterprise_price_id", $row["enterprise_price_id"]);
    		array_push($enterprise_scheme_object_arr,$enterprise_scheme_object);
    	}
    	return $enterprise_scheme_object_arr;
   }
   
}

