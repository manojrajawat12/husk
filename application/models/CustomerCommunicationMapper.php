<?php
class Application_Model_CustomerCommunicationMapper
{
    protected $_db_table;
    public function __construct()
    {
        $this->_db_table = new Application_Model_DbTable_CustomerCommunication();
    }

    public function addNewCustomerCommunication(Application_Model_CustomerCommunication $Comm)
    {

        $data = array(
                'state_id' => $Comm->__get("state_id"),
                'site_id' => $Comm->__get("site_id"),
                'consumer_id' => $Comm->__get("consumer_id"),
                'query_id' => $Comm->__get("query_id"),
				'sub_query'=> $Comm->__get("sub_query_id"),
                'query_comment' => $Comm->__get("query_comment"),
        		'query_status' => $Comm->__get("query_status"),  
        		'user_id' => $Comm->__get("user_id"),
        		'timestamp' => $Comm->__get("timestamp"),
        		'user_type' => $Comm->__get("user_type"),
				'attachment' => $Comm->__get("attachment"),
				'attach_sec' => $Comm->__get("attach_sec"),
                'entry_date' => $Comm->__get("entry_date"),
                'effective_date' => $Comm->__get("effective_date"),
                'amount' => $Comm->__get("amount") 
        );

        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return $result;
        }
    }
     
    public function getConsumerConnectionById($id)
    {
        
        $result = $this->_db_table->find($id);
        
        if( count($result) == 0 ) {
            $com = new Application_Model_CustomerCommunication();
            return $com;
        }
        $row = $result->current();
        $com = new Application_Model_CustomerCommunication($row);
		return $com; 
    }
   
    public function getAllCustomerQueries($status=NULL,$role_site_id=NULL)
    {
        $where = array();
        if($status!=NULL) {
            $where = "  query_status='".$status."'";
        }else{
			$where = "  query_status!='Inactive'"; 
		}
		if($role_site_id!=NULL){
			$where .= "  and site_id in (".implode(",",$role_site_id).")";
		}
		    
        $result = $this->_db_table->fetchAll($where,array('timestamp DESC'));
        if( count($result) == 0 ) {
            return false;
        }
		 
        $customer_query_object_arr = array();
        foreach ($result as $row)
        {
            $customer_query_object = new Application_Model_CustomerCommunication($row);
            array_push($customer_query_object_arr,$customer_query_object);
        }
		//print_r($customer_query_object_arr);exit;
        return $customer_query_object_arr;
    }
	
    public function updateCustomerCommunication(Application_Model_CustomerCommunication $customercommunication)
    {
        
        $data = array(
                'state_id' => $customercommunication->__get("state_id"),
                'site_id' => $customercommunication->__get("site_id"),
                'consumer_id' => $customercommunication->__get("consumer_id"),
				'query_id' => $customercommunication->__get("query_id"),
                'sub_query' => $customercommunication->__get("sub_query_id"),
                'amount' => $customercommunication->__get("amount"),
				'entry_date' => $customercommunication->__get("entry_date"),
                'effective_date' => $customercommunication->__get("effective_date"),
                'query_comment' => $customercommunication->__get("query_comment"),
				'attachment' => $customercommunication->__get("attachment"),
				'attach_sec' => $customercommunication->__get("attach_sec")				
            );
			 
        $where = "id = " . $customercommunication->__get("id");

        $result = $this->_db_table->update($data,$where);

        if($result==0) {
            return false;
        } else {
            return true;
        }
    } 
	
    public function deleteCustomerCommunicationById($customer_query_id)
    {
        $where = "id = " . $customer_query_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
            return false; 
        }
        else 
        {
            return true;
        }
    }
    public function getAllQueriesOfACustomer($consumer_id){
    	
		$query = "SELECT * FROM customer_communication WHERE query_status!='Inactive' and  consumer_id = " . $consumer_id;
    
    	$customerqueries = $this->_db_table->getAdapter()->query($query);
    	$result = $customerqueries->fetchAll();
    	
    	if (count($result) == 0) {
    		return false;
    	}else {
    		return $result;
    	}
    }
	
	 public function updateCustomerCommunicationStatus(Application_Model_CustomerCommunication $customercommunication)
    {
        
        $data = array(
                'query_status' => $customercommunication->__get("query_status"),
                'verified_by' => $customercommunication->__get("verified_by"),
                'verified_date' => $customercommunication->__get("verified_date")
            );
        $where = "id = " . $customercommunication->__get("id");

        $result = $this->_db_table->update($data,$where);

        if($result==0) {
            return false;
        } else {
            return true;
        }
    } 
}