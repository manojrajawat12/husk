<?php
class Application_Model_FormsMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Forms();
	}
/*--------------------- 22 Jan 2018 (start) ----------------------------------*/
	public function addNewForms(Application_Model_Forms $Forms)
	{

		$data = array(
				'form_name' => $Forms->__get("form_name"),
				'form_attachment' => $Forms->__get("form_attachment"),
				'time_stamp' => $Forms->__get("time_stamp"),
				'user_id' => $Forms->__get("user_id"),
				'user_type' => $Forms->__get("user_type"),
				'status' => 'enable'
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
/*--------------------- 22 Jan 2018 (end) ----------------------------------*/

	public function getAllForms()
	{
		$where = " status!='disable'";
		
		$result = $this->_db_table->fetchAll($where,array('id'));

		if( count($result) == 0 ) {
			return false;
		}
		$Form_object_arr = array();
		foreach ($result as $row)
		{
			$Form_object = new Application_Model_Forms($row);
			array_push($Form_object_arr,$Form_object);
		}
		return $Form_object_arr;
	}

	public function deleteFormsById($id)
	{
		//$where = "id = " . $id;
		//$result = $this->_db_table->delete($where);
		$data = array(
    			'status' => 'disable',
    	);
    	$where = "id = " . $id;
    	 
    	$result = $this->_db_table->update($data,$where); 
		if(count($result)==0)
		{
			return false;
		}
		else
		{
	
			return true;
		}
	}
}