<?php
class Application_Model_SparkData
{
    private $id;
    private $code;
    private $credit_balance;
    private $debt_balance;
    private $ground_id;
	private $ground_name;
    private $customer_id;
    private $active;
    private $address;
    private $coords;
	private $current_tariff_name;
    private $is_running_plan;
    private $last_energy;
    private $last_energy_datetime;
    private $operating_mode;
	private $plan_balance;
	private $serial;
    private $total_cycle_energy;
    private $customer_name;
    private $phone_number;
    private $phone_number_verified;
	private $timestamp;
	private $unit;
    
	public function __construct($spark_row = null)
    {
        if( !is_null($spark_row) && $spark_row instanceof Zend_Db_Table_Row ) {
				
                $this->id = $spark_row->id;
                $this->code = $spark_row->code;
                $this->credit_balance = $spark_row->credit_balance;
                $this->debt_balance = $spark_row->debt_balance;
                $this->ground_id = $spark_row->ground_id;
				$this->ground_name = $spark_row->ground_name;
                $this->customer_id = $spark_row->customer_id;
                $this->active = $spark_row->active;
                $this->address = $spark_row->address;
                $this->coords = $spark_row->coords;
				$this->current_tariff_name = $spark_row->current_tariff_name;
                $this->is_running_plan = $spark_row->is_running_plan;
                $this->last_energy = $spark_row->last_energy;
                $this->last_energy_datetime = $spark_row->last_energy_datetime;
                $this->operating_mode = $spark_row->operating_mode;
				$this->plan_balance = $spark_row->plan_balance;
				$this->serial = $spark_row->serial;
                $this->total_cycle_energy = $spark_row->total_cycle_energy;
                $this->customer_name = $spark_row->customer_name;
                $this->phone_number = $spark_row->phone_number;
                $this->phone_number_verified = $spark_row->phone_number_verified;
				$this->timestamp = $spark_row->timestamp;
				$this->unit = $spark_row->unit; 
       }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }
}

 