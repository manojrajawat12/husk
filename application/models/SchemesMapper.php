<?php
class Application_Model_SchemesMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Schemes();
	}

	public function addNewScheme(Application_Model_Schemes $scheme)
	{
		$data = array(
				'scheme' => $scheme->__get("scheme"),
				'scheme_type' => $scheme->__get("scheme_type"),
				'celamed_id' => $scheme->__get("celamed_id"),
				'down_payment' => $scheme->__get("down_payment"),
				'emi_amt' => $scheme->__get("emi_amt"),
				'emi_duration' => $scheme->__get("emi_duration"),
				'discount_amt' => $scheme->__get("discount_amt"),
				'discount_month' => $scheme->__get("discount_month"),
				'gen_scheme_type' => $scheme->__get("gen_scheme_type"),
				'advance_payment' => $scheme->__get("advance_payment"),
				'advance_dis_month' => $scheme->__get("advance_dis_month"),
				'advance_dis_amt' => $scheme->__get("advance_dis_amt"),
				'flat_dis' => $scheme->__get("flat_dis"),
				'per_dis' => $scheme->__get("per_dis"),
				'state_id' => $scheme->__get("state_id"),
				'site_id' => $scheme->__get("site_id"),
				'package_id' => $scheme->__get("package_id"),
				'consumer_id' => $scheme->__get("consumer_id"),  
				'start_date' => $scheme->__get("start_date"),
				'end_date' => $scheme->__get("end_date"),
				'enable_date' => $scheme->__get("enable_date"),
				'scheme_applied' => $scheme->__get("scheme_applied"),
				'is_postpaid' => $scheme->__get("is_postpaid"),
				'consumer_type' => $scheme->__get("consumer_type"),
				'package_continue' => $scheme->__get("package_continue"),
				'scheme_continue' => $scheme->__get("scheme_continue"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	public function getSchemeById($scheme_id)
	{
		$result = $this->_db_table->find($scheme_id);
		if( count($result) == 0 ) {
			$scheme = new Application_Model_Schemes();
			return $scheme;
		}
		$row = $result->current();
		$scheme = new Application_Model_Schemes($row);
		return $scheme;
	}
	
	public function getAllSchemes($scheme_apply=NULL,$enable=NULL)
	{
		$date = new Zend_Date();
		$date->setTimezone("Asia/Kolkata");
		$curr_date=$date->toString("yyyy-MM-dd");
		$where=array();
		if($enable==NULL){
			$where = "scheme_status='enable'"; 
		}
		if($scheme_apply!=NULL)
		{
		 	$where = "date(enable_date)<='".$curr_date."' and date(start_date)>='".$curr_date."' and scheme_status='enable'";
		}
		  
		$result = $this->_db_table->fetchAll($where,array('scheme_id ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$scheme_object_arr = array();
		foreach ($result as $row)
		{
			$scheme_object = new Application_Model_Schemes($row);
			array_push($scheme_object_arr,$scheme_object);
		}
		return $scheme_object_arr;
	}
	
	public function updateScheme(Application_Model_Schemes $scheme)
	{
		$data = array(
				'scheme' => $scheme->__get("scheme"),
				'scheme_type' => $scheme->__get("scheme_type"),
				'celamed_id' => $scheme->__get("celamed_id"),
				'down_payment' => $scheme->__get("down_payment"),
				'emi_amt' => $scheme->__get("emi_amt"),
				'emi_duration' => $scheme->__get("emi_duration"),
				'discount_amt' => $scheme->__get("discount_amt"),
				'discount_month' => $scheme->__get("discount_month"),
				'gen_scheme_type' => $scheme->__get("gen_scheme_type"),
				'advance_payment' => $scheme->__get("advance_payment"),
				'advance_dis_month' => $scheme->__get("advance_dis_month"),
				'advance_dis_amt' => $scheme->__get("advance_dis_amt"),
				'flat_dis' => $scheme->__get("flat_dis"),
				'per_dis' => $scheme->__get("per_dis"),
				'state_id' => $scheme->__get("state_id"),
				'site_id' => $scheme->__get("site_id"),
				'package_id' => $scheme->__get("package_id"),
				'consumer_id' => $scheme->__get("consumer_id"),
				'start_date' => $scheme->__get("start_date"),
				'end_date' => $scheme->__get("end_date"),
				'enable_date' => $scheme->__get("enable_date"),
				'scheme_applied' => $scheme->__get("scheme_applied"),
				'scheme_status' => $scheme->__get("scheme_status"),
				'is_postpaid' => $scheme->__get("is_postpaid"),
				'consumer_type' => $scheme->__get("consumer_type"),
				'package_continue' => $scheme->__get("package_continue"),
				'scheme_continue' => $scheme->__get("scheme_continue"),
		);
		
		$where = "scheme_id = " . $scheme->__get("scheme_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		} 
		/* $scheme= $schemes->__get("scheme");
		$scheme_id = $schemes->__get("scheme_id");
		
		$query ="update schemes set scheme='".$scheme."' where scheme_id=".$scheme_id;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		//echo $result;
		if($result)
		{
			return true;
		} */
	}
	
	public function deleteSchemeById($scheme_id)
	{
		$data = array(
			'scheme_status' => 'disable',
		);
		
		$where = "scheme_id = " . $scheme_id;
		$result = $this->_db_table->update($data,$where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function  getLastPackageHistoryByConumerId($scheme_type=null,$gen_schem_type=null)
	{
		$query="select * from schemes where scheme_type='".$scheme_type."' and gen_scheme_type=".$gen_schem_type." and scheme_status='disable'";
 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		return $result;
	}
	
	public function updateSchemeStatusById($scheme_id=null,$status=null)
	{
		$data=array(
				"scheme_status"=>$status,
		);
		 
		$where = "scheme_id = ".$scheme_id;
		 
		$result = $db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function checkDate($scheme_id=null,$curdate=null)
	{
		$query="select * from schemes where scheme_id='".$scheme_id."' and date(start_date)>='".$curdate."' and date(end_date)<='".$curdate."'";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		return $result;
	}
	
	public function getSchemeDetailsByConsumerId($consumer_id=null)
	{
		$query="select * from schemes s inner join consumer_scheme cs on s.scheme_id=cs.consumer_scheme where cs.consumer_id=".$consumer_id;
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		return $result;
	}
	
	public function getSchemeBySiteId($site_id=null)
	{
		
		$query="SELECT * FROM schemes where  FIND_IN_SET(".$site_id.", site_id)";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		return $result;
	}
	
}