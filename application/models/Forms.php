<?php

class Application_Model_Forms
{
    private $id;
	private $form_name;
    private $form_attachment;
    private $time_stamp;
    private $user_id;
    private $user_type;
	private $status;
	
    public function __construct($Forms_row = null)
    {
        if( !is_null($Forms_row) && $Forms_row instanceof Zend_Db_Table_Row ) {

                $this->id = $Forms_row->id;
                $this->form_name = $Forms_row->form_name;
                $this->form_attachment = $Forms_row->form_attachment;
                $this->time_stamp = $Forms_row->time_stamp;
                $this->user_id = $Forms_row->user_id;
                $this->user_type = $Forms_row->user_type;
				$this->status = $Forms_row->status;
        }
    }
	
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

