<?php

class Application_Model_LoginHistory
{
    private $id;
    private $user_id;
    private $status;
    private $ip_address;
    private $timestamp;
    
    public function __construct($log_row = null)
    {
        if( !is_null($log_row) && $log_row instanceof Zend_Db_Table_Row ) {
                $this->id = $log_row->id;
                $this->user_id = $log_row->user_id;
                $this->status = $log_row->status;
                $this->ip_address = $log_row->ip_address;
                $this->timestamp = $log_row->timestamp;
        }
    }
    
    public function __set($name, $value)
    {
        switch($name)
        {
            case "id":
                //throw new Exception('Cannot update Show\'s ID'');
                break;
            default:
                $this->$name = $value;
        break;
        }
    }
    
    public function __get($name)
    {
            return $this->$name;
    }
}

