<?php

class Application_Model_CashRegister
{

    private $cr_id;
    private $consumer_id;
    private $user_id;
    private $receipt_number; 
    private $transaction_id;
    private $cr_entry_type;
    private $cr_amount;
    private $timestamp;
    private $cr_status;
	private $mtr_entry;
    private $delete_type;
	private $receipt_count;
	private $user_val;
	private $transaction_type;
    private $cron_entry;
    private $amt_prompt;
    private $remark;
	private $entry_status;
	private $package_id;
	private $approved_date;
	private $approved_by;
	private $remark_create;
	 
    public function __construct($cr_row = null)
    {
     
      
        if( !is_null($cr_row) && $cr_row instanceof Zend_Db_Table_Row ) {
            
                $this->cr_id = $cr_row->cr_id;
                $this->consumer_id = $cr_row->consumer_id;
                $this->user_id = $cr_row->user_id;
                $this->receipt_number = $cr_row->receipt_number;
                $this->transaction_id = $cr_row->transaction_id;
                $this->cr_entry_type = $cr_row->cr_entry_type;
                $this->cr_amount = $cr_row->cr_amount;
                $this->timestamp = $cr_row->timestamp; 
                $this->cr_status=$cr_row->cr_status;
				$this->mtr_entry=$cr_row->mtr_entry;
                $this->delete_type=$cr_row->delete_type;
				$this->user_val = $cr_row->user_val;
				$this->transaction_type = $cr_row->transaction_type;
                $this->cron_entry = $cr_row->cron_entry;
                $this->amt_prompt = $cr_row->amt_prompt;
                $this->remark = $cr_row->remark;
				$this->entry_status = $cr_row->entry_status;
				$this->package_id = $cr_row->package_id;
				$this->approved_date = $cr_row->approved_date;
				$this->approved_by = $cr_row->approved_by;
				$this->remark_create = $cr_row->remark_create; 
        }
    }
    
    public function __set($name, $value)
    {
           
        $this->$name = $value;
               

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

