<?php
class Application_Model_SitesMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Sites();
			$this->_db_table_OptCharges = new Application_Model_DbTable_OtpCharges();
			$this->_db_table_siteStatusDetail = new Application_Model_DbTable_SiteStatusDetail();
    }

    public function addNewSite(Application_Model_Sites $site ,$user_id=NULL)
    {
        $data = array(
		'site_name' => $site->__get("site_name"),
        'cluster_id' => $site->__get("cluster_id"),
        'num_of_poles' => $site->__get("num_of_poles"),    
        'site_code' => $site->__get("site_code"),  
        'state_id' => $site->__get("state_id"),
        'site_status' => "New",
        'x' => $site->__get("x") !== null?$site->__get("x"):new Zend_Db_Expr('NULL'),
        'y' => $site->__get("y") !== null?$site->__get("y"):new Zend_Db_Expr('NULL'),
		 'size_of_plant' => $site->__get("size_of_plant"),
        'Solar_panel_size' => $site->__get("Solar_panel_size"),
        'Solar_panel_no' => $site->__get("Solar_panel_no"),
        'Charges_controller_size' => $site->__get("Charges_controller_size"),
        'Charges_controller_no' => $site->__get("Charges_controller_no"),
        'Inverter_size' => $site->__get("Inverter_size"),
        'Inverter_no' => $site->__get("Inverter_no"),
        'Battery_size' => $site->__get("Battery_size"),
        'Battery_no' => $site->__get("Battery_no"),
		 
		);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            $site = new Application_Model_Sites();    
            return $site;
        }
        else
        {
            $date = new Zend_Date();
            $date->setTimezone("Asia/Calcutta");
            $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
            $data = array(
                'site_order' => $result,
                'site_timestamp' => $timestamp,
            );
            $where = "site_id = " . $result;
            $update= $this->_db_table->update($data,$where);

			
			$datas = array(
            		'site_id' => $result,
            		'timestamp' => $timestamp,
            		'status' => "Enable",
            		'change_by' => $user_id,
            );
            $results = $this->_db_table_siteStatusDetail->insert($datas);	
				
            return $result;
        }
    }
    public function getSiteById($site_id)
    {
        $result = $this->_db_table->find($site_id);
        if( count($result) == 0 ) {
                $site = new Application_Model_Sites();    
                $site->__set("site_name","---Deleted---");
                return $site;
        }
        $row = $result->current();
        $site = new Application_Model_Sites($row);
        return $site;
    }
    
    
    
    public function getSiteByClusterId($cluster_id,$check=NULL)
    {
    	$where = array();
    	if($check!=NULL){
    		$checks=" and site_id!=1 ";
    	}else{
    		$checks=" ";
    	}
        $where[] = " cluster_id=".$cluster_id." and site_status!='Disable'".$checks;
                
        $result = $this->_db_table->fetchAll($where,array('cluster_id DESC'));
      
        if( count($result) == 0 ) {
                return false;
        }
        $site_object_arr = array();
        foreach ($result as $row)
        {
                $site_object = new Application_Model_Sites($row);
                array_push($site_object_arr,$site_object);
        }
        return $site_object_arr;
    }
    
    
    public function getAllSites($user=NULL,$state_id=NULL,$role_sites_id=NULL)
    {
    	 
    	$where=NULL;
    	if($role_sites_id !=NULL)
    	{
    		$where= " site_id in (".implode(",", $role_sites_id).") ";
    	}else{
    		$where= "site_status!='Disable'";
    	}
        $result = $this->_db_table->fetchAll($where,array('site_name asc'));
        if( count($result) == 0 ) {
                return false;
        }
        $site_object_arr = array();
        foreach ($result as $row)
        {
                $site_object = new Application_Model_Sites($row);
                array_push($site_object_arr,$site_object);
        }
        return $site_object_arr;
    }
    public function updateSite(Application_Model_Sites $site)
    {
            $date = new Zend_Date();
            $date->setTimezone("Asia/Calcutta");
            $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");

         $data = array(
		'site_name' => $site->__get("site_name"),
        'cluster_id' => $site->__get("cluster_id"),
        'num_of_poles' => $site->__get("num_of_poles"),    
        'site_code' => $site->__get("site_code"),
        'state_id' => $site->__get("state_id"),
        'site_status' => $site->__get("site_status"),
        'site_order' => $site->__get("site_order"),
        'site_timestamp' => $timestamp,
        'x' => $site->__get("x") !== null?$site->__get("x"):new Zend_Db_Expr('NULL'),
        'y' => $site->__get("y") !== null?$site->__get("y"):new Zend_Db_Expr('NULL'),
		'size_of_plant' => $site->__get("size_of_plant"),
        'Solar_panel_size' => $site->__get("Solar_panel_size"),
        'Solar_panel_no' => $site->__get("Solar_panel_no"),
			'Solar_panel_kw' => $site->__get("Solar_panel_kw"),
        'Charges_controller_size' => $site->__get("Charges_controller_size"),
        'Charges_controller_no' => $site->__get("Charges_controller_no"),
        'Inverter_size' => $site->__get("Inverter_size"),
        'Inverter_no' => $site->__get("Inverter_no"),
        'Battery_size' => $site->__get("Battery_size"),
        'Battery_no' => $site->__get("Battery_no"),
			'Battery_no_v' => $site->__get("Battery_no_v"),
		'otp_act_id' => $site->__get("otp_act_id"),
        'otp_mtr_id' => $site->__get("otp_mtr_id"),
			
			'dg_capacity' => $site->__get("dg_capacity"),
			'dg_no' => $site->__get("dg_no"),
			'bio_capacity' => $site->__get("bio_capacity"),
			'bio_no' => $site->__get("bio_no"),
			'radiation' => $site->__get("radiation"),
			'site_target' => $site->__get("site_target"),
			'plant_duration' => $site->__get("plant_duration"),
			'plant_target' => $site->__get("plant_target"),
			'array_no' => $site->__get("array_no"),
			'array_capacity' => $site->__get("array_capacity"),
			'day_factor' => $site->__get("day_factor"),
			'night_factor' => $site->__get("night_factor"),
		);
        $where = "site_id = " . $site->__get("site_id");
        $result = $this->_db_table->update($data,$where);
        
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
   public function deleteSiteById($site_id,$user_id=null)
    {
        /* $where = "site_id = " . $site_id;
        $result = $this->_db_table->delete($where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        } */
    	$data = array(
    			'site_status' => "Disable",
    	);
    	$where = "site_id = " . $site_id;
    	$result = $this->_db_table->update($data,$where);
    	
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		$date = new Zend_Date();
    		$date->setTimezone("Asia/Calcutta");
    		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
    		$datas = array(
    				'site_id' => $site->__get("site_id"),
    				'timestamp' => $timestamp,
    				'status' => "Disable",
    				'change_by' => $user_id,
    		);
    		$results = $this->_db_table_siteStatusDetail->insert($datas);
    		
    		return true;
    	}
    }
    
    public function getSiteOutstanding($siteConsumers){
        
        $consumersMapper = new Application_Model_ConsumersMapper();
        $total = $siteOutstanding = $outstandingAmount = $remainingActOutstanding = 0;
        foreach($siteConsumers as $consumer){
            $outstandingAmount = $consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
            $remainingActOutstanding = $consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
            $siteOutstanding = $outstandingAmount + $remainingActOutstanding;
            $total = $total + $siteOutstanding;
        }
        return $total;
    }
    public function getSiteOutstandingAmount($siteConsumers){
        
        $consumersMapper = new Application_Model_ConsumersMapper();
        $total = $siteOutstanding = $outstandingAmount = $remainingActOutstanding = 0;
        foreach($siteConsumers as $consumer){
            $outstandingAmount = $outstandingAmount +  $consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
            $total++;
        }
        //echo $outstandingAmount;
        return $outstandingAmount;
    }
    public function getSiteActAmount($siteConsumers){
        
        $consumersMapper = new Application_Model_ConsumersMapper();
        $total = $siteOutstanding = $outstandingAmount = $remainingActOutstanding = 0;
        foreach($siteConsumers as $consumer){
            $remainingActOutstanding = $remainingActOutstanding +  $consumersMapper->getRemainingActivation($consumer->__get("consumer_id"));
        }
        return $remainingActOutstanding;
    }
    public function getSiteBySiteCode($site_code)
    {
        /* $where = array(
            "site_code = ?"=>$site_code
        ); */
        $where = array();
        $where[] = " site_code='".$site_code."' and site_status!='Disable'";
        
        $result = $this->_db_table->fetchRow($where);
        if( count($result) == 0 ) {
                $site = new Application_Model_Sites();    
                $site->__set("site_name","---Deleted---");
                return $site;
        }
        $row = $result;
        $site = new Application_Model_Sites($row);
        return $site;
    }
     
    public function getSiteByStateId($state_id=NULL,$role_sites_id=NULL,$site_id=NULL)
    {
    	$where = array();
    	if($site_id!=NULL){
    		$where[] = " site_id in (".$site_id.") and site_status!='Disable' and site_id!=1";
    	}elseif($state_id!=NULL){
    		$where[] = " state_id in (".$state_id.") and site_status!='Disable' and site_id!=1";
    	}else{
    		$where[] = " site_id in (".implode(",", $role_sites_id).") and site_status!='Disable' and site_id!=1";
    	}
    	
    	$result = $this->_db_table->fetchAll($where,array('site_name'));
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$site_object_arr = array();
    	foreach ($result as $row)
    	{
    		$site_object = new Application_Model_Sites($row);
    		array_push($site_object_arr,$site_object);
    	}
    	return $site_object_arr;
    }
    public function getSitesByStateId($state_id=NULL)
    {
    	$where = array();
    
    	$where[] = " state_id=".$state_id." and site_status!='Disable'";
    	 
    	$result = $this->_db_table->fetchAll($where,array('site_id DESC'));
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$site_object_arr = array();
    	foreach ($result as $row)
    	{
    		$site_object = new Application_Model_Sites($row);
    		array_push($site_object_arr,$site_object);
    	}
    	return $site_object_arr;
    }
   
   public function getSiteByName($site_name)
    {
        $stmt = $this->_db_table->getAdapter();
  		$query = "SELECT * from sites WHERE site_name='$site_name' limit 1";
  	    $result = $stmt->fetchRow($query);
        //$site = new Application_Model_Sites($row);
        return $result;
    }
    
    public function getAllSitesForSitePage()
    {
    	$where=NULL;
    	$result = $this->_db_table->fetchAll($where,array('site_order ASC'));
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$site_object_arr = array();
    	foreach ($result as $row)
    	{
	   		$site_object = new Application_Model_Sites($row);
	   		array_push($site_object_arr,$site_object);
    	}
    	return $site_object_arr; 
   	}
   	
   	public function getAllSitesForMeterReading($site_id)
   	{
   	$query="SELECT  * FROM  site_meter  where site_id=".$site_id;
			
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	else{
    		return $result;
    	}
   	}
   	public function getAllSitesForMpptReading($site_id)
   	{
   		$query="SELECT  * FROM  mppts  where site_id=".$site_id." order by mppt_name";
   			
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
	
		public function getstatesbysiteid($site_id)
   	{
   		$query="SELECT  * FROM sites inner join states on sites.state_id= states.state_id where site_id=".$site_id." limit 1";
   			
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result[0];
   		}
   	}
	
		public function getsizeOfPlant($state_id=NULL,$site_id=NULL,$role_sites_id=NULL)
   	{
   		if($state_id!=null){
   			$sites=" where state_id =".$state_id." and site_status!='Disable'";
   		}elseif($site_id!=null){
   			$sites=" where site_id in (".$site_id.") and site_status!='Disable'";
   		}else{
   			$sites= " where site_id in (".implode(",", $role_sites_id).") and site_status!='Disable'";
   		}
   		  $query="SELECT  sum(size_of_plant) as sizeplant FROM  sites ".$sites;
   	 
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result[0]["sizeplant"];
   		}
   	}
	
	public function getSitesByStateAndSite($site_id=NULL,$state_id=NULL,$role_site_id=NULL)
   	{
   		if($site_id!=null){
   			$sites=" where s.site_id in (".implode(",", $site_id).") and site_status!='Disable'";
   		}elseif($state_id!=null){
   			$sites=" where s.state_id in (".implode(",", $state_id).") and s.site_id in (".implode(",", $role_site_id).") and site_status!='Disable'";    
   		}else{
   			$sites= " where s.site_id in (".implode(",", $role_site_id).") and site_status!='Disable'";
   		}
   		 
		$query="SELECT  * FROM sites s inner join states st on s.state_id=st.state_id ".$sites." order by s.site_name";
   		 
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
	
	public function getChargesbyType($type=NULL)
   	{
   		 
   		$query="SELECT * FROM otp_charges";
   		 
   		$stmt =  $this->_db_table_OptCharges->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
	
		public function getAllNewSites($site_status=NULL,$role_site_id=NULL)
   	{
   		 
   		//$query="SELECT * FROM sites where site_status='".$site_status."'";
   		 
   		$where="site_status='".$site_status."'"; 
    	$result = $this->_db_table->fetchAll($where,array('site_order ASC'));
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$site_object_arr = array();
    	foreach ($result as $row)
    	{
	   		$site_object = new Application_Model_Sites($row);
	   		array_push($site_object_arr,$site_object);
    	}
    	return $site_object_arr;
   	}
   	
   	public function getChargesbyID($id=NULL)
   	{
   	
   		$query="SELECT * FROM otp_charges where id=".$id;
   	
   		$stmt =  $this->_db_table_OptCharges->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
   	
   	public function updateSitesById($site_id=NULL,$status=NULL,$remark=NULL,$approve_by=NULL){
   		 
   		$where="";
   		if($status!="Enable"){
   			$where="site_id=".$site_id;
   		}else{
   			$where="site_id in (".$site_id.")";
   		}
   		$date = new Zend_Date();
   		$date->setTimezone("Asia/Calcutta");
   		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
   		
   		$query ="update sites set site_status='".$status."',remark='".$remark."',approve_date='".$timestamp."',approve_by=".$approve_by."   where ".$where;
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result=$stmt->execute();
   		 
   		if($result)
   		{
   			return true;
   		}
   	}
	
	public function addSiteStatusDetail($site_id=null,$status=null,$user_id){
   		$date = new Zend_Date();
   		$date->setTimezone("Asia/Calcutta");
   		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
   		 
   		$datas = array(
   				'site_id' => $site_id,
   				'timestamp' => $timestamp,
   				'status' => $status,
   				'change_by' => $user_id,
   		);
   		$results = $this->_db_table_siteStatusDetail->insert($datas);
   		if($result)
   		{
   			return true;
   		}
   	}
	
	public function getSitesForStateId($state_id=NULL,$role_site_id=NULL)
   	{
   		$role_site="";
   		if($role_site_id!=NULL){
   			$role_site=" and site_id in (".implode(",", $role_site_id).")"; 
   		}
   		$query="SELECT  * FROM  sites where state_id=".$state_id.$role_site." and site_id!=1 and site_status!='Disable' order by site_name";
   	
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
	
	public function getSitesByCollectionSite($site_id=NULL,$onlySite=NULL)
   	{
	 
   		if($onlySite!=NULL){
   			$site=" and site_id in (".$onlySite.")";
   		}else{
   			$site="";
   		}
   		if($site_id!=null){
   			$where=" where site_id in (".implode(",", $site_id).") and site_status!='Disable'".$site;
   		}else{
   			$where=" where site_status!='Disable' ".$site;    
   		}
   		
		$query="SELECT * FROM sites ".$where." order by site_name";
   		
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
	public function getPlantDetailsBySites($site_id=NULL,$state_id=NULL,$role_site_id=NULL)
   	{
   		if($site_id!=null){
   			$sites=" where s.site_id in (".$site_id.") and site_status!='Disable' and s.site_id!=1";
   		}elseif($state_id!=null){
   			$sites=" where s.state_id in (".$state_id.") and site_status!='Disable' and s.site_id!=1";
   		}else{
   			$sites= " where s.site_id in (".implode(",", $role_site_id).") and site_status!='Disable' and s.site_id!=1";
   		}
   		
		$query="SELECT  sum(Solar_panel_kw) as Solar_panel_kw,sum(Battery_no_v) as Battery_no_v,sum(dg_capacity) as dg_capacity,sum(bio_capacity) as bio_capacity,
						avg(radiation) as radiation,avg(site_target) as site_target,avg(plant_duration) as plant_duration,avg(plant_target) as plant_target,
						sum(Inverter_no) as Inverter_no,sum(Charges_controller_no) as Charges_controller_no,sum(Battery_no) as Battery_no,sum(array_no) as array_no,
						sum(Solar_panel_no) as Solar_panel_no,avg(array_capacity) as array_capacity,avg(day_factor) as day_factor,avg(night_factor) as night_factor
						FROM sites s inner join states st on s.state_id=st.state_id ".$sites." order by s.site_name"; 
   		 
   		$stmt = $this->_db_table->getAdapter()->query($query); 
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result[0];
   		} 
   	}
	
	public function getSparkSites()
   	{
	    $query="SELECT * FROM sites where spark=1 order by site_name asc";
   		
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result; 
   		}
   	}
}
