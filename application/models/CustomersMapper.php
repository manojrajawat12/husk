<?php
class Application_Model_CustomersMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Customers();
    }

    public function addNewCustomer(Application_Model_Customers $customer)
    {
         $data = array(
            'package_id' => $customer->__get("package_id"),
            'site_id' => $customer->__get("site_id"),
            'customer_code' => $customer->__get("customer_code"),
            'customer_name' => $customer->__get("customer_name"),
            'customer_father_name' => $customer->__get("customer_father_name"),
            'customer_status' => $customer->__get("customer_status"),
            'customer_act_date' => $customer->__get("customer_act_date"),
            'customer_act_charge' => $customer->__get("customer_act_charge"),
            'customer_connection_id' => $customer->__get("customer_connection_id"),
             'is_micro_enterprise' => $customer->__get("is_micro_enterprise"),
            'micro_entrprise_price' => $customer->__get("micro_entrprise_price"),
            'suspension_date' => $customer->__get("suspension_date"),
			'type_of_me' => $consumer->__get("type_of_me"),
         	'notes' => $consumer->__get("notes"),
    );
        $result = $this->_db_table->insert($data);
        print_r($result); die;
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getConsumerById($consumer_id)
    {
        $result = $this->_db_table->find($consumer_id);
        if( count($result) == 0 ) {
            $consumer = new Application_Model_Customers(); 
            return $consumer;
        }
        $row = $result->current();
        $consumer = new Application_Model_Customers($row);
        return $consumer;
    }
    public function getAllCustomers()
    {
        $result = $this->_db_table->fetchAll(null,array('consumer_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Customers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    public function updateConsumer(Application_Model_Customers $consumer)
    {
        $data = array(
            'package_id' => $consumer->__get("package_id"),
            'site_id' => $consumer->__get("site_id"),
            'consumer_code' => $consumer->__get("consumer_code"),
            'consumer_name' => $consumer->__get("consumer_name"),
            'consumer_father_name' => $consumer->__get("consumer_father_name"),
            'consumer_status' => $consumer->__get("consumer_status"),
            'consumer_act_date' => $consumer->__get("consumer_act_date"),
            'consumer_act_charge' => $consumer->__get("consumer_act_charge"),
            'is_micro_enterprise' => $consumer->__get("is_micro_enterprise"),
            'micro_entrprise_price' => $consumer->__get("micro_entrprise_price"),
            'consumer_connection_id' => $consumer->__get("consumer_connection_id"),
            'suspension_date' => $consumer->__get("suspension_date"),
			'enterprise_type' => $consumer->__get("enterprise_type"),
    
        	'type_of_me' => $consumer->__get("type_of_me"),
        	'notes' => $consumer->__get("notes"),
	);
        $where = "consumer_id = " . $consumer->__get("consumer_id");
        $result = $this->_db_table->update($data,$where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteConsumerById($consumer_id)
    {
        $where = "consumer_id = " . $consumer_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
    public function searchByColumnValue($column=NULL,$column_value=NULL,$consumer_code=FALSE){
        if($column!=NULL){
            if($consumer_code)
            {
                $where = array($column."=?" => $column_value);
            }
            else
            {
                $where = array($column." LIKE ?" => "%".$column_value."%");
            }
        }
        else
        {
            $where = NULL;
        }
         
        $result = $this->_db_table->fetchAll($where,array('consumer_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Customers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    public function getConsumerOutstanding($consumer_id)
    {
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        $enteries = $cashRegisterMapper->getEntryByConsumerId($consumer_id);
        $outstandingAmount = 0;
        if($enteries){
            $creditAmount = $debitAmount = 0;
            $typesArray = array("CREDIT");

            $creditEnteries = $cashRegisterMapper->applyTypeFilter($enteries, $typesArray);

            if($creditEnteries){
                foreach($creditEnteries as $entry){
                    $creditAmount += $entry->__get("cr_amount");
                }

            }

            $typesArray = array("DEBIT","PENALTY","REFUND");
            $debitEnteries = $cashRegisterMapper->applyTypeFilter($enteries, $typesArray);

            if($debitEnteries){
                foreach($debitEnteries as $entry){
                    $debitAmount += $entry->__get("cr_amount");
                }
            }

            $outstandingAmount = $debitAmount - $creditAmount;
       }
       return $outstandingAmount;
    }
    public function getRemainingActivation($consumer_id)
    {
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        $enteries = $cashRegisterMapper->getEntryByConsumerId($consumer_id);
        $consumer = $this->getConsumerById($consumer_id);
        $actAmount = $consumer->__get("consumer_act_charge");
        $activationAmount = 0;
        if($enteries){
            $creditAmount = $debitAmount = 0;
            $typesArray = array("ACTIVATION");

            $activationEnteries = $cashRegisterMapper->applyTypeFilter($enteries, $typesArray);

            if($activationEnteries){
                foreach($activationEnteries as $entry){
                    $activationAmount += $entry->__get("cr_amount");
                }

            }
            $actAmount = $actAmount - $activationAmount;
       }
       return $actAmount;
    }
    
    public function searchConsumerBySite($site_name=NULL,$site_id=NULL)
    {
        if($site_name!=NULL){
            $query = "SELECT sites.*,consumers.*
                     FROM sites
                     INNER JOIN consumers
                     ON sites.site_id=consumers.site_id
                     WHERE sites.site_name LIKE '%".$site_name."%'";
        }
        elseif($site_id!=NULL)
        {
            $query = "SELECT sites.*,consumers.*
                     FROM sites
                     INNER JOIN consumers
                     ON sites.site_id=consumers.site_id
                     WHERE sites.site_id=".$site_id;
        }
        else{
            return FALSE;
        }
        
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if( count($result) == 0 ) {
                return false;
        }
        
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Customers();
                $consumer_object->__set("consumer_id",$row['consumer_id']);
                $consumer_object->__set("package_id",$row['package_id']);
                $consumer_object->__set("site_id",$row['site_id']);
                $consumer_object->__set("consumer_code",$row['consumer_code']);
                $consumer_object->__set("consumer_name",$row['consumer_name']);
                $consumer_object->__set("consumer_father_name",$row['consumer_father_name']);
                $consumer_object->__set("consumer_status",$row['consumer_status']);
                $consumer_object->__set("consumer_act_date",$row['consumer_act_date']);
                $consumer_object->__set("consumer_act_charge",$row['consumer_act_charge']);
                $consumer_object->__set("consumer_connection_id",$row['consumer_connection_id']);
                array_push($consumer_object_arr,$consumer_object);
        }
    
        return $consumer_object_arr;
    }
    
    public function cashPaidByConsumer($consumer_id,$from=NULL,$to=NULL){
        
        $query = "SELECT * FROM cash_register
                 WHERE consumer_id=".$consumer_id." AND (cr_entry_type = 'CREDIT' OR cr_entry_type = 'ACTIVATION' OR cr_entry_type = 'REFUND')";
               // echo $query; exit;
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        
        $amount = 0;
        $filteredValues = array();
        
        if( count($result) != 0 ) {
            /*Date Filter */
            if($from != NULL && $to != NULL){
                foreach ($result as $row){
                    $date = new Zend_Date($row['timestamp'],"yyyy-MM-dd HH:mm:ss");
                    if($date->isEarlier($to) && $date->isLater($from)){         
                        $filteredValues[] = $row; 
                    }
                }
            }
            else{
                /*without Date filter */
                foreach ($result as $row){
                    $filteredValues[] = $row;
                }
            }
            
            foreach ($filteredValues as $row){
                if($row['cr_entry_type']=="CREDIT" or $row['cr_entry_type']=="ACTIVATION"){
                    $amount = $amount + $row['cr_amount'];
                }
                elseif($row['cr_entry_type']=="REFUND"){
                    $amount = $amount - $row['cr_amount'];
                }
            }
        }
        return $amount;
    }
    
    public function getCustomersByColumn($site_id,$package_id)
    {
        $where = array(
            "site_id = ?"=>$site_id,
            "package_id = ?"=>$package_id
        );
        
        $result = $this->_db_table->fetchAll($where,array('consumer_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Customers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
    
    public function getMonthlyActiveCustomers($site_id,$consumer_status){
        
        $where = array(
            "site_id =?"=>$site_id,
            "consumer_status =?"=>$consumer_status,
        );
        
        $result = $this->_db_table->fetchAll($where,array('consumer_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Customers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
        
    }
    public function getTotalCustomersBySite($site_id,$month=NULL)
    {
        $consumers = array();
        if($month==NULL)
        {   
            $zendDate = new Zend_Date ();
            $y = $zendDate->toString(Zend_Date::YEAR);
            $currentMonth = $zendDate->toString(Zend_Date::MONTH_SHORT);
            for($currentMonth;$currentMonth>=1;$currentMonth--)
            {
                $month31 = array(1,3,5,7,8,10,12);
                $month30 = array(4,6,9,11);
                $month28 = array(2);
                if(in_array($currentMonth, $month31))
                {
                    $date = "31";
                }
                elseif(in_array($currentMonth, $month30))
                {
                    $date = "30";
                }
                elseif(in_array($currentMonth, $month28))
                {
                    $date = "28";
                }
                else
                {
                    $date = "0";
                }
                
                
                $query = "SELECT * FROM consumers WHERE site_id='".$site_id."' AND DATE(consumer_act_date) < '".$y."-".$currentMonth."-".$date."'";
                $stmt = $this->_db_table->getAdapter()->query($query);
                $result = $stmt->fetchAll();
                $count = count($result);
                $consumers[] = $count;
            }
            return array_reverse($consumers);
        }
    }
    
    public function getCustomersByConnectionId($connection_id)
    {
        $where = array(
            "consumer_connection_id =? "=> $connection_id
        );
        
        $result = $this->_db_table->fetchAll($where);
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_object = new Application_Model_Customers($row);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
    }
}
