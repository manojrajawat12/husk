<?php

class Application_Model_MeterReadings
{
    private $meter_reading_id;
	private $consumer_id;
	private $meter_reading;
	private $timestamp;
	private $package_id;
	private $start_reading;

	private $meter_no;
	private $no_of_containers;
	private $no_of_hours;
	private $operating_mode;
	private $is_running_plan;
	private $package_name;
	private $serial;
	private $unit;
	private $entry_by;
	private $entry_type;  
	
	public function __construct($change_row = null)
	{
		if( !is_null($change_row) && $change_row instanceof Zend_Db_Table_Row ) {

			$this->meter_reading_id = $change_row->meter_reading_id;
			$this->consumer_id = $change_row->consumer_id;
			$this->meter_reading = $change_row->meter_reading;
			$this->timestamp = $change_row->timestamp;
			$this->package_id = $change_row->package_id;
			$this->start_reading = $change_row->start_reading;
			 
			$this->meter_no = $change_row->meter_no;
			$this->no_of_container = $change_row->no_of_containers;
			$this->no_of_hours= $change_row->no_of_hours;
			$this->operating_mode = $change_row->operating_mode;
			$this->is_running_plan = $change_row->is_running_plan;
			$this->package_name = $change_row->package_name;
			$this->serial = $change_row->serial;
			$this->unit= $change_row->unit;
			$this->entry_by = $change_row->entry_by;
			$this->entry_type= $change_row->entry_type;
		}
	}
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}
}
  
 
