<?php

class Application_Model_Permission
{
    private $permission_id;
    private $menu_id;
    private $role_id;
    private $permission;
//     private $parent_menu;

    public function __construct($permission_row = null)
    {
        if( !is_null($permission_row) && $permission_row instanceof Zend_Db_Table_Row ) {
            
                $this->permission_id = $permission_row->permission_id;
                $this->menu_id = $permission_row->menu_id;
                $this->role_id = $permission_row->role_id;
                $this->permission = $permission_row->permission;
//                 $this->parent_menu = $permission_row->parent_menu;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}
