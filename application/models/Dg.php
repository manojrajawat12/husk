<?php

class Application_Model_Dg
{
    private $id;
    private $site_id;
    private $date_of_purchase;
    private $rate;
    private $amount;
    private $unit;
    private $bill_attach;
	private $user_id;
    private $timestamp;
	private $claimed_by;
    private $verified_by;
    private $verified_date;
    private $status;
	private $invoice_no;

    public function __construct($DG_row = null)
    {
        if( !is_null($DG_row) && $DG_row instanceof Zend_Db_Table_Row ) {
            
                $this->dg_id = $DG_row->dg_id;
                $this->site_id = $DG_row->site_id;
                $this->date_of_purchase = $DG_row->date_of_purchase;
                $this->amount = $DG_row->amount;
                $this->rate = $DG_row->rate;
                $this->unit = $DG_row->unit;
                $this->bill_attach = $DG_row->bill_attach;
				$this->user_id = $DG_row->user_id;
                $this->timestamp = $DG_row->timestamp; 
				$this->claimed_by = $DG_row->claimed_by;
                $this->verified_by = $DG_row->verified_by;
                $this->verified_date = $DG_row->verified_date;
                $this->status = $DG_row->status; 
				$this->invoice_no = $DG_row->invoice_no; 
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

