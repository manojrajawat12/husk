<?php

class Application_Model_Device
{
    private $device_id;
    private $device_name;
    private $site_id;
    private $iccid;
	private $device_sim;
	private $time;
	private $sms_status;
	private $sms_timestamp;
	
    public function __construct($state_row = null)
    {
        if( !is_null($state_row) && $state_row instanceof Zend_Db_Table_Row ) {
            
                $this->device_id = $state_row->device_id;
                $this->device_name = $state_row->device_name;
                $this->site_id = $state_row->site_id;
				$this->iccid = $state_row->iccid;
				$this->device_sim = $state_row->device_sim;
				$this->time = $state_row->time;
				$this->sms_status = $state_row->sms_status;
				$this->sms_timestamp = $state_row->sms_timestamp;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

