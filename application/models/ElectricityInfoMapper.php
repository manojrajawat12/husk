<?php
class Application_Model_ElectricityInfoMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_ElectricityInfo();
    }

     public function addNewElectricityInfo(Application_Model_ElectricityInfo $electric)
    { 
       $data = array(
		'node' => $electric->__get("node"),
        'version' => $electric->__get("version"),
		'device_name' => $electric->__get("device_name"),
        'iccid' => $electric->__get("iccid"),
        'site_id' => $electric->__get("site_id"),
		'votage' => $electric->__get("votage"),
       	'consumer_id' => $electric->__get("consumer_id"),
        'info_date' => $electric->__get("info_date"),
       	'volt' => $electric->__get("volt"),
       );
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result; 
        }
    }
 
    public function updateElectricityInfo(Application_Model_ElectricityInfo $electric)
    {
    	$data = array(
			'node' => $electric->__get("node"),
	        'version' => $electric->__get("version"),
	        'iccid' => $electric->__get("iccid"),
	        'site_id' => $electric->__get("site_id"),
			'votage' => $electric->__get("votage"),
	       	'consumer_id' => $electric->__get("consumer_id"),
	        'info_date' => $electric->__get("info_date"),
	       	'volt' => $electric->__get("volt"),
         );
    	$where = "device_name = '" . $electric->__get("device_name")."'";
    	$result = $this->_db_table->update($data,$where);
    		
    	return true;
    		
    }

     public function  getElectricitystatusByDevice($device_name=NULL)
    {
   	 	$query="SELECT * from ierms_data where device_name ='".$device_name."' order by info_date desc";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
	 
		if( count($result) == 0 ) {
			return false;
		}else{
			return $result;
		}
    	 
    }
  
      public function  getAllElectricitystatus($role_site_id=NULL)
    {
    	$query="SELECT * from ierms_data where site_id in (".implode(",",$role_site_id).")";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result;
    	}
    
    }
   
   public function  getElectricityForAccount($consumer_id,$second,$secondEnd)
    {
       $mongoDriver= new MongoClient();
       $mongoDB=$mongoDriver->tara_devices;
       $collmongo=$mongoDB->ierms; 
 
         //echo $second." to ".$secondEnd." next "; 
       $info=array("info_date"=>array('$lte'=>$secondEnd, '$gte'=>$second),"current.consumer_id"=> $consumer_id);
       $result=  $collmongo->find($info)->sort(array("info_date"=>-1));

        if( count($result) == 0 ) {
            return false;
        }
        return $result;
    }
/*public function  getElectricitystatusForAccount($consumer_id)
    {
        $query = "SELECT ie.* FROM ierms_data ie inner join node_map np on ie.device_id=np.device_name and ie.node= np.node where np.consumer_id='".$consumer_id."' 
		 order by info_date desc";

        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();

        if( count($result) == 0 ) {
            return false;
        }

        return $result[0];
    }*/
	
	 public function  getElectricitystatusForAccount($device_name)
    {
		//$query = "SELECT ie.* FROM ierms_data ie inner join node_map np on ie.device_id=np.device_name and ie.node= np.node where np.consumer_id='".$consumer_id."' order by info_date desc";
        //$stmt = $this->_db_table->getAdapter()->query($query);
        //$result = $stmt->fetchAll();

        $mongoDriver= new MongoClient();
        $mongoDB=$mongoDriver->tara_devices;
        $collmongo=$mongoDB->ierms;
  
       $info=array("devices.name"=> "$device_name");  
        $result=  $collmongo->find($info)->sort(array("info_date"=>-1))->limit(1);
		/*$ops = array(
        		
        		 array(
        				'$match' => array(
        						'devices.name' =>  "$device_name",
        				)
        		  ),
        		array(
        				'$sort'=>array("info_date"=>-1)
        		),
        		array(
        				'$limit'=>1
        		)
        	);
        
        }
        
        $result=  $collmongo->aggregate($ops);
		print_r($result);exit;
		 $data = [];
        foreach ($result as $key => $value){
            $data[] = $value;
        }  
        print_r($data);exit;*/
		
        if( count($result) == 0 ) {
            return false;
        }
 
        return $result;
    }
	
	public function getElectricityByConsumerIdAndStarAndEndDateTime($site_id,$DateTimeFrom,$DateTimeTo)
    {
        $mongoDriver= new MongoClient();
        $mongoDB=$mongoDriver->tara_devices;
        $mongoCollection = $mongoDB->ierms;
		
        $result=  $mongoCollection->find([
            "info_date"=>[

                '$gte'=>$DateTimeFrom,
                '$lte'=>$DateTimeTo,
            ],
            "devices.site_id"=> $site_id
        ])->sort([
            "info_date"=>-1
        ]);

        if($result->count() == 0) {
            return false;
        }
        $data = [];
        foreach ($result as $key => $value){
            $data[] = $value;
        }
          
        return $data;
    }

	public function  getElectricitystatusByConid($consumer_id=NULL)
    { 
    	$node_map=new Application_Model_AddDeviceMapper();
    	$consumers=$node_map->getDeviceDetailByConid($consumer_id);
    	if($consumers){
	    	$query="SELECT * from ierms_data i inner join devices d on i.device_name=d.device_name where i.device_name ='".$consumers["device_name"]."'";
	    	$stmt = $this->_db_table->getAdapter()->query($query);
	    	$result = $stmt->fetchAll();
	    
	    	if( count($result) == 0 ) {
	    		return false;
	    	}else{
	    		return $result[0];
	    	}	
    	}else{
    		return  false;
    	}
    }
	
}
