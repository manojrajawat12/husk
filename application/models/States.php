<?php

class Application_Model_States
{
    private $state_id;
    private $state_name;
    

    public function __construct($state_row = null)
    {
        if( !is_null($state_row) && $state_row instanceof Zend_Db_Table_Row ) {
            
                $this->state_id = $state_row->state_id;
                $this->state_name = $state_row->state_name;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

