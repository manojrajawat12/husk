<?php

class Application_Model_ReportPermission
{ 
    private $id;
    private $report_name;
    private $User_id;
   
    

    public function __construct($CP_row = null)
    {
        if( !is_null($CP_row) && $CP_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $CP_row->id;
                $this->report_name = $CP_row->report_name;
                $this->user_id = $CP_row-> user_id;
        }
    }
 public function __set($name, $value)
    {
           
         $this->$name = $value;
            
    }
    public function __get($name)
    {
            return $this->$name;
    }
}

