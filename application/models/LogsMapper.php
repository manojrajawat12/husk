<?php

class Application_Model_LogsMapper
{
     protected $_db_table;
    public function __construct()
    {
            //Instantiate the Table Data Gateway for the Shows table
            $this->_db_table = new Application_Model_DbTable_Logs();
    }
    public function addNewLogs(Application_Model_Logs $log)
    {
        $data = array(
            'user_id' => $log->__get("user_id"),
            'username' => $log->__get("username"),
            'priority_level' => $log->__get("priority_level"),
            'priority_name' => $log->__get("priority_name"),
            'timestamp' => $log->__get("timestamp"),
            'message' => $log->__get("message"),
            
        );
        $result = $this->_db_table->insert($data);
        if(!$result)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function getAllLogs()
    {
        $result = $this->_db_table->fetchAll(null,array('id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $log_object_arr = array();
        foreach ($result as $row)
        {
                $logs_object = new Application_Model_Logs($row);
                array_push($log_object_arr,$logs_object);
        }
        return $log_object_arr;
    }
    
    public function deleteLogById($id)
    {
        $where = "id = " . $id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }

 
	  public function getConsumerReactiveLogs($day=null,$month=null,$year=null,$status=NULL)
     {
     	if($status!=null){
     		$day_data=" day(timestamp)=".$day." and ";
     	}else{
     		$day_data="";
     	}
$where="";
     	  $query = "SELECT * FROM logs where ".$where." (message like '%changed to active from banned%')
					and ".$day_data." month(timestamp)=".$month." and year(timestamp)=".$year." order by timestamp desc";
     	$stmt = $this->_db_table->getAdapter()->query($query);
     	$result = $stmt->fetchAll();
     	if( count($result) == 0 ) {
     		return false;
     	}
     	return $result;
     }
	 
	 public function getConsumerReactiveLogsBydate($currDate=null,$lastDate=NULL)
     {
     	 $where="";
     	$query = "SELECT * FROM logs where ".$where." (message like '%changed to active from banned%')
					and date(timestamp)<='".$currDate."' and date(timestamp)>='".$lastDate."' order by timestamp desc";
     	$stmt = $this->_db_table->getAdapter()->query($query);
     	$result = $stmt->fetchAll();
     	if( count($result) == 0 ) {
     		return false;
     	}
     	return $result;
     }
	 
	  public function getConsumerBannedLogsConsumer($currdate=NULL,$connection_id=NULL)
     {
        $query = "SELECT * FROM logs where ".$where." (message like '%changed to banned from active%')
					and date(timestamp)<'".$currdate."' and (message like '%".$connection_id."%') order by timestamp desc"; 
     	$stmt = $this->_db_table->getAdapter()->query($query);  
     	$result = $stmt->fetchAll();
     	if( count($result) == 0 ) {
     		return false;
     	}
     	return $result[0];
     }
	 public function getAllLogsbyConnectionID($is_conn=null,$month=null,$year=null,$connection_id=null)
     {
     	 if($is_conn!=null){
	     	$where="message like '%".$connection_id."%' and ";
	     }else{
	     	$where=null;
	     }

     	 $query = "SELECT * FROM logs where ".$where."
					(message like '%changed to banned from active%' or message like '%changed to active from banned%') 
					and month(timestamp)=".$month." and year(timestamp)=".$year." order by timestamp desc";
     	 $stmt = $this->_db_table->getAdapter()->query($query);
     	 $result = $stmt->fetchAll();
     	 if( count($result) == 0 ) { 
     		return false;
     	 }
     	 return $result;
     }
	 
	 public function getConsumerBannedLogsBydate($currDate=null,$lastDate=NULL)
     {
     	 $where="";
     	$query = "SELECT * FROM logs where ".$where." (message like '%changed to banned from active%')
					and date(timestamp)<='".$currDate."' and date(timestamp)>='".$lastDate."' order by timestamp desc";
     	$stmt = $this->_db_table->getAdapter()->query($query);
     	$result = $stmt->fetchAll();
     	if( count($result) == 0 ) {
     		return false;
     	}
     	return $result;
     }
	
}