<?php
class Application_Model_RevenueTarget
{
    private $target_id;
    private $state_id;
    private $site_id; 
    private $month;
    private $year;
    private	$target;
    private $opex;
	private $type;
      
    public function __construct($revenue_row = null)
    {
        if( !is_null($revenue_row) && $revenue_row instanceof Zend_Db_Table_Row ) {
			$this->target_id = $revenue_row->target_id;
			$this->state_id = $revenue_row->state_id;
			$this->site_id = $revenue_row->site_id;
			$this->year = $revenue_row->year;
			$this->month = $revenue_row->month;
			$this->target = $revenue_row->target;
			$this->opex = $revenue_row->opex;
			$this->type = $revenue_row->type;
        }
    }
    
	public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

