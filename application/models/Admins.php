<?php

class Application_Model_Admins
{
    private $admin_id;
    private $username;
    private $hashed_password;
    private $admin_fname;
    private $admin_lname;
    private $admin_email;
    private $profile_image;
    private $admin_role;
    private $reset_code;

    public function __construct($admin_row = null)
    {
        if( !is_null($admin_row) && $admin_row instanceof Zend_Db_Table_Row ) {
                $this->admin_id = $admin_row->admin_id;
                $this->username = $admin_row->username;
                $this->hashed_password = $admin_row->hashed_password;
                $this->admin_fname = $admin_row->admin_fname;
                $this->admin_lname = $admin_row->admin_lname;
                $this->admin_email = $admin_row->admin_email;
                $this->profile_image = $admin_row->profile_image;
                $this->admin_role = $admin_row->admin_role;
                $this->reset_code = $admin_row->reset_code;
        }
    }
    public function __set($name, $value)
    {
           
      $this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }

}

