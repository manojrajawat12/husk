<?php
class Application_Model_ClusterManagersMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_ClusterManagers();
    }

    public function addNewClusterManager(Application_Model_ClusterManagers $clusterManager)
    {
        $data = array(
	'cluster_manager_name' => $clusterManager->__get("cluster_manager_name"),
	'cluster_manager_phone' => $clusterManager->__get("cluster_manager_phone"),
	'cluster_manager_email' => $clusterManager->__get("cluster_manager_email"),
	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getClusterManagerById($cluster_manager_id)
    {
        $result = $this->_db_table->find($cluster_manager_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $clusterManager = new Application_Model_ClusterManagers($row);
        return $clusterManager;
    }
    public function getAllClusterManagers()
    {
        $result = $this->_db_table->fetchAll(null,array('cluster_manager_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $clusterManager_object_arr = array();
        foreach ($result as $row)
        {
                $clusterManager_object = new Application_Model_ClusterManagers($row);
                array_push($clusterManager_object_arr,$clusterManager_object);
        }
        return $clusterManager_object_arr;
    }
    public function updateClusterManager(Application_Model_ClusterManagers $clusterManager)
    {
        $data = array(
	'cluster_manager_name' => $clusterManager->__get("cluster_manager_name"),
	'cluster_manager_phone' => $clusterManager->__get("cluster_manager_phone"),
	'cluster_manager_email' => $clusterManager->__get("cluster_manager_email"),
	);
        $where = "cluster_manager_id = " . $clusterManager->__get("cluster_manager_id");
        $result = $this->_db_table->update($data,$where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteClusterManagerById($cluster_manager_id)
    {
        $where = "cluster_manager_id = " . $cluster_manager_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
}
