<?php

class Application_Model_Celamed
{
    private $id;
    private $consumer_id;
    private $site_id;
    private $intervention_type;
    private $equipment_cost_total;
    private $equipment_cost_tara;
    private $equipment_type;
    private $equipment_summary;
    private $wattage;
    private $size;
    private $device_type;
    private $description;
    private $Contact_date;
    private $payment_date;
    private $completion_date;
    
    
    public function __construct($consumer_row = null)
    {
        if( !is_null($consumer_row) && $consumer_row instanceof Zend_Db_Table_Row ) {
                $this->id = $consumer_row->id;
                $this->consumer_id = $consumer_row->consumer_id;
                $this->site_id = $consumer_row->site_id;
                $this->intervention_type = $consumer_row->intervention_type;
                $this->equipment_cost_total = $consumer_row->equipment_cost_total;
                $this->equipment_cost_tara = $consumer_row->equipment_cost_tara;
                $this->equipment_type = $consumer_row->equipment_type;
                $this->equipment_summary = $consumer_row->equipment_summary;
                $this->Contact_date = $consumer_row->Contact_date;
                $this->payment_date = $consumer_row->payment_date;
                $this->completion_date = $consumer_row->completion_date;
                $this->wattage = $consumer_row->wattage;
                $this->size = $consumer_row->size;
                $this->device_type=$consumer_row->device_type;
                $this->description = $consumer_row->description;
         
                
        }
    }
    public function __set($name, $value)
    {
           
         $this->$name = $value;
            
    }
    public function __get($name)
    {
            return $this->$name;
    }

}

