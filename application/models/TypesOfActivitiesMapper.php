<?php
class Application_Model_TypesOfActivitiesMapper
{
	protected $_db_table;

	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_TypesOfActivities();
	}

	public function getAllActivities()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('id'));

		if( count($result) == 0 ) {
			return false;
		}
		$Form_object_arr = array();
		foreach ($result as $row)
		{
			$Form_object = new Application_Model_TypesOfActivities($row);
			array_push($Form_object_arr,$Form_object);
		}
		return $Form_object_arr;
	}
}