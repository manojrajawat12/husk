<?php

class Application_Model_SmsGateway
{
    private $gateway_id;
    private $user_name;
    private $password;
    

    public function __construct($gateway_row = null)
    {
        if( !is_null($gateway_row) && $gateway_row instanceof Zend_Db_Table_Row ) {
            
                $this->gateway_id = $gateway_row->gateway_id;
                $this->user_name = $gateway_row->user_name;
                $this->password = $gateway_row->password;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }
}

