<?php

class Application_Model_ConsumerType
{
    private $consumer_type_id;
    private $consumer_type_name;
    private $consumer_type_code;
    private $type_id;
    private $people_impact;
	private $status;
    private $parent_id;
  
   
    public function __construct($consumer_type_row = null)
    {
        if( !is_null($consumer_type_row) && $consumer_type_row instanceof Zend_Db_Table_Row ) {
            
            $this->consumer_type_id = $consumer_type_row->consumer_type_id;
            $this->consumer_type_name = $consumer_type_row->consumer_type_name;
            $this->consumer_type_code = $consumer_type_row->consumer_type_code;
            $this->type_id = $consumer_type_row->type_id;
            $this->people_impact = $consumer_type_row->people_impact;
			$this->status = $consumer_type_row->status;
            $this->parent_id = $consumer_type_row->parent_id;
            
        }
    } 
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

