<?php
class Application_Model_PermissionMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Permission();
	}

	public function addNewPermission(Application_Model_Permission $permission)
	{

		$data = array(
				'menu_id' => $permission->__get("menu_id"),
				'role_id' => $permission->__get("role_id"),
				'permission' => $permission->__get("permission"),
// 				'parent_menu' => $permission->__get("parent_menu"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getPermissionById($Permission_id)
	{
		$result = $this->_db_table->find($Permission_id);
		if( count($result) == 0 ) {
			$Permission = new Application_Model_Permission();
			return $Permission;
		}
		$row = $result->current();
		$Permission = new Application_Model_Permission($row);
		return $Permission;
	}
	
	public function getAllPermission()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('permission_id ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$permission_object_arr = array();
		foreach ($result as $row)
		{
			$permission_object = new Application_Model_Permission($row);
			array_push($permission_object_arr,$permission_object);
		}
		return $permission_object_arr;
	}
	
	public function updatePermission(Application_Model_Permission $permission)
	{
		$data = array(
				'menu_id' => $permission->__get("menu_id"),
				'role_id' => $permission->__get("role_id"),
				'permission' => $permission->__get("permission"),
// 				'parent_menu' => $permission->__get("parent_menu"),
		);
		$where = "permission_id = " . $permission->__get("permission_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	public function deletePermissionById($role_id)
	{
		$where = "role_id = " . $role_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public function getAllMenuIdByRoleId($role_id)
	{
	
		$query="SELECT p.menu_id,p.role_id, m.parent_menu,(select menu_desc from menu where menu_id=m.parent_menu) as menu_desc ,m.menu_desc as subMenu_name from permission p inner join menu m on p.menu_id=m.menu_id where role_id='$role_id' order by menu_id";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		//print_r($result);
		if( count($result) == 0 ) {
			return false;
		}
		$menu_object_arr = array();
		foreach ($result as $row)
		{
			$menu_object = new Application_Model_Menu($row);
			$menu_object->__set("menu_id", $row["menu_id"]);
			$menu_object->__set("role_id", $row["role_id"]);
 			$menu_object->__set("parent_menu", $row["parent_menu"]);
 			$menu_object->__set("menu_desc", $row["menu_desc"]);
 			$menu_object->__set("subMenu_name", $row["subMenu_name"]);
			array_push($menu_object_arr,$menu_object);
		}
		return $menu_object_arr;
	
	}
	public function deleteMenuById($role_id,$menu_id=NULL)
	{
		if($menu_id==NULL){
			$where = "role_id = " . $role_id;
		}
		else{
			$where = " menu_id not in (".implode(",", $menu_id).")  and role_id = ".$role_id;
		}
		  
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function checkMenuById($role_id,$menu_id)
	{
		$query="SELECT * from permission where role_id=".$role_id." and menu_id =".$menu_id;
		
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		$count=count($result);
		 
		if( $count == 0 ) {
			return $count;
		}
		else
		{
			return $count;
		}
	}
}