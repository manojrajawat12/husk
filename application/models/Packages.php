<?php

class Application_Model_Packages
{
    private $package_id;
    private $package_name;
    private $package_cost;
    private $package_details;
    private $is_postpaid;
    private $wattage;
    private $unit;
	private $extra_charges;
    private $status;
 	private $feeder_hours;
 	private $feeder_id;
 	private $state_id;
 	private $site_id;
 	private $created_date;
 	private $connection_type; 
 	private $updated_date;
	private $user_id;
	private $remark;
 	private $update_type;
 	private $approve_by;
	private $light_load;
	private $security_deposit;
    public function __construct($package_row = null)
    {
        if( !is_null($package_row) && $package_row instanceof Zend_Db_Table_Row ) {
                $this->package_id = $package_row->package_id;
                $this->package_name = $package_row->package_name;
                $this->package_cost = $package_row->package_cost;
                $this->package_details = $package_row->package_details;
                $this->is_postpaid = $package_row->is_postpaid;
                $this->wattage = $package_row->wattage;
                $this->unit = $package_row->unit;
                $this->extra_charges = $package_row->extra_charges;
                $this->status = $package_row->status;
   				$this->feeder_hours=$package_row->feeder_hours;
   				$this->feeder_id=$package_row->feeder_id;
   				$this->state_id=$package_row->state_id;
   				$this->site_id=$package_row->site_id;
   				$this->created_date=$package_row->created_date;
   				$this->connection_type=$package_row->connection_type;
   				$this->updated_date=$package_row->updated_date;
				$this->user_id=$package_row->user_id;
				$this->remark=$package_row->remark;
   				$this->update_type=$package_row->update_type;
   				$this->approve_by=$package_row->approve_by;
				$this->light_load=$package_row->light_load;  
				$this->security_deposit=$package_row->security_deposit;
        }
    }
public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }

}

