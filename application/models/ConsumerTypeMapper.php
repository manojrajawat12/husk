<?php
class Application_Model_ConsumerTypeMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_ConsumerTypes();
	}

	public function addNewConsumerType(Application_Model_ConsumerType $ConsumerType)
	{
		
		$data = array(
		    'consumer_type_name' => $ConsumerType->__get("consumer_type_name"),
		    'consumer_type_code' => $ConsumerType->__get("consumer_type_code"),
		    'parent_id' => ($ConsumerType->__get("status")!='menu')?$ConsumerType->__get("parent_id"):NULL,
			'status' => $ConsumerType->__get("status"),
		    'people_impact' => $ConsumerType->__get("people_impact"),
		    				
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else 
		{
			return $result;
		}
	}
	
	public function getConsumerTypeById($Consumer_type_id)
	{

	    $result = $this->_db_table->find($Consumer_type_id);
		
		if( count($result) == 0 ) {
		    $ConsumerType = new Application_Model_ConsumerType();
		    return $ConsumerType;
		}
		$row = $result->current();
		$ConsumerType = new Application_Model_ConsumerType($row);
	
		return $ConsumerType;
	}
	
	public function getAllConsumerType()
	{
	    $where = array();
	    
		$result = $this->_db_table->fetchAll($where,array('status asc'));
		if( count($result) == 0 ) {
			return false;
		}
		$ConsumerType_object_arr = array();
		foreach ($result as $row)
		{
		    $ConsumerType_object = new Application_Model_ConsumerType($row);
		    array_push($ConsumerType_object_arr,$ConsumerType_object);
		}
		return $ConsumerType_object_arr;
	}
		
	public function updateConsumerType(Application_Model_ConsumerType $ConsumerType)
	{
		$data = array(
				'consumer_type_name' => $ConsumerType->__get("consumer_type_name"),
				'consumer_type_code' => $ConsumerType->__get("consumer_type_code"),
				'parent_id' =>  ($ConsumerType->__get("status")!='menu')?$ConsumerType->__get("parent_id"):NULL,
				'status' => $ConsumerType->__get("status"),
				'people_impact' => $ConsumerType->__get("people_impact"), 
				 
		);
		$where = "Consumer_type_id = " . $ConsumerType->__get("consumer_type_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function deleteConsumerTypeById($id)
	{
	    $where = "Consumer_type_id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
	
			return true;
		}
	}
	
	
	

}