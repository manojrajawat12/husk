<?php
class Application_Model_UpdatedConsumersMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_UpdatedConsumers();
	}

	public function addNewUpdatedConsumers(Application_Model_UpdatedConsumers $updated_consumer)
	{
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		$data = array(
				'consumer_id' => $updated_consumer->__get("consumer_id"),
				'timestamp' => $timestamp,
				'previous_debit_value' => $updated_consumer->__get("previous_debit_value"),
				'current_debit_value' => $updated_consumer->__get("current_debit_value"),
				'cr_id' => $updated_consumer->__get("cr_id"),
				'previous_package_id' => $updated_consumer->__get("previous_package_id"),
				'current_package_id' => $updated_consumer->__get("current_package_id")
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function truncateUpdatedConsumer()
	{
		$query="TRUNCATE TABLE updated_consumers";
		$this->_db_table->getAdapter()->query($query);
		return $this;
	}
	public function getUpdatedConsumersById($updated_consumer_id)
	{
		$result = $this->_db_table->find($updated_consumer_id);
		if( count($result) == 0 ) {
			$updatedConsumer = new Application_Model_UpdatedConsumers();
			return $updatedConsumer;
		}
		$row = $result->current();
		$updatedConsumer = new Application_Model_UpdatedConsumers($row);
		return $updatedConsumer;
	}
	
	public function getAllUpdatedConsumers()
	{
		$result = $this->_db_table->fetchAll(null,array('updated_consumers_id ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$updated_consumer_object_arr = array();
		foreach ($result as $row)
		{
			$updated_consumer_object = new Application_Model_UpdatedConsumers($row);
			array_push($updated_consumer_object_arr,$updated_consumer_object);
		}
		return $updated_consumer_object_arr;
		
	}
	
	
	public function deleteUpdatedConsumerById($updated_consumer_id)
	{
		$where = "updated_consumer_id = " . $updated_consumer_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}