<?php

class Application_Model_Settings
{
    private $setting_id;
    private $setting_name;
    private $setting_value;

    public function __construct($setting_row = null)
    {
        if( !is_null($setting_row) && $setting_row instanceof Zend_Db_Table_Row ) {
                $this->setting_id = $setting_row->setting_id;
                $this->setting_name = $setting_row->setting_name;
                $this->setting_value = $setting_row->setting_value;
                
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "settings_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }

}

