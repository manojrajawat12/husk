<?php
class Application_Model_SiteMasterMeterMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_SiteMasterMeter();
    }

    public function addNewSiteMaster(Application_Model_SiteMasterMeter $meter_master)
    {  
		$date = new Zend_Date(); 
        $date->setTimezone("Asia/Calcutta");
        $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");  
        
		$data = array(
			'site_id' => $meter_master->__get("site_id"),
			'sim_no' => $meter_master->__get("sim_no"),
			'meter_name' => $meter_master->__get("meter_name"),
			'meter_keyword' => $meter_master->__get("meter_keyword"),
        	'description' => $meter_master->__get("description"),
			'start_time' => $meter_master->__get("start_time"),
			'end_time' => $meter_master->__get("end_time"),
        	'is_24_hr' => $meter_master->__get("is_24_hr"),
        	'feeder_type' => $meter_master->__get("feeder_type"),	
			'status' => "new",
			'timestamp' =>  $timestamp,
        );
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return $result;
        }
    }
    public function getMMById($id)
    {
    	 
    	$result = $this->_db_table->find($id);
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$row = $result->current();
    	$cluster = new Application_Model_SiteMasterMeter($row);
    	return $cluster;
    }
    public function getSiteMasterById($site_id)
    {
         $query = " SELECT * From site_meter  where site_id ='$site_id' order by feeder_no asc  ";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$SiteMaster_object_arr = array();
    	foreach ($result as $row)
    	{
    		$starttime=($row["start_time"]!=null)?$row["start_time"]:"";
    		$endtime=($row["end_time"]!=null)?$row["end_time"]:"";
    		$SiteMaster_object = new Application_Model_SiteMasterMeter($row);
    		$SiteMaster_object->__set("id", $row["id"]);
			$SiteMaster_object->__set("site_id", $row["site_id"]);
			$SiteMaster_object->__set("sim_no", $row["sim_no"]);
    		$SiteMaster_object->__set("meter_name", $row["meter_name"]);
    		$SiteMaster_object->__set("meter_keyword", $row["meter_keyword"]);
    		$SiteMaster_object->__set("description", $row["description"]);
			$SiteMaster_object->__set("start_time", $starttime);
			$SiteMaster_object->__set("end_time", $endtime);
			$SiteMaster_object->__set("is_24_hr", $row["is_24_hr"]);
			$SiteMaster_object->__set("feeder_type", $row["feeder_type"]);
			$SiteMaster_object->__set("meter_no", $row["meter_no"]); 
    		array_push($SiteMaster_object_arr,$SiteMaster_object);
    	}
    	return $SiteMaster_object_arr;
    }
    public function getAllSiteMeter($status=NULL)
    {
    	$stat="";
    	if($status!=NULL){
    		$stat=" and site_meter.status='".$status."'";
    	}else{
    		$stat=" and site_meter.status='enable'";
    	}
          $query = " SELECT * From site_meter inner join sites on site_meter.site_id=sites.site_id where sites.site_status!='Disable' 
						and site_meter.status!='disable' ".$stat."  order by feeder_no asc";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$SiteMaster_object_arr = array();
    	foreach ($result as $row)
    	{
    		$starttime=($row["start_time"]!=null)?$row["start_time"]:"";
    		$endtime=($row["end_time"]!=null)?$row["end_time"]:"";
    		$SiteMaster_object = new Application_Model_SiteMasterMeter($row);
    		$SiteMaster_object->__set("id", $row["id"]);
			$SiteMaster_object->__set("sim_no", $row["sim_no"]);
    		$SiteMaster_object->__set("site_id", $row["site_id"]);
    		$SiteMaster_object->__set("meter_name", $row["meter_name"]);
    		$SiteMaster_object->__set("meter_keyword", $row["meter_keyword"]);
    		$SiteMaster_object->__set("description", $row["description"]);
			$SiteMaster_object->__set("start_time", $starttime);
			$SiteMaster_object->__set("end_time", $endtime);
			$SiteMaster_object->__set("is_24_hr", $row["is_24_hr"]);
			$SiteMaster_object->__set("feeder_type", $row["feeder_type"]);
			$SiteMaster_object->__set("timestamp", $row["timestamp"]);
    		array_push($SiteMaster_object_arr,$SiteMaster_object);
    	}
    	return $SiteMaster_object_arr;
    }
   public function updateSiteMeter(Application_Model_SiteMasterMeter $siteMeter)
    {
       
        $site_id= $siteMeter->__get("site_id");
        $sim_no= $siteMeter->__get("sim_no");
        $id=$siteMeter->__get("id");
        $meter_name=$siteMeter->__get("meter_name");
        $meter_keyword=$siteMeter->__get("meter_keyword");
        $description=$siteMeter->__get("description");
		$start_time=$siteMeter->__get("start_time");
		$end_time=$siteMeter->__get("end_time");
		$is_24_hr=$siteMeter->__get("is_24_hr");
		$feeder_type=$siteMeter->__get("feeder_type");
         $date = new Zend_Date(); 
            $date->setTimezone("Asia/Calcutta");
            $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");  
			
      $query ="update site_meter set site_id=".$site_id.",meter_name='".$meter_name."',meter_keyword='".$meter_keyword."'
      		,feeder_type='".$feeder_type."',description='".$description."',start_time='".$start_time."',end_time='".$end_time."'
      		,is_24_hr='".$is_24_hr."',sim_no='".$sim_no."',timestamp='".$timestamp."' where id=".$id;  

 
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result=$stmt->execute();
        
        if($result)
        {
        	return true;
        }
    }
    public function deleteEnterprisePriceById($enterprise_price_id)
    {
        $where = "enterprise_price_id = " . $enterprise_price_id;
        $result = $this->_db_table->delete($where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
  public function  getEnterprisePriceByUnit($consumer_id)
   {
	   	$result = $this->_db_table->fetchAll(null,array('unit_from ASC'));
	   	if( count($result) == 0 ) {
	   		return false;
	   	}
	   	$enterprise_object_arr = array();
	   	foreach ($result as $row)
	   	{
	   		$enterprise_object = new Application_Model_EnterprisePrice($row);
	   		array_push($enterprise_object_arr,$enterprise_object);
	   	}
	   	return $enterprise_object_arr;
   }
   
   public function  deleteSiteMeterBySiteId($site_id)
   {
   		$query ="delete from site_meter where site_id=".$site_id;

        $stmt = $this->_db_table->getAdapter()->query($query);
        $result=$stmt->execute();
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
   }
   public function  deleteSiteMeterById($id)
   {
   	$query ="delete from site_meter where id=".$id;
   
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result=$stmt->execute();
   	if($result==0)
   	{
   		return false;
   	}
   	else
   	{
   		return true;
   	}
   }
   
   public function getMasterMeterReadingByMeterId($meter_id)
   {
		$query = " SELECT * From site_meter_reading where meter_id ='$meter_id' order by reading desc limit 1";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		 
		if( count($result) == 0 ) {
			return false;
		}
		$SiteMaster_object_arr = array();
		foreach ($result as $row)
		{
			$SiteMaster_object = new Application_Model_SiteMeterReading($row);
			$SiteMaster_object->__set("id", $row["id"]);
			$SiteMaster_object->__set("meter_id", $row["meter_id"]);
			$SiteMaster_object->__set("site_id", $row["site_id"]);
			$SiteMaster_object->__set("reading_date", $row["reading_date"]);
			$SiteMaster_object->__set("reading", $row["reading"]);
			array_push($SiteMaster_object_arr,$SiteMaster_object);
		}
		return $SiteMaster_object_arr;
   }
   
   public function checkSiteMasterBySiteId($site_id)
   {
   	$query = " SELECT * From site_meter  where site_id ='$site_id'  order by feeder_no asc";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
    
   	return $result;
   }
   
    public function getMasterMeterByMeterKey($meter_keyword)
   {
   	$query = " SELECT * From site_meter inner join site_meter_reading on site_meter.id=site_meter_reading.meter_id 
				where site_meter.meter_keyword ='".$meter_keyword."' and site_meter.status!='disable' order by reading_date desc";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
    
   		return $result;
   }
   
      public function getMasterMeterByMeterName($meter_name)
   {
   	$query = " SELECT * From site_meter where meter_keyword ='$meter_name' and status!='disable'";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
   
   	return $result;
   }
   
   public function getMasterMeterByMeterSite($meter_name,$site_id)
   {
   	$query = " SELECT * From site_meter where meter_name ='$meter_name' and site_id=".$site_id." and status!='disable'  order by id";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
   	 
   	return $result;
   }
   
   public function  deleteSiteMeterBySiteIdMeter($meter_name,$site_id)
   {
   	$query ="update site_meter set status='disable' where site_id=".$site_id." and meter_name ='".$meter_name."'";  
   
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result=$stmt->execute();
   	if($result==0)
   	{
   		return false;
   	}
   	else
   	{
   		return true;
   	}
   }
   
   public function getMasterMeterByNumber($number=null,$site_id=null)
   {
   	$query = " SELECT * From site_meter where sim_no ='".$number."' and site_id=".$site_id." and status!='disable'";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
   	 
   	return $result[0];
   }
   
    public function geteditMasterMeterByNumber($number=null,$site_id=null)
   {
   	$query = " SELECT * From site_meter where sim_no ='".$number."' and site_id!=".$site_id." and status!='disable'";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
   	 
   	return $result[0];
   }
   
    public function getMasterMeterByStartEndTime($start=null,$end=null,$id=null)
   {
      $query = " SELECT * From site_meter where start_time =".$start." and end_time=".$end." and id=".$id." and status!='disable'";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
   	 
   return $result[0];
   }
    public function updateSiteNo($sim_no=null,$site_id=null)
   {
   	 
   
   	$query ="update site_meter set sim_no='".$sim_no."' where site_id=".$site_id;
   
   
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result=$stmt->execute();
   
   	if($result)
   	{
   		return true;
   	}
   }
   
    public function updateFeedersById($id=NULL,$status=NULL,$remark=NULL,$approve_by=NULL){
   
   	$where="";
   	if($status!="enable"){
   		$where="id=".$id;
   	}else{
   		$where="id in (".$id.")";
   	}
   	$date = new Zend_Date();
   	$date->setTimezone("Asia/Calcutta");
   	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
   	 
   	$query ="update site_meter set status='".$status."',remark='".$remark."',approve_date='".$timestamp."',approve_by=".$approve_by."   where ".$where;
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result=$stmt->execute();
   
   	if($result)
   	{
   		return true;
   	}
   }
   
      public function getMasterMeterReadingByFeederType($date=NULL,$site_id=NULL)
   {
    
   	 $query =  "SELECT 
   					(select sum(reading) FROM site_meter sm inner join site_meter_reading smr on smr.meter_id=sm.id
						where sm.feeder_type='DD' and sm.description!='Plant' and sm.site_id=".$site_id." and 
						date(reading_date)='".$date."') as DD_Unit,
					
					(select sum(reading) FROM site_meter sm inner join site_meter_reading smr on smr.meter_id=sm.id
						where sm.feeder_type='NN' and sm.description!='Plant' and sm.site_id=".$site_id." and
						date(reading_date)='".$date."') as NN_Unit,
					
					(select sum(reading) FROM site_meter sm inner join site_meter_reading smr on smr.meter_id=sm.id
						where sm.feeder_type='DN' and sm.description!='Plant' and sm.site_id=".$site_id." and 
						date(reading_date)='".$date."') as DN_Unit,
					
					(select sum(reading) FROM site_meter sm inner join site_meter_reading smr on smr.meter_id=sm.id
						where  sm.description='Plant' and sm.site_id=".$site_id." and date(reading_date)='".$date."') as Plant_Unit
								
			 from site_meter limit 1"; 
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   
   	if( count($result) == 0 ) {
   		return false;
   	}
   	return $result[0];
   }
    
   public function getAllFeederBySiteID($site_id=NULL,$state_id=NULL,$role_site_id=NULL,$first=NULL,$feeder=NULL)
   {
	   	$feederjoin=" inner join sites s on s.site_id=sm.site_id";
	   	 
	   	if($site_id!=NULL){
	   		$feederFormat=" where sm.site_id in (".$site_id.") and date(sm.timestamp)<='".$first."' and s.site_status!='Disable'";
	   	}elseif($state_id!=NULL){
	   		$feederFormat=" where s.state_id in (".$state_id.") and date(sm.timestamp)<='".$first."' and s.site_status!='Disable'";
	   	}else{
	   		$feederFormat=" where sm.site_id in (".implode(",", $role_site_id).") and date(sm.timestamp)<='".$first."' and s.site_status!='Disable'";
	   	}
	   	
	   	if($feeder!=NULL){
	   		$feederFormat.=" and id in (".$feeder.")";
	   	}
	   	 
	   	$query = "select sm.* from site_meter sm ".$feederjoin.$feederFormat." ";
	    
	   	$stmt = $this->_db_table->getAdapter()->query($query);
	   	$result = $stmt->fetchAll();
	   
	   	if( count($result) == 0 ) {
	   		return false;
	   	}else{
	   		return $result;
	   	}
   }
   
   public function getMasterMeterReadingByDate($date=NULL,$site_id=NULL)
   {
	   
	   	$site_ids="";
	   	if($site_id!=NULL){
	   		$site_ids=" smr.site_id in (".implode(",", $site_id).")"; 
	   	}
		
		$dates="";
	   	if($date!=NULL){
	   		$dates=" and date(smr.reading_date)='".$date."'";
	   	}
	   	
	    $query = " SELECT * From site_meter sm inner join site_meter_reading smr on sm.id=smr.meter_id where ".$site_ids.$dates." order by sm.feeder_no asc";
	       
	   	$stmt = $this->_db_table->getAdapter()->query($query);
	   	$result = $stmt->fetchAll();
	   
	   	if( count($result) == 0 ) {
	   		return false;
	   	}
	    return $result;
   }
   public function getMasterMeterRecord($feeder_id=NULL,$date=NULL,$status=NULL)
   {
   	
		$dates="";
		if($date!=NULL){ 
			$dates=" and date(smr.reading_date)='".$date."'";
		}
		
		if($status!=NULL){
			$dates=" and date(smr.reading_date)<='".$date."'";
		} 
		 
		
	    $query = " SELECT * From site_meter sm inner join site_meter_reading smr on sm.id=smr.meter_id where smr.meter_id=".$feeder_id.$dates." order by smr.reading_date desc";
		
	   	$stmt = $this->_db_table->getAdapter()->query($query);
	   	$result = $stmt->fetchAll();
	   
	   	if( count($result) == 0 ) {
	   		return false;
	   	}
	   	return $result[0];
   }
     public function updateFeedersByDateAndId($meter_id=NULL,$reading,$unit=NULL,$date=NULL){
    	 
    	$where=" date(reading_date)='".$date."' and meter_id=".$meter_id;
    	$query ="update site_meter_reading set reading='".$unit."',actual_reading='".$reading."'  where ".$where; 
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    	 
    	if($result)
    	{
    		return true;
    	}
    }
}

