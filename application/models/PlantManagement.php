<?php

class Application_Model_PlantManagement
{
	private $id;
    private $state_id;
    private $site_id;
    private $activity_by;
    private $activity_date;
    private $maintenance_date;
    private $sup_by;
    private $remark;
    private $user_id;
    private $timestamp;
    private $user_type;

    public function __construct($plant_management_row = null)
    {
        if( !is_null($plant_management_row) && $plant_management_row instanceof Zend_Db_Table_Row ) {
        		$this->id = $plant_management_row->id;
                $this->state_id = $plant_management_row->state_id;
                $this->site_id = $plant_management_row->site_id;
                $this->activity_by = $plant_management_row->activity_by;
                $this->activity_date = $plant_management_row->activity_date;
                $this->maintenance_date = $plant_management_row->maintenance_date;
                $this->sup_by = $plant_management_row->sup_by;
                $this->remark = $plant_management_row->remark;
                $this->user_id = $plant_management_row->user_id;
                $this->timestamp = $plant_management_row->timestamp;
                $this->user_type = $plant_management_row->user_type;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

