<?php

class Application_Model_Opex
{
    private $id;
    private $types_of_cost;
    private $parent_id;
    private $menu_type;

    public function __construct($Opex_row = null)
    {
        if( !is_null($Opex_row) && $Opex_row instanceof Zend_Db_Table_Row ) {
        
            $this->id = $Opex_row->id;
            $this->types_of_cost = $Opex_row->types_of_cost;
            $this->parent_id = $Opex_row->parent_id;
            $this->menu_type = $Opex_row->menu_type;
        }
    }
 
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

