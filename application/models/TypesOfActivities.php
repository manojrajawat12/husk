<?php

class Application_Model_TypesOfActivities
{
    private $id;
    private $activity;

    public function __construct($Activites_row = null)
    {
        if( !is_null($Activites_row) && $Activites_row instanceof Zend_Db_Table_Row ) {

                $this->id = $Activites_row->id;
                $this->activity = $Activites_row->activity;
        }
    }
/*--------------------- 22 Jan 2018 (end) ----------------------------------*/
    public function __set($name, $value)
    {   
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

