<?php
class Application_Model_MeterData
{
	
	
	private $id;
	private $site_id;
	private $meter_id;
	private $ins_vol;
	private $ins_cur;
	private $pow_fac;
	private $ins_freq;
	private $act_eng;
	private $app_eng;

	public function __construct($change_row = null)
	{
		if( !is_null($change_row) && $change_row instanceof Zend_Db_Table_Row ) {

			$this->id = $change_row->id;
			$this->site_id = $change_row->site_id;
			$this->meter_id = $change_row->meter_id;
			$this->ins_vol = $change_row->ins_vol;
			$this->ins_cur = $change_row->ins_cur;
			$this->pow_fac = $change_row->pow_fac;
			$this->ins_freq = $change_row->ins_freq;
			$this->act_eng = $change_row->act_eng;
			$this->app_eng = $change_row->app_eng;

		}
	}
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}
}