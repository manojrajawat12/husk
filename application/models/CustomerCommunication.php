<?php

class Application_Model_CustomerCommunication
{
    private $id;
    private $consumer_id;
    private $state_id;
    private $site_id;
    private $query_id;
	private $sub_query;
    private $query_comment;
    private $query_status;
    private $user_id;
    private $timestamp;
    private $user_type;
	
	private $amount;
    private $entry_date;
    private $effective_date;
    private $attachment;
	private $verified_by;
    private $verified_date;
	private $attach_sec;

    public function __construct($customercommunication_row = null)
    {
        if( !is_null($customercommunication_row) && $customercommunication_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $customercommunication_row->id;
                $this->consumer_id = $customercommunication_row->consumer_id;
                $this->state_id = $customercommunication_row->state_id;
                $this->site_id = $customercommunication_row->site_id;
                $this->query_id = $customercommunication_row->query_id;
				$this->sub_query = $customercommunication_row->sub_query;
                $this->query_comment = $customercommunication_row->query_comment;
                $this->query_status = $customercommunication_row->query_status;
                $this->user_id = $customercommunication_row->user_id;
                $this->timestamp = $customercommunication_row->timestamp;
                $this->user_type = $customercommunication_row->user_type;
				
				$this->amount = $customercommunication_row->amount;
                $this->entry_date = $customercommunication_row->entry_date;
                $this->effective_date = $customercommunication_row->effective_date;
                $this->attachment = $customercommunication_row->attachment; 
				$this->verified_by = $customercommunication_row->verified_by;
				$this->verified_date = $customercommunication_row->verified_date;  
				$this->attach_sec = $customercommunication_row->attach_sec;  
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "ap_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

