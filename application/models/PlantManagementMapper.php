<?php
class Application_Model_PlantManagementMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_PlantManagement();
	}

	public function addPlant($plant)
	{
				
		$data = array(
				'state_id' => $plant->__get("state_id"),
				'site_id' => $plant->__get("site_id"),
				'activity_by' => $plant->__get("activity_by"),
				'activity_date' => $plant->__get("activity_date"),
				'maintenance_date' => $plant->__get("maintenance_date"),
				'sup_by' => $plant->__get("sup_by"),
				'remark' => $plant->__get("remark"),
				'user_id' => $plant->__get("user_id"),
				'timestamp' => $plant->__get("timestamp"),
				'user_type' => $plant->__get("user_type"),
				'activity_type'=>$plant->__get("activity_type"),
				'attachment'=>$plant->__get("attachment"),
				
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	
	public function getAllPlants()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('id DESC'));
		if( count($result) == 0 ) {
			return false;
		}
		$plant_object_arr = array();
		foreach ($result as $row)
		{
			$plant_object = new Application_Model_PlantManagement($row);
			array_push($plant_object_arr,$plant_object);
		}
		return $plant_object_arr;
	}
	
	public function updatePlantById(Application_Model_PlantManagement $plant)
	{
		$data = array(
				'id'=> $plant->__get("id"),
				'state_id' => $plant->__get("state_id"),
				'site_id' => $plant->__get("site_id"),
				'activity_by' => $plant->__get("activity_by"),
				'activity_date' => $plant->__get("activity_date"),
				'maintenance_date' => $plant->__get("maintenance_date"),
				'sup_by' => $plant->__get("sup_by"),
				'remark' => $plant->__get("remark"),
				'user_id' => $plant->__get("user_id"),
				'timestamp' => $plant->__get("timestamp"),
				
		);
		$where = "id = " . $plant->__get("id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	public function deletePlantById($id)
	{
		$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}