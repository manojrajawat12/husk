<?php

class Application_Model_RoleSite
{
    private $role_site_id;
    private $role_id;
    private $site_id;
    

    public function __construct($role_site_row = null)
    {
        if( !is_null($role_site_row) && $role_site_row instanceof Zend_Db_Table_Row ) {
            
                $this->role_site_id = $role_site_row->role_site_id;
                $this->role_id = $role_site_row->role_id;
                $this->site_id = $role_site_row->site_id;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}
