<?php
class Application_Model_SparkDataMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_SparkData();
	}
 
	public function addSparkData(Application_Model_SparkData $SparkData)
	{
		$data = array(
			'code' => $SparkData->__get("code"),
			'credit_balance' => $SparkData->__get("credit_balance"),
			'debt_balance' => $SparkData->__get("debt_balance"),
			'ground_id' => $SparkData->__get("ground_id"),
			'ground_name' => $SparkData->__get("ground_name"),
			'customer_id' => $SparkData->__get("customer_id"),
			'active' => $SparkData->__get("active"),
			'address' => $SparkData->__get("address"),
			'coords' => $SparkData->__get("coords"),
			'current_tariff_name' => $SparkData->__get("current_tariff_name"),
			'is_running_plan' => $SparkData->__get("is_running_plan"),
			'last_energy' => $SparkData->__get("last_energy"),
			'unit' => $SparkData->__get("unit"),
			'last_energy_datetime' => $SparkData->__get("last_energy_datetime"),
			'operating_mode' => $SparkData->__get("operating_mode"),
			'plan_balance' => $SparkData->__get("plan_balance"),
			'serial' => $SparkData->__get("serial"),
			'total_cycle_energy' => $SparkData->__get("total_cycle_energy"),
			'customer_name' => $SparkData->__get("customer_name"),
			'phone_number' => $SparkData->__get("phone_number"),
			'phone_number_verified' => $SparkData->__get("phone_number_verified"),
			'timestamp'=>$SparkData->__get("timestamp"), 
		);
		
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	
	public function getAllSparkData()
	{
		$query=" Select * from spark_data";
    
    	$stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
		
		if( count($result) == 0 ) {
			return false;
		}else{
			return $result;
		}
	}
	
	public function getLatestReadingByConsumerId($consumer_id=NULL)
	{
		$query=" Select * from spark_data where customer_id='".$consumer_id."' order by timestamp desc" ;
		
    	$stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
		
		$blank=array();
		if( count($result) == 0 ) {
			return $blank;
		}else{
			return $result;
		}
	}
	 
}