<?php

class Application_Model_PlantVisit
{
    private $id;
    private $site_id;
    private $date_of_visit;
    private $hours_on_plant;
    private $attachment;
    private $issues;
    private $remark;
    private $entry_by;  
    private $entry_date;
    private $approve_by;
    private $approve_date;

    public function __construct($plantvisit_row = null)
    {
        if( !is_null($plantvisit_row) && $plantvisit_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $plantvisit_row->id;
                $this->site_id = $plantvisit_row->site_id;
                $this->date_of_visit = $plantvisit_row->date_of_visit;
                $this->hours_on_plant = $plantvisit_row->hours_on_plant;
                $this->attachment = $plantvisit_row->attachment;
                $this->issues = $plantvisit_row->issues;
                $this->remark = $plantvisit_row->remark;
                $this->entry_by = $plantvisit_row->entry_by;
                $this->entry_date = $plantvisit_row->entry_date;
                $this->approve_by = $plantvisit_row->approve_by;
                $this->approve_date = $plantvisit_row->approve_date;
        }
    }
    
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "ap_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

