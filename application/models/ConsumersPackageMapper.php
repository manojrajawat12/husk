<?php
class Application_Model_ConsumersPackageMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_ConsumersPackage();
    }

     public function addNewConsumersPackage(Application_Model_ConsumersPackage $ConsumersPackage , $trigger=1)
    {
        $data = array(
	        'consumer_id' => $ConsumersPackage->__get("consumer_id"),
            'package_id' => $ConsumersPackage->__get("package_id"),
     	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0){
                return false;
        }
        else{
        	 
        	$packageMapper=new Application_Model_PackagesMapper();
        	$consumerMapper=new Application_Model_ConsumersMapper();
        	$packages=$packageMapper->getPackageById($ConsumersPackage->__get("package_id"));
        	$package_cost=$packages->__get("package_cost");
        	$is_postpaid=$packages->__get("is_postpaid");
        	if($is_postpaid==0 && $trigger!=0){     
	        	$date = new Zend_Date(); //"2018-05-01",'yyyy-MM-dd'
	        	$date->setTimezone("Asia/Kolkata"); 
	        	$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
	        	$current_date = date_parse_from_format("Y-m-d", $curr_date);
	        	
	        	$day = $current_date["day"];
	        	$month = sprintf("%02d", $current_date["month"]);
	        	$year = $current_date["year"];
	        	$consumers=$consumerMapper->getConsumerById($ConsumersPackage->__get("consumer_id"));
	        	$Act_date = date_parse_from_format("Y-m-d", $consumers->__get("consumer_act_date"));
	        	$Act_day = $Act_date["day"];
	        	$Act_month = sprintf("%02d", $Act_date["month"]);
	        	$Act_year = $Act_date["year"];
	        	
	        	$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);
	        	if($month==$Act_month && $year==$Act_year){
	        		$package_val=($package_cost/$total_days_in_month) * ($total_days_in_month-$Act_day);
	        	}else{
	        		$package_val=($package_cost/$total_days_in_month) * ($total_days_in_month-$day); 
	        	}
				
	        	$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
	        	$cr_amount = str_pad(intval($package_val), 4, "0", STR_PAD_LEFT);
	        	$transaction_id = $timestamp . "-" . $cr_amount;
	        	 
	        	$cashRegisterMapper = new Application_Model_CashRegisterMapper();
	        	$cashRegister = new Application_Model_CashRegister();
	        	$cashRegister->__set("user_id", 0);
	        	$cashRegister->__set("consumer_id", $ConsumersPackage->__get("consumer_id"));
	        	$cashRegister->__set("cr_entry_type", "DEBIT");
	        	$cashRegister->__set("cr_amount", intval($package_val));
	        	$cashRegister->__set("receipt_number", 0);
	        	$cashRegister->__set("transaction_id", $transaction_id);
	        	$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
	        	$cashRegister->__set("entry_status", "CHG");
	        	$cashRegister->__set("remark", "New package added");
				$cashRegister->__set("approved_by", 0);
				$cashRegister->__set("approved_date", $date->toString("yyyy-MM-dd HH:mm:ss"));
	        	$cashRegister->__set("package_id",$ConsumersPackage->__get("package_id"));
	        	$cashRegisterMapper->addNewCashRegister($cashRegister);
        	}
            return $result;
        }
    }
     
    
    public function getPackageByConsumerId($consumer_id=null,$package_id=null)
    {
       	$package="";
    	if($package_id!=null){
    		$package=" and c.package_id=".$package_id;
    	}
    	 $query="SELECT p.*,c.consumer_id,c.consumer_package_id FROM consumer_packages c inner join packages p on p.package_id=c.package_id
  				where c.consumer_id='".$consumer_id."'".$package;
    
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else{
    		return $result;
    	}
    }
    
    public function updatePackageByConsumerID(Application_Model_ConsumersPackage $con_pack)
    {
        $data = array(
			'consumer_id' => $con_pack->__get("consumer_id"),
			'package_id' => $con_pack->__get("package_id"),
		);
        $where = "consumer_package_id = " . $con_pack->__get("consumer_package_id");
        $result = $this->_db_table->update($data,$where);
        if($result==0){
            return false;
        }
        else{
            return true;
        }
    }
    
    public function deletePackageByConsumerId($consumer_id=null,$current_package=null)
    {
		$where="";
		if($current_package!=null){
			$where=" and package_id = ".$current_package;
		}
    	$query = "delete from consumer_packages WHERE consumer_id = ".$consumer_id.$where;
    	$result = $this->_db_table->getAdapter()->query($query);
        if(count($result)==0) {
            return false;
        }
        else{
            return true;
        }
    }
    public function deleteConsumerPackById($conPack_id) 
	{
		 
		$where = "consumer_package_id = " . $conPack_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
    
	public function updatePackageId($consumer_id, $new_package,$current_package)
    {
		$query = "UPDATE consumer_packages SET package_id = $new_package WHERE consumer_id = $consumer_id and package_id = $current_package";
	 
		$result = $this->_db_table->getAdapter()->query($query);
        if(count($result)==0){
                return false;
        }
        else{
            return $result;
        }
    }
	
	 public function checkSchemePackageByConsumerID($consumer_id=null)
    {
    
    	$query="select *,s.package_id as package_value from consumer_scheme cs inner join consumers c on c.consumer_id=cs.consumer_id inner join schemes s on 
    			s.scheme_id=cs.consumer_scheme where s.scheme_status='enable' and s.scheme_type='equipment' and cs.equip_month_dis>0 and cs.consumer_id=".$consumer_id;
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if( count($result) == 0 ) {
    		return false;
    	}
      	return $result;
    }
	
	  public function addNewConsumersPackageForScheme(Application_Model_ConsumersPackage $ConsumersPackage)
    {
    	$data = array(
    			'consumer_id' => $ConsumersPackage->__get("consumer_id"),
    			'package_id' => $ConsumersPackage->__get("package_id"),
    	);
    	$result = $this->_db_table->insert($data);
    	if(count($result)==0){
    		return false;
    	}
    	else{
    		return $result;
    	} 
    } 
     public function getPackageChangeByConsumer($consumer_id)
    {
    	if(!empty($consumer_id)){
    		$query = "SELECT * FROM `change_package` WHERE `consumer_id` = " . $consumer_id;
    
    		$meterchanges = $this->_db_table->getAdapter()->query($query);
    		$result = $meterchanges->fetchAll();
    		 
    		if (count($result) == 0) {
    			return false;
    		}else
    		{
    			return $result;
    		}
    	}else{
    		return false;
    	}
    }   
	
	public function getPackageForIntegration($consumer_id){
    
    	 $query=" Select * from consumer_packages cp inner join packages p on cp.package_id=p.package_id where 
					cp.consumer_id=".$consumer_id." and is_postpaid in (1,2) limit 1";
		
    	$stmt= $this->_db_table->getAdapter()->query($query);
    
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    } 
}
