<?php

class Application_Model_ReceivedPaymentDetails
{
    private $rpd_id;
    private $cr_id;
    private $user_id;
    private $amount;
    private $note;
    private $timestamp;

    public function __construct($received_payment_row = null)
    {
        if( !is_null($received_payment_row) && $received_payment_row instanceof Zend_Db_Table_Row ) {
                $this->rpd_id = $received_payment_row->rpd_id;
                $this->cr_id = $received_payment_row->cr_id;
                $this->user_id = $received_payment_row->user_id;
                $this->amount = $received_payment_row->amount;
                $this->note = $received_payment_row->note;
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "package_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }

}

