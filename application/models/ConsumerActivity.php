<?php

class Application_Model_ConsumerActivity
{
	private $id;
    private $curr_package_id;
    private $new_package_id;
    private $site_id;
    private $consumer_id;
 	private $user_id;
 	private $user_type;
 	private $effective_date;
 	private $timestamp;
 	private $status;
 	private $verified_by;
 	private $verified_date;
 	private $remark;
 	private $instant;
 	private $activity;
 	private $closing_reading;
 	private $opening_reading;
	private $remark_approve;
 	 
    public function __construct($package_row = null)
    {
        if( !is_null($package_row) && $package_row instanceof Zend_Db_Table_Row ) {
        		$this->id = $package_row->id;
                $this->curr_package_id = $package_row->curr_package_id;
                $this->new_package_id = $package_row->new_package_id;
                $this->site_id=$package_row->site_id;
                $this->consumer_id = $package_row->consumer_id;
   				$this->user_id=$package_row->user_id;
   				$this->user_type=$package_row->user_type;
   				$this->timestamp=$package_row->timestamp;
   				$this->effective_date=$package_row->effective_date;
   				$this->status=$package_row->status;
   				$this->verified_by=$package_row->verified_by;
   				$this->verified_date=$package_row->verified_date;
   				$this->remark=$package_row->remark;
   				$this->instant=$package_row->instant;
   				$this->activity=$package_row->activity;
   				$this->closing_reading=$package_row->closing_reading;
   				$this->opening_reading=$package_row->opening_reading;
				$this->remark_approve=$package_row->remark_approve; 
        }
    }
	public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }

}

