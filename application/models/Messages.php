<?php

class Application_Model_Messages
{
	private $message_id;
	private $message_type;
	private $message_level;
	private $message_name;
	private $message;
	
	

	public function __construct($message_row = null)
	{
		if( !is_null($message_row) && $message_row instanceof Zend_Db_Table_Row ) {
			$this->message_id = $message_row->message_id;
			$this->message_type = $message_row->message_type;
			$this->message_level = $message_row->message_level;
			$this->message_name = $message_row->message_name;
			$this->message = $message_row->message;
		}
	}
	public function __set($name, $value)
	{
		 
		$this->$name = $value;

	}
	public function __get($name)
	{
		return $this->$name;
	}

}

