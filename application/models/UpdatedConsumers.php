<?php

class Application_Model_UpdatedConsumers
{
    private $updated_consumers_id;
    private $consumer_id;
    private $timestamp;
    private $previous_debit_value;
    private $current_debit_value;
    private $cr_id;
    private $previous_package_id;
    private $current_package_id;

    public function __construct($updated_consumer_row = null)
    {
        if( !is_null($updated_consumer_row) && $updated_consumer_row instanceof Zend_Db_Table_Row ) {
            
                $this->updated_consumers_id = $updated_consumer_row->updated_consumers_id;
                $this->consumer_id = $updated_consumer_row->consumer_id;
                $this->timestamp = $updated_consumer_row->timestamp;
                $this->previous_debit_value = $updated_consumer_row->previous_debit_value;
                $this->current_debit_value = $updated_consumer_row->current_debit_value;
                $this->cr_id = $updated_consumer_row->cr_id;
                $this->previous_package_id = $updated_consumer_row->previous_package_id;
                $this->current_package_id = $updated_consumer_row->current_package_id;
            
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}
