<?php
class Application_Model_PackageChangeMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_PackageChange();
    }
    
    public function addPackageChange(Application_Model_PackageChange $changePackage)
    {
    	 
    	$data = array(
    			'site_id'=> $changePackage->__get("site_id"),
    			'consumer_id'=> $changePackage->__get("consumer_id"),
    			'curr_package_id'=> $changePackage->__get("curr_package_id"),
    			'new_package_id'=> $changePackage->__get("new_package_id"),
    			'assign_package'=> $changePackage->__get("assign_package"),
    			'activity'=> $changePackage->__get("activity"),
    			'instant'=> $changePackage->__get("instant"),
    			'effective_date'=> $changePackage->__get("effective_date"),
    			'user_id'=> $changePackage->__get("user_id"),
    			'user_type'=> $changePackage->__get("user_type"),
    			'timestamp'=> $changePackage->__get("timestamp"),
    			'opening_reading'=> $changePackage->__get("opening_reading"),
    			'closing_reading'=> $changePackage->__get("closing_reading"),
    	);
    
    	$result = $this->_db_table->insert($data);
        
        if(count($result)==0) {
        	return false;
        } else {
    		return true;
    	}
    }
    
    public function getPackageChangeById($Pk_id)
    {
    
    	$result = $this->_db_table->find($Pk_id);
    
    	if( count($result) == 0 ) {
    		$CP = new Application_Model_PackageChange();
    		return $CP;
    	}
    	$row = $result->current();
    	$CP = new Application_Model_PackageChange($row);
    
    	return $CP;
    }
    
    public function getAllPackageChange($status=NULL,$role_sites_id=NULL)
    {
    	$where = array();
      	if($status!=NULL){
      		$where="  status='".$status."' and site_id in (".implode(",", $role_sites_id).")";
      	}else{
			$where=" site_id in (".implode(",", $role_sites_id).") and status!='disable'";
		}
    	$result = $this->_db_table->fetchAll($where,array('timestamp desc'));
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$CP_object_arr = array();
    	foreach ($result as $row)
    	{
    		$CP_object = new Application_Model_PackageChange($row);
    		array_push($CP_object_arr,$CP_object);
    	}
    	return $CP_object_arr;
    }
    
    public function updatePackageChange(Application_Model_PackageChange $changePackage)
    {
    	$data = array(
    			'consumer_id'=> $changePackage->__get("consumer_id"),
    			'curr_package_id'=> $changePackage->__get("curr_package_id"),
    			'new_package_id'=> $changePackage->__get("new_package_id"),
    			'effective_date'=> $changePackage->__get("effective_date"),
    			'user_id'=> $changePackage->__get("user_id"),
    			'user_type'=> $changePackage->__get("user_type"),
    			'timestamp'=> $changePackage->__get("timestamp"),
    	);
    		
    	$where = "id = " . $changePackage->__get("id");
    	$result = $this->_db_table->update($data,$where);
    	if($result==0) {
    		return false;
    	}
    	else {
    		return true;
    	}
    }
    
    public function deletePackageChangeById($id)
    {
    	$where = "id = " . $id;
    	$result = $this->_db_table->delete($where);
    	if(count($result)==0)
    	{
    		return false;
    	}
    	else
    	{
     		return true;
    	}
    }
    
    public function getpackageChangeFuture($timestamp=NULL)
    {
    	$query = " SELECT * From change_package where date(effective_date) >='".$date."' order by effective_date desc";
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if( count($result) == 0 ) {
    		return false;
    	}
    		
    	return $result[0];
    }
	  public function getPackageChangeByConsumer($consumer_id)
    {
    	if(!empty($consumer_id)){
    		$query = "SELECT * FROM `change_package` WHERE `consumer_id` = " . $consumer_id;
    
    		$meterchanges = $this->_db_table->getAdapter()->query($query);
    		$result = $meterchanges->fetchAll();
    		 
    		if (count($result) == 0) {
    			return false;
    		}else
    		{
    			return $result;
    		}
    	}else{
    		return false;
    	}
    }  
    public function updatePackageChangeById($id=NULL,$status=NULL,$remark=NULL,$timestamp=NULL,$user_id=NULL)
    {
    
    	  $query ="update change_package set status='".$status."',remark='".$remark."',
    			verified_date='".$timestamp."',verified_by=".$user_id." where id =".$id;
   
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    
    	if($result)
    	{
    		return true;
    	}
    }
	
	public function  getAllPackageActivityOfDate($timestamp=NULL){ 
    	
    	$query = "SELECT * FROM change_package WHERE date(effective_date)='".$timestamp."' and instant='No' and status not in ('disable','new')";
    	
    	$meterchanges = $this->_db_table->getAdapter()->query($query);
    	$result = $meterchanges->fetchAll();
    	 
    	if (count($result) == 0) {
    		return false;
    	} else {
    		return $result;
    	}
    }
	
	public function  getAllPackageActivityForVerification($package_id=NULL,$consumer_id=NULL,$timestamp=NULL,$activity=NULL){
    	 
		if($activity=='change'){
			$where=" and new_package_id=".$package_id." and consumer_id=".$consumer_id;
		}else{
			$where=" and assign_package=".$package_id." and consumer_id=".$consumer_id;
		}
		
        $query = "SELECT * FROM change_package WHERE date(verified_date)='".$timestamp."' and activity='".$activity."' and status not in ('disable','new') ".$where;
    	 
    	$meterchanges = $this->_db_table->getAdapter()->query($query);
    	$result = $meterchanges->fetchAll();
    	 
    	if (count($result) == 0) {
    		return false;
    	} else {
    		return $result[0];
    	}
    }
	
	public function  getAllPackageActivityOfConsumerInMonth($consumer_id=NULL,$month=NULL,$year=NULL){
    	 
    	$query = "SELECT * FROM change_package where consumer_id='".$consumer_id."' and  month(timestamp)='".$month."' and 
				year(timestamp)='".$year."' and status not in ('disable','new') and activity in ('change','new') order by timestamp asc";
    	
    	$meterchanges = $this->_db_table->getAdapter()->query($query);
    	$result = $meterchanges->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	} else {
    		return $result;
    	}
    }
    
}