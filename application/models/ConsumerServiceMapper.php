<?php
class Application_Model_ConsumerServiceMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
		$this->_db_table_service = new Application_Model_DbTable_ServiceRequest();
		$this->_db_table_complain = new Application_Model_DbTable_ComplainRemark();
    }
	
	public function addNewServiceRequest($service=NULL)
    {
        $data = array(
    		'service' => $service,
        );
		
        $result = $this->_db_table_service->insert($data);

        if(count($result)==0) {
			return false;
        } else {
            return $result;
        }
    }
	public function getAllServiceRequest()
    {
        $query = "SELECT * FROM `service_request`";
        $stmt= $this->_db_table_service->getAdapter()->query($query);
        $result = $stmt->fetchAll();

        if( count($result) == 0 ) {
                return false;
        }else{
            return $result;
        }
    } 
     
	public function addNewComplainRemark($remark=NULL,$service_id=NULL)
    {
        $data = array(
    		'remark' => $remark,
            'service_id' => $service_id,
        );
        $result = $this->_db_table_complain->insert($data);

        if(count($result)==0) {
			return false;
        } else {
            return $result;
        }
    }
	
	public function getAllComplainRemark($service_id=NULL)
    {
        $query = "SELECT * FROM `complain_remark` WHERE `service_id`= ".$service_id." ";
        $stmt= $this->_db_table_complain->getAdapter()->query($query);
        $result = $stmt->fetchAll();

        if( count($result) == 0 ) {
                return false;
        }else{
            return $result;
        }
    } 
 
   /* public function deleteQueryById($query_id)
    {
        $where =  "`id`=".$query_id." OR `parent_id`=".$query_id."";
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function updateQueryById($query_id, $query)
    {
        $where =  "`id`=".$query_id;
        $data = array('query' => $query );
        $result = $this->_db_table->update($data, $where);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }*/
}
