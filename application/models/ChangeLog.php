<?php
class Application_Model_ChangeLog
{
	private $change_log_id;
	private $changes;
	private $timestamp;


	public function __construct($change_row = null)
	{
		if( !is_null($change_row) && $change_row instanceof Zend_Db_Table_Row ) {

			$this->change_log_id = $change_row->change_log_id;
			$this->changes = $change_row->changes;
			$this->timestamp = $change_row->timestamp;

		}
	}
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}
}