
<?php
class Application_Model_ConsumerDisconnectionMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_ConsumerDisconnection();
		$this->_db_table_con = new Application_Model_DbTable_Consumers();
	}

	public function addConsumerDisconnection(Application_Model_ConsumerDisconnection $CD)
	{
		$data = array(
				'consumer_id' => $CD->__get("consumer_id"),
				'status' => $CD->__get("status"),
				'timestamp' => $CD->__get("timestamp"),
				'user_id' => $CD->__get("user_id"),
				'user_type' => $CD->__get("user_type")
				
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	public function updateConsumerTempStatus($status=NULL,$consumer_id=NULL)
	{
	
		$query ="update consumers set temp_dis=".$status." where consumer_id in (".implode(",", $consumer_id).")";
	
		$stmt = $this->_db_table_con->getAdapter()->query($query);
		$result=$stmt->execute();
	
		if($result)
		{
			return true;
		}
	}
	
	public function getConsumerTempConnection($site_id=NULL)
	{
	
		if($site_id!=NULL){
			$sites=" and site_id=".$site_id;
		}else{
			$sites="";
		}
		
		$query ="select * from consumers where consumer_status!='banned' ".$sites; 
	
		$stmt= $this->_db_table_con->getAdapter()->query($query);
    
    	$result = $stmt->fetchAll();
    
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
	}
	 
}