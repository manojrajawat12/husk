<?php
class Application_Model_Mppts
{
	private $id;
	private $site_id;
	private $mppt_name;
	private $mppt_keyword;
	private $description;
	private $timestamp;
	private $approve_date;
	private $approve_by;
	private $remark;
	private $status;
	private $mppt_no;
	
	public function __construct($change_row = null)
	{
		if( !is_null($change_row) && $change_row instanceof Zend_Db_Table_Row ) {

			$this->id = $change_row->id;
			$this->site_id = $change_row->site_id;
			$this->mppt_name = $change_row->mppt_name;
			$this->mppt_keyword = $change_row->mppt_keyword;
			$this->description = $change_row->description;
			$this->timestamp = $change_row->timestamp;
			$this->approve_date = $change_row->approve_date;
			$this->approve_by = $change_row->approve_by;
			$this->remark = $change_row->remark;
			$this->status = $change_row->status;
			$this->mppt_no = $change_row->mppt_no;
		}
	}
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}
}