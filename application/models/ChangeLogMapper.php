<?php
class Application_Model_ChangeLogMapper {

	protected $_db_table;

	public function __construct() {
		$this->_db_table = new Application_Model_DbTable_ChangeLog();
	}
	public function getAllChangeLogs($date=NULL) {
		$where=array();
		if($date !=null)
		{
			echo $before1Week=$date->sub(7, Zend_Date :: DAY)->toString("YYYY-MM-dd");
			$date = new Zend_Date();
			echo $date->toString("YYYY-MM-dd");
			$where=array('Date(timestamp) < ?'=> $date,
						 'Date(timestamp) >= ?'=> $before1Week
						);
		
		}
		$result = $this->_db_table->fetchAll($where, array('change_log_id DESC'));
	
		if (count($result) == 0) {
			return false;
		}
		$change_log_object_arr = array();
		foreach ($result as $row) {
			$change_log_object = new Application_Model_ChangeLog($row);
			array_push($change_log_object_arr, $change_log_object);
		}
	
		return $change_log_object_arr;
	}
	
}