<?php

class Application_Model_ConnectionRequest
{
	private $id;
    private $name;
    private $mob_no;
    private $ref_name;
    private $ref_no;
    private $state;
    private $site;
    private $status;
    private $track_status;
    private $aprrove_by;
    private $approve_date;
    private $entry_date;
    
    public function __construct($Forms_row = null)
    {
        if( !is_null($Forms_row) && $Forms_row instanceof Zend_Db_Table_Row ) {

                $this->id = $Forms_row->id;
                $this->name = $Forms_row->name;
                $this->mob_no = $Forms_row->mob_no;
                $this->ref_name = $Forms_row->ref_name;
                $this->ref_no = $Forms_row->ref_no;
                $this->state = $Forms_row->state;
                $this->site = $Forms_row->site;
                $this->status = $Forms_row->status;
                $this->track_status = $Forms_row->track_status;
                $this->aprrove_by = $Forms_row->aprrove_by;
                $this->approve_date = $Forms_row->approve_date;
                $this->entry_date = $Forms_row->entry_date;
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}
