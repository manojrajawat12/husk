<?php

class Application_Model_CollectionAgentsMapper {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Application_Model_DbTable_CollectionAgents();
    }

    public function addNewCollectionAgent(Application_Model_CollectionAgents $collectionagent) {
        $data = array(
            'agent_mobile' => $collectionagent->__get("agent_mobile"),
            'agent_fname' => $collectionagent->__get("agent_fname"),
            'agent_lname' => $collectionagent->__get("agent_lname"),
            'agent_email' => $collectionagent->__get("agent_email"),
             'username' => $collectionagent->__get("username"),
            'agent_status' => $collectionagent->__get("agent_status"),
            'site_id' => $collectionagent->__get("site_id"),
            'agent_balance' => $collectionagent->__get("agent_balance"),
            'password' => $collectionagent->__get("password"),
            'permission'=>$collectionagent->__get("permission"),
			'assigned_site' => $collectionagent->__get("assigned_site"),
			'line_manager_no' => $collectionagent->__get("line_manager_no") 
        );


        $result = $this->_db_table->insert($data);

        if ($result == 0) {
            return false;
        } else {
            return $result;
        }
    }

    public function getCollectionAgentByPhone($agent_mobile) {

        $where = array("agent_mobile = ?" => $agent_mobile);
        $result = $this->_db_table->fetchRow($where);
		 
        if (count($result) == 0) {
            return false;
        }
        $row = $result;
        $collectionagent = new Application_Model_CollectionAgents($row);
        return $collectionagent;
    }

    public function getCollectionAgentById($collection_agent_id) {
        $result = $this->_db_table->find($collection_agent_id);
        if (count($result) == 0) {
            $collectionAgent = new Application_Model_CollectionAgents();
            $collectionAgent->__set("agent_fname", "--DELETED--");
			$collectionAgent->__set("agent_lname", "");
            return $collectionAgent;
        }
        $row = $result->current();
        $collectionagent = new Application_Model_CollectionAgents($row);
        return $collectionagent;
    }

    public function getAllCollectionAgents() {
	
		$where = "agent_status !='Inactive'";
		 
        $result = $this->_db_table->fetchAll($where, array('agent_fname ASC')); 
        if (count($result) == 0) {
            return false;
        }
        $collectionagent_object_arr = array();
        foreach ($result as $row) {
            $collectionagent_object = new Application_Model_CollectionAgents($row);
            array_push($collectionagent_object_arr, $collectionagent_object);
        }
        return $collectionagent_object_arr;
    }
 
    public function getAllCollectionBySiteId($site_id) {
        $where = array(
            "site_id = ?" => $site_id,
        );
        $result = $this->_db_table->fetchAll($where);
        if (count($result) == 0) {
            return false;
        }
        $collectionagent_object_arr = array();
        foreach ($result as $row) {
            $collectionagent_object = new Application_Model_CollectionAgents($row);
            array_push($collectionagent_object_arr, $collectionagent_object);
        }
        return $collectionagent_object_arr;
    }

    public function getAllCollectionAgentsBySiteId($site_id) {
        $where = array(
            "site_id =?" => $site_id
        );
        $result = $this->_db_table->fetchAll($where);
        if (count($result) == 0) {
            return false;
        }
        $collectionagent_object_arr = array();
        foreach ($result as $row) {
            $collectionagent_object = new Application_Model_CollectionAgents($row);
            array_push($collectionagent_object_arr, $collectionagent_object);
        }
        return $collectionagent_object_arr;
    }

    public function updateCollectionAgent(Application_Model_CollectionAgents $collectionagent) {
        

	$password=$collectionagent->__get("password");
    	if($password!=NULL){
  	
        $data = array(
            'agent_mobile' => $collectionagent->__get("agent_mobile"),
            'agent_fname' => $collectionagent->__get("agent_fname"),
            'agent_lname' => $collectionagent->__get("agent_lname"),
            'agent_email' => $collectionagent->__get("agent_email"),
            'username' => $collectionagent->__get("username"),
            'agent_status' => $collectionagent->__get("agent_status"),
            'site_id' => $collectionagent->__get("site_id"),
            'agent_balance' => $collectionagent->__get("agent_balance"),
			'password' =>$password,
            'permission'=>$collectionagent->__get("permission"),
			'assigned_site'=>$collectionagent->__get("assigned_site"),
			'line_manager_no' => $collectionagent->__get("line_manager_no")
        );
    	}
    	else{
    		$data = array(
    				'agent_mobile' => $collectionagent->__get("agent_mobile"),
    				'agent_fname' => $collectionagent->__get("agent_fname"),
    				'agent_lname' => $collectionagent->__get("agent_lname"),
    				'agent_email' => $collectionagent->__get("agent_email"),
    				'username' => $collectionagent->__get("username"),
    				'agent_status' => $collectionagent->__get("agent_status"),
    				'site_id' => $collectionagent->__get("site_id"),
    				'agent_balance' => $collectionagent->__get("agent_balance"),
    				'permission'=>$collectionagent->__get("permission"),
					'assigned_site'=>$collectionagent->__get("assigned_site"),
					'line_manager_no' => $collectionagent->__get("line_manager_no")
    		);
    	} 
       $where = "collection_agent_id = " . $collectionagent->__get("collection_agent_id");
		
        $result = $this->_db_table->update($data, $where);
		
        if ($result== 0) {
            return false;
        } else {
            return true;
        }
    }

    public function deleteCollectionAgentById($collection_agent_id) {
        $where = "collection_agent_id = " . $collection_agent_id;
        $result = $this->_db_table->delete($where);
        if (count($result) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getCollectionAgentByTransactionId($transaction_id) {
    	$stmt = $this->_db_table->getAdapter();
    	$query = " SELECT agent.* FROM collection_agents agent INNER JOIN ca_transactions transaction 
 				  ON agent.collection_agent_id=transaction.collection_agent_id WHERE transaction.ca_transaction_id ='$transaction_id'";
     
    	$result = $stmt->fetchRow($query);
    	
       if (count($result) == 0) {
            $collectionAgent = new Application_Model_CollectionAgents();
            $collectionAgent->__set("agent_fname", "--DELETED--");
            return $collectionAgent;
        }
        $row = $result->current();
        $collectionagent = new Application_Model_CollectionAgents($row);
        return $collectionagent;
    }
    
    public function loginAgent($password,$agent_email)
	{
		$stmt = $this->_db_table->getAdapter();
    	$query = " SELECT * FROM collection_agents WHERE password ='$password' AND (agent_email='$agent_email' or username='$agent_email') and agent_status!='Inactive';";
      
    	$result = $stmt->fetchRow($query);
    	$collectionagent = $result; 
        
        return $collectionagent;
	}
	 
	public function getCollBySiteId($site_id=null) 
	{
		$query="SELECT * FROM collection_agents where FIND_IN_SET(".$site_id.", assigned_site) and agent_status!='Inactive'";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		return $result;
	}
	
	public function getAgentBySiteId($site_id=null) 
	{
		$query="SELECT * FROM collection_agents where FIND_IN_SET(".$site_id.", site_id) and agent_status!='Inactive'";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		return $result;
	}
	
}
