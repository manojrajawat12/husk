<?php

class Application_Model_ConsumerComplain
{
	private $id;
    private $consumer_id;
    private $service_id;
    private $remark_id;
    private $rating;
    private $state;
    private $site;
    private $status;
    private $aprrove_by;
	private $approve_type;
    private $approve_date;
    private $entry_date;
    
    public function __construct($Forms_row = null)
    {
        if( !is_null($Forms_row) && $Forms_row instanceof Zend_Db_Table_Row ) {

                $this->id = $Forms_row->id;
                $this->consumer_id = $Forms_row->consumer_id;
                $this->service_id = $Forms_row->service_id;
                $this->remark_id = $Forms_row->remark_id;
                $this->rating = $Forms_row->rating;
                $this->status = $Forms_row->status;
                $this->aprrove_by = $Forms_row->aprrove_by;
				$this->approve_type = $Forms_row->approve_type;
                $this->approve_date = $Forms_row->approve_date;
                $this->entry_date = $Forms_row->entry_date;
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}
