<?php

class Application_Model_CollectionAgents 
{
    private $collection_agent_id;
    private $agent_mobile;
    private $agent_fname;
    private $agent_lname;
    private $agent_email;
	private $username;
    private $site_id;
    private $agent_status;
    private $agent_balance;
 	private $password;
	private $permission;
	private $assigned_site;
	private $user_id; 
	private $line_manager_no; 
	 
	 
    public function __construct($catransactions_row= NULL)
    {
        if(!is_null($catransactions_row)) 
        {
                $this->collection_agent_id = $catransactions_row->collection_agent_id;
                $this->agent_mobile = $catransactions_row->agent_mobile;
                $this->agent_fname = $catransactions_row->agent_fname;
                $this->agent_lname = $catransactions_row->agent_lname;
                $this->agent_email = $catransactions_row->agent_email;
				$this->username = $catransactions_row->username;
                $this->site_id = $catransactions_row->site_id;
                $this->agent_status = $catransactions_row->agent_status;
                $this->agent_balance = $catransactions_row->agent_balance;
				$this->password = $catransactions_row->password;
				$this->permission = $catransactions_row->permission;
				$this->assigned_site = $catransactions_row->assigned_site;
				$this->user_id = $catransactions_row->user_id;
				$this->line_manager_no = $catransactions_row->line_manager_no;
        }
    }
    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name)
    {
    return $this->$name;
    }

}

