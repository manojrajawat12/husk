<?php

class Application_Model_ConsumerDisconnection
{
    private $id;
    private $consumer_id;
    private $status;
    private $timestamp;
    private $user_id;
    private $user_type;
 
    public function __construct($Forms_row = null)
    {
        if( !is_null($Forms_row) && $Forms_row instanceof Zend_Db_Table_Row ) {

                $this->id = $Forms_row->id;
                $this->consumer_id = $Forms_row->consumer_id;
                $this->status = $Forms_row->status;
                $this->timestamp = $Forms_row->timestamp;
                $this->user_id = $Forms_row->user_id;
                $this->user_type = $Forms_row->user_type;
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}
