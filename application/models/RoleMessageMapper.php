<?php
class Application_Model_RoleMessageMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_RoleMessage();
	}

	public function addRoleMessage(Application_Model_RoleMessage $roleMsg)
	{

		$data = array(
				'message_id' => $roleMsg->__get("message_id"),
				'role_id' => $roleMsg->__get("role_id"),
				'is_metered' => $roleMsg->__get("isMetered"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getRoleMessageById($role_id)
	{
		$query="SELECT * FROM role_message where role_id=".$role_id;
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	 
    	return $result;
	}
	
	public function getAllRoleMessage()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('role_message_id ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$role_site_object_arr = array();
		foreach ($result as $row)
		{
			$role_site_object = new Application_Model_RoleMessage($row);
			array_push($role_site_object_arr,$role_site_object);
		}
		return $role_site_object_arr;
	}
	
	public function updateRoleMsg(Application_Model_RoleMessage $roleMsg)
	{
		$data = array(
				'is_metered' => $roleMsg->__get("is_metered"),
		);
		$where = "role_id = " . $roleMsg->__get("role_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	public function deleteRoleMessageById($role_id)
	{
		 
		$where = "role_id = " . $role_id;
		 
		
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public function deleteRoleMessageByMessageTypeId($role_id,$messageType=NULL)
	{
			
	    if($messageType==NULL){
		$where = "role_id = " . $role_id;
		}
		else{
			$where = " message_id not in (".implode(",", $messageType).")  and role_id = ".$role_id;
		}
		
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function checkRoleMessageById($role_id,$message_id)
	{
		$query="SELECT * from role_message where role_id=".$role_id." and message_id =".$message_id;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		$count=count($result);
		 
		if( $count == 0 ) {
			return $count;
		}
		else
		{
			return $count;
		}
	}
	public function checkRoleSiteById($role_id,$site_id)
	{
	$query="SELECT * from role_sites where role_id=".$role_id." and site_id =".$site_id;
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	$count=count($result);
    	
    	if( $count == 0 ) {
    		return $count;
    	}
		else
		{
			return $count;
		}
	}
	
	public function getAllRoleIdByMetered()
	{
		$query="select * from role_message where is_metered=true";
		
		$stmt= $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if (count($result) == 0) {
			return false;
		}
		return $result;
	}
	
 	public function getRoleMessageByRole($role_id,$type)
	{
		$query="SELECT * FROM role_message  rm  join messages m on rm.message_id=m.message_id
				where rm.role_id=$role_id and m.message_type='$type'" ;
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	//$count=count($result);
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
		else
		{
			return $result;
		}
	}


}