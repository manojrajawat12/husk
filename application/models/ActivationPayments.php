<?php

class Application_Model_ActivationPayments
{
    private $ap_id;
    private $consumer_id;
    private $user_id;
    private $amount;
    private $timestamp;

    public function __construct($ap_row = null)
    {
        if( !is_null($ap_row) && $ap_row instanceof Zend_Db_Table_Row ) {
                $this->ap_id = $ap_row->ap_id;
                $this->consumer_id = $ap_row->consumer_id;
                $this->user_id = $ap_row->user_id;
                $this->amount = $ap_row->amount;
                $this->timestamp = $ap_row->timestamp;
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "ap_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

