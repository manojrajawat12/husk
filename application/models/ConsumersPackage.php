<?php

class Application_Model_ConsumersPackage
{ 
    private $consumer_package_id;
    private $consumer_id;
    private $package_id;
	private $serial;
	
    

    public function __construct($CP_row = null)
    {
        if( !is_null($CP_row) && $CP_row instanceof Zend_Db_Table_Row ) {
            
                $this->consumer_package_id = $CP_row->consumer_package_id;
                $this->consumer_id = $CP_row->consumer_id;
                $this->package_id = $CP_row->package_id;
				$this->serial = $CP_row->serial;
        }
    }
 public function __set($name, $value)
    {
           
         $this->$name = $value;
            
    }
    public function __get($name)
    {
            return $this->$name;
    }
}

