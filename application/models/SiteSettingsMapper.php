<?php

class Application_Model_SiteSettingsMapper
{
    protected $_db_table;

    public function __construct()
    {
        $this->_db_table = new Application_Model_DbTable_SiteSettings();
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * @param Application_Model_SiteSettings $obj
     * @return bool|mixed
     */
    public function addNewSiteSetting(Application_Model_SiteSettings $obj)
    {
        $data = array(
            "time_slot" => $obj->__get("time_slot"),
            "site_id" => $obj->__get("site_id"),
            "brp" => $obj->__get("brp"),
            "hsf" => $obj->__get("hsf"),
//            "x" => $obj->__get("x"),
//            "y" => $obj->__get("y"),
        );

        $result = $this->_db_table->insert($data);
        if ($result == 0) {
            return false;
        } else {
            return $result;
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * @param bool $indexByTimeSlot
     * @return array|bool
     */
    public function getAllSiteSettings($indexByTimeSlot = false)
    {
        $result = $this->_db_table->fetchAll(null, array("id DESC"));
        if (count($result) == 0) {
            return false;
        }
        $requested_object_arr = array();
        foreach ($result as $row) {
            $requested_object = new Application_Model_SiteSettings($row);
            
            if($indexByTimeSlot){
                $requested_object_arr[$requested_object->__get('time_slot')] = $requested_object;
            }else{
                array_push($requested_object_arr, $requested_object);
            }

        }
        return $requested_object_arr;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * @param bool $indexByTimeSlot
     * @return array|bool
     */
    public function getAllSiteSettingsBySiteId($site_id ,$indexByTimeSlot = false)
    {
        $where = array(
            "site_id = ?" => $site_id,
        );
        $result = $this->_db_table->fetchAll($where, array("id DESC"));
        if (count($result) == 0) {
            return false;
        }
        $requested_object_arr = array();
        foreach ($result as $row) {
            $requested_object = new Application_Model_SiteSettings($row);

            if($indexByTimeSlot){
                $requested_object_arr[$requested_object->__get('time_slot')] = $requested_object;
            }else{
                array_push($requested_object_arr, $requested_object);
            }

        }
        return $requested_object_arr;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * @param $id
     * @return int
     */
    public function deleteSiteSettingById($id)
    {
        $where = array(
            "id = ?" => $id
        );
        $result = $this->_db_table->delete($where);
        return $result;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * @param Application_Model_SiteSettings $obj
     * @return int
     */
    public function updateSiteSettingById(Application_Model_SiteSettings $obj)
    {
        $query = "update site_settings 
                  set time_slot = ? , 
                  site_id =  ? , 
                  brp = ? , 
                  hsf = ?  
                  where id = ?";

        $stmt = $this->_db_table->getAdapter()->query($query,[
            $obj->__get('time_slot'),
            $obj->__get("site_id"),
            $obj->__get("brp"),
            $obj->__get("hsf"),
            $obj->__get("id"),
        ]);
        $result=$stmt->execute();

        return $result;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * @param $id
     * @return Application_Model_SiteSettings|bool
     */
    public function getSiteSettingById($id)
    {
        $where = array(
            "id = ?" => $id
        );
        $result = $this->_db_table->fetchRow($where);
        if (count($result) == 0) {
            return false;
        }
        $row_obj = new Application_Model_SiteSettings($result);
        return $row_obj;
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * @param $id
     * @return Application_Model_SiteSettings|bool
     */
    public function getSiteSettingBySiteIdAndTimeSlot($site_id,$time_slot)
    {
        $where = array(
            "time_slot = ?" => $time_slot,
            "site_id = ?" => $site_id
        );
        $result = $this->_db_table->fetchRow($where);
        if (count($result) == 0) {
            return false;
        }
        $row_obj = new Application_Model_SiteSettings($result);
        return $row_obj;
    }


}
