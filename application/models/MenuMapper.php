<?php
class Application_Model_MenuMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Menu();
	}

	public function addNewMenu(Application_Model_Menu $menus)
	{

		$data = array(
				'menu_desc' => $menus->__get("menu_desc"),
				'parent_menu' => $menus->__get("parent_menu"),
				'menu_type' => $menus->__get("menu_type"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getMenuById($menu_id)
	{
		$result = $this->_db_table->find($menu_id);
		if( count($result) == 0 ) {
			$menu = new Application_Model_Menu();
			return $menu;
		}
		$row = $result->current();
		$menu = new Application_Model_Menu($row);
		return $menu;
	}
	
	public function getAllMenu()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('menu_id ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$menu_object_arr = array();
		foreach ($result as $row)
		{
			$menu_object = new Application_Model_Menu($row);
			array_push($menu_object_arr,$menu_object);
		}
		return $menu_object_arr;
	}
	
	public function updateMenu(Application_Model_Menu $menu)
	{
		$data = array(
				'menu_desc' => $menus->__get("menu_desc"),
				'parent_menu' => $menus->__get("parent_menu"),
				'menu_type' => $menus->__get("menu_type"),
		);
		$where = "menu_id = " . $menu->__get("menu_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	public function deleteMenuById($menu_id)
	{
		$where = "menu_id = " . $menu_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public function getAllMenuByType($menu_type=NULL)
	{
		if($menu_type=='menu')
		{
		$query = " SELECT * From menu where menu_type ='$menu_type'";
		}
		if($menu_type=='sub_menu')
		{
			$query = " SELECT * From menu where menu_type ='sub_menu' or menu_type ='page_item'";
		}
		$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	//print_r($result);
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$menu_object_arr = array();
    	foreach ($result as $row)
    	{
    		$menu_object = new Application_Model_Menu($row);
    		$menu_object->__set("menu_id", $row["menu_id"]);
    		$menu_object->__set("menu_desc", $row["menu_desc"]);
    		$menu_object->__set("menu_type", $row["menu_type"]);
    		$menu_object->__set("parent_menu", $row["parent_menu"]);
    		array_push($menu_object_arr,$menu_object);
    	}
    	return $menu_object_arr;
	}
}