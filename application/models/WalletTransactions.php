<?php

/**
 * Created by PhpStorm.
 * User: Amit Bhardwaj
 * Date: 09-02-2017
 * Time: 12:31
 */
class Application_Model_WalletTransactions
{
    private $id;
    private $consumer_id;
    private $user_id;
    private $receipt_number;
    private $transaction_id;
    private $entry_type;
    private $amount;
    private $timestamp;
    private $balance;
    private $comment;

    public function __construct($wallet_transactions_row = NULL)
    {
        if (!is_null($wallet_transactions_row)  && $wallet_transactions_row instanceof Zend_Db_Table_Row ) {
            $this->id = $wallet_transactions_row->id;
            $this->consumer_id = $wallet_transactions_row->consumer_id;
            $this->user_id = $wallet_transactions_row->user_id;
            $this->receipt_number = $wallet_transactions_row->receipt_number;
            $this->transaction_id = $wallet_transactions_row->transaction_id;
            $this->entry_type = $wallet_transactions_row->entry_type;
            $this->amount = $wallet_transactions_row->amount;
            $this->timestamp = $wallet_transactions_row->timestamp;
            $this->balance = $wallet_transactions_row->balance;
            $this->comment = $wallet_transactions_row->comment;

        }
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}

?>