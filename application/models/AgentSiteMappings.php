<?php

class Application_Model_AgentSiteMappings
{
    private $agent_site_mapping_id;
    private $collection_agent_id;
    private $site_id;
    private $timestamp;
    

    public function __construct($agent_site_mapping_row = null)
    {
        if( !is_null($agent_site_mapping_row) && $agent_site_mapping_row instanceof Zend_Db_Table_Row ) {
                $this->agent_site_mapping_id = $agent_site_mapping_row->agent_site_mapping_id;
                $this->collection_agent_id = $agent_site_mapping_row->collection_agent_id;
                $this->site_id = $agent_site_mapping_row->site_id;
                $this->timestamp = $agent_site_mapping_row->timestamp;
                
        }
    }
    public function __set($name, $value)
    {
            
                            $this->$name = $value;
                  

    }
    public function __get($name)
    {
            return $this->$name;
    }

}

