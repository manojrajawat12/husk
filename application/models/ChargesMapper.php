<?php
class Application_Model_ChargesMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Charges();
	}

	public function addNewCharges(Application_Model_Charges $ChargesData)
	{

		$data = array(
		    'charge_name' => $ChargesData->__get("charge_name"),
		    'charge_parameter' => $ChargesData->__get("charge_parameter"),
		    'hindi_text' => $ChargesData->__get("hindi_text"),
		    'charge_code' => $ChargesData->__get("charge_code"),
		    				
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	

	public function getAllCharges()
	{
	    $where = array();
	    
		$result = $this->_db_table->fetchAll($where,array('charge_id'));
		if( count($result) == 0 ) {
			return false;
		}
		$Charges_object_arr = array();
		foreach ($result as $row)
		{
		    $Charges_object = new Application_Model_Charges($row);
		    array_push($Charges_object_arr,$Charges_object);
		}
		return $Charges_object_arr;
	}
	
	public function updateCharges(Application_Model_Charges $ChargesData)
	{
		$data = array(
		    'charge_name' => $ChargesData->__get("charge_name"),
		    'charge_parameter' => $ChargesData->__get("charge_parameter"),
		    'hindi_text' => $ChargesData->__get("hindi_text"),
		    'charge_code' => $ChargesData->__get("charge_code"),
				
		);
		$where = "Charge_id = " . $ChargesData->__get("charge_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function deleteChargesById($charge_id)
	{
	    $where = "Charge_id = " . $charge_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
	
			return true;
		}
	}
	
	
	

}