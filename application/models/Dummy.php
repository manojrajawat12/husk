<?php

class Application_Model_Dummy
{ 
    private $id;
    private $connection_id;
    private $type;
    private $amount;
    private $timestamp;
	
    public function __construct($AppDetail_row = null)
    {
        if( !is_null($AppDetail_row) && $AppDetail_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $AppDetail_row->id;
                $this->connection_id = $AppDetail_row->connection_id;
                $this->type = $AppDetail_row->type;
                $this->amount = $AppDetail_row->amount;
				$this->timestamp = $AppDetail_row->timestamp;
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

