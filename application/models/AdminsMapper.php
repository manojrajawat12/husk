<?php
class Application_Model_AdminsMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Admins();
    }

    public function addNewAdmin(Application_Model_Admins $admin)
    {
        $data = array(
	'username'=> $admin->__get("username"),
	'hashed_password'=> $admin->__get("hashed_password"),
	'admin_fname'=> $admin->__get("admin_fname"),
	'admin_lname'=> $admin->__get("admin_lname"),
	'admin_email'=> $admin->__get("admin_email"),
        'profile_image'=> $admin->__get("profile_image"),
	'admin_role'=> $admin->__get("admin_role"),
        'reset_code'=> $admin->__get("reset_code"),
	);
        $result = $this->_db_table->insert($data);
        if($result==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getAdminById($admin_id)
    {
        $result = $this->_db_table->find($admin_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $admin = new Application_Model_Admins($row);
        return $admin;
    }
    public function getAllAdmins()
    {
        $result = $this->_db_table->fetchAll(null,array('admin_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $admin_object_arr = array();
        foreach ($result as $row)
        {
                $admin_object = new Application_Model_Admins($row);
                array_push($admin_object_arr,$admin_object);
        }
        return $admin_object_arr;
    }
    public function updateAdmin(Application_Model_Admins $admin)
    {
        $data = array(
	'username'=> $admin->__get("username"),
	'hashed_password'=> $admin->__get("hashed_password"),
	'admin_fname'=> $admin->__get("admin_fname"),
	'admin_lname'=> $admin->__get("admin_lname"),
	'admin_email'=> $admin->__get("admin_email"),
        'profile_image'=> $admin->__get("profile_image"),
	'admin_role'=> $admin->__get("admin_role"),
        'reset_code'=> $admin->__get("reset_code"),
	);
        $where = "admin_id = ". $admin->__get("admin_id");
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteAdminById($admin_id)
    {
        $where = "admin_id = ". $admin_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
    public function getAdminByEmail($email)
    {
            $where = array('admin_email = ?'=>$email);
            $result = $this->_db_table->fetchRow($where);
            if( count($result) == 0 ) {
                    return false;
            }
            
            $admin_object = new Application_Model_Admins($result);
            return $admin_object;
    }
    
    public function getAdminByResetCode($code)
    {
            $where = array('reset_code = ?'=>$code);
            $result = $this->_db_table->fetchRow($where);
            if( count($result) == 0 ) {
                    return false;
            }
            
            $admin_object = new Application_Model_Admins($result);
            return $admin_object;
    }
    public function checkUsernameAvailability($username)
    {
            $where = array('username = ?'=>$username);
            $result = $this->_db_table->fetchRow($where);
            if( count($result) == 0 ) {
                    return true;
            }
            else
            {
                return false;
            }
    }
    public function checkEmailAvailability($email)
    {
            $where = array('admin_email = ?'=>$email);
            $result = $this->_db_table->fetchRow($where);
            if( count($result) == 0 ) {
                    return true;
            }
            else
            {
                return false;
            }
    }
        
}
