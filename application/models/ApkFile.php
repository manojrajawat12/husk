<?php

class Application_Model_ApkFile
{
    private $apk_id;
    private $apk_name;
    
    

    public function __construct($gateway_row = null)
    {
        if( !is_null($gateway_row) && $gateway_row instanceof Zend_Db_Table_Row ) {
            
                $this->apk_id = $gateway_row->apk_id;
                $this->apk_name = $gateway_row->apk_name;
                 
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }
}

