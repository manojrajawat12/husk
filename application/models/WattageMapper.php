<?php
class Application_Model_WattageMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Wattage();
		
	}

	public function addNewWattage(Application_Model_Wattage $wattage)
	{

		$data = array(
				'wattage' => $wattage->__get("wattage"),
				'site_id' => $wattage->__get("site_id"),
		);
		 
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	
	
	public function getWattageById($site_id=null,$site=null,$watt=null)
	{
		$where="";
		if($site_id!=null){
			$where=" where site_id=".$site_id;
		}else{
			$where=null;
		}
		if($site!=null){
			$where.="where site_id!=".$site." and wattage not in (".implode(",", $watt).") group by wattage";
		}
	    $query = "select * from wattage ".$where;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		 
		return $result;
	}
	
	public function getWattageBywattId($watt_id)
	{
		$result = $this->_db_table->find($watt_id);
			
		if( count($result) == 0 ) {
			$wattage = new Application_Model_Wattage();
			return $wattage;
		}
		$row = $result->current();
		$wattage = new Application_Model_Wattage($row);
		return $wattage;
		
	}		
		
	
	public function checkWattageBySiteId($site_id=null,$wattage=null)
	{
		$query = "select * from wattage where site_id=".$site_id." and wattage=".$wattage; 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if(count($result)==0){
			return false;
		}else{
			return $result;
		}	
		
	}
	
	public function deleteWattageBySiteId($site_id=null,$watt=null)
	{
	   // echo $query = "delete from wattage where site_id=".$site_id." and wattage not in (".implode(",", $watt).")";
	     
		$where = "site_id=".$site_id." and wattage not in (".implode(",", $watt).")";
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	} 
	
	public function checkWattageById($wattage_id=null)
	{
		$query = "select * from wattage where watt_id  in (".implode(",", $wattage_id).")"; 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if(count($result)==0){
			return false;
		}else{
			return $result;
		}	
		
	}
	public function getWattageByMultipleId($site_id=null)
	{
		$query = "select * from wattage w where w.site_id  in (".implode(",", $site_id).") group by w.wattage";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if(count($result)==0){
			return false;
		}else{
			return $result;
		}
	
	}
}