<?php
class Application_Model_ReportPermissionMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_ReportPermission();
            
    }
    

    public function addReportPermission($report_name=null,$user_id=null)
    {
    	
    	$data= array(
	    	'report_name' => $report_name,
	    	'user_id' => $user_id,
    	);
    	
    	$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}


	public function updateReportPermission(Application_Model_ReportPermission $reports)
	{
		
		$data = array(
				'report_name' => $reports->__get("report_name"),
				'user_id' => implode(",", $reports->__get("user_id")),
		);
		
		$where = "id = " . $reports->__get("id");
		
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	public function deleteReportById($id)
	{
		
		$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}


	public function getAllReports($where=NULL)
	{
		$where = array();
	
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('id desc'));
		if( count($result) == 0 ) {
			return false;
		}
		$report_object_arr = array();
		foreach ($result as $row)
		{
			$report_object = new Application_Model_ReportPermission($row);
			array_push($report_object_arr,$report_object);
		}
		return $report_object_arr;
	}

	
	public function getReportsByReportName($Report=NULL)
	{
		 
		$query = "SELECT * FROM report_permission where report_name='".$Report."'";
		$stmt =   $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
	
		if( count($result) == 0 ) {
			return false;
		}
		return $result[0];
	
	}



}