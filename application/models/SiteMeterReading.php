<?php
class Application_Model_SiteMeterReading
{
	private $id;
	private $meter_id;
	private $site_id;
	private $reading_date;
	private $reading;
	private $entry_by;
	private $entry_type;
	private $meter_no;
	private $status;
	private $actual_reading;

 
	public function __construct($change_row = null)
	{
		if( !is_null($change_row) && $change_row instanceof Zend_Db_Table_Row ) {

			$this->id = $change_row->id;
			$this->meter_id = $change_row->meter_id;
			$this->site_id = $change_row->site_id;
			$this->reading_date = $change_row->reading_date;
			$this->reading = $change_row->reading;
			$this->entry_by = $change_row->entry_by; 
			$this->entry_type = $change_row->entry_type;
			$this->meter_no = $change_row->meter_no;
			$this->status = $change_row->status;
			$this->actual_reading = $change_row->actual_reading;
    
		}
	}
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}
}