<?php
class Application_Model_OpexMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Opex();
	}

	public function getAllOpex()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('types_of_cost ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$opex_object_arr = array();
		foreach ($result as $row)
		{
			$opex_object = new Application_Model_Opex($row);
			array_push($opex_object_arr,$opex_object);
		}
		return $opex_object_arr;
	}
/*----------17 January2018 (start)--------------*/
	public function getAllOpexByParentId($id)
	{
		$where = array();
        $where[] = "parent_id=".$id;
		
		if(count($where)==0)
		{
			$where = null;
		}
		
		$result = $this->_db_table->fetchAll($where,array('types_of_cost ASC'));
		
		if( count($result) == 0 ) {
			return false;
		}
		$opex_object_arr = array();
		foreach ($result as $row)
		{
			$opex_object = new Application_Model_Opex($row);
			array_push($opex_object_arr,$opex_object);
		}
		return $opex_object_arr;
	}

	public function getAllOpexByMenuType($menu)
	{
		$where = array();
        $where[] = "menu_type='".$menu."'";
		
		if(count($where)==0)
		{
			$where = null;
		}
		
		$result = $this->_db_table->fetchAll($where,array('types_of_cost ASC'));
		
		if( count($result) == 0 ) {
			return false;
		}
		$opex_object_arr = array();
		foreach ($result as $row)
		{
			$opex_object = new Application_Model_Opex($row);
			array_push($opex_object_arr,$opex_object);
		}
		return $opex_object_arr;
	}
/*----------17January 2018(end)---------------------*/
	
	public function getOpexData($opex_id=NULL)   
	{
		$query ="select * from opex where parent_id in (select id from opex where parent_id=".$opex_id.")";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
	
		if(count($where)==0){
			$where = null;
		}
		else{
			return $result;
		}
	}
	
	public function getAllParentOpex()
	{
		$where = "`parent_id` = 0";
	
		$result = $this->_db_table->fetchAll($where,array('id DESC'));
		if( count($result) == 0 ) {
			return false;
		}
		$opex_object_arr = array();
		foreach ($result as $row)
		{
			$opex_object = new Application_Model_Opex($row);
			array_push($opex_object_arr,$opex_object);
		}
		return $opex_object_arr;
	}

	public function addNewOpex(Application_Model_Opex $opex)
	{
		$data = array(
				'types_of_cost' => $opex->__get("types_of_cost"),
				'parent_id' => $opex->__get("parent_id"),
				'menu' => $opex->__get("menu"),
		);

		$result = $this->_db_table->insert($data);

		if(count($result)==0) {
			return false;
		} else {
			return $result;
		}
	}

	public function deleteCostById($id)
	{
		$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
	
			return true;
		}
	}

	public function updateCost(Application_Model_Opex $opexes)
    {
        $data = array(
                'types_of_cost' => $opexes->__get("types_of_cost"),
        );
        $where = "id = " . $opexes->__get("id");
        $result = $this->_db_table->update($data,$where);
        
        if($result==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}