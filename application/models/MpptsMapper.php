<?php
class Application_Model_MpptsMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Mppts();
    }

    public function addNewMppts(Application_Model_Mppts $mppts)
    {
        $data = array(
			'site_id' => $mppts->__get("site_id"),
			'mppt_name' => $mppts->__get("mppt_name"),
			'mppt_keyword' => $mppts->__get("mppt_keyword"),
        	'description' => $mppts->__get("description"),
			'status' => "new",
        );
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return $result;
        }
    }
    public function getMpptsById($id)
    {
    	 
    	$result = $this->_db_table->find($id);
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$row = $result->current();
    	$Mppts = new Application_Model_Mppts($row);
    	return $Mppts;
    }
    public function getMpptsBySiteId($site_id)
    {
        $query = " SELECT * From mppts  where site_id ='$site_id' and status='enable' order by mppt_no asc"; 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$SiteMaster_object_arr = array();
    	foreach ($result as $row)
    	{
    		$SiteMaster_object = new Application_Model_Mppts($row);
    		$SiteMaster_object->__set("id", $row["id"]);
    		$SiteMaster_object->__set("mppt_name", $row["mppt_name"]);
    		$SiteMaster_object->__set("mppt_keyword", $row["mppt_keyword"]);
    		$SiteMaster_object->__set("description", $row["description"]);
    		array_push($SiteMaster_object_arr,$SiteMaster_object);
    	}
    	return $SiteMaster_object_arr;
    }
     public function getAllMppts($status=NULL)
    {
    	$stat="";
    	if($status!=NULL){
    		$stat=" and mppts.status='".$status."'";
    	}else{
    		$stat=" and mppts.status='enable'";
    	}
    	
        $query = " SELECT * From mppts inner join sites on mppts.site_id=sites.site_id where sites.site_status!='Disable' ".$stat." order by mppt_no asc";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$SiteMaster_object_arr = array();
    	foreach ($result as $row)
    	{
    		$SiteMaster_object = new Application_Model_Mppts($row);
    		$SiteMaster_object->__set("id", $row["id"]);
    		$SiteMaster_object->__set("site_id", $row["site_id"]);
    		$SiteMaster_object->__set("mppt_name", $row["mppt_name"]);
    		$SiteMaster_object->__set("mppt_keyword", $row["mppt_keyword"]);
    		$SiteMaster_object->__set("description", $row["description"]);
    		array_push($SiteMaster_object_arr,$SiteMaster_object);
    	}
    	return $SiteMaster_object_arr;
    }
    public function updateMppts(Application_Model_Mppts $mppts)
    {
       
        $site_id= $mppts->__get("site_id");
        $id=$mppts->__get("id");
        $meter_name=$mppts->__get("mppt_name");
        $meter_keyword=$mppts->__get("mppt_keyword");
        $description=$mppts->__get("description");
        
        
        $query ="update mppts set site_id=".$site_id.",mppt_name='".$meter_name."',mppt_keyword='".$meter_keyword."' ,description='".$description."' where id=".$id;

        $stmt = $this->_db_table->getAdapter()->query($query);
        $result=$stmt->execute();
        
        if($result)
        {
        	return true;
        }
    }
    
  
   public function  deleteMpptsBySiteId($site_id)
   {
   		$query ="delete from mppts where site_id=".$site_id;

        $stmt = $this->_db_table->getAdapter()->query($query);
        $result=$stmt->execute();
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
   }
   public function  deleteMpptsById($id)
   {
   	$query ="delete from mppts where id=".$id;
   
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result=$stmt->execute();
   	if($result==0)
   	{
   		return false;
   	}
   	else
   	{
   		return true;
   	}
   }
   
   public function getMpptsReadingByMeterId($meter_id)
   {
   	$query = " SELECT * From mppts_reading where meter_id ='$meter_id' order by reading desc limit 1";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
   	$SiteMaster_object_arr = array();
   	foreach ($result as $row)
   	{
   		$SiteMaster_object = new Application_Model_MpptsReading($row);
   		$SiteMaster_object->__set("id", $row["id"]);
   		$SiteMaster_object->__set("mppt_id", $row["mppt_id"]);
   		$SiteMaster_object->__set("site_id", $row["site_id"]);
   		$SiteMaster_object->__set("reading_date", $row["reading_date"]);
   		$SiteMaster_object->__set("reading", $row["reading"]);
   		array_push($SiteMaster_object_arr,$SiteMaster_object);
   	}
   	return $SiteMaster_object_arr;
   }

public function getMpptsByMPPTsname($mppt_keyword)
   {
   	$query = " SELECT * From mppts where mppt_keyword ='".$mppt_keyword."' order by mppt_no asc";
	
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}    
   	return $result[0];
   }
   
   public function updateMpptsById($id=NULL,$status=NULL,$remark=NULL,$approve_by=NULL){
   	 
		$where="";
		if($status!="enable"){
			$where="id=".$id;
		}else{
			$where="id in (".$id.")";
		}
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		 
		$query ="update mppts set status='".$status."',remark='".$remark."',approve_date='".$timestamp."',approve_by=".$approve_by."   where ".$where;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		 
		if($result)
		{
			return true;
		}
   }
   
   public function getMasterMeterReadingByFeederType($date=NULL,$site_id=NULL)
   {
   
   	$query =  "SELECT
   					(select sum(mr.reading) FROM mppts m inner join mppts_reading mr on mr.mppt_id=m.id
						where m.mppt_name like '%MPPT%' and m.site_id=".$site_id." and
						date(mr.reading_date)='".$date."') as Solar,
			
					(select sum(reading) FROM mppts m inner join mppts_reading mr on mr.mppt_id=m.id
						where m.mppt_name like '%BIOGAS%' and m.site_id=".$site_id." and
						date(mr.reading_date)='".$date."') as BIOGAS,
			
					(select sum(reading) FROM mppts m inner join mppts_reading mr on mr.mppt_id=m.id
						where m.mppt_name like '%DG%'  and m.site_id=".$site_id." and
						date(mr.reading_date)='".$date."') as DG,

					(select sum(unit) from dg where  site_id=".$site_id." and
						date(date_of_purchase)='".$date."') as DG_page,

					(select count(site_status_id) from site_status where  status='INACTIVE' and site_id=".$site_id." and
						date(timestamp)='".$date."') as site_status 
   
			 from mppts limit 1";
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result = $stmt->fetchAll();
   	 
   	if( count($result) == 0 ) {
   		return false;
   	}
   	return $result[0];
   }
   
}

