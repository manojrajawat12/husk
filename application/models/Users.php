<?php

class Application_Model_Users
{
    private $user_id;
    private $username;
    private $hashed_password;
    private $user_fname;
    private $user_lname;
    private $user_email;
    private $profile_image;
    private $phone;
    private $reset_code;
    private $user_role;
    private $state_id;
	private $agent_id;

    public function __construct($user_row = null)
    {
        if( !is_null($user_row) && $user_row instanceof Zend_Db_Table_Row ) {
                $this->user_id = $user_row->user_id;
                $this->username = $user_row->username;
                $this->hashed_password = $user_row->hashed_password;
                $this->user_fname = $user_row->user_fname;
                $this->user_lname = $user_row->user_lname;
                $this->user_email = $user_row->user_email;
                $this->profile_image = $user_row->profile_image;
                $this->phone = $user_row->phone;
                $this->reset_code = $user_row->reset_code;
                $this->user_role= $user_row->user_role;
                 $this->state_id= $user_row->state_id;
				  $this->agent_id= $user_row->agent_id; 
        }
    }
    
    public function __set($name, $value)
    {
          
        $this->$name = $value;

    }
    public function __get($name)
    {
            return $this->$name;
    }

}

