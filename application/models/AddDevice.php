<?php

class Application_Model_AddDevice
{
    private $device_id;
    private $device_name;
    private $consumer_id;
    private $sequence;
    private $wattage;
    private $timestamp;

    public function __construct($device_row = null)
    {
        if( !is_null($device_row) && $device_row instanceof Zend_Db_Table_Row ) {
            
                $this->device_id = $device_row->device_id;
                $this->device_name = $device_row->device_name;
                $this->consumer_id = $device_row->consumer_id;
                $this->node = $device_row->node;
                $this->wattage = $device_row->wattage;
                $this->timestamp = $device_row->timestamp;
                
        }
    }
 public function __set($name, $value)
    {
           
        $this->$name = $value;
               

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

