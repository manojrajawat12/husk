<?php
class Application_Model_ReceivedPaymentDetailsMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_ReceviedPaymentDetails();
    }

    public function addNewRecevied_Payment_Detail(Application_Model_ReceivedPaymentDetails $received_payment_details)
    {
        $data = array(
	'cr_id' => $received_payment_details->__get("cr_id"),
	'user_id' => $received_payment_details->__get("user_id"),
	'amount' => $received_payment_details->__get("amount"),
	'note' => $received_payment_details->__get("note"),
	'timestamp' => $received_payment_details->__get("timestamp"),
	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getReceviedPaymentDetailById($rpd_id)
    {
        $result = $this->_db_table->find($rpd_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $received_payment_details = new Application_Model_ReceivedPaymentDetails($row);
        return $received_payment_details;
    }
    public function getAllReceviedPaymentDetails()
    {
        $result = $this->_db_table->fetchAll(null,array('rpd_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $received_payment_details_object_arr = array();
        foreach ($result as $row)
        {
                $received_payment_details_object = new Application_Model_ReceivedPaymentDetails($row);
                array_push($received_payment_details_object_arr,$received_payment_details_object);
        }
        return $received_payment_details_object_arr;
    }
    public function updateRecevied_Payment_Detail(Application_Model_ReceivedPaymentDetails $received_payment_details)
    {
        $data = array(
	'cr_id' => $received_payment_details->__get("cr_id"),
	'user_id' => $received_payment_details->__get("user_id"),
	'amount' => $received_payment_details->__get("amount"),
	'note' => $received_payment_details->__get("note"),
	'timestamp' => $received_payment_details->__get("timestamp"),
	);
        $where = "rpd_id = " . $received_payment_details->__get("rpd_id");
        $result = $this->_db_table->update($data,$where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteRecevied_Payment_DetailById($rpd_id)
    {
        $where = "rpd_id = " . $rpd_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
}
