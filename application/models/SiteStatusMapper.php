<?php
class Application_Model_SiteStatusMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_SiteStatus();
    }

    public function addNewSiteStatus(Application_Model_SiteStatus $SiteStatus)
    {
        $data = array(
                'site_id' => $SiteStatus->__get("site_id"),
                'status' => $SiteStatus->__get("status"),
                'timestamp' => $SiteStatus->__get("timestamp"),
        		'feedar_id' => $SiteStatus->__get("feedar_id"),
        		'start_date' => $SiteStatus->__get("start_date"),
				'entry_by' => $SiteStatus->__get("entry_by"),
        		'entry_type' => $SiteStatus->__get("entry_type"),
        		'entry_date' => $SiteStatus->__get("entry_date"),
                );

        $result = $this->_db_table->insert($data);
        if(count($result)==0){
            $site = new Application_Model_SiteStatus();    
            return $site;
        }else{
            return $result;
        }
    }

    

   	public function getSiteStatusByDate($site_id,$month,$year)
   	{
   	 
   	        $query="SELECT (select count(*) from site_status where timestamp >='".$year."-".$month."-01' and  timestamp <'".$year."-".$month."-08' and site_id=s.site_id and status='INACTIVE') as week1,
                (select count(*) from site_status where timestamp >='".$year."-".$month."-08' and  timestamp <'".$year."-".$month."-15' and site_id=s.site_id and status='INACTIVE') as week2,
                (select count(*) from site_status where timestamp >='".$year."-".$month."-15' and  timestamp <'".$year."-".$month."-22' and site_id=s.site_id and status='INACTIVE') as week3,
                (select count(*) from site_status where timestamp >='".$year."-".$month."-22' and  timestamp <='".$year."-".$month."-31' and site_id=s.site_id and status='INACTIVE') as week4
                from site_status as s where s.site_id=".$site_id." and status='INACTIVE' group by timestamp limit 1;";
   	    
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
            return $result[0];
   		}
   	}

   	public function getSiteStatusBySiteId($site_id)
   	{
           $date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
   	    $query="SELECT * from site_status where status='INACTIVE' and  site_id=".$site_id." and month(timestamp)=month('".$timestamp."') and year(timestamp)=year('".$timestamp."') order by timestamp desc";
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
               return $result;
   		}
   	}
     public function getActiveBySiteId($site_id)
   	{
           $date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
   	    $query="SELECT * from site_status where status='ACTIVE' and  site_id=".$site_id." and month(timestamp)=month('".$timestamp."') and year(timestamp)=year('".$timestamp."') order by timestamp desc";
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
               return $result[0];
   		}
   	}
   	
   	public function getSiteStatusByFeedarId($feedar_id)
   	{
   		$date = new Zend_Date();
   		$date->setTimezone("Asia/Calcutta");
   		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
   		$query="SELECT * from site_status where status='INACTIVE' and  feedar_id=".$feedar_id." and month(timestamp)=month('".$timestamp."') and year(timestamp)=year('".$timestamp."') order by timestamp desc";
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
   	public function getActiveByFeedarId($feedar_id)
   	{
   		$date = new Zend_Date();
   		$date->setTimezone("Asia/Calcutta");
   		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
   		$query="SELECT * from site_status where status='ACTIVE' and  feedar_id=".$feedar_id." and month(timestamp)=month('".$timestamp."') and year(timestamp)=year('".$timestamp."') order by timestamp desc";
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result[0];
   		}
   	}
   	
   	public function getdayStatusByFeedarId($feedar_id,$timestamp)
   	{
   		 
   		  $query="SELECT * from site_status where status='INACTIVE' and  feedar_id=".$feedar_id." and day(timestamp)=day('".$timestamp."') and month(timestamp)=month('".$timestamp."') and year(timestamp)=year('".$timestamp."') order by timestamp desc";
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
   	
	public function getSiteStatusByFeederData($feedar_id=null,$month=null,$year=null)
   	{
   		 if($feedar_id==null){
				$feedar_id=0;
		 }
   		
 		$query="select count(*) as total from site_status where 
   				month(timestamp) =".$month." and  year(timestamp) =".$year." and 
   				feedar_id in (".$feedar_id.") and status='INACTIVE'";
   		
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return 0;
   		}
   		else{
   			return $result[0]["total"];
   		}
   	}
	public function getAllSiteStatus()
   	{
   		$query = " SELECT * From site_status inner join sites on site_status.site_id=sites.site_id where sites.site_status!='Disable' group by sites.site_id" ;
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		 
   		if( count($result) == 0 ) {
   			return false;
   		}
   		$SiteMaster_object_arr = array();
   		foreach ($result as $row)
   		{
   			 
   			$SiteMaster_object = new Application_Model_SiteStatus($row);
   			 
   			$SiteMaster_object->__set("site_id", $row["site_id"]);
   			$SiteMaster_object->__set("feedar_id", $row["feedar_id"]);
   			$SiteMaster_object->__set("status", $row["status"]);
   			$SiteMaster_object->__set("timestamp", $row["timestamp"]);
   			$SiteMaster_object->__set("start_date", $row["start_date"]);
   			$SiteMaster_object->__set("site_name", $row["site_name"]);
   			array_push($SiteMaster_object_arr,$SiteMaster_object);
   		}
   		return $SiteMaster_object_arr;
   	}
	  	public function getStartDate($site_id, $feeder_id, $timestamp) {
   	
   		 $query = "SELECT `start_date` from `site_status` WHERE `site_id`=".$site_id."
              				AND `feedar_id`=".$feeder_id."
              				AND date(timestamp)='".$timestamp."'";
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
   	public function checkEntry($site_id, $feeder_id, $timestamp, $start_date){
   		$query = "SELECT `start_date` from `site_status` WHERE `site_id`=".$site_id."
              AND `feedar_id`=".$feeder_id."
              AND `timestamp`='".$timestamp."'
              AND `start_date`='".$start_date."'";
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
	
	public function shutdownTimeCapture($site_id=NULL, $feeder_id=NULL, $start_date=NULL, $end_date=NULL){
   		
		$query = "SELECT * from `site_status` WHERE `site_id`=".$site_id." AND `feedar_id`=".$feeder_id." AND 
						date(timestamp)>='".$start_date."' AND date(timestamp)<='".$end_date."' and status='INACTIVE'";
		
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
   			return $result;
   		}
   	}
}
