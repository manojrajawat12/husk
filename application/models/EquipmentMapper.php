<?php
class Application_Model_EquipmentMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Equipment();
	}

	public function addNewEquipment(Application_Model_Equipment $Equipment)
	{

		$data = array(
				'name' => $Equipment->__get("name"),
				'brand' => $Equipment->__get("brand"),
				'type' => $Equipment->__get("type"),
				
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getEquipmentById($Equipment_id)
	{
		
		$result = $this->_db_table->find($Equipment_id);
		
		if( count($result) == 0 ) {
			return false;
		}
		$row = $result->current();
		$state = new Application_Model_Equipment($row);
	
		return $state;
	}
	
	public function getAllEquipment()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('id'));
		if( count($result) == 0 ) {
			return false;
		}
		$state_object_arr = array();
		foreach ($result as $row)
		{
			$state_object = new Application_Model_Equipment($row);
			array_push($state_object_arr,$state_object);
		}
		return $state_object_arr;
	}
	
	public function updateEquipment(Application_Model_Equipment $Equipment)
	{
		$data = array(
				'name' => $Equipment->__get("name"),
				'brand' => $Equipment->__get("brand"),
				'type' => $Equipment->__get("type"),
				
		);
		$where = "id = " . $Equipment->__get("id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function deleteEquipmentById($id)
	{
		$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
	
			return true;
		}
	}

}