<?php

class Application_Model_CaTransactionsMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_CaTransactions();
    }

    public function addNewCaTransaction(Application_Model_CaTransactions $catransaction)
    {
        $data = array(
                'collection_agent_id'=> $catransaction->__get("collection_agent_id"),
                'transaction_type'=> $catransaction->__get("transaction_type"),
                'transaction_amount'=> $catransaction->__get("transaction_amount"),
                'timestamp'=> $catransaction->__get("timestamp"),
                'remarks'=> $catransaction->__get("remarks"),
	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getCaTransactionById($ca_transaction_id)
    {
        $result = $this->_db_table->find($ca_transaction_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $catransaction = new Application_Model_CaTransactions($row);
        return $catransaction;
    }
    public function getAllCaTransactions()
    {
        $result = $this->_db_table->fetchAll(null,array('ca_transaction_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $catransaction_object_arr = array();
        foreach ($result as $row)
        {
                $catransaction_object = new Application_Model_CaTransactions($row);
                array_push($catransaction_object_arr,$catransaction_object);
        }
        return $catransaction_object_arr;
    }
    public function updateCaTransaction(Application_Model_CaTransactions $catransaction)
    {
        $data = array(
            'collection_agent_id'=> $catransaction->__get("collection_agent_id"),
                'transaction_type'=> $catransaction->__get("transaction_type"),
                'transaction_amount'=> $catransaction->__get("transaction_amount"),
                'timestamp'=> $catransaction->__get("timestamp"),
                'remarks'=> $catransaction->__get("remarks"),
	);
        $where = "ca_transaction_id = ". $catransaction->__get("ca_transaction_id");
        $result = $this->_db_table->update($data,$where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteCaTransactionById($ca_transaction_id)
    {
        $where = "ca_transaction_id = ". $ca_transaction_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
}

