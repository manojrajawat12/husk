<?php
class Application_Model_PlantVisitMapper
{
    protected $_db_table;
    public function __construct()
    {
        $this->_db_table = new Application_Model_DbTable_PlantVisit();
    }

    public function addNewPlantVisit(Application_Model_PlantVisit $plantvisit)
    {
        $data = array(
                'date_of_visit' => $plantvisit->__get("date_of_visit"),
                'hours_on_plant' => $plantvisit->__get("hours_on_plant"),
                'attachment' => $plantvisit->__get("attachment"),
                'issues' => $plantvisit->__get("issues"),
                'remark' => $plantvisit->__get("remark"),
                'entry_by' => $plantvisit->__get("entry_by"),
                'entry_date' => $plantvisit->__get("entry_date"),
        		'site_id' => $plantvisit->__get("site_id"),
                //'approve_by' => $plantvisit->__get("approve_by"),
                //'approve_date' => $plantvisit->__get("approve_date")
            );
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return $result;
        }
    }
      
    // public function getCustomerCommunicationById($id)
    // {
        
    //     $result = $this->_db_table->find($id);
        
    //     if( count($result) == 0 ) {
    //         $customercommunication = new Application_Model_CustomerCommunication();
    //         return $customercommunication;
    //     }
    //     $row = $result->current();
    //     $customercommunication = new Application_Model_CustomerCommunication($row);
    
    //     return $customercommunication;
    // }
    
    public function getAllPlantVisits()
    {
        $where = array();
        
        if(count($where)==0)
        {
            $where = null;
        }
    
        $result = $this->_db_table->fetchAll($where,array('id DESC'));
        if( count($result) == 0 ) {
            return false;
        }
        $customer_query_object_arr = array();
        foreach ($result as $row)
        {
            $customer_query_object = new Application_Model_PlantVisit($row);
            array_push($customer_query_object_arr,$customer_query_object);
        }

        return $customer_query_object_arr;
    }
    
    public function updatePlantVisit(Application_Model_PlantVisit $plantvisit)
    {
        $data = array(
                'date_of_visit' => $plantvisit->__get("date_of_visit"),
                'hours_on_plant' => $plantvisit->__get("hours_on_plant"),
                'attachment' => $plantvisit->__get("attachment"),
                'issues' => $plantvisit->__get("issues"),
                'remark' => $plantvisit->__get("remark"),
        		'site_id' => $plantvisit->__get("site_id"),
            );
        $where = "id = " . $plantvisit->__get("id");
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    
    public function deletePlantVisitById($visit_id)
    {
        $where = "id = " . $visit_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
 
    
}