
<?php
class Application_Model_ConnectionRequestMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_ConnectionRequest();
	}

	public function addConnectionRequest(Application_Model_ConnectionRequest $CD)
	{
		$data = array(
				'name' => $CD->__get("name"),
				'mob_no' => $CD->__get("mob_no"),
				'ref_name' => $CD->__get("ref_name"),
				'ref_no' => $CD->__get("ref_no"),
				'state' => $CD->__get("state"),
				'site' => $CD->__get("site"),
				'status' => $CD->__get("status"),
				'track_status' => $CD->__get("track_status"),
				'entry_date' => $CD->__get("entry_date"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	 
	
	public function getConnectionRequest($status=NULL,$site_id=NULL)
	{
	 
		$query ="select * from connection_request";
	
		$stmt = $this->_db_table_con->getAdapter()->query($query);
		$result=$stmt->execute();
	
		if($result)
		{
			return true;
		}
	}
	 
}