<?php

class Application_Model_EnterprisePrice
{
	private $enterprise_price_id;
	private $unit_from;
	private $unit_to;
	private $enterprise_price;
	private $enterprise_scheme;

	public function __construct($enterprise_row = null)
	{
		if( !is_null($enterprise_row) && $enterprise_row instanceof Zend_Db_Table_Row ) {
			$this->enterprise_price_id = $enterprise_row->enterprise_price_id;
			$this->unit_from = $enterprise_row->unit_from;
			$this->unit_to = $enterprise_row->unit_to;
			$this->enterprise_price = $enterprise_row->enterprise_price;
			$this->enterprise_scheme = $enterprise_row->enterprise_scheme;
		}
	}
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}

}

