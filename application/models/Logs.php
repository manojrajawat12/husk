<?php

class Application_Model_Logs
{
    private $id;
    private $user_id;
    private $username;
    private $priority_level;
    private $priority_name;
    private $timestamp;
    private $message;
    
    public function __construct($log_row = null)
    {
        if( !is_null($log_row) && $log_row instanceof Zend_Db_Table_Row ) {
                $this->id = $log_row->id;
                $this->user_id = $log_row->user_id;
                $this->username = $log_row->username;
                $this->priority_level = $log_row->priority_level;
                $this->priority_name = $log_row->priority_name;
                $this->timestamp = $log_row->timestamp;
                $this->message = $log_row->message;
                
                
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }



}

