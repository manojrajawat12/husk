<?php
class Application_Model_RevenueTargetMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_RevenueTarget();
    }
  public function addNewRevenueTarget(Application_Model_RevenueTarget $revenue)
    {
        $data = array(
    		'site_id' => $revenue->__get("site_id"), 
            'state_id' => $revenue->__get("state_id"),
            'month' => $revenue->__get("month"),    
            'year' => $revenue->__get("year"),  
            'target' => $revenue->__get("target"),
            'opex' => $revenue->__get("opex"),
			'type' => $revenue->__get("type"),   
        );
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            $rev = new Application_Model_RevenueTarget();    
            return $rev;
        }
        else
        {
            return $result;
        }
    }
	
    public function getSiteById($site_id)
    {
        $result = $this->_db_table->find($site_id);
        if( count($result) == 0 ) {
                $site = new Application_Model_Sites();    
                $site->__set("site_name","---Deleted---");
                return $site;
        }
        $row = $result->current();
        $site = new Application_Model_Sites($row);
        return $site;
    }
    
    
    
    public function getSiteByClusterId($cluster_id)
    {
    	$where = array();
        $where[] = " cluster_id=".$cluster_id." and site_status!='Disable'";
                
        $result = $this->_db_table->fetchAll($where,array('cluster_id DESC'));
      
        if( count($result) == 0 ) {
                return false;
        }
        $site_object_arr = array();
        foreach ($result as $row)
        {
                $site_object = new Application_Model_Sites($row);
                array_push($site_object_arr,$site_object);
        }
        return $site_object_arr;
    }
    
    
    public function getAllRevenueTarget()
    {
     
    	$where=NULL;
    	 
        $result = $this->_db_table->fetchAll($where,array('state_id desc','site_id desc','target_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $site_object_arr = array();
        foreach ($result as $row)
        {
                $site_object = new Application_Model_RevenueTarget($row);
                array_push($site_object_arr,$site_object);
        }
        return $site_object_arr;
    }
 
	public function updateRevenueTarget(Application_Model_RevenueTarget $revenue)
    {
         $data = array(
    		    'site_id' => $revenue->__get("site_id"),
            'state_id' => $revenue->__get("state_id"),
            'month' => $revenue->__get("month"),    
            'year' => $revenue->__get("year"),
            'target' => $revenue->__get("target"),
            'opex' => $revenue->__get("opex"),
			 'type' => $revenue->__get("type"),
           );
       // $where = "target_id = " . $revenue->__get("target_id");
       // $result = $this->_db_table->update($data,$where);
         $query ="UPDATE `revenue_target` SET `site_id` =". $revenue->__get('site_id').", `state_id` =".$revenue->__get('state_id').", `month` = '".$revenue->__get('month')."', `year` =". $revenue->__get('year').",
          `target` = ".$revenue->__get('target').", `opex` =". $revenue->__get('opex').", `type` ='". $revenue->__get('type')."'  WHERE target_id =  ".$revenue->__get('target_id');
         
         $stmt = $this->_db_table->getAdapter()->query($query);
         $result=$stmt->execute();
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
 
 
	public function deleteRevenueTargetById($revenue_id)
    {
        $where = "target_id = " . $revenue_id;
        $result = $this->_db_table->delete($where);
        if($result==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
     	
   	public function checkRevenueTarget($site_id=NULL,$month=NULL,$year=NULL,$id=NULL)
   	{
   		if($id!=NULL){
   			$checkid=" and target_id!=$id ";
   		}else{
   			$checkid=" ";
   		}
   	  $query="SELECT count(*) as countResult FROM  revenue_target  
   	 		where site_id=".$site_id." and month='".$month."' and year=".$year." ".$checkid;
			
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	else{
    		return $result[0]["countResult"];
    	}
   	}
   	
   	
   	
   	
   	public function updateRevenueTargetSiteId($site_id,$month,$year,$target)
   	{
   		$query ="update revenue_target set target='".$target."' where  site_id=".$site_id." and month='".$month."' and year='".$year."'";
   	
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result=$stmt->execute();
   	
   		if($result)
   		{
   			return true;
   		}else{
   			return false;
   		}
   	}
   	
   public function getAllRevenueSiteId($site_id,$month,$year,$opex,$type)
   	{ 
   		 $query ="select * from revenue_target where  site_id=".$site_id." and month='".$month."' and year=".$year." and opex=".$opex." and type='".$type."'";
   	 
   		 
   	   $stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	else{
    		return $result;
    	}
   	}
 	public function getAllRevenueByMonthYear($month,$year,$state_id=NULL,$site_id=NULL,$role_sites_id=NULL)
   	{
   		if($state_id!=NULL){
   			$where=" and sites.state_id in (".$state_id.")";
   		}elseif($site_id!=NULL){
   			$where=" and sites.site_id in (".$site_id.")";
   		}else{
   			$where=" and sites.site_id in (".implode(",", $role_sites_id).")";
   		}
   	    $query ="select sum(target) as targets from revenue_target inner join sites on revenue_target.site_id=sites.site_id 
   	   		where sites.site_status!='Disable' and opex=44  ".$where." and month='".$month."' and year=".$year;
    
   	
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( $result[0]["targets"] == NULL ) {
   			return 0;
   		}
   		else{
   			return $result[0]["targets"];
   		}
   	}
	
public function getAllRevenueSumBySiteId($site_id,$month,$year)
   	{
   		$query ="select sum(target) as targets from revenue_target where  site_id=".$site_id." and month='".$month."' and year=".$year;
   	
   	
   		$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   	    if( $result[0]["targets"] == NULL ) {
   			return 0;
   		}
   		else{
   			return $result[0]["targets"];
   		}
   	}
 public function getAllRevenueTargetByOpex($opex)
    {
      
      $where="";
       
        $result = $this->_db_table->fetchAll($where,array('state_id desc','site_id desc','target_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $site_object_arr = array();
        foreach ($result as $row)
        {
                $site_object = new Application_Model_RevenueTarget($row);
                array_push($site_object_arr,$site_object);
        }
        return $site_object_arr;
    }
	
	  public function getAllRevenueMonthSiteId($site_id=null,$month=null,$year=null,$opex=null,$state_id=null,$role_sites_id=null)
    {
    	if($site_id!=NULL){
    		$where="  s.site_id in (".$site_id.")";
    	}elseif($state_id!=NULL){
    		$where="  s.state_id in (".$state_id.")";
    	}else{
    		$where="  s.site_id in (".implode(",", $role_sites_id).")";
    	}
    	$dateObj   = DateTime::createFromFormat('!m', $month);
    	$monthName = $dateObj->format('F');
    	
    	$query ="select * from revenue_target rt inner join s on s.site_id=rt.site_id
    			 where  ".$where." and month='".$monthName."' and year=".$year." and opex=".$opex;
    
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	else{
    		return $result;
    	}
    }   
}
