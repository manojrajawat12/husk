<?php
class Application_Model_ActivationPaymentsMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_ActivationPayments();
    }

    public function addNewActivationPayment(Application_Model_ActivationPayments $activation_payment)
    {
        $data = array(
	'consumer_id' => $activation_payment->__get("consumer_id"),
	'user_id' => $activation_payment->__get("user_id"),
	'amount' => $activation_payment->__get("amount"),
	'timestamp' => $activation_payment->__get("timestamp"),
	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getActivationPaymentById($ap_id)
    {
        $result = $this->_db_table->find($ap_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $activation_payment = new Application_Model_ActivationPayments($row);
        return $activation_payment;
    }
    public function getAllActivationPayments()
    {
        $result = $this->_db_table->fetchAll(null,array('ap_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $activation_payment_object_arr = array();
        foreach ($result as $row)
        {
                $activation_payment_object = new Application_Model_ActivationPayments($row);
                array_push($activation_payment_object_arr,$activation_payment_object);
        }
        return $activation_payment_object_arr;
    }
    public function updateActivationPayment(Application_Model_ActivationPayments $activation_payment)
    {
        $data = array(
	'consumer_id' => $activation_payment->__get("consumer_id"),
	'user_id' => $activation_payment->__get("user_id"),
	'amount' => $activation_payment->__get("amount"),
	'timestamp' => $activation_payment->__get("timestamp"),
	);
        $where = "ap_id = " . $activation_payment->__get("ap_id");
        $result = $this->_db_table->update($data,$where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteActivationPaymentById($ap_id)
    {
        $where = "ap_id = " . $ap_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
}
