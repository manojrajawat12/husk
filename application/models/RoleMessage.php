<?php

class Application_Model_RoleMessage
{
    private $role_message_id;
    private $message_id;
    private $role_id;
    private $is_metered;

    public function __construct($role_msg_row = null)
    {
        if( !is_null($role_msg_row) && $role_msg_row instanceof Zend_Db_Table_Row ) {
            
                $this->role_message_id = $role_msg_row->role_message_id;
                $this->message_id = $role_msg_row->message_id;
                $this->role_id = $role_msg_row->role_id;
                $this->is_metered = $role_msg_row->is_metered;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}
