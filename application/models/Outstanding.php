<?php

class Application_Model_Outstanding
{
	private $id;
    private $consumer_id;
    private $consumer_name;
    private $consumer_connection_id;
    private $outstanding;
    private $site_id;
    
    public function __construct($out_row = null)
    {
        if( !is_null($out_row) && $out_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $out_row->id;
                $this->consumer_id = $out_row->consumer_id;
                $this->consumer_name = $out_row->consumer_name;
                $this->consumer_connection_id = $out_row->consumer_connection_id;
                $this->outstanding = $out_row->outstanding;
                $this->site_id = $out_row->site_id;
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

