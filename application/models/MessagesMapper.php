<?php
class Application_Model_MessagesMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Messages();
	}

	public function addRoleMessage(Application_Model_RoleMessage $roleMsg)
	{

		$data = array(
				'message_id' => $roleMsg->__get("message_id"),
				'role_id' => $roleMsg->__get("role_id"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getRoleMessageById($message_id)
	{
		$result = $this->_db_table->find($message_id);
		if( count($result) == 0 ) {
			$role = new Application_Model_Messages();
			return $role;
		}
		$row = $result->current();
		$role = new Application_Model_Messages($row);
		return $role;
	}
	
	public function getAllMessages()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('message_id ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$role_site_object_arr = array();
		foreach ($result as $row)
		{
			$role_site_object = new Application_Model_Messages($row);
			array_push($role_site_object_arr,$role_site_object);
		}
		return $role_site_object_arr;
	}
	
	
	
	
	
	public function deleteRoleSiteById($role_id,$site_id=NULL)
	{
		if($site_id==NULL){
		$where = "role_id = " . $role_id;
		}
		else{
			$where = " site_id not in (".implode(",", $site_id).")  and role_id = ".$role_id;
		}
		
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function checkRoleSiteById($role_id,$site_id)
	{
	$query="SELECT * from role_sites where role_id=".$role_id." and site_id =".$site_id;
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	$count=count($result);
    	
    	if( $count == 0 ) {
    		return $count;
    	}
		else
		{
			return $count;
		}
	}
	
	public function getAllRoleMessages($role_id,$site_id)
	{
		$query="SELECT * from role_sites where role_id=".$role_id." and site_id =".$site_id;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		$count=count($result);
		 
		if( $count == 0 ) {
			return $count;
		}
		else
		{
			return $count;
		}
	}
	
	public function getRoleMessageByMessageId($role_id)
	{
		$query="SELECT * FROM messages where message_id in (".implode(",", $role_id).")";
	 
		$stmt= $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if (count($result) == 0) {
			return false;
		}
	
		return $result;
	}
	
}