<?php

class Application_Model_ConsumerScheme
{
	
	private $consumer_scheme_id;
	private $consumer_id;
	private $consumer_scheme;
	private $cluster_id;
	private $site_id;
	private $package_continue;
	private $scheme_continue;
	private $total_month;
	private $equip_month_dis;
	private $timestamp;
	private $assign_date;
	
	public function __construct($consumerScheme_row = null)
	{
		if( !is_null($consumerScheme_row) && $consumerScheme_row instanceof Zend_Db_Table_Row ) {
			$this->consumer_scheme_id = $consumerScheme_row->consumer_scheme_id;
			$this->consumer_id = $consumerScheme_row->consumer_id;
			$this->consumer_scheme = $consumerScheme_row->consumer_scheme;
			$this->cluster_id = $consumerScheme_row->cluster_id;
			$this->site_id = $consumerScheme_row->site_id;
			$this->package_continue = $consumerScheme_row->package_continue;
			$this->scheme_continue = $consumerScheme_row->scheme_continue;
			$this->total_month = $consumerScheme_row->total_month;
			$this->equip_month_dis = $consumerScheme_row->equip_month_dis;
			$this->timestamp = $consumerScheme_row->timestamp;
			$this->assign_date = $consumerScheme_row->assign_date;
		}
	}
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}

}