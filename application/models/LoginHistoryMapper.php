<?php

class Application_Model_LoginHistoryMapper
{
    protected $_db_table;
    
    public function __construct()
    {
            //Instantiate the Table Data Gateway for the Shows table
            $this->_db_table = new Application_Model_DbTable_LoginHistory();
    }
    
    public function getAllLastLoginHistoryById($user_id=NULL,$status=NULL) {

         $query = "SELECT * from `login_history` where status='Login' and entry_by='".$status."' and user_id=".$user_id." order by timestamp desc" ;

        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();

        if( count($result) == 0 ) {
            return false;
        }
        return $result;
    }
	
	 public function getAllLastLoginHistoryByIP($user_id=NULL,$status=NULL) {

         $query = "SELECT * from `login_history` where status='Login' and entry_by='".$status."' and user_id=".$user_id." group by ip_address order by timestamp desc" ; 

        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();

        if( count($result) == 0 ) {
            return false;
        }
        return $result;
    }
}   