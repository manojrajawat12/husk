<?php

class Application_Model_wattage
{
    private $watt_id;
    private $wattage;
    private $site_id;

    public function __construct($wattage_row = null)
    {
        if( !is_null($wattage_row) && $wattage_row instanceof Zend_Db_Table_Row ) {
            
                $this->watt_id = $wattage_row->watt_id;
                $this->wattage = $wattage_row->wattage;
                $this->site_id = $wattage_row->site_id;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

