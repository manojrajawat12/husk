<?php
class Application_Model_AgentSiteMappingsMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_AgentSiteMappings();
    }

    public function addNewAgentSiteMapping(Application_Model_AgentSiteMappings $agent_site_mapping)
    {
        $data = array(
	'collection_agent_id' => $agent_site_mapping->__get("collection_agent_id"),
	'site_id' => $agent_site_mapping->__get("site_id"),
      
	
	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getAgentSiteMappingById($agent_site_mapping_id)
    {
        $result = $this->_db_table->find($agent_site_mapping_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $agent_site_mapping = new Application_Model_AgentSiteMappings($row);
        return $agent_site_mapping;
    }
    
      public function getAgentSiteMappingBySiteId($site_id)
    {
        $where=array(
           "site_id = ?"  =>  $site_id,
        ) ; 
          
        $result = $this->_db_table->fetchAll($where);
        if( count($result) == 0 ) {
                return false;
        }
        $agent_site_mapping_object_arr = array();
        foreach ($result as $row)
        {
                $agent_site_mapping_object = new Application_Model_AgentSiteMappings($row);
                array_push($agent_site_mapping_object_arr,$agent_site_mapping_object);
        }
        return $agent_site_mapping_object_arr;
    }
    public function getAllAgentSiteMappings()
    {
        $result = $this->_db_table->fetchAll(null,array('agent_site_mapping_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $agent_site_mapping_object_arr = array();
        foreach ($result as $row)
        {
                $agent_site_mapping_object = new Application_Model_AgentSiteMappings($row);
                array_push($agent_site_mapping_object_arr,$agent_site_mapping_object);
        }
        return $agent_site_mapping_object_arr;
    }
    public function updateAgentSiteMapping(Application_Model_AgentSiteMappings $agent_site_mapping)
    {
         $data = array(
	'collection_agent_id' => $agent_site_mapping->__get("collection_agent_id"),
	'site_id' => $agent_site_mapping->__get("site_id"),
	);
        $where = "agent_site_mapping_id = " . $agent_site_mapping->__get("agent_site_mapping_id");
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteAgentSiteMappingById($agent_site_mapping_id)
    {
        $where = "agent_site_mapping_id = " . $agent_site_mapping_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
}
