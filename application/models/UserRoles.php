<?php

class Application_Model_UserRoles
{
    private $user_role_id;
    private $user_role;
    

    public function __construct($user_role_row = null)
    {
        if( !is_null($user_role_row) && $user_role_row instanceof Zend_Db_Table_Row ) {
            
                $this->user_role_id = $user_role_row->user_role_id;
                $this->user_role = $user_role_row->user_role;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

