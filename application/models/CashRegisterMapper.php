<?php

class Application_Model_CashRegisterMapper {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Application_Model_DbTable_CashRegister();
		$this->_db_table_cashpending = new Application_Model_DbTable_CashRegisterPending();
        $logger = new My_Logger();
        $this->_logger = $logger->getLogger();
     //   $auth=new My_Auth('user');
      //  $this->_userName=$auth->getIdentity()->user_fname;
    }

    public function addNewCashRegister(Application_Model_CashRegister $cash_register,$status=null,$pending=null) {
         $timestamp = $cash_register->__get("timestamp");
         $mtr_entry=$cash_register->__get("mtr_entry");
         $user_val=$cash_register->__get("user_val"); 
         $transaction_type=$cash_register->__get("transaction_type");
		if($cash_register->__get("cron_entry")!=null){
			$cron_entry=$cash_register->__get("cron_entry");
		}else{
			$cron_entry=null;
		}
        if($timestamp==NULL)
        {
            $date = new Zend_Date(); 
            $date->setTimezone("Asia/Calcutta");
            $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
        }
        if($mtr_entry ==NULL)
        {
        	$mtr_entry='0';
        }
        if($cash_register->__get("user_val")==NULL){
        	$user_val=0;
        }
        
		if($cash_register->__get("cr_entry_type")=='DEBIT' || $cash_register->__get("cr_entry_type")=='DISCOUNT'){
			$date = new Zend_Date(); 
            $date->setTimezone("Asia/Calcutta");
            $approved_date = $date->toString("yyyy-MM-dd HH:mm:ss");
			
			$consumersMapper = new Application_Model_ConsumersMapper();
			$consumer_for_site = $consumersMapper->getConsumerById($cash_register->__get("consumer_id"));
			$site_id_value=$consumer_for_site->__get("site_id");
			$receipt_numbers=$this->getMaxRecieptNumber($site_id_value); 
			if($receipt_numbers){
				$receipt_number=intval($receipt_numbers)+1;
			}else{
				$receipt_number=1;
			}
		}else{
			$receipt_number=$cash_register->__get("receipt_number");
			$approved_date =NULL;
		} 
			$settingMapper=new Application_Model_SettingsMapper();
			$setings=$settingMapper->getSettingById(29);
			if($setings->__get("setting_value")=="no"){
				$cr_status='ACTIVE';
			}else{
				if($cash_register->__get("cr_entry_type")=='DEBIT' || $pending=="pending"){
					$date = new Zend_Date(); 
					$date->setTimezone("Asia/Calcutta");
					$approved_date = $date->toString("yyyy-MM-dd HH:mm:ss");
					$cr_status='ACTIVE';
				}else{
					$cr_status='NEW';
				}
			}
			
			$data = array(
            'consumer_id' => $cash_register->__get("consumer_id"),
            'user_id' => $cash_register->__get("user_id"),
            'receipt_number' => $receipt_number,
            'transaction_id' => $cash_register->__get("transaction_id"),
            'cr_entry_type' => $cash_register->__get("cr_entry_type"),
            'cr_amount' => $cash_register->__get("cr_amount"),
			'cr_status' => $cr_status,
            'timestamp' => $timestamp,
        	'mtr_entry' =>$mtr_entry,
        	'user_val' =>$user_val,
        	'transaction_type' =>$transaction_type,
			'cron_entry'=>$cron_entry,
        	'remark' => $cash_register->__get("remark"),
			'entry_status' => $cash_register->__get("entry_status"), 
			'package_id' => $cash_register->__get("package_id"),
			'approved_date' =>$approved_date,
			'approved_by' => $cash_register->__get("approved_by"),
		
        );
       
	   $result = $this->_db_table->insert($data);
			
        if (count($result) == 0) {
            return false;
        } else{
        		$auth=new My_Auth('user');
	        	 
	        	if(count($auth->getIdentity())==0){
	        		$collectionMapper=new Application_Model_CollectionAgentsMapper();
	        		$collection=$collectionMapper->getCollectionAgentById($cash_register->__get("user_id"));
				  $this->_logger->info("New CashRegister ID ".$result." created by ".$collection->__get("agent_fname")."/".$collection->__get("agent_mobile")." for consumer ID ".$cash_register->__get("consumer_id")." type ".$cash_register->__get("cr_entry_type")." amount ".$cash_register->__get("cr_amount")."");
	        	}elseif(count($auth->getIdentity())==1){
	        		$this->_logger->info("New CashRegister ID ".$result." created by ".$auth->getIdentity()->user_fname." for consumer ID ".$cash_register->__get("consumer_id")." type ".$cash_register->__get("cr_entry_type")." amount ".$cash_register->__get("cr_amount")."");
	            }else{
	            	$this->_logger->info("New CashRegister ID ".$result." created by CRON for consumer ID ".$cash_register->__get("consumer_id")." type ".$cash_register->__get("cr_entry_type")." amount ".$cash_register->__get("cr_amount")."");
	            }
				  $consumer_id=$cash_register->__get("consumer_id");
				  $cr_entry_type=$cash_register->__get("cr_entry_type");
				  $cr_amount=$cash_register->__get("cr_amount");
				  $trans_date=date_parse_from_format("Y-m-d", $timestamp);
				  $day=$trans_date["day"];
				  $month= $trans_date["month"];
				  $year= $trans_date["year"];
				   
				  $current_date=$year."-".$month."-".$day;
	        	 
				  $selectEntry="call insertDefaulterfromcashRegister($consumer_id,'$current_date','$cr_entry_type',$cr_amount)";
				  //echo $selectEntry;
				 $stmt = $this->_db_table->getAdapter()->query($selectEntry);
				 $entry_result=$stmt->execute();
        
        	return $result;
        }
    }

    public function getCashRegisterById($cr_id) {
       
        $result = $this->_db_table->find($cr_id);
        if (count($result) == 0) {
            return false;
        }
        $row = $result->current();
        $cash_register = new Application_Model_CashRegister($row);
        return $cash_register;
    }

    public function getAllCashRegister() {
    	
        $result = $this->_db_table->fetchAll(null, array('timestamp DESC'));
      
        if (count($result) == 0) {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister($row);
            array_push($cash_register_object_arr, $cash_register_object);
        }
        
        return $cash_register_object_arr;
    }
    
     public function getAllCashRegisterBysiteAndCollectionId($site_id=NULL,$agent_id=NULL,$curr_date = NULL,$cluster_id=NULL,$user=NULL,$state_id=NULL,$sites_id=NULL,$last_date=NULL,$entry=NULL,$er_entry_type=NULL,$package_type=NULL) {
         
        $join="INNER JOIN consumers c ON c.consumer_id=cr.consumer_id inner join sites s on c.site_id=s.site_id ";
        if($site_id!=NULL){
			$where = " and c.site_id=".$site_id;
        }else{
        	$where = " and s.site_id in (".implode(",", $sites_id).") and s.site_id!=1 ";
		}
        
		if($cluster_id!=NULL) {
			$where.= " and s.cluster_id=".$cluster_id;
        }
		
		if($agent_id!=NULL) {
			$where.= " and cr.user_id=".$agent_id." and cr.transaction_type in ('(M)','(S)')"; 
        }
		
		if($entry!=NULL) {
			if($entry[0]=="'DISCOUNT'"){
				//$where.= " and cr.entry_status in (".implode(",", $entry).")";
			}else{
				$where.= " and cr.entry_status in (".implode(",", $entry).")";
			}
        }
		if($er_entry_type=='DEBIT') {
			$where.= " and cr.cr_entry_type in ('DEBIT')";
        }else{
			if($entry==NULL) {
				$where.= " and cr.cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT')"; 
			}elseif($entry[0]=="'DISCOUNT'"){
				$where.= " and cr.cr_entry_type in ('DISCOUNT')"; 
			}elseif(!in_array("'DISCOUNT'", $entry)) {
				$where.= " and cr.cr_entry_type in ('CREDIT','ACTIVATION')"; 
			} else{
				$where.= " and cr.cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT')"; 
			} 
		}
		if($curr_date!=NULL && $last_date!=NULL) {
			//$months = date("m", strtotime($month));
			$where.=" and date(cr.timestamp)>='".$last_date."' and date(cr.timestamp)<='".$curr_date."'"; 
        }
        if($package_type!=NULL){
			if($package_type==2){
				$join.=" inner join packages p ON p.package_id=cr.package_id";
				$where.=" and p.is_postpaid=0"; 
			}elseif($package_type==1){
				$join.=" inner join packages p ON p.package_id=cr.package_id ";
				$where.=" and p.is_postpaid!=0"; 
			} 
		}
		 
		$query="SELECT * FROM cash_register cr ".$join." where cr.cr_status != 'INACTIVE' ".$where." ORDER BY cr.timestamp DESC";
        
       // $query="call getAllCashRegisterBysiteAndCollectionId($site_id,$agent_id,'$date',$cluster_id,'$role_id')";
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        $cash_register_object_arr = array();
      
        if (count($result) == 0) {
            return false;
        }
        foreach ($result as $key => $row) {
          
            $cash_register_object = new Application_Model_CashRegister($row);
            foreach($row as $key=>$value)
            {
                $cash_register_object->__set($key,$value);
            }
            array_push($cash_register_object_arr, $cash_register_object);
       
         }
        return $cash_register_object_arr;
    }
    
    public function getCashRegisterByMonthYear($month,$year) {
        $query = "SELECT * FROM cash_register WHERE MONTH(`timestamp`)=".$month." AND YEAR(`timestamp`)=".$year." and cr_status <> 'INACTIVE' ORDER BY cr_id DESC";
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        //$result = $this->_db_table->fetchAll(null, array('cr_id DESC'));
        if (count($result) == 0) {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister($row);
            foreach($row as $key=>$value)
            {
                $cash_register_object->__set($key,$value);
            }
            array_push($cash_register_object_arr, $cash_register_object);
        }
        return $cash_register_object_arr;
    }
    public function updateCashRegister(Application_Model_CashRegister $cash_register) {
        $data = array(
            'consumer_id' => $cash_register->__get("consumer_id"),
            'user_id' => $cash_register->__get("user_id"),
			'entry_status' => $cash_register->__get("entry_status"),
            'transaction_id' => $cash_register->__get("transaction_id"),
            'cr_entry_type' => $cash_register->__get("cr_entry_type"),
            'cr_amount' => $cash_register->__get("cr_amount"),
            'timestamp' => $cash_register->__get("timestamp"),
        );
        $where = "cr_id = " . $cash_register->__get("cr_id");
        
        $result = $this->_db_table->update($data, $where);
        if (count($result) == 0) {
            return false;
        } else {
        	
        	$consumer_id=$cash_register->__get("consumer_id");
        	$date = new Zend_Date();
        	$date->setTimezone("Asia/Calcutta");
        	$trans_dates = $date->toString("yyyy-MM-dd HH:mm:ss");
        	
        	$trans_date= date_parse_from_format("Y-m-d", $trans_dates);
        	$cr_entry_type=$cash_register->__get("cr_entry_type");
        	$cr_amount=$cash_register->__get("cr_amount");
        	
        	$day=$trans_date["day"];
        	$month= $trans_date["month"];
        	$year= $trans_date["year"];
        	
        	$current_date=$year."-".$month."-".$day;
        	 
        	  $selectEntry="call insertDefaulterfromcashRegister($consumer_id,'$current_date','$cr_entry_type',$cr_amount)";
            
        	 $stmt = $this->_db_table->getAdapter()->query($selectEntry);
        	 $entry_result=$stmt->execute();
            	
            return true;
        }
    }

      public function deleteCashRegisterById($cr_id) {
			$cashMapper=$this->getCashRegisterById($cr_id);
			$consumer_id=$cashMapper->__get("consumer_id");
    		$cr_entry_type=$cashMapper->__get("cr_entry_type");
    		$cr_amount=0;

        $where = "cr_id = " . $cr_id;
        $result = $this->_db_table->delete($where);
        if (count($result) == 0) {
            return false;
        } else {
 		
			
		$date = new Zend_Date($cashMapper->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
    		 $day=$date->toString("dd");
    		 $month= $date->toString("MM");
    		 $year= $date->toString("YYYY");
    		
    		$current_date=$year."-".$month."-".$day;
        	 
			
    		$selectEntry="call insertDefaulterfromcashRegister($consumer_id,'$current_date','$cr_entry_type',$cr_amount)";
    		$stmt = $this->_db_table->getAdapter()->query($selectEntry);
    		$entry_result=$stmt->execute();
            return true;
        }
    }
    public function deleteCashRegisterByTransactionID($transaction_id) {
    	$cashMapper=$this->getCrByTxnId($transaction_id);
	if($cashMapper){
    	$consumer_id=$cashMapper["consumer_id"];
    	$cr_entry_type=$cashMapper["cr_entry_type"];
    	$cr_amount=0;
    	
        $where = "transaction_id = '" . $transaction_id."'";
        $result = $this->_db_table->delete($where);
        if (count($result) == 0) {
            return false;
        } else {
		$auth=new My_Auth('user');
        	if(count($auth->getIdentity())==1){
        		$this->_logger->info("Transaction Id ".$transaction_id." has been deleted by ".$auth->getIdentity()->user_fname);
        	}  
			
			$date = new Zend_Date($cashMapper["timestamp"], "yyyy-MM-dd HH:mm:ss");
    		 $day=$date->toString("dd");
    		 $month= $date->toString("MM");
    		 $year= $date->toString("YYYY");
    		
    		$current_date=$year."-".$month."-".$day;
        	 
			
    		$selectEntry="call insertDefaulterfromcashRegister($consumer_id,'$current_date','$cr_entry_type',$cr_amount)";
    		$stmt = $this->_db_table->getAdapter()->query($selectEntry);
    		$entry_result=$stmt->execute();
            return true;
        }
	}else{
	 return false;

	}
    }

    public function getEntriesByType($value) {
        $result = array();
        $cash_register_object_arr = array();
        if (is_array($value)) {

            $whereString = "";
            foreach ($value as $str) {
                if (empty($whereString)) {
                    $whereString .= "cr_entry_type = '" . $str . "'";
                } else {
                    $whereString .= "OR cr_entry_type = '" . $str . "'";
                }
            }
            $where = $this->_db_table->select()->where($whereString)->order("cr_id DESC");
            $result = $this->_db_table->fetchAll($where);
            foreach ($result as $row) {
                $cash_register_object = new Application_Model_CashRegister($row);
                array_push($cash_register_object_arr, $cash_register_object);
            }
        } else {
            $result = $this->_db_table->fetchAll(array('cr_entry_type = ?' => $value), array('cr_id DESC'));
            foreach ($result as $row) {
                $cash_register_object = new Application_Model_CashRegister($row);
                array_push($cash_register_object_arr, $cash_register_object);
            }
        }

        if (count($cash_register_object_arr) == 0) {
            return false;
        }

        return $cash_register_object_arr;
    }

    public function searchByColumnValue($column = NULL, $column_value = NULL) {
        if ($column != NULL) {
            $where = array($column . " = ?" => $column_value);
        } else {
            $where = NULL;
        }
        $result = $this->_db_table->fetchAll($where, array('cr_id DESC'));
        if (count($result) == 0) {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister($row);
            array_push($cash_register_object_arr, $cash_register_object);
        }
        return $cash_register_object_arr;
    }

    public function applyTypeFilter($cashRegister, $typesArray) {
        $filteredValues = array();

        if ($cashRegister != Null) {
            foreach ($cashRegister as $value) {
                if (in_array($value->__get("cr_entry_type"), $typesArray)) {
                    $filteredValues[] = $value;
                }
            }
            $cashRegister = $filteredValues;
            $filteredValues = array();
        }
        return $cashRegister;
    }

    public function getEntryByConsumerId($consumer_id) {
    	$status='INACTIVE';
        $where = array("consumer_id = ?" => $consumer_id
        			, "cr_status <> ?" =>$status
        		
       			 );
        $result = $this->_db_table->fetchAll($where, array('timestamp DESC'));
     
        if (count($result) == 0) {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister($row);
            array_push($cash_register_object_arr, $cash_register_object);
        }
        return $cash_register_object_arr;
    }

    public function searchByConsumerCode($code) {
        /* $query = "SELECT cash_register.*,consumers.* 
                FROM cash_register
                INNER JOIN consumers
                ON cash_register.consumer_id=consumers.consumer_id
                WHERE consumers.consumer_code='" . $code . "' and cr_status <> 'INACTIVE'"; */
    	$query="call searchByConsumerCode($code)";
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister();
            $cash_register_object->__set("cr_id", $row["cr_id"]);
            $cash_register_object->__set("consumer_id", $row["consumer_id"]);
            $cash_register_object->__set("user_id", $row["user_id"]);
            $cash_register_object->__set("receipt_number", $row["receipt_number"]);
            $cash_register_object->__set("transaction_id", $row["transaction_id"]);
            $cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
            $cash_register_object->__set("cr_amount", $row["cr_amount"]);
            $cash_register_object->__set("timestamp", $row["timestamp"]);
            array_push($cash_register_object_arr, $cash_register_object);
        }
        return $cash_register_object_arr;
    }

    public function searchBySite($site_name) {
       /*  $query = "SELECT sites.*,consumers.*,cash_register.*
                 FROM cash_register
                 INNER JOIN consumers
                 ON consumers.consumer_id=cash_register.consumer_id
                 INNER JOIN sites
                 ON sites.site_id=consumers.site_id
                 WHERE sites.site_name LIKE '%" . $site_name . "%' and cr_status <> 'INACTIVE'"; */

        $query="call searchCrBySite($site_name)";
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister();
            $cash_register_object->__set("cr_id", $row["cr_id"]);
            $cash_register_object->__set("consumer_id", $row["consumer_id"]);
            $cash_register_object->__set("user_id", $row["user_id"]);
            $cash_register_object->__set("receipt_number", $row["receipt_number"]);
            $cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
            $cash_register_object->__set("cr_amount", $row["cr_amount"]);
            $cash_register_object->__set("timestamp", $row["timestamp"]);
            array_push($cash_register_object_arr, $cash_register_object);
        }
        return $cash_register_object_arr;
    }

    public function entryBySite($site_id) {

       /*  $query = "SELECT sites.*,consumers.*,cash_register.*
                 FROM cash_register
                 INNER JOIN consumers
                 ON consumers.consumer_id=cash_register.consumer_id
                 INNER JOIN sites
                 ON sites.site_id=consumers.site_id
                 WHERE sites.site_id=" . $site_id . " and cr_status <> 'INACTIVE' AND (cr_entry_type = 'CREDIT' OR cr_entry_type = 'ACTIVATION' OR cr_entry_type = 'REFUND')    
                 "; */

		$query="call searchEntryBySiteId($site_id)";
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister();
            $cash_register_object->__set("cr_id", $row["cr_id"]);
            $cash_register_object->__set("consumer_id", $row["consumer_id"]);
            $cash_register_object->__set("user_id", $row["user_id"]);
            $cash_register_object->__set("receipt_number", $row["receipt_number"]);
            $cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
            $cash_register_object->__set("cr_amount", $row["cr_amount"]);
            $cash_register_object->__set("timestamp", $row["timestamp"]);
            array_push($cash_register_object_arr, $cash_register_object);
        }
        return $cash_register_object_arr;
    }

    public function monthCredits($month, $consumer_id = NULL) {
        $query = "SELECT *, SUM(cr_amount) AS `total_amount` FROM `cash_register` WHERE MONTH(`timestamp`) = '" . $month . "' AND `cr_entry_type` = 'CREDIT' and cr_status <> 'INACTIVE'";
        if ($consumer_id != NULL) {
            $query .= "AND `consumer_id`= '" . $consumer_id . "'";
        }
        
      //  echo $query; exit;
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();

        if ($result[0]['cr_id'] == "" || $result[0]['cr_id'] == "NULL") {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister();
            $cash_register_object->__set("cr_id", $row["cr_id"]);
            $cash_register_object->__set("consumer_id", $row["consumer_id"]);
            $cash_register_object->__set("user_id", $row["user_id"]);
            $cash_register_object->__set("receipt_number", $row["receipt_number"]);
            $cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
            $cash_register_object->__set("cr_amount", $row["cr_amount"]);
            $cash_register_object->__set("timestamp", $row["timestamp"]);
            $cash_register_object->__set("total_amount", $row["total_amount"]);
            array_push($cash_register_object_arr, $cash_register_object);
        }
        return $cash_register_object_arr;
    }

    
    public function calculateAmount($type) {
        
        $query = "SELECT *, SUM(`cr_amount`) AS `total_amount` FROM `cash_register` WHERE `cr_entry_type` = '".$type."' and cr_status <> 'INACTIVE'";
        
        //echo $query; exit;
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();

        if ($result[0]['cr_id'] == "" || $result[0]['cr_id'] == "NULL") {
            return false;
        }
        $cash_register_object_arr = array();
        foreach ($result as $row) {
            $cash_register_object = new Application_Model_CashRegister();
            $cash_register_object->__set("cr_id", $row["cr_id"]);
            $cash_register_object->__set("consumer_id", $row["consumer_id"]);
            $cash_register_object->__set("user_id", $row["user_id"]);
            $cash_register_object->__set("receipt_number", $row["receipt_number"]);
            $cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
            $cash_register_object->__set("cr_amount", $row["cr_amount"]);
            $cash_register_object->__set("timestamp", $row["timestamp"]);
            $cash_register_object->__set("total_amount", $row["total_amount"]);
            array_push($cash_register_object_arr, $cash_register_object);
        }
        return $cash_register_object_arr;
    }

    public function getCosumersCreditAmount($consumers) {
        
        $date = new Zend_Date();
        $date->sub("1", "MM");
        $month = $date->toString("MM");

        $total_amount = 0;
        foreach ($consumers as $consumer) {

            $consumer_id = $consumer->__get("consumer_id");
            $result = $this->monthCredits($month, $consumer_id);
            if($result){
                $total_amount += $result[0]->total_amount;
            }
            
        }
        
        return $total_amount;
    }
	public function getTotalCollectedByMonth($curr_date=NULL,$site_id=NULL,$entry_type=NULL,$last_date=NULL,$state_id=NULL,$role_sites_id=NULL,$cluster_id=NULL,$entry_status=NULL,$segment=NULL,$consumer_type=NULL,$graph=null)
    {
    	
		$where_con = " where cr_status != 'INACTIVE'";
      
        if($curr_date!=null){
        	$where_con .=" and date(timestamp) <= '".$curr_date."' AND  date(timestamp)>= '".$last_date."'";
		}
		
		$join="INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id inner join sites on consumers.site_id=sites.site_id ";
		
		if($site_id !=NULL){
			$where_con .=" and consumers.site_id in (".$site_id.") and consumers.site_id!=1";
		}else if ($state_id !=NULL){
			$where_con .=" and sites.state_id in (".$state_id.") and consumers.site_id in (".implode(",", $role_sites_id).")  and consumers.site_id!=1";
		}else{
			$where_con .=" and consumers.site_id in (".implode(",", $role_sites_id).")  and consumers.site_id!=1";
		}
		 
        if($entry_type!=null) {
            $where_con.=" and cr_entry_type='".$entry_type."'";
		    if($graph==NULL){
				if($entry_type=='DEBIT'){
					$where_con.=" and entry_status!='SCHEME' ";
				}elseif($entry_type=='CREDIT'){
					$where_con.=" and entry_status!='SCHEME' ";
				}
		    }
        }
        
        if($entry_status!=null) {
			$where_con.=" and entry_status in (".implode(",",$entry_status).")";
		}
		
	    $segments="";$consumer_types="";
		if($segment!=NULL || $consumer_type!=NULL){
			$join.=" inner join consumer_packages cp on consumers.consumer_id=cp.consumer_id inner join packages p on consumers.package_id=p.package_id ";
		}
		
		if($segment!=NULL){
			$where_con.=" and consumers.type_of_me in (".$segment.")";
		}
		
		if($consumer_type!=NULL){
			$where_con.=" and p.light_load in (".$consumer_type.")";
		}
         $query  = "SELECT SUM(cr_amount) AS total FROM cash_register ".$join.$where_con." ORDER BY timestamp DESC";
         //echo   $query;exit; 
      //   $query="call getTotalCollectedByMonth('$curr_date','$last_date','$site_id','$state_id','$entry_type','$role_id',$cluster_id,"."'$entry_status'".")"; 
         
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
    
        if (count($result) == 0) {
            return false;
        }
        
        return $result[0]["total"];
    }
    
    public function getTotalCollectedActivation($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$date_string=NULL,$year=NULL)
    {
        
      /*if($site_id!=NULL)
        {
             $site =" INNER JOIN consumers "
                . "ON consumers.consumer_id=cash_register.consumer_id Where consumers.site_id='".$site_id."' and cr_entry_type='ACTIVATION' AND cr_status <> 'INACTIVE'";
        } else {
        
        		$site=" inner join consumers on consumers.consumer_id=cash_register.consumer_id inner Join sites on 
						sites.site_id=consumers.site_id "
        			. " where sites.site_id in (".implode(",", $role_sites_id).") "
        			. " and cr_entry_type='ACTIVATION' AND cr_status <> 'INACTIVE'";
        	
         }
        
        $query = "SELECT SUM(cr_amount) AS total FROM cash_register $site"; */
    	$role_id=implode(",", $role_sites_id);
    	if($site_id==""){
    		$site_id='null';
    	}
    	if($state_id==""){
    		$state_id='null';
    	}
    	if($date_string==""){
    		$date_string='null';
    	}
    	if($year==""){
    		$year='null';
    	}
    	$query="call getTotalCollectedActivation('$site_id','$state_id','$role_id','$date_string','$year')"; 
    	//echo $query;
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        return $result[0]["total"];
    }
    public function getLastestEntryByConsumerId($consumer_id)
    {
        $query = "SELECT MAX(cr_id) AS recent_id FROM cash_register WHERE consumer_id=".$consumer_id;
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        $cr_id =  $result[0]["recent_id"];
        $cr = $this->getCashRegisterById($cr_id);
        return $cr;
    }
    public function getLastestCHGEntryByConsumerId($consumer_id)
    {
        $query = "SELECT cr_id AS recent_id FROM cash_register WHERE cr_entry_type='CREDIT' AND consumer_id=".$consumer_id." AND cr_status <> 'INACTIVE' ORDER BY timestamp DESC LIMIT 1";
        //echo $query;exit;
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        $cr_id =  $result[0]["recent_id"];
        $cr = $this->getCashRegisterById($cr_id);
        return $cr;
    }
    public function getMonthlyOverDueByConsumerId($consumer_id,$date_string)
    {
        
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type='DEBIT' AND consumer_id=".$consumer_id." AND cr_status <> 'INACTIVE' AND timestamp<STR_TO_DATE('".$date_string."','%d/%m/%Y')";
        //echo $query;exit;
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"];
        $query = "SELECT SUM(cr_amount) As credit_amount from cash_register WHERE cr_entry_type='CREDIT' AND consumer_id=".$consumer_id." AND cr_status <> 'INACTIVE' AND timestamp<STR_TO_DATE('".$date_string."','%d/%m/%Y')";
        $result = $stmt->fetchRow($query);
        $credit_amount = $result["credit_amount"];
        $overdue = $debit_amount-$credit_amount;
        return $overdue;
    }
    public function getBalanceOTPByConsumerId($consumer_id)
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $consumer = $consumersMapper->getConsumerById($consumer_id);
        $total_otp = $consumer->__get("consumer_act_charge");
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS activation_amount from cash_register WHERE cr_entry_type='ACTIVATION' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id;
        $result = $stmt->fetchRow($query);
        $activation_amount = $result["activation_amount"];
        $balance_otp = $total_otp-$activation_amount;
        return $balance_otp;
    }
    public function getMonthlyDueByConsumerId($consumer_id,$month,$year,$package_id=null,$entry_status=null,$collection_sheet=NULL,$ispost_paid=NULL,$day=NULL,$activity=NULL)
    {
    	$cond="";
    	if($month != null )
    	{
    		$cond="AND MONTH(timestamp)='".$month."'";
    	}
    	if($year !=null)
    	{
    		$cond .=" AND YEAR(timestamp)='".$year."'";
    	}
    	$package="";
    	if($package_id!=NULL){
    		$package=" and cr.package_id=".$package_id;
    	}
		
		$entrys_status="";
    	if($entry_status!=NULL){
    		$entrys_status=" and entry_status='".$entry_status."'";
    	}
		$join="";$group="";
		 
		if($collection_sheet!=NULL){
			$join=" inner join packages p on p.package_id=cr.package_id";
			if($ispost_paid==0){
				$cond .=" and (p.is_postpaid=0)";
			}elseif($ispost_paid==1){
				$cond .=" and (p.is_postpaid=2)";
			}else{
				$cond .=" and (p.is_postpaid!=0)";
			}
			if($ispost_paid==1){
				if($day!=NULL && $activity!=NULL){
					$cond .=" AND day(timestamp)!='".$day."'";
				}else{
					$cond .=" AND day(timestamp)='".$day."'";
				}
			}
		}
			
		
		
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register cr ".$join." WHERE cr_entry_type='DEBIT' ".$entrys_status."  
					and entry_status not in ('SCHEME','ACT','OTHERS') AND cr_status != 'INACTIVE'  AND cr.consumer_id=".$consumer_id." ".$cond.$package;
		 
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"];
        if($debit_amount==NULL)
        {
            $debit_amount = 0;
        }
        return $debit_amount;
    }
    public function getMonthlyInByConsumerId($consumer_id,$month,$year,$package_id=null,$entry_status=null)
    {
    	$cond="";
    if($month != null )
    	{
    		$cond="AND MONTH(timestamp)='".$month."'";
    	}
    	if($year !=null)
    	{
    		$cond .=" AND YEAR(timestamp)='".$year."'";
    	}
    	
    	$package="";
    	if($package_id!=NULL){
    		$package=" and package_id=".$package_id;
    	}
    	$entrys_status="";
    	if($entry_status!=NULL){
    		$entrys_status=" and entry_status='".$entry_status."'";
    	}
        $stmt = $this->_db_table->getAdapter();  
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type in ('CREDIT','DISCOUNT') ".$entrys_status." and entry_status  not in ('SCHEME','SD','OTHERS')  AND consumer_id=".$consumer_id." AND cr_status != 'INACTIVE' "." ".$cond.$package;
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"]; 
        if($debit_amount==NULL)
        {
            $debit_amount = 0;
        }
        return $debit_amount;
    }
    public function getOTPInByConsumerId($consumer_id,$month=NULL,$year=NULL)
    {
    	$cond="";
		if($month != null )
    	{
    		$cond="AND MONTH(timestamp)='".$month."'";
    	}
    	if($year !=null)
    	{
    		$cond .=" AND YEAR(timestamp)='".$year."'";
    	}
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type='ACTIVATION' AND consumer_id=".$consumer_id." AND cr_status <> 'INACTIVE' "." ".$cond;
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"];
        if($debit_amount==NULL)
        {
            $debit_amount = 0;
        }
        return $debit_amount;
    }
    public function getOTPDueByConsumerId($consumer_id,$month,$year)
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $consumer = $consumersMapper->getConsumerById($consumer_id);
        $act_date = explode("-", $consumer->__get("consumer_act_date"));
        if(count($act_date)!=2)
        {
            return 0;
        }
        $act_month = $act_date[1];
        $act_year = $act_date[0];
        $act_amount = $consumer->__get("consumer_act_charge");
        if($act_month==$month and $act_year==$year)
        {
            return 0;
        }
        $date_string = "01/".$month."/".$year;
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type='ACTIVATION' AND consumer_id=".$consumer_id." and cr_status <> 'INACTIVE' AND timestamp<STR_TO_DATE('".$date_string."','%d/%m/%Y')";
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"];
        if($debit_amount==NULL)
        {
            return 0;
        }
        
        $left_amount = $act_amount-$debit_amount;
        if($left_amount>100)
        {
            $return = 100;
        }
        else
        {
            $return = $left_amount;
        }
        return $return;
    }
    public function getTotalOTPDueByConsumerId($consumer_id,$month,$year)
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $consumer = $consumersMapper->getConsumerById($consumer_id);
        $act_amount = $consumer->__get("consumer_act_charge");
        $date_string = "01/".$month."/".$year;
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type='ACTIVATION' AND consumer_id=".$consumer_id." and cr_status <> 'INACTIVE' AND timestamp<STR_TO_DATE('".$date_string."','%d/%m/%Y')";
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"];
        if($debit_amount==NULL)
        {
            $debit_amount =  0;
        }
        $left_amount = $act_amount-$debit_amount;
        return $left_amount;
    }
    public function getOTPDueBySiteId($site_id,$month,$year)
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $consumers = $consumersMapper->getConsumersByColumnValue("site_id", $site_id);
		
        $total  = 0;
        if($consumers)
        {
            foreach($consumers as $consumer)
            {
                $consumer_id = $consumer->__get("consumer_id");
                $total_otp_due = $this->getTotalOTPDueByConsumerId($consumer_id, $month, $year);
                $otp_in = $this->getOTPInByConsumerId($consumer_id, $month, $year);
                $otp_carry_forward = $total_otp_due-$otp_in;
                $otp_due = 0;
                if($total_otp_due==$otp_carry_forward)
                {
                    if($total_otp_due<100)
                    {
                        $otp_due = $total_otp_due;
                    }
                    else
                    {
                        $otp_due = 100;
                    }
                }
              
                $total += $otp_due;
                
            }
        }
        return $total;
    }
    public function getOutstandingBySiteId($site_id,$date)
    {
        
        $date_string = $date;
        $stmt = $this->_db_table->getAdapter();
        
        /* $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register "
                . "INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id "
                . "WHERE cr_entry_type='DEBIT' AND consumers.site_id=".$site_id." and cr_status <> 'INACTIVE' AND timestamp<STR_TO_DATE('".$date_string."','%d/%m/%Y')"; */
       
         $query="call getDebitOutstandingBySiteId($site_id,'$date_string')";
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"];
        /* $query = "SELECT SUM(cr_amount) AS credit_amount from cash_register "
                . "INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id "
                . "WHERE cr_entry_type='CREDIT' AND consumers.site_id=".$site_id." and cr_status <> 'INACTIVE' AND timestamp<STR_TO_DATE('".$date_string."','%d/%m/%Y')";
      */ 
		$query="call getCreditOutstandingBySiteId($site_id,'$date_string')";
        $result = $stmt->fetchRow($query);
        $credit_amount = $result["credit_amount"];
        $overdue = $debit_amount-$credit_amount;
        return $overdue;
    }
    public function getTotalOutstanding($date_string)
    {
        $stmt = $this->_db_table->getAdapter();
        /* $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register "
                . "INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id"
                . " WHERE cr_entry_type='DEBIT' AND consumers.consumer_status!='banned' and cr_status <> 'INACTIVE' AND timestamp<STR_TO_DATE('".$date_string."','%d/%m/%Y')";
 		*/
        $query="call getDebitTotalOutstanding('$date_string')";
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"];
       /*  $query = "SELECT SUM(cr_amount) AS credit_amount from cash_register "
                . "INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id"
                . " WHERE cr_entry_type='CREDIT' AND consumers.consumer_status!='banned' and cr_status <> 'INACTIVE' AND timestamp<STR_TO_DATE('".$date_string."','%d/%m/%Y')";
       */ 
         $query="call getCreditTotalOutstanding('$date_string')";
        $result = $stmt->fetchRow($query);
        $credit_amount = $result["credit_amount"];
        $overdue = $debit_amount-$credit_amount;
        return $overdue;
    }
    public function getOutstandingAmount($site_id=false,$role_sites_id=NULL)
    {
    	if($site_id==""){$site_id='null';}
    	
       /*if($site_id==""){
            $site = " AND consumers.site_id=".$site_id."";
         }
         else{
             $site="";
         }
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register "
                . "INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id "
                . "WHERE cr_entry_type='DEBIT' $site AND consumers.consumer_status!='banned' and  cr_status <> 'INACTIVE'";
        */
    	$stmt = $this->_db_table->getAdapter();
    	$query="call getDebitOutstandingAmount($site_id)";
        $result = $stmt->fetchRow($query);
        $debit_amount = $result["debit_amount"];
        /*$query = "SELECT SUM(cr_amount) AS credit_amount from cash_register "
                . "INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id "
                . "WHERE cr_entry_type='CREDIT' $site AND consumers.consumer_status!='banned' and cr_status <> 'INACTIVE'";
        */  
        $query="call getCreditOutstandingAmount($site_id)";
        $result = $stmt->fetchRow($query);
        $credit_amount = $result["credit_amount"];
        $overdue = $debit_amount-$credit_amount;
        $amount=array(
            "credit"=>$credit_amount,
            "debit"=>$debit_amount,
                );
        return $amount;
    }
    public function getDuplicateEntry($site_id=false,$startDate=null,$endDate=null,$duplicate=false,$pending_delete=false)
    {
    	if($site_id){
    		$site = " AND co.site_id=".$site_id."";
    	}
    	else{
    		$site="";
    	}
    	$startCond="";
    	$endCond="";
    	if($startDate != null)
    	{
    		$startCond="AND date(timestamp) >='$startDate'";
    	}
    	
    	if($endDate != null)
    	{
    		$endCond="AND  date(timestamp)<='$endDate'";
    	}
    	$duplicateCond="AND cr.cr_status <> 'INACTIVE'";
		$roleSession = new Zend_Session_Namespace('roles');
    	$role_sites_id=$roleSession->site_id;
    	$site_ids=implode(',',$role_sites_id);
		if($site=="")
		{
			$site .= " AND co.site_id in (".$site_ids.")";
		}
		
    	if($duplicate){
		
    		$duplicateCond = " AND (cr.cr_status <> 'INACTIVE' and  cr.cr_status <> 'NOTDUPLICATE' and  cr.cr_status <> 'PENDING')";
    	}
    	if($pending_delete){
    		$duplicateCond = " AND (cr.cr_status <> 'INACTIVE' and  cr.cr_status <> 'NOTDUPLICATE' and  cr.cr_status <> 'ACTIVE')";
    	}
    	//echo $site."jhsdbjc";
    	$createTempTableQuery="DROP TABLE IF EXISTS cash_enrty;DROP TABLE IF EXISTS activation_count;DROP TABLE IF EXISTS credit_count; CREATE TABLE cash_enrty AS ( SELECT cr.* FROM cash_register cr INNER JOIN consumers co ON cr.consumer_id=co.consumer_id "
    	    	." WHERE (cr.cr_entry_type='CREDIT' or cr_entry_type='ACTIVATION') $duplicateCond $startCond  $endCond $site  );"
 				."CREATE TABLE credit_count AS(select * from(SELECT count(cr_id) as cr_count,month(timestamp) as months,year(timestamp) as years,consumer_id from cash_enrty"
 				." WHERE cr_entry_type='CREDIT' group by cr_amount,day(timestamp),month(timestamp),year(timestamp),consumer_id) as tab_cr_count where cr_count >1);"
 				." CREATE TABLE activation_count AS(select * from(SELECT count(cr_id) as act_count,month(timestamp) as months,year(timestamp)as years,consumer_id from cash_enrty" 
				." WHERE cr_entry_type='ACTIVATION' group by cr_amount,day(timestamp),month(timestamp),year(timestamp),consumer_id) as tab_act_count where act_count>1);";
    	/*
    	$selectCashEntry="SELECT ce.* FROM cash_enrty ce INNER JOIN credit_count cc on ce.consumer_id =cc.consumer_id WHERE month(ce.timestamp)=cc.months and year(ce.timestamp)=cc.years and ce.cr_entry_type='CREDIT'
							UNION
							SELECT ce.* FROM cash_enrty ce INNER JOIN activation_count cc on ce.consumer_id =cc.consumer_id WHERE month(ce.timestamp)=cc.months and year(ce.timestamp)=cc.years and ce.cr_entry_type='ACTIVATION' 
							ORDER BY consumer_id,timestamp desc,cr_entry_type;"; */
    	
    	
    	$selectCashEntry="call selectCashEntry()";
    	
    	$stmt= $this->_db_table->getAdapter();
    	$stmt->getConnection()->prepare($createTempTableQuery)->execute();
    	$result = $stmt->fetchAll($selectCashEntry);

    	$cash_register_object_arr = array();
    	foreach ($result as $row) {
    		$cash_register_object = new Application_Model_CashRegister();
    		$cash_register_object->__set("cr_id", $row["cr_id"]);
    		$cash_register_object->__set("consumer_id", $row["consumer_id"]);
    		$cash_register_object->__set("user_id", $row["user_id"]);
    		$cash_register_object->__set("receipt_number", $row["receipt_number"]);
    		$cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
    		$cash_register_object->__set("cr_amount", $row["cr_amount"]);
    		$cash_register_object->__set("timestamp", $row["timestamp"]);
    		$cash_register_object->__set("transaction_id", $row["transaction_id"]);
    		$cash_register_object->__set("cr_status", $row["cr_status"]);
    		$cash_register_object->__set("user_val", $row["user_val"]);
			$cash_register_object->__set("entry_status", $row["entry_status"]);
			$cash_register_object->__set("transaction_type", $row["transaction_type"]);
    		array_push($cash_register_object_arr, $cash_register_object);
    	}
    	//echo $createTempTableQuery.$selectQuery.$dropTempTableQuery;
    	return $cash_register_object_arr;
    }

    
    public function getDefaulterCashAmounts($consumer_id,$month,$year)
    {
    	$date=$year."-".$month."-01";
    	
    	$query="call getDefaulterCashAmounts($consumer_id,'$date')";
    	
    	/* $query="select cr.consumer_id,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where cr_entry_type='DEBIT' and  date(cr1.timestamp)<= date(last_day('$date' - INTERVAL 3 MONTH)) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as debit_amount_history,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where cr_entry_type='CREDIT' and  date(cr1.timestamp)<= date(last_day('$date' - INTERVAL 3 MONTH)) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as credit_amount_history,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where cr_entry_type='ACTIVATION' and  date(cr1.timestamp)<= date(last_day('$date' - INTERVAL 3 MONTH)) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as activation_amount_history,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where cr_entry_type='DEBIT' and date(cr1.timestamp)>= date(last_day('$date' - INTERVAL 3 MONTH) + INTERVAL 1 day)  and  date(cr1.timestamp)<= date(last_day('$date' - INTERVAL 2 MONTH)) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as monthly_amount_prev2,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where (cr_entry_type='CREDIT' or cr_entry_type='ACTIVATION') and date(cr1.timestamp)>= date(last_day('$date' - INTERVAL 3 MONTH) + INTERVAL 1 day)  and  date(cr1.timestamp)<= date(last_day('$date' - INTERVAL 2 MONTH)) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as total_paid_prev2,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where cr_entry_type='DEBIT' and date(cr1.timestamp)>= date(last_day('$date' - INTERVAL 2 MONTH) + INTERVAL 1 day)  and  date(cr1.timestamp)<= date(last_day('$date' - INTERVAL 1 MONTH)) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as monthly_amount_prev1,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where (cr_entry_type='CREDIT' or cr_entry_type='ACTIVATION') and date(cr1.timestamp)>= date(last_day('$date' - INTERVAL 2 MONTH) + INTERVAL 1 day)  and  date(cr1.timestamp)<= date(last_day('$date' - INTERVAL 1 MONTH)) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as total_paid_prev1,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where cr_entry_type='DEBIT' and date(cr1.timestamp)>= date('$date')  and  date(cr1.timestamp)<= date(last_day('$date')) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as monthly_amount_current,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where (cr_entry_type='CREDIT' or cr_entry_type='ACTIVATION') and date(cr1.timestamp)>= date('$date')  and  date(cr1.timestamp)<= date(last_day('$date')) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as total_paid_current,
    	(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where  cr_entry_type='ACTIVATION' and date(cr1.timestamp)>= date(last_day('$date' - INTERVAL 3 MONTH) + INTERVAL 1 day)  and  date(cr1.timestamp)<= date(last_day('$date')) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
    	as otp_current,
		(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where cr_entry_type='DEBIT' and date(cr1.timestamp)<= date(last_day(Now())) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
		as debit_amount_all,
 		(select COALESCE(sum(cr1.cr_amount),0) from cash_register cr1 where cr_entry_type='CREDIT' and date(cr1.timestamp)<= date(last_day(Now())) and cr1.consumer_id = cr.consumer_id and cr1.cr_status <> 'INACTIVE')
  		as credit_amount_all
    	from cash_register cr WHERE cr.consumer_id=$consumer_id and  cr.cr_status <> 'INACTIVE'  group by consumer_id order by consumer_id;";
     */
    	
    	 $stmt= $this->_db_table->getAdapter();
    	//$result=$stmt->getConnection()->prepare($query)->fetchAll();
    	$result = $stmt->fetchAll($query); 
        //print_r($result);
    
    	$cash_register_object_arr = array();
    	foreach ($result as $row) {
    		$cash_register_object = new Application_Model_CashRegister();
    		$cash_register_object->__set("consumer_id", $row["consumer_id"]);
    		$cash_register_object->__set("debit_amount_history", $row["debit_amount_history"]);
    		$cash_register_object->__set("credit_amount_history", $row["credit_amount_history"]);
    		$cash_register_object->__set("activation_amount_history", $row["activation_amount_history"]);
    		$cash_register_object->__set("monthly_amount_prev2", $row["monthly_amount_prev2"]);
    		$cash_register_object->__set("total_paid_prev2", $row["total_paid_prev2"]);
    		$cash_register_object->__set("monthly_amount_prev1", $row["monthly_amount_prev1"]);
    		$cash_register_object->__set("total_paid_prev1", $row["total_paid_prev1"]);
    		$cash_register_object->__set("monthly_amount_current", $row["monthly_amount_current"]);
    		$cash_register_object->__set("total_paid_current", $row["total_paid_current"]);
    		$cash_register_object->__set("otp_current", $row["otp_current"]);
    		$cash_register_object->__set("carry_forward_amount", $row["debit_amount_all"] -$row["credit_amount_all"]);
    		array_push($cash_register_object_arr, $cash_register_object);
    	}
    	//echo $createTempTableQuery.$selectQuery.$dropTempTableQuery;
    	return $cash_register_object_arr;
    }
    

     public function updateStatus($cr_id,$status,$mode,$remark=null,$pend_table=false,$user_id=NULL) {
	
		$date = new Zend_Date(); 
        $date->setTimezone("Asia/Calcutta");
        $approved_date = $date->toString("yyyy-MM-dd HH:mm:ss");
			
    	$data = array(
    			'cr_status' => $status,
    			'delete_type'=>$mode,
    			'remark'=>$remark,
				'approved_by'=>$user_id,
				'approved_date' => $approved_date
    	);
    	$where = "cr_id = " . $cr_id;
    	
    	
    		$result = $this->_db_table->update($data, $where);
    
    	if (count($result) == 0) {
    		return false;
    	} else {
    		if($pend_table!=true){
			$cashMapper=$this->getCashRegisterById($cr_id);
    		$consumer_id=$cashMapper->__get("consumer_id");
    		$cr_entry_type=$cashMapper->__get("cr_entry_type");
    		$cr_amount=0;
			
			$date = new Zend_Date($cashMapper->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
			//$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
           //$trans_date=date_parse_from_format("Y-m-d",$cashMapper->__get("timestamp"));
    		 $day=$date->toString("dd");//new Zend_Date($cashMapper->__get("timestamp"), "dd");//$trans_date["day"];
    		 $month= $date->toString("MM");//$trans_date["month"];
    		 $year= $date->toString("YYYY");//$trans_date["year"];
    		
    		 $current_date=$year."-".$month."-".$day;
    		 
			
    		$selectEntry="call insertDefaulterfromcashRegister($consumer_id,'$current_date','$cr_entry_type',$cr_amount)";
    		$stmt = $this->_db_table->getAdapter()->query($selectEntry);
    		$entry_result=$stmt->execute();
    		}
    		return true;
    	}
    }

    public function getMismatchedValue($consumer_id,$site_id=false,$startDate, $endDate)
    {
    	/* if($site_id){
    		$site = " AND c.site_id=".$site_id."";
    	}
    	else{
    		$site="";
    	} */
    	$query="call getMismatchedValue($consumer_id,$site_id,'$startDate','$endDate')";
        /* $query="select cr.cr_id,cr.consumer_id,c.consumer_name,c.consumer_connection_id,p.package_cost,cr.cr_amount,cr.cr_entry_type,cr.timestamp
 					from cash_register cr inner join consumers c on cr.consumer_id =c.consumer_id
					inner join packages p on c.package_id=p.package_id 
					where ((p.package_cost <> cr.cr_amount and cr.cr_entry_type='CREDIT')
					or((cr.cr_amount <>1000 and  cr.cr_amount <>600 and cr.cr_amount <>100) and cr.cr_entry_type='ACTIVATION'))
					and cr.consumer_id=$consumer_id and date(timestamp) >='$startDate' and and cr.cr_status <> 'INACTIVE'  AND  date(timestamp)<='$endDate' $site
					order by cr.consumer_id, timestamp desc;" */
    
    	$stmt= $this->_db_table->getAdapter();
    	//$result=$stmt->getConnection()->prepare($query)->fetchAll();
    	$result = $stmt->fetchAll($query);
    	$cash_register_object_arr = array();
    	foreach ($result as $row) {
    		$cash_register_object = new Application_Model_CashRegister();
    		$cash_register_object->__set("consumer_id", $row["consumer_id"]);
    		$cash_register_object->__set("consumer_name", $row["consumer_name"]);
    		$cash_register_object->__set("consumer_connection_id", $row["consumer_connection_id"]);
    		$cash_register_object->__set("package_cost", $row["package_cost"]);
    		$cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
    		$cash_register_object->__set("cr_amount", $row["cr_amount"]);
    		$cash_register_object->__set("timestamp", $row["timestamp"]);
    		array_push($cash_register_object_arr, $cash_register_object);
    	}
    	//echo $createTempTableQuery.$selectQuery.$dropTempTableQuery;
    	return $cash_register_object_arr;
    }
    
    public function getAllEntryByConsumerId($consumer_id) {
    	$where = array("consumer_id = ?" => $consumer_id);
    	$result = $this->_db_table->fetchAll($where, array('timestamp ASC'));
    	
    	if (count($result) == 0) {
    		return false;
    	}
    	$cash_register_object_arr = array();
    	foreach ($result as $row) {
    		$cash_register_object = new Application_Model_CashRegister($row);
    		array_push($cash_register_object_arr, $cash_register_object);
    	}
    	return $cash_register_object_arr;
    }
    
    public function getAllCashRegisterByStatus($is_rf=false) {
    	
    	if(!$is_rf){
    	$query = "SELECT cr.* from cash_register cr INNER JOIN consumers c on c.consumer_id= cr.consumer_id INNER JOIN sites s
    	 on c.site_id=s.site_id where s.site_status!='Disable' and cr.cr_status!='INACTIVE'";
    	}
    	else {
    		$zendDate = new Zend_Date();
    		$zendDate->setTimezone("Asia/Calcutta");
    		$timestamp = $zendDate->toString("yyyy-MM-dd");
    	$query = "SELECT cr.* from cash_register cr INNER JOIN consumers c on c.consumer_id= cr.consumer_id INNER JOIN sites s
    		on c.site_id=s.site_id where s.site_status!='Disable' and cr.cr_status!='INACTIVE'
    		and cr.timestamp between DATE_SUB(timestamp,interval 2 YEAR) and timestamp";
    	}
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$resultData = $stmt->fetchAll();
    	$cash_register_object_arr = array();
    	foreach ($resultData as $row) {
    		$cash_register_object = new Application_Model_CashRegister();
    		$cash_register_object->__set("cr_id", $row["cr_id"]);
    		$cash_register_object->__set("consumer_id", $row["consumer_id"]);
    		$cash_register_object->__set("user_id", $row["user_id"]);
    		$cash_register_object->__set("receipt_number", $row["receipt_number"]);
    		$cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
    		$cash_register_object->__set("cr_amount", $row["cr_amount"]);
    		$cash_register_object->__set("timestamp", $row["timestamp"]);
    		$cash_register_object->__set("transaction_id", $row["transaction_id"]);
    		$cash_register_object->__set("cr_status", $row["cr_status"]);
    		array_push($cash_register_object_arr, $cash_register_object);
    	}
    	return $cash_register_object_arr;
    }
    public function getDebitEntryByConsumerId($consumer_id,$year,$month) {
    	$status='DEBIT';
    	$where = array("consumer_id = ?" => $consumer_id,
    			 "cr_entry_type = ?" =>$status,
    			"month(timestamp)= ?" =>$month,
    			"year(timestamp)= ?"=>$year
    	);

    	$stmt = $this->_db_table->getAdapter();
          $query = "SELECT * from cash_register WHERE cr_entry_type='DEBIT' and entry_status='CHG' and  cr_status!='INACTIVE' AND consumer_id =$consumer_id and month(timestamp)=$month and year(timestamp)=$year and mtr_entry !='1' order by timestamp DESC";

    	$result = $stmt->fetchAll($query);
    	  if (count($result) == 0) {
    		return false;
    	}   
    	$cash_register_object_arr = array();
    	foreach ($result as $row) {
    		$cash_register_object = new Application_Model_CashRegister();
    		$cash_register_object->__set("cr_id", $row["cr_id"]);
    		$cash_register_object->__set("consumer_id", $row["consumer_id"]);
    		$cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
    		$cash_register_object->__set("cr_amount", $row["cr_amount"]);
    		$cash_register_object->__set("timestamp", $row["timestamp"]);
    		array_push($cash_register_object_arr, $cash_register_object);
    	}
    	return $cash_register_object_arr;
    }
    public function updateCashRegisterAmount($calcuatedamount,$cr_id,$status=null,$amt_prompt=null)
    {
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$timestamp = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
       	$timestamps = $zendDate->toString("ddMMYYss".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9));
    	$tcr_amount = str_pad($calcuatedamount, 4, "0", STR_PAD_LEFT);
    	$transaction_id = $timestamps . "-" . $tcr_amount;
    	if($status!=null){
			$cron_entry=$status;
		}else{
			$cron_entry=null;
		}
		if($amt_prompt!=null){ 
			$amt_prompts=$amt_prompt;
		}else{
			$amt_prompts=null;
		}
    		$data = array(
    				'cr_amount' => $calcuatedamount,
    				'timestamp'=>$timestamp,
    				'transaction_id'=>$transaction_id,
					'cron_entry'=>$cron_entry,
					'amt_prompt'=>$amt_prompts
    		);
    
    		$where = "cr_id = " .$cr_id;
    
         $result = $this->_db_table->update($data,$where);
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
		    $cashMapper=$this->getCashRegisterById($cr_id);
    		$consumer_id=$cashMapper->__get("consumer_id");
    		$cr_entry_type=$cashMapper->__get("cr_entry_type");
    		$cr_amount=$calcuatedamount;
    		$trans_date=date_parse_from_format("Y-m-d", $timestamp);
    		$day=$trans_date["day"];
    		$month= $trans_date["month"];
    		$year= $trans_date["year"];
    		
    		$current_date=$year."-".$month."-".$day;
        	 
    		$selectEntry="call insertDefaulterfromcashRegister($consumer_id,'$current_date','$cr_entry_type',$cr_amount)";
    		
    		$stmt = $this->_db_table->getAdapter()->query($selectEntry);
    		$entry_result=$stmt->execute();

    		return true;
    	}
    }

  
  public function pendingNotifications($mode) {
  	
  	$roleSession = new Zend_Session_Namespace('roles');
  	$role_sites_id=$roleSession->site_id;
  	$site_id=implode(',',$role_sites_id);
	
  	$stmt = $this->_db_table->getAdapter();
  	$result =null;
	
  	if($mode=='Entries')
  	{
  		$year=date("Y");
  		$mon=($year==date("Y"))?date("n"):12;
  		$month=$mon;
  		
  		$cycle=3;
  		$cash_data=array();
  		while($cycle)
  		{
  			$cycle--;
  			$consumer_array_data=array();
  			$startDate=$year."-".$month."-01";
  			$endDate=$year."-".$month."-".date('t',strtotime($startDate));
  				$data_cashs =$this->getDuplicateEntry(false,$startDate,$endDate,true); 
	  		
  			$year=($month==1)?$year-1:$year;
	  		$month=($month==1)?12:$month-1;
	  		foreach($data_cashs as $data_cash)
	  		{
	  		$cashDataArray=array(
				"cr_entry_type" => $data_cash->__get("cr_entry_type"),
				"cr_amount" => $data_cash->__get("cr_amount"),
				"transaction_id"=> $data_cash->__get("transaction_id"),
				"cr_id"=>$data_cash->__get("cr_id"),
				"cr_status"=>$data_cash->__get("cr_status")
			);
			array_push($cash_data, $cashDataArray);
	  		}
  		}
  		$result=$cash_data;
  	}elseif($mode=='Credit'){
  		$query = "SELECT cr.* from cash_register cr inner join consumers c on c.consumer_id=cr.consumer_id 
    			      where cr_status='NEW' and c.site_id in (".implode(',',$role_sites_id).") order by timestamp desc";
  		$stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        //echo count($result);exit;
        return  $result  ;
  	}else{
  		 $query = "SELECT cr.* from cash_register cr INNER JOIN consumers c on c.consumer_id= cr.consumer_id
  		 		 WHERE c.site_id in($site_id) AND cr_status='PENDING' AND delete_type='$mode' ";
  		$resultData = $stmt->fetchAll($query);
  		$cash_register_object_arr = array();
  		foreach ($resultData as $row) {
  			$cash_register_object = new Application_Model_CashRegister();
  			$cash_register_object->__set("cr_id", $row["cr_id"]);
  			$cash_register_object->__set("consumer_id", $row["consumer_id"]);
  			$cash_register_object->__set("user_id", $row["user_id"]);
  			$cash_register_object->__set("receipt_number", $row["receipt_number"]);
  			$cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
  			$cash_register_object->__set("cr_amount", $row["cr_amount"]);
  			$cash_register_object->__set("timestamp", $row["timestamp"]);
  			$cash_register_object->__set("transaction_id", $row["transaction_id"]);
  			$cash_register_object->__set("cr_status", $row["cr_status"]);
  			array_push($cash_register_object_arr, $cash_register_object);
  		}
 		$result=$cash_register_object_arr;
  	}

  	return $result;
  }
  public function lastPaymentMonth($consumer_id)
  {
  	$consumersMapper = new Application_Model_ConsumersMapper();
  	$consumer = $consumersMapper->getConsumerById($consumer_id);
  
  	$stmt = $this->_db_table->getAdapter();
  	$query = "SELECT timestamp from cash_register WHERE (cr_entry_type='CREDIT' or cr_entry_type='ACTIVATION') AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id." ORDER BY timestamp desc";
  	$result = $stmt->fetchRow($query);
  	$date = new Zend_Date($result["timestamp"]);
	$month = intval($date->toString("MM"));
	/*$day=intval($date->toString("dd"));
	if($day >=25)
	{
		$month=($month==12)?1:$month+1;
	}*/
  	
  
  	return $month;
  }
  
  
  public function getdefaultersEntry($currDate,$site_id)
  {
  	$selectEntry="call getDefaulters('$currDate',$site_id)";
  	$stmt= $this->_db_table->getAdapter();
  	$result = $stmt->fetchAll($selectEntry);
  	
  	return $result;
  }
  
  public function getdefaulterByMonth($currDate)
  {
  	$selectEntry="call getDefaulterByMonth('$currDate')";
  	$stmt= $this->_db_table->getAdapter();
  	$result = $stmt->fetchAll($selectEntry);
  	 
  	return $result;
  }
  
public function getPendingOtp($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$date_string=NULL,$year=NULL)
    {
    	$date_value="";
    	
 	if($site_id!=NULL && $site_id!="")
        {
             $site =" INNER JOIN consumers "
                . "ON consumers.consumer_id=cash_register.consumer_id  Where consumers.site_id='".$site_id."' and cr_entry_type='ACTIVATION' AND cr_status <> 'INACTIVE'";
        }elseif($state_id!=NULL){
    		$site=" inner join consumers on consumers.consumer_id=cash_register.consumer_id inner Join sites on 
						sites.site_id=consumers.site_id "
        			. " where sites.state_id = ".$state_id." and sites.site_status!='Disable'"
        			. " and cr_entry_type='ACTIVATION' AND cr_status <> 'INACTIVE'";
    	} else {
        
        		$site=" inner join consumers on consumers.consumer_id=cash_register.consumer_id inner Join sites on 
						sites.site_id=consumers.site_id "
        			. " where sites.site_id in (".implode(",", $role_sites_id).") and sites.site_status!='Disable'"
        			. " and cr_entry_type='ACTIVATION' AND cr_status <> 'INACTIVE'";
        	
         }
        if($date_string!='' && $date_string !=NULL)
			$date_value=" and MONTH(consumer_act_date)=".$date_string;

  		if($year !='')
			$date_value=$date_value." and YEAR(consumer_act_date)=".$year;
 		$query = "";
        $createQuery = "DROP TABLE IF EXISTS otpInCharge;
        CREATE TABLE otpInCharge  As (SELECT sum(cr_amount) as otpin,consumers.consumer_id FROM cash_register
         $site $date_value group by consumers.consumer_id)";
		
		$query="SELECT consumers.consumer_id,site_id,DATE_FORMAT(`consumer_act_date`,'%M %Y') as month_name,consumer_name,consumer_connection_id,consumer_act_charge,otpin,
		(select timestamp from cash_register where consumer_id= consumers.consumer_id and cr_entry_type='ACTIVATION' order by timestamp desc limit 1) as timestamp
  		FROM consumers 
 		inner join otpInCharge on consumers.consumer_id=otpInCharge.consumer_id 
		 where consumers.consumer_act_charge >otpInCharge.otpin; DROP TABLE otpInCharge;";
 		    	
    	$stmt= $this->_db_table->getAdapter();
    	$stmt->getConnection()->prepare($createQuery)->execute();
    	$result = $stmt->fetchAll($query);
       
		
        if (count($result) == 0) {
            return false;
        }
        return $result;
    }
  

   public function getCmNextMonthReport($site_id=NULL){
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
		$curDate=$zendDate->toString('yyyy-MM-dd HH:MM:ss');
		
		$selectEntry="call getCmNextMonth('$curDate',$site_id)";
		
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($selectEntry);
		 
		return $result;
    	
    }
    public function deleteDuplicateTransactionById($cr_id=NULL){
    
    	$query="select * from cash_register where cr_id in (".implode(",", $cr_id).") ";
        $stmt = $this->_db_table->getAdapter()->query($query);
        $cashMapper = $stmt->fetchAll();
       
    	$query ="update cash_register set cr_status='INACTIVE' where cr_id in (".implode(",", $cr_id).") ";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    	if($result)
    	{
			if (count( $cashMapper) == 0) {
				return false;
			}
		  foreach($cashMapper as $cash )
		   {
		    $consumer_id=$cash->__get("consumer_id");
    		$cr_entry_type=$cash->__get("cr_entry_type");
    		$cr_amount=0;
			
			$date = new Zend_Date($cash->__get("timestamp"), "yyyy-MM-dd HH:mm:ss");
    		 $day=$date->toString("dd");
    		 $month= $date->toString("MM");
    		 $year= $date->toString("YYYY");
    		 $current_date=$year."-".$month."-".$day;
    		 
			
    		$selectEntry="call insertDefaulterfromcashRegister($consumer_id,'$current_date','$cr_entry_type',$cr_amount)";
    		$stmt = $this->_db_table->getAdapter()->query($selectEntry);
    		$entry_result=$stmt->execute();
		   }
    		return true;
    	}
    }
	
	public function updateReceiptCountByTransactionId($receipt_number ,$receipt_count,$consumer_id ) {
		  
		$query ="update cash_register set receipt_count=".$receipt_count." where receipt_number='".$receipt_number."' and consumer_id=".$consumer_id."";
		 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		
    	if($result){ 
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    
    public function getCashRegisterDetailsByConsumerId($consumer_id=null,$entry=null,$limit=NULL){
		$entrys="";$group_by="";
		if($entry!=NULL){
			$entrys=" and cr_entry_type in('CREDIT','ACTIVATION') and entry_status not in ('SCHEME')";
			$group_by=' group by user_id,receipt_number';
		}
		
		$limits="";
		if($limit!=NULL){
			$limits=" limit ".$limit.",10";
		}
		
    	$query = "SELECT * from cash_register where consumer_id=".$consumer_id.$entrys." and 
					cr_status!='INACTIVE' ".$group_by." order by timestamp desc ".$limits;
    	 
    	$stmt = $this->_db_table->getAdapter();
    	$resultData = $stmt->fetchAll($query);
    	 
    	$cash_register_object_arr = array();
    	foreach ($resultData as $row) {
    		$cash_register_object = new Application_Model_CashRegister();
    	    $cash_register_object->__set("cr_amount", $row["cr_amount"]);
    		$cash_register_object->__set("timestamp", $row["timestamp"]);
    		$cash_register_object->__set("transaction_id", $row["transaction_id"]);
    		$cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
			$cash_register_object->__set("entry_status", $row["entry_status"]);
			$cash_register_object->__set("receipt_number", $row["receipt_number"]);
			$cash_register_object->__set("user_id", $row["user_id"]); 
			$cash_register_object->__set("receipt_count", $row["receipt_count"]);  
    		array_push($cash_register_object_arr, $cash_register_object);
    	}
    	return $cash_register_object_arr;

    }
 public function getTotalCollectedNextMonth($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$cluster_id=NULL)
    {
    	
		$cond="";
		$where=" where consumer_status<>'Banned' and p.is_postpaid=0";
		if($role_sites_id !=NULL)
		{
			$where =$where." and c.site_id in (".implode(",", $role_sites_id).") ";
		}
		if($site_id !=NULL)
		{
			$where =$where." and c.site_id = $site_id";
		}
		else if($cluster_id !=NULL)
		{
			$cond=" inner join sites s on s.site_id = c.site_id inner join clusters cl on cl.cluster_id= s.cluster_id";
			$where =$where." and cl.cluster_id = $cluster_id";
		}
		else if ($state_id !=NULL)
		{
			$cond=" inner join sites s on s.site_id = c.site_id inner join clusters cl on cl.cluster_id= s.cluster_id
					 inner join states st on st.state_id=cl.state_id ";
			$where =$where." and st.state_id = $state_id";
			
		}
        $query="select sum(p.package_cost) as total from consumers c inner join packages p on c.package_id =p.package_id $cond".$where; 
		//echo $query;
        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
       // print_r($result);
        if (count($result) == 0) {
            return false;
        }
        return $result[0]["total"];

    }
    
	/* public function getCashRegisterDetailsByConsumerId($consumer_id){
     $query = "SELECT * from cash_register where consumer_id=".$consumer_id." order by timestamp desc limit 10";
      
     $stmt = $this->_db_table->getAdapter();
     $resultData = $stmt->fetchAll($query);
      
     $cash_register_object_arr = array();
     foreach ($resultData as $row) {
      $cash_register_object = new Application_Model_CashRegister();
         $cash_register_object->__set("cr_amount", $row["cr_amount"]);
      $cash_register_object->__set("timestamp", $row["timestamp"]);
      $cash_register_object->__set("transaction_id", $row["transaction_id"]);
     
      array_push($cash_register_object_arr, $cash_register_object);
     }
     return $cash_register_object_arr;

    } */
	public function getMTRReadingByConsumerId($consumer_id){
      
     $query="SELECT m.meter_reading,m.consumer_id,m.timestamp,c.cr_amount FROM meter_readings m inner join cash_register c on c.consumer_id=m.consumer_id
      where m.consumer_id=".$consumer_id." and cr_entry_type='DEBIT' and DATE_FORMAT(m.timestamp,'%m-%d-%Y %h:%i')=DATE_FORMAT(c.timestamp,'%m-%d-%Y %h:%i') order by c.timestamp limit 10";
      
     $stmt= $this->_db_table->getAdapter()->query($query);
     $result = $stmt->fetchAll();
     if (count($result) == 0) {
      return false;
     }else
     {
      return $result;
     }
    }
    
    public function getAgeingofDebtorsByConsumerId($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$timestamp=NULL,$consumer_id=NULL){
    	
       $query_val=" cr inner join consumers c on c.consumer_id=cr.consumer_id inner join sites s on 
            s.site_id=c.site_id ";
    	
    	if ($site_id!=NULL){
    		$where="  s.site_status!='Disable' and s.site_id in (".$site_id.") and s.site_id!=1";
    	}elseif($state_id!=NULL){
    		$where="  s.site_status!='Disable' and s.state_id in (".$state_id.") and s.site_id!=1";
    	}elseif ($role_sites_id!=NULL){
    		$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).") and s.site_id!=1";
    	}else{
    		$where=" s.site_status!='Disable' ";
    	}
    	if($consumer_id!=NULL){
    		$consumer=" and c.consumer_id= ".$consumer_id;
    	}else{
    		$consumer=" ";
    	}
         $query="SELECT count(DISTINCT c.consumer_id) as TotalConsumerCount,
			 
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT'  and entry_status not in ('SCHEME')  and cr_status!='INACTIVE' ".$consumer." and date(timestamp)<='".$timestamp."') as total_Debit,
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)<='".$timestamp."') as total_Credit,
			
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as sum_debit_30,
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as sum_credit_30,
			(select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as consumer_30,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)   and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as sum_debit_60,
            (select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)   and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as sum_credit_60,
            (select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 60 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as consumer_60,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 90 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)) as sum_debit_90,
            (select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 90 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)) as sum_credit_90,
            (select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 90 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)) as consumer_90,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'   ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 120 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 90 DAY)) as sum_debit_120,
            (select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE'   ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 120 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 90 DAY)) as sum_credit_120,
            (select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 120 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 90 DAY)) as consumer_120,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 180 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 120 DAY)) as sum_debit_180,
            (select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 180 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 120 DAY)) as sum_credit_180,
            (select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 180 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 120 DAY)) as consumer_180,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 180 DAY)) as sum_debit_above_180,
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 180 DAY)) as sum_credit_above_180
			
			FROM cash_register ".$query_val." where ".$where."  and cr_status!='INACTIVE' ".$consumer." limit 1";
			 
		/*$query="SELECT count(DISTINCT c.consumer_id) as TotalConsumerCount,
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT'  and entry_status not in ('SCHEME')  and cr_status!='INACTIVE' ".$consumer." and date(timestamp)<='".$timestamp."') as total_Debit,
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type in ('CREDIT','ACTIVATION') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)<='".$timestamp."') as total_Credit,
			
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as sum_debit_30,
			(select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as consumer_30,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)   and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as sum_debit_60,
            (select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 60 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as consumer_60,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 90 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)) as sum_debit_90,
            (select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 90 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)) as consumer_90,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'   ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 120 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 90 DAY)) as sum_debit_120,
            (select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 120 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 90 DAY)) as consumer_120,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 180 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 120 DAY)) as sum_debit_180,
            (select count(DISTINCT c.consumer_id) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$consumer." and date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 180 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 120 DAY)) as consumer_180,
            
			(select sum(cr_amount) from cash_register ".$query_val." where ".$where." and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$consumer." and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 180 DAY)) as sum_debit_above_180
			FROM cash_register ".$query_val." where ".$where."  and cr_status!='INACTIVE' ".$consumer." limit 1";*/
			
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result[0];
    	}
    }
	
    public function calculateActivationAmount($type,$consumer_id) {
    
    	$query = "SELECT SUM(`cr_amount`) AS `total_amount` FROM `cash_register` WHERE `cr_entry_type` = '".$type."' and consumer_id=".$consumer_id." and cr_status <> 'INACTIVE'";
    
    	//echo $query; exit;
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if ($result[0]['total_amount'] == 0) {
    		return false;
    	}
    	else{
    		return $result[0]['total_amount'];
    	} 
    }	
    
    public function getTotalOutstandingAmountBySiteId($date_string=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL)
    {
    	
    	if ($site_id!=NULL){
    		$where="  s.site_status!='Disable' and s.site_id in (".$site_id.") and s.site_id!=1 ";
    	}elseif($state_id!=NULL){
    		$where="  s.site_status!='Disable' and s.state_id in (".$state_id.") and s.site_id!=1 ";
    	}elseif ($role_sites_id!=NULL){
    		$where="  s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).") and s.site_id!=1 "; 
    	}else{
    		$where=" s.site_status!='Disable' ";
    	}
    	$stmt = $this->_db_table->getAdapter();
    	$query = "SELECT SUM(cr_amount) AS debit_amount from cash_register 
					INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id inner join sites s on s.site_id=consumers.site_id
					WHERE ".$where." and cr_entry_type='DEBIT' and cash_register.entry_status not in ('SCHEME')  and cr_status != 'INACTIVE' AND date(timestamp)<='".$date_string."'";
    	 
    	$result = $stmt->fetchRow($query);
    	 $debit_amount = $result["debit_amount"]; 
    	
    	 $query = "SELECT SUM(cr_amount) AS credit_amount from cash_register 
    	  INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id inner join sites s on s.site_id=consumers.site_id
    	  WHERE ".$where." and cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT')   and cash_register.entry_status not in ('SCHEME','SD')   and cr_status != 'INACTIVE' AND date(timestamp)<='".$date_string."'";
    	 
    	  
    	$result = $stmt->fetchRow($query);
        $credit_amount = $result["credit_amount"];
    	$overdue = $debit_amount-$credit_amount;
    	return $overdue;
    }
    public function getAgeingsofDebtorsByConsumerId($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$timestamp=NULL,$consumer_id=NULL,$days=NULL){
    	 
    	$query_val="  inner join sites s on s.site_id=cn.site_id ";
    	
    	if ($site_id!=NULL){
    		$where="   s.site_status!='Disable' and s.site_id=".$site_id;
    	}elseif($state_id!=NULL){
			$where="   s.site_status!='Disable' and s.state_id=".$state_id;
    	}elseif ($role_sites_id!=NULL){
    		$where="   s.site_status!='Disable' and s.site_id in (".implode(",", $role_sites_id).")";
    	} 
    	
    	$consumer=" and consumer_id= cn.consumer_id";
    	
		if($days==30){
    		$dayCon=" date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as sum_debit_30 ";
    	}elseif($days==60){
    		$dayCon=" date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)   and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 30 DAY)) as sum_debit_60";
    	}elseif($days==90){
    		$dayCon=" date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 90 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 60 DAY)) as sum_debit_90";
    	} elseif($days==120){
    		$dayCon=" date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 120 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 90 DAY)) as sum_debit_120";
    	} elseif($days==180){
    		$dayCon=" date(timestamp)>=DATE_SUB('".$timestamp."', INTERVAL 180 DAY) and date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 120 DAY)) as sum_debit_180 ";
    	}elseif ($days==200){
    		$dayCon=" date(timestamp)<=DATE_SUB('".$timestamp."', INTERVAL 180 DAY)) as sum_debit_above_180 ";
    	}  
    	 
           $query="SELECT cn.*,
     		(select sum(cr_amount) from cash_register   
					where cr_entry_type='DEBIT' and entry_status!='SCHEME' and cr_status!='INACTIVE' ".$consumer." and date(timestamp)<='".$timestamp."') as total_Debit,
			(select sum(cr_amount) from cash_register   
					where cr_entry_type in ('CREDIT','ACTIVATION') and entry_status not in ('SD','SCHEME') and cr_status!='INACTIVE'".$consumer." and date(timestamp)<='".$timestamp."') as total_Credit,
			(select sum(cr_amount) from cash_register 
					where cr_entry_type='DEBIT' and entry_status!='SCHEME' and  cr_status!='INACTIVE' ".$consumer." and ".$dayCon."  
			
			FROM  consumers cn ".$query_val." where  ".$where." and cn.consumer_status!='banned' and cn.consumer_id=".$consumer_id;
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result[0];
    	}
    }
    
    public function getWeeklyTxn($timestamp){
    	
    	/* $zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$timestamp=$zendDate->toString('yyyy-MM-dd HH:MM:ss');
    	
    	$date_val=date('d M Y H:i:s', strtotime(date('d M Y H:i:s') .' -7 day'));
    	$date_s = new Zend_Date($date_val,"yyyy-MM-dd HH:MM:ss");
    	$timestamps = $date_s->toString("yyyy-MM-dd HH:MM:ss"); */
    	
    	$query="SELECT *,sum(cr_amount) as total_amount FROM cash_register inner join consumers on cash_register.consumer_id=consumers.consumer_id
				inner join sites on sites.site_id=consumers.site_id where date(timestamp)='".$timestamp."' and 
				cash_register.cr_status!='INACTIVE' and sites.site_status!='Disable' and sites.site_id!=1 group by sites.site_id,cash_register.user_id";
     
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
     		return $result;
	    }
    }
    public function getCashRegisterByCollectionId($collection_id){
    	 
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$timestamp=$zendDate->toString('yyyy-MM-dd HH:MM:ss');
    	$date_val=date('d M Y H:i:s', strtotime(date('d M Y H:i:s') .' -8 day'));
    	$date_s = new Zend_Date($date_val,"yyyy-MM-dd HH:MM:ss");
    	$timestamps = $date_s->toString("yyyy-MM-dd HH:MM:ss");
    	
    	 $query="SELECT *,sum(cr_amount) as total_amount FROM cash_register inner join consumers on cash_register.consumer_id=consumers.consumer_id
				inner join sites on sites.site_id=consumers.site_id where date(timestamp)<='".$timestamp."' and
				date(timestamp) >='".$timestamps."' and user_id=".$collection_id." and sites.site_status!='Disable' and sites.site_id!=1 group by sites.site_id ";
    	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result;
    	}
    }
    public function getCashRegisterByUserId($collection_id){
    
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$timestamp=$zendDate->toString('yyyy-MM-dd HH:MM:ss');
    	$date_val=date('d M Y H:i:s', strtotime(date('d M Y H:i:s') .' -8 day'));
    	$date_s = new Zend_Date($date_val,"yyyy-MM-dd HH:MM:ss");
    	$timestamps = $date_s->toString("yyyy-MM-dd HH:MM:ss");
    	 
    	$query="SELECT *,sum(cr_amount) as total_amount FROM cash_register inner join consumers on cash_register.consumer_id=consumers.consumer_id
				inner join sites on sites.site_id=consumers.site_id where date(timestamp)<='".$timestamp."' and
				date(timestamp) >='".$timestamps."' and user_val=".$collection_id." and sites.site_status!='Disable' and sites.site_id!=1 group by sites.site_id ";
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result;
    	}
    }
    public function getAllUserVal(){
    
    	 $query="SELECT distinct(user_val) as user_val FROM tara.cash_register";
    
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result;
    	}
    }
    public function getOutstandingBySiteIdAndDate($consumer_id,$date,$package_id=NULL)
    {
    	$trans_date=date_parse_from_format("Y-m-d", $date);
    	$day=$trans_date["day"];
    	$month= $trans_date["month"];
    	$year= $trans_date["year"];
    	
    	$current_date=$year."-".$month."-01";
    	 
    	$stmt = $this->_db_table->getAdapter();
    	
    	$packages="";
    	if($package_id!=NULL){
    		$packages=" and cash_register.package_id=".$package_id;
    	}
    	
    	$query = "SELECT SUM(cr_amount) AS debit_amount from cash_register "
    	 . "INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id "
    	. "WHERE cr_entry_type='DEBIT' AND consumers.consumer_id=".$consumer_id." and cr_status <> 'INACTIVE' AND timestamp< '".$current_date."'".$packages; 
    	
    	$result = $stmt->fetchRow($query);
    	$debit_amount = $result["debit_amount"];
    	$query = "SELECT SUM(cr_amount) AS credit_amount from cash_register "
    	 . "INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id "
    	 . "WHERE cr_entry_type='CREDIT' AND consumers.consumer_id=".$consumer_id." and cr_status <> 'INACTIVE' AND timestamp<'".$current_date."'".$packages;
    	
    	
    	$result = $stmt->fetchRow($query);
    	$credit_amount = $result["credit_amount"];
    	$overdue = $debit_amount-$credit_amount;
    	return $overdue;
    }
    
    public function getTotalOutstandingAmountForSummary($date=NULL,$site_id=NULL)
    {
    	$trans_date=date_parse_from_format("Y-m-d", $date);
    	$day=$trans_date["day"];
    	$month= $trans_date["month"];
    	$year= $trans_date["year"];
    	  
    	$current_date=$year."-".$month."-01";
    	
    	$stmt = $this->_db_table->getAdapter();
    	$query = "SELECT SUM(cr_amount) AS debit_amount from cash_register
    	 INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id inner join sites s on s.site_id=consumers.site_id
    	  WHERE ".$where." and cr_entry_type='DEBIT' AND consumers.consumer_status!='banned' and cr_status = 'ACTIVE' AND date(timestamp)<'".$current_date."'";
    
    	$result = $stmt->fetchRow($query);
    	$debit_amount = $result["debit_amount"];
    	 
    	$query = "SELECT SUM(cr_amount) AS credit_amount from cash_register
    	  INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id inner join sites s on s.site_id=consumers.site_id
    	  WHERE ".$where." and cr_entry_type='CREDIT' AND consumers.consumer_status!='banned' and cr_status = 'ACTIVE' AND date(timestamp)<'".$current_date."'";
    
    
    	$result = $stmt->fetchRow($query);
    	$credit_amount = $result["credit_amount"];
    	$overdue = $debit_amount-$credit_amount;
    	return $overdue;
    }
	
	 public function getTotalOutstandingAmountForSummarybanned($date=NULL,$site_id=NULL)
    {
    	$trans_date=date_parse_from_format("Y-m-d", $date);
    	$day=$trans_date["day"];
    	$month= $trans_date["month"];
    	$year= $trans_date["year"];
    
    	$current_date=$year."-".$month."-01";
    	 
    	$stmt = $this->_db_table->getAdapter();
    	  $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register
    	 INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id inner join sites s on s.site_id=consumers.site_id
    	  WHERE consumers.site_id=".$site_id." and cash_register.cr_entry_type='DEBIT' AND consumers.consumer_status!='banned' and cash_register.cr_status != 'INACTIVE' AND cash_register.timestamp<'".$current_date."'";
     
    	$result = $stmt->fetchRow($query);
    	$debit_amount = $result["debit_amount"];
    
    	$query = "SELECT SUM(cr_amount) AS credit_amount from cash_register
    	  INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id inner join sites s on s.site_id=consumers.site_id
    	  WHERE consumers.site_id=".$site_id." and cash_register.cr_entry_type='CREDIT' AND consumers.consumer_status!='banned' and cash_register.cr_status != 'INACTIVE' AND cash_register.timestamp<'".$current_date."'";
    
    
    	$result = $stmt->fetchRow($query);
    	$credit_amount = $result["credit_amount"];
    	$overdue = $debit_amount-$credit_amount;
    	return $overdue;
    }
    
  public function getCrByPackageId($month=NULL,$year=NULL,$site_id=NULL){
    	
    	$query="SELECT p.package_name,sum(cr.cr_amount)as cr_amount,count(c.consumer_id)as totalCr FROM cash_register cr 
				inner join consumers c on c.consumer_id=cr.consumer_id
				inner join packages p on p.package_id=c.package_id 
				where cr.cr_entry_type='DEBIT' and MONTH(timestamp)=".$month." and year(timestamp)=".$year." 
				and c.site_id=".$site_id." group by p.package_id;";
    	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result;
    	}
    }
/*public function getMonthlyDebitByConsumerId($consumer_id,$month,$year)
    {
    	  
    	if($month != null )
    	{
    		$curDate=$year.'-'.$month.'-24';
        	if($month==1){
        		$LastCurDate=($year-1).'-12-25';
        	}else{
        		$LastCurDate=$year.'-'.($month-1).'-25';
        	}
        }
    	 
    	$stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type='DEBIT' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id." 
    			and date(timestamp)>='".$LastCurDate."' and date(timestamp)<='".$curDate."'";
     
    	$result = $stmt->fetchRow($query);
    	$debit_amount = $result["debit_amount"];
    	if($debit_amount==NULL)
    	{
    		$debit_amount = 0;
    	}
    	return $debit_amount;
    }*/

  public function getMonthlyDebitByConsumerId($consumer_id,$month,$year,$package_id=NULL)
    {
    	  
    	if($month != null )
    	{
    		$curDate=$year.'-'.$month.'-24';
        	if($month==1){
        		$LastCurDate=($year-1).'-12-25';
        	}else{
        		$LastCurDate=$year.'-'.($month-1).'-25';
        	}
        }
        $package="";
        if($package_id!=NULL){
        	$package=" and package_id=".$package_id;
        }
    	$stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type='DEBIT' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id." 
    			and date(timestamp)>='".$LastCurDate."' and date(timestamp)<='".$curDate."'".$package;
     
    	$result = $stmt->fetchRow($query);
    	$debit_amount = $result["debit_amount"];
    	if($debit_amount==NULL)
    	{
    		$debit_amount = 0;
    	}
    	return $debit_amount;
    }
    
    public function getMonthlyDebitsByConsumerId($consumer_id,$month,$year,$package_id=NULL)
    {
    	 
    	if($month != null )
    	{
    		$curDate=$year.'-'.$month.'-24';
    		if($month==1){
    			$LastCurDate=($year-1).'-12-25';
    		}else{
    			$LastCurDate=$year.'-'.($month-1).'-25';
    		}
    	}
    	$package="";
    	if($package_id!=NULL){
    		$package=" and package_id=".$package_id;
    	}
    	$stmt = $this->_db_table->getAdapter();
    	 $query = "SELECT * from cash_register WHERE cr_entry_type='DEBIT' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id."
    			and date(timestamp)>='".$LastCurDate."' and date(timestamp)<='".$curDate."'".$package;
    	 
    	$result = $stmt->fetchAll($query);
    	
    	return $result;
    }
    public function getMonthlyCreditsByConsumerId($consumer_id,$month,$year,$package_id=NULL)
    {
    
    	if($month != null )
    	{
    		$curDate=$year.'-'.$month.'-24';
    		if($month==1){
    			$LastCurDate=($year-1).'-12-25';
    		}else{
    			$LastCurDate=$year.'-'.($month-1).'-25';
    		}
    	}
    	$package="";
    	if($package_id!=NULL){
    		$package=" and package_id=".$package_id;
    	}
    	$stmt = $this->_db_table->getAdapter();
    	 $query = "SELECT * from cash_register WHERE cr_entry_type='CREDIT' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id."
    			and date(timestamp)>='".$LastCurDate."' and date(timestamp)<='".$curDate."'".$package;
    
    	$result = $stmt->fetchAll($query);
    	
    	return $result;
    }
    
    public function getOutByConsumerId($consumer_id=null,$month=null,$year=null,$checkDate=null,$package_id=NULL,$entry_status=null,$datetime=NULL)
    {
    	 
    	if($month != null )
    	{
    		if($checkDate==NULL){
				if($datetime==NULL){
					$curDate=$year.'-04-01';
				}else{
					$curDate=$datetime;
				}
    		}else{
    			$curDate=$year.'-03-25';
    		}
    	}
    	$packages="";
    	if($package_id!=NULL){
    		$packages=" and package_id=".$package_id;
    	}
		
		$entrys_status="";
    	if($entry_status!=NULL){
    		$entrys_status=" and entry_status='".$entry_status."'";
    	}
		
    	$stmt = $this->_db_table->getAdapter();
        $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type='DEBIT' ".$entrys_status." and entry_status not in ('OTHERS','SCHEME','ACT') AND cr_status != 'INACTIVE' AND consumer_id=".$consumer_id."
    			and date(timestamp)<'".$curDate."'".$packages;
    	 
    	$result = $stmt->fetchRow($query);
    	$debit_amount = $result["debit_amount"];
    	if($debit_amount==NULL)
    	{
    		$debit_amount = 0;
    	}
    	
    	   $querys = "SELECT SUM(cr_amount) AS credit_amount from cash_register WHERE cr_entry_type in ('CREDIT','DISCOUNT') ".$entrys_status." and entry_status not in ('OTHERS','SCHEME','SD','ACT') AND cr_status != 'INACTIVE' AND consumer_id=".$consumer_id."
    			and date(timestamp)<'".$curDate."'".$packages;
    	     
    	$results = $stmt->fetchRow($querys);
    	$credit_amount = $results["credit_amount"];
    	if($credit_amount==NULL)
    	{
    		$credit_amount = 0;
    	}
		 
    	$outstanding=$debit_amount-$credit_amount; 
    	return $outstanding;
    }
    public function getCrByTxnId($txn_id=NULL){
    	 
    	$query="SELECT  * from cash_register where transaction_id='".$txn_id."'";
				 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result[0];
    	}
    }

	 public function getCr_pendingByTxnId($txn_id=NULL){
    	 
    	$query="SELECT  * from cash_register where transaction_id='".$txn_id."' and cr_status='NEW'";
				 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result[0];
    	}
    }
    public function getMonthDebitsByConsumerId($consumer_id,$month,$year,$package_id=NULL)
    {
    
    	$cond="";
    	if($month != null )
    	{
    		$cond="AND MONTH(timestamp)='".$month."'";
    	}
    	if($year !=null)
    	{
    		$cond .=" AND YEAR(timestamp)='".$year."'";
    	}
    	$packages="";
    	if($package_id!=NULL){
    		$packages=" and package_id=".$package_id;
    	}
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT * from cash_register WHERE cr_entry_type='DEBIT' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id." ".$cond.$packages;
	   
         $result = $stmt->fetchAll($query);
    	 
    	return $result;
    }
     public function getMonthCreditsByConsumerId($consumer_id,$month,$year,$package_id=NULL)
    {
    
    	$cond="";
    	if($month != null )
    	{
    		$cond="AND MONTH(timestamp)='".$month."'";
    	}
    	if($year !=null)
    	{
    		$cond .=" AND YEAR(timestamp)='".$year."'";
    	}
    	$packages="";
    	if($package_id!=NULL){
    		$packages=" and package_id=".$package_id;
    	}
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT * from cash_register WHERE cr_entry_type='CREDIT' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id." ".$cond.$packages;
	   
         $result = $stmt->fetchAll($query);
    	 
    	return $result;
    }


 public function getBalanceBySite($sites,$month,$year,$type)
    {
    
    	$cond="";
    	if($month != null )
    	{
    		$cond=" AND MONTH(timestamp)='".$month."'";
    	}
    	if($year !=null)
    	{
    		$cond .=" AND YEAR(timestamp)='".$year."'";
    	}
    	$stmt = $this->_db_table->getAdapter();
    	 $query = "SELECT sum(cr_amount) as amount from cash_register as cr inner join consumers as c on c.consumer_id=cr.consumer_id
    			 WHERE cr_entry_type='".$type."' AND cr_status <> 'INACTIVE' and c.consumer_status!='banned' AND c.site_id=".$sites.$cond;
     
    	$result = $stmt->fetchRow($query);
    	$debit_amount = $result["amount"];
    	if($debit_amount==NULL)
    	{
    		$debit_amount = 0;
    	}
    	return $debit_amount;
    }
    public function getBalanceByState($sites,$month,$year,$type)
    {
    
    	$cond="";
    	if($month != null )
    	{
    		$cond=" AND MONTH(timestamp)='".$month."'";
    	}
    	if($year !=null)
    	{
    		$cond .=" AND YEAR(timestamp)='".$year."'";
    	}
    	$stmt = $this->_db_table->getAdapter();
    	$query = "SELECT sum(cr_amount) as amount from cash_register as cr inner join consumers as c on c.consumer_id=cr.consumer_id
    			inner join sites as s on c.site_id=s.site_id 
    			 WHERE cr_entry_type='".$type."' AND cr_status <> 'INACTIVE' and c.consumer_status!='banned' AND s.site_status!='Disable' and s.state_id=".$sites.$cond;
    	 
    	$result = $stmt->fetchRow($query);
    	$debit_amount = $result["amount"];
    	if($debit_amount==NULL)
    	{
    		$debit_amount = 0;
    	}
    	return $debit_amount;
    }
	
	public function getMixedModeEntry($cron_entry,$day,$month,$year)
    {
     
	    $query = "SELECT * from cash_register  WHERE cron_entry='".$cron_entry."' and cr_entry_type='debit' and day(timestamp)=".$day." and month(timestamp)=".$month." and year(timestamp)=".$year;
     
    	$stmt = $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   	
   		if( count($result) == 0 ) {
   			return false;
   		}
   		else{
            return $result;
   		}
    }
    public function getTotalTransactionForRfMonthly()
    {
    	$zendDate = new Zend_Date();
    	$zendDate->setTimezone("Asia/Calcutta");
    	$timestamp = $zendDate->toString("yyyy-MM-dd");
    	 $query ="SELECT (select sum(cr_amount) from cash_register  cr INNER JOIN consumers c on c.consumer_id= cr.consumer_id INNER JOIN sites s
on c.site_id=s.site_id where s.site_status!='Disable' and cr.cr_status!='INACTIVE' 
and date(timestamp)< DATE_SUB('".$timestamp."',interval 2 YEAR) and cr_entry_type='DEBIT') as total_Debit,
(select sum(cr_amount) from cash_register cr INNER JOIN consumers c on c.consumer_id= cr.consumer_id INNER JOIN sites s
on c.site_id=s.site_id where s.site_status!='Disable' and cr.cr_status!='INACTIVE' 
and date(timestamp)< DATE_SUB('".$timestamp."',interval 2 YEAR) and cr_entry_type='CREDIT') as total_Credit,
(select sum(cr_amount) from cash_register cr INNER JOIN consumers c on c.consumer_id= cr.consumer_id INNER JOIN sites s
on c.site_id=s.site_id where s.site_status!='Disable' and cr.cr_status!='INACTIVE' 
and date(timestamp)< DATE_SUB('".$timestamp."',interval 2 YEAR) and cr_entry_type='ACTIVATION') 
as total_Activation";
    
    	$stmt = $this->_db_table->getAdapter();
    	$result = $stmt->fetchRow($query);
    	print_r($result);
    	if( count($result) == 0 ) {
    		return false;
    	}
    	else{
    		return $result;
    	}
    	
    	
    }
	
  public function getTotalCreditByConsumerId($consumerId){
    
    	$stmt = $this->_db_table->getAdapter();
    	$query = "SELECT SUM(cr_amount) AS credit_amount from cash_register INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id WHERE cr_entry_type='CREDIT' AND cash_register.consumer_id = ".$consumerId." AND 
    			  consumers.consumer_status != 'banned' AND  cr_status <> 'INACTIVE'";
    	$result = $stmt->fetchRow($query);
    	if(!$result){
    	   return 0;
    	}
    	$credit_amount = $result["credit_amount"];
    	return $credit_amount ? $credit_amount:0;    
    }      
    
    public function getTotalDebitByConsumerId($consumerId){
    	$stmt = $this->_db_table->getAdapter();         
    	$query = "SELECT SUM(cr_amount) AS credit_amount from cash_register                 
    			INNER JOIN consumers ON consumers.consumer_id=cash_register.consumer_id                  
    			WHERE cr_entry_type='DEBIT' AND cash_register.consumer_id = ".$consumerId." AND 
    			consumers.consumer_status != 'banned' AND cr_status <> 'INACTIVE'";
    	$result = $stmt->fetchRow($query);
    	if(!$result){
    	 return 0;
    	}         
    	$credit_amount = $result["credit_amount"];
    	return $credit_amount ? $credit_amount:0; 
        }	
    
  public function getAllDEBITEntryByConsumerId($consumer_id)
        {
        
        	$stmt = $this->_db_table->getAdapter();
        	$query = "SELECT * from cash_register WHERE cr_entry_type='DEBIT' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id." order by timestamp";
        
	        $stmt = $this->_db_table->getAdapter()->query($query);
	   		$result = $stmt->fetchAll();
	   	
	   		if( count($result) == 0 ) {
	   			return false;
	   		}
	   		else{
	            return $result;
	   		}
        }
      
        public function getAllCREDITEntryByConsumerId($consumer_id,$timeatamp)
        {
            $trans_date=date_parse_from_format("Y-m-d", $timeatamp); 
        	$month= $trans_date["month"];
        	$year= $trans_date["year"];
        	
        	$stmt = $this->_db_table->getAdapter();
        	$query = "SELECT * from cash_register WHERE cr_entry_type='CREDIT' AND cr_status <> 'INACTIVE' AND consumer_id=".$consumer_id." 
        			and month(timestamp)=".$month." and year(timestamp)=".$year;
        
        	$stmt = $this->_db_table->getAdapter()->query($query);
        	$result = $stmt->fetchAll();
        	
        	if( count($result) == 0 ) {
        		return false;
        	}
        	else{
        		return $result;
        	}
        }
    
        public function getAllCashRegisterPending($role_site_id=null,$site_id=null) {
          
	         if($site_id!=NULL){
	          	$where=" and c.site_id=".$site_id;
	         }elseif($role_site_id!=NULL){
	          	$where=" and c.site_id in (".implode(",", $role_site_id).")";
	         }else{
	         	$where="";
	         }
         
          	$query="SELECT cr.* from cash_register cr inner join consumers c on c.consumer_id=cr.consumer_id 
             where cr_status='NEW' ".$where." order by timestamp desc"; 
        	 
        	$stmt= $this->_db_table->getAdapter()->query($query);
        	$result = $stmt->fetchAll();
        	$cash_register_object_arr = array();
        
        	if (count($result) == 0) {
        		return false;
        	}
        	foreach ($result as $key => $row) {
        
        		$cash_register_object = new Application_Model_CashRegister($row);
        		foreach($row as $key=>$value)
        		{
        			$cash_register_object->__set($key,$value);
        		}
        		array_push($cash_register_object_arr, $cash_register_object);
        		 
        	}
        	return $cash_register_object_arr;
        }
        
        public function getAllCashRegisterPendingById($cr_id){
            $query="SELECT * from cash_register where cr_id in (".implode(",", $cr_id).") and cr_status!='INACTIVE'";
        	
        	$stmt= $this->_db_table->getAdapter()->query($query);
        	$result = $stmt->fetchAll();
        	$cash_register_object_arr = array();
        	
        	if (count($result) == 0) {
        		return false;
        	}
        	foreach ($result as $key => $row) {
        	
        		$cash_register_object = new Application_Model_CashRegister($row);
        		foreach($row as $key=>$value)
        		{
        			$cash_register_object->__set($key,$value);
        		}
        		array_push($cash_register_object_arr, $cash_register_object);
        		 
        	}
        	return $cash_register_object_arr;
        }
        
         public function updateCashRegisterPendingById($cr_id){
            
        	$query ="delete from  cash_register_pending where cr_id in (".implode(",", $cr_id).") ";
        	
            $stmt = $this->_db_table_cashpending->getAdapter()->query($query);
        	$result=$stmt->execute();
        	if (count($result) == 0) {
        		return false;
        	}else{
        		return true;
        	}
		}
		
		public function getMonthlyDebitAmountByConsumerId($consumer_id,$month,$year,$package_id)
		{
		 
			$stmt = $this->_db_table->getAdapter();
			$query = "SELECT SUM(cr_amount) AS debit_amount from cash_register WHERE cr_entry_type='DEBIT' and entry_status='CHG' AND consumer_id=".$consumer_id." and package_id=".$package_id." AND cr_status != 'INACTIVE' AND month(timestamp)=".$month." and year(timestamp)=".$year;
			$result = $stmt->fetchRow($query);
			$debit_amount = intval($result["debit_amount"]);
			return $debit_amount;
		} 
  
		 public function getAllTransactionType($state_id=NULL,$site_id=NULL,$package_type=NULL,$package_id=NULL,$status=NULL,$collection_agent=NULL,$type=NULL,$currDate=NULL,$lastDate=NULL,$role_site=NULL)
		{
			$join="inner join consumers c on c.consumer_id=cr.consumer_id inner join sites s on s.site_id=c.site_id ";
			if($package_id!=NULL){
				$join.=" inner join consumer_packages cp on c.consumer_id=cp.consumer_id inner join packages p on p.package_id=cp.package_id ";
				$where=" where cp.package_id in (".implode(",", $package_id).")";
			}elseif($package_type!=NULL && $site_id!=NULL){
				$join.=" inner join consumer_packages cp on c.consumer_id=cp.consumer_id inner join packages p on p.package_id=cp.package_id ";
				$where=" where cp.is_postpaid in (".implode(",", $package_type).") and c.site_id in (".implode(",", $site_id).")";
			}elseif($package_type!=NULL){
				$join.=" inner join consumer_packages cp on c.consumer_id=cp.consumer_id inner join packages p on p.package_id=cp.package_id ";
				$where=" where cp.is_postpaid in (".implode(",", $package_type).")";
			}elseif($site_id!=NULL){
				$where=" where c.site_id in (".implode(",", $site_id).") and s.site_status!='Disable'";
			}elseif($state_id!=NULL){
				$where=" where s.state_id in (".implode(",", $state_id).") and s.site_status!='Disable'";
			}else{
				$where =" where c.site_id in (".implode(",", $role_site).") and s.site_status!='Disable'";
			}
			 $where.=" and s.site_id!=1 ";
			$statuss="";
			if($status!=NULL){
			    if(in_array("'ACTIVE'", $status)){
					$statuss=" and cr_status!='INACTIVE' ";
				}
			}
			 
			$types="";
			if($type!=NULL){
				$types=" and entry_status in (".implode(",", $type).")";
			}
			$collection_agents="";
			if($collection_agent!=NULL){
				$collection_agents=" and user_id in (".implode(",", $collection_agent).")";
			}
			$date=" and date(timestamp)<='".$currDate."' and date(timestamp)>='".$lastDate."'";
			$stmt = $this->_db_table->getAdapter();
			   
			$query = "SELECT cr.*,s.state_id,s.site_name,c.consumer_connection_id,c.consumer_name,c.consumer_act_date,c.consumer_status from cash_register cr ".$join.$where.$statuss.$date.$types.$collection_agents."  and c.site_id!=1 order by s.site_id";
			  
			$stmt = $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			 
			if( count($result) == 0 ) {
				return false;
			}
			else{
				return $result;
			}
		}
		public function getAllTransactionTypeForPending($state_id=NULL,$site_id=NULL,$package_type=NULL,$package_id=NULL,$status=NULL,$collection_agent=NULL,$type=NULL,$outstanding=NULL,$not_paid=NULL,$currDate=NULL,$lastDate=NULL)
		{
			$join="inner join consumers c on c.consumer_id=cr.consumer_id inner join sites s on s.site_id=c.site_id inner join consumer_packages cp on c.consumer_id=cp.consumer_id inner join packages p on p.package_id=cp.package_id";
			if($package_id!=NULL){
				$where=" where cp.package_id in (".implode(",", $package_id).")";
			}elseif($package_type!=NULL && $site_id!=NULL){
				$where=" where cp.is_postpaid in (".implode(",", $package_type).") and c.site_id in (".implode(",", $site_id).")";
			}elseif($package_type!=NULL){
				$where=" where cp.is_postpaid in (".implode(",", $package_type).")";
			}elseif($site_id!=NULL){
				$where=" where c.site_id in (".implode(",", $site_id).")";
			}elseif($state_id!=NULL){
				$where=" where s.state_id in (".implode(",", $state_id).")";
			}
				
			$statuss="";
			if($status!=NULL){
				if(in_array("'INACTIVE'", $status) && in_array("'PENDING'", $status)){
					$statuss=" and cr_status!=''";
				}elseif(in_array("'PENDING'", $status)){
					$statuss=" and cr_status='ACTIVE' ";
				}else{
					$statuss=" and cr_status='INACTIVE' ";
				}
			}
			$types="";
			if($type!=NULL){
				$types=" and entry_status in (".implode(",", $type).")";
			}
			$collection_agents="";
			if($collection_agent!=NULL){
				$collection_agents=" and user_id in (".implode(",", $collection_agent).")";
			}
			  $date=" and date(timestamp)<='".$currDate."' and date(timestamp)>='".$lastDate."'";
			    $query = "SELECT cr.*,s.state_id,s.site_name,c.consumer_connection_id,c.consumer_name,c.consumer_act_date,c.consumer_status from cash_register_pending cr ".$join.$where.$statuss.$date.$types.$collection_agents."  and c.site_id!=1 order by s.site_id";
			 
			$stmt =  $this->_db_table_cashpending->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if( count($result) == 0 ) {
				return false;
			}
			else{
				return $result;
			}
		}
		public function getAllTransactionForBot($day=NULL,$month=NULL,$year=NULL,$sites=NULL,$status=NULL,$entry_status=NULL){
			
			$table_field=$this->_db_table;
			$types="";$days="";
			if($status=='ACTIVE'){
				$types= " and cr.cr_status !='INACTIVE' and cr.cr_status !='NEW' and cr.entry_status !='SCHEME' ";
				$table="cash_register";
			}elseif ($status=='INACTIVE'){
				$types= " and cr.cr_status ='INACTIVE'";
				$table="cash_register";
				$table_field=$this->_db_table_cashpending;
			}elseif ($status=='PENDING'){
				$types= " and cr.cr_status ='NEW' and cr.entry_status !='SCHEME' ";
				$table="cash_register";
				$table_field=$this->_db_table_cashpending;
			}
			
			if($entry_status=='CrNote'){
				$types= " and cr.entry_status ='DISCOUNT'";
				$table="cash_register";
			}elseif ($entry_status=='DrNote'){
				$types= " and cr.entry_status ='BILL'";
				$table="cash_register";
			}
			
			if($day!=NULL){
				if($status!='INACTIVE'){
					$days=" and day(cr.timestamp)=".$day." and month(cr.timestamp)=".$month." and year(cr.timestamp)=".$year;
				}else{
					$days=" and day(cr.approved_date)=".$day." and month(cr.approved_date)=".$month." and year(cr.approved_date)=".$year;
				}
			}elseif($status!='PENDING'){
				if($status=='INACTIVE'){
					$days=" and month(cr.approved_date)=".$month." and year(cr.approved_date)=".$year;
				}else{
					$days=" and month(cr.timestamp)=".$month." and year(cr.timestamp)=".$year;
				}
			}
				 
			          $query = "SELECT *,cr.remark as remarks from ".$table."  cr inner join consumers c on c.consumer_id=cr.consumer_id inner join sites s on s.site_id=c.site_id
					 WHERE cr.cr_entry_type!='DEBIT' ".$types." AND s.site_id in (".implode(",", $sites).") and c.site_id!=1
        			   ".$days." order by s.site_id"; 
			 
			$stmt = $table_field->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			 
			if( count($result) == 0 ) {
				return false;
			}
			else{
				return $result;
			}
		}
		
		public function getOutstandingByConsumers($consumer_id=NULL,$currDate=NULL,$lastDate=NULL)
		{
			$date="";
			if($currDate!=null){
				$date.=" and date(timestamp)<='".$currDate."'";
			}
			if($lastDate!=NULL){
				$date.=" and date(timestamp)>='".$lastDate."'";
			}
			
			  $query="select (select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and
					cr_entry_type='CREDIT' and cr_status!='INACTIVE' ".$date.") as CREDIT,
					(select sum(cr_amount) from cash_register where consumer_id=c.consumer_id and
							cr_entry_type='DEBIT' and cr_status!='INACTIVE' ".$date.") as DEBIT,consumer_id
							from cash_register c where c.consumer_id=".$consumer_id." limit 1";
			 
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
		public function getOutstandingByFilter($month=NULL,$year=NULL,$site_id=NULL,$state_id=NULL,$role_site_id=NULL)
		{
			
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id inner join sites s on c.site_id=s.site_id where c.site_id!=1 and";
				
			if($site_id !=NULL){
				$where ="  c.site_id = ".$site_id." and cr.cr_status!='INACTIVE'";
			}else if ($state_id !=NULL){
				$where ="  s.state_id = ".$state_id." and cr.cr_status!='INACTIVE'";
			}else{
				$where ="  c.site_id in (".implode(",", $role_site_id).") and cr.cr_status!='INACTIVE'";
			}
			
			$date=" and month(timestamp)=".$month." and year(timestamp)='".$year."'";
				
		 	 $query="select (select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
					cr_entry_type='CREDIT' and cr_status!='INACTIVE' ".$date.") as CREDIT,
					(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
							cr_entry_type='DEBIT' and cr_status!='INACTIVE' ".$date.") as DEBIT
							from cash_register limit 1";
		    
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
				
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
		public function updateCRPendingById($cr_id,$remark,$user_id){
		
			$date = new Zend_Date(); 
            $date->setTimezone("Asia/Calcutta");
            $approved_date = $date->toString("yyyy-MM-dd HH:mm:ss");
			
			   $query ="update cash_register set cr_status='INACTIVE', remark='".$remark."', approved_date='".$approved_date."' , 
						approved_by=".$user_id." where cr_id in (".implode(",", $cr_id).") ";
			 
			$stmt = $this->_db_table->getAdapter()->query($query);
			$result=$stmt->execute();
			if (count($result) == 0) {
				return false;
			}else{
				return true;
			}
		}
		
		public function getTotalCollectedForEntryType($curr_date=NULL,$last_date=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$segment=NULL,$consumer_type=NULL)
		{
		$joins="";
			 if($segment!=NULL || $consumer_type!=NULL){
				$joins=" inner join consumer_packages cp on c.consumer_id=cp.consumer_id inner join packages p on c.package_id=p.package_id ";
			}
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id inner join sites s on c.site_id=s.site_id ".$joins." where c.site_id!=1 and s.site_status!='Disable' and"; 
			
			if($site_id !=NULL){
				$where =" and c.site_id in (".$site_id.") and cr.cr_status!='INACTIVE'";
			}else if ($state_id !=NULL){
				$where =" and s.state_id in (".$state_id.") and cr.cr_status!='INACTIVE'";
			}else{
				$where =" and c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE'";
			}
			
			if($segment!=NULL){
				$where.=" and c.type_of_me in (".$segment.") ";
			}
				
			if($consumer_type!=NULL){
				$where.=" and p.light_load in (".$consumer_type.")";
			}
			if($curr_date!=NULL && $last_date!=NULL){
				//$month_val = date("m", strtotime($month));
				$where.=" and date(timestamp)<='".$curr_date."' and date(timestamp)>='".$last_date."' ";
			}
			
			   $query="SELECT 	(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='EED' and cr_entry_type='CREDIT' ".$where.") as EED,
								(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='MED' and cr_entry_type='CREDIT' ".$where.") as MED,
								(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='CHG' and cr_entry_type='CREDIT' ".$where.") as CHG,
								(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='ACT' and cr_entry_type='ACTIVATION' ".$where.") as ACT,
								(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='WATER' and cr_entry_type='CREDIT' ".$where.") as WATER,
								(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='OTHERS' and cr_entry_type='CREDIT' ".$where.") as OTHERS,
								(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='SD' and cr_entry_type='CREDIT' ".$where.") as SD,
								(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='SCHEME' and cr_entry_type='CREDIT' ".$where.") as DISCOUNT,
								(select sum(cr_amount) FROM cash_register cr ".$join." entry_status='METER' and cr_entry_type='CREDIT' ".$where.") as METER,
								(select sum(cr_amount) FROM cash_register cr ".$join." cr_entry_type='DISCOUNT' ".$where.") as cr_note,

								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='EED' and cr_entry_type='CREDIT' ".$where.") as EED_count,
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='MED' and cr_entry_type='CREDIT' ".$where.") as MED_count,
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='CHG' and cr_entry_type='CREDIT' ".$where.") as CHG_count,
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='ACT' and cr_entry_type='ACTIVATION' ".$where.") as ACT_count,
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='WATER' and cr_entry_type='CREDIT' ".$where.") as WATER_count,
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='OTHERS' and cr_entry_type='CREDIT' ".$where.") as OTHERS_count,
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='METER' and cr_entry_type='CREDIT' ".$where.") as METER_count, 
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='SD' and cr_entry_type='CREDIT' ".$where.") as SD_count,
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." entry_status='SCHEME' and cr_entry_type='CREDIT' ".$where.") as DISCOUNT_count,
								(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." cr_entry_type='DISCOUNT' ".$where.") as cr_note_count
					from cash_register limit 1";
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			 
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		
		public function getAllDebitTransactionForBot($month=NULL,$year=NULL,$sites=NULL){
				
			$days=" and day(cr.timestamp)!=1 and month(cr.timestamp)=".$month." and year(cr.timestamp)=".$year;
			 
			$stmt = $this->_db_table->getAdapter();
			  $query = "SELECT * from cash_register cr inner join consumers c on c.consumer_id=cr.consumer_id 
						inner join sites s on s.site_id=c.site_id
					 WHERE cr.cr_entry_type='DEBIT' and s.site_id in (".implode(",", $sites).")
        			   ".$days." and cr_status='ACTIVE' order by s.site_id";
			 
			$stmt = $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
				
			if(count($result) == 0) {
				return false;
			}else{
				return $result;
			}
		}
		
		public function getBillsBySiteId($site_id=NULL,$month=NULL,$year=NULL,$state_id=NULL,$role_sites_id=NULL)
		{
			if($state_id!=NULL){
				$site="s.state_id=".$state_id." and s.site_id in (".implode(",", $role_sites_id).") and s.site_id!=1";
			}elseif($site_id!=NULL){
				$site="c.site_id=".$site_id;
			}else{
				$site="s.site_id in (".implode(",", $role_sites_id).") and s.site_id!=1";
			}
	 
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id 
					inner join sites s on c.site_id=s.site_id where ".$site." and cr.cr_status!='INACTIVE'";
				
			$date=" and month(cr.timestamp)=".$month." and year(cr.timestamp)=".$year;
			 	
			   $query=" select (select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='DEBIT' and entry_status!='SCHEME' ".$date.") as BILL,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT'  and entry_status!='DISCOUNT' and entry_status='CHG' ".$date.") as CHG,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='ACTIVATION' and entry_status='ACT' ".$date.") as ACT, 
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='EED' ".$date.") as EED,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='MED' ".$date.") as MED,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='WATER' ".$date.") as WATER,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='OTHERS' ".$date.") as OTHERS,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='SCHEME' ".$date.") as SCHEME,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='DEBIT' and entry_status='SCHEME' ".$date.") as DISCOUNT
							 
							from cash_register limit 1";
			 
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
				
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
		public function getOtherAmtByConsumerId($consumer_id=NULL,$type=NULL)
		{
			$stmt = $this->_db_table->getAdapter();
			$query = "SELECT SUM(cr_amount) AS other_amount_debit from cash_register WHERE cr_entry_type='DEBIT' and 
					  entry_status='".$type."' AND consumer_id=".$consumer_id." AND cr_status != 'INACTIVE'";
			$result = $stmt->fetchRow($query);
			$other_amount_debit = $result["other_amount_debit"];
			
			if($other_amount_debit==NULL){
				$other_amount_debit = 0;
			}
			
			$query = "SELECT SUM(cr_amount) AS other_amount_credit from cash_register WHERE cr_entry_type='CREDIT' and
					  entry_status='".$type."' AND consumer_id=".$consumer_id." AND cr_status != 'INACTIVE'";
			$result = $stmt->fetchRow($query);
			$other_amount_credit = $result["other_amount_credit"];
				
			if($other_amount_credit==NULL){
				$other_amount_credit = 0;
			}
			return ($other_amount_debit-$other_amount_credit);
		}
		 
		public function checkEntryAmtByConsumerId($consumer_id=NULL,$status=NULL,$datetime=NULL)
		{
			$query = "SELECT * from cash_register WHERE entry_status='".$status."' AND consumer_id=".$consumer_id." 
						and cr_status != 'INACTIVE'";
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		 
			if (count($result) == 0) {
				return false;
			}
			return $result;
		}
		
		public function getTotalTariffAmount($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL)
		{
			$join=" inner join consumers c on cr.consumer_id=c.consumer_id";
			
			$month_data=" (month(timestamp)=".$month." and year(timestamp)=".$year." and cr_status!='INACTIVE')";
			
	    	$condition=" and type_of_me=".$consumer_type_id." and site_id=".$site_id."";
	    	
	    	    $query=" Select (select sum(cr_amount) from cash_register cr ".$join." where ".$month_data.$condition." and 
									cr_entry_type='DEBIT' and entry_status='CHG' ) as billed,
								(select sum(cr_amount) from cash_register cr ".$join." where ".$month_data.$condition." and 
									cr_entry_type='DEBIT' and entry_status in ('EED','MED') ) as bill_eed,
								(select sum(cr_amount) from cash_register cr ".$join." where ".$month_data.$condition." and 
									cr_entry_type='DEBIT' and entry_status='SCHEME' ) as bill_scheme,
								(select sum(cr_amount) from cash_register cr ".$join." where ".$month_data.$condition." and 
										cr_entry_type='CREDIT' and entry_status='CHG' ) as collected,
								(select sum(cr_amount) from cash_register cr ".$join." where ".$month_data.$condition." and 
										cr_entry_type='CREDIT' and entry_status  in ('EED','MED') ) as coll_eed,
								(select sum(cr_amount) from cash_register cr ".$join." where ".$month_data.$condition." and 
										cr_entry_type='CREDIT' and entry_status='SCHEME' ) as coll_dis
									
	    			 from consumers limit 1";
	    	    
	    	$stmt= $this->_db_table->getAdapter()->query($query);
	    	
	    	$result = $stmt->fetchAll();
	    	
	    	if (count($result) == 0) {
	    		return false;
	    	}
	    	return $result[0];
		}
		
		public function getTotalBilledForEntryType($curr_date=NULL,$last_date=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$type_of_me=null)
		{
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
     				inner join sites s on c.site_id=s.site_id ";
			 
			if($site_id !=NULL){
				$where =" and c.site_id in (".$site_id.") and cr.cr_status!='INACTIVE' and c.site_id!=1";
			}else if ($state_id !=NULL){
				$where =" and s.state_id in (".$state_id.") and c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE' and c.site_id!=1";
			}else{
				$where =" and c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE' and c.site_id!=1";
			}
			 
			if($curr_date!=NULL && $last_date!=NULL){
				//$month_val = date("m", strtotime($month));
				$where.=" and date(timestamp)<='".$curr_date."' and date(timestamp)>='".$last_date."' ";
			}
			if($type_of_me!=NULL){
				$where.=" and c.type_of_me=".$type_of_me; 
			}
			 
			 $query="SELECT (select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='EED' and cr_entry_type='DEBIT' ".$where.") as EED,
       						(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='MED' and cr_entry_type='DEBIT' ".$where.") as MED,
       						(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='CHG' and cr_entry_type='DEBIT' ".$where.") as CHG,
             				(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='ACT' and cr_entry_type='DEBIT' ".$where.") as ACT,
             				(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='WATER' and cr_entry_type='DEBIT' ".$where.") as WATER,
             				(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='OTHERS' and cr_entry_type='DEBIT' ".$where.") as OTHERS,
							(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='METER' and cr_entry_type='DEBIT' ".$where.") as METER,
							(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='SCHEME' and cr_entry_type='DEBIT' ".$where.") as SCHEME,
							
							(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='EED' and cr_entry_type='DEBIT' ".$where.") as EED_count,
       						(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='MED' and cr_entry_type='DEBIT' ".$where.") as MED_count,
       						(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='CHG' and cr_entry_type='DEBIT' ".$where.") as CHG_count,
             				(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='ACT' and cr_entry_type='DEBIT' ".$where.") as ACT_count,
             				(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='WATER' and cr_entry_type='DEBIT' ".$where.") as WATER_count,
             				(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='OTHERS' and cr_entry_type='DEBIT' ".$where.") as OTHERS_count, 
							(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='METER' and cr_entry_type='DEBIT' ".$where.") as METER_count,
							(select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='SCHEME' and cr_entry_type='DEBIT' ".$where.") as SCHEME_count
     				from cash_register limit 1";
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		
		
		public function getOutstandingDataByConsumerId($site_arr=null,$consumer_id=null,$entry_status=null,$ispost_paid=NULL)
		{
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
					inner join sites s on c.site_id=s.site_id where c.site_id!=1 and s.site_status!='Disable' and ";
			 
			if($ispost_paid!=NULL){
				if($ispost_paid=='0'){
					 $join.=" c.type=0 and ";
				}else{
					 $join.=" c.type!=0 and ";
				}
			}
			
			$consumer_id_status="";
			if($consumer_id!=NULL){
				$consumer_id_status=" and c.consumer_id='".$consumer_id."'";
			}
			
			$where ="";
			
			if($site_arr !=NULL){
				$where =" and c.site_id in (".implode(",", $site_arr).") ";
			}
			
			$entrys_status="";
			if($entry_status!=NULL){
				$entrys_status=" and entry_status='".$entry_status."'";
			}
			
			
			$stmt = $this->_db_table->getAdapter();
			    $query = "SELECT SUM(cr_amount) AS debit_amount from cash_register cr ".$join." cr_entry_type='DEBIT' ".$entrys_status.$where." and entry_status!='SCHEME' AND
						cr_status != 'INACTIVE'".$consumer_id_status;
		  
			$result = $stmt->fetchRow($query);
			$debit_amount = $result["debit_amount"];
			if($debit_amount==NULL)
			{
				$debit_amount = 0;
			}
			 
			     $querys = "SELECT SUM(cr_amount) AS credit_amount from cash_register cr ".$join."  cr_entry_type in ('CREDIT','ACTIVATION','DISCOUNT') ".$entrys_status.$where." and entry_status not in('SCHEME','SD') AND
					 cr_status != 'INACTIVE'".$consumer_id_status;
		 
			$results = $stmt->fetchRow($querys);
			$credit_amount = $results["credit_amount"];
			if($credit_amount==NULL)
			{
				$credit_amount = 0;
			}
				
			$outstanding=$debit_amount-$credit_amount;
			return $outstanding;
		}
	
		public function getProrataBillByConsumerID($consumer_id=NULL,$date=NULL)
		{
			$query=" Select cr.cr_amount as amount from cash_register cr inner join consumers c on cr.consumer_id=c.consumer_id 
						where c.consumer_id=".$consumer_id." and date(timestamp)='".$date."'";
		 
			$stmt= $this->_db_table->getAdapter()->query($query);
		
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return 0;
			}
			return $result[0]["amount"];
		}
		public function getSchemeConsumers($month=NULL,$year=NULL,$site_id=NULL,$consumer_type_id=NULL)
		{
		
			$query="SELECT c.* FROM tara.cash_register cr inner join consumers c on c.consumer_id=cr.consumer_id
						where month(cr.timestamp)=".$month." and year(cr.timestamp)=".$year." and entry_status='SCHEME' 
						and c.type_of_me=".$consumer_type_id." and c.site_id=".$site_id;
						
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return 0;
			}
			return $result;
		}
		public function getMonthlyDueBySiteId($site_id=null,$month=null,$year=null,$act=NULL,$consumer_id=NULL,$collection_sheet=NULL,$is_postpaid=NULL,$day=NULL,$activity=NULL,$entry_status=NULL)
		{
			
			$cond="";
			if($month != null ) {
				$cond=" AND MONTH(cr.timestamp)='".$month."'";
			}
			if($year !=null) {
				$cond .=" AND YEAR(cr.timestamp)='".$year."'";
			}
			if($act==NULL){
				$act_val=",'ACTIVATION'";
				$act_data="";
			}else{
				$act_val="";
				$act_data=",'ACT'";
			}
			if($consumer_id!=NULL){ 
				$where=" and c.consumer_id=".$consumer_id;
			}else{
				$where=""; 
			}
			$entrys_status="";
			if($entry_status!=NULL){
				$entrys_status=" and entry_status='".$entry_status."'";
			}
			$join="";$cond1="";
			if($collection_sheet!=NULL){
				$join=" inner join packages p on p.package_id=cr.package_id";
				if($is_postpaid==0){
					$cond1 .=" and p.is_postpaid=0";
				}elseif($is_postpaid==1){
					$cond1 .=" and p.is_postpaid=2";
				}else{
					$cond1 .=" and p.is_postpaid!=0";
				}
				if($is_postpaid==1){
					if($day!=NULL && $activity!=NULL){
						$cond .=" AND day(timestamp)='".$day."'";
					}else{
						$cond .=" AND day(timestamp)!='".$day."'";
					}
				}
			}
			
			
			$stmt = $this->_db_table->getAdapter();
			$query = "select (SELECT SUM(cr_amount)  from cash_register cr inner join consumers c  on c.consumer_id=cr.consumer_id ".$join."
					  WHERE cr_entry_type='DEBIT' AND cr_status != 'INACTIVE' and cr.entry_status not in ('OTHERS','SCHEME'".$act_data.") 
					  		 AND c.site_id=".$site_id." ".$cond.$cond1.$where.$entrys_status.") as debit_amount,
					  		 (SELECT SUM(cr_amount) from cash_register cr inner join consumers c  on c.consumer_id=cr.consumer_id 
					  WHERE cr_entry_type in ('CREDIT','DISCOUNT'".$act_val.") AND cr_status != 'INACTIVE' and cr.entry_status not in ('OTHERS','SCHEME','SD')
					  		 AND c.site_id=".$site_id." ".$cond.$where.$entrys_status.") as credit_amount from cash_register limit 1";
		   
			$result = $stmt->fetchAll($query);
			 
			return $result[0];
		}
		
		public function getMonthDebitByConsumerId($consumer_id=null,$month=null,$year=null)
		{
			$cond="";
			if($month != null ) {
				$cond=" AND MONTH(timestamp)='".$month."'";
			}
			if($year !=null) {
				$cond .=" AND YEAR(timestamp)='".$year."'";
			}
		
		
			$stmt = $this->_db_table->getAdapter();
			 $query = "SELECT SUM(cr_amount) as amount  from cash_register cr inner join consumers c  on c.consumer_id=cr.consumer_id
					  WHERE cr_entry_type='DEBIT' AND cr_status != 'INACTIVE' and cr.entry_status in ('CHG','SCHEME')
					  		 AND c.consumer_id=".$consumer_id." ".$cond." limit 1";
			 	 
			$result = $stmt->fetchAll($query);
			if (count($result) == 0) {
				return false;
			}
			return $result[0]["amount"];
		}
		public function getACTCHGBills($site_id=NULL,$curr_date=NULL,$last_date=NULL,$user_id=NULL,$role_site_id=NULL,$er_entry_type=NULL,$range=NULL,$package_type=NULL)
		{
			$joins="";	
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
					inner join sites s on c.site_id=s.site_id ";
			
			$where=" where c.site_id!=1 and";		
			if($site_id==NULL){
				$where .="  c.site_id in (".implode(",", $role_site_id).") and cr.cr_status!='INACTIVE'";
			}else{
				$where .="  c.site_id=".$site_id." and cr.cr_status!='INACTIVE'";
			}
			 
			if($range!=NULL){
				$date=" and date(timestamp)>='".$last_date."' and date(timestamp)<='".$curr_date."'";
			}else{
				if($curr_date !=NULL && $last_date!=NULL){
					$month_val = date("m", strtotime($curr_date));
					$date=" and month(timestamp)=".$month_val." and year(timestamp)=".$last_date;
				}
			}
			
			if($user_id!=NULL){
				$where.=" and user_id=".$user_id." and transaction_type!='(W)'";
			}
			
			if($er_entry_type=='DEBIT') { 
				$type= "'DEBIT'";
			}else{
				$type= "'CREDIT','ACTIVATION'"; 
			}
			
			if($package_type!=NULL){
				if($package_type==2){
					$join.=" inner join packages p ON p.package_id=cr.package_id";
					$where.=" and p.is_postpaid=0"; 
				}elseif($package_type==1){
					$join.=" inner join packages p ON p.package_id=cr.package_id ";
					$where.=" and p.is_postpaid!=0"; 
				} 
			}
			  $query="select (select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in (".$type.") ".$date." and entry_status not in ('SCHEME','DISCOUNT')) as CREDIT,
							 
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and  
								cr_entry_type in (".$type.") ".$date." and entry_status !='SCHEME') as DEBIT,
							 
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in (".$type.") ".$date." and entry_status ='ACT') as ACT,
							(select sum(cr_amount) from cash_register cr ".$join.$joins."  ".$where." and
								cr_entry_type in (".$type.") ".$date." and entry_status ='CHG') as CHG,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in (".$type.") ".$date." and entry_status ='EED') as EED,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in (".$type.") ".$date." and entry_status ='MED') as MED,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in (".$type.") ".$date." and entry_status ='OTHERS') as OTHERS,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in (".$type.") ".$date." and entry_status ='WATER') as WATER,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in ('CREDIT') ".$date." and entry_status ='SD') as SD,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in ('CREDIT') ".$date." and entry_status ='METER') as METER 								
							
							from cash_register limit 1";
		
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
		public function getTotalBilledAmountBySegment($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL) 
		{
			$join=" inner join consumers c on cr.consumer_id=c.consumer_id ";
				
			$month_data=" (month(timestamp)=".$month." and year(timestamp)=".$year." and cr_status!='INACTIVE') 
							and type_of_me=".$consumer_type_id." and c.site_id=".$site_id."";
				
			
			  $query=" Select (select sum(cr_amount) from cash_register cr ".$join." where ".$month_data." and 
									cr_entry_type='DEBIT' and entry_status in ('CHG','SCHEME') and c.type=0) as billed_pre,
							(select sum(cr_amount) from cash_register cr ".$join." where ".$month_data." and 
									cr_entry_type='DEBIT' and entry_status in ('CHG','SCHEME') and c.type!=0) as billed_post,
	    					(select sum(cr_amount) from cash_register cr ".$join." where ".$month_data." and 
	    							cr_entry_type in ('CREDIT','ACTIVATION') and entry_status in ('CHG','ACT','OTHERS','SCHEME')) as collected
	    			 from consumers limit 1";
			 
			$stmt= $this->_db_table->getAdapter()->query($query);
		
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
		public function getTotalOTPAndOthersBySegment($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL)
		{
			$join=" inner join consumers c on cr.consumer_id=c.consumer_id";
		
			$month_data=" (month(timestamp)=".$month." and year(timestamp)=".$year." and cr_status!='INACTIVE')
							and type_of_me=".$consumer_type_id." and c.site_id=".$site_id."";
		
				
			$query=" Select (select sum(cr_amount) from cash_register cr ".$join." where ".$month_data." and
									cr_entry_type='DEBIT' and entry_status='ACT') as billed_OTP,
							(select sum(cr_amount) from cash_register cr ".$join." where ".$month_data." and
									cr_entry_type='DEBIT' and entry_status='OTHERS') as billed_OTHERS
							
	    			from consumers limit 1";
		    
			$stmt= $this->_db_table->getAdapter()->query($query);
		
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
		public function getConsumerDefaulter($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL)
		{
				
			$query="select cr.* from cash_register cr inner join consumers c on cr.consumer_id=c.consumer_id
					 where cr.cr_entry_type='CREDIT' and cr.entry_status='CHG' and cr.cr_status!='INACTIVE' 
	          		 and month(cr.timestamp)=".$month." and year(cr.timestamp)=".$year." and c.type_of_me=".$consumer_type_id." 
	          				and c.site_id=".$site_id." group by cr.consumer_id";
			$stmt= $this->_db_table->getAdapter()->query($query);
			
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
			return $result;
		}
		
		public function getOnlyBilledForARPU($curr_date=NULL,$last_date=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$segment=NULL,$consumer_type=NULL,$onlyChg=NULL)
		{
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
     				inner join sites s on c.site_id=s.site_id";
			$onlyenergy="";
			if($onlyChg!=NULL){
				$onlyenergy=" inner join packages p on cr.package_id=p.package_id ";
			}
			if($segment!=NULL || $consumer_type!=NULL){
				$join.=" inner join consumer_packages cp on c.consumer_id=cp.consumer_id ";
			}
		  
			if($site_id !=NULL){
				$where =" and c.site_id in (".$site_id.") and cr.cr_status!='INACTIVE'";
			}else if ($state_id !=NULL){
				$where =" and s.state_id in (".$state_id.") and c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE'";
			}else{
				$where =" and c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE'";
			}
			$where .=" and s.site_id!=1 ";
			if($curr_date!=NULL && $last_date!=NULL){
				$where.=" and date(timestamp)<='".$curr_date."' and date(timestamp)>='".$last_date."' "; 
			}
		    
			if($segment!=NULL){
				$where.=" and c.type_of_me in (".$segment.") ";
			}
			 
			if($consumer_type!=NULL){ 
				$where.=" and p.light_load in (".$consumer_type.")";
			}
			 $query="SELECT (select sum(cr_amount) FROM cash_register cr ".$join." where entry_status in ('CHG','ACT','SCHEME') and c.type=0 and cr_entry_type='DEBIT' ".$where.") as Pre_CHG,
       					     (select sum(cr_amount) FROM cash_register cr ".$join." where entry_status in ('CHG','ACT','SCHEME') and c.type!=0 and cr_entry_type='DEBIT' ".$where.") as Post_CHG,
       					     (select sum(cr_amount) FROM cash_register cr ".$join." where entry_status in ('CHG','ACT','SCHEME') and cr_entry_type='DEBIT' ".$where.") as Total_CHG,
							 
							 (select sum(cr_amount) FROM cash_register cr ".$join." where c.type=0 and entry_status not in ('SCHEME','CHG') and cr_entry_type='DEBIT' ".$where.") as Pre_debit,
       					     (select sum(cr_amount) FROM cash_register cr ".$join." where c.type!=0 and entry_status not in ('SCHEME','CHG') and cr_entry_type='DEBIT' ".$where.") as Post_debit,
       					     (select sum(cr_amount) FROM cash_register cr ".$join." where c.type=0  and entry_status not in ('SCHEME','DISCOUNT','CHG') and cr_entry_type in ('ACTIVATION','CREDIT') ".$where.") as Pre_credit,
       					     (select sum(cr_amount) FROM cash_register cr ".$join." where c.type!=0 and entry_status not in ('SCHEME','DISCOUNT','CHG') and cr_entry_type in ('ACTIVATION','CREDIT') ".$where.") as Post_credit,
							 (select sum(cr_amount) FROM cash_register cr ".$join." where c.type=0 and entry_status in ('SD') and cr_entry_type in ('CREDIT') ".$where.") as Pre_SD, 
							 (select sum(cr_amount) FROM cash_register cr ".$join." where c.type!=0 and entry_status in ('SD') and cr_entry_type in ('CREDIT') ".$where.") as Post_SD,
							 
							 (select sum(cr_amount) FROM cash_register cr ".$join.$onlyenergy." where p.is_postpaid=0 and entry_status in ('CHG') and cr_entry_type='DEBIT' ".$where.") as Pre_debit_chg,
       					     (select sum(cr_amount) FROM cash_register cr ".$join.$onlyenergy." where p.is_postpaid!=0 and entry_status in ('CHG') and cr_entry_type='DEBIT' ".$where.") as Post_debit_chg,
       					     (select sum(cr_amount) FROM cash_register cr ".$join.$onlyenergy." where p.is_postpaid=0  and entry_status in ('CHG') and cr_entry_type in ('CREDIT') ".$where.") as Pre_credit_chg,
       					     (select sum(cr_amount) FROM cash_register cr ".$join.$onlyenergy." where p.is_postpaid!=0 and entry_status in ('CHG') and cr_entry_type in ('CREDIT') ".$where.") as Post_credit_chg		 					 
       					      
     				from cash_register limit 1";  
			///echo    $query;exit; 
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		  
		public function getEntryDetailByEntryType($entry_status=NULL)
		{
			$query=" select * from cash_register where entry_status='".$entry_status."' and cr_status!='INACTIVE'";
			$stmt= $this->_db_table->getAdapter()->query($query);
			
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
			return $result;
		}
		
		public function getMonthlyBilledByConsumerId($consumer_id=NULL,$month=null,$year=NULL,$package_id=NULL)
		{
			$month_data=" month(timestamp)=".$month." and year(timestamp)=".$year." and cr_status!='INACTIVE' and consumer_id=".$consumer_id." and package_id=".$package_id;

		     $query=" select sum(cr_amount) as total_bill from cash_register  where ".$month_data." and
									cr_entry_type='DEBIT' and entry_status='CHG'   limit 1";
		   
			$stmt= $this->_db_table->getAdapter()->query($query);
		
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return 0;
			}
			return $result[0]["total_bill"];
		}
		
		public function getTotalCollectionByCollectionId($curr_date=NULL,$last_date=NULL,$site_id=NULL,$agent_id=null,$user_id=NULL)
		{
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
     				inner join sites s on c.site_id=s.site_id ";
		
			$where =" and c.site_id in (".$site_id.") and cr.cr_status!='INACTIVE'";
				
			if($curr_date!=NULL && $last_date!=NULL){
				$where.=" and date(timestamp)<='".$curr_date."' and date(timestamp)>='".$last_date."' ";
			}
			
			if($agent_id!=NULL){
				if($user_id==NULL){
					$user_id="''";
				}
				$where.=" and ((cr.user_id=".$agent_id." and cr.transaction_type in ('(M)','(S)')) or
						 (cr.user_id=".$user_id." and cr.transaction_type in ('(W)')))";
			}
			
			  $query="SELECT (select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='EED' and cr_entry_type='CREDIT' ".$where.") as EED,
       						(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='MED' and cr_entry_type='CREDIT' ".$where.") as MED,
       						(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='CHG' and cr_entry_type='CREDIT' ".$where.") as CHG,
             				(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='ACT' and cr_entry_type='ACTIVATION' ".$where.") as ACT,
             				(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='WATER' and cr_entry_type='CREDIT' ".$where.") as WATER,
             				(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='OTHERS' and cr_entry_type='CREDIT' ".$where.") as OTHERS,
							(select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='SD' and cr_entry_type='CREDIT' ".$where.") as SD
     				from cash_register limit 1";
	 
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		
		public function getOpeningBalanceData($curr_date=NULL,$last_date=NULL,$site_id=NULL)
		{ 
			$where =" and site_id in (".implode(",", $site_id).") ";
			$init_date="2018-04-01";
			
			  $query="SELECT (select sum(cr_amount) FROM cash_register cr inner join consumers c on c.consumer_id=cr.consumer_id  where  entry_status not in ('DISCOUNT','SCHEME') and cr_entry_type in ('CREDIT','ACTIVATION') 
								and date(timestamp)<'".$last_date."' and date(timestamp)>='".$init_date."' ".$where.") as collected,
       					   (select sum(amount) FROM collections where date(curr_timestamp)<'".$last_date."' and date(curr_timestamp)>='".$init_date."' ".$where.") as bankdeposit
     				from cash_register limit 1";  
		
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		
		public function getTotalDebitAndCredit($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$type=NULL)
		{
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
     				inner join sites s on c.site_id=s.site_id ";
		
			if($site_id !=NULL){
				$where =" where c.site_id in (".$site_id.") and cr.cr_status!='INACTIVE'";
			}else if ($state_id !=NULL){
				$where =" where s.state_id in (".$state_id.") and c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE' and s.site_id!=1";
			}else{
				$where =" where c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE' and s.site_id!=1";
			}
			if($type=='DEBIT'){
				$where.=" and cr_entry_type='DEBIT' and entry_status not in ('SCHEME') ";
			}else{
				$where.=" and cr_entry_type in ('CREDIT','ACTIVATION') and entry_status not in ('SD','SCHEME') ";
			}
			
			$query="select sum(cr_amount) as amount, c.consumer_id from cash_register  cr ".$where." group by c.consumer_id order by debit desc";
			  
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		
		public function getDiscountByConsumerId($consumer_id=NULL,$type=NULL)
		{
			$stmt = $this->_db_table->getAdapter();
			$query = "SELECT SUM(cr_amount) AS discount_amt from cash_register WHERE cr_entry_type='DISCOUNT' and 
					  entry_status='".$type."' AND consumer_id=".$consumer_id." AND cr_status != 'INACTIVE'";
					  
			$result = $stmt->fetchRow($query);
			$discount_amt = $result["discount_amt"];
			
			if($discount_amt==NULL){
				$discount_amt = 0;
			}
			
		 
			return $discount_amt;
		}
		
		public function getChargesByConsumerId($consumer_id=NULL) 
		{
			 $where =" and consumer_id in (".$consumer_id.") and cr_status!='INACTIVE'";
				
		     $query="SELECT (select sum(cr_amount) FROM cash_register where entry_status='EED' and cr_entry_type='DEBIT' ".$where.") as EED,
       						(select sum(cr_amount) FROM cash_register where entry_status='MED' and cr_entry_type='DEBIT' ".$where.") as MED,
       						(select sum(cr_amount) FROM cash_register where entry_status='CHG' and cr_entry_type='DEBIT' ".$where.") as CHG,
             				(select sum(cr_amount) FROM cash_register where entry_status='ACT' and cr_entry_type='DEBIT' ".$where.") as ACT,
             				(select sum(cr_amount) FROM cash_register where entry_status='WATER' and cr_entry_type='DEBIT' ".$where.") as WATER,
             				(select sum(cr_amount) FROM cash_register where entry_status='OTHERS' and cr_entry_type='DEBIT' ".$where.") as OTHERS, 
							(select sum(cr_amount) FROM cash_register where entry_status='METER' and cr_entry_type='DEBIT' ".$where.") as METER,
							(select sum(cr_amount) FROM cash_register where cr_entry_type='DISCOUNT' ".$where.") as DISCOUNT 
     				from cash_register limit 1";
	 
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		
		public function getTotalOtpDueSiteWise($site_id=NULL)
		{
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
     				inner join sites s on c.site_id=s.site_id ";
		
			if($site_id !=NULL){
				$where =" and c.site_id = ".$site_id." and cr.cr_status!='INACTIVE'";
			}else{
				$where =" and cr.cr_status!='INACTIVE'";
			} 
			 
			$query="SELECT (select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='ACT' and cr_entry_type in ('ACTIVATION','DISCOUNT') ".$where.") as ACT,
       					   (select sum(cr_amount) FROM cash_register cr ".$join." where entry_status='ACT' and cr_entry_type='DEBIT' ".$where.") as Bill_ACT 
             		from cash_register limit 1"; 
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		
		public function getOutstandingBySiteIdAndFilter($site_id=NULL,$start_date=NULL,$end_date=NULL,$user_id=NULL)
		{
			date_default_timezone_set('Asia/Kolkata');
			if($start_date==NULL && $end_date==NULL){
				$timestamp=date('Y-m-d H:i:s');
				$trans_date=date_parse_from_format("Y-m-d", $timestamp);
				$month= $trans_date["month"];
				$year= $trans_date["year"];
				$total_days_in_month=cal_days_in_month(CAL_GREGORIAN, $month, $year);

				$start_date=$year."-".$month."-".$total_days_in_month;
				$last_date=$year."-".$month."-01";
				
				$next='-1 month';
				 $last_date_mtr = date('Y-m-d H:i:s', strtotime($timestamp ."-".$total_days_in_month." day"));
				
				$trans_dates=date_parse_from_format("Y-m-d", $last_date_mtr);
				$months= $trans_dates["month"];
				$years= $trans_dates["year"];
				$total_days_in_months=cal_days_in_month(CAL_GREGORIAN, $months, $years);
				
				$last_start_date=$years."-".$months."-".$total_days_in_months;
				$last_last_date=$years."-".$months."-01";
			}else{
				$trans_dates=date_parse_from_format("Y-m-d", date('Y-m-d H:i:s', strtotime($start_date ."-10 day")));
				$months= $trans_dates["month"];
				$years= $trans_dates["year"];
				$total_days_in_months=cal_days_in_month(CAL_GREGORIAN, $months, $years);
				
				$last_start_date=$years."-".$months."-".$total_days_in_months;
				$last_last_date=$years."-".$months."-01";
				
				$last_date=$start_date; 
				$start_date=$end_date;
				
			}
			
			
				
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
					inner join sites s on c.site_id=s.site_id where c.site_id!=1 and";
				
			if($site_id !=NULL){
				$where ="  c.site_id in (".implode(",", $site_id).") and cr.cr_status!='INACTIVE'";
			}else{
				$where ="  cr.cr_status!='INACTIVE'";
			}
			
		 
		
			$bill_date=" and ((date(timestamp)>='".$last_date."' and date(timestamp)<='".$start_date."' and c.type=0) 
						or (date(timestamp)>='".$last_last_date."' and date(timestamp)<='".$last_start_date."' and c.type!=0)) ";
				
			$bill_pre_date=" and date(timestamp)>='".$last_date."' and date(timestamp)<='".$start_date."' and c.type=0";
				
			$bill_mtr_date=" and (date(timestamp)>='".$last_last_date."' and date(timestamp)<='".$last_start_date."' and c.type!=0) ";
				
			$date=" and date(timestamp)>='".$last_date."' and date(timestamp)<='".$start_date."'";
			
			$user="";
			if($user_id!=NULL){
				$user=" and (cr.user_id=".$user_id."  and cr.transaction_type in ('(M)','(S)'))";
			}
		     
		  $query="select (select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type  in ('CREDIT','ACTIVATION') and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$date.") as CREDIT,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type in ('CREDIT','ACTIVATION') and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$date.$user.") as My_CREDIT,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$bill_date.") as DEBIT,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE'  ".$bill_pre_date.") as Pre_DEBIT,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type='DEBIT' and entry_status not in ('SCHEME') and cr_status!='INACTIVE' ".$bill_mtr_date.") as MTR_DEBIT,
								(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type='CREDIT' and entry_status  in ('SD') and cr_status!='INACTIVE'  ".$bill_pre_date.") as Pre_DEBIT_SD,
							(select sum(cr_amount) from cash_register cr ".$join."  ".$where." and
								cr_entry_type='CREDIT' and entry_status  in ('SD') and cr_status!='INACTIVE' ".$bill_mtr_date.") as MTR_DEBIT_SD 
							from cash_register limit 1";
		 
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
		public function getCashRegister($site_id=NULL,$site_id_on=NULL,$start_date=NULL,$end_date=NULL,$collection_type=NULL,$skip_count=NULL,$userVal=NULL,$consumer_id=NULL){
			
			$entry="";
			
			if($site_id_on!=NULL){
				$where= " where cr.cr_entry_type in ('CREDIT','ACTIVATION') and entry_status!='SCHEME' and s.site_id =".$site_id_on." and s.site_status!='Disable'";
			}elseif($site_id!=NULL){
				$where= " where cr.cr_entry_type in ('CREDIT','ACTIVATION') and entry_status!='SCHEME' and s.site_id in (".implode(",", $site_id).") and s.site_status!='Disable'";
			}else{
				$where= " where cr.cr_entry_type in ('CREDIT','ACTIVATION') and entry_status!='SCHEME' and s.site_status!='Disable' ";
			}
			
			if($consumer_id!=NULL){
				$where.=" and c.consumer_id=".$consumer_id;
			}
			 
			$date="";
			if($start_date!=NULL && $end_date!=NULL){
				$date=" and date(timestamp)<='".$end_date."' and date(timestamp)>='".$start_date."'";
			}else{
				$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp_date = $date->toString("yyyy-MM-dd HH:mm:ss");
				$date=" and month(timestamp)=month('".$timestamp_date."') and year(timestamp)=year('".$timestamp_date."')";
			}
			$collection="";
			if($collection_type=='Other'){
				$collection=" and cr.user_id!='".$userVal."'";
			}elseif($collection_type==1){  
				$collection=" and cr.user_id='".$collection_type."'";
			}
			  
			$skip=" order by cr.timestamp desc ";
			if($skip_count!=NULL){
				$skip.=" limit ".$skip_count.",50";
			}else{
				$skip.=" limit 0,50";
			}
			  
		    $query = "select cr.* from consumers c inner join cash_register cr on c.consumer_id=cr.consumer_id inner join sites s on c.site_id=s.site_id 
								".$where.$date.$collection.$entry.$skip;
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			 
			if (count($result) == 0) {
				return false;
			}
			$cash_register_object_arr = array();
			foreach ($result as $row)
			{
				$cash_register_object = new Application_Model_CashRegister($row);
				$cash_register_object->__set("consumer_id", $row["consumer_id"]); 
				$cash_register_object->__set("transaction_id", $row["transaction_id"]);
				$cash_register_object->__set("cr_amount", $row["cr_amount"]);
				$cash_register_object->__set("entry_status", $row["entry_status"]);
				$cash_register_object->__set("timestamp", $row["timestamp"]);
				$cash_register_object->__set("cr_entry_type", $row["cr_entry_type"]);
				$cash_register_object->__set("user_id", $row["user_id"]);
				$cash_register_object->__set("receipt_number", $row["receipt_number"]);
				 
				array_push($cash_register_object_arr,$cash_register_object);
			}
			return $cash_register_object_arr;
		}
		
		
		public function getCashRegisterCount($site_id=NULL,$site_id_on=NULL,$start_date=NULL,$end_date=NULL,$collection_type=NULL,$skip_count=NULL,$userVal=NULL,$consumer_id=NULL){
			
			 
			if($site_id_on!=NULL){
				$where= " where cr.cr_entry_type in ('CREDIT','ACTIVATION') and entry_status!='SCHEME' and s.site_id =".$site_id_on." and s.site_status!='Disable'";
			}elseif($site_id!=NULL){
				$where= " where cr.cr_entry_type in ('CREDIT','ACTIVATION') and entry_status!='SCHEME' and s.site_id in (".implode(",", $site_id).") and s.site_status!='Disable'";
			}else{
				$where= " where cr.cr_entry_type in ('CREDIT','ACTIVATION') and entry_status!='SCHEME' and s.site_status!='Disable' ";
			}
			
			
			if($consumer_id!=NULL){
				$where.=" and c.consumer_id=".$consumer_id;
			}
			 
			$date="";
			if($start_date!=NULL && $end_date!=NULL){
				$date=" and date(timestamp)<='".$end_date."' and date(timestamp)>='".$start_date."'";
			}else{
				$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp_date = $date->toString("yyyy-MM-dd HH:mm:ss");
				$date=" and month(timestamp)=month('".$timestamp_date."') and year(timestamp)=year('".$timestamp_date."')";
			}
			$collection="";
			if($collection_type=='Other'){
				$collection=" and cr.user_id!='".$userVal."'";
			}elseif($collection_type==1){  
				$collection=" and cr.user_id='".$collection_type."'";
			}
			  
			$skip=" order by cr.timestamp desc ";
			if($skip_count!=NULL){
				$skip.=" limit ".$skip_count.",50";
			}else{
				$skip.=" limit 0,50";
			}
			
		    $query = "select count(*) as cash_count from consumers c inner join cash_register cr on c.consumer_id=cr.consumer_id inner join sites s on c.site_id=s.site_id 
								".$where.$date.$collection;
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			 
			if (count($result) == 0) {
				return false;
			}
			
			return $result[0]["cash_count"];
		}
	public function getBillsByRangeSiteId($start_date=NULL,$end_date=NULL,$role_sites_id=NULL,$site_id=NULL,$collection_type=NULL,$userVal=NULL,$consumer_id=NULL)
		{
				if($site_id!=NULL){
					$site="s.site_id =".$site_id." and s.site_id!=1";
				}
				elseif($role_sites_id!=NULL){
					$site="s.site_id in (".implode(",", $role_sites_id).") and s.site_id!=1";
				}else{
					$site=" s.site_id!=1";
				}
				if($consumer_id!=NULL){
					$site.=" and c.consumer_id=".$consumer_id;
				}
				$date="";
				if($start_date!=NULL && $end_date!=NULL){
					$date=" and date(timestamp)<='".$end_date."' and date(timestamp)>='".$start_date."'";
				}else{
					$date_val = new Zend_Date();
					$date_val->setTimezone("Asia/Calcutta");
					$timestamp_date = $date_val->toString("yyyy-MM-dd HH:mm:ss");
					$date=" and month(timestamp)=month('".$timestamp_date."') and year(timestamp)=year('".$timestamp_date."')";
				}
			 
				if($collection_type=='Other'){
					$date.=" and cr.user_id!='".$userVal."'";
				}elseif($collection_type!=NULL){
					$date.=" and cr.user_id='".$collection_type."'"; 
				} 
				
				$join=" inner join consumers c ON c.consumer_id=cr.consumer_id 
					inner join sites s on c.site_id=s.site_id where ".$site." and cr.cr_status!='INACTIVE'";
				
			  	 
				   $query=" select (select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='DEBIT' and entry_status!='SCHEME' ".$date.") as BILL,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT'  and entry_status='CHG' ".$date.") as CHG,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='ACTIVATION' and entry_status='ACT' ".$date.") as ACT, 
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='EED' ".$date.") as EED,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='MED' ".$date.") as MED,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='WATER' ".$date.") as WATER,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='OTHERS' ".$date.") as OTHERS,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='DISCOUNT' ".$date.") as SCHEME,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='DEBIT' and entry_status='SCHEME' ".$date.") as DISCOUNT,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='SD' ".$date.") as SD,
							(select sum(cr_amount) from cash_register cr ".$join." and cr_entry_type='CREDIT' and entry_status='METER' ".$date.") as METER
							 
							from cash_register limit 1"; 
			  
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
				
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		 
		public function getAllTransactionByConsumerID($consumer_id=NULL)
		{ 
			$query=" Select * from cash_register where consumer_id=".$consumer_id." and cr_status!='INACTIVE' order by timestamp";
		
			$stmt= $this->_db_table->getAdapter()->query($query);
				
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result;
		}
		
		public function getBillingComparision($consumer_id=null,$entry_type=null,$month,$year,$lastMonth,$lastYear,$lastToLastMonth,$lastToLastyear){
			
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
					inner join packages p ON cr.package_id=p.package_id
					inner join sites s on c.site_id=s.site_id where cr.cr_status!='INACTIVE' and cr_entry_type='DEBIT' and cr.consumer_id=".$consumer_id;
			
			if($entry_type!=NULL){
				$join.=" and entry_status in (".$entry_type.")";
			}else{
				$join.=" and entry_status in ('CHG')";
			}
			
			$curr_date_pre=" and month(timestamp)=".$month." and year(timestamp)=".$year." and p.is_postpaid=0 ";
			$last_date_pre=" and month(timestamp)=".$lastMonth." and year(timestamp)=".$lastYear." and p.is_postpaid=0 ";
			$last_date_post=" and month(timestamp)=".$lastMonth." and year(timestamp)=".$lastYear." and p.is_postpaid!=0 ";
			$last_to_last_date_post=" and month(timestamp)=".$lastToLastMonth." and year(timestamp)=".$lastToLastyear." and p.is_postpaid!=0 ";
			
			$query=" select (select sum(cr_amount) from cash_register cr ".$join.$curr_date_pre.") as curr_month_pre,
							(select sum(cr_amount) from cash_register cr ".$join.$last_date_pre.") as last_month_pre,
							(select sum(cr_amount) from cash_register cr ".$join.$last_date_post.") as last_month_post,
							(select sum(cr_amount) from cash_register cr ".$join.$last_to_last_date_post.") as last_to_last_month_post
					from cash_register limit 1";
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
	 public function getCashRegisterTransactionByReceiptNo($consumer_id=null,$receipt=null,$user_id=null,$order_by=NULL,$cr_entry_type=NULL,$site_code=NULL){
		
		$where="";
		$join="";
		if($cr_entry_type=="DEBIT"){
			$where.=" and cr_entry_type='DEBIT' and s.site_code='".$site_code."'";
			$join=" inner join consumers c on c.consumer_id=cr.consumer_id
					inner join sites s on s.site_id=c.site_id";
		}else{
			$where.=" and cr_entry_type!='DEBIT'";
		}
		
		if($consumer_id!=null){
			$where.=" and cr.consumer_id=".$consumer_id;
		}
		if($receipt!=null){
			$where.=" and cr.receipt_number='".$receipt."'";
		}
		if($user_id!=null){
			$where.=" and cr.user_id=".$user_id;
		}
		
		
		if($order_by!=null){
			$where.=" order by cr.receipt_number desc";
		}else{
			$where.=" order by cr.timestamp desc";
		}
    	$query = "SELECT cr.* from cash_register cr ".$join." where cr_status!='INACTIVE' ".$where;
    	 
    	$stmt = $this->_db_table->getAdapter();
    	$resultData = $stmt->fetchAll($query);
    	 
    	return $resultData;

    }
	
		public function getMaxRecieptNumber($site_id=null){
			
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id";
			if($site_id!=null){
				$where=" and c.site_id=".$site_id;
			}else{
				$where=" and c.site_id=0";
			}
			
			$query=" select receipt_number from cash_register cr ".$join." where cr_entry_type='DEBIT' and cr_status!='INACTIVE' 
					 ".$where." order by timestamp desc limit 1";
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
			return $result[0]["receipt_number"];
		}
		
		public function getMaxUserNumber($user_id=null){
			
			$query=" select max(cast(receipt_number AS UNSIGNED)) as receipt_number from cash_register where 
						cr_entry_type in ('CREDIT','ACTIVATION') and cr_status!='INACTIVE' and user_id=".$user_id."
						order by timestamp desc limit 1";
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
			
			return $result[0]["receipt_number"];
		}
		
		public function getOutstandingConsumer($lastDate=NULL,$currentDate=NULL,$consumer_id=NULL){
			
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
					inner join sites s on c.site_id=s.site_id where cr.cr_status!='INACTIVE'";
			
			
			 $query=" select c.consumer_id,c.consumer_connection_id,c.consumer_status from cash_register cr ".$join." 
						and date(cr.timestamp)>='".$lastDate."' and date(cr.timestamp)<='".$currentDate."' and 
						cr.cr_entry_type in ('CREDIT','ACTIVATION') and cr.entry_status in ('CHG','EED','MED','WATER','ACT','OTHERS') and c.consumer_id=".$consumer_id;
			
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		}
		
		public function getTotalBillConsumerId($lastDate=NULL,$currentDate=NULL,$consumer_id=NULL){
			
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
					inner join sites s on c.site_id=s.site_id where cr.cr_status!='INACTIVE'";
			
			 
			 $query=" select sum(cr.cr_amount) as amount from cash_register cr ".$join." 
						and date(cr.timestamp)>='".$lastDate."' and date(cr.timestamp)<='".$currentDate."' and 
						cr.cr_entry_type in ('DEBIT') and cr.entry_status in ('CHG','EED','MED','WATER','ACT','OTHERS') and c.consumer_id=".$consumer_id;
	
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
			return $result[0]["amount"];
		}
		
		public function getBillCount($curr_date=NULL,$last_date=NULL,$site_id=NULL,$state_id=NULL,$role_sites_id=NULL)
		{
			$join=" inner join consumers c ON c.consumer_id=cr.consumer_id
     				inner join sites s on c.site_id=s.site_id ";
			 
			if($site_id !=NULL){
				$where =" and c.site_id in (".$site_id.") and cr.cr_status!='INACTIVE' and c.site_id!=1";
			}else if ($state_id !=NULL){
				$where =" and s.state_id in (".$state_id.") and c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE' and c.site_id!=1";
			}else{
				$where =" and c.site_id in (".implode(",", $role_sites_id).") and cr.cr_status!='INACTIVE' and c.site_id!=1";
			}
			 
			if($curr_date!=NULL && $last_date!=NULL){
				$where.=" and date(timestamp)<='".$curr_date."' and date(timestamp)>='".$last_date."' ";
			}
			 
			 
			$query="SELECT  (select count(distinct cr.consumer_id) FROM cash_register cr ".$join." where entry_status='CHG' and cr_entry_type='DEBIT' ".$where.") as CHG_count
             		 from cash_register limit 1";
			 
			$stmt= $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
		
		}
		
		public function checktransaction($month=NULL,$year=NULL,$consumer_id=NULL) {
			
			$query = "SELECT * FROM cash_register WHERE MONTH(`timestamp`)=".$month." AND YEAR(`timestamp`)=".$year." AND consumer_id=".$consumer_id." and 
						cr_status != 'INACTIVE' and cr_entry_type='DEBIT' and entry_status='CHG' ORDER BY timestamp desc";
			$stmt = $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
			
			if (count($result) == 0) {
				return false;
			}
		    
			return $result[0];
		}
		
		public function updateTransaction($cr_id=NULL,$transaction_id=NULL,$cr_amount=NULL)
		{
			$query ="update cash_register set cr_amount=".$cr_amount.",transaction_id='".$transaction_id."' where cr_id =".$cr_id;
			
			$stmt = $this->_db_table->getAdapter()->query($query);
			$result=$stmt->execute();
		
			if($result)
			{
				return true;
			}
		}
}