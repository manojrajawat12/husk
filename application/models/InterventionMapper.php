<?php
class Application_Model_InterventionMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Intervention();
	}

	public function addNewIntervention(Application_Model_Intervention $Intervention)
	{

		$data = array(
				'type' => $Intervention->__get("type"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getInterventionById($Intervention_id)
	{
		
		$result = $this->_db_table->find($Intervention_id);
		
		if( count($result) == 0 ) {
			$state = new Application_Model_Intervention();
			return $state;
		}
		$row = $result->current();
		$state = new Application_Model_Intervention($row);
	
		return $state;
	}
	
	public function getAllIntervention()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('id'));
		if( count($result) == 0 ) {
			return false;
		}
		$state_object_arr = array();
		foreach ($result as $row)
		{
			$state_object = new Application_Model_Intervention($row);
			array_push($state_object_arr,$state_object);
		}
		return $state_object_arr;
	}
	
	public function updateIntervention(Application_Model_Intervention $Intervention)
	{
		$data = array(
				'type' => $Intervention->__get("type"),
		);
		$where = "id = " . $Intervention->__get("id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	public function deleteInterventionById($id)
	{
		$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{

			return true;
		}
	}

}