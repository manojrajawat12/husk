<?php
class Application_Model_SmsGatewayMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_SmsGateway();
	}

	public function addNewSmsGateway(Application_Model_SmsGateway $smsGateway)
	{

		$data = array(
				'user_name' => $smsGateway->__get("user_name"),
				'password' => $smsGateway->__get("password"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getSmsGatewayById($gateway_id)
	{
		$result = $this->_db_table->find($gateway_id);
		if( count($result) == 0 ) {
			$gateway = new Application_Model_SmsGateway();
			return $gateway;
		}
		$row = $result->current();
		$gateway = new Application_Model_SmsGateway($row);
		return gateway;
	}
	
	public function getAllSmsGateway()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('gateway_id ASC'));
		
		if( count($result) == 0 ) {
			return $result;
		}else{
		$gateway_object_arr = array();
		foreach ($result as $row)
		{
			$sms_object = new Application_Model_SmsGateway($row);
			array_push($gateway_object_arr,$sms_object);
		}
		return $gateway_object_arr;
	}
	}
	
	public function updateSmsGateway(Application_Model_SmsGateway $smsGateway)
	{
	
		$gateway_id= $smsGateway->__get("gateway_id");
		$user_name = $smsGateway->__get("user_name");
		$password = $smsGateway->__get("password");
		
		$query ="update sms_gateway set user_name='".$user_name."', password='".$password."' where gateway_id=".$gateway_id;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		//echo $result;
		if($result)
		{
			return true;
		}
	}
	
	
	public function deleteSmsGatewayById($gateway_id)
	{
		$where = "gateway_id = " . $gateway_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}