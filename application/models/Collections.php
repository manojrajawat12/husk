<?php

class Application_Model_Collections
{
    private $id;
    private $site_id;
    private $timestamp;
    private $deposit_by;
    private $amount;
    private $deposit_location;
    private $attachment;
	private $user_id;
	private $curr_timestamp;
	private $verified_by;
    private $verified_date;
    private $status;
	private $bank_credit_date;
	private $bank_amount;
	private $remark;
 
    public function __construct($Collections_row = null)
    {
        if( !is_null($Collections_row) && $Collections_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $Collections_row->id;
                $this->site_id = $Collections_row->site_id;
                $this->timestamp = $Collections_row->timestamp;
                $this->amount = $Collections_row->amount;
                $this->deposit_by = $Collections_row->deposit_by;
                $this->deposit_location = $Collections_row->deposit_location;
                $this->attachment = $Collections_row->attachment;
				$this->user_id = $Collections_row->user_id;
				$this->curr_timestamp = $Collections_row->curr_timestamp;
				$this->verified_by = $Collections_row->verified_by;
                $this->verified_date = $Collections_row->verified_date;
                $this->status = $Collections_row->status;
				
				$this->bank_credit_date = $Collections_row->bank_credit_date;
                $this->bank_amount = $Collections_row->bank_amount;
                $this->remark = $Collections_row->remark;
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

