<?php

/**
 * Created by PhpStorm.
 * User: Amit Bhardwaj
 * Date: 13-02-2017
 * Time: 15:20
 */
class Application_Model_SiteSettings
{
    private $id;
    private $site_id;
    private $time_slot;
    private $brp;
    private $hsf;
//    private $x;
//    private $y;

    public function __construct($site_settings_row = NULL)
    {
        if (!is_null($site_settings_row) && $site_settings_row instanceof Zend_Db_Table_Row) {
            $this->id = $site_settings_row->id;
            $this->site_id = $site_settings_row->site_id;
            $this->time_slot = $site_settings_row->time_slot;
            $this->brp = $site_settings_row->brp;
            $this->hsf = $site_settings_row->hsf;
//            $this->x = $site_settings_row->x;
//            $this->y = $site_settings_row->y;

        }
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}

?>