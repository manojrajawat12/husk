<?php
class Application_Model_MpptsReadingMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_MpptsReading();
	}

	public function addNewMpptsReading(Application_Model_MpptsReading $meter_reading)
	{
		$data = array(
				'site_id' => $meter_reading->__get("site_id"),
				 'mppt_id' => $meter_reading->__get("mppt_id"),
				 'reading_date' => $meter_reading->__get("reading_date"),
				 'reading' => $meter_reading->__get("reading"),
				 'entry_by' => $meter_reading->__get("entry_by"),
				 'entry_type' => $meter_reading->__get("entry_type"), 
				 'actual_reading' => $meter_reading->__get("actual_reading"), 
			);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getMpptsById($site_id)
	{
		$query = " SELECT * From mppts where site_id ='$site_id' and status='enable' order by mppt_no asc";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		 
		if( count($result) == 0 ) {
			return false;
		}
		$SiteMaster_object_arr = array();
		foreach ($result as $row)
		{
			$SiteMaster_object = new Application_Model_Mppts($row);
			$SiteMaster_object->__set("id", $row["id"]);
			$SiteMaster_object->__set("mppt_name", $row["mppt_name"]);
			$SiteMaster_object->__set("mppt_keyword", $row["mppt_keyword"]);
			array_push($SiteMaster_object_arr,$SiteMaster_object);
		}
		return $SiteMaster_object_arr;
	}
	public function getAllMpptsReading()
	{
		$query = " SELECT * From mppts_reading inner join sites on mppts_reading.site_id=sites.site_id where sites.site_status!='Disable'";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
			
		if( count($result) == 0 ) {
			return false;
		}else{
				
		 $SiteMaster_object_arr = array();
		 foreach ($result as $row)
		 {
		 	$SiteMaster_object = new Application_Model_MpptsReading($row);
		 	$SiteMaster_object->__set("site_id", $row["site_id"]);
		 	$SiteMaster_object->__set("mppt_id", $row["mppt_id"]);
		 	$SiteMaster_object->__set("id", $row["id"]);
		 	$SiteMaster_object->__set("reading_date", $row["reading_date"]);
		 	$SiteMaster_object->__set("reading", $row["reading"]);
		 	array_push($SiteMaster_object_arr,$SiteMaster_object);
		 }
		 return $SiteMaster_object_arr;
		}
	}
	public function updateMpptsReading(Application_Model_MpptsReading $siteMeter)
	{
		$id=$siteMeter->__get("id");
		$site_id= $siteMeter->__get("site_id");
		$meter_id= $siteMeter->__get("mppt_id");
		$reading_date=$siteMeter->__get("reading_date");
		$reading=$siteMeter->__get("reading");


		$query ="update mppts_reading set site_id=".$site_id.",mppt_id='".$meter_id."',reading_date='".$reading_date."',reading='".$reading."' where id=".$id;

		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();

		if($result)
		{
			return true;
		}
	}
	public function updateMpptsReadingByMeterId($site_id,$meter_id,$date,$reading)
	{
		 

		$query ="update mppts_reading set reading='".$reading."' where  site_id=".$site_id." and mppt_id='".$meter_id."' and reading_date='".$date."'";

		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();

		if($result)
		{
			return true;
		}else{
			return false;
		}
	}
	public function deleteMpptReadingById($id)
	{
		$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function checkSiteReading($site_id,$meter_id,$reading_date=NULL,$id=NULL){
		 
		 
		if($id!=NULL){
			$check_id=" and id!=".$id;
		}else{
			$check_id=" ";
		}
		$query="SELECT * FROM mppts_reading where site_id=".$site_id." and mppt_id=".$meter_id." and reading_date='".$reading_date."'".$check_id;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();

		if( count($result) == 0 ) {
			return false;
		}
		else{ 
			return $result[0];
		}

	}



	public function getSiteMpptsReadingByMeterId($meter_id,$reading_date,$site_id)
	{
		$query = " SELECT * From mppts_reading where site_id=".$site_id." and mppt_id ='".$meter_id."'";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		 
		if( count($result) == 0 ) {
			return false;
		}else{
			return true;
		}
	}

	 public function getMpptsReadingByMeterId($meter_id)
	{
	 
		$query = " SELECT * From mppts_reading where mppt_id ='$meter_id' order by reading_date";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();

		if( count($result) == 0 ) {
			return false; 
		}else{

			$SiteMaster_object_arr = array();
			foreach ($result as $row)
			{
				$SiteMaster_object = new Application_Model_SiteMeterReading($row);
				$SiteMaster_object->__set("site_id", $row["site_id"]);
				$SiteMaster_object->__set("mppt_id", $row["mppt_id"]);
				$SiteMaster_object->__set("id", $row["id"]);
				$SiteMaster_object->__set("reading_date", $row["reading_date"]);
				$SiteMaster_object->__set("reading", $row["reading"]);
				array_push($SiteMaster_object_arr,$SiteMaster_object);
			}
			return $SiteMaster_object_arr;
		} 
		/*$mongoDriver= new MongoClient();
		$mongoDB=$mongoDriver->tara_devices;
		$mongoCollection = $mongoDB->mppt_reading;
		 
		$result=  $mongoCollection->find([
            "mppt_id"=> $meter_id
        ]);

        if($result->count() == 0) {
            return false;
        }
		 
        $data = [];
        foreach ($result as $key => $value){
            $data[] = $value;
        }
         
        date_default_timezone_set('Asia/Kolkata');
         
        
        $main_array=array();
        for ($i=0;$i<count($data);$i++){
        	$date_val =  date('Y-m-d', strtotime($data[$i]["timestamp"]));
        	$allData=array();
        	 for ($j=$i;$j<count($data);$j++){
        		if($date_val==date('Y-m-d', strtotime($data[$j]["timestamp"]))){
        			$dates=array(
        						 "site_id"=>$data[$j]["site_id"],
        						 "mppt_id"=>$data[$j]["mppt_id"],
        						 "mppt_reading"=>$data[$j]["enegry_from_pv_today"],
        						 "timestamp"=>$data[$j]["timestamp"]);
        			$allData[]=$dates;
        			$k=$j;
        		}
        	}
        	array_push($main_array,$allData);
        	$i=$k;
        }
        return $main_array;*/
	}
	public function getDateSiteReadingByMeterId($meter_id,$reading_date,$site_id)
	{
		$query = " SELECT * From mppts_reading where site_id=".$site_id." and mppt_id ='$meter_id' and reading_date='".$reading_date."'";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();

		if( count($result) == 0 ) {
			return true;
		}else{
			return false;
		}
	}

	public function getEnergyGenerationById($curr_date=NULL,$last_date=NULL,$site_id=NULL,$role_sites_id=NULL,$state_id=NULL,$meter_id=NULL)
	{
	 
	 if($site_id!=NULL){
	  	$where=" and sites.site_id in (".$site_id.")";
	 }elseif($state_id!=NULL){
	  	$where=" and sites.state_id in (".$state_id.")";
	 }elseif(count($role_sites_id)>0){
	  	$where=" and sites.site_id in (".implode(",", $role_sites_id).")";
	 } 
 
	 if($meter_id!=NULL){
	  $meter_ids=" and mppt_id =".$meter_id;
	 }else{
	  $meter_ids=" ";
	 }
    
     $query = "select sum(reading) as TotalReading from mppts_reading inner join mppts on mppts.id = mppts_reading.mppt_id inner join sites on sites.site_id=mppts_reading.site_id 
       where date(reading_date) >= '".$last_date."' AND  date(reading_date)<'".$curr_date."'".$where." and sites.site_status!='Disable' ".$meter_ids;
	 $stmt = $this->_db_table->getAdapter()->query($query);
	 $result = $stmt->fetchAll();
	  
	  
	 if( count($result) == 0 ) {
	  return false;
	 }else{
	  
	  return $result[0]["TotalReading"];
	 }
    }
public function getDateSiteReadingByMpptId($meter_id,$reading_date,$site_id)
	{
		$query = " SELECT * From mppts_reading where site_id=".$site_id." and mppt_id ='$meter_id' and reading_date='".$reading_date."'";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();

		if( count($result) == 0 ) {
			return true;
		}else{
			return false;
		}
	}
public function getSiteReadingByMpptsId($meter_id,$reading_date,$site_id)
	{
	    $query = " SELECT * From mppts where site_id=".$site_id." and id ='".$meter_id."'";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		 
		if( count($result) == 0 ) {
			return false;
		}else{
			return true;
		}
	}

	
	
	public function  getMpptReadingbydate($site_id,$month,$year)
    {
		$mongoDriver= new MongoClient();
        $mongoDB=$mongoDriver->tara_devices;
        $collmongo=$mongoDB->mppt_reading;
        
		$nmonth = date("m", strtotime($month));
        $nextmonth=($nmonth==12)?01:$nmonth+1;
        $nextmonth=sprintf("%02d",$nextmonth);
        
        $first= $year."-".$nmonth."-01 00:00:00";
        $second=$year."-".$nextmonth."-01 00:00:00";
        
        $ops = array(
        		array(
        				'$match' => array(
        						 "timestamp"=>[
					                '$gte'=>"$first",
					                '$lte'=>"$second",
					            ],
					            "site_id"=> "$site_id"
        				)
        		  ),
				 array( '$group'=> array('_id' => null, 
						"solar" => array( '$sum'=> '$enegry_from_pv_hr' ),
				 		"battery" => array( '$sum'=> '$enegry_battery_today' ),
				 		)) 
		      );

        $result=  $collmongo->aggregate($ops);
		$data = [];
        foreach ($result as $key => $value){
            $data[] = $value;
        }  
       
        if( count($data[0]) == 0 ) {
            return false;
        }
 
        return $data[0][0];
    }
    
    public function  getInverterReadingbydate($site_id,$month,$year)
    {
    	$mongoDriver= new MongoClient();
    	$mongoDB=$mongoDriver->tara_devices;
    	$collmongo=$mongoDB->inverter_reading;
    	$nmonth = date("m", strtotime($month));
    	$nextmonth=($nmonth==12)?01:$nmonth+1;
    	$nextmonth=sprintf("%02d",$nextmonth);
    
    	$first= $year."-".$nmonth."-01 00:00:00";
    	$second=$year."-".$nextmonth."-01 00:00:00";
    
    	$ops = array(
    			array(
    					'$match' => array(
    							"timestamp"=>[
    									'$gte'=>"$first",
    									'$lte'=>"$second",
    							],
    									"site_id"=> "$site_id"
    							)
    							),
						 array( '$group'=> array('_id' => null,
		    				 		"units_sold" => array( '$sum'=> '$load_pow_kwah' ),
						 			"diesel" => array( '$sum'=> '$generator_ac_pow' ),
		    				 ))
    							);
    
    	$result=  $collmongo->aggregate($ops);
    	$data = [];
    	foreach ($result as $key => $value){
    			$data[] = $value;
    	}
        if( count($data[0]) == 0 ) {
    		return false;
    	}
        return $data[0][0];
    }
	
	public function getMpptDataByMpptId($mppt_id=null,$site_id=null,$start=null,$end=null)
    {
    	$mongoDriver= new MongoClient();
    	$mongoDB=$mongoDriver->tara_devices;
    	$collmongo=$mongoDB->mppt_reading;
    
    	$info=array("timestamp"=>array('$lte'=>$end, '$gte'=>$start),
    									"site_id"=> $site_id,
    									"mppt_id"=> $mppt_id,
    	);
    	$result=  $collmongo->find($info)->sort(array("timestamp"=>1));
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$data = [];
    	foreach ($result as $key => $value){
    		$data[] = $value;
    	}
    	  
   		return $data;
    }
	
	public function getTotalMpptReadingByDate($site_id=NULL,$day=NULL,$month=NULL,$year=NULL)
    {
    	
    	  $query = "SELECT * from mppts_reading where site_id=".$site_id." and day(reading_date)=".$day." 
    			and month(reading_date)=".$month." and year(reading_date)=".$year;
    	 			 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result;
    	}
    }
    
     public function getTotalMpptReadingByMpptName($site_id=NULL,$day=NULL,$month=NULL,$year=NULL,$mppt_unit=NULL,$sign=NULL)
    {
    	 
    	$join="inner join mppts_reading mr on m.id=mr.mppt_id "; 
    	$dateFormat="where mr.site_id=".$site_id." and day(reading_date)=".$day." and month(reading_date)=".$month." and year(reading_date)=".$year." ";
    	$signs="";
    	if($sign!=NULL){
    		if($sign=='less'){
    			$signs=" and reading<=".$mppt_unit;
    		}else{
    			$signs=" and reading>=".$mppt_unit;
    		}
    	} 
    	  $query = "SELECT (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 1' ) as MPPT1,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 2' ) as MPPT2,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 3' ) as MPPT3,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 4' ) as MPPT4,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 5' ) as MPPT5,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 6' ) as MPPT6,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 7' ) as MPPT7,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 8' ) as MPPT8,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 9' ) as MPPT9,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='MPPT 10' ) as MPPT10,
					 (select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='BIOGAS' ) as BIOGAS,
    			(select sum(reading) as reading from mppts m ".$join.$dateFormat.$signs." and m.mppt_name='DG' ) as DG 
    		 from mppts_reading limit 1";
    		 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0];
    	}
    }
	
	public function getTotalMpptReadingconsumerID($state_id=NULL,$site_id=NULL,$role_sites_id=NULL,$months=NULL,$year=NULL)
	{
		if($site_id!=NULL){
			$where=" and s.site_id=".$site_id;
		}elseif($state_id!=NULL){
			$where=" and s.state_id=".$state_id;
		}elseif(count($role_sites_id)>0){
			$where=" and s.site_id in (".implode(",", $role_sites_id).")";
		}else{
			$where=" ";
		}
		
		$nmonth = date("m", strtotime($months));
	     	$query = " SELECT sum(m.reading) as reading From mppts_reading m inner join sites s on s.site_id=m.site_id where s.site_status!='Disable' and 
				month(reading_date)=".$nmonth." and year(reading_date)=".$year."".$where; 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
	
		if( count($result) == 0 ) {
			return false;
		}else{
			return $result[0]["reading"];
		}
	}
	
	 public function getTotalMpptReadingBySiteDate($site_id=NULL,$first=NULL,$second=NULL)
    {
     
    	$join="inner join mppts_reading mr on m.id=mr.mppt_id ";
    	if($second!=NULL){
    		$dateFormat=" where mr.site_id =".$site_id." and date(reading_date)>='".$first."' and date(reading_date)<='".$second."'";
    	}else{
    		$dateFormat="where mr.site_id=".$site_id." and date(reading_date)='".$first."'";
    	} 
    	
	   $query = "SELECT (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 1' ) as MPPT1,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 2' ) as MPPT2,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 3' ) as MPPT3,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 4' ) as MPPT4,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 5' ) as MPPT5,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 6' ) as MPPT6,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 7' ) as MPPT7,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 8' ) as MPPT8,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 9' ) as MPPT9,
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='MPPT 10' ) as MPPT10,
					 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='BIOGAS - PHASE 1' ) as BIOGAS1, 
    				 (select sum(reading) as reading from mppts m ".$join.$dateFormat." and m.mppt_name='DG' ) as DG
    		 from mppts_reading limit 1"; 
    	 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0];
    	}
    }
	
	public function getEnergyGenerationSolarById($curr_date=NULL,$last_date=NULL,$site_id=NULL,$role_sites_id=NULL,$state_id=NULL)
	{
	 
		if($site_id!=NULL){
			$where=" and sites.site_id in (".$site_id.")";
		}elseif($state_id!=NULL){
			$where=" and sites.state_id in (".$state_id.")";
		}elseif(count($role_sites_id)>0){
			$where=" and sites.site_id in (".implode(",", $role_sites_id).")";
		} 
 
		  $query = "select (select sum(reading) from mppts_reading 
							inner join mppts on mppts.id = mppts_reading.mppt_id inner join sites on sites.site_id=mppts_reading.site_id 
							where date(reading_date) >= '".$last_date."' AND  date(reading_date)<'".$curr_date."'".$where." and mppts.mppt_name like '%MPPT%' and sites.site_status!='Disable') as solar,
						(select sum(reading) from mppts_reading 
							inner join mppts on mppts.id = mppts_reading.mppt_id inner join sites on sites.site_id=mppts_reading.site_id 
							where date(reading_date) >= '".$last_date."' AND  date(reading_date)<'".$curr_date."'".$where."  and mppts.mppt_name like '%DG%' and sites.site_status!='Disable') as DG, 
						(select sum(reading) from mppts_reading 
							inner join mppts on mppts.id = mppts_reading.mppt_id inner join sites on sites.site_id=mppts_reading.site_id 
							where date(reading_date) >= '".$last_date."' AND  date(reading_date)<'".$curr_date."'".$where."  and sites.site_status!='Disable') as totalunit
					from mppts limit 1" ; 
				
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
	  
		if( count($result) == 0 ) {
			return false;
		}else{ 
			return $result[0];
		}
    }
	 
	public function getMpptReadingSiteWise($site_id,$mppt_id)
    {
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Kolkata");
    	$curr_date=$date->toString("yyyy-MM-dd");
    	
    	$mongoDriver= new MongoClient();
    	$mongoDB=$mongoDriver->tara_devices;
    	$mongoCollection = $mongoDB->mppt_reading;
    
    	$result=  $mongoCollection->find([
    			"timestamp"=>[
   						 '$gte'=>$curr_date,
					],
    					"site_id"=> $site_id, 
						"mppt_id"=> "$mppt_id"
    			])->sort([
    			"timestamp"=>-1
    	]);
		
    	if($result->count() == 0) {
    		return false;
		}
		$data = [];
    	foreach ($result as $key => $value){
    		$data[] = $value["enegry_from_pv_today"];  
    	} 
		return $data;
    }
	 
	public function getPlantDataByFilters($site_id=NULL,$state_id=NULL,$role_site_id=NULL,$first=NULL,$second=NULL,$feeder=NULL,$mppt=NULL)
    {
    	$mpptjoin=" inner join sites s on s.site_id=mr.site_id  inner join mppts mm on mm.id=mr.mppt_id";
    	$feederjoin=" inner join sites s on s.site_id=sm.site_id inner join site_meter m on m.id=sm.meter_id";
    	$downjoin=" inner join sites s on s.site_id=ss.site_id";
    	
    	if($site_id!=NULL){
    		$mpptFormat=" where mr.site_id in (".$site_id.") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
    		$feederFormat=" where sm.site_id in (".$site_id.") and date(sm.reading_date)<='".$first."' and date(sm.reading_date)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
    		$downformat=" where ss.site_id in (".$site_id.") and date(ss.timestamp)<='".$first."' and date(ss.timestamp)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
    		 
    	}elseif($state_id!=NULL){
    		$mpptFormat=" where s.state_id in (".$state_id.") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
    		$feederFormat=" where s.state_id in (".$state_id.") and date(sm.reading_date)<='".$first."' and date(sm.reading_date)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
    		$downformat=" where s.state_id in (".$state_id.") and date(ss.timestamp)<='".$first."' and date(ss.timestamp)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
    	}else{
    		$mpptFormat=" where mr.site_id in (".implode(",", $role_site_id).") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable' and s.site_id!=1 ";
    		$feederFormat=" where sm.site_id in (".implode(",", $role_site_id).") and date(sm.reading_date)<='".$first."' and date(sm.reading_date)>='".$second."' and s.site_status!='Disable' and s.site_id!=1";
    		$downformat=" where ss.site_id in (".implode(",", $role_site_id).") and date(ss.timestamp)<='".$first."' and date(ss.timestamp)>='".$second."' and s.site_status!='Disable' and s.site_id!=1";
    	}
		 
		if($feeder!=NULL){
			$feederFormat.=" and meter_id in (".$feeder.")";
		}
		if($mppt!=NULL){
			$mpptFormat.=" and mppt_id in (".$mppt.")";
		}
    	
    	
    	     $query = "select ( select sum(mr.reading) from mppts_reading mr ".$mpptjoin.$mpptFormat.") as mppt,
							( select sum(mr.reading) from mppts_reading mr ".$mpptjoin.$mpptFormat." and mm.mppt_name = 'DG') as DG,
							( select sum(mr.reading) from mppts_reading mr ".$mpptjoin.$mpptFormat."  and mm.mppt_name like '%BIOGAS%') as BIOGAS,
							(select sum(sm.reading) from site_meter_reading sm ".$feederjoin.$feederFormat.") as feeder,
							(select sum(sm.reading) from site_meter_reading sm ".$feederjoin.$feederFormat." and m.feeder_type='DD') as feeder_DD,
							(select sum(sm.reading) from site_meter_reading sm ".$feederjoin.$feederFormat." and m.feeder_type='NN') as feeder_NN,
							(select sum(sm.reading) from site_meter_reading sm ".$feederjoin.$feederFormat." and m.feeder_type='DN') as feeder_DN,
							(select count(*) from site_status ss ".$downjoin.$downformat." and ss.status='INACTIVE' ) as down 
    			       from mppts_reading limit 1";
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0];
    	}
    }
	
	 public function getFeederAndMppt($table_name=NULL,$column_name=NULL)
    { 
    	$query = " SELECT distinct(".$column_name.")  From ".$table_name." where site_id!=1 order by ".$column_name;
     
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $result;
    }
     
    public function getMpptTotalByFilter($site_id=NULL,$state_id=NULL,$role_site_id=NULL,$first=NULL,$second=NULL,$mppt_name=NULL)
    {
    	
    	$mpptjoin=" inner join mppts_reading mr on m.id=mr.mppt_id inner join sites s on s.site_id=m.site_id";
    	  
    	if($site_id!=NULL){
    		$mpptFormat=" where m.site_id in (".$site_id.") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable'";
    	}elseif($state_id!=NULL){
    		$mpptFormat=" where s.state_id in (".$state_id.") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable'";
    	}else{
    		$mpptFormat=" where m.site_id in (".implode(",", $role_site_id).") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable'";
    	}
    	
    	  $query = "select mppt_name,sum(reading) as reading from mppts m ".$mpptjoin." ".$mpptFormat." and m.mppt_name='".$mppt_name."'";
    	
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $result[0];
    }
	
	public function getMpptsReadingByDate($date=NULL,$site_id=NULL)
    {
    	
    	$site_ids="";
    	if($site_id!=NULL){
    		$site_ids=" smr.site_id in (".implode(",", $site_id).")"; 
    	}
		$dates="";
    	if($date!=NULL){
    		$dates=" and date(smr.reading_date)='".$date."'";
    	}
    	 
    	$query = " SELECT * From mppts sm inner join mppts_reading smr on sm.id=smr.mppt_id where ".$site_ids.$dates." and sm.status='enable' order by sm.mppt_no asc";
		   
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $result;
    }
    
	public function getMpptsReadingByDateID($mppt_id=NULL,$reading_date=NULL)
	{
	
		if($reading_date!=NULL){
			$dates=" and date(smr.reading_date)='".$reading_date."'";
		}else{
			$dates=" ";
		}
		 
	   $query = " SELECT * From mppts sm inner join mppts_reading smr on sm.id=smr.mppt_id  where smr.mppt_id=".$mppt_id." ".$dates." order by smr.reading_date desc";
	  
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		 
		if( count($result) == 0 ) {
			return false;
		}else{
			return $result[0];
		}
	}
	
	public function getDieselReport($site_id=NULL,$state_id=NULL,$role_site_id=NULL,$first=NULL,$second=NULL)
    {
    	$mpptjoin=" inner join sites s on s.site_id=mr.site_id  inner join mppts mm on mm.id=mr.mppt_id";
     	$dgjoin=" inner join sites s on s.site_id=d.site_id";
    	
		if($site_id!=NULL){
    		$mpptFormat=" where mr.site_id in (".$site_id.") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
			$dgFormat=" where d.site_id in (".$site_id.") and date(d.date_of_purchase)<='".$first."' and date(d.date_of_purchase)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
    	
		}elseif($state_id!=NULL){
    		$mpptFormat=" where s.state_id in (".$state_id.") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
			$dgFormat=" where s.state_id in (".$state_id.") and date(d.date_of_purchase)<='".$first."' and date(d.date_of_purchase)>='".$second."' and s.site_status!='Disable'  and s.site_id!=1";
    	 
		}else{
    		$mpptFormat=" where mr.site_id in (".implode(",", $role_site_id).") and date(mr.reading_date)<='".$first."' and date(mr.reading_date)>='".$second."' and s.site_status!='Disable' and s.site_id!=1 ";
			$dgFormat=" where d.site_id in (".implode(",", $role_site_id).") and date(d.timestamp)<='".$first."' and date(d.timestamp)>='".$second."' and s.site_status!='Disable' and s.site_id!=1 ";
		
		}
		 
		 
    	     $query = "select ( select sum(mr.reading) from mppts_reading mr ".$mpptjoin.$mpptFormat." and mm.mppt_name = 'DG') as DG,
							  ( select sum(d.amount) from dg d ".$dgjoin.$dgFormat." ) as DG_Cost,
							  ( select sum(d.unit) from dg d ".$dgjoin.$dgFormat." ) as DG_unit
						 	
    			       from mppts_reading limit 1";
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0];
    	}
    }
	
	 // Edit Mppt
    public function getmpptReadingByFeederIdDate($meter_id=NULL,$first=NULL,$second=NULL)
    {
    	$query = "select sum(reading) as reading from mppts_reading where
					date(reading_date)>='".$first."' and date(reading_date)<='".$second."' and mppt_id=".$meter_id;
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0]["reading"];
    	}
    }
    public function getFeedarReadingByDateVal($site_id=NULL,$first=NULL,$mppt_id=NULL)
    {
    	$join="inner join mppts_reading mr on m.id=mr.mppt_id ";
    	$dateFormat="where mr.site_id=".$site_id." and date(reading_date)='".$first."'";
    	 
    	$query = "SELECT sum(reading) as reading from mppts m ".$join.$dateFormat." and mppt_id =". $mppt_id."";
    	 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0]["reading"];
    	}
    }
    
    public function getMpptReadingBySiteIdReadingDate($site_id=NULL,$first=NULL)
    {
    	$join="inner join mppts_reading mr on m.id=mr.mppt_id ";
    	$dateFormat="where mr.site_id=".$site_id." and date(reading_date)='".$first."' and status='enable' order by mppt_name asc";  
    
    	$query = "SELECT * from mppts m ".$join.$dateFormat;
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result;
    	}
    }
    public function updateMpptByDateAndId($mppt_id=NULL,$unit=NULL,$date=NULL){
    	 
    	$where=" date(reading_date)='".$date."' and mppt_id=".$mppt_id;
    	$query ="update mppts_reading set reading='".$unit."' where ".$where;
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    	 
    	if($result)
    	{
    		return true;
    	}
    }
}

