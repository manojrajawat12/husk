<?php
class Application_Model_CollectionsMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Collections();
	}

	public function addNewCollections(Application_Model_Collections $Collections)
	{
			$zendDate = new Zend_Date();
			$zendDate->setTimezone("Asia/Kolkata");
			$timestamp_val = $zendDate->toString("yyyy-MM-dd HH:mm:ss");

		$data = array(
				'site_id' => $Collections->__get("site_id"),
				'timestamp' => $Collections->__get("timestamp"),
				'deposit_by' => $Collections->__get("deposit_by"),
				'amount' => $Collections->__get("amount"),
				'deposit_location' => $Collections->__get("deposit_location"),
				'attachment' => $Collections->__get("attachment"),
				'user_id' => $Collections->__get("user_id"),
				'curr_timestamp' => $timestamp_val,
				
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	public function getCollectionsById($Collections_id)
	{
		
		$result = $this->_db_table->find($Collections_id);
		
		if( count($result) == 0 ) {
			$Collections = new Application_Model_Collections();
			return $Collections;
		}
		$row = $result->current();
		$Collections = new Application_Model_Collections($row);
	
		return $Collections;
	}
	 
	public function getAllCollections($role_site_id=NULL,$curr_date=NULL,$last_date=NULL,$curr_system_date=NULL,$last_system_date=NULL)
	{
		$where = array();
		$range="";
		if($curr_date!=NULL && $last_date!=NULL){
			$range=" and date(timestamp)<='".$curr_date."'and date(timestamp)>='".$last_date."'"; 
		}
		
		if($curr_system_date!=NULL && $last_system_date!=NULL){
			$range=" and date(curr_timestamp)<='".$curr_system_date."'and date(curr_timestamp)>='".$last_system_date."' and status!='Inactive'"; 
		}
		if($role_site_id!=NULL)
		{
			  $where = "site_id in (".implode(",", $role_site_id).")". $range; 
		} 
	 
		$result = $this->_db_table->fetchAll($where,array('curr_timestamp desc'));   
		if( count($result) == 0 ) {
			return false;
		}
		$Collection_object_arr = array();
		foreach ($result as $row)
		{
			$Collection_object = new Application_Model_Collections($row);
			array_push($Collection_object_arr,$Collection_object);
		}
			return $Collection_object_arr;
	}
	
	public function updateCollections(Application_Model_Collections $Collections)
	{
		$data = array(
				'site_id' => $Collections->__get("site_id"),
				'timestamp' => $Collections->__get("timestamp"),
				'deposit_by' => $Collections->__get("deposit_by"),
				'amount' => $Collections->__get("amount"),
				'deposit_location' => $Collections->__get("deposit_location"),
				'attachment' => $Collections->__get("attachment"),
				'status' => $Collections->__get("status"),
				
		);
		 
		$where = "id = " . $Collections->__get("id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function deleteCollectionsById($id=null,$by=NULL,$timestamp=NULL,$remark=null)
	{
		/*$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
	
			return true;
		}*/
		$query ="update collections set status='Inactive',verified_by=".$by." ,verified_date='".$timestamp."' where id =".$id;
		  
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		
		if($result)
		{
			return true;
		}
	}
	  
	public function verifyCollectionsById($id=NULL,$by=NULL,$timestamp=NULL,$bank_credit_date=null,$bank_amount=null,$remark=null){
		
		$query ="update collections set status='Credited',verified_by=".$by." ,verified_date='".$timestamp."',
					bank_credit_date='".$bank_credit_date."',bank_amount=".$bank_amount.",remark='".$remark."'  where id =".$id;
		  
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		
		if($result)
		{
			return true;
		}
	}
	public function getAllDepositBySiteData($site_id=NULL,$curr_date=NULL,$last_date=NULL)
	{
		if($site_id!=NULL){
			$site =" Where c.site_id in (".implode(",", $site_id).")  and s.site_status!='Disable'" ;
		}else{
			$site =" Where c.site_status!='Disable'";
		}
		
		if($curr_date!=NULL && $last_date!=NULL){
			$site.=" and date(c.timestamp)<='".$curr_date."' and date(c.timestamp)>='".$last_date."' and c.status='Credited'";
		}
		 
		$query = "select sum(amount) as totalAmount from collections c inner join sites s on s.site_id=c.site_id ".$site;
		
		$stmt= $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if (count($result) == 0) {
			return false;
		}
		return $result[0]["totalAmount"];
	}
	public function getAllDepositBySiteId($site_id=NULL,$from=NULL,$to=NULL,$skip_count=NULL)
	{
		if($site_id!=NULL){
			$site =" Where c.site_id in (".implode(",", $site_id).")  and s.site_status!='Disable'" ;
		}else{
			$site =" Where c.site_status!='Disable'";
		}
		$timestamp="";
		if($from!=NULL && $to!=NULL){
			$timestamp=" and date(c.timestamp)>='".$from."' and date(c.timestamp)<='".$to."'";
		}
		$skip="";
		if($skip_count!=NULL){
			$skip=" limit ".$skip_count.",50";
		}else{
			$skip=" limit 0,50";
		}
		 $query = "select * from collections c inner join sites s on s.site_id=c.site_id ".$site.$timestamp.$skip;
		 
		$stmt= $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if (count($result) == 0) {
			return false;
		}
		return $result;
	}
}