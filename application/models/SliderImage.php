<?php

class Application_Model_SliderImage
{
    private $id;
    private $name;
	private $timestamp;
    private $site_id;
    

    public function __construct($SliderImage_row = null)
    {
        if( !is_null($SliderImage_row) && $SliderImage_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $SliderImage_row->id;
				$this->name = $SliderImage_row->name;
                $this->timestamp = $SliderImage_row->timestamp;
                $this->site_id = $SliderImage_row->site_id;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }
}

