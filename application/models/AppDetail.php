<?php

class Application_Model_AppDetail
{ 
    private $id;
    private $user_id;
    private $user_type;
    private $device_token;
    private $device_type;
	private $timestamp;
   
    public function __construct($AppDetail_row = null)
    {
        if( !is_null($AppDetail_row) && $AppDetail_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $AppDetail_row->id;
                $this->user_id = $AppDetail_row->user_id;
                $this->user_type = $AppDetail_row->user_type;
                $this->device_token = $AppDetail_row->device_token;
                $this->device_type = $AppDetail_row->device_type;
				$this->timestamp = $AppDetail_row->timestamp;
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

