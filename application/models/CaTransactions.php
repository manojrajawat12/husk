<?php

class Application_Model_CaTransactions 
{
        private $ca_transaction_id;
        private $collection_agent_id;
        private $transaction_type;
        private $transaction_amount;
        private $timestamp;
        private $remarks;
    

    public function __construct($catransactions_row = NULL)
    {
        if(!is_null($catransactions_row))
        {
                $this->ca_transaction_id = $catransactions_row->ca_transaction_id;
                $this->collection_agent_id = $catransactions_row->collection_agent_id;
                $this->transaction_type = $catransactions_row->transaction_type;
                $this->transaction_amount = $catransactions_row->transaction_amount;
                $this->timestamp = $catransactions_row->timestamp;
                $this->remarks = $catransactions_row->remarks;
        }
    }
    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name)
    {
    return $this->$name;
    }
}
