<?php

class Application_Model_SiteStatus
{
    private $site_id;
    private $site_status_id; 
    private $status;
    private $timestamp;
    private $feedar_id;
    private $start_date;
    private $entry_by;
    private $entry_type;
    private $entry_date;
    
    public function __construct($site_status_row = null)
    {
        if( !is_null($site_status_row) && $site_status_row instanceof Zend_Db_Table_Row ) {
                $this->site_id = $site_status_row->site_id;
                $this->site_status_id = $site_status_row->site_status_id;
                $this->status = $site_status_row->status;
                $this->timestamp = $site_status_row->timestamp;
                $this->feedar_id = $site_status_row->feedar_id;
                $this->start_date = $site_status_row->start_date;
                $this->entry_by = $site_status_row->entry_by;
                $this->entry_type = $site_status_row->entry_type;
                $this->entry_date = $site_status_row->entry_date;
        }
    }
    public function __set($name, $value)
    {
            $this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }

}

