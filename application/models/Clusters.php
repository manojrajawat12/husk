<?php

class Application_Model_Clusters
{
    private $cluster_id;
    private $cluster_name;
    private $cluster_manager_id;
    private $state_id;
    

    public function __construct($cluster_row = null)
    {
        if( !is_null($cluster_row) && $cluster_row instanceof Zend_Db_Table_Row ) {
            
                $this->cluster_id = $cluster_row->cluster_id;
                $this->cluster_name = $cluster_row->cluster_name;
                $this->cluster_manager_id = $cluster_row->cluster_manager_id;
                $this->state_id = $cluster_row->state_id;
                
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "ap_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

