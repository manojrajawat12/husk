<?php
class Application_Model_MeterReadingsMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_MeterReadings();
    }
	 
    public function addNewMeterReading(Application_Model_MeterReadings $meter_reading)
    {
        $data = array(
			'consumer_id' => $meter_reading->__get("consumer_id"),
			'meter_reading' => $meter_reading->__get("meter_reading"),
			'timestamp' => $meter_reading->__get("timestamp"),
        	'package_id' => $meter_reading->__get("package_id"),
			'start_reading' => $meter_reading->__get("start_reading"),
			
			'meter_no'	=> $meter_reading->__get("meter_no"),
        	'no_of_containers'	=> $meter_reading->__get("no_of_containers"),
			'no_of_hours'	=> $meter_reading->__get("no_of_hours"),
        	'operating_mode'	=> $meter_reading->__get("operating_mode"),
			'is_running_plan'	=> $meter_reading->__get("is_running_plan"),
        	'package_name'	=> $meter_reading->__get("package_name"),
        	'serial'	=> $meter_reading->__get("serial"),
			'unit'	=> $meter_reading->__get("unit"),
			'entry_by'	=> $meter_reading->__get("entry_by"),
			'entry_type'	=> $meter_reading->__get("entry_type"),
        	
		);  
		
		$result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getMeterReadingById($meter_reading_id)
    {
        $result = $this->_db_table->find($meter_reading_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $meter_reading = new Application_Model_MeterReadings($row);
        return $meter_reading;
    }
    public function getAllMeterReadings()
    {
        $result = $this->_db_table->fetchAll(null,array('meter_reading_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $meter_reading_object_arr = array();
        foreach ($result as $row)
        {
                $meter_reading_object = new Application_Model_MeterReadings($row);
                array_push($meter_reading_object_arr,$meter_reading_object);
        }
        return $meter_reading_object_arr;
    }
    public function getMeterReadingsByConsumer($consumer_id)
    {
        $where = array(
            "consumer_id = ?"=>$consumer_id
        );
		
		
        $result = $this->_db_table->fetchAll($where,array('timestamp DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $meter_reading_object_arr = array();
        foreach ($result as $row)
        {
                $meter_reading_object = new Application_Model_MeterReadings($row);
                array_push($meter_reading_object_arr,$meter_reading_object);
        }
        return $meter_reading_object_arr;
    }
    public function updateMeterReading(Application_Model_MeterReadings $meter_reading)
    {
        $data = array(
				'consumer_id' => $meter_reading->__get("consumer_id"),
				'meter_reading' => $meter_reading->__get("meter_reading"),
				'timestamp' => $meter_reading->__get("timestamp"),
        		'package_id' => $meter_reading->__get("package_id"),
				'operating_mode'=> $meter_reading->__get("operating_mode"), 
		);
        $where = "meter_reading_id = " . $meter_reading->__get("meter_reading_id");
        $result = $this->_db_table->update($data,$where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteMeterReadingById($meter_reading_id)
    {
        $where = "meter_reading_id = " . $meter_reading_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
	
    public function getLatestReadingByConsumerId($consumer_id=NULL,$package_id=NULL)
    {
        $where = array(
            "consumer_id = ?"=>$consumer_id,
        //	"package_id = ?"=>$package_id
        ); 
		 
        $result = $this->_db_table->fetchAll($where,array('timestamp DESC'));
         
        if( count($result) == 0 ) {
                return false;
        }
        $meter_reading_object = new Application_Model_MeterReadings($result[0]);
         
        return $meter_reading_object;
    }
	
    public function checkMeterReadingsByConsumerId($consumer_id){
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Kolkata");
    	$date = $date->toString("yyyy-MM-dd");
        
		$query="select * from meter_readings where month(timestamp)=month('".$date."') and year(timestamp)=year('".$date."')
				 and consumer_id=".$consumer_id." and start_reading!=1 ";
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return true;
    	}
    }
    public function getCurrentMeterReadingsByConsumerId($consumer_id){
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Kolkata");
    	$date = $date->toString("yyyy-MM-dd");
    	$trans_date=date_parse_from_format("Y-m-d", $date);
    	$month= $trans_date["month"];
    	$year= $trans_date["year"];
   		 $query="select ((select meter_reading from meter_readings where month(timestamp)=month('".$date."') and 
				year(timestamp)=year('".$date."') and consumer_id=m.consumer_id and start_reading!=1  order by timestamp desc limit 1 ) -
				(select meter_reading from meter_readings where date(timestamp)<'".$year."-".$month."-01'  and consumer_id=m.consumer_id
 				order by timestamp desc limit 1 )) as total_meter from meter_readings m where m.consumer_id=".$consumer_id." limit 1"; 
   		
   		$stmt= $this->_db_table->getAdapter()->query($query);
   		$result = $stmt->fetchAll();
   		if (count($result) == 0) {
   			return false;
   		}
   		return $result[0]["total_meter"];
    }
    
    public function getMTRReadingByConsumerId($consumer_id){
    	 
    	//$query="SELECT m.meter_reading,m.consumer_id,m.timestamp,c.cr_amount FROM meter_readings m inner join cash_register c on c.consumer_id=m.consumer_id
  		//		where m.consumer_id=".$consumer_id." and cr_entry_type='DEBIT' and DATE_FORMAT(m.timestamp,'%m-%d-%Y %h:%i')=DATE_FORMAT(c.timestamp,'%m-%d-%Y %h:%i') order by c.timestamp ";  
    	 $query="SELECT * FROM meter_readings  where  consumer_id=".$consumer_id."  order by timestamp desc";  
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result;
    	}
    }

      public function checkMonthMeterReadingsByConsumerId($consumer_id,$package_id=null){
    	$date =  new Zend_Date();
    	$date->setTimezone("Asia/Kolkata");
    	$date = $date->toString("yyyy-MM-dd");
    	$where="";
    	if($package_id!=null){
    		$where=" and package_id=".$package_id;
    	}
          $query="select * from meter_readings where month(timestamp)=month('".$date."') and year(timestamp)=year('".$date."')
				 and consumer_id=".$consumer_id.$where." order by timestamp desc limit 1" ;
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result[0];
    	}
    }
	
	 public function getMeterReadingsByFilters($consumer_id=NULL,$curr_date=null,$last_date=null,$listing=null,$filter=NULL,$package_id=NULL){
    	$list="";
		 
    	if($listing=='unit_consumerWise'){
			if($filter==1){
				$list=" limit 1";
			}else{
				$list=" limit 1,1";
			}
    	} 
				$packages="";
				if($package_id!=NULL){
					$packages= " and m.package_id=".$package_id;
				}
         $query="select  meter_reading,timestamp from meter_readings m inner join consumers c on c.consumer_id=m.consumer_id
				where c.consumer_id = ".$consumer_id.$packages." and date(m.timestamp)<='".$curr_date."' and date(m.timestamp)>='".$last_date."' 
				and m.start_reading!=1 order by m.timestamp desc".$list;
    	  
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result[0];
    	}
    }
	public function getMeterReadingsByFiltersSecond($consumer_id=NULL,$curr_date=null,$last_date=null,$listing=null,$filter=NULL){
    	$list="";
    		
    	if($listing=='unit_consumerWise'){ 
    		 
	      $query="select  meter_reading,timestamp from meter_readings m inner join consumers c on c.consumer_id=m.consumer_id
	    	  where c.consumer_id = ".$consumer_id." and   
			  ((date(timestamp)<'".$last_date."' and start_reading=1 )  or  
				(date(timestamp)>='".$last_date."' and date(timestamp)<='".$curr_date."'  and start_reading=1) or 
				(date(timestamp)<'".$last_date."' and start_reading!=1) ) order by m.timestamp desc limit 1";
	      
	    	$stmt= $this->_db_table->getAdapter()->query($query);
	    	$result = $stmt->fetchAll();
	    	if (count($result) == 0) {
	    		return false;
	    	}else
	    	{
	    		return $result[0];
	    	}
    	}else{
    		return false;
    	}
    
    }
	public function getCurrentandLstMeterReadingsByConsumerId($consumer_id){
    	$date = new Zend_Date(); 
    	$date->setTimezone("Asia/Kolkata");
    	$date = $date->toString("yyyy-MM-dd");
    	$trans_date=date_parse_from_format("Y-m-d", $date);
    	$month= $trans_date["month"];
    	$year= $trans_date["year"];
    	    $query="select (select meter_reading from meter_readings where month(timestamp)=month('".$date."') and
				year(timestamp)=year('".$date."') and consumer_id=m.consumer_id and start_reading!=1 order by timestamp desc limit 1 ) as curReading,
				(select meter_reading from meter_readings where  
				((date(timestamp)<='".$year."-".$month."-01' )  or  (date(timestamp)>='".$year."-".$month."-01' and start_reading=1))  
				 and consumer_id=m.consumer_id
 				order by timestamp desc limit 1 ) as lastReading from meter_readings m where m.consumer_id=".$consumer_id." limit 1";
    
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
	
	
	 public function getTotalUnitSiteWise($consumer_id=NULL,$month=NULL,$year=NULL){
    	
    	$join="inner join consumers c on c.consumer_id=m.consumer_id
    		   inner join sites s on s.site_id=c.site_id";
    	 $total_days_in_months=cal_days_in_month(CAL_GREGORIAN, $month, $year);
		 
		 $lastDay=$year."-".$month."-".$total_days_in_months;
		 
    	   $query="select (select meter_reading from meter_readings m ".$join." where month(timestamp)=".$month." and 
					year(timestamp)=".$year." and c.consumer_id=".$consumer_id." and start_reading!=1 order by timestamp desc limit 1 ) as curr_reading ,
				(select meter_reading from meter_readings  m ".$join." where  
				((date(timestamp)>='".$year."-".$month."-01' and timestamp<=mr.timestamp  and start_reading=1) or 
				(date(timestamp)<'".$year."-".$month."-01' and start_reading!=1) or (date(timestamp)<'".$year."-".$month."-01' and start_reading=1 ) )
					and c.consumer_id=".$consumer_id." order by timestamp desc limit 1 ) as last_reading,mr.package_id,mr.timestamp,mr.meter_no from meter_readings mr
						where month(mr.timestamp)=".$month." and  year(mr.timestamp)=".$year." and mr.consumer_id=".$consumer_id." order by mr.timestamp desc limit 1 ";  
		 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
	
	public function getLatestReadingByPackageConsumerId($consumer_id=NULL,$package_id=NULL)
    { 
    	$wheres = array(
    			"consumer_id = ?"=>$consumer_id,
    			"package_id = ?"=>$package_id
    	);
		 $pack="";
		if($package_id!=NULL){
		  $pack=" and package_id=".$package_id;
		} 
		$where = "consumer_id = " . $consumer_id.$pack;
    	$result = $this->_db_table->fetchAll($where,array('timestamp DESC'));
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$meter_reading_object = new Application_Model_MeterReadings($result[0]);
    	 
    	return $meter_reading_object;
    }
	
	 public function getMeterReadingsByConsumerId($consumer_id=NULL,$package_id=null,$timestamps=NULL){
    	 
		$trans_date=date_parse_from_format("Y-m-d", $timestamps);
							 	
		$month= $trans_date["month"];
		$year= $trans_date["year"];
		
		$timestamp_last=$year."-".$month."-01";
		
    	 $query="select meter_reading from meter_readings where consumer_id = ".$consumer_id." and  
    			date(timestamp)<='".$timestamps."' and date(timestamp)>='".$timestamp_last."' order by timestamp desc limit 1";
    	  
    		$stmt= $this->_db_table->getAdapter()->query($query);
    		$result = $stmt->fetchAll();
    		
    		if (count($result) == 0) {
    			$meter_reading1=0;
    		}else
    		{
    			$meter_reading1=$result[0]["meter_reading"];
    		}

			$query="select meter_reading from meter_readings where consumer_id = ".$consumer_id." and
    			date(timestamp)<='".$timestamps."'   order by timestamp desc limit 1,1";
    		 
    		$stmt= $this->_db_table->getAdapter()->query($query);
    		$result = $stmt->fetchAll();
    		
    		if (count($result) == 0) {
    			$meter_reading2=0;
    		}else
    		{
    			$meter_reading2=$result[0]["meter_reading"];
    		} 
    		
    	$totalReading=$meter_reading1-$meter_reading2;
    	$data=array(
    		"meter_reading"=>$meter_reading1,
    		"unit"=>$totalReading
    	);
    	return $data;
    }
	
	 public function getMTRByConsumerId($consumer_id,$limit=NULL){
    	 
		$limit_data="";
		if($limit!=NULL){ 
			$limit_data=" limit 50";
		}
    	$query="SELECT * FROM meter_readings where consumer_id=".$consumer_id." order by timestamp desc ".$limit_data;  
    	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result;
    	}
    } 
	
	public function getTotalReadingByLastDate($consumer_id=NULL,$last_date=NULL,$curr_date=NULL){
    		
    	$query="select * from meter_readings where date(timestamp)>='".$last_date."' and date(timestamp)<='".$curr_date."' and consumer_id=".$consumer_id;
		 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
    
    public function getTotalReadingByPreviousDate($consumer_id=NULL,$last_date=NULL,$package_id=NULL,$order=NULL){
    
		$where=""; 
    	if($package_id!=null){
    		$where=" and package_id=".$package_id;
    	}
		if($order!=null){
    		$where.=" order by timestamp desc";
    	}
    	$query="select * from meter_readings where date(timestamp)<'".$last_date."' and consumer_id=".$consumer_id.$where;
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
	
	 public function checkMonthMeterReadingsByDateConsumerId($consumer_id,$package_id=null,$date=null){
    	
    	$where=""; 
    	if($package_id!=null){
    		$where=" and package_id=".$package_id;
    	}
          $query="select * from meter_readings where month(timestamp)=month('".$date."') and year(timestamp)=year('".$date."')
				 and consumer_id=".$consumer_id.$where." order by timestamp desc limit 1" ;
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else
    	{
    		return $result[0];
    	}
    }
	
	public function getReadingByConPackID($consumer_id=NULL,$last_date=NULL,$package_id){
    
		$where=""; 
    	if($package_id!=null){
    		$where=" and package_id=".$package_id;
    	}
    	$query="select * from meter_readings where date(timestamp)<='".$last_date."' and consumer_id=".$consumer_id.$where." order by timestamp desc";
    	 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
	
	public function getReadingByCustomer($consumer_id=NULL){ 
     
    	$query="SELECT meter_reading_id, consumer_id, meter_reading, unit, max(timestamp) as timestamp, package_id, start_reading, 
					meter_no, serial, package_name, no_of_containers, no_of_hours, operating_mode, is_running_plan FROM meter_readings 
					where consumer_id=".$consumer_id." group by month(timestamp),year(timestamp),start_reading";
    	
		$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false; 
    	}
    	return $result; 
    }
	public function getAllMeterOnconsumerId($consumer_id=NULL){
    
    	$query="select * from meter_readings where consumer_id=".$consumer_id. " group by meter_no";
    
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
	
	public function getMeterReadingByMeterNo($meter_no=NULL,$consumer_id=NULL){
		$where="";
		if($consumer_id!=NULL){
			$where = " and consumer_id=".$consumer_id; 
		}
    	$query="select * from meter_readings where meter_no='".$meter_no."' ".$where." order by timestamp desc";
    
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
	
	public function getTotalUnitBySites($site_id=NULL,$state_id=NULL,$role_sites_id=NULL,$curr_date=NULL,$last_date=NULL){
    	
		$join="	inner join consumers c on c.consumer_id=m.consumer_id
				inner join consumer_packages cp on cp.consumer_id=c.consumer_id
				inner join packages p on p.package_id=cp.package_id 
				inner join sites s on s.site_id=c.site_id"; 
				
		if ($site_id!=NULL){
    		$where=" s.site_id in (".$site_id.")  ";
    	}elseif ($state_id!=NULL){
    		$where=" s.state_id in (".implode(",", $state_id).") and site_status!='Disable' and s.site_id!=1 "; 
    	}else{
    		$where=" s.site_id in (".implode(",", $role_sites_id).") and site_status!='Disable'  and s.site_id!=1 ";
    	} 	
		
    	  $query="select sum(m.unit) as unit from meter_readings m ".$join."
				where ".$where." and date(m.timestamp)>='".$last_date."' and date(m.timestamp)<'".$curr_date."' and p.is_postpaid!=0";
		 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0]["unit"];
    }
	
	public function getTotalUnitByConsumerId($consumer_id=NULL,$package_id=NULL,$month=NULL,$year=NULL){
    	 
    	 $query="select sum(unit) as unit from meter_readings where month(timestamp)='".$month."' and year(timestamp)='".$year."' and 
				consumer_id=".$consumer_id."  and package_id=".$package_id."";
		
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0]["unit"];
    }
	
	public function getTotalResetInMonth($consumer_id=NULL,$month=NULL,$year=NULL){
    	
    	$query="select * from meter_readings where month(timestamp)='".$month."' and year(timestamp)='".$year."' and 
				consumer_id=".$consumer_id." and start_reading=1 order by timestamp asc";
    
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result; 
    }
	
	public function getTotalUnitByConsumerIdDate($consumer_id=NULL,$start_date=NULL,$end_date=NULL){
    	 
    	$query="select sum(unit) as unit from meter_readings where timestamp<='".$start_date."' and timestamp>='".$end_date."' and 
				consumer_id=".$consumer_id;
		 
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0]["unit"];
    }
	
	public function getTotalUnitTillDate($consumer_id=NULL,$openingDay=NULL,$lastDay=NULL){
    
    	$join="inner join consumers c on c.consumer_id=m.consumer_id
    		   inner join sites s on s.site_id=c.site_id";
    	  
		$query="select (select meter_reading from meter_readings m ".$join." where date(timestamp)>'".$lastDay."'
							and c.consumer_id=".$consumer_id."  and start_reading!=1 order by timestamp asc limit 1 ) as curr_reading ,
					   (select meter_reading from meter_readings  m ".$join." where date(timestamp)<='".$lastDay."'  
							and c.consumer_id=".$consumer_id." and start_reading=1 order by timestamp desc limit 1 ) as last_reading 
						from meter_readings limit 1";   
		
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
	public function getTotalUnitByFeederId($feeder_id=NULL,$start_date=NULL,$end_date=NULL,$site_id=NULL){
    	 
    	$query="select sum(unit) as unit from meter_readings m inner join consumers c on c.consumer_id=m.consumer_id
					where date(timestamp)<='".$start_date."' and date(timestamp)>='".$end_date."' and site_meter_id=".$feeder_id." 
					and c.site_id=".$site_id." and c.consumer_status!='banned'";  
		
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll(); 
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0]["unit"];
    }
	
	public function getMeterReadingsByConsumerIdTimestamp($consumer_id=null,$package_id=NULL,$month=null,$year=NULL,$val=NULL){
    	
		$startReading="";
		if($val==NULL){  
			$startReading=" and start_reading!=1";
		}
		$query="select * from meter_readings where month(timestamp)=".$month." and year(timestamp)=".$year."
				 and consumer_id=".$consumer_id." and package_id=".$package_id.$startReading." order by timestamp desc limit 1";
    	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else {
    		return $result[0];
    	}
    }
	
	public function getTotalUnitByOnlyConsumerId($consumer_id=NULL,$month=NULL,$year=NULL){
		
		$query="select sum(unit) as unit from meter_readings where month(timestamp)='".$month."' and year(timestamp)='".$year."' and 
				consumer_id=".$consumer_id;
		
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0]["unit"];
    }
	public function getOnlyMeterReadingsByConsumerIdTimestamp($consumer_id=null,$month=null,$year=NULL){
    	
		$query="select * from meter_readings where month(timestamp)=".$month." and year(timestamp)=".$year."
				 and consumer_id=".$consumer_id." and start_reading!=1 order by timestamp desc limit 1";
    	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else {
    		return $result[0];
    	}
    }
	 
	public function getReadingDetailsByMeter($meter_no=NULL){
    	
		$query="select * from meter_readings where meter_no='".$meter_no."' order by timestamp desc limit 1";
    	
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else {
    		return $result[0];
    	}
    }
	
	public function checkMeterNo($meter_no=NULL,$consumer_id=NULL){
    	
		$query="select * from meter_readings where meter_no='".$meter_no."' and consumer_id=".$consumer_id." order by timestamp desc limit 1";
		
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}else {
    		return $result[0];
    	}
    }
	
	 public function getMeterDetailsForSparkConsumerId($consumer_id=NULL,$package_id=NULL,$month=NULL,$year=NULL){
    	 
		$total_days_in_months=cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$currMonth=$year."-".$month."-".$total_days_in_months;
		$lastMonth=$year."-".$month."-01";
		
    	$query="select m.operating_mode,m.is_running_plan,m.package_name,m.meter_no,m.timestamp,m.package_id,
					(select cm.meter_reading from meter_readings cm where date(cm.timestamp)<='".$currMonth."' and 
					cm.consumer_id=".$consumer_id." order by cm.timestamp desc limit 1 ) as curr_reading,
					(select lm.meter_reading from meter_readings lm where date(lm.timestamp)<'".$lastMonth."'  and 
					lm.consumer_id=".$consumer_id." order by lm.timestamp desc limit 1 ) as last_reading,
					(select sum(u.unit) as unit from meter_readings u where month(u.timestamp)='".$month."' and year(u.timestamp)='".$year."' and 
					u.consumer_id=".$consumer_id." and u.package_id=".$package_id." ) as units
					 from meter_readings m 
					where m.consumer_id=".$consumer_id."  order by m.timestamp desc limit 1"; 
					
    	$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0];
    }
	
	public function updateReading($meter_reading_id=NULL,$reading=NULL,$unit=NULL)
    {
		
    	$query ="update meter_readings set meter_reading=".$reading.",unit=".$unit."  where meter_reading_id =".$meter_reading_id;
    	
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    
    	if($result)
    	{
    		return true;
    	}
    }
	
	public function getReadingByCustomerData($consumer_id=NULL){ 
     
    	$query="SELECT * FROM meter_readings where consumer_id=".$consumer_id." order by timestamp desc";
    	
		$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result;
    }
	
	public function getConsumerMeterReading($consumer_id=NULL,$timestamp=NULL,$month=NULL,$year=NULL,$reset=NULL){ 
     
		if($reset==0){
			$order_by=" order by timestamp asc";
		}else{
			$order_by=" order by timestamp desc";
		}
		
		$querys="SELECT * FROM meter_readings where consumer_id=".$consumer_id." and 
				start_reading=".$reset."  and  date(timestamp)<='".$timestamp."' and month(timestamp)='".$month."' and year(timestamp)='".$year."' ".$order_by;
    	
		$stmt= $this->_db_table->getAdapter()->query($querys);
    	$results = $stmt->fetchAll();
    	if (count($results) == 0) {
			$current_date = date_parse_from_format("Y-m-d", $timestamp);
			$month = sprintf("%02d", $current_date["month"]);
			$year = $current_date["year"];
			$dateVal=$year."-".$month."-01"; 
			$query="SELECT meter_reading,timestamp,(SELECT sum(unit) FROM meter_readings 
						where consumer_id=".$consumer_id." and date(timestamp)>='".$dateVal."' and date(timestamp)<='".$timestamp."') as unit,meter_no,package_id FROM meter_readings 
						where consumer_id=".$consumer_id." and date(timestamp)>='".$dateVal."' and date(timestamp)<='".$timestamp."' order by timestamp desc";
    	}else{
			$query="SELECT meter_reading,timestamp,(SELECT sum(unit) FROM meter_readings where 
						consumer_id=".$consumer_id." and date(timestamp)>='".$results[0]["timestamp"]."' and date(timestamp)<='".$timestamp."') as unit,meter_no,package_id FROM meter_readings where 
						consumer_id=".$consumer_id." and date(timestamp)>='".$results[0]["timestamp"]."' and date(timestamp)<='".$timestamp."' order by timestamp desc";
    	}
		
    	 
    	
		$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false;
    	}
    	return $result[0]; 
    }
	
	public function getReadingByMeterNoCustomerData($consumer_id=NULL,$meter_no=NULL){ 
     
    	$query="SELECT * FROM meter_readings where consumer_id=".$consumer_id." and meter_no='".$meter_no."' group by month(timestamp),year(timestamp)";
    	
		$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false; 
    	}
    	return $result;
    }
	
	public function updateMeterNumberByLastNumber($consumer_id=NULL,$new_meter_no=NULL,$last_number=NULL)
    {
		
    	$query ="update meter_readings set meter_no='".$new_meter_no."' where consumer_id =".$consumer_id." and meter_no='".$last_number."'";
    	
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    
    	if($result) 
    	{
    		return true;
    	}
    }
	
	public function getUnitForSparkMeter($timestamp=NULL){ 
       
    	$query="select  
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 00:00:00' and timestamp<'".$timestamp." 01:00:00' ) as 0h_1h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 01:00:00' and timestamp<'".$timestamp." 02:00:00' ) as 1h_2h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 02:00:00' and timestamp<'".$timestamp." 03:00:00' ) as 2h_3h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 03:00:00' and timestamp<'".$timestamp." 04:00:00' ) as 3h_4h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 04:00:00' and timestamp<'".$timestamp." 05:00:00' ) as 4h_5h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 05:00:00' and timestamp<'".$timestamp." 06:00:00' ) as 5h_6h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 06:00:00' and timestamp<'".$timestamp." 07:00:00' ) as 6h_7h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 07:00:00' and timestamp<'".$timestamp." 08:00:00' ) as 7h_8h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 08:00:00' and timestamp<'".$timestamp." 09:00:00' ) as 8h_9h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 09:00:00' and timestamp<'".$timestamp." 10:00:00' ) as 9h_10h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 10:00:00' and timestamp<'".$timestamp." 11:00:00' ) as 10h_11h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 11:00:00' and timestamp<'".$timestamp." 12:00:00' ) as 11h_12h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 12:00:00' and timestamp<'".$timestamp." 13:00:00' ) as 12h_13h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 13:00:00' and timestamp<'".$timestamp." 14:00:00' ) as 13h_14h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 14:00:00' and timestamp<'".$timestamp." 15:00:00' ) as 14h_15h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 15:00:00' and timestamp<'".$timestamp." 16:00:00' ) as 15h_16h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 16:00:00' and timestamp<'".$timestamp." 17:00:00' ) as 16h_17h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 17:00:00' and timestamp<'".$timestamp." 18:00:00' ) as 17h_18h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 18:00:00' and timestamp<'".$timestamp." 19:00:00' ) as 18h_19h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 19:00:00' and timestamp<'".$timestamp." 20:00:00' ) as 19h_20h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 20:00:00' and timestamp<'".$timestamp." 21:00:00' ) as 20h_21h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 21:00:00' and timestamp<'".$timestamp." 22:00:00' ) as 21h_22h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 22:00:00' and timestamp<'".$timestamp." 23:00:00' ) as 22h_23h,
				( select sum(unit) from meter_readings where consumer_id in ( select consumer_id from consumers where site_id=62) and   timestamp>='".$timestamp." 23:00:00' and timestamp<'".$timestamp." 24:00:00' ) as 23h_24h
				
				from consumers limit 1";    
			
		$stmt= $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	if (count($result) == 0) {
    		return false; 
    	}
    	return $result[0];
    }
}
