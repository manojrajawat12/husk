<?php
class Application_Model_PackageHistoryMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_PackageHistory();
	}

	public function addNewPackageHistory(Application_Model_PackageHistory $package)
	{
			$date = new Zend_Date(); //"2018-05-01",'yyyy-MM-dd'
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		
		$data = array(
				
				'package_id' =>$package->__get("package_id"),
				'consumer_id' =>$package->__get("consumer_id"),
				'package_change_date' =>$timestamp,
				'change_by' =>$package->__get("change_by"),
				'package_cost' =>$package->__get("package_cost"),
				'change_description' =>$package->__get("change_description"),
				'timestamp' =>$timestamp,
				'status' =>$package->__get("status"),
				'last_package' =>$package->__get("last_package"),

		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	public function  getpackageHistoryByConsumerId($consumer_id)
	{
		$query = " SELECT * From package_history where consumer_id ='$consumer_id' order by package_change_date desc";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		$package_history_object_arr = array();
		foreach ($result as $row)
		{
			$package_history_object = new Application_Model_PackageHistory($row);
			$package_history_object->__set("package_history_id", $row["package_history_id"]);
			$package_history_object->__set("package_id", $row["package_id"]);
			$package_history_object->__set("consumer_id", $row["consumer_id"]);
			$package_history_object->__set("package_change_date", $row["package_change_date"]);
			$package_history_object->__set("change_by", $row["change_by"]);
			$package_history_object->__set("package_cost", $row["package_cost"]);
			$package_history_object->__set("change_description", $row["change_description"]);
			$package_history_object->__set("timestamp", $row["timestamp"]);
			$package_history_object->__set("status", $row["status"]);
			$package_history_object->__set("last_package", $row["last_package"]);
			array_push($package_history_object_arr,$package_history_object);
		}
		return $package_history_object_arr;
	}
	public function  getpackageHistoryByDate($start_date,$end_date,$consumer_id)
	{
		$query = " SELECT * From package_history where date(package_change_date) >='$start_date' and  date(package_change_date) <='$end_date' and consumer_id=$consumer_id  order by package_change_date desc";
		//$query = " SELECT * From package_history where date(package_change_date) >='$start_date' and  date(package_change_date) <='$end_date' and consumer_id=$consumer_id  order by package_change_date desc";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		/* if( count($result) == 0 ) {
			return false;
		} */
		$package_history_object_arr = array(); 
		foreach ($result as $row)
		{
			$package_history_object = new Application_Model_PackageHistory($row);
			$package_history_object->__set("package_history_id", $row["package_history_id"]);
			$package_history_object->__set("package_id", $row["package_id"]);
			$package_history_object->__set("consumer_id", $row["consumer_id"]);
			$package_history_object->__set("package_change_date", $row["package_change_date"]);
			$package_history_object->__set("change_by", $row["change_by"]);
			$package_history_object->__set("package_cost", $row["package_cost"]);
			$package_history_object->__set("change_description", $row["change_description"]);
			$package_history_object->__set("timestamp", $row["timestamp"]);
			$package_history_object->__set("status", $row["status"]);
			$package_history_object->__set("last_package", $row["last_package"]);
			array_push($package_history_object_arr,$package_history_object);
		}
		return $package_history_object_arr;
	}
	
	public function  getpackageHistoryPastEntry($date,$consumer_id)
	{
		$query = " SELECT * From package_history where date(package_change_date) <'$date' and consumer_id=$consumer_id  order by package_change_date desc limit 1";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			$query = " SELECT * From package_history where consumer_id=$consumer_id  order by package_change_date asc limit 1";
			$stmt = $this->_db_table->getAdapter()->query($query);
			$result = $stmt->fetchAll();
		}
		$package_history_object_arr = array();
		foreach ($result as $row)
		{
			$package_history_object = new Application_Model_PackageHistory($row);
			$package_history_object->__set("package_history_id", $row["package_history_id"]);
			$package_history_object->__set("package_id", $row["package_id"]);
			$package_history_object->__set("consumer_id", $row["consumer_id"]);
			$package_history_object->__set("package_change_date", $row["package_change_date"]);
			$package_history_object->__set("change_by", $row["change_by"]);
			$package_history_object->__set("package_cost", $row["package_cost"]);
			$package_history_object->__set("change_description", $row["change_description"]);
			$package_history_object->__set("timestamp", $row["timestamp"]);
			$package_history_object->__set("status", $row["status"]);
			$package_history_object->__set("last_package", $row["last_package"]);
			array_push($package_history_object_arr,$package_history_object);
		}
		return $package_history_object_arr;
	}
	
	public function getWeeklyPackageHistoryByConsumerId($consumer_id,$year,$month) {
		$initialDate=$year.'-'.$month.'-01 06:00';
		$dataArray=array();
		$stmt = $this->_db_table->getAdapter();
	    /*$query = "select c.consumer_id, (select package_cost from package_history where consumer_id=c.consumer_id and package_change_date <'$initialDate'
				 order by package_change_date desc limit 1) as initialCost
				 ,(select package_id from package_history where consumer_id=c.consumer_id and package_change_date <'$initialDate'
				order by package_change_date desc limit 1) as initialPackage
				,(select package_cost from package_history where package_change_date >='$initialDate' and 
				date(package_change_date) <='$year-$month-07' and consumer_id=c.consumer_id order by package_change_date desc limit 1) as weekCost2
				,(select package_id from package_history where package_change_date >='$initialDate' and date(package_change_date) <='$year-$month-07' 
				and consumer_id=c.consumer_id order by package_change_date desc limit 1) as weekPackage2
				,(select package_cost from package_history where date(package_change_date) >='$year-$month-08' and 
				date(package_change_date) <='$year-$month-14' and consumer_id=c.consumer_id order by package_change_date desc limit 1) as weekCost3
				,(select package_id from package_history where package_change_date >='$year-$month-08' and 
				date(package_change_date) <='$year-$month-14' and consumer_id=c.consumer_id order by package_change_date desc limit 1) as weekPackage3
				,(select package_cost from package_history where date(package_change_date) >='$year-$month-15' and 
				date(package_change_date) <='$year-$month-22' and consumer_id=c.consumer_id order by package_change_date desc limit 1) as weekCost4
				,(select package_id from package_history where package_change_date >='$year-$month-15' and 
				date(package_change_date) <='$year-$month-22' and consumer_id=c.consumer_id order by package_change_date desc limit 1) as weekPackage4
				 from consumers c where c.consumer_id=$consumer_id";*/
	
		  $query="call getWeeklyPackageHistoryByConsumerId($consumer_id,$year,$month,'$initialDate')";
		
		$result = $stmt->fetchAll($query);

		if (count($result) == 0) {
			return false;
		}
		$package_history_object_arr = array();
		foreach ($result as $row)
		{
			$package_history_object = new Application_Model_PackageHistory($row);
			$package_history_object->__set("consumer_id", $row["consumer_id"]);
			$package_history_object->__set("initialCost", $row["initialCost"]);
			$package_history_object->__set("initialPackage", $row["initialPackage"]);
			$package_history_object->__set("weekCost2", $row["weekCost2"]);
			$package_history_object->__set("weekPackage2", $row["weekPackage2"]);
			$package_history_object->__set("weekCost3", $row["weekCost3"]);
			$package_history_object->__set("weekPackage3", $row["weekPackage3"]);
			$package_history_object->__set("weekCost4", $row["weekCost4"]);
			$package_history_object->__set("weekPackage4", $row["weekPackage4"]);
			
			$package_history_object->__set("initialPackageType", $row["initialPackageType"]);
			$package_history_object->__set("weekPackage2Type", $row["weekPackage2Type"]);
			$package_history_object->__set("weekPackage3Type", $row["weekPackage3Type"]);
			$package_history_object->__set("weekPackage4Type", $row["weekPackage4Type"]);
			$package_history_object->__set("weekPackage5Type", $row["weekPackage5Type"]);
			
			$package_history_object->__set("initialReading", $row["initialReading"]); 
			$package_history_object->__set("week2Reading", $row["week2Reading"]);
			$package_history_object->__set("week3Reading", $row["week3Reading"]);
			$package_history_object->__set("week4Reading", $row["week4Reading"]);
			
			array_push($package_history_object_arr,$package_history_object);
		}
		return $package_history_object_arr;
	}
	
	
		public function  getpackageHistoryByConId($consumer_id)
	{
		//$query = " SELECT * From package_history where consumer_id ='$consumer_id' order by package_change_date desc";
		$query="select *,(select package_id from  package_history p1 where consumer_id='$consumer_id' 
 				and p1.package_history_id<p.package_history_id order by p1.package_change_date desc limit 1) as old_package
 				from  package_history p where consumer_id='$consumer_id'  order by p.package_change_date desc ";
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		$package_history_object_arr = array();
		foreach ($result as $row)
		{
			$package=new Application_Model_PackagesMapper();
			
			$packages=$package->getPackageById($row["package_id"]);
			if($packages){
					$packageName=$packages->__get("package_name");
			}else{
					$packageName="";
			}
			/* $old_packages=$package->getPackageById($row["old_package"]);
			print_r($old_packages);
			echo "p".$packages->__get("package_name");
			echo "o".$old_packages->__get("package_name"); */
			$package_history_object = new Application_Model_PackageHistory($row);
			$package_history_object->__set("package_history_id", $row["package_history_id"]);
			$package_history_object->__set("package_id", $row["package_id"]);
			$package_history_object->__set("consumer_id", $row["consumer_id"]);
			$package_history_object->__set("package_change_date", $row["package_change_date"]);
			$package_history_object->__set("change_by", $row["change_by"]);
			$package_history_object->__set("package_cost", $row["package_cost"]);
			$package_history_object->__set("change_description", $row["change_description"]);
			$package_history_object->__set("timestamp", $row["timestamp"]);
			$package_history_object->__set("status", $row["status"]);
			$package_history_object->__set("package_name", $packageName);
			$package_history_object->__set("old_package_name", $row["old_package"]);
			$package_history_object->__set("last_package", $row["last_package"]);
			array_push($package_history_object_arr,$package_history_object);
		}
		return $package_history_object_arr;
	}
	/*public function updatePackageHistory($consumer_id)
	{
		 $date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		$data = array(
				'status' => "Package Updated",
				'timestamp' => $timestamp,
		);
	
	
		$where = "consumer_id = " . $consumer_id." order by package_change_date desc limit 1";
		 
		$result = $this->_db_table->update($data,$where);
		 
		if($result==0)
		{
			return false;
		}
		else
		{
			return true; 
		} 
	}*/
	public function updatePackageHistory($consumer_id)
	{
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
		$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
		
		$query ="update package_history set status='Package Updated',timestamp='$timestamp' where consumer_id='$consumer_id' and status='Request Received'";
		
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		
		if($result)
		{
			return true;
		}
	}
	public function  getDailypackageHistoryReport($day=NULL,$month=NULL,$year=NULL,$site_id=NULL)
	{
		$query = " SELECT c.*,ph.package_id as ph_package_id,ph.last_package,ph.package_change_date,ph.change_by  From package_history ph inner join consumers c on c.consumer_id=ph.consumer_id inner join sites s on s.site_id=c.site_id
				  where c.site_id in (".implode(",", $site_id).") and s.site_status='enable' and day(ph.package_change_date)=".$day." and month(ph.package_change_date)=".$month." and  year(ph.package_change_date) =".$year." and ph.last_package is not null order by ph.package_change_date";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
		 return false;
		}
		
		return $result;
	}
	public function  getMonthlypackageHistoryReport($month=NULL,$year=NULL,$site_id=NULL)
	{
		$query = " SELECT c.*,ph.package_id as ph_package_id,ph.last_package,ph.package_change_date,ph.change_by  From package_history ph inner join consumers c on c.consumer_id=ph.consumer_id inner join sites s on s.site_id=c.site_id
				  where c.site_id in (".implode(",", $site_id).") and s.site_status='enable' and month(ph.package_change_date)=".$month." and  year(ph.package_change_date) =".$year." and ph.last_package is not null order by ph.package_change_date";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
	
		return $result;
	}
 
	public function  getMonthlypackageHistoryStatus($currDate=NULL, $lastDate=NULL,$site_id=NULL,$state_id=NULL,$role_site_id=NULL,$segment=NULL,$consumer_type=NULL)
	{
		if ($site_id!=NULL){
			$where=" s.site_id in (".$site_id.")";
		}else if($state_id!=NULL){
			$where =" s.state_id in (".$state_id.")";
		}else if ($role_site_id!=NULL){
			$where  =" s.site_id in (".implode(",", $role_site_id).")";
		}
		$segments="";$consumer_types="";
		if($segment!=NULL || $consumer_type!=NULL){
			$join=" inner join packages p on ph.package_id=p.package_id ";  
		}
		
		if($segment!=NULL){
			$segments=" and c.type_of_me in (".$segment.")";
		} 
		if($consumer_type!=NULL){
			$consumer_types=" and p.light_load in (".$consumer_type.")";
		}
		 
		   $query = " SELECT c.*,ph.package_id as ph_package_id, ph.last_package From package_history ph inner join 
						consumers c on c.consumer_id=ph.consumer_id inner join sites s on s.site_id=c.site_id ".$join."
					  where ".$where." and s.site_status='enable' and date(ph.package_change_date)<='".$currDate."' and  date(ph.package_change_date) >='".$lastDate."' 
					  and ph.last_package is not null ".$segments.$consumer_types." order by ph.package_change_date";
		 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
	
		return $result;
	}
	
	public function  getLastPackageHistoryDetailsByConumerId($consumer_id=null,$currDate=NULL,$lastDate=NULL)
	{
		  $query="select package_history.*, package_history.package_id as ph_package_id  from package_history inner join packages on package_history.package_id = packages.package_id
				where package_history.package_change_date<='".$currDate."' and package_history.package_change_date>='".$lastDate."' and
				package_history.consumer_id=".$consumer_id." and last_package is not null
    			order by package_history.package_change_date desc limit 1";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		 
		return $result[0];
	}
	
	public function getpackageHistoryByConsumerAndDate($date,$consumer_id)
	{
		$query = " SELECT * From package_history where date(package_change_date) <='".$date."' 
					and consumer_id=".$consumer_id."  order by package_change_date desc";
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
		 return false;
		} 
		 
		return $result[0];
	}
	
	public function getPackageDetailsByConsumers($consumer_id=NULL,$curr_date=null,$last_date=null,$package_id=NULL){
		
		$packages="";
		if($package_id!=NULL){
			$packages= " and ph.package_id=".$package_id;
		}
			
		$query="select ph.* from package_history ph inner join consumers c on ph.consumer_id=c.consumer_id
    	  where ph.consumer_id = ".$consumer_id.$packages." and date(ph.package_change_date)<='".$curr_date."' and date(ph.package_change_date)>='".$last_date."' and ph.last_package is not null order by ph.package_change_date desc";
	
		$stmt= $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if (count($result) == 0) {
			return false;
		}else
		{
			return $result;
		}
	}
	
	public function getpackageDetailByConsumerID($consumer_id=NULL,$year=NULL,$timestamp=NULL)
	{ 
		$date=$year."-04-01";
		   $query = " SELECT * From package_history where consumer_id=".$consumer_id." and date(package_change_date)>='".$date."' 
					and date(package_change_date)<='".$timestamp."' order by package_change_date desc";
		 
		$stmt = $this->_db_table->getAdapter()->query($query); 
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
			 
		return $result;
	}
	public function getpackageDetailByTypeOfMe($consumer_type_id=NULL,$month=NULL,$year=NULL,$site_id=NULL)
	{
		$query = "SELECT c.consumer_id,p.package_id as new_package, ph.last_package as last_package FROM tara.package_history ph inner join consumers c on c.consumer_id=ph.consumer_id 
							inner join packages p on ph.package_id = p.package_id
					where month(ph.package_change_date)=".$month." and year(ph.package_change_date)=".$year." and 
							c.type_of_me=".$consumer_type_id." and c.site_id=".$site_id." and ph.last_package is not null
									 order by ph.package_change_date desc";
	 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
			
		return $result;
	}
	
	public function getpackageHistoryByPackageID($package_id=NULL,$consumer_id=NULL)
	{
	 $query = " SELECT * From package_history where package_id=".$package_id." and consumer_id=".$consumer_id."  order by package_change_date desc";
	 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		
		return $result[0];
	}
	
	public function getpackageHistoryForLastEntry($date,$consumer_id) 
	{
		$query = " SELECT * From package_history ph inner join packages p on p.package_id=ph.package_id where date(ph.package_change_date) <='".$date."' 
					and ph.consumer_id=".$consumer_id." and ph.status!='remove' order by ph.package_change_date desc limit 1";
		
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
		 return false;
		} 
		 
		return $result[0];
	}
}
