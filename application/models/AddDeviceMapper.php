<?php
class Application_Model_AddDeviceMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_AddDevice();
    }

    public function addDevice(Application_Model_AddDevice $device)
    {
        $date = new Zend_Date();
        $date->setTimezone("Asia/Calcutta");
        $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
        $data = array(
	
		'device_name' => $device->__get("device_name"),
        'consumer_id' => $device->__get("consumer_id"),
        'node' => $device->__get("node"),
        'wattage' => $device->__get("wattage"),
            'timestamp'=>$timestamp
	
		);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getDeviceById($device_id)
    {
    	
        $result = $this->_db_table->find($device_id);
		
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $device = new Application_Model_AddDevice($row);
        return $device;
    }
    public function getAllAddDevice($condition=NULL)
    {
    	 	$where=NULL;
    	 	if($condition!=NULL)
    	 	{
    	 		$where = "device_name = " . $condition;
    	 	}
    	  
    	$result = $this->_db_table->fetchAll($where,array('device_id DESC'));
    	 
        if( count($result) == 0 ) {
                return false;
        }
        $device_object_arr = array();
        foreach ($result as $row)
        {
                $device_object = new Application_Model_AddDevice($row);
                array_push($device_object_arr,$device_object);
        }
        return $device_object_arr;
    }
    public function updateDevice(Application_Model_AddDevice $Device)
    {
        $date = new Zend_Date();
        $date->setTimezone("Asia/Calcutta");
        $timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
        $data = array(
			//'device_name' => $Device->__get("device_name"),
			'consumer_id' => $Device->__get("consumer_id"),
		    //'node' => $Device->__get("node"),
        	'wattage' => $Device->__get("wattage"),
            'timestamp'=>$timestamp
    	);
        
        $where = "device_name = " . $Device->__get("device_name")." and node=".$Device->__get("node");
        $result = $this->_db_table->update($data,$where);
    		 return true;
      
    }
    public function deleteDeviceById($Device_id)
    {
        $where = "device_name = " . $Device_id;
     
        $result = $this->_db_table->delete($where);
       
        if($result==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteConsumerDeviceById($Device_id)
    {
    	$where = "device_id = " . $Device_id;
    	 
    	$result = $this->_db_table->delete($where);
    	 
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
    public function getAllConsumerForDevice($all=NULL,$device_id=NULL,$site_id=NULL)
    {
    	if($all==NULL){
    		$all=" where consumers.consumer_id not in (SELECT consumer_id FROM node_map) ";
    	}else{
    		$all="where consumers.consumer_id not in (SELECT consumer_id FROM node_map where device_name!=".$device_id.") ";
    	}
    	
		$query="select * from consumers inner join sites on sites.site_id=consumers.site_id where sites.site_id=".$site_id;	
    	 
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if(count($result) == 0 ) {
    		return false;
    	}
    	$consumer_object_arr = array();
    	foreach ($result as $row)
    	{
    		$consumer_object = new Application_Model_Consumers();
    		foreach($row as $key=>$value)
    		{
    			$consumer_object->__set($key,$value);
    		}
    		array_push($consumer_object_arr,$consumer_object);
    	}
    
    	return $consumer_object_arr;
    }
    
    public function getDataByDeviceName($device_name=NULL)
    {
    	 
    	$query="SELECT s.* FROM devices s inner join node_map cd on s.device_id =cd.device_name
				where s.device_name='".$device_name."' order by node";
    
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if(count($result) == 0 ) {
    		return false;
    	}
    	$consumer_object_arr = array();
    	foreach ($result as $row)
    	{
    		$consumer_object = new Application_Model_Device();
    		foreach($row as $key=>$value)
    		{
    			$consumer_object->__set($key,$value);
    		}
    		array_push($consumer_object_arr,$consumer_object);
    	}
    
    	return $consumer_object_arr;
    }
    public function getDataByDeviceid($device_name=NULL)
    {
    
    	 $query="SELECT *,s.device_name as name FROM devices s inner join node_map cd on s.device_id =cd.device_name
				where s.device_id='".$device_name."' order by node";
		
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if(count($result) == 0 ) {
    		return false;
    	}
    	return $result;
    }

    public function getTimestampByDeviceid($device_name=NULL)
    {

        $query="SELECT * FROM ierms_data where device_id=".$device_name." order by info_date desc limit 1";

        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);

        if(count($result) == 0 ) {
            return false;
        }
        return $result[0];
    }
    public function getMsgByDeviceid($device_name=NULL)
    {

        $query="SELECT * FROM ierms_data where device_id=".$device_name." order by info_date desc limit 50";

        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);

        if(count($result) == 0 ) {
            return false;
        }
        return $result[0];
    }

    /*  public function getSocketDatabyDeviceid($device_name=NULL)
    {
        $query = "select * from logs where ( message like '%STOP%' or message like '%LCDID%') and message like '%$device_name%' order by timestamp desc limit 50";

        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        return $result;
    } */
    public function getSocketDatabyDeviceid($device_name=NULL)
    {
       $mongoDriver= new MongoClient();
       $mongoDB=$mongoDriver->tara_devices;
       $collmongo=$mongoDB->logs; 
 
         //echo $second." to ".$secondEnd." next "; 
       $info=array("device_name"=> $device_name);
       $result=  $collmongo->find($info)->sort(array("timestamp"=>-1))->limit(50);

        if( count($result) == 0 ) {
            return false;
        }
        return $result; 
    }
    public function getFullSocketDatabyDeviceid($device_name=NULL)
    {
        $query = "select * from logs where ( message like '%STOP%' or message like '%LCDID%') and message like '%$device_name%' order by timestamp desc";

        $stmt= $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
        if (count($result) == 0) {
            return false;
        }
        return $result;
    }
	
	public function getVolByConid($consumer_id=NULL)
    {

        $query="SELECT * FROM node_map where consumer_id=".$consumer_id;

        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);

        if(count($result) == 0 ) {
            return false;
        }
        return $result[0];
    }
	
	 public function getNodeByConid($consumer_id=NULL,$device_id=null)
    {

          $query="SELECT * FROM node_map where consumer_id='".$consumer_id."' and device_name=".$device_id." order by timestamp desc ";
 
        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);

        if(count($result) == 0 ) {
            return false;
        }
        return $result[0];
    }
	
	 public function getDeviceDetailByConid($consumer_id=NULL)
    {
    
      	$query="SELECT * FROM node_map n inner join devices d on d.device_id=n.device_name where n.consumer_id=".$consumer_id." order by timestamp desc ";
    
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if(count($result) == 0 ) {
    		return false;
    	}
    	return $result[0];
    }
	
	 public function getAllConsumerForIERMS($site_id=NULL,$site_arr=NULL)
    {
    	if($site_id!=NULL){
			$where =" where d.site_id =".$site_id;
		}else{
			$where =" where d.site_id in (".implode(",", $site_arr).") and s.site_status!='Disable'";
		}
		
		  $query="select * from consumers c 
						inner join sites s on s.site_id=c.site_id 
						inner join node_map n on n.consumer_id=c.consumer_id 
						inner join devices d on d.device_id=n.device_name".$where." order by d.device_name asc"; 
    	 
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if(count($result) == 0 ) {
    		return false;
    	}
		return $result;
    }
}
