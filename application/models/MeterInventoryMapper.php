<?php
class Application_Model_MeterInventoryMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_MeterInventory();
    }

    public function addNewMeterInventory(Application_Model_MeterInventory $meter_inventory)
    {
    	$data = array(
			'meter_no' => $meter_inventory->__get("meter_number"), 
        	'reading' =>$meter_inventory->__get("reading"),
			'status' => $meter_inventory->__get("status"),
			'brand' => $meter_inventory->__get("brand"),
			'meter_status'=>2
        ); 
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return $result;
        }
    }
  
    public function getAllMeterInventories($status=NULL)
    {
		if($status==NULL){
			 $where[] = "  meter_status=2";
		}else{
			 $where[] = "  meter_status=1";
		}
         
        $result = $this->_db_table->fetchAll($where,array('id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $query_object_arr = array();
        foreach ($result as $row)
        {
                $query_object = new Application_Model_MeterInventory($row);
                array_push($query_object_arr,$query_object);
        }
        return $query_object_arr;
    } 

    public function updateMeterInventory(Application_Model_MeterInventory $meter_inventory)
    {
        $data = array(
            'meter_no' => $meter_inventory->__get("meter_number"), 
            'reading' =>$meter_inventory->__get("reading"),
            'status' => $meter_inventory->__get("status"),
            'brand' => $meter_inventory->__get("brand")
        );
        $where = "id = " . $meter_inventory->__get("id");
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function deleteMeterInventoryById($id)
    {
        $where = "id = " . $id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
	
	public function getMeterNoByNo($meter_no=null)
	{
		$query="SELECT * FROM meter_inventry where meter_no='".$meter_no."' and meter_status=2"; 
	 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		return $result[0];
	}
	
	public function updateMeterInventoryById(Application_Model_MeterInventory $meter_inventory)
    { 	
        $data = array(
            'consumer_id' => $meter_inventory->__get("consumer_id"), 
            'reading' =>$meter_inventory->__get("reading"),
            'status' => $meter_inventory->__get("status"),
        );
        $where = "id = " . $meter_inventory->__get("id");
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
	public function updateInventory($meter_no=null,$status=NULL,$consumer_id=NULL)
	{
		$query ="update meter_inventry set meter_status='".$status."',consumer_id=".$consumer_id." where meter_no='".$meter_no."'";
		 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		
		if($result)
		{
			return true;
		}
	}
	
	public function getMeterNoByStatus($status=0,$meter_status=NULL,$site_id=NULL)
	{
		$query="SELECT * FROM meter_inventry m inner join consumers c on c.consumer_id=m.consumer_id
				where m.meter_status=".$status." and c.site_id=".$site_id;
		
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false; 
		}
		return $result;
	}
	 
	public function updateMeterStatus($meter_no=null,$status=NULL,$meter_status=NULL,$consumer_id=NULL,$timestamp=NULL)
	{
		$where="";
		if($timestamp!=NULL){
			$where=" ,add_timestamp='".$timestamp."'";
		}
		$query ="update meter_inventry set status='".$meter_status."',meter_status=".$status.", consumer_id=".$consumer_id.$where." where meter_no='".$meter_no."'";
		
		$stmt = $this->_db_table->getAdapter()->query($query); 
		$result=$stmt->execute();
		
		if($result) {
			return true;
		}
	} 
	
	public function getDetailByMeterNo($Meter_no=NULL) 
	{
		$query="SELECT * FROM meter_inventry where meter_no='".$Meter_no."' and meter_status!=3";
		
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false; 
		}
		return $result;
	}
}

