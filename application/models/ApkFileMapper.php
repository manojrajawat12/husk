<?php
class Application_Model_ApkFileMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_ApkFile();
	}

	public function addNewApkFile(Application_Model_ApkFile $ApkFile)
	{

		$data = array(
				 
				'apk_name' => $ApkFile->__get("apk_name"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getApkFileById($ApkFile)
	{
		$result = $this->_db_table->find($ApkFile);
		if( count($result) == 0 ) {
			$gateway = new Application_Model_ApkFile();
			return $gateway;
		}
		$row = $result->current();
		$gateway = new Application_Model_ApkFile($row);
		return gateway;
	}
	
	public function getAllApkFile()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('apk_id ASC'));
		
		if( count($result) == 0 ) {
			return $result;
		}else{
		$gateway_object_arr = array();
		foreach ($result as $row)
		{
			$sms_object = new Application_Model_ApkFile($row);
			array_push($gateway_object_arr,$sms_object);
		}
		return $gateway_object_arr;
	}
	}
	
	public function updateApkFile(Application_Model_ApkFile $ApkFile)
	{
	
		$apk_id= $ApkFile->__get("apk_id");
		$apk_name = $ApkFile->__get("apk_name");
		
		$query ="update apk_record set apk_name='".$apk_name."' where apk_id=".$apk_id;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		 
		if($result)
		{
			return true;
		}
	}
	
	
	public function deleteApkFileById($ApkFile)
	{
		$where = "apk_id = " . $ApkFile;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}