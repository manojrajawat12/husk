<?php

class Application_Model_MappingClusterSites
{
    private $mapping_cluster_site_id;
    private $site_id;
    private $cluster_manager_id;
    
    
    

    public function __construct($mapping_row = null)
    {
        if( !is_null($mapping_row) && $mapping_row instanceof Zend_Db_Table_Row ) {
            
                $this->mapping_cluster_site_id = $mapping_row->mapping_cluster_site_id;
                $this->site_id = $mapping_row->site_id;
                $this->cluster_manager_id = $mapping_row->cluster_manager_id;
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "ap_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

