<?php
class Application_Model_AppDetailMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_AppDetail();
	}

	public function addNewCollections(Application_Model_AppDetail $AppDetail)
	{
 
		$data = array(
				'user_id' => $AppDetail->__get("user_id"),
				'user_type' => $AppDetail->__get("user_type"),
				'device_token' => $AppDetail->__get("device_token"),
				'device_type' => $AppDetail->__get("device_type"),
				'timestamp' => $AppDetail->__get("timestamp"),
				
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	public function getCollectionsById($Collections_id)
	{
		
		$result = $this->_db_table->find($Collections_id);
		
		if( count($result) == 0 ) {
			$Collections = new Application_Model_Collections();
			return $Collections;
		}
		$row = $result->current();
		$Collections = new Application_Model_Collections($row);
	
		return $Collections;
	}
	
	public function getAllCollections()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('id'));
		if( count($result) == 0 ) {
			return false;
		}
		$Collection_object_arr = array();
		foreach ($result as $row)
		{
			$Collection_object = new Application_Model_Collections($row);
			array_push($Collection_object_arr,$Collection_object);
		}
		return $Collection_object_arr;
	}
	
	public function updateCollections(Application_Model_Collections $Collections)
	{
		$data = array(
				'site_id' => $Collections->__get("site_id"),
				'timestamp' => $Collections->__get("timestamp"),
				'deposit_by' => $Collections->__get("deposit_by"),
				'amount' => $Collections->__get("amount"),
				'deposit_location' => $Collections->__get("deposit_location"),
				'attachment' => $Collections->__get("attachment"),
				
		);
		 
		$where = "id = " . $Collections->__get("id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function deleteCollectionsById($id)
	{
		$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
	
			return true;
		}
	}

}