<?php
class Application_Model_DgMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Dg();
	}

	public function addNewDG(Application_Model_Dg $Dg)
	{

		$data = array(
				'site_id' => $Dg->__get("site_id"),
				'date_of_purchase' => $Dg->__get("date_of_purchase"),
				'rate' => $Dg->__get("rate"),
				'amount' => $Dg->__get("amount"),
				'unit' => $Dg->__get("unit"),
				'bill_attach' => $Dg->__get("bill_attach"),
				'user_id' => $Dg->__get("user_id"),
				'timestamp' => $Dg->__get("timestamp"),
				'invoice_no' => $Dg->__get("invoice_no"),
				
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	public function getDGById($dg_id)
	{
		
		$result = $this->_db_table->find($dg_id);
		
		if( count($result) == 0 ) {
			$state = new Application_Model_Dg();
			return $state;
		}
		$row = $result->current();
		$state = new Application_Model_Dg($row);
	
		return $state;
	}
	
	public function getAllDG($role_site_id=NULL,$curr_date=NULL,$last_date=NULL,$dg_curr_date=NULL,$dg_last_date=NULL)
	{
		$where = array();
		$range="";
		if($curr_date!=NULL && $last_date!=NULL){
			$range=" and date(date_of_purchase)<='".$curr_date."'and date(date_of_purchase)>='".$last_date."'"; 
		}
		if($dg_curr_date!=NULL && $dg_last_date!=NULL){
			$range=" and date(timestamp)<='".$dg_curr_date."'and date(timestamp)>='".$dg_last_date."'"; 
		}
		if($role_site_id!=NULL)
		{
			$where = "site_id in (".implode(",", $role_site_id).")". $range;
		} 
	
		$result = $this->_db_table->fetchAll($where,array('timestamp desc'));
		if( count($result) == 0 ) {
			return false;
		}
		$Dg_object_arr = array();
		foreach ($result as $row)
		{
			$Dg_object = new Application_Model_Dg($row);
			array_push($Dg_object_arr,$Dg_object);
		}
		return $Dg_object_arr;
	}
	
	public function updateDg(Application_Model_Dg $Dg)
	{
		$data = array(
				'site_id' => $Dg->__get("site_id"),
				'date_of_purchase' => $Dg->__get("date_of_purchase"),
				'rate' => $Dg->__get("rate"),
				'amount' => $Dg->__get("amount"),
				'unit' => $Dg->__get("unit"),
				'bill_attach' => $Dg->__get("bill_attach"),
				'status' => $Dg->__get("status"),
				'invoice_no' => $Dg->__get("invoice_no"),
				
		);
		$where = "dg_id = " . $Dg->__get("dg_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function deleteDgById($id)
	{
		$where = "dg_id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
	
			return true;
		}
	}
	public function verifyDGById($id=NULL,$by=NULL,$timestamp=NULL,$claimed_by=NULL){
	
		  $query ="update dg set status='Claimed',verified_by=".$by." ,verified_date='".$timestamp."',claimed_by='".$claimed_by."' where dg_id =".$id;
	 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
	
		if($result)
		{
			return true;
		}
	}
	
	public function getAllDgBySiteId($site_id=NULL,$from=NULL,$to=NULL,$skip_count=NULL)
	{
		if($site_id!=NULL){
			$site =" Where d.site_id in (".implode(",", $site_id).")  and s.site_status!='Disable'" ;
		}else{
			$site =" Where d.site_status!='Disable'";
		}
		$timestamp="";
		if($from!=NULL && $to!=NULL){
			$timestamp=" and date(d.date_of_purchase)>='".$from."' and date(d.date_of_purchase)<='".$to."'";
		}
		$skip="";
		if($skip_count!=NULL){
			$skip=" limit ".$skip_count.",50";
		}else{
			$skip=" limit 0,50";
		}
	    $query = "select * from dg d inner join sites s on s.site_id=d.site_id ".$site.$timestamp.$skip;
	 
		$stmt= $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if (count($result) == 0) {
			return false;
		}
		return $result;
	}
}