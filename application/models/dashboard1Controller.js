var huskApp = angular.module('huskApp');

huskApp.controller("dashboard1Controller", function($scope, $http, $modal, $log,$rootScope) {
	
	$scope.init=function(){
	   	$http.get(baseUrl + "default/auth/auth-data")
	   	  .success(function(response) {
	   		  $scope.userRole = response.data.userRole;
	   		$rootScope.states_array = response.data.state_id;
	   		$scope.sites = response.data.sites;
	   		$scope.subMenus=response.data.subMenu_id;
	        $scope.sites.unshift($scope.all_site);
	        
	    	$rootScope.allSites=response.data.sites;
	 
	        $scope.states = $rootScope.states_array;
	    	$scope.selected_state = $rootScope.states_array[0];
	    	//$scope.selected_states = response.data[0];
	    	$scope.states.unshift($scope.all_states);
	    	
	    	 $scope.filter=false;
			 var date_val = new Date();
			 
			 var day = (date_val.getDate() -1);  
			 var month = date_val.getMonth();
			 var year = date_val.getYear();
			  
			function daysInMonth(month, year){
				return month == 2 ? (year %4 ? 28 : (year %100 ? 29 : (year %400 ? 28 :29))) : ((month -1) %7% 2 ? 30 :31);
			} 
			var nr_days = daysInMonth(month, year);
			var number=364-(nr_days-day);
			var next_number=(nr_days-day);
			
			$scope.checkFilter(); 
			 
			 
	    });  
    }

	$scope.init();
	 
	$scope.checkFilter=function(){
		 
		  
		  function daysInMonths(month, year){
				return month == 2 ? (year %4 ? 28 : (year %100 ? 29 : (year %400 ? 28 :29))) : ((month -1) %7% 2 ? 30 :31);
			}
	     var month_days = daysInMonths($rootScope.MonthNo+1, $scope.selected_year);
	      
    	 if($rootScope.YearTypes=='on'){
			  
			  var dl = new Date($scope.selected_year+"/"+$scope.selected_month+"/01");
				  dl.setMonth(dl.getMonth() - 14);
    		  
				var options = { 
    			      year: "numeric", month: "long",  day: "numeric" 
    			};
    		  
			$rootScope.last_date_graph=dl.toLocaleDateString("en-us",options);
			 
			$rootScope.current_date_graph=$scope.selected_month+" "+month_days+", "+$scope.selected_year;
	     }else{
			$rootScope.last_date_graph="April 1,2017";
			$rootScope.current_date_graph="March 31, 2018";
		 }
	     if($scope.ds_site_id==undefined || $scope.ds_site_id=="" || $scope.ds_site_id==null){
	    	 $rootScope.site_ds_id="ALL";
	     }else{ 
	    	 $rootScope.site_ds_id=$scope.ds_site_id; 
	     }
	     if($scope.ds_state_id==undefined || $scope.ds_state_id=="" || $scope.ds_state_id==null){
	    	 $rootScope.state_ds_id="ALL";
	     }else{  
	    	 $rootScope.state_ds_id=$scope.ds_state_id; 
	     }
	     $scope.monthly_billed();
    }
  	
 
 
	$scope.baseUrl = baseUrl;
	$scope.energyGraph=0;
	$scope.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	$scope.entryStatus = ["ALL Type", "CHG", "ACT", "EED", "MED", "OTHERS"];
	$scope.selected_entry = $scope.entryStatus[0];
	$scope.select_enrty = function(entry)
    {
        $scope.selected_entry = entry;
    };
    $scope.selected_entrys = $scope.entryStatus[0];
	$scope.select_enrtys = function(entry)
    {
        $scope.selected_entrys = entry;
    };
    $scope.years = [];
    $scope.stack_year=[];
    $scope.yearTypes=["Regular Year","Fiscal year"];
    $scope.selected_year_Type=$scope.yearTypes[0];
    $rootScope.YearTypes='on';
    $('#yearType').bootstrapToggle({
        on: 'Regular Year',
        off: 'Fiscal year'
      });
   
    $scope.selectYearType=function(){
    	if(document.getElementById("yearType").value=="off"){
    		   document.getElementById("yearType").value="on";
    	}
        else if(document.getElementById("yearType").value=="on"){
    		   document.getElementById("yearType").value="off";
        }
    	$rootScope.YearTypes=document.getElementById("yearType").value;
    	
    }
    var d = new Date();
   
    if (d.getMonth() == 0) {
    	 $scope.last_selected_month = $scope.months[11];
    	 $scope.last_selected_year = (d.getFullYear() - 1);
		 $scope.next_selected_month = $scope.months[1];
    } else {
    	 $scope.last_selected_month = $scope.months[(d.getMonth())-1];
    	 $scope.last_selected_year = d.getFullYear();
		  $scope.next_selected_month = $scope.months[(d.getMonth())+1];
    }
 
    for(var i=2014;i<= d.getFullYear();i++)
	{
    	$scope.years.push(i);
    	$scope.stack_year.push(i);
	}

    $scope.selected_month = $scope.months[d.getMonth()];
	$rootScope.MonthNo=d.getMonth();
    $scope.selected_year = d.getFullYear();
 	 
    $scope.selected_stack_year =$scope.selected_year;// d.getFullYear();
    $scope.selected_vill_year = $scope.selected_year;//d.getFullYear();

    $scope.selectMonth = function(month)
    {
        $scope.selected_month = month;
        var checkMonth=($scope.months).indexOf(month);
		$rootScope.MonthNo=checkMonth;
        $scope.last_selected_month=(checkMonth==0)?'December':$scope.months[checkMonth-1];
		$scope.next_selected_month=(checkMonth==0)?'February':$scope.months[checkMonth+1];
        $scope.last_selected_year=($scope.selected_month=='January')?($scope.selected_year)-1:$scope.selected_year;
    };
    $scope.selectYear = function(year)
    {
        $scope.selected_year = year;
        var checkMonth=($scope.months).indexOf($scope.selected_month);
        $scope.last_selected_month=(checkMonth==0)?'December':$scope.months[checkMonth-1];
        $scope.last_selected_year=($scope.selected_month=='January')?year-1:year;
		$scope.next_selected_month=(checkMonth==0)?'February':$scope.months[checkMonth+1];
        
    };
    $scope.select_stack_Year = function(year)
    {
        $scope.selected_stack_year = year;
    };
    $scope.select_Vill_Year = function(year)
    {
        $scope.selected_vill_year = year;
    };
    $scope.select_state=function(state)
    {
    	$scope.selected_state = state;
    };
    $scope.select_states=function(state) 
    {
    	$scope.selected_states = state;
    };
    $scope.all_states = {state_id:"ALL",state_name:"All States"};
    $scope.selected_states = $scope.all_states;
    
    $scope.all_site = {site_id:"ALL",site_name:"All Sites"};
    $scope.selected_site = $scope.all_site;
    $scope.selected_sites = $scope.all_site;
    
    $scope.selectSite = function(site)
    {
        $scope.selected_site = site;
    };
    $scope.selectSites = function(site)
    {
        $scope.selected_sites = site;
    };
    

    
     $scope.updateSites=function(){
    	var state_array=$rootScope.allSites; 
    	if($scope.ds_state_id=="ALL")
		{
    		$scope.sites=$rootScope.allSites;
    	}else{ 
	    	var stateData= $.grep(state_array, function(n) {
				return ($scope.ds_state_id).indexOf(n.state_id) != -1;
		    });
	    	$scope.sites = stateData;
    	} 
    } 
 
	$scope.monthly_billed=function(){
    	
    	 
    	    if($scope.selected_site.site_id=='ALL'){
    	        $http.get(baseUrl + "api/graphs/energy-consumed/yearType/on/curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
    	        					"/site_id/"+$rootScope.site_ds_id + "/state_id/"+$rootScope.state_ds_id).success(
	    	        		function(response) {
	    	                	var controls=document.getElementById('ebarChartforMeters');
	    	                	if(controls!=null){
									$scope.chartForConGen(controls,response.data[0],response.data[1],'#4cabce','Energy Consumption');
	    	                		//echarts.ebarChartforMeters(controls,response.data[0],response.data[1],'Energy Consumption','#4cabce');
	    	                	}
	    	                	if (response.meta.code != 200) {
	    	                        Command: toastr["error"](response.meta.message);
	    	                    }
	    	                });
    	        }
    	    
    	    if($scope.selected_site.site_id!='ALL'){
    	        $http.get(baseUrl + "api/graphs/energy-consumed-site-id/yearType/on/curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
    	        					"/site_id/"+$scope.selected_site.site_id + "/state_id/ALL").success(
    	                    function(response) {
    	                    	$scope.energyGraph=0;
    	                    	var controls=document.getElementById('ebarChartforMeters');
    	    	            	if(controls!=null){
    	    	                	$scope.chartForConGenSiteWise(controls,response.data[0],response.data[1]);
									//echarts.ebarChartforMetersSiteWise(controls,response.data[0],response.data[1]);
    	    	            	}               	 
    	    	            	if (response.meta.code != 200) {
    	                            Command: toastr["error"](response.meta.message);
    	                        }
    	                    });
    	       }
    	    
    	    /* Energy Generation Graph */
    	    
    	    if($scope.selected_sites.site_id=='ALL'){
    	        $http.get(baseUrl + "api/graphs/energy-generation/yearType/on/curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
    	        					"/site_id/"+$rootScope.site_ds_id + "/state_id/"+$rootScope.state_ds_id).success(
	    	        		function(response) {
	    	                	var controls=document.getElementById('ebarChartforMppt');
	    	                	if(controls!=null){
									$scope.chartForConGen(controls,response.data[0],response.data[1],'#ff7f50','Energy Generation');
	    	                		//echarts.ebarChartforMeters(controls,response.data[0],response.data[1],'Energy Generation','#ff7f50');
	    	                	}
	    	                	if (response.meta.code != 200) {
	    	                        Command: toastr["error"](response.meta.message);
	    	                    }
	    	                });
    	        }
    	    
    	    if($scope.selected_sites.site_id!='ALL'){
    	        $http.get(baseUrl + "api/graphs/energy-generation-site-id/yearType/on/curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
    	        					"/site_id/"+$scope.selected_sites.site_id + "/state_id/ALL").success(
    	                    function(response) {
    	                    	$scope.energyGraph=0;
    	                    	var controls=document.getElementById('ebarChartforMppt');
    	    	            	if(controls!=null){
									$scope.chartForConGenSiteWise(controls,response.data[0],response.data[1]);
    	    	                	//echarts.ebarChartforMetersSiteWise(controls,response.data[0],response.data[1]);
    	    	            	}               	 
    	    	            	if (response.meta.code != 200) {
    	                            Command: toastr["error"](response.meta.message);
    	                        }
    	                    });
    	       } 
    	     
				setTimeout(function () {	  
				 
				$http.get(baseUrl + "api/graphs/destroy").success( function(response) {
					$data = response.data;
					
				});
				
				},300);	
    }
	
	$scope.chartForConGen=function(control,xaxis,yaxis,colors,name){
		var myChart = echarts.init(control);
		var app = {};
		option = null;
		app.title = name;
	
		option = {
		    color: [colors],
		    tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
		    grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    calculable: true,
		    xAxis : [
		        {
		            type : 'category',
		            data : yaxis,
		            axisTick: {
		                alignWithLabel: true
		            },
		            splitArea : {
                        show: true,
                        areaStyle:{
                            color:['white']
                        }
                    },
		        }
		    ],
		    yAxis: [{
            	type: 'value',
                splitArea : {
                    show: true,
                    areaStyle:{
                        color:['black']
                    }
                },
            }],
		    series : [
		        {
		            name:name,
		            type:'bar',
		            barWidth: '80%',
		            data:xaxis,
		             label: {
		                normal: {
		                    show: true,
		                    position: 'top',
		                }
		            },
		        }
		    ]
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
	$scope.chartForConGenSiteWise=function(control,reading,months){
		var myChart = echarts.init(control);
		option = null;
		var meter_id=[];
   	 	var readingdatas = [];
   	 	var month_detail=[];
   	 	var totalSession=[];
        
   	 	for (var i = 0; i < reading.length; i++){
   	 		meter_id.push(reading[i]["meter_id"]); 
   	 		readingdatas.push(reading[i]["reading"]);
       	}
        
        for(var j=0;j<meter_id.length;j++){
	       	var session = {
	       			 name:meter_id[j], 
	       			 type:"bar", 
	       			 data:readingdatas[j],
	       			 label: {
		                normal: {
		                    show: true,
		                    position: 'top',
		                }
		             },
	       	};
	       	 
	       	totalSession.push(session);
        }
		   
        if(meter_id.length<=0){
	       	 meter_id.push("NULL");
	       	 totalSession=[{
	       		 name:"NULL", 
	           	 type:"bar", 
	           	 data:[0,0,0,0,0,0,0,0,0,0,0,0],
	             label: {
		                normal: {
		                    show: true,
		                    position: 'top',
		                }
		            },
	         }];
	     }
        
		option = {
		    tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
		    legend: {
                data: meter_id,
            },
            grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    calculable: true,
		    xAxis: [{
                type: 'category',
                data: months,
                axisTick: {
	                alignWithLabel: true
	            },
	            splitArea : {
                    show: true,
                    areaStyle:{
                        color:['white']
                    }
                },
                
            }],
            yAxis: [{
            	type: 'value',
            	splitArea : {
                    show: true,
                    areaStyle:{
                        color:['black']
                    }
                },
            }],
		    series: totalSession,
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
});
