<?php
class Application_Model_CelamedMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Celamed();
		$this->consumer_type_db_table = new Application_Model_DbTable_ConsumerTypes();
	}

	public function addNewCelamed(Application_Model_Celamed $Celamed)
	{

		$data = array(
				'consumer_id' => $Celamed->__get("consumer_id"),
				'site_id' => $Celamed->__get("site_id"),
				'intervention_type' => $Celamed->__get("intervention_type"),
				'equipment_cost_total' => $Celamed->__get("equipment_cost_total"),
				'equipment_cost_tara' => $Celamed->__get("equipment_cost_tara"),
				'equipment_type' => $Celamed->__get("equipment_type"),
				'equipment_summary' => $Celamed->__get("equipment_summary"),
				'Contact_date' => $Celamed->__get("Contact_date"),
				'payment_date' => $Celamed->__get("payment_date"),
				'completion_date' => $Celamed->__get("completion_date"),
				'wattage' => $Celamed->__get("wattage"),
				'size' => $Celamed->__get("size"),
				'device_type' => $Celamed->__get("device_type"),
				'description' => $Celamed->__get("description"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			$date = new Zend_Date();
        	$date->setTimezone("Asia/Kolkata");
        	$curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");
        	$current_date = date_parse_from_format("Y-m-d", $curr_date);
        	
        	  
        	$timestamp = $date->toString("ddMMYYss" . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9));
        	$cr_amount = str_pad($Celamed->__get("equipment_cost_tara"), 4, "0", STR_PAD_LEFT);
        	$transaction_id = $timestamp . "-" . $cr_amount;
        	
        	$cashRegisterMapper = new Application_Model_CashRegisterMapper();
        	$cashRegister = new Application_Model_CashRegister();
        	$cashRegister->__set("user_id", 0);
        	$cashRegister->__set("consumer_id", $Celamed->__get("consumer_id"));
        	$cashRegister->__set("cr_entry_type", "DEBIT");
        	$cashRegister->__set("cr_amount", $Celamed->__get("equipment_cost_tara"));
        	$cashRegister->__set("receipt_number", 0);
        	$cashRegister->__set("transaction_id", $transaction_id);
        	$cashRegister->__set("timestamp", $date->toString("yyyy-MM-dd HH:mm:ss"));
        	$cashRegister->__set("entry_status", ($Celamed->__get("equipment_type")==0?"EED":"MED"));
        	$cashRegister->__set("remark", "Transaction for CeLAMeD");
        	$cashRegisterMapper->addNewCashRegister($cashRegister,null,'web_tran');
			
			return $result;
		}
	}
	public function getCelamedById($id)
	{
		
		$result = $this->_db_table->find($id);
		
		if( count($result) == 0 ) {
			$state = new Application_Model_Celamed();
			return $state;
		}
		$row = $result->current();
		$state = new Application_Model_Celamed($row);
	
		return $state;
	}
	
	public function getAllCelameds()
	{
		$query="select celamed.* from celamed inner join consumers on celamed.consumer_id=consumers.consumer_id 
				inner join sites on consumers.site_id=sites.site_id where consumers.consumer_status!='banned' and sites.site_status!='Disable' order by celamed.consumer_id desc";
	
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($query);
	
		if(count($result) == 0 ) {
			return false;
		}
		$consumer_object_arr = array();
		foreach ($result as $row)
		{
			$consumer_object = new Application_Model_Celamed();
			foreach($row as $key=>$value)
			{
				$consumer_object->__set($key,$value);
			}
			array_push($consumer_object_arr,$consumer_object);
		}
	
		return $consumer_object_arr;
	}
	
	public function updateCelamed(Application_Model_Celamed $Celamed)
	{
		$data = array(
				'consumer_id' => $Celamed->__get("consumer_id"),
				'site_id' => $Celamed->__get("site_id"),
				'intervention_type' => $Celamed->__get("intervention_type"),
				'equipment_cost_total' => $Celamed->__get("equipment_cost_total"),
				'equipment_cost_tara' => $Celamed->__get("equipment_cost_tara"),
				'equipment_type' => $Celamed->__get("equipment_type"),
				'equipment_summary' => $Celamed->__get("equipment_summary"),
				'Contact_date' => $Celamed->__get("Contact_date"),
				'payment_date' => $Celamed->__get("payment_date"),
				'completion_date' => $Celamed->__get("completion_date"),
				'wattage' => $Celamed->__get("wattage"),
				'size' => $Celamed->__get("size"),
		);
		$where = "id = " . $Celamed->__get("id");
		$result = $this->_db_table->update($data,$where);
			return true;
		
	}
	
	
	public function deleteCelamedsById($id)
	{
		$where = "id = " . $id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public function getCelamedBySiteId($column,$value,$all=true,$year=NULL,$month=NULL,$suspense=FALSE)
	{
		 
		/* $where = array(
		 $column." = ?" => $value
		);*/
		$where="";
		
		$query="select celamed.* from celamed inner join consumers on celamed.consumer_id=consumers.consumer_id 
				inner join sites on consumers.site_id=sites.site_id where consumers.".$column." ='".$value."' and sites.site_status<>'Disable' order by celamed.consumer_id desc";
	
		//echo $query;
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($query);
	
		if(count($result) == 0 ) {
			return false;
		}
		$consumer_object_arr = array();
		foreach ($result as $row)
		{
			$consumer_object = new Application_Model_Celamed();
			foreach($row as $key=>$value)
			{
				$consumer_object->__set($key,$value);
			}
			array_push($consumer_object_arr,$consumer_object);
		}
	
		return $consumer_object_arr;
	}
	
	public function getAllConsumersByCelamed()
	{
		$where="";
	
		$query="SELECT * FROM consumers inner join sites on sites.site_id=consumers.site_id
 where consumer_id not in(select consumer_id from celamed) and consumers.consumer_status!='banned' and sites.site_status!='Disable'";
	
		 
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($query);
	
		if(count($result) == 0 ) {
			return false;
		}
		 $consumer_object_arr = array();
        foreach ($result as $row)
        {
            $consumer_object = new Application_Model_Consumers($row);
	    	$consumer_object->__set("consumer_id", $row["consumer_id"]);
	    	$consumer_object->__set("package_id", $row["package_id"]);
            $consumer_object->__set("site_id", $row["site_id"]);
            $consumer_object->__set("consumer_code", $row["consumer_code"]);
            $consumer_object->__set("consumer_name", $row["consumer_name"]);
            $consumer_object->__set("consumer_father_name", $row["consumer_father_name"]);
            $consumer_object->__set("consumer_status", $row["consumer_status"]);
            $consumer_object->__set("consumer_act_date", $row["consumer_act_date"]);
            $consumer_object->__set("consumer_act_charge", $row["consumer_act_charge"]);
            $consumer_object->__set("is_micro_enterprise", $row ["is_micro_enterprise"]);
            $consumer_object->__set("meter_start_reading", $row["meter_start_reading"]);
           
            $consumer_object->__set("micro_entrprise_price", $row["micro_entrprise_price"]);
            $consumer_object->__set("consumer_connection_id", $row["consumer_connection_id"]);
            $consumer_object->__set("suspension_date", $row["suspension_date"]);
            $consumer_object->__set("sms_opt_in", $row["sms_opt_in"]);
            $consumer_object->__set("enterprise_type", $row["enterprise_type"]);
            $consumer_object->__set("type_of_me", $row["type_of_me"]);
            $consumer_object->__set("notes", $row["notes"]);
                array_push($consumer_object_arr,$consumer_object);
        }
        return $consumer_object_arr;
	}

	public function checkConsumerByConsumerId($consumer_id)
	{
		$where="";
	
		$query="SELECT * FROM celamed where consumer_id=".$consumer_id;
	
			
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($query);
	
		if(count($result) == 0 ) {
			return true;
		}
		else{
			return false;
		}
	}
	public function updateSiteIdCelamed($site_id,$consumer_id)
	{
		
		$query="update celamed set site_id=".$site_id." where consumer_id=".$consumer_id;
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		
		if($result)
		{
			return true;
		}
	}
	
	 
	public function deleteCelamedByConsumerId($consumer_id)
	{
	
		$query="delete from celamed where consumer_id=".$consumer_id;
	
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
	
		if($result)
		{
			return true;
		}
	}
	
	public function getCelamedConsumerBySiteId($site_id=NULL)
	{
			
	 
		 $query="select * from celamed inner join consumers on celamed.consumer_id=consumers.consumer_id
				inner join sites on consumers.site_id=sites.site_id where celamed.site_id=".$site_id." and consumers.consumer_status!='banned' and sites.site_status!='Disable' order by celamed.consumer_id desc";
	
		 
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($query);
	
		if(count($result) == 0 ) {
			return false;
		}
		$consumer_object_arr = array();
		foreach ($result as $row)
		{
			$consumer_object = new Application_Model_Celamed();
			foreach($row as $key=>$value)
			{
				$consumer_object->__set($key,$value);
			}
			array_push($consumer_object_arr,$consumer_object);
		}
	
		return $consumer_object_arr;
	}
	public function getCelamedByStateId($state_id=NULL,$role_sites_id=NULL)
	{
			
	
		$query="select * from celamed inner join consumers on celamed.consumer_id=consumers.consumer_id
				inner join sites on consumers.site_id=sites.site_id where sites.state_id=".$state_id." and consumers.consumer_status!='banned' and consumers.site_id in (".implode(",", $role_sites_id).") and sites.site_status!='Disable' order by celamed.consumer_id desc";
	
			
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($query);
	
		if(count($result) == 0 ) {
			return false;
		}
		$consumer_object_arr = array();
		foreach ($result as $row)
		{
			$consumer_object = new Application_Model_Celamed();
			foreach($row as $key=>$value)
			{
				$consumer_object->__set($key,$value);
			}
			array_push($consumer_object_arr,$consumer_object);
		}
	
		return $consumer_object_arr;
	}
}