<?php

class Application_Model_PackageHistory
{
	private $package_history_id;
	private $package_id;
	private $consumer_id;
	private $package_change_date;
	private $change_by;
	private $package_cost;
	private $change_description;
	private $timestamp;
	private $status; 
	private $last_package;

	public function __construct($package_history_row = null)
	{
		if( !is_null($package_history_row) && $package_history_row instanceof Zend_Db_Table_Row ) {
			$this->package_history_id = $package_history_row->package_history_id;
			$this->package_id = $package_history_row->package_id;
			$this->consumer_id = $package_history_row->consumer_id;
			$this->package_change_date = $package_history_row->package_change_date;
			$this->change_by = $package_history_row->change_by;
			$this->package_cost = $package_history_row->package_cost;
			$this->change_description=$package_history_row->change_description;
			$this->timestamp=$package_history_row->timestamp;
			$this->status=$package_history_row->status;
			$this->last_package=$package_history_row->last_package;
		}
	}
	public function __set($name, $value)
	{
		 
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}

}

