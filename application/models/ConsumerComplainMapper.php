
<?php
class Application_Model_ConsumerComplainMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_ConsumerComplain();
	}

	public function addConsumerComplain(Application_Model_ConsumerComplain $CD)
	{
		$data = array(
				'consumer_id' => $CD->__get("consumer_id"),
				'service_id' => $CD->__get("service_id"),
				'remark_id' => $CD->__get("remark_id"),
				'rating' => $CD->__get("rating"),
				'status' => $CD->__get("status"),
				'entry_date' => $CD->__get("entry_date"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	 
	
	public function getConsumerComplain($consumer_id=NULL,$status=NULL)
	{
		$where="";
		if($consumer_id!=NULL){
			$where=" where consumer_id=".$consumer_id;
		}
		
		if($status!=NULL){
			$where.=" and status='Completed' and (rating is null or rating ='' or rating='undefined')";
		}
		  
		$query ="select * from consumer_complain".$where;
		
		$stmt= $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
	
		if (count($result) == 0) {
			return false;
		}
		return $result;
	}
	
	public function updateRatingByReqId($consumer_id=NULL,$service_id=NULL,$rating=NULL)
	{
	
		$query ="update consumer_complain set rating=".$rating." where consumer_id =".$consumer_id." and service_id=".$service_id;
 	 
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
	
		if($result)
		{
			return true;
		}
	}
	 
}