<?php
class Application_Model_DeviceMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Device();
	}

	public function addDevice(Application_Model_Device $state)
	{

		$data = array(
				'device_name' => $state->__get("device_name"),
				'site_id' => $state->__get("site_id"),
				'iccid' => $state->__get("iccid"),
				'device_sim' => $state->__get("device_sim"),
				'time' => $state->__get("time"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getDeviceById($device_id)
	{
		
		$result = $this->_db_table->find($device_id);
		
		if( count($result) == 0 ) {
            $device = new Application_Model_Device();
			return $device;
		}
		$row = $result->current();
        $device = new Application_Model_Device($row);
	
		return $device;
	}
	
	public function getAllDevice($condition=NULL,$page=NULL,$order=NULL)
	{
	   
		$where = "";
		if($condition!=NULL)
		{
			$where = " where site_id = " . $condition." ";
		}
		/*if($page!=null){
			  $limit=" limit ".$page.",6 ";
		}else{
			  $limit=" limit 0,12 ";
		}*/
		
		if($order!=null){
			  $order=" order by ".$order." ";
		}else{
			  $order=" "; 
		} 
		//$result = $this->_db_table->fetchAll($where,array('device_id DESC'))->limit(2); 
		   
		  $query="SELECT * FROM devices ".$where.$order;
 
        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);
		 
		if( count($result) == 0 ) {
			return false;
		} 
		return $result; 
		/*$state_object_arr = array();
		foreach ($result as $row)
		{
			$state_object = new Application_Model_Device($row);
			array_push($state_object_arr,$state_object);
		}
		return $state_object_arr;*/
	}
	
	public function updateDevice(Application_Model_Device $states)
	{
		$data = array(
				'time' => $states->__get("time"),
                'device_sim' => $states->__get("device_sim"),
				'device_name' => $states->__get("device_name"),
		);
		$where = "device_id = " . $states->__get("device_id");
		$result = $this->_db_table->update($data,$where);
		 
			return true;
		 
	}
	
	  
	public function deleteDeleteById($device_id)
	{
		$where = "device_id = " . $device_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function getDeviceBySimNo($sim_no)
   	{
   		$where = array(
   				"device_sim = ?" => $sim_no
   		);
   		$result = $this->_db_table->fetchRow($where);
   		if( count($result) == 0 ) {
   			return false;
   		}
   		$row = $result;
   		$Device = new Application_Model_Device($row);
   		return $Device;
   	}
	
	public function getDeviceCount($site_id=null)
    { 
		$where = "";
		if($site_id!=NULL)
		{
			$where = " where site_id = " . $site_id;
		}
         $query="SELECT * FROM devices ".$where;
 
        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);
		 
		if( count($result) == 0 ) { 
			return 0;
		} 
		return count($result); 
    }
	 public function getDeviceByDeviceName($device_id=null,$device_name=null)
    {
    	   $query="SELECT * FROM devices where device_name='".$device_name."' and device_id!=".$device_id;
 
        $stmt= $this->_db_table->getAdapter();
        $result = $stmt->fetchAll($query);
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return count($result); 
    }
	
	public function updateSmsDeviceStatsus($sms_status=NULL,$sms_timestamp=NULL,$device_id=NULL)
	{
		$data = array(
				'sms_status' => $sms_status,
                'sms_timestamp' => $sms_timestamp,
		);
		$where = "device_id = " . $device_id;
		$result = $this->_db_table->update($data,$where);
		 
			return true;
		 
	}
}