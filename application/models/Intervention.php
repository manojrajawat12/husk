<?php

class Application_Model_Intervention
{
    private $id;
    private $type;
    

    public function __construct($state_row = null)
    {
        if( !is_null($state_row) && $state_row instanceof Zend_Db_Table_Row ) {
            
                $this->id = $state_row->id;
                $this->type = $state_row->type;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

