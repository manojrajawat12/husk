<?php

class Application_Model_WalletTransactionsMapper
{
    protected $_db_table;

    public function __construct()
    {
        $this->_db_table = new Application_Model_DbTable_WalletTransactions();
    }

    public function addNewWalletTransaction(Application_Model_WalletTransactions $obj)
    {
        $data = array(
            "consumer_id" => $obj->__get("consumer_id"),
            "user_id" => $obj->__get("user_id"),
            "receipt_number" => $obj->__get("receipt_number"),
            "transaction_id" => $obj->__get("transaction_id"),
            "entry_type" => $obj->__get("entry_type"),
            "amount" => $obj->__get("amount"),
            "timestamp" => $obj->__get("timestamp"),
            "balance" => $obj->__get("balance"),
            "comment" => $obj->__get("comment"),
        );

        $result = $this->_db_table->insert($data);
        if ($result == 0) {
            return false;
        } else {
        	$consumersmapper=new Application_Model_ConsumersMapper();
        	$consumers=$consumersmapper->updateConsumerCodeByCustomer_id('wallet_bal',$obj->__get("balance"),$obj->__get("consumer_id"));
            return $result;
        }
    }


   /*  public function getAllWalletTransactionsByConsumerId($consumer_id)
    {
        $where = array(
            "consumer_id = ?" => $consumer_id,
        );
        $result = $this->_db_table->fetchAll($where,array("id DESC"));
        if (!$result) {
            return false;
        }
        $result_arr = array();

        foreach ($result as $row) {
            $row_obj = new Application_Model_WalletTransactions($row);
            $result_arr[] = $row_obj;
        }
        return $result_arr;

    }

    public function getAllWalletTransactions()
    {
        $result = $this->_db_table->fetchAll(null,array("id DESC"));
        if( count($result) == 0 ) {
            return false;
        }
        $requested_object_arr = array();
        foreach ($result as $row)
        {
            $requested_object = new Application_Model_WalletTransactions($row);
            array_push($requested_object_arr,$requested_object);
        }
        return $requested_object_arr;
    } */
    
	public function getLastEntryconsumerID($consumer_id)
    {
    	$mongoDriver= new MongoClient();
    	$mongoDB=$mongoDriver->tara_devices;
    	$mongoCollection = $mongoDB->ierms;
        
    	$result=  $mongoCollection->find(["current"=>array('$elemMatch'=>array(
							"consumer_id"=> "$consumer_id","volt"=>array('$ne'=>"0000")
						))])
			->sort([
    			"info_date"=>-1
    	  	])->limit(1);
		 
    	if(count($result) == 0) {
    		return false;
    	}
		  
    	$data = [];
		
    	foreach ($result as $key => $value){
    		$data[] = $value;
    	}
		return $data[0];
    }
	
	public function getAllWalletTransactions()
    {
    	$mongoDriver= new MongoClient();
    	$mongoDB=$mongoDriver->tara_devices;
    	$mongoCollection = $mongoDB->wallet_transactions;
     
    	$result=  $mongoCollection->find([
            "entry_type"=> "CREDIT"
        ])->sort([
    			"timestamp"=>-1
    	]);
      
    	if($result->count() == 0) {
    		return false;
		}
    	$data = [];
    	foreach ($result as $key => $value){
    		$data[] = $value;
    	}
		//echo count($data);exit;
		//print_r($data);exit;
    	return $data;
    }
    
    public function getAllWalletTransactionsByConsumerId($consumer_id)
    {
    	$mongoDriver= new MongoClient();
        $mongoDB=$mongoDriver->tara_devices;
        $mongoCollection = $mongoDB->wallet_transactions;
		
        $result=  $mongoCollection->find([
            "consumer_id"=> $consumer_id
        ])->sort([
            "timestamp"=>-1
        ]);

        if($result->count() == 0) {
            return false;
        }
        $data = [];
        foreach ($result as $key => $value){
            $data[] = $value;
        }
          
        return $data;
    
    }
    
    public function deleteWalletTransactionsById($id)
    {
        $where = array(
            "id = ?" => $id
        );
        $result = $this->_db_table->delete($where);
        return $result;
    }

    public function getLastWalletTransactionByConsumerId($consumerId){
        $stmt = $this->_db_table->getAdapter();
        $query = "SELECT * from wallet_transactions where consumer_id = ".$consumerId." order by id desc limit 1";
        
        $result = $stmt->fetchRow($query);
        if(!$result){
            return false;
        }

        $object = new Application_Model_WalletTransactions();
        $object->__set('id',$result['id']);
        $object->__set('consumer_id',$result['consumer_id']);
        $object->__set('user_id',$result['user_id']);
        $object->__set('receipt_number',$result['receipt_number']);
        $object->__set('transaction_id',$result['transaction_id']);
        $object->__set('entry_type',$result['entry_type']);
        $object->__set('amount',$result['amount']);
        $object->__set('timestamp',$result['timestamp']);
        $object->__set('balance',$result['balance']);
        $object->__set('comment',$result['comment']);

        return $object;

    }

    /**
     * @param $consumerId
     * @return int
     */
    public static function getConsumerBalance($consumerId)
    {
        if(!$consumerId || !intval($consumerId)){
            return 0;
        }
        $walletTransactionMapper = new Application_Model_WalletTransactionsMapper();
        $lastWalletEntry = $walletTransactionMapper->getLastWalletTransactionByConsumerId($consumerId);
        if($lastWalletEntry){
            $balance = $lastWalletEntry->__get('balance');
            return $balance?$balance:0;
        }
        $cashRegisterMapper = new Application_Model_CashRegisterMapper();
        $creditAmount = $cashRegisterMapper->getTotalCreditByConsumerId($consumerId);
        $debitAmount = $cashRegisterMapper->getTotalDebitByConsumerId($consumerId);
        $creditAmount = $creditAmount?$creditAmount:0;
        $debitAmount = $debitAmount?$debitAmount:0;

        $balance = $creditAmount - $debitAmount;

        return $balance;

    }
    
    public function updateWalletTransaction(Application_Model_WalletTransactions $WalletTransaction) {
    	 $data = array(
    				'user_id' => $WalletTransaction->__get("user_id"),
    				'receipt_number' => $WalletTransaction->__get("receipt_number"),
    				'transaction_id' => $WalletTransaction->__get("transaction_id"),
    				'entry_type' => $WalletTransaction->__get("entry_type"),
    				'amount' => $WalletTransaction->__get("amount"),
    				'timestamp' => $WalletTransaction->__get("timestamp"),
    				'balance' => $WalletTransaction->__get("balance"),
    				'comment'=>$WalletTransaction->__get("comment"),
    		);
    	 
    	 
    	$where = "consumer_id = " . $WalletTransaction->__get("consumer_id");
    
    	$result = $this->_db_table->update($data, $where);
    
    	if ($result== 0) {
    		return false;
    	} else {
    		$consumersmapper=new Application_Model_ConsumersMapper();
    		$consumers=$consumersmapper->updateConsumerCodeByCustomer_id('wallet_bal',$WalletTransaction->__get("balance"),$WalletTransaction->__get("consumer_id"));
    		return true;
    	}
    }
}
