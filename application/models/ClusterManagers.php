<?php

class Application_Model_ClusterManagers
{
    private $cluster_manager_id;
    private $cluster_manager_name;
    private $cluster_manager_phone;
    private $cluster_manager_email;
    

    public function __construct($cluster_manager_row = null)
    {
        if( !is_null($cluster_manager_row) && $cluster_manager_row instanceof Zend_Db_Table_Row ) {
            
                $this->cluster_manager_id = $cluster_manager_row->cluster_manager_id;
                $this->cluster_manager_name = $cluster_manager_row->cluster_manager_name;
                $this->cluster_manager_phone = $cluster_manager_row->cluster_manager_phone;
                $this->cluster_manager_email = $cluster_manager_row->cluster_manager_email;
                
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "ap_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }
}

