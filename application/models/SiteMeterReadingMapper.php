 <?php
class Application_Model_SiteMeterReadingMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_SiteMeterReading();
    }

    public function addNewSiteMeterReading(Application_Model_SiteMeterReading $meter_reading)
    {
        $data = array(
        	 'site_id' => $meter_reading->__get("site_id"),
			 'meter_id' => $meter_reading->__get("meter_id"),
			 'reading_date' => $meter_reading->__get("reading_date"),
			 'reading' => $meter_reading->__get("reading"),
			 'actual_reading' => $meter_reading->__get("actual_reading"),
			 'entry_by' => $meter_reading->__get("entry_by"),
			 'entry_type' => $meter_reading->__get("entry_type"),
        );
		
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return $result;
        }
    }
    public function getSiteMasterById($site_id)
    {
        $query = " SELECT * From site_meter where site_id ='$site_id' order by feeder_no asc";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$SiteMaster_object_arr = array();
    	foreach ($result as $row)
    	{
    		$SiteMaster_object = new Application_Model_SiteMasterMeter($row);
    		$SiteMaster_object->__set("id", $row["id"]);
    		$SiteMaster_object->__set("meter_name", $row["meter_name"]);
    		$SiteMaster_object->__set("meter_keyword", $row["meter_keyword"]);
    		array_push($SiteMaster_object_arr,$SiteMaster_object);
    	}
    	return $SiteMaster_object_arr;
    }
    public function getAllSiteMeterReading()
    {
    $query = " SELECT * From site_meter_reading inner join sites on site_meter_reading.site_id=sites.site_id where sites.site_status!='Disable'";
	$stmt = $this->_db_table->getAdapter()->query($query);
	$result = $stmt->fetchAll();
	 
	if( count($result) == 0 ) {
		return false;
	}else{
		
	 $SiteMaster_object_arr = array();
	 foreach ($result as $row)
	 {
	 $SiteMaster_object = new Application_Model_SiteMeterReading($row);
	 $SiteMaster_object->__set("site_id", $row["site_id"]);
	 $SiteMaster_object->__set("meter_id", $row["meter_id"]);
	 $SiteMaster_object->__set("id", $row["id"]);
	 $SiteMaster_object->__set("reading_date", $row["reading_date"]);
	 $SiteMaster_object->__set("reading", $row["reading"]);
	 array_push($SiteMaster_object_arr,$SiteMaster_object);
	 }
	 return $SiteMaster_object_arr; 
	}
    }
    public function updateSiteReading(Application_Model_SiteMeterReading $siteMeter)
    {
    	$id=$siteMeter->__get("id");
    	$site_id= $siteMeter->__get("site_id");
    	$meter_id= $siteMeter->__get("meter_id");
        $reading_date=$siteMeter->__get("reading_date");
        $reading=$siteMeter->__get("reading");
        
        $query ="update site_meter_reading set site_id=".$site_id.",meter_id='".$meter_id."',reading_date='".$reading_date."',reading='".$reading."' where id=".$id;
		
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result=$stmt->execute();
        
        if($result)
        {
        	return true;
        }
    }
    public function updateSiteReadingByMeterId($site_id,$meter_id,$date,$reading)
    {
     
    
    	 $query ="update site_meter_reading set reading='".$reading."' where  site_id=".$site_id." and meter_id='".$meter_id."' and reading_date='".$date."'";
		
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    
    	if($result)
    	{
    		return true;
    	}
    }
    public function deleteSiteReadingById($id)
    {
        $where = "id = " . $id;
        $result = $this->_db_table->delete($where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }

    /*public function checkSiteReading($site_id,$meter_id,$reading_date=NULL){
    	
    	if($reading_date!=NULL){
    		$reading_dates=" and reading_date='".$reading_date."'";
    	}else{
    		$reading_dates=" ";
    	}
    	$query="SELECT * FROM site_meter_reading where site_id=".$site_id." and meter_id=".$meter_id." ".$reading_dates." order by reading_date desc limit 1";
			
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	//print_r($result);
    	if( count($result) == 0 ) {
    		return false;
    	}
    	else{
    		return $result[0];
    	}
    
    }*/
   public function checkSiteReading($site_id,$meter_id,$reading_date=NULL,$id=NULL){
     
		if($id!=NULL){
			$check_id=" and id!=".$id;
		}else{
			$check_id=" ";
		} 
			
		$query="SELECT * FROM site_meter_reading where site_id=".$site_id." and meter_id=".$meter_id." and reading_date='".$reading_date."'".$check_id;
    	
		$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	else{
    		return $result[0]; 
    	}
    
    }
  public function  getEnterprisePriceByUnit($consumer_id)
   {
	   	$result = $this->_db_table->fetchAll(null,array('unit_from ASC'));
	   	if( count($result) == 0 ) {
	   		return false;
	   	}
	   	$enterprise_object_arr = array();
	   	foreach ($result as $row)
	   	{
	   		$enterprise_object = new Application_Model_EnterprisePrice($row);
	   		array_push($enterprise_object_arr,$enterprise_object);
	   	}
	   	return $enterprise_object_arr;
   }
   
   public function  deleteSiteMeterBySiteId($site_id)
   {
   		$query ="delete from site_meter where site_id=".$site_id;

        $stmt = $this->_db_table->getAdapter()->query($query);
        $result=$stmt->execute();
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
   }
   public function  deleteSiteMeterById($id)
   {
   	$query ="delete from site_meter where id=".$id;
   
   	$stmt = $this->_db_table->getAdapter()->query($query);
   	$result=$stmt->execute();
   	if($result==0)
   	{
   		return false;
   	}
   	else
   	{
   		return true;
   	}
   }
   public function  getEnterprisePriceBySchemeId($enterprise_scheme)
   {
   	    $query = " SELECT * From enterprise_price where enterprise_scheme ='$enterprise_scheme'";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	//print_r($result);
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$enterprise_scheme_object_arr = array();
    	foreach ($result as $row)
    	{
    		$enterprise_scheme_object = new Application_Model_EnterprisePrice($row);
    		$enterprise_scheme_object->__set("unit_from", $row["unit_from"]);
    		$enterprise_scheme_object->__set("unit_to", $row["unit_to"]);
    		$enterprise_scheme_object->__set("enterprise_price", $row["enterprise_price"]);
    		$enterprise_scheme_object->__set("enterprise_scheme", $row["enterprise_scheme"]);
    		$enterprise_scheme_object->__set("enterprise_price_id", $row["enterprise_price_id"]);
    		array_push($enterprise_scheme_object_arr,$enterprise_scheme_object);
    	}
    	return $enterprise_scheme_object_arr;
   }

public function getSiteReadingByMeterId($meter_id,$reading_date,$site_id)
    {
        $query = " SELECT * From site_meter where site_id=".$site_id." and id ='".$meter_id."' order by feeder_no asc";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return true;
    	}
    }   

public function getMeterReadingByMeterId($meter_id)
{
	$query = " SELECT * From site_meter_reading where meter_id ='$meter_id' order by reading_date desc";
	$stmt = $this->_db_table->getAdapter()->query($query);
	$result = $stmt->fetchAll();
	 
	if( count($result) == 0 ) {
		return false;
	}else{
		
	 $SiteMaster_object_arr = array();
	 foreach ($result as $row)
	 {
		 $SiteMaster_object = new Application_Model_SiteMeterReading($row);
		 $SiteMaster_object->__set("site_id", $row["site_id"]);
		 $SiteMaster_object->__set("meter_id", $row["meter_id"]);
		 $SiteMaster_object->__set("id", $row["id"]);
		 $SiteMaster_object->__set("reading_date", $row["reading_date"]);
		 $SiteMaster_object->__set("reading", $row["reading"]);
		 $SiteMaster_object->__set("actual_reading", $row["actual_reading"]);
		array_push($SiteMaster_object_arr,$SiteMaster_object);
	 }
	 return $SiteMaster_object_arr; 
	}
}
public function getDateSiteReadingByMeterId($meter_id,$reading_date,$site_id)
{
	$query = " SELECT * From site_meter_reading where site_id=".$site_id." and meter_id ='$meter_id' and reading_date='".$reading_date."'";
	$stmt = $this->_db_table->getAdapter()->query($query);
	$result = $stmt->fetchAll();
	 
	if( count($result) == 0 ) {
		return true;
	}else{
		return false;
	}
}
public function getEnergyConsumedById($curr_date=NULL,$last_date=NULL,$site_id=NULL,$role_sites_id=NULL,$state_id=NULL,$meter_id=NULL)
{
	 if($site_id!=NULL){
	  	$where=" and sites.site_id in (".$site_id.")";
	 }elseif($state_id!=NULL){
	  	$where=" and sites.state_id in (".$state_id.")";
	 }elseif(count($role_sites_id)>0){
	  	$where=" and sites.site_id in (".implode(",", $role_sites_id).")";
	 } 
 
	 if($meter_id!=NULL){
		$meter_ids=" and meter_id =".$meter_id;
	 }else{
		$meter_ids=" ";
	 }
	  
    $query = "select sum(reading) as TotalReading from site_meter_reading inner join site_meter on site_meter.id =site_meter_reading.meter_id inner join sites on sites.site_id=site_meter_reading.site_id 
       where date(reading_date) >= '".$last_date."' AND  date(reading_date)<'".$curr_date."'".$where." and sites.site_status!='Disable' ".$meter_ids;
	         
	 $stmt = $this->_db_table->getAdapter()->query($query);
	 $result = $stmt->fetchAll();
	  
	 if( count($result) == 0 ) {
	  return false;
	 }else{
	  
	  return $result[0]["TotalReading"];
	 }
    }

	
	 
	public function getTotalFeedarReadingByDate($site_id=NULL,$day=NULL,$month=NULL,$year=NULL)
    {
    	 
    	   $query = "SELECT * FROM site_meter_reading where site_id=".$site_id." and day(reading_date)=".$day." and month(reading_date)=".$month." and year(reading_date)=".$year;
    	 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result;
    	}
    }
    public function getTotalFeedarReadingByFeedarName($site_id=NULL,$day=NULL,$month=NULL,$year=NULL,$feedar_unit=NULL,$sign=NULL)
    {
    
    	$join="inner join site_meter_reading mr on m.id=mr.meter_id ";
    	$dateFormat="where mr.site_id=".$site_id." and day(reading_date)=".$day." and month(reading_date)=".$month." and year(reading_date)=".$year." "; 
    	$signs="";
    	if($sign!=NULL){ 
    		if($sign=='less'){
    			$signs=" and reading<=".$feedar_unit;
    		}else{
    			$signs=" and reading>=".$feedar_unit;
    		}
    	}
    	   $query = "SELECT (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER1' ) as FEEDER1,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER2' ) as FEEDER2,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER3' ) as FEEDER3,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER4' ) as FEEDER4,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER5' ) as FEEDER5,
					 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER6' ) as FEEDER6,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER7' ) as FEEDER7,
					 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER8' ) as FEEDER8,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER9' ) as FEEDER9,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER10' ) as FEEDER10,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER11' ) as FEEDER11,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER12' ) as FEEDER12,
					 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER13' ) as FEEDER13,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER14' ) as FEEDER14,
					 (select sum(reading) as reading from site_meter m ".$join.$dateFormat.$signs." and m.meter_name='FEEDER15' ) as FEEDER15
    		 from site_meter_reading limit 1";
    	 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0];
    	}
    }
	
	
    public function getTotalFeedaarReadingFilters($state_id=NULL,$site_id=NULL,$role_sites_id=NULL,$months=NULL,$year=NULL)
    {
    	if($site_id!=NULL){
    		$where=" and s.site_id=".$site_id;
    	}elseif($state_id!=NULL){
    		$where=" and s.state_id=".$state_id;
    	}elseif(count($role_sites_id)>0){
    		$where=" and s.site_id in (".implode(",", $role_sites_id).")";
    	}else{
    		$where=" ";
    	}
    	$nmonth = date("m", strtotime($months));
    	$query = " SELECT sum(m.reading) as reading From site_meter_reading m inner join sites s on s.site_id=m.site_id where s.site_status!='Disable' and
				month(reading_date)=".$nmonth." and year(reading_date)=".$year."".$where;
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0]["reading"];
    	}
    }
	
	public function getFeedarReadingBySiteIdDate($site_id=NULL,$first=NULL,$second=NULL)
    {
    	$join="inner join site_meter_reading mr on m.id=mr.meter_id ";
    	if($second!=NULL){
    		$dateFormat=" where mr.site_id =".$site_id." and date(mr.reading_date)>='".$first."' and date(mr.reading_date)<='".$second."'";
    	}else{
    		$dateFormat="where mr.site_id=".$site_id." and date(reading_date)='".$first."'";
    	}
    	   $query = "SELECT (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER1' ) as FEEDER1,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER2' ) as FEEDER2,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER3' ) as FEEDER3,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER4' ) as FEEDER4,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER5' ) as FEEDER5,
					 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER6' ) as FEEDER6,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER7' ) as FEEDER7,
					 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER8' ) as FEEDER8,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER9' ) as FEEDER9,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER10' ) as FEEDER10,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER11' ) as FEEDER11,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER12' ) as FEEDER12,
					 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER13' ) as FEEDER13,
    				 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER14' ) as FEEDER14,
					 (select sum(reading) as reading from site_meter m ".$join.$dateFormat." and m.meter_name='FEEDER15' ) as FEEDER15
    		 from site_meter_reading limit 1";
    	 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0];
    	}
    }
	
	public function getFeedarReadingBySiteIdDesc($site_id=NULL)
    {
    	   $dateFormat=" where site_id =".$site_id;
     
    	     $query = "SELECT (select description from site_meter m ".$dateFormat." and m.meter_name='FEEDER1' limit 1 ) as description1,
    				 (select description from site_meter m ".$dateFormat." and m.meter_name='FEEDER2' limit 1 ) as description2,
    				 (select description from site_meter m ".$dateFormat." and m.meter_name='FEEDER3' limit 1 ) as description3,
    				 (select description from site_meter m ".$dateFormat." and m.meter_name='FEEDER4' limit 1 ) as description4,
    				 (select description from site_meter m ".$dateFormat." and m.meter_name='FEEDER5' limit 1 ) as description5,
					 (select description from site_meter m ".$dateFormat." and m.meter_name='FEEDER6' limit 1 ) as description6,
    				 (select description from site_meter m ".$dateFormat." and m.meter_name='FEEDER7' limit 1 ) as description7
    		 from site_meter_reading limit 1";
    	  
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0];
    	}
    }
	
	public function getFeederTotalByFilter($site_id=NULL,$state_id=NULL,$role_site_id=NULL,$first=NULL,$second=NULL,$meter_name=NULL)
    {
    	 
    	$mpptjoin=" inner join site_meter_reading smr on sm.id=smr.meter_id inner join sites s on s.site_id=sm.site_id";
    	 
    	if($site_id!=NULL){
    		$mpptFormat=" where sm.site_id in (".$site_id.") and date(smr.reading_date)<='".$first."' and date(smr.reading_date)>='".$second."' and s.site_status!='Disable'";
    	}elseif($state_id!=NULL){
    		$mpptFormat=" where s.state_id in (".$state_id.") and date(smr.reading_date)<='".$first."' and date(smr.reading_date)>='".$second."' and s.site_status!='Disable'";
    	}else{
    		$mpptFormat=" where sm.site_id in (".implode(",", $role_site_id).") and date(smr.reading_date)<='".$first."' and date(smr.reading_date)>='".$second."' and s.site_status!='Disable'";
    	}
    	 
    	$query = "select meter_name,feeder_type,description, sum(reading) as reading from site_meter sm ".$mpptjoin." ".$mpptFormat." and sm.meter_name='".$meter_name."'";
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $result[0];
    }
	 
	public function getLastReadingByMeterId($meter_id,$reading_date,$site_id)
	{
		$query = " SELECT * From site_meter_reading where site_id=".$site_id." and meter_id ='$meter_id' and reading_date<'".$reading_date."' order by reading_date desc";
		
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		 
		if( count($result) == 0 ) {
			return true;
		}else{
			return $result[0];
		}
	}
	
	public function getFeedarReadingByFeederIdDate($meter_id=NULL,$first=NULL,$second=NULL)
    {
    	$query = "select sum(reading) as reading from site_meter_reading where 
					date(reading_date)>='".$first."' and date(reading_date)<='".$second."' and meter_id=".$meter_id;
    	
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0]["reading"];
    	}
    }
	
	public function getFeedarReadingByDateVal($site_id=NULL,$first=NULL,$feeder_id=NULL)
    {
    	$join="inner join site_meter_reading mr on m.id=mr.meter_id ";
    	$dateFormat="where mr.site_id=".$site_id." and date(reading_date)='".$first."'";
    	
    	$query = "SELECT sum(reading) as reading from site_meter m ".$join.$dateFormat." and meter_id =". $feeder_id."";
    	
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0]["reading"];
    	}
    }
	
	public function getFeedarReadingBySiteIdReadingDate($site_id=NULL,$first=NULL)
    {
    	$join="inner join site_meter_reading mr on m.id=mr.meter_id ";
    	$dateFormat="where mr.site_id=".$site_id." and date(reading_date)='".$first."' order by meter_name asc";
    	 
    	$query = "SELECT * from site_meter m ".$join.$dateFormat;
    	 
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result;
    	}
    }
    
    public function getFeedarReadingByTwoSideCheck($feedar_id=NULL,$first=NULL)
    {
    	$dateFormatLast="where meter_id=".$feedar_id." and date(reading_date)<'".$first."' order by reading_date desc limit 1";
    	$dateFormatNext="where meter_id=".$feedar_id." and date(reading_date)>'".$first."' order by reading_date asc limit 1";
    
    	$query = "SELECT (select actual_reading from site_meter_reading ".$dateFormatLast." ) as last_reading,
    					 (select actual_reading from site_meter_reading ".$dateFormatNext." ) as next_reading from site_meter_reading limit 1";
    	
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}else{
    		return $result[0];
    	}
    }
}



