<?php
class Application_Model_MeterDataMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_MeterData();
    }

    public function addNewSiteMaster(Application_Model_MeterData $meter_master)
    {
    	$data = array(
			'site_id' => $meter_master->__get("site_id"), 
        	'meter_id' =>$meter_master->__get("meter_id"),
			'ins_vol' => $meter_master->__get("ins_vol"),
			'ins_cur' => $meter_master->__get("ins_cur"),
        	'pow_fac' => $meter_master->__get("pow_fac"),
			'ins_freq' =>$meter_master->__get("ins_freq"),
			'act_eng' => $meter_master->__get("act_eng"),
        	'app_eng' => $meter_master->__get("app_eng"),
        );
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
            return false;
        }
        else
        {
            return $result;
        }
    }
  
}

