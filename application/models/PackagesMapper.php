<?php
class Application_Model_PackagesMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Packages();
			$this->charges =  new Application_Model_DbTable_Charges();
		 
    }
 
     public function addNewPackage(Application_Model_Packages $package)
    {
    	 $data=array(
       			"package_name"=>$package->__get("package_name"),
       			"package_cost"=>$package->__get("package_cost"),
       			"package_details"=>$package->__get("package_details"),
       			"is_postpaid"=>$package->__get("is_postpaid"),
       			"wattage"=>$package->__get("wattage"),
       			"unit"=>$package->__get("unit"),
       			"connection_type"=>$package->__get("connection_type"),
       			"extra_charges"=>$package->__get("extra_charges"),
       			"status"=>$package->__get("status"),
       			"state_id"=>$package->__get("state_id"),
       			"site_id"=>$package->__get("site_id"),
       			"feeder_id"=>$package->__get("feeder_id"),
       			"created_date"=>$package->__get("created_date"),
       			"user_id"=>$package->__get("user_id"),
    	 		"light_load"=>$package->__get("light_load"),
				"security_deposit"=>$package->__get("security_deposit"),
       			
       		);
       	/* if ($unit=="" || $unit== null)
       	{
       		 $unit= null;
       	}
       	$query ="call create_package(null,$package_cost, '$package_details', $is_postpaid, $watt_id, $unit, $extra_charges, $state_id, $site_id, $feeder_id, $consumer_type,null,'null',$user_id)";
        */  
       	
    	$result = $this->_db_table->insert($data);
        if(count($result)==0) {
            return false;
        }
        else {
            return $result;
        }
    }
	
    public function getPackageById($package_id)
    {
        $result = $this->_db_table->find($package_id);
        if( count($result) == 0 ) {
                $package = new Application_Model_Packages();
                $package->__set("package_name", "--DELETED--");
                return $package;
        } 
        $row = $result->current();
        $package = new Application_Model_Packages($row);
        return $package;
    }
	public function getAllPackages($status=null,$role_site_id=NULL)
    {
		if($status!=null){
    		$where="status !='new'";
    	}else{
    		$where = "status ='enable'";
    	}
		 
        $result = $this->_db_table->fetchAll($where,array('package_name ASC')); 
        if( count($result) == 0 ) {
                return false;
        }
        $package_object_arr = array();
        foreach ($result as $row)
        {
                $package_object = new Application_Model_Packages($row);
                array_push($package_object_arr,$package_object);
        } 
        return $package_object_arr;
    }
    
	public function updatePackage(Application_Model_Packages $package,$user_id=null)
    {
    	 $data=array(
       			"package_name"=>$package->__get("package_name"),
       			"package_cost"=>$package->__get("package_cost"),
       			"package_details"=>$package->__get("package_details"),
       			"is_postpaid"=>$package->__get("is_postpaid"),
       			"wattage"=>$package->__get("wattage"),
       			"unit"=>$package->__get("unit"),
       			"connection_type"=>$package->__get("connection_type"),
       			"extra_charges"=>$package->__get("extra_charges"),
       			"status"=>$package->__get("status"),
       			"state_id"=>$package->__get("state_id"),
       			"site_id"=>$package->__get("site_id"),
       			"feeder_id"=>$package->__get("feeder_id"),
       			"light_load"=>$package->__get("light_load"),
				"security_deposit"=>$package->__get("security_deposit"),
       			
       		);
        
      
       // $query ="call create_package(null,$package_cost, '$package_details', $is_postpaid, $watt_id, $unit, $extra_charges, $state_id, $site_id, $feeder_id, $consumer_type,'$status',$package_id,$user_id)";
         
    	$where = "package_id = " . $package->__get("package_id");
    	 
    	$result = $this->_db_table->update($data,$where);
    	if($result==0) {
    		return false;
    	}
    	else {
    		return true;
    	}
    }
	
    public function deletePackageById($package_id)
    {
       /*  $where = "package_id = " . $package_id;
        $result = $this->_db_table->delete($where); */
    	$data = array(
    			'status' => 'disable',
    	);
    	$where = "package_id = " . $package_id;
    	 
    	$result = $this->_db_table->update($data,$where);
        if($result==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function getPackagesByPackageName($package_name,$package_id=NULL)
    {
		$where="";
		if($package_id!=NULL){
			$where=" and package_id!=".$package_id;
		}
	 
        $query = "SELECT * FROM packages WHERE  package_name ='".$package_name."' ".$where ; 
        $stmt = $this->_db_table->getAdapter()->query($query);
        $result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$package_object_arr = array();
    	foreach ($result as $row)
    	{
    		$package_object = new Application_Model_Packages($row);
    		$package_object->__set("package_id", $row["package_id"]);
    		$package_object->__set("package_name", $row["package_name"]);
    		$package_object->__set("package_cost", $row["package_cost"]);
    		array_push($package_object_arr,$package_object);
    	}
    	return $package_object_arr;
    }
	 public function getAllEnableDisablePackages($status=null)
    {
    	if($status!=null){
    		$where=null;
    	}else{
    		$where = "status !='disable'";
    	}
        $result = $this->_db_table->fetchAll($where,array('package_id DESC'));
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    
    	$package_object_arr = array();
    	foreach ($result as $row)
    	{
    		$package_object = new Application_Model_Packages($row);
    		$package_object->__set("package_id", $row["package_id"]);
    		$package_object->__set("package_name", $row["package_name"]);
    		$package_object->__set("package_cost", $row["package_cost"]);
    		$package_object->__set("package_details", $row["package_details"]);
    		$package_object->__set("is_postpaid", $row["is_postpaid"]);
    		$package_object->__set("wattage", $row["wattage"]);
    		$package_object->__set("unit", $row["unit"]);
    		$package_object->__set("extra_charges", $row["extra_charges"]);
    		$package_object->__set("status", $row["status"]);
    		$package_object->__set("site_id", $row["site_id"]);
    		$package_object->__set("feeder_id", $row["feeder_id"]);
    		$package_object->__set("connection_type", $row["connection_type"]);
    		
    		array_push($package_object_arr,$package_object);
    	}
    	return $package_object_arr;
    }
    
  
    public function getMultiPackagesByPackageID($package_id=NULL)
    {
    	$query = "SELECT * FROM packages WHERE package_name in ('".implode(",", $package_id)."')";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$package_object_arr = array();
    	foreach ($result as $row)
    	{
    		$package_object = new Application_Model_Packages($row);
    		$package_object->__set("package_id", $row["package_id"]);
    		$package_object->__set("package_name", $row["package_name"]);
    		$package_object->__set("package_cost", $row["package_cost"]);
    		array_push($package_object_arr,$package_object);
    	}
    	return $package_object_arr;
    }
  
    public function getPackageByPackageConsumerId($package_id=NULL,$consumer_id=NULL)
    {
    	$where=null;
    	if($package_id!=null){
    		$where=" p.package_id in (".$package_id.")";
    	}elseif($consumer_id!=null){
    		$where=" cp.consumer_id in (".$consumer_id.")";
    	}
    	
    	if($where==null){
    		return false;
    	}
    	$query = "SELECT * FROM packages p inner join consumer_packages cp on p.package_id=cp.package_id where ".$where;
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	
    	return $result;
    }
	
	 public function getAllCharges()
    {
    	/* mysql_query ("set character_set_client='utf8'");
    	mysql_query ("set character_set_results='utf8'");
    	mysql_query ("set collation_connection='utf8_general_ci'");*/
    	 
    	$query = "SELECT * FROM charges ";
    	
    	$stmt =   $this->charges->getAdapter()->query($query);
    	
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $result;
    }
	  public function getChargesByparameter($parameter=NULL)
    {
    	$query = "SELECT * FROM charges where charge_code='".$parameter."'";
    	$stmt =   $this->charges->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $result[0];
    }
	
	public function getPackagesByFilter($site_id=NULL,$postpaid=NULL,$status=NULL)
    {
    	$where="";
    	if($site_id!=NULL){
    		$where=" where site_id=".$site_id." and status !='disable'";
    	}elseif ($postpaid!=NULL){
    		$where=" where is_postpaid=".$postpaid." and status !='disable'";
    	}elseif ($status!=NULL){
    		$where=" where status='".$status."' and status !='disable'";
    	}else{
			$where = "status !='disable'";
		} 
    	
    	$query = "SELECT * FROM packages ".$where;
    	$stmt =   $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
        
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $result;
    
    }
	
	 
	public function getPackagesByDate($state_id=NULL,$site_id=NULL,$package_type=NULL,$package_id=NULL,$status=NULL,$wattage=NULL,$currDate=NULL,$lastDate=NULL)
    {
    	$where="";
    	if($package_id!=NULL){
    		$where="where package_id in (".implode(",", $package_id).")";
    	}elseif($package_type!=NULL && $site_id!=NULL){
    		$where="where is_postpaid in (".implode(",", $package_type).") and site_id in (".implode(",", $site_id).")";
    	}elseif($package_type!=NULL){
    		$where="where is_postpaid in (".implode(",", $package_type).")";
    	}elseif($site_id!=NULL){
    		$where="where site_id in (".implode(",", $site_id).")";
    	}elseif($state_id!=NULL){
    		$where="where state_id in (".implode(",", $state_id).")";
    	}
    	$status_q="";
    	if($status!=NULL){
    		$status_q=" and status in (".implode(",", $status).")";
    	}
    	$wattage_q="";
    	if($wattage!=NULL){
    		$wattage_q=" and wattage in (".implode(",", $wattage).")";
    	}
    	   $query = "SELECT * FROM packages ".$where.$status_q.$wattage_q." and date(created_date)<='".$currDate."' and date(created_date)>='".$lastDate."'";
    	 
    	$stmt =   $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $result;
    
    }
	
	public function getPackagesBySitePackagetype($site_id=NULL,$package_type=NULL)
    {
    	$where="";
    	if($package_type!=NULL && $site_id!=NULL){
    		$where="where is_postpaid in (".implode(",", $package_type).") and site_id in (".implode(",", $site_id).") and status !='disable'";
    	}elseif($package_type!=NULL){
    		$where="where is_postpaid in (".implode(",", $package_type).") and status !='disable'";
    	}elseif($site_id!=NULL){
    		$where="where site_id in (".implode(",", $site_id).") and status !='disable'";
    	}else{
			$where = "status !='disable'";
		} 
    	$query = "SELECT * FROM packages ".$where;
    
    	$stmt =   $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	 
    	
        return $result;
    
    }
	
	  public function updatePackageById($package_id=NULL,$status=NULL,$remark=NULL,$approve_by=NULL){
    	
    	$where="";
    	if($status!="enable"){
    		$where="package_id=".$package_id;
    	}else{
    		$where="package_id in (".$package_id.")";
    	}
    	$date = new Zend_Date();
    	$date->setTimezone("Asia/Calcutta");
    	$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
        $query ="update packages set status='".$status."',remark='".$remark."',updated_date='".$timestamp."',approve_by=".$approve_by."   where ".$where;
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
     
    	if($result)
    	{
    		return true;
    	}	
    }
	
	public function getAllPackagesNew($status=null,$role_site_id=NULL)
    {
		if($status!=null){
    		$where="status ='new' and site_id in (".implode(",", $role_site_id).")";
    	}else{
    		$where = "status ='enable' and site_id in (".implode(",", $role_site_id).")";
    	}
		 
        $result = $this->_db_table->fetchAll($where,array('package_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $package_object_arr = array();
        foreach ($result as $row)
        {
                $package_object = new Application_Model_Packages($row);
                array_push($package_object_arr,$package_object);
        } 
        return $package_object_arr;
    }
	
	public function getPackagesForFilter($site_id=NULL,$package_type=NULL,$feeder_id=NULL)
    {
    	$where = " where status!='disable'"; 
    	if($site_id!=NULL){
    		$where.=" and FIND_IN_SET(".$site_id.", site_id)";
    	}
    	if($package_type!=NULL){
    		$where.=" and is_postpaid=".$package_type;
    	}
    	if($feeder_id!=NULL){
    		$where.=" and FIND_IN_SET(".$feeder_id.", feeder_id)";
    	}
    	 
    	$query = "SELECT * FROM packages ".$where;
		
    	$stmt =   $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    
    	if( count($result) == 0 ) {
    		return false;
    	}
		return $result;
    } 
	
	public function getPackagesByFeederId($feeder_id=NULL)
    {
    	$query = "SELECT * FROM packages where FIND_IN_SET(".$feeder_id.", feeder_id)  and status!='Disable'";  
    	
    	$stmt =   $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	
        return $result;
    }
	
	public function getReasonRemark()
    { 
		$this->reason =  new Application_Model_DbTable_ReasonRemark();
    	$query = "SELECT * FROM reason_remark";
    	
    	$stmt =   $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	
        return $result;
    }
}
