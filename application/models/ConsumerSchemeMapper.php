<?php
class Application_Model_ConsumerSchemeMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_ConsumerScheme();
    }

    public function addNewConsumerScheme(Application_Model_ConsumerScheme $consumer_schemes)
    {
    	/*$date = new Zend_Date();
		//$date = new Zend_Date('2017-10-31 01:01:00', 'yyyy-MM-dd HH:mm:ss');
		$date->setTimezone("Asia/Kolkata");
	    $curr_date=$date->toString("yyyy-MM-dd HH:mm:ss");*/
		
		$data = array(
				'consumer_scheme_id' => $consumer_schemes->__get("consumer_scheme_id"),
				'consumer_id' => $consumer_schemes->__get("consumer_id"),
   			    'consumer_scheme' => $consumer_schemes->__get("consumer_scheme"),
        		'cluster_id' => $consumer_schemes->__get("cluster_id"),
        		'site_id' => $consumer_schemes->__get("site_id"),
        		'package_continue' => $consumer_schemes->__get("package_continue"),
        		'scheme_continue' => $consumer_schemes->__get("scheme_continue"),
        		'total_month' => $consumer_schemes->__get("total_month"),
				'equip_month_dis' => $consumer_schemes->__get("equip_month_dis"),
				'assign_date' => $consumer_schemes->__get("assign_date"),
				'timestamp' => $consumer_schemes->__get("assign_date"), 
			);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else{
            return $result;
        }
    }
    
    public function getAllConsumerScheme()
    {
    	$roleSession = new Zend_Session_Namespace('roles');
    	$role_sites_id=$roleSession->site_id;
    	
    	if($role_sites_id!=NULL){
    		$where= " and sites.site_id in (".implode(",", $role_sites_id).") ";
    	}else{
    		$where=" ";
    	}
    	$query="select consumer_scheme.consumer_scheme_id  ,consumer_scheme.consumer_scheme,consumer_scheme.consumer_id,
   		        consumer_scheme.cluster_id,consumer_scheme.site_id,consumer_scheme.package_continue,consumer_scheme.scheme_continue from consumer_scheme inner join sites on sites.site_id = consumer_scheme.site_id where 
   			    sites.site_status!='Disable' ".$where." order by cluster_id Asc,site_id ASC,consumer_scheme ASC;";
        $stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    	//  $result = $this->_db_table->fetchAll($where,array('consumer_id ASC'));
	   	if( count($result) == 0 ) {
		      return false;
	    }
       
        $consumer_scheme_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_scheme_object = new Application_Model_ConsumerScheme($row);
                $consumer_scheme_object->__set("consumer_scheme_id", $row["consumer_scheme_id"]);
                $consumer_scheme_object->__set("consumer_id", $row["consumer_id"]);
                $consumer_scheme_object->__set("consumer_scheme", $row["consumer_scheme"]);
                $consumer_scheme_object->__set("cluster_id", $row["cluster_id"]);
                $consumer_scheme_object->__set("site_id", $row["site_id"]);
                $consumer_scheme_object->__set("package_continue", $row["package_continue"]);
                $consumer_scheme_object->__set("scheme_continue", $row["scheme_continue"]);
                array_push($consumer_scheme_object_arr,$consumer_scheme_object);
        }
        return $consumer_scheme_object_arr;
    }
    
    public function checkSchemeByConsumerID($consumer_id=null,$site=null,$scheme_id=null)
    {
    	 
     	  $query="select * from consumer_scheme where consumer_id=".$consumer_id." and site_id=".$site." and consumer_scheme=".$scheme_id;
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	 
    	$consumer_scheme_object_arr = array();
    	foreach ($result as $row)
    	{
    		$consumer_scheme_object = new Application_Model_ConsumerScheme($row);
    		$consumer_scheme_object->__set("consumer_scheme_id", $row["consumer_scheme_id"]);
    		$consumer_scheme_object->__set("consumer_id", $row["consumer_id"]);
    		$consumer_scheme_object->__set("consumer_scheme", $row["consumer_scheme"]);
    		$consumer_scheme_object->__set("cluster_id", $row["cluster_id"]);
    		$consumer_scheme_object->__set("site_id", $row["site_id"]);
    		$consumer_scheme_object->__set("package_continue", $row["package_continue"]);
    		$consumer_scheme_object->__set("scheme_continue", $row["scheme_continue"]);
    		array_push($consumer_scheme_object_arr,$consumer_scheme_object);
    	}
    	return $consumer_scheme_object_arr;
    }
    
    public function updateConsumerScheme(Application_Model_ConsumerScheme $consumer_schemes)
    {
         $data = array(
 				'consumer_id' => $consumer_schemes->__get("consumer_id"),
        		'consumer_scheme' => $consumer_schemes->__get("consumer_scheme"),
         		'cluster_id' => $consumer_schemes->__get("cluster_id"),
        		'site_id' => $consumer_schemes->__get("site_id"),
         		'package_continue' => $consumer_schemes->__get("package_continue"),
         		'scheme_continue' => $consumer_schemes->__get("scheme_continue"),
         		'total_month' => $consumer_schemes->__get("total_month"),
				'equip_month_dis' => $consumer_schemes->__get("equip_month_dis"),
		
			);
		
       $where = "consumer_scheme_id = " . $consumer_schemes->__get("consumer_scheme_id");
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
    public function deleteConsumerSchemeById($consumer_scheme_id)
    {
        $where = "consumer_scheme_id = " . $consumer_scheme_id;
        $result = $this->_db_table->delete($where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
    public function deleteConsumerSchemeBySchemeId($scheme_id)
    {
    	$where = "consumer_scheme = " . $scheme_id;
    	$result = $this->_db_table->delete($where);
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;

    	}
    }
    
    public function getConsumerschemeByConsumerId($consumer_id)
    {
    	$result=NULL;
    	 try{ 
    	 
    	$stmt = $this->_db_table->getAdapter();
    	$query = "SELECT consumer_scheme_id,consumer_id,consumer_scheme,cluster_id,site_id,scheme_continue,package_continue from consumer_scheme where consumer_id=" .$consumer_id;
    	$result = $stmt->fetchRow($query);
    	
    	
    	if (!$result) {
    		return false;
    	}
    	 }
    	catch(Exception $e){
    		$e->getMessage();

    	} 
    	
    		$consumer_scheme_object = new Application_Model_ConsumerScheme($result);
    		$consumer_scheme_object->__set("consumer_scheme_id", $result["consumer_scheme_id"]);
    		$consumer_scheme_object->__set("consumer_id", $result["consumer_id"]);
    		$consumer_scheme_object->__set("consumer_scheme", $result["consumer_scheme"]);
    		$consumer_scheme_object->__set("cluster_id", $result["cluster_id"]);
    		$consumer_scheme_object->__set("site_id", $result["site_id"]);
    		$consumer_scheme_object->__set("package_continue", $result["package_continue"]);
    		$consumer_scheme_object->__set("scheme_continue", $result["scheme_continue"]);
    	return $consumer_scheme_object;
    }
    
    public function updateConsumerSchemeByScheme($consumer_scheme,$consumer_scheme_id)
    {
    	$data = array(
    		 	'consumer_scheme' => $consumer_scheme,
    			);
    	$where = "consumer_scheme_id = " . $consumer_scheme_id;
    	$result = $this->_db_table->update($data,$where);
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
    
    public function deleteschemeByConsumerId($consumer_id=null,$scheme_id=null)
    {
    	 
    
    	$query ="delete from consumer_scheme where consumer_scheme=".$scheme_id." and consumer_id not in (".implode(',', $consumer_id).")";
    
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result=$stmt->execute();
    
    	if($result)
    	{
    		return true;
    	}else{
    		return false;
    	}
    }

    public function getSchemeByConsumerID($scheme_id=null)
    {
    
        $query="select * from consumer_scheme cs inner join consumers c on c.consumer_id=cs.consumer_id where cs.consumer_scheme=".$scheme_id." and equip_month_dis!='-1'";   
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    
    	$consumer_scheme_object_arr = array();
    	foreach ($result as $row)
    	{
    		$consumer_scheme_object = new Application_Model_ConsumerScheme($row);
    		$consumer_scheme_object->__set("consumer_scheme_id", $row["consumer_scheme_id"]);
    		$consumer_scheme_object->__set("consumer_id", $row["consumer_id"]);
    		$consumer_scheme_object->__set("consumer_scheme", $row["consumer_scheme"]);
    		$consumer_scheme_object->__set("cluster_id", $row["cluster_id"]);
    		$consumer_scheme_object->__set("site_id", $row["site_id"]);
    		$consumer_scheme_object->__set("package_continue", $row["package_continue"]);
    		$consumer_scheme_object->__set("scheme_continue", $row["scheme_continue"]);
    		$consumer_scheme_object->__set("total_month", $row["total_month"]);
			$consumer_scheme_object->__set("consumer_connection_id", $row["consumer_connection_id"]);
			$consumer_scheme_object->__set("equip_month_dis", $row["equip_month_dis"]);
    		array_push($consumer_scheme_object_arr,$consumer_scheme_object);
    	}
    	return $consumer_scheme_object_arr;
    }
    
    public function updateConsumerTotalMonth($total_month,$consumer_scheme_id,$column)
    {
    	$data = array(
    			"$column" => $total_month,
    	);
		 
    	$where = "consumer_scheme_id = " . $consumer_scheme_id;
    	$result = $this->_db_table->update($data,$where);
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
	
	
	 public function checkSchemePackageByConsumerID($consumer_id=null)
    {
    
    	$query="select *,s.package_id as package_value from consumer_scheme cs inner join consumers c on c.consumer_id=cs.consumer_id inner join schemes s on 
    			s.scheme_id=cs.consumer_scheme where s.scheme_status='enable' and s.scheme_type='equipment' and cs.equip_month_dis>0 and cs.consumer_id=".$consumer_id;
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if( count($result) == 0 ) {
    		return false;
    	}
      	return $result;
    }
	
	public function getschemePackageByConsumerID($consumer_id=null)
    {
     
    	  $query="SELECT s.scheme,p.package_name,p.feeder_hours,w.wattage,s.start_date,s.discount_month FROM consumer_scheme cs
					inner join schemes s on s.scheme_id=cs.consumer_scheme
					inner join packages p on p.package_id=s.package_id
					inner join wattage w on w.watt_id=p.wattage
					where cs.consumer_id=".$consumer_id." and s.scheme_type='equipment'";
			 	
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if( count($result) == 0 ) {
    		return false;
    	}
      	return $result;
    }
	 public function getConsumerSchemeByConsumerData($month=NULL,$year=NULL,$consumer_id=NULL)
    {
    	  $query="select * from consumer_scheme cs inner join schemes s on s.scheme_id=cs.consumer_scheme 
    			where  cs.consumer_id=".$consumer_id." and s.scheme_type='equipment'";
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	
    	return $result[0];
    }
	
	public function getConsumerSchemeOfZero($continue=NULL)
    {
    	$query="select cs.*,s.package_id from consumer_scheme cs inner join schemes s on s.scheme_id=cs.consumer_scheme
    			where equip_month_dis=0 and cs.package_continue='".$continue."' and s.scheme_type='equipment' and cs.site_id!=1";
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	
    	return $result;
    }
	 public function getAllConsumerSchemeSiteWise($site_id=NULL,$limit=NULL,$from=NULL,$to=NULL,$status=NULL,$scheme_id=NULL)
    {
    	if($site_id!=NULL){ 
    		$where=" s.site_id in (".implode(",", $site_id).") and s.site_status!='Disable'";
    	}else{
    		$where=" s.site_status!='Disable' ";
    	}
		if($scheme_id!=NULL){ 
    		$where.=" and cs.consumer_scheme=".$scheme_id;
    	} 
		
		$lim="";
		if($limit!=NULL){
    		$lim= " limit ".$limit.",50";
    	}else{
			$lim= " limit 0,50";
		}
		$date="";
		if($from!=NULL && $to!=NULL ){
    		$date= " and ((sc.start_date <= '".$from."' and sc.end_date >= '".$from."') or (sc.start_date <= '".$to."' and sc.end_date >= '".$to."'))";
    	}
		
		$stat="";
		if($status!=NULL){
			if($status==0){
				$stat= " and sc.scheme_status ='disable'";
			}elseif($status==1){
				$stat= " and sc.scheme_status ='enable'";
			}
    	} 
		
		     $query="select cs.consumer_scheme_id ,cs.consumer_scheme,cs.consumer_id,s.site_name,
   		        cs.cluster_id,cs.site_id,cs.package_continue,cs.scheme_continue,cs.timestamp from consumer_scheme cs
    			inner join sites s on s.site_id = cs.site_id inner join schemes sc on sc.scheme_id = cs.consumer_scheme where
   			    ".$where.$date.$stat.$lim;  
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
    	
    	if( count($result) == 0 ) {
    		return false;
    	}
    	 
    	$consumer_scheme_object_arr = array();
    	foreach ($result as $row)
    	{
    		$consumer_scheme_object = new Application_Model_ConsumerScheme($row);
    		$consumer_scheme_object->__set("consumer_scheme_id", $row["consumer_scheme_id"]);
    		$consumer_scheme_object->__set("consumer_id", $row["consumer_id"]);
    		$consumer_scheme_object->__set("consumer_scheme", $row["consumer_scheme"]);
    		$consumer_scheme_object->__set("cluster_id", $row["cluster_id"]);
    		$consumer_scheme_object->__set("site_id", $row["site_id"]);
			$consumer_scheme_object->__set("site_name", $row["site_name"]);
    		$consumer_scheme_object->__set("package_continue", $row["package_continue"]);
    		$consumer_scheme_object->__set("scheme_continue", $row["scheme_continue"]);
			$consumer_scheme_object->__set("timestamp", $row["timestamp"]);
    		array_push($consumer_scheme_object_arr,$consumer_scheme_object);
    	}
    	return $consumer_scheme_object_arr;
    }
	
	public function getConsumerSchemeByDateRange($site_id=NULL,$state_id=NULL,$role_site_id=NULL,$current_date=NULL,$last_date=NULL)
    {
		 $join="inner join consumers c on c.consumer_id=cs.consumer_id
				inner join sites s on s.site_id=c.site_id
				inner join schemes sm on sm.scheme_id=cs.consumer_scheme";
						
		if($site_id!=NULL){
			$where=" where s.site_id in (".implode(",",$site_id).")";
		}elseif($state_id!=NULL){
			$where=" where s.state_id in (".implode(",",$state_id).") and  s.site_id in (".implode(",",$role_site_id).")";
		}else{
			$where=" where s.site_id in (".implode(",",$role_site_id).")";
		}
		
    	$query="select *,c.consumer_id as consumer_vaue from consumer_scheme cs ".$join.$where." and s.site_status!='Disable' and 
				date(timestamp)>='".$last_date."' and date(timestamp)<='".$current_date."' group by cs.consumer_id"; 
		
    	$stmt= $this->_db_table->getAdapter();
    	$result = $stmt->fetchAll($query);
		
    	if( count($result) == 0 ) {
    		return false;
    	}
    	
    	return $result;
    }
	
	public function getSchemesByConsumerID($consumer_id=NULL,$current_date=NULL,$last_date=NULL)
    {
		 $join="inner join consumers c on c.consumer_id=cs.consumer_id
				inner join sites s on s.site_id=c.site_id
				inner join schemes sm on sm.scheme_id=cs.consumer_scheme";
						
    	 $query="select sm.scheme,sm.package_id,cs.timestamp from consumer_scheme cs ".$join.$where." and s.site_status!='Disable' and 
				date(timestamp)>='".$last_date."' and date(timestamp)<='".$current_date."' and cs.consumer_id=".$consumer_id; 
		
    	$stmt= $this->_db_table->getAdapter();  
    	$result = $stmt->fetchAll($query);
		
    	if( count($result) == 0 ) {
    		return false;
    	}
    	
    	return $result;
    }
	
	public function getConsumerSchemeDetailByConsumerId($consumer_id)
    {
    	 
    	$stmt = $this->_db_table->getAdapter();
    	$query = "SELECT * from consumer_scheme where consumer_id=" .$consumer_id;
    	$stmt= $this->_db_table->getAdapter();  
    	$result = $stmt->fetchAll($query);
    	 
    	if( count($result) == 0 ) {
    		return false;
    	}
    	
		return $result;
    }
}
