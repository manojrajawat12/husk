<?php

class Application_Model_Sites
{
 
    private $site_id;
    private $site_name;
    private $cluster_id; 
    private $num_of_poles;
    private $site_code;
    private $state_id;
    private $site_status;
    private $site_order;
    private $site_timestamp;
    private $x;
    private $y; 
    private $size_of_plant;
    private $Solar_panel_size;
    private $Solar_panel_no;
	private $Solar_panel_kw;
    private $Charges_controller_size; 
    private $Charges_controller_no;
    private $Inverter_size;
    private $Inverter_no;
    private $Battery_size;
    private $Battery_no;
	private $Battery_no_v;
	private $otp_act_id;
    private $otp_mtr_id;
	private $approve_date;
    private $approve_by;
    private $remark;
	private $opening_bal;
	private $dg_capacity;
	private $dg_no;
	private $bio_capacity;
	private $bio_no;
	private $radiation;
	private $site_target;
	private $plant_duration;
	private $plant_target;
	private $array_no;
	private $array_capacity;
	private $day_factor;
	private $night_factor;
	private $diesel_factor;
	private $spark; 
	private $spark_url;
		
    public function __construct($site_row = null)
    { 
        if( !is_null($site_row) && $site_row instanceof Zend_Db_Table_Row ) {
				$this->site_id = $site_row->site_id;
                $this->site_name = $site_row->site_name;
                $this->cluster_id = $site_row->cluster_id;
                $this->num_of_poles = $site_row->num_of_poles;
                $this->site_code = $site_row->site_code;
                $this->state_id = $site_row->state_id;
                $this->site_status = $site_row->site_status;
                $this->site_order = $site_row->site_order;
                $this->site_timestamp = $site_row->site_timestamp;
                $this->x = $site_row->x;
                $this->y = $site_row->y;
                $this->size_of_plant = $site_row->size_of_plant;
                $this->Solar_panel_size = $site_row->Solar_panel_size;
                $this->Solar_panel_no = $site_row->Solar_panel_no;
				$this->Solar_panel_kw = $site_row->Solar_panel_kw;
                $this->Charges_controller_size = $site_row->Charges_controller_size;
                $this->Charges_controller_no = $site_row->Charges_controller_no;
                $this->Inverter_size = $site_row->Inverter_size;
                $this->Inverter_no = $site_row->Inverter_no;
                $this->Battery_size = $site_row->Battery_size;
                $this->Battery_no = $site_row->Battery_no;
				$this->Battery_no_v = $site_row->Battery_no_v;
				$this->otp_act_id = $site_row->otp_act_id;
                $this->otp_mtr_id = $site_row->otp_mtr_id;
				$this->approve_date = $site_row->approve_date;
                $this->approve_by = $site_row->approve_by;
                $this->remark = $site_row->remark;
				$this->opening_bal = $site_row->opening_bal; 
				$this->dg_capacity = $site_row->dg_capacity;
				$this->dg_no = $site_row->dg_no;
				$this->bio_capacity = $site_row->bio_capacity;
				$this->bio_no = $site_row->bio_no;
				$this->radiation = $site_row->radiation;
				$this->site_target = $site_row->site_target; 
				$this->plant_duration = $site_row->plant_duration;
				$this->plant_target = $site_row->plant_target;
				$this->array_no = $site_row->array_no;
				$this->array_capacity = $site_row->array_capacity;
				$this->day_factor = $site_row->day_factor;
				$this->night_factor = $site_row->night_factor; 
				$this->diesel_factor = $site_row->diesel_factor; 
				$this->spark = $site_row->spark; 
				$this->spark_url = $site_row->spark_url; 
        }
    }
    public function __set($name, $value)
    {
            switch($name)
            {
                    case "sites_id":
                            //throw new Exception('Cannot update Show\'s ID'');
                            break;
                    default:
                            $this->$name = $value;
                    break;
            }

    }
    public function __get($name)
    {
            return $this->$name;
    }

}

