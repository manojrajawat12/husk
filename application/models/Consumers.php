<?php

class Application_Model_Consumers
{
    private $consumer_id;
    private $package_id;
    private $site_id;
    private $consumer_code;
    private $consumer_name;
    private $consumer_father_name;
    private $consumer_status;
    private $consumer_act_date;
    private $consumer_act_charge;
    private $consumer_connection_id;
    private $is_micro_enterprise;
    private $micro_entrprise_price;
    private $wattage;
    private $meter_start_reading;
    private $suspension_date; 
    private $sms_opt_in;
    private $enterprise_type;
    private $type_of_me ;
	private $sub_type_of_me;
    private $notes ;
	private $site_meter_id;
	private $credit_limit;
	private $wallet_bal;
	private $flag_date;
	private $sms_flag;
	private $remark;
    private $consumer_first_status;
    private $scheme_apply;
	private $gender;
	private $notional_bal;
	
	private $created_by;
	private $create_type;
	private $approve_by;
	private $approve_date;
	private $reason_remark;
	private $mobile_no;
	private $email_id; 
	private $type;
	private $temp_dis;  
	private $dob;
	
    public function __construct($consumer_row = null)
    {
        if( !is_null($consumer_row) && $consumer_row instanceof Zend_Db_Table_Row ) {
                $this->consumer_id = $consumer_row->consumer_id;
                $this->package_id = $consumer_row->package_id;
                $this->site_id = $consumer_row->site_id;
                $this->consumer_code = $consumer_row->consumer_code;
                $this->consumer_name = $consumer_row->consumer_name;
                $this->consumer_father_name = $consumer_row->consumer_father_name;
                $this->consumer_status = $consumer_row->consumer_status;
                $this->consumer_act_date = $consumer_row->consumer_act_date;
                $this->consumer_act_charge = $consumer_row->consumer_act_charge;
                $this->is_micro_enterprise = $consumer_row->is_micro_enterprise;
                $this->micro_entrprise_price = $consumer_row->micro_entrprise_price;
                $this->wattage = $consumer_row->wattage;
                $this->meter_start_reading = $consumer_row->meter_start_reading;
                $this->consumer_connection_id = $consumer_row->consumer_connection_id;
                $this->suspension_date = $consumer_row->suspension_date;
                $this->sms_opt_in = $consumer_row->sms_opt_in;
                $this->enterprise_type=$consumer_row->enterprise_type;
                $this->type_of_me=$consumer_row->type_of_me;
				$this->sub_type_of_me=$consumer_row->sub_type_of_me;
                $this->notes=$consumer_row->notes;
				$this->site_meter_id=$consumer_row->site_meter_id;
				$this->credit_limit=$consumer_row->credit_limit;
				$this->wallet_bal=$consumer_row->wallet_bal;
				$this->flag_date=$consumer_row->flag_date;
				$this->sms_flag=$consumer_row->sms_flag;
				$this->remark=$consumer_row->remark;
  				$this->consumer_first_status=$consumer_row->consumer_first_status;
  				$this->scheme_apply=$consumer_row->scheme_apply;
				$this->gender=$consumer_row->gender;
				$this->notional_bal=$consumer_row->notional_bal;
				 
				$this->created_by=$consumer_row->created_by;
				$this->create_type=$consumer_row->create_type;
				$this->approve_by=$consumer_row->approve_by;
				$this->approve_date=$consumer_row->approve_date;
				$this->reason_remark=$consumer_row->reason_remark;
				$this->mobile_no=$consumer_row->mobile_no;
				$this->email_id=$consumer_row->email_id;
				$this->type=$consumer_row->type;
				$this->temp_dis=$consumer_row->temp_dis;
				$this->dob=$consumer_row->dob;
		}
    }
    
    public function __set($name, $value)
    {
           
         $this->$name = $value;
            
    }
    public function __get($name)
    {
            return $this->$name;
    }

}

