<?php
class Application_Model_SettingsMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Settings();
    }

    public function addNewSetting(Application_Model_Settings $setting)
    {
        $data = array(
	'setting_name' => $setting->__get("setting_name"),
	'setting_value' => $setting->__get("setting_value"),
	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getSettingById($setting_id)
    {
        $result = $this->_db_table->find($setting_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $setting = new Application_Model_Settings($row);
        return $setting;
    }
    public function getAllSettings()
    {
        $result = $this->_db_table->fetchAll(null,array('setting_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $setting_object_arr = array();
        foreach ($result as $row)
        {
                $setting_object = new Application_Model_Settings($row);
                array_push($setting_object_arr,$setting_object);
        }
        return $setting_object_arr;
    }
    public function updateSetting(Application_Model_Settings $setting)
    {
        $data = array(
	'setting_name' => $setting->__get("setting_name"),
	'setting_value' => $setting->__get("setting_value"),
	);
        $where = "setting_id = " . $setting->__get("setting_id");
        $result = $this->_db_table->update($data,$where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteSettingById($setting_id)
    {
        $where = "setting_id = " . $setting_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
    public function getSettingByName($setting_name)
    {   
        $row = $this->_db_table->fetchRow($this->_db_table->select()->where('setting_name = ?',$setting_name));
        $setting = new Application_Model_Settings($row);
        return $setting;
    }
    public function getBackupEmails()
    {
        $where = array(
            "setting_name = 'backup_email' " 
        );
        $result = $this->_db_table->fetchAll($where,array('setting_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $setting_object_arr = array();
        foreach ($result as $row)
        {
                $setting_object = new Application_Model_Settings($row);
                array_push($setting_object_arr,$setting_object);
        }
        return $setting_object_arr;
    }
    
    public function getNotifyEmails()
    {
        $where = array(
            "setting_name = 'NOTIFY_EMAILS' " 
        );
        $result = $this->_db_table->fetchAll($where,array('setting_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $setting_object_arr = array();
        foreach ($result as $row)
        {
                $setting_object = new Application_Model_Settings($row);
                array_push($setting_object_arr,$setting_object);
        }
        return $setting_object_arr;
    }
}
