<?php
class Application_Model_RoleSiteMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_RoleSite();
	}

	public function addNewRoleSite(Application_Model_RoleSite $roleSite)
	{

		$data = array(
				'role_id' => $roleSite->__get("role_id"),
				'site_id' => $roleSite->__get("site_id"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getRoleSiteById($role_id)
	{
		$result = $this->_db_table->find($role_id);
		if( count($result) == 0 ) {
			$role = new Application_Model_RoleSite();
			return $role;
		}
		$row = $result->current();
		$role = new Application_Model_RoleSite($row);
		return $role;
	}
	public function getAllRoleSiteById($role_id)
	{
		  //$query = " SELECT * From role_sites where role_id ='$role_id'";
	    /*$query="SELECT role_site_id,role_sites.site_id,sites.cluster_id,clusters.state_id FROM tara.role_sites
 				  inner join sites on sites.site_id=role_sites.site_id inner join clusters on clusters.cluster_id=sites.cluster_id
  				  where role_id='$role_id' order by sites.site_name";*/
		$query="call getAllRoleSiteById($role_id)";
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	//print_r($result);
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$role_site_object_arr = array();
    	foreach ($result as $row)
    	{
    		$role_site_object = new Application_Model_RoleSite($row);
    		$role_site_object->__set("role_site_id", $row["role_site_id"]);
    		$role_site_object->__set("site_id", $row["site_id"]);
    		$role_site_object->__set("cluster_id", $row["cluster_id"]);
    		$role_site_object->__set("state_id", $row["state_id"]);
    		array_push($role_site_object_arr,$role_site_object);
    	}
    	return $role_site_object_arr;
		
	}
	public function getAllRoleSite()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('role_site_id ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$role_site_object_arr = array();
		foreach ($result as $row)
		{
			$role_site_object = new Application_Model_RoleSite($row);
			array_push($role_site_object_arr,$role_site_object);
		}
		return $role_site_object_arr;
	}
	
	public function updateRoleSite(Application_Model_RoleSite $roleSite)
	{
		$data = array(
				'site_id' => $roleSite->__get("site_id"),
		);
		$where = "role_id = " . $roleSite->__get("role_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	public function deleteRoleSiteById($role_id,$site_id=NULL)
	{
		if($site_id==NULL){
		$where = "role_id = " . $role_id;
		}
		else{
			$where = " site_id not in (".implode(",", $site_id).")  and role_id = ".$role_id;
		}
		
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function checkRoleSiteById($role_id,$site_id)
	{
	$query="SELECT * from role_sites where role_id=".$role_id." and site_id =".$site_id;
    	$stmt = $this->_db_table->getAdapter()->query($query);
    	$result = $stmt->fetchAll();
    	$count=count($result);
    	
    	if( $count == 0 ) {
    		return $count;
    	}
		else
		{
			return $count;
		}
	}
	
}