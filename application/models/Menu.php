<?php

class Application_Model_Menu
{
    private $menu_id;
    private $menu_desc;
    private $parent_menu;
    private $menu_type;

    public function __construct($menu_row = null)
    {
        if( !is_null($menu_row) && $menu_row instanceof Zend_Db_Table_Row ) {
            
                $this->menu_id = $menu_row->menu_id;
                $this->menu_desc = $menu_row->menu_desc;
                $this->parent_menu = $menu_row->parent_menu;
                $this->menu_type = $menu_row->menu_type;
     
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}
