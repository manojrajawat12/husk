<?php

class Application_Model_Schemes
{
    private $scheme_id;  
    private $scheme;  
    private $scheme_type;  
    private $celamed_id;  
    private $down_payment;  
    private $emi_amt; 
    private $emi_duration;  
    private $discount_amt;  
    private $discount_month;  
    private $gen_scheme_type; 
    private $advance_payment;  
    private $advance_dis_month;  
    private $advance_dis_amt;  
    private $flat_dis;  
    private $per_dis;  
    private $state_id;  
    private $site_id;  
    private $package_id;  
    private $consumer_id;
    private $start_date;
    private $end_date;
    private $scheme_status;
    private $enable_date;
    private $scheme_applied;
    private $is_postpaid;
    private $consumer_type;
	private $package_continue;
    private $scheme_continue; 
    
    public function __construct($scheme_row = null)
    {
        if( !is_null($scheme_row) && $scheme_row instanceof Zend_Db_Table_Row ) {
            
                $this->scheme_id = $scheme_row->scheme_id;
                $this->scheme = $scheme_row->scheme;
                $this->scheme_type = $scheme_row->scheme_type;
                $this->celamed_id = $scheme_row->celamed_id;
                $this->down_payment = $scheme_row->down_payment;
                $this->emi_amt = $scheme_row->emi_amt;
                $this->emi_duration = $scheme_row->emi_duration;
                $this->discount_amt = $scheme_row->discount_amt;
                $this->discount_month = $scheme_row->discount_month;
                $this->gen_scheme_type = $scheme_row->gen_scheme_type;
                $this->advance_payment = $scheme_row->advance_payment;
                $this->advance_dis_month = $scheme_row->advance_dis_month;
                $this->advance_dis_amt = $scheme_row->advance_dis_amt;
                $this->flat_dis = $scheme_row->flat_dis;
                $this->per_dis = $scheme_row->per_dis;
                $this->state_id = $scheme_row->state_id;
                $this->site_id = $scheme_row->site_id;
                $this->package_id = $scheme_row->package_id;
                $this->consumer_id = $scheme_row->consumer_id;
                $this->start_date = $scheme_row->start_date;
                $this->end_date = $scheme_row->end_date;
                $this->scheme_status = $scheme_row->scheme_status;
                $this->enable_date = $scheme_row->enable_date;
                $this->scheme_applied = $scheme_row->scheme_applied;
                $this->is_postpaid = $scheme_row->is_postpaid;
                $this->consumer_type = $scheme_row->consumer_type;
				$this->package_continue = $scheme_row->package_continue;
                $this->scheme_continue = $scheme_row->scheme_continue;
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
            return $this->$name;
    }
}

