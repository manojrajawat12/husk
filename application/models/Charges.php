<?php

class Application_Model_Charges
{
    private $charge_id;
    private $charge_name;
    private $charge_parameter;
    private $hindi_text;
    private $charge_code;
  
   
    public function __construct($charges_row = null)
    {
        if( !is_null($charges_row) && $charges_row instanceof Zend_Db_Table_Row ) {
            
            $this->charge_id = $charges_row->charge_id;
            $this->charge_name = $charges_row->charge_name;
            $this->charge_parameter = $charges_row->charge_parameter;
            $this->hindi_text = $charges_row->hindi_text;
            $this->charge_code = $charges_row->charge_code;
            
        }
    }
    public function __set($name, $value)
    {
    	$this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }
}

