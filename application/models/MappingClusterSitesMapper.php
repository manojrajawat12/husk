<?php
class Application_Model_MappingClusterSitesMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_MappingClusterSites();
    }

    public function addNewMappingClusterSite(Application_Model_MappingClusterSites $mappingClusterSite)
    {
        $data = array(
	'cluster_manager_id' => $mappingClusterSite->__get("cluster_manager_id"),
	'site_id' => $mappingClusterSite->__get("site_id"),
	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getMappingClusterSiteById($mapping_cluster_site_id)
    {
        $result = $this->_db_table->find($mapping_cluster_site_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $mappingClusterSite = new Application_Model_MappingClusterSites($row);
        return $mappingClusterSite;
    }
    public function getAllMappingClusterSites()
    {
        $result = $this->_db_table->fetchAll(null,array('mapping_cluster_site_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $mappingClusterSite_object_arr = array();
        foreach ($result as $row)
        {
                $mappingClusterSite_object = new Application_Model_MappingClusterSites($row);
                array_push($mappingClusterSite_object_arr,$mappingClusterSite_object);
        }
        return $mappingClusterSite_object_arr;
    }
    public function updateMappingClusterSite(Application_Model_MappingClusterSites $mappingClusterSite)
    {
        $data = array(
	'cluster_manager_id' => $mappingClusterSite->__get("cluster_manager_id"),
	'site_id' => $mappingClusterSite->__get("site_id"),
	);
        $where = "mapping_cluster_site_id = " . $mappingClusterSite->__get("mapping_cluster_site_id");
        $result = $this->_db_table->update($data,$where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteMappingClusterSiteById($mapping_cluster_site_id)
    {
        
        $where = "mapping_cluster_site_id = " . $mapping_cluster_site_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
}
