<?php
class Application_Model_UsersMapper
{
    protected $_db_table;
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Users();
			$this->_loginDetail_table = new Application_Model_DbTable_LoginHistory();
    }

    public function addNewUser(Application_Model_Users $user)
    {
        $data = array(
	'username'=> $user->__get("username"),
	'hashed_password'=> $user->__get("hashed_password"),
	'user_fname'=> $user->__get("user_fname"),
	'user_lname'=> $user->__get("user_lname"),
	'user_email'=> $user->__get("user_email"),
        'user_role'=> $user->__get("user_role"),
        'profile_image'=> $user->__get("profile_image"),
        'phone'=> $user->__get("phone"),
        'reset_code'=> $user->__get("reset_code"),    
	);
        $result = $this->_db_table->insert($data);
        if($result==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getUserById($user_id)
    {
        $result = $this->_db_table->find($user_id);
        //print_r($result);
        if( count($result) == 0 ) {
                $user = new Application_Model_Users();
                return $user;
        }
        $row = $result->current();
        $user = new Application_Model_Users($row);
        return $user;
    }
    public function getAllUsers()
    {
        $result = $this->_db_table->fetchAll(null,array('user_fname ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $user_object_arr = array();
        foreach ($result as $row)
        {
                $user_object = new Application_Model_Users($row);
                array_push($user_object_arr,$user_object);
        }
        return $user_object_arr;
    }
    public function getAllClusterManagers()
    {
        $where = array(
            "user_role = ?" => "2" 
        );
        $result = $this->_db_table->fetchAll($where,array('user_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $user_object_arr = array();
        foreach ($result as $row)
        {
                $user_object = new Application_Model_Users($row);
                array_push($user_object_arr,$user_object);
        }
        return $user_object_arr;
    }
    public function updateUser(Application_Model_Users $user)
    {
        $data = array(
	'username'=> $user->__get("username"),
	'hashed_password'=> $user->__get("hashed_password"),
	'user_fname'=> $user->__get("user_fname"),
	'user_lname'=> $user->__get("user_lname"),
	'user_email'=> $user->__get("user_email"),
        'profile_image'=> $user->__get("profile_image"),
        'phone'=> $user->__get("phone"),
        'reset_code'=> $user->__get("reset_code"),
        'user_role'=> $user->__get("user_role"),
	);
        $where = "user_id = ". $user->__get("user_id");
    
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteUserById($user_id)
    {
        $where = "user_id = ". $user_id;
        $result = $this->_db_table->delete($where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
    public function getUserByEmail($email)
    {
            $where = array('user_email = ?'=>$email);
            $result = $this->_db_table->fetchRow($where);
            if($result== 0 ) {
                    return false;
            }
            
            $user_object = new Application_Model_Users($result);
            return $user_object;
    }
    
    public function getUserByResetCode($code)
    {
            $where = array('reset_code = ?'=>$code);
            $result = $this->_db_table->fetchRow($where);
            if( count($result) == 0 ) {
                    return false;
            }
            
            $user_object = new Application_Model_Users($result);
            return $user_object;
    }
    
    public function getCashCollected($user_id,$from_date=NULL,$to_date=NULL,$filter=NULL)
    {
        $cashTable = new Application_Model_DbTable_CashRegister();
        $where = array(
            "user_id = ?" => $user_id,
            "cr_entry_type = 'CREDIT' OR cr_entry_type = 'REFUND' OR cr_entry_type = 'ACTIVATION'"
        );
        $result = $cashTable->fetchAll($where,array('user_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $amount = 0;
        foreach ($result as $row)
        {
            if($filter!=NULL){
                $date =new Zend_Date($row->timestamp);
                $from = new Zend_Date($from_date);
                $to = new Zend_Date($to_date);
                
                if($date->isEarlier($to) && $date->isLater($from)){         
                    if($row->cr_entry_type=="CREDIT" or $row->cr_entry_type=="ACTIVATION")
                    {
                        $amount = $amount+$row->cr_amount;
                    }
                    elseif($row->cr_entry_type=="REFUND")
                    {
                        $amount = $amount-$row->cr_amount;
                    }     
                }
                
            }
            else{
                if($row->cr_entry_type=="CREDIT" or $row->cr_entry_type=="ACTIVATION")
                {
                    $amount = $amount+$row->cr_amount;
                }
                elseif($row->cr_entry_type=="REFUND")
                {
                    $amount = $amount-$row->cr_amount;
                }    
            }
            
        }
        return $amount;
    }
    
    public function getCashSent($user_id)
    {
        $cashTable = new Application_Model_DbTable_CashRegister();
        $where = array(
            "user_id = ?" => $user_id,
            "cr_entry_type = 'RECEIVED'"
        );
        $result = $cashTable->fetchAll($where,array('user_id ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $amount = 0;
        foreach ($result as $row)
        {
            $amount = $amount+$row->cr_amount;
        }
        return $amount;
    }
    public function getOutstandingCash($user_id)
    {
        $outstanding = $this->getCashCollected($user_id) - $this->getCashSent($user_id);
        return $outstanding;
    }
    public function checkUsernameAvailability($username)
    {
        $where = array('username = ?'=>$username);
        $result = $this->_db_table->fetchRow($where);
        if( count($result) == 0 ) {
                return true;
        }
        else
        {
            return false;
        }
    }
    public function checkEmailAvailability($email)
    {
        $where = array('user_email = ?'=>$email);
        $result = $this->_db_table->fetchRow($where);
        if( count($result) == 0 ) {
                return true;
        }
        else
        {
            return false;
        }
    }

public function getUserByRoleId($role_id)
    {
    	$where = array(
    			"user_role = ?" => $role_id
    	);
    	$result = $this->_db_table->fetchAll($where,array('user_id DESC'));
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$user_object_arr = array();
    	foreach ($result as $row)
    	{
    		$user_object = new Application_Model_Users($row);
    		array_push($user_object_arr,$user_object);
    	}
    	return $user_object_arr;
    }

public function getAllUsersByCelamedConsumer()
    {
    	 $query="select u.* from users u inner join user_role ur on ur.user_role_id=u.user_role where 
    	 			ur.user_role='Celamed'";
		 
		$stmt= $this->_db_table->getAdapter();
		$result = $stmt->fetchAll($query);
	
		if(count($result) == 0 ) {
			return false;
		}
		$user_object_arr = array();
		foreach ($result as $row)
		{
			$user_object = new Application_Model_Users();
			foreach($row as $key=>$value)
			{
				$user_object->__set($key,$value);
			}
			array_push($user_object_arr,$user_object);
		}
	
		return $user_object_arr;
    }
	 public function updatePassword($new_password, $user_id)
    {
    	$data = array(
    			'hashed_password' => $new_password,
    	);
    	
    	$where = "user_id = " . $user_id;
    
    	$result = $this->_db_table->update($data,$where);
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
	
	 public function userAgent($password,$user_email)
    {
    	$stmt = $this->_db_table->getAdapter();
    	$query = " SELECT * FROM users WHERE hashed_password ='".sha1($password)."' AND (user_email='$user_email' or username='$user_email')";
    	 
    	$result = $stmt->fetchRow($query);
    	$users = $result;
    	if( count($result) == 0 ) {
    		return false;
    	}
    	return $users;
    }
	public function addLogindetails($user_id=NULL,$timestamp=NULL,$status=NULL,$ip_address=NULL,$entry_by=NULL)
    {
    	$data = array(
    			'user_id'=> $user_id,
    			'timestamp'=> $timestamp,
    			'status'=> $status,
    			'ip_address' =>$ip_address,
				'entry_by'=>$entry_by, 
    			 
    	);
    	$result = $this->_loginDetail_table->insert($data);
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return $result;
    	}
    }
 }
