<?php
class Application_Model_StatesMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_States();
		$this->_db_table_dummy = new Application_Model_DbTable_Dummy();
	}

	public function addNewState(Application_Model_States $state)
	{

		$data = array(
				'state_name' => $state->__get("state_name"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getStateById($state_id)
	{
		$result = $this->_db_table->find($state_id);
		if( count($result) == 0 ) {
			$state = new Application_Model_States();
			return $state;
		}
		$row = $result->current();
		$state = new Application_Model_States($row);
		return $state;
	}
	
	public function getAllStates()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('state_id DESC'));
		if( count($result) == 0 ) {
			return false;
		}
		$state_object_arr = array();
		foreach ($result as $row)
		{
			$state_object = new Application_Model_States($row);
			array_push($state_object_arr,$state_object);
		}
		return $state_object_arr;
	}
	
	public function updateState(Application_Model_States $states)
	{
		$data = array(
				'state_name' => $states->__get("state_name"),
		);
		$where = "state_id = " . $states->__get("state_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	public function deleteStateById($state_id)
	{
		$where = "state_id = " . $state_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function getAllDummy()
	{
		 $query="SELECT *,d.type as entry_type FROM dummy d inner join consumers c on c.consumer_connection_id=d.connection_id inner join sites s on s.site_id=c.site_id";
		
		$stmt = $this->_db_table_dummy->getAdapter()->query($query);
		$result = $stmt->fetchAll();
		if( count($result) == 0 ) {
			return false;
		}
		return $result;
	}
}