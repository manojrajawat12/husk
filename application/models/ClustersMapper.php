<?php
class Application_Model_ClustersMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_Clusters();
    }

    public function addNewCluster(Application_Model_Clusters $cluster)
    {
      
        $data = array(
	
		'cluster_name' => $cluster->__get("cluster_name"),
        'cluster_manager_id' => $cluster->__get("cluster_manager_id"),
        'state_id' => $cluster->__get("state_id"),
	
	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
    public function getClusterById($cluster_id)
    {
        $result = $this->_db_table->find($cluster_id);
        if( count($result) == 0 ) {
                return false;
        }
        $row = $result->current();
        $cluster = new Application_Model_Clusters($row);
        return $cluster;
    }
    public function getAllClusters($user=NULL,$allCluster=NULL,$clusters_id=NULL)
    {
    	/* if(($user=="3" ||$user=="5") && $state_id!=NULL){ 
    		$where=" cluster_id in (".implode(",", $clusters_id).") ";
    	}else{ */
    	if($allCluster==true){
    		$where=NULL;
    	}else{
    		$where=" cluster_id in (".implode(",", $clusters_id).") ";
    	}
    	
        $result = $this->_db_table->fetchAll($where,array('cluster_id DESC'));
        if( count($result) == 0 ) {
                return false;
        }
        $cluster_object_arr = array();
        foreach ($result as $row)
        {
                $cluster_object = new Application_Model_Clusters($row);
                array_push($cluster_object_arr,$cluster_object);
        }
        return $cluster_object_arr;
    }
    public function updateCluster(Application_Model_Clusters $cluster)
    {
        $data = array(
	'cluster_id' => $cluster->__get("cluster_id"),
	'cluster_name' => $cluster->__get("cluster_name"),
    'cluster_manager_id' => $cluster->__get("cluster_manager_id"),
    'state_id' => $cluster->__get("state_id"),
	
	);
        $where = "cluster_id = " . $cluster->__get("cluster_id");
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteClusterById($cluster_id)
    {
        $where = "cluster_id = " . $cluster_id;
     
        $result = $this->_db_table->delete($where);
       
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function getClusterByStateId($state_id)
    {
    
    	$where=array(
    	"state_id = ?" => $state_id,
    	);
    
    	$result = $this->_db_table->fetchAll($where,array('cluster_id DESC'));
    
    	if( count($result) == 0 ) {
    		return false;
    	}
    	$cluster_object_arr = array();
    	foreach ($result as $row)
    	{
    		$cluster_object = new Application_Model_Clusters($row);
    		array_push($cluster_object_arr,$cluster_object);
    	}
    	return $cluster_object_arr;
    }
   
}
