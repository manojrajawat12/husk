<?php
class Application_Model_ConsumerSchemeMapper
{
    protected $_db_table;
    
    
    public function __construct()
    {
            $this->_db_table = new Application_Model_DbTable_ConsumerScheme();
    }

    public function addNewConsumerScheme(Application_Model_ConsumerScheme $consumer_schemes)
    {
        $data = array(
				'consumer_scheme_id' => $consumer_schemes->__get("consumer_scheme_id"),
				'consumer_id' => $consumer_schemes->__get("consumer_id"),
   			    'consumer_scheme' => $consumer_schemes->__get("consumer_scheme"),
        		'cluster_id' => $consumer_schemes->__get("cluster_id"),
        		'site_id' => $consumer_schemes->__get("site_id"),
        		
        		
   	);
        $result = $this->_db_table->insert($data);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return $result;
        }
    }
       
    public function getAllConsumerScheme()
    {
        $result = $this->_db_table->fetchAll(null,array('cluster_id Asc','site_id ASC','consumer_scheme ASC'));
        if( count($result) == 0 ) {
                return false;
        }
        $consumer_scheme_object_arr = array();
        foreach ($result as $row)
        {
                $consumer_scheme_object = new Application_Model_ConsumerScheme($row);
                array_push($consumer_scheme_object_arr,$consumer_scheme_object);
        }
        return $consumer_scheme_object_arr;
    }
    public function updateConsumerScheme(Application_Model_ConsumerScheme $consumer_schemes)
    {
         $data = array(
				'consumer_id' => $consumer_schemes->__get("consumer_id"),
        		'consumer_scheme' => $consumer_schemes->__get("consumer_scheme"),
         		'cluster_id' => $consumer_schemes->__get("cluster_id"),
         		'site_id' => $consumer_schemes->__get("site_id"),
	);
        $where = "consumer_scheme_id = " . $consumer_schemes->__get("consumer_scheme_id");
        $result = $this->_db_table->update($data,$where);
        if($result==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    public function deleteConsumerSchemeById($consumer_scheme_id)
    {
        $where = "consumer_scheme_id = " . $consumer_scheme_id;
        $result = $this->_db_table->delete($where);
        if(count($result)==0)
        {
                return false;
        }
        else
        {
            return true;
        }
    }
    
    public function  deleteConsumerSchemeBySchemeId($scheme_id)
    {
    	$where = "consumer_scheme = " . $scheme_id;
    	$result = $this->_db_table->delete($where);
    	if($result==0)
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
    
   public function  getConsumerschemeByConsumerId($consumer_id)
    {
    	$stmt = $this->_db_table->getAdapter();
    	$query = "SELECT consumer_scheme_id,consumer_id,consumer_scheme,cluster_id,site_id from consumer_scheme where consumer_id=" .$consumer_id;
    	$result = $stmt->fetchRow($query);
    	//print_r($result);
    	if( count($result) == 0 ) {
    		return false;
    	}
    		$consumer_scheme_object = new Application_Model_ConsumerScheme($result);
    		$consumer_scheme_object->__set("consumer_scheme_id", $result["consumer_scheme_id"]);
    		$consumer_scheme_object->__set("consumer_id", $result["consumer_id"]);
    		$consumer_scheme_object->__set("consumer_scheme", $result["consumer_scheme"]);
    		$consumer_scheme_object->__set("cluster_id", $result["cluster_id"]);
    		$consumer_scheme_object->__set("site_id", $result["site_id"]);
    	return $consumer_scheme_object;
    }
    
}
