<?php
class Application_Model_SiteMasterMeter
{
	private $id;
	private $site_id;
	private $meter_name;
	private $meter_keyword;
	private $description;
	private $start_time;
	private $end_time;
	private $is_24_hr;
	private $hours;
	private $sim_no;
	private $feeder_type;
	private $timestamp;
	private $approve_date;
	private $approve_by;
	private $remark;
	private $status;
	private $meter_no; 
	private $feeder_no;
	public function __construct($change_row = null)
	{
		if( !is_null($change_row) && $change_row instanceof Zend_Db_Table_Row ) {

			$this->id = $change_row->id;
			$this->site_id = $change_row->site_id;
			$this->meter_name = $change_row->meter_name; 
			$this->meter_keyword = $change_row->meter_keyword;
			$this->description = $change_row->description;
			$this->start_time = $change_row->start_time;
			$this->end_time = $change_row->end_time;
			$this->is_24_hr = $change_row->is_24_hr;
			$this->hours = $change_row->hours;
			$this->sim_no = $change_row->sim_no;
			$this->feeder_type = $change_row->feeder_type;
			$this->timestamp = $change_row->timestamp;
			$this->approve_date = $change_row->approve_date;
			$this->approve_by = $change_row->approve_by;
			$this->remark = $change_row->remark;
			$this->status = $change_row->status;
			$this->meter_no = $change_row->meter_no;
			$this->feeder_no = $change_row->feeder_no;
		}
	}
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	public function __get($name)
	{
		return $this->$name;
	}
}