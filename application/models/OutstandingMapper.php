<?php
class Application_Model_OutstandingMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_Outstanding();
	}

	public function addNewOutstanding(Application_Model_Outstanding $Outstanding)
	{

		$data = array(
				'consumer_id' => $Outstanding->__get("consumer_id"),
				'consumer_name' => $Outstanding->__get("consumer_name"),
				'consumer_connection_id' => $Outstanding->__get("consumer_connection_id"),
				'outstanding' => $Outstanding->__get("outstanding"),
				'site_id' => $Outstanding->__get("site_id"),
		);
		
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	
	public function getDataByConsumerId($consumer_id=NULL)
	{ 
			$query=" Select * from outstanding where consumer_id=".$consumer_id;
		
			$stmt= $this->_db_table->getAdapter()->query($query);
				
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result[0];
	}
	
	public function updateOutstanding(Application_Model_Outstanding $Outstanding)
	{
		$data = array(
				'consumer_id' => $Outstanding->__get("consumer_id"),
				'consumer_name' => $Outstanding->__get("consumer_name"),
				'consumer_connection_id' => $Outstanding->__get("consumer_connection_id"),
				'outstanding' => $Outstanding->__get("outstanding"),
				'site_id' => $Outstanding->__get("site_id"),
		);
		$where = "id = " . $Outstanding->__get("id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function getTopDataByConsumerId($site_id=NULL,$state_id=NULL,$role_sites_id=NULL)
	{ 
			if($site_id!=NULL){
				$site = " Where c.site_id in (".$site_id.")" ;
			}else if($state_id!=NULL){
				$site=" inner join sites s on c.site_id=s.site_id WHERE s.state_id in (".$state_id.") and s.site_status!='Disable'";
			}else{
				$site=" Where c.site_id in (".implode(",", $role_sites_id).") ";
			}
			
			$query=" Select o.* from outstanding o inner join consumers c on c.consumer_id=o.consumer_id order by o.outstanding desc limit 50";
		
			$stmt= $this->_db_table->getAdapter()->query($query);
				
			$result = $stmt->fetchAll();
		
			if (count($result) == 0) {
				return false;
			}
			return $result;
	}
	 
}