<?php
class Application_Model_UserRolesMapper
{
	protected $_db_table;
	public function __construct()
	{
		$this->_db_table = new Application_Model_DbTable_UserRoles();
	}

	public function addNewUserRole(Application_Model_UserRoles $userRole)
	{

		$data = array(
				'user_role' => $userRole->__get("user_role"),
		);
		$result = $this->_db_table->insert($data);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return $result;
		}
	}
	public function getUserRoleById($user_role_id)
	{
		$result = $this->_db_table->find($user_role_id);
		if( count($result) == 0 ) {
			$userRole = new Application_Model_UserRoles();
			return $userRole;
		}
		$row = $result->current();
		$userRole = new Application_Model_UserRoles($row);
		return $userRole;
	}
	
	public function getAllUserRoles()
	{
		$where = array();
		
		if(count($where)==0)
		{
			$where = null;
		}
	
		$result = $this->_db_table->fetchAll($where,array('user_role_id ASC'));
		if( count($result) == 0 ) {
			return false;
		}
		$userRole_object_arr = array();
		foreach ($result as $row)
		{
			$userRole_object = new Application_Model_UserRoles($row);
			array_push($userRole_object_arr,$userRole_object);
		}
		return $userRole_object_arr;
	}
	
	public function updateUserRole(Application_Model_UserRoles $UserRole)
	{
		/*$data = array(
				'user_role' => $UserRole->__get("user_role"),
		);
		$where = "user_role_id = " . $UserRole->__get("user_role_id");
		$result = $this->_db_table->update($data,$where);
		if($result==0)
		{
			return false;
		}
		else
		{
			return true;
		}*/
		$user_role= $UserRole->__get("user_role");
		$user_role_id=$UserRole->__get("user_role_id");
		
		$query ="update user_role set user_role='".$user_role."' where user_role_id=".$user_role_id;
		$stmt = $this->_db_table->getAdapter()->query($query);
		$result=$stmt->execute();
		//echo $result;
		if($result)
		{
			return true;
		}
	}
	
	
	public function deleteUserRoleById($user_role_id)
	{
		$where = "user_role_id = " . $user_role_id;
		$result = $this->_db_table->delete($where);
		if(count($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}