(function() {
    'use strict';
    var huskApp = angular.module('huskApp', ['googlechart','ngRoute', 'infinite-scroll', 'ui.bootstrap','ngSanitize', 'ngCsv','ngFileUpload','anguFixedHeaderTable']);

    huskApp.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
			when('/User', {
                templateUrl: 'angular_app/components/users/userView.html',
                controller: 'userController'
            }).
           
            when('/Dashboard', {
                templateUrl: 'angular_app/components/dashboard/dashboardView.html',
                controller: 'dashboardController'
            }).
			when('/modify-entry', {
                templateUrl: 'angular_app/components/modify-entry/modifyEntryView.html',
                controller: 'modifyEntryController'
            }).
            otherwise({
                redirectTo: '/Dashboard'
            });
        }
    ]);
  
})();
var baseUrl = "http://18.220.202.50/husk/html/";

