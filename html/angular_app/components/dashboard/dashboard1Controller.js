var huskApp = angular.module('huskApp');

huskApp.controller("dashboard1Controller", function($scope, $http, $log,$rootScope) {
	document.title = "Dashboard2";
	$scope.init=function(){
	   	$http.get(baseUrl + "default/auth/auth-data")
	   	  .success(function(response) {
	   		  $scope.userRole = response.data.userRole;
	   		$rootScope.states_array = response.data.state_id;
	   		$scope.sites = response.data.sites;
			
			 
	   		$scope.subMenus=response.data.subMenu_id;
			$scope.sitesgraph = response.data.sites;
			
			$scope.sites.unshift($scope.all_sites);
			 
	    	$rootScope.allSites=response.data.sites;
	 
	        $scope.states = $rootScope.states_array;
	    	$scope.selected_state = $rootScope.states_array[0];
	    	$scope.selected_states = response.data[0];
			$scope.statesgraph = $rootScope.states_array;
	    	//$scope.statesGen.unshift($scope.all_states);
	    	
	    	 $scope.filter=false;
			 var date_val = new Date();
			 
			 var day = (date_val.getDate() -1);  
			 var month = date_val.getMonth();
			 var year = date_val.getYear();
			  
			function daysInMonth(month, year){
				return month == 2 ? (year %4 ? 28 : (year %100 ? 29 : (year %400 ? 28 :29))) : ((month -1) %7% 2 ? 30 :31);
			} 
			var nr_days = daysInMonth(month, year);
			var number=364-(nr_days-day);
			var next_number=(nr_days-day);
			$('#reportrange span').html( moment().subtract('days', day).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
			
			$scope.widget();
			
			$scope.checkFilter(); 
			 
			 
	    });  
    }

	$scope.init();
	 
	 
	$http.get(baseUrl + "api/mppts/get-all-mppts")
		.success(function(response) {
			$scope.allMppts = response.data;
			$rootScope.Mppts = response.data;
		}); 
	
	$http.get(baseUrl + "api/site-master-meter/get-all-master-meter")
		.success(function(response) {
			$scope.allFeeders = response.data;
			$rootScope.Feeders = response.data;
		});
	
	$('[data-toggle="tooltip"]').tooltip();
     $('#reportrange').daterangepicker({
     	opens: ('left'),
     	format: 'dd-mm-yyyy',
         separator: ' to ',
         startDate: moment().subtract('days', 29),
         endDate: moment(),
         ranges: {
				'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Previous 7 Days': [moment().subtract('days', 6), moment()],
                 'Previous 30 Days': [moment().subtract('days', 29), moment()],
                 'Current Month': [moment().startOf('month'), moment()],
                 'Previous Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
         },
     },
     function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          $scope.widget();
     }); 
	 
	 
	$scope.checkFilter=function(){
		 
		  
		  function daysInMonths(month, year){
				return month == 2 ? (year %4 ? 28 : (year %100 ? 29 : (year %400 ? 28 :29))) : ((month -1) %7% 2 ? 30 :31);
			}
	     var month_days = daysInMonths($rootScope.MonthNo+1, $scope.selected_year);
	      
    	 if($rootScope.YearTypes=='on'){
			  
			  var dl = new Date($scope.selected_year+"/"+$scope.selected_month+"/01");
				  dl.setMonth(dl.getMonth() - 14);
    		  
				var options = { 
    			      year: "numeric", month: "long",  day: "numeric" 
    			};
    		  
			$rootScope.last_date_graph=dl.toLocaleDateString("en-us",options);
			 
			$rootScope.current_date_graph=$scope.selected_month+" "+month_days+", "+$scope.selected_year;
	     }else{
			$rootScope.last_date_graph="April 1,"+$scope.selected_year;
			$rootScope.current_date_graph="March 31, "+($scope.selected_year+1);
		 }
	     if($scope.graph_site_id==undefined || $scope.graph_site_id=="" || $scope.graph_site_id==null){
	    	 $rootScope.site_graph_id="ALL";
	     }else{ 
	    	 $rootScope.site_graph_id=$scope.graph_site_id; 
	     }
	     if($scope.graph_state_id==undefined || $scope.graph_state_id=="" || $scope.graph_state_id==null){
	    	 $rootScope.state_graph_id="ALL";
	     }else{  
	    	 $rootScope.state_graph_id=$scope.graph_state_id;  
	     }
	     $scope.monthly_billed();
    }
  	
 
 
	$scope.baseUrl = baseUrl;
	$scope.energyGraph=0;
	$scope.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	$scope.entryStatus = ["ALL Type", "CHG", "ACT", "EED", "MED", "OTHERS"];
	$scope.selected_entry = $scope.entryStatus[0];
	$scope.select_enrty = function(entry)
    {
        $scope.selected_entry = entry;
    };
    $scope.selected_entrys = $scope.entryStatus[0];
	$scope.select_enrtys = function(entry)
    {
        $scope.selected_entrys = entry;
    };
    $scope.years = [];
    $scope.stack_year=[];
    $scope.yearTypes=["Regular Year","Fiscal year"];
    $scope.selected_year_Type=$scope.yearTypes[0];
    $rootScope.YearTypes='on';
    $('#yearType').bootstrapToggle({
        on: 'Regular Year',
        off: 'Fiscal year'
      });
   
    $scope.selectYearType=function(){
    	if(document.getElementById("yearType").value=="off"){
    		   document.getElementById("yearType").value="on";
    	}
        else if(document.getElementById("yearType").value=="on"){
    		   document.getElementById("yearType").value="off";
        }
    	$rootScope.YearTypes=document.getElementById("yearType").value;
    	
    }
    var d = new Date();
   
    if (d.getMonth() == 0) {
    	 $scope.last_selected_month = $scope.months[11];
    	 $scope.last_selected_year = (d.getFullYear() - 1);
		 $scope.next_selected_month = $scope.months[1];
    } else {
    	 $scope.last_selected_month = $scope.months[(d.getMonth())-1];
    	 $scope.last_selected_year = d.getFullYear();
		  $scope.next_selected_month = $scope.months[(d.getMonth())+1];
    }
 
    for(var i=2014;i<= d.getFullYear();i++)
	{
    	$scope.years.push(i);
    	$scope.stack_year.push(i);
	}

    $scope.selected_month = $scope.months[d.getMonth()];
	$rootScope.MonthNo=d.getMonth();
    $scope.selected_year = d.getFullYear();
 	 
    $scope.selected_stack_year =$scope.selected_year;// d.getFullYear();
    $scope.selected_vill_year = $scope.selected_year;//d.getFullYear();

    $scope.selectMonth = function(month)
    {
        $scope.selected_month = month;
        var checkMonth=($scope.months).indexOf(month);
		$rootScope.MonthNo=checkMonth;
        $scope.last_selected_month=(checkMonth==0)?'December':$scope.months[checkMonth-1];
		$scope.next_selected_month=(checkMonth==0)?'February':$scope.months[checkMonth+1];
        $scope.last_selected_year=($scope.selected_month=='January')?($scope.selected_year)-1:$scope.selected_year;
    };
    $scope.selectYear = function(year)
    {
        $scope.selected_year = year;
        var checkMonth=($scope.months).indexOf($scope.selected_month);
        $scope.last_selected_month=(checkMonth==0)?'December':$scope.months[checkMonth-1];
        $scope.last_selected_year=($scope.selected_month=='January')?year-1:year;
		$scope.next_selected_month=(checkMonth==0)?'February':$scope.months[checkMonth+1];
        
    };
        
	$scope.all_states = {state_id:"ALL",state_name:"All States"};
    $scope.selected_states = $scope.all_states;
    
    $scope.all_sites = {site_id:"ALL",site_name:"All Sites"};
	 $scope.selected_site = $scope.all_sites;
    $scope.selected_sites = $scope.all_sites;
    
    $scope.selectSite = function(site)
    {
        $scope.selected_site = site;
    };
    $scope.selectSites = function(site)
    {
        $scope.selected_sites = site;
    };
    

    
    $scope.updateSites=function(){
    	var state_array=$rootScope.allSites; 
    	if($scope.ds_state_id=="ALL" || $scope.ds_state_id=="" || $scope.ds_state_id==undefined || $scope.ds_state_id==null)
		{
    		$scope.sites=$rootScope.allSites;
    		$scope.allMppts=$rootScope.Mppts;
    		$scope.allFeeders=$rootScope.Feeders;
    	}else{ 
	    	var stateData= $.grep(state_array, function(n) {
				return ($scope.ds_state_id).indexOf(n.state_id) != -1;
		    });
	    	$scope.sites = stateData;
	    	
	    	var mpptData= $.grep($rootScope.Mppts, function(n) {
				return ($scope.ds_state_id).indexOf(n.state_id) != -1;
		    });
	    	$scope.allMppts = mpptData;
	    	
	    	var feederData= $.grep($rootScope.Feeders, function(n) {
				return ($scope.ds_state_id).indexOf(n.state_id) != -1;
		    });
	    	$scope.allFeeders = feederData;
	    	
    	} 
    }
	 $scope.updateSitesgraph=function(){
		var state_array=$rootScope.allSites; 
    	if($scope.graph_state_id=="ALL" || $scope.graph_state_id=="" || $scope.graph_state_id==undefined || $scope.graph_state_id==null)
		{ 
    		$scope.sitesgraph=$rootScope.allSites;
    	}else{ 
	    	var stateData= $.grep(state_array, function(n) {
				return ($scope.graph_state_id).indexOf(n.state_id) != -1;
		    });
	    	$scope.sitesgraph = stateData;
	    	 
	    	
    	} 
	}
	 
      $scope.updateFeederMppt=function(){
      	if($scope.ds_site_id=="ALL"  || $scope.ds_site_id=="" || $scope.ds_site_id==undefined || $scope.ds_site_id==null)
  		{
      		$scope.allMppts=$rootScope.Mppts;
      		$scope.allFeeders=$rootScope.Feeders;
      	}else{ 
  	    
  	    	var mpptData= $.grep($rootScope.Mppts, function(n) {
  				return ($scope.ds_site_id).indexOf(n.site_id) != -1;
  		    });
  	    	$scope.allMppts = mpptData;
  	    	
  	    	var feederData= $.grep($rootScope.Feeders, function(n) {
  				return ($scope.ds_site_id).indexOf(n.site_id) != -1;
  		    });
  	    	$scope.allFeeders = feederData;
  	    	
      	} 
      } 
	
	
	$scope.monthly_billed=function(){
    	
    	 
    	   /* if($scope.selected_site.site_id=='ALL'){
    	        $http.get(baseUrl + "api/graphs/energy-consumed/yearType/on/curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
    	        					"/site_id/"+$rootScope.site_graph_id + "/state_id/"+$rootScope.state_graph_id).success(
	    	        		function(response) {
	    	                	var controls=document.getElementById('ebarChartforMeters');
	    	                	if(controls!=null){
									$scope.chartForConGen(controls,response.data[0],response.data[1],'#4cabce','Energy Consumption'); 
	    	                		//echarts.ebarChartforMeters(controls,response.data[0],response.data[1],'Energy Consumption','#4cabce');
	    	                	}
	    	                	if (response.meta.code != 200) {
	    	                        Command: toastr["error"](response.meta.message);
	    	                    }
	    	                });
    	        }*/
    	    
    	    if($scope.selected_site.site_id!='ALL'){
    	        $http.get(baseUrl + "api/graphs/energy-consumed-site-id/yearType/on/curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
    	        					"/site_id/"+$scope.selected_site.site_id + "/state_id/ALL").success(
    	                    function(response) {
    	                    	$scope.energyGraph=0;
    	                    	var controls=document.getElementById('ebarChartforMeters');
    	    	            	if(controls!=null){
    	    	                	$scope.chartForConGenSiteWise(controls,response.data[0],response.data[1]);
									//echarts.ebarChartforMetersSiteWise(controls,response.data[0],response.data[1]);
    	    	            	}               	 
    	    	            	if (response.meta.code != 200) {
    	                            Command: toastr["error"](response.meta.message);
    	                        }
    	                    });
    	       }
    	    
    	    /* Energy Generation Graph */
    	     
    	    if($scope.selected_sites.site_id=='ALL'){
					$http.get(baseUrl + "api/graphs/energy-generation/yearType/on/curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
    	        					"/site_id/"+$rootScope.site_graph_id + "/state_id/"+$rootScope.state_graph_id).success(
	    	        		function(response) {
								var controls=document.getElementById('ebarChartforMppt');
	    	                	if(controls!=null){
									$scope.chartForConGen(controls,response.data[0],response.data[1],response.data[2],response.data[3],response.data[4],'Total Generation' ,'PV Generation','DG Generation','BIOGAS Generation','#f36a5b','#2ab4c0','#34495e','#0bf91d'); 
	    	                		//echarts.ebarChartforMeters(controls,response.data[0],response.data[1],'Energy Consumption','#4cabce');
	    	                	}
								if($scope.selected_site.site_id=='ALL'){
									var controls=document.getElementById('ebarChartforMeters');
									if(controls!=null){
										$scope.chartForConGen(controls,response.data[5],response.data[1],response.data[6],response.data[7],response.data[8],'Total Consumption' ,'PV Consumption','DG Consumption','BIOGAS Consumption','#f716d1','#1dd7ef','#d48265','#120ef9'); 
										//echarts.ebarChartforMeters(controls,response.data[0],response.data[1],'Energy Consumption','#4cabce');
									}
								}
								var controls=document.getElementById('ebarChartforUnitSold');
									if(controls!=null){
										$scope.chartForConGen(controls,response.data[9],response.data[1],response.data[10],response.data[11],response.data[12],'Total Sold' ,'PV Sold','DG Sold','BIOGAS Sold','#f7ca18','#f36a5b','#ca8622','#0ff1bd'); 
										//echarts.ebarChartforMeters(controls,response.data[0],response.data[1],'Energy Consumption','#4cabce');
									}
									var controls=document.getElementById('ebarChartforUtilization');
									if(controls!=null){
										$scope.chartForPlantUtilization(controls,response.data[13],response.data[1],'#2ab4c0','Plant Utilization'); 
										//echarts.ebarChartforMeters(controls,response.data[0],response.data[1],'Energy Consumption','#4cabce');
									}
									
	    	                	if (response.meta.code != 200) {
	    	                        Command: toastr["error"](response.meta.message);
	    	                    }
	    	                });
    	        }
    	    
    	    if($scope.selected_sites.site_id!='ALL'){
    	        $http.get(baseUrl + "api/graphs/energy-generation-site-id/yearType/on/curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
    	        					"/site_id/"+$scope.selected_sites.site_id + "/state_id/ALL").success(
    	                    function(response) {
    	                    	$scope.energyGraph=0;
    	                    	var controls=document.getElementById('ebarChartforMppt');
    	    	            	if(controls!=null){
									$scope.chartForConGenSiteWise(controls,response.data[0],response.data[1]);
    	    	                	//echarts.ebarChartforMetersSiteWise(controls,response.data[0],response.data[1]);
    	    	            	}               	 
    	    	            	if (response.meta.code != 200) {
    	                            Command: toastr["error"](response.meta.message);
    	                        }
    	                    });
    	       } 
    	     
				setTimeout(function () {	  
					$http.get(baseUrl + "api/graphs/destroy").success( function(response) {
						$data = response.data;
					});
				},300);	
    } 
	
	 $scope.widget=function(){
    	 
    	 if($scope.ds_site_id==undefined || $scope.ds_site_id=="" || $scope.ds_site_id==null){
 	    	 $rootScope.site_ds_id="ALL";
 	     }else{ 
 	    	 $rootScope.site_ds_id=$scope.ds_site_id; 
 	     }
 	     if($scope.ds_state_id==undefined || $scope.ds_state_id=="" || $scope.ds_state_id==null){
 	    	 $rootScope.state_ds_id="ALL";
 	     }else{  
 	    	 $rootScope.state_ds_id=$scope.ds_state_id; 
 	     }
 	     
    	 var fullDate= document.getElementById("dateRange").innerHTML;
    	 var full_Date= fullDate.split("-");
	     $rootScope.last_date_widget=full_Date[0];
	     $rootScope.current_date_widget=full_Date[1];
	      
		 var last_full_Date1 = full_Date[0].split(", ");
	     var last_full_Date2 = full_Date[1].split(", ");
	     
	     $rootScope.lastdate_first= last_full_Date1[0] +", " + (last_full_Date1[1]-1);
	     $rootScope.lastdate_second= last_full_Date2[0] +", " + (last_full_Date2[1]-1);
		  
	     $http.get(baseUrl + "api/graphs/plant-data-for-widget/curr_date/"+$rootScope.current_date_widget+"/last_date/"+$rootScope.last_date_widget+
	    		 "/site_id/"+$rootScope.site_ds_id + "/state_id/"+$rootScope.state_ds_id+
	    		 "/mppt_id/"+$scope.mppt_id + "/feeder_id/"+$scope.feeder_id).success(
			function(response) {
				if (response.meta.code != 200) {
					Command: toastr["error"](response.meta.message);
				}else{
					$scope.plantWidget=response.data;
				}
			});
			
		 $http.get(baseUrl + "api/graphs/plant-data-for-table/curr_date/"+$rootScope.current_date_widget+"/last_date/"+$rootScope.last_date_widget+
	    		 "/site_id/"+$rootScope.site_ds_id + "/state_id/"+$rootScope.state_ds_id+
	    		 "/mppt_id/"+$scope.mppt_id + "/feeder_id/"+$scope.feeder_id +
	    		 "/lastYear_curr_date/"+$rootScope.lastdate_second+"/lastYear_last_date/"+$rootScope.lastdate_first).success(
			function(response) {
				if (response.meta.code != 200) {
					Command: toastr["error"](response.meta.message);
				}else{
					$scope.mppts=response.data.mppt;
					$scope.mpptothers=response.data.mppt_other;
					$scope.feeders=response.data.feeder;
				}
			});
     }
	
	$scope.chartForConGen=function(control,total,month,pv,dg,bio,total_name,pv_name,dg_name,bio_name,color1,color2,color3,color4){
		var myChart = echarts.init(control);
		var app = {};
		option = null;
		 
		option = {
		   tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
			legend: {
                    data: [total_name,pv_name,dg_name,bio_name],
                },
		    grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    calculable: true,
		    xAxis : [
		        {
		            type : 'category',
		            data : month,
		            axisTick: {
		                alignWithLabel: true
		            },
		            
                    
		        }
		    ],
		    yAxis: [{
            	type: 'value',
                
            }],
		    series : [
		       
				{
		            name:total_name,
		            type:'bar',
					color:[color1],
		            data:total,
		             label: {
		                normal: {
		                    show: true,
		                    position: 'inside',
							 rotate: 90,
		                }
		            },
		        },
				{
		            name:pv_name,
		            type:'bar',
					color:[color2],
		            data:pv,
		             label: {
		                normal: {
		                    show: true,
		                    position: 'inside',
							rotate: 90,
		                }
		            },
		        },
				 {
		            name:dg_name,
		            type:'bar',
					color:[color3],
		            data:dg,
		             label: {
		                normal: {
		                    show: true,
		                    position: 'inside',
							rotate: 90,
		                }
		            },
		        },
				{
		            name:bio_name,
		            type:'bar',
					color:[color4],
		            data:bio,
		             label: {
		                normal: {
		                    show: true,
		                    position: 'inside',
							rotate: 90, 
		                }
		            },
		        }
		    ]
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
	$scope.chartForPlantUtilization=function(control,xaxis,yaxis,colors,name){
		var myChart = echarts.init(control);
		var app = {};
		option = null;
		app.title = name;
	
		option = {
		    color: [colors],
		    tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
		    grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    calculable: true,
		    xAxis : [
		        {
		            type : 'category',
		            data : yaxis,
		            axisTick: {
		                alignWithLabel: true
		            },
		            
                    
		        }
		    ],
		    yAxis: [{
            	type: 'value',
                
            }],
		    series : [
		        {
		            name:name,
		            type:'bar',
		            barWidth: '80%',
		            data:xaxis,
		             label: {
		                normal: {
		                    show: true,
		                    position: 'top',
		                }
		            },
		        }
		    ]
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
	$scope.chartForConGenSiteWise=function(control,reading,months){
		var myChart = echarts.init(control);
		option = null;
		var meter_id=[];
   	 	var readingdatas = [];
   	 	var month_detail=[];
   	 	var totalSession=[];
        
   	 	for (var i = 0; i < reading.length; i++){
   	 		meter_id.push(reading[i]["meter_id"]); 
   	 		readingdatas.push(reading[i]["reading"]);
       	}
        
        for(var j=0;j<meter_id.length;j++){
	       	var session = {
	       			 name:meter_id[j], 
	       			 type:"bar", 
	       			 data:readingdatas[j],
	       			 label: {
		                normal: {
		                    show: true,
		                    position: 'top',
		                }
		             },
	       	};
	       	 
	       	totalSession.push(session);
        }
		   
        if(meter_id.length<=0){
	       	 meter_id.push("NULL");
	       	 totalSession=[{
	       		 name:"NULL", 
	           	 type:"bar", 
	           	 data:[0,0,0,0,0,0,0,0,0,0,0,0],
	             label: {
		                normal: {
		                    show: true,
		                    position: 'top',
		                }
		            },
	         }];
	     }
        
		option = {
		    tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
		    legend: {
                data: meter_id,
            },
            grid: {
		        left: '1%',
		        right: '1%', 
		        bottom: '1%',
		        containLabel: true
		    },
		    calculable: true,
		    xAxis: [{
                type: 'category',
                data: months,
                axisTick: {
	                alignWithLabel: true
	            },
	           
            }],
            yAxis: [{
            	type: 'value',
            	
            }],
		    series: totalSession,
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
});
