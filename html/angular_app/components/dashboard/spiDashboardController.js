var huskApp = angular.module('huskApp');

huskApp.controller("spiDashboardController", function($scope, $http, $log,$rootScope) {
	
	$scope.init=function(){
	   	$http.get(baseUrl + "default/auth/auth-data")
	   	  .success(function(response) {
	   		$scope.userRole = response.data.userRole;
			$rootScope.states_array = response.data.state_id;
			$scope.sites = response.data.sites;
			$scope.subMenus=response.data.subMenu_id;
			
			$rootScope.allSites=response.data.sites;
			$scope.states = $rootScope.states_array;
			  
	    	
	    	$scope.checkFilter(); 
	    });  
    }

	$scope.init();
	 
	$scope.checkFilter=function(){
		
			function daysInMonths(month, year){
				return month == 2 ? (year %4 ? 28 : (year %100 ? 29 : (year %400 ? 28 :29))) : ((month -1) %7% 2 ? 30 :31);
			}
	     
		 	var month_days = daysInMonths($rootScope.MonthNo, $scope.selected_year);
	  
		 	 var dl = new Date($scope.selected_year+"/"+$scope.selected_month+"/01");
			  dl.setMonth(dl.getMonth() - 5);
		  
			var options = { 
			      year: "numeric", month: "long",  day: "numeric" 
			};
		  
			$rootScope.last_date_graph=dl.toLocaleDateString("en-us",options);
			$rootScope.current_date_graph=$scope.selected_month+" "+month_days+", "+$scope.selected_year;
		
		 if($scope.spi_site_id==undefined || $scope.spi_site_id=="" || $scope.spi_site_id==null){
	    	 $rootScope.site_spi_id="ALL";
	     }else{ 
	    	 $rootScope.site_spi_id=$scope.spi_site_id; 
	     }
	     if($scope.spi_state_id==undefined || $scope.spi_state_id=="" || $scope.spi_state_id==null){
	    	 $rootScope.state_spi_id="ALL"; 
	     }else{ 
	    	 $rootScope.state_spi_id=$scope.spi_state_id; 
	     }
	     $scope.monthly_billed();
    }
     	
	$scope.baseUrl = baseUrl;
	$scope.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    $scope.years = [];
    var d = new Date();
    for(var i=2014;i<= d.getFullYear();i++)
	{
    	$scope.years.push(i);
    }

    $scope.selected_month = $scope.months[d.getMonth()];
	$rootScope.MonthNo=d.getMonth()+1;
    $scope.selected_year = d.getFullYear();
 	 
    $scope.selectMonth = function(month)
    {
        $scope.selected_month = month;
        var checkMonth=($scope.months).indexOf(month);
        $rootScope.MonthNo=checkMonth+1;
      
    }
    $scope.selectYear = function(year)
    {
        $scope.selected_year = year;
    }

    $scope.updateSites=function(){
    	var state_array=$rootScope.allSites; 
    	if($scope.spi_state_id=="ALL")
		{
    		$scope.sites=$rootScope.allSites;
    	}else{
	    	var stateData= $.grep(state_array, function(n) {
				return ($scope.spi_state_id).indexOf(n.state_id) != -1;
		    });
	    	$scope.sites = stateData;
    	} 
    }
      
	$scope.monthly_billed=function(){
		
    	/*New consumer Count*/
		$http.get(baseUrl + "api/spi-dashboard/get-data-widget/" + "month/"+$scope.selected_month+"/year/"+$scope.selected_year+
            		"/site_id/"+$rootScope.site_spi_id+"/state_id/"+ $rootScope.state_spi_id).success(
        	function(response) {
            	var control=document.getElementById('consumer_count');
            	var TagName="Total Customers";
    	    	if(control!=null){
    	        	$scope.SpiebarCharts(control,response.data[0].data,TagName,'#4cabce','bar');
    	    	}
    	    	
    	    	var control_rev=document.getElementById('unit_per_user');
             	var TagName_rev="Avg. Unit Per User";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[3].data,TagName_rev,'#F7350B','bar');
     	    	}
     	    	
     	    	var control_mon=document.getElementById('revenue_avg');
	            var TagName_mon="Avg. Revenue";
	    	    if(control_mon!=null){
	    	       	$scope.SpiebarCharts(control_mon,response.data[1].data,TagName_mon,'#32cd32','bar');
	    	    }

     	    	var control_rev=document.getElementById('units');
             	if(control_rev!=null){
     	        	$scope.SpimultiebarCharts(control_rev,response.data[6].data,response.data[7].data,response.data[8].data);
     	    	}
             	
             	var control_rev=document.getElementById('monthly_revenue_data');
             	var TagName_rev="Monthly Revenue";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[2].data,TagName_rev,'#7b68ee','bar');
     	    	}
     	    	
     	    	var control_rev=document.getElementById('cost');
             	var TagName_rev="Cost";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[4].data,TagName_rev,'#87cefa','bar');
     	    	}
     	    	
     	    	var control_rev=document.getElementById('EBITDA');
             	var TagName_rev="EBITDA";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[5].data,TagName_rev,'#E20BF7','bar');
     	    	}
     	    	
     	});
		
		/*New consumer Count*/
		$http.get(baseUrl + "api/spi-dashboard/get-data-month-wise/" + "curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
            		"/site_id/"+$rootScope.site_spi_id+"/state_id/"+ $rootScope.state_spi_id).success(
        	function(response) {
            	var control=document.getElementById('consumer_count_month');
            	var TagName="Total Customers";
    	    	if(control!=null){
    	        	$scope.SpiebarCharts(control,response.data[0].data,TagName,'#4cabce','line');
    	    	}
    	    	
    	    	var control_rev=document.getElementById('unit_per_user_month');
             	var TagName_rev="Avg. Unit Per User";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[3].data,TagName_rev,'#F7350B','line');
     	    	}
     	    	
     	    	var control_mon=document.getElementById('revenue_avg_month');
	            var TagName_mon="Avg. Revenue";
	    	    if(control_mon!=null){
	    	       	$scope.SpiebarCharts(control_mon,response.data[1].data,TagName_mon,'#32cd32','line');
	    	    }

     	    	var control_rev=document.getElementById('units_consumed_month'); 
     	    	 var TagName_mon="Unit Consumed";
             	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[6].data,TagName_mon,'#7b68ee','line');
     	    	}
             	var control_rev=document.getElementById('units_sold_month');
    	    	 var TagName_mon="Unit Sold";
            	if(control_rev!=null){
    	        	$scope.SpiebarCharts(control_rev,response.data[7].data,TagName_mon,'#ff69b4','line');
    	    	}
             	var control_rev=document.getElementById('monthly_revenue_data_month');
             	var TagName_rev="Monthly Revenue";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[2].data,TagName_rev,'#F7350B','line');
     	    	}
     	    	
     	    	var control_rev=document.getElementById('cost_month');
             	var TagName_rev="Cost";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[4].data,TagName_rev,'#7b68ee','line');
     	    	}
     	    	
     	    	var control_rev=document.getElementById('EBITDA_month');
             	var TagName_rev="EBITDA";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[5].data,TagName_rev,'#4cabce','line');
     	    	}
     	    	var control_rev=document.getElementById('EBITDA_month_total');
             	var TagName_rev="EBITDA Total";
     	    	if(control_rev!=null){
     	        	$scope.SpiebarCharts(control_rev,response.data[8].data,TagName_rev,'#ff69b4','bar');
     	    	}
				var control_rev=document.getElementById('generation_mix');
             	if(control_rev!=null){ 
     	        	$scope.SpiGenerationCharts(control_rev,response.data[9].data,response.data[10].data,response.data[11].data);
     	    	}
    	    	 
        });
		
		/*New consumer Count*/
		$http.get(baseUrl + "api/spi-dashboard/get-stack-chart-consumer-revenue/" + "curr_date/"+$rootScope.current_date_graph+"/last_date/"+$rootScope.last_date_graph+
            		"/site_id/"+$rootScope.site_spi_id+"/state_id/"+ $rootScope.state_spi_id).success(
        	function(response) {
            	var control=document.getElementById('customer_mix');
            	if(control!=null){
    	        	$scope.SpiStackCharts(control,response.data[1].data,response.data[2].data,response.data[3].data,response.data[4].data,response.data[0].data);
    	    	}
    	    	 
    	    	var control_rev=document.getElementById('revenue_mix');
    	    	if(control_rev!=null){
    	        	$scope.SpiStackRevenueCharts(control_rev,response.data[5].data,response.data[6].data,response.data[7].data,response.data[8].data);
    	    	}
     	});
	 	 
		/*People Impacted*/
		$http.get(baseUrl + "api/spi-dashboard/get-people-impacted/" + "month/"+$scope.selected_month+"/year/"+$scope.selected_year+
        		"/site_id/"+$rootScope.site_spi_id+"/state_id/"+ $rootScope.state_spi_id).success(
    	function(response) {
        	var control=document.getElementById('people_impacted');
        	var TagName="People Impacted";
	    	if(control!=null){
	        	$scope.SpiebarCharts(control,response.data[0].data,TagName,'#4cabce','line'); 
	    	}
    	});
    }

	//Functions for Graphs and charts
	 
	$scope.SpiebarCharts=function(control,Maindata,TagName,colors,graphtype){ 
		var myChart = echarts.init(control);
		var app = {};
		option = null;
		app.title = name;
		var Main_datas = [];
		var month_detail=[];
		for (var i = 0; i < Maindata.length; i++){
			Main_datas.push(Maindata[i][1]);
			month_detail.push(Maindata[i][0]);
		}
		option = {
		    color: [colors],
		    tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
			legend: {
				data: [TagName],
	    	},
		    grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : month_detail,
		            axisTick: {
		                alignWithLabel: true
		            },
					axisLabel : {
					  rotate : 15
					},
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ], 
		    series : [
		        {
		            name:TagName,
		            type:graphtype,  
		            barWidth: '70%',
		            data:Main_datas,
		             label: {
		                normal: {
		                    show: true,
		                    position: "top",
		                }
		            },
		        }
		    ]
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
	$scope.SpimultiebarCharts=function(control,Maindata,generation,sold){
		var myChart = echarts.init(control);
		var app = {};
		option = null;
		app.title = name;
		var Main_datas = [];
		var month_detail=[];
		var generation_arr=[];
		var sold_arr=[];
		for (var i = 0; i < Maindata.length; i++){
			Main_datas.push(Maindata[i][1]);
			generation_arr.push(generation[i][1]);
			sold_arr.push(sold[i][1]);
			month_detail.push(Maindata[i][0]);
		}
		
		option = {
		   tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
			legend: {
				data: ["Avg. Units Generated","Avg. Units Consumed","Avg. Units Sold"],
			},
		    grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : month_detail,
		            axisTick: {
		                alignWithLabel: true
		            },
					axisLabel : {
					  rotate : 15
					},
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		     series: [{
					name: "Avg. Units Generated",
					type: 'bar',
					data: generation_arr,
					 color:['#f36a5b'],
					label: {
						normal: {
							show: true,
							position: "top",
							rotate:90,
							align: 'left',
							verticalAlign: 'middle',
						}
					}
				},
				{
					name: "Avg. Units Consumed",
					type: 'bar',
					data: Main_datas,
					color:['#2ab4c0'],
					label: {
						normal: {
							show: true,
							position: "top", 
							rotate:90,
							align: 'left',
							verticalAlign: 'middle',
						}
					}
				},
				{
					name: "Avg. Units Sold",
					type: 'bar',
					data: sold_arr,
					color:['#5fc9a7'],
					label: {
						normal: {
							show: true,
							position: "top",
							rotate:90,
							align: 'left',
							verticalAlign: 'middle',
						}
					}
		    	}], 
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
	$scope.SpiGenerationCharts=function(control,solar,dg,totalunit){
		var myChart = echarts.init(control);
		var app = {};
		option = null;
		app.title = name;
		var solar_arr = [];
		var dg_arr=[];
		var totalunit_arr=[];
				    	        	  
		var month_detail=[];
		for (var i = 0; i < solar.length; i++){
			solar_arr.push(solar[i][1]);
			dg_arr.push(dg[i][1]);
			totalunit_arr.push(totalunit[i][1]);
			month_detail.push(solar[i][0]);
		}
		
		option = {
		   tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
			legend: {
				data: ["Solar","DG","Unit Generated"],
			},
		    grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : month_detail,
		            axisTick: {
		                alignWithLabel: true
		            },
					axisLabel : {
					  rotate : 15
					},
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		     series: [{
					name: "Solar",
					type: 'bar',
				    data: solar_arr,
				    stack:true,
					 color:['#f36a5b'],
					label: {
						normal: {
							show: true,
							position: "inside",
							 
						}
					}
				},
				{
					name: "DG",
					type: 'bar',
					data: dg_arr,
					stack:true,
					color:['#2ab4c0'],
					label: {
						normal: {
							show: true,
							position: "inside", 
							 
						}
					}
				},
				{
					name: "Unit Generated",
					type: 'line',
					data: totalunit_arr,   
					stack:true,
					color:['#5fc9a7'],
					label: {
						normal: {
							show: true,
							position: "top",
							 
						}
					}
		    	}], 
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
	$scope.SpiStackCharts=function(control,household,shop,commercial,telecom,consumer){
		var myChart = echarts.init(control);
		var app = {};
		option = null;
		app.title = name;
		var hh = [];  var sh=[]; var comm=[];  var tele=[]; var con=[];  var month_detail=[];
		for (var i = 0; i < household.length; i++){
			hh.push(household[i][1]);
			sh.push(shop[i][1]);
			comm.push(commercial[i][1]);
			tele.push(telecom[i][1]);
			con.push(consumer[i][1]);
			month_detail.push(household[i][0]);
		}
		 
		option = {
		   tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
			legend: {
				data: ["Household","Shops","Commercial",'Telecom',"Total Customers"],
			},
		    grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : month_detail,
		            axisTick: {
		                alignWithLabel: true
		            },
					axisLabel : {
					  rotate : 15
					},
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		     series: [{
					name: "Household",
					type: 'bar',
					data: hh,
					stack:true,
					color:['#f36a5b'],
					label: {
						normal: {
							show: true,
							position: "inside",
							 
						}
					}
				},
				{
					name: "Shops",
					type: 'bar',
					data: sh,
					stack:true,
					color:['#2ab4c0'],
					label: {
						normal: {
							show: true,
							position: "inside", 
							 
						}
					}
				},
				{
					name: "Commercial",
					type: 'bar',
					data: comm,
					stack:true,
					color:['#5fc9a7'],
					label: {
						normal: {
							show: true,
							position: "inside",
							 
						}
					}
		    	},
				{
					name: "Telecom",
					type: 'bar',
					data: tele,
					stack:true,
					color:['#ca8622'],
					label: {
						normal: {
							show: true,
							position: "inside",
							 
						}
					}
		    	},
				{
					name: "Total Customers",
					type: 'line',
					data: con,
					stack:true,
					color:['#2f4554'],
					label: {
						normal: {
							show: true,
							position: "top",
							 
						}
					}
		    	}
				
				], 
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	
	$scope.SpiStackRevenueCharts=function(control,household,shop,commercial,telecom){
		var myChart = echarts.init(control);
		var app = {};
		option = null;
		app.title = name;
		var hh = [];  var sh=[];  var comm=[]; var tele=[]; var month_detail=[];
		for (var i = 0; i < household.length; i++){
			hh.push(household[i][1]);
			sh.push(shop[i][1]);
			comm.push(commercial[i][1]);
			tele.push(telecom[i][1]);
			month_detail.push(household[i][0]);
		}
		 
		option = {
		   tooltip : {
		        trigger: 'axis',
		        axisPointer : {             
		            type : 'shadow'        
		        }
		    },
			legend: {
				data: ["Household","Shops","Commercial",'Telecom'],
			},
		    grid: {
		        left: '1%',
		        right: '1%',
		        bottom: '1%',
		        containLabel: true
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : month_detail,
		            axisTick: {
		                alignWithLabel: true
		            },
					axisLabel : {
					  rotate : 15
					},
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		     series: [{
					name: "Household",
					type: 'bar',
					data: hh,
					stack:true,
					color:['#f36a5b'],
					label: {
						normal: {
							show: true,
							position: "inside",
							 
						}
					}
				},
				{ 
					name: "Shops",
					type: 'bar',
					data: sh,
					stack:true,
					color:['#2ab4c0'],
					label: {
						normal: {
							show: true,
							position: "inside", 
							 
						}
					}
				},
				{
					name: "Commercial",
					type: 'bar',
					data: comm,
					stack:true,
					color:['#5fc9a7'],
					label: { 
						normal: {
							show: true,
							position: "inside",
							 
						}
					}
		    	},
				{
					name: "Telecom",
					type: 'bar',
					data: tele,
					stack:true,
					color:['#ca8622'],
					label: {
						normal: {
							show: true,
							position: "inside",
							  
						}
					}
		    	} 
				
				], 
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
});
