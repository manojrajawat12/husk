var huskApp = angular.module('huskApp');

huskApp.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

huskApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
    	
       var fd = new FormData();
       fd.append('file', file);
    
       $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       })
      
      .success(function(response){
       })
    
       .error(function(){
       });
      console.log(uploadUrl);
    }
 }]);
huskApp.controller("userController", function($scope, $http, $log , fileUpload) {
  //  Command: toastr["info"]("Loading data, Please wait...");
    $http.get(baseUrl + "api/users/get-all-users")
            .success(function(response) {
                $scope.users = response.data;
//  Command: toastr["success"]("Users have been loaded");
            });
	$http.get(baseUrl + "api/user-role/get-all-user-roles")
    .success(function(response) {
        $scope.userRoles = response.data;
 //       Command: toastr["success"]("UserRole have been loaded");
    });
  
    $scope.addUser = function() {
		var profile="";
    	if($scope.myFile!=undefined){
    		profile=$scope.myFile.name;
    	}
        var userData = "user_fname/" + $scope.user_fname+ "/";
        userData += "user_lname/" + $scope.user_lname+ "/";
        userData += "user_role_id/" + $scope.user_role_id + "/";
        userData += "username/" + $scope.username + "/";
        userData += "user_email/" + $scope.user_email+ "/";
        userData += "hashed_password/" + $scope.hashed_password + "/";
        userData += "phone/" + $scope.phone +"/";
        userData += "profile_image/" + profile; 
  
        console.log(baseUrl + "api/users/add-user/" + userData);
        $http.post(baseUrl + "api/users/add-user/" + userData)
                .success(function(response) {
                    if (response.meta.code == "200") {
                        $('#addUserForm')[0].reset();
                        var new_user = {
                            user_fname: response.data.user_fname,
                            user_lname: response.data.user_lname,
                            username: response.data.username,
                            user_email: response.data.user_email,
                            user_role: response.data.user_role,
                            user_id: response.data.user_id,
                            phone: response.data.phone,
							user_role_id: response.data.user_role_id,
                            profile_image:response.data.profile_image, 
                        };
                         Command: toastr["success"]("User has been added");

                        $scope.users.unshift(new_user);
                    } else {
                   Command: toastr["error"]("Oops Somthing Went Worng");
                    }
                });
    }
    $scope.deleteUser = function(user) {
        console.log(user);
        var modalInstance = $modal.open({
            templateUrl: 'deleteUser.html',
            controller: 'ModalUserCtrl',
            resolve: {
                selectedUser: function() {
                    return user;
                }
            }
        });
        modalInstance.result.then(function(user) {

            $scope.selectedUser = user;
            $scope.confirmDelete(user);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }
    $scope.confirmDelete = function(user) {

        $http.post(baseUrl + "api/users/delete-user-by-id/id/" + user.user_id)
                .success(function(response) {

                    if (response.meta.code == "200") {

                        Command: toastr["info"]("User deleting please Wait");

                        var position = $scope.users.indexOf(user);

                        if (position > -1) {

                            $scope.users.splice(position, 1);
                        }

                        Command: toastr["success"]("User has been deleted");
                    } else {
                        Command: toastr["danger"]("User has been updated");
                    }

                });

    }

    $scope.editUser = function(user) {
        
          Command: toastr["info"]("User Updating Please Wait");
        var modalInstance = $modal.open({
            templateUrl: 'editUser.html',
            controller: 'ModalUserCtrl',
            resolve: {
                selectedUser: function() {
                    return user;
                }
            }
        });

        modalInstance.result.then(function(selectedUser) {
            $scope.selectedUser = selectedUser;
            $scope.updateUser(selectedUser);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });



    };
    $scope.uploadFile = function(files){
    	var types = ["image/jpeg", "image/jpg", "image/png", "image/gif"];
       var file = files;
	   console.log(file);
       var checkFile=(types).indexOf(file.type); 
       console.log(checkFile);
		if(checkFile>=0){
		
       var uploadUrl ="http://taraurja.in/angular_app/components/users/uploadImg.php";
 	   fileUpload.uploadFileToUrl(file, uploadUrl);
		}else{
			Command: toastr["error"]("Invalid Image Type.");
			exit();
		}
     }
 
    $scope.submitForm = function(isValid) {
        if (isValid) 
        {  
        	if($scope.myFile!=undefined){
        		$scope.uploadFile($scope.myFile);
        	}
        
        	$scope.addUser();
        }
        else
        {
            Command: toastr["error"]("Error while validating form");
        }

    };
    $scope.updateUser = function(user) {
       
	   var profile;
    	if(user.myFile!=undefined){
    		$scope.uploadFile(user.myFile);
    		profile=user.myFile.name;
    	}else{
    		profile=user.profile_image;
    	}   
        var userData = "user_fname/" + user.user_fname+ "/";
        userData += "user_lname/" + user.user_lname+ "/";
        userData += "user_role_id/" + user.user_role_id + "/";
        userData += "username/" + user.username + "/";
        userData += "user_email/" + user.user_email+ "/";
        userData += "hashed_password/" + user.hashed_password + "/";
        userData += "phone/" + user.phone + "/";
        userData += "profile_image/" + profile;
        console.log(baseUrl + "api/users/update-user-by-id/user_id/" + user.user_id + "/" + userData);
        $http.post(baseUrl + "api/users/update-user-by-id/user_id/" + user.user_id + "/" + userData).success(function(response) {
            if (response.meta.code == "200") {
				$http.get(baseUrl + "api/users/get-all-users")
                .success(function(response) {
                    $scope.users = response.data;
                });
                Command: toastr["success"]("User has been updated");
            }
            else
            {
                Command: toastr["error"]("Error occured while updating. Please try again.");
            }
        });
    }
});

huskApp.controller('ModalUserCtrl', function($scope, $http, $modalInstance, selectedUser) {
    $scope.selectedUser = selectedUser;
   
    $http.get(baseUrl + "api/users/get-all-users")
            .success(function(response) {
                $scope.users = response.data;
 // Command: toastr["success"]("Users have been loaded");
            });
   $http.get(baseUrl + "api/user-role/get-all-user-roles")
    .success(function(response) {
        $scope.userRoles = response.data;
     }); 
    $scope.ok = function() {
        $modalInstance.close($scope.selectedUser);
    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.confirm = function() {
        $modalInstance.close($scope.selectedUser);
    };
   
});

