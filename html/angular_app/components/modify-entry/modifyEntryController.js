var huskApp = angular.module('huskApp');
huskApp.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

huskApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
    	
       var fd = new FormData();
       fd.append('file', file);
    
       $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       })
      
      .success(function(response){
		  
       })
    
       .error(function(){
       });
      
    }
 }]);
huskApp.controller("modifyEntryController", function($scope, $http, $modal, $log,fileUpload) {
 
	 $scope.uploadfile = function(files){
		 	/*var types = ["application/vnd.ms-excel"];
	        var file = files;
	        var checkFile=(types).indexOf(file.type); 
		    if(checkFile>=0){*/
			  	var uploadUrl =baseUrl+"angular_app/components/modify-entry/uploadcsv.php";
				
			  		fileUpload.uploadFileToUrl(files, uploadUrl);
			  		
			/*}else{
				Command: toastr["error"]("Invalid Image Type.");
				exit();
			}*/
	 }
	 
	 $scope.add = function(){
		 $scope.uploadfile($scope.addfile);
		 $scope.addFileCsv($scope.addfile);
	 }
	 
	 $scope.update = function(){
		 $scope.uploadfile($scope.updatefile);
		 $scope.updateFileCsv($scope.updatefile);
	 }
	 
	 $scope.delete = function(){
		 $scope.uploadfile($scope.deletefile);
		 $scope.deleteFileCsv($scope.deletefile);
	 
	 }
	 $scope.inactive = function(){
		 $scope.uploadfile($scope.inactivefile);
		 $scope.inactiveFileCsv($scope.inactivefile);
	 
	 }
	 $scope.bankStatement = function(){
		 var filename=$scope.uploadfile($scope.bankStatementFile);
		 console.log(filename);
		 $scope.bankStatementCSV($scope.bankStatementFile);
	 
	 }
	 
	 
	 $scope.deleteFileCsv = function() {
			var profile="";
	    	if($scope.deletefile!=undefined){
	    		profile=$scope.deletefile.name;
	    	}
	        var userData = "delete_file/" + profile+ "/";
	         Command: toastr["info"]("Delete transactions Please Wait");
	        $http.post(baseUrl + "api/modify-entry/delete-transaction/" + userData)
	                .success(function(response) {
	                    if (response.meta.code == "200") {
	                         Command: toastr["success"]("Transactions have been deleted");  
							 window.location.reload();
	                    } else {
	                   Command: toastr["error"]("Oops Somthing Went Worng");
	                    }
	                });
	 }
	        
	        $scope.addFileCsv = function() {
				var profile="";
		    	if($scope.addfile!=undefined){
		    		profile=$scope.addfile.name;
		    	}
		        var userData = "add_file/" + profile+ "/";
		          Command: toastr["info"]("CSV adding transaction Please Wait");
		        $http.post(baseUrl + "api/modify-entry/add-transaction/" + userData)
		                .success(function(response) {
		                    if (response.meta.code == "200") {
		                        Command: toastr["success"]("Transaction have been Added");
								//window.location.reload();
		                    } else {
		                   Command: toastr["error"]("Oops Somthing Went Worng");
		                    }
		                });
	        
	        }
	        
	        $scope.updateFileCsv = function() {
				var profile="";
		    	if($scope.updatefile!=undefined){
		    		profile=$scope.updatefile.name;
		    	}
		        var userData = "update_file/" + profile+ "/"; 
		          Command: toastr["info"]("CSV update transaction Please Wait");
		        $http.post(baseUrl + "api/modify-entry/update-transaction/" + userData)
		                .success(function(response) {
		                    if (response.meta.code == "200") {
		                         Command: toastr["success"]("Transaction have been updated");
								 window.location.reload();
		                    } else {
		                   Command: toastr["error"]("Oops Somthing Went Worng");
		                    }
		                });
	        
	        }
	        
	        $scope.inactiveFileCsv = function() {
				var profile="";
		    	if($scope.inactivefile!=undefined){
		    		profile=$scope.inactivefile.name;
		    	}
		        var userData = "inactive_file/" + profile+ "/";
		         Command: toastr["info"]("CSV inactive transaction Please Wait");
		        $http.post(baseUrl + "api/modify-entry/inactive-transaction/" + userData)
		                .success(function(response) {
		                    if (response.meta.code == "200") {
		                         Command: toastr["success"]("Transactions have been inactive");
								 window.location.reload();
		                    } else {
		                   	Command: toastr["error"]("Oops Somthing Went Worng");
		                    }
		                });
	        }
			
			$scope.bankStatementCSV = function() {
				var bank="";
		    	if($scope.bankStatementFile!=undefined){
		    		bank=$scope.bankStatementFile.name;
		    	} 
		        var userData = "statement_file/" + bank+ "/";
		        Command: toastr["info"]("CSV inactive transaction Please Wait");
				angular.element("input[type='file']").val(null);
		        $http.post(baseUrl + "api/modify-entry/bank-statement/" + userData)
		                .success(function(response) {
		                    if (response.meta.code == "200") {
		                         Command: toastr["success"]("Bank Statement have been uploaded.");
								 // window.location.reload();
							 } else {
		                   	Command: toastr["error"]("Oops Somthing Went Worng");
		                    }
		                }); 
			}
});

	      
			
	







