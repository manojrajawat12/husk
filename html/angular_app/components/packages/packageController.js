var huskApp = angular.module('huskApp');

huskApp.controller("packageController", function($scope, $http , $log,$rootScope) {
   // Command: toastr["info"]("Loading data, Please wait...");
   	document.title = "Packages";
    $http.get(baseUrl + "api/packages/get-all-packages/status/true")
            .success(function(response) {
            	
                $scope.packages = response.data;
                $rootScope.Allpackages=response.data;
                $scope.value=0;
    });
    $scope.unit =0;
    $scope.extra_charges = 0;
    $scope.value=0;
   
    $scope.init=function(){
    	$http.get(baseUrl + "default/auth/auth-data")
		 .success(function(response) {
			$scope.userRole = response.data.userRole;
			$scope.states=response.data.state_id;
			$scope.sites=response.data.sites;
			$rootScope.allSites=response.data.sites;
			$rootScope.selectedSite=response.data.sites;
			$rootScope.AllStates=response.data.state_id;
			$scope.allsites=response.data.sites;
			$scope.subMenus= response.data.subMenu_id;
		});
	}
   $scope.init();
   
   $http.get(baseUrl + "api/site-master-meter/get-all-master-meter")
   	.success(function(response) {
   		$rootScope.meter_id = response.data;
		$rootScope.feeder_types = response.feeder_type;
   		$rootScope.descriptions = response.description;
   }); 
   
   $scope.updateSites=function(){
	   	if(($scope.state_id).length==0){
	   		$scope.sites = $rootScope.allSites;
	   	}else{
		    var site_array=$rootScope.allSites; 
		    var siteData= $.grep(site_array, function(n) {
		    	return ($scope.state_id).indexOf(n.state_id) != -1;
			});
		    $scope.sites = siteData;
	   	}
   }
   
   $scope.getFeederWattage= function(){
   		$http.post(baseUrl + "api/wattage/get-wattage-by-multiple-site-id/site_id/" + $scope.site_id)
   			.success(function(response) {
   				$scope.wattages = response.Allwatt;
   				
		});

   	    if(($scope.site_id).length==0){
	    	Command: toastr["error"]("There is no master meter on this site.");
	   	}else{
		    var meter_array=$rootScope.meter_id; 
		    var meterData= $.grep(meter_array, function(n) {
		    	return ($scope.site_id).indexOf(n.site_id) != -1;
			});
		    $scope.site_meters = meterData;
	   	}
	    
	 $rootScope.site_value_meters = $scope.site_meters; 
 	 $rootScope.All_value_meters = $scope.site_meters;
		
   }
   $scope.getFeederType= function(){
  		 
  	    if(($scope.feeder_type).length==0){
	    	$scope.site_meters = $rootScope.site_value_meters ;
	   	}else{
		    var meter_array=$rootScope.meter_id; 
		    
		    var siteData= $.grep(meter_array, function(n) {
		    	return ($scope.site_id).indexOf(n.site_id) != -1;
			});
		    
		    var meterData= $.grep(siteData, function(n) {
		    	return ($scope.feeder_type).indexOf(n.feeder_type) != -1;
			});
		    $scope.site_meters = meterData;
	   	}
  	  $rootScope.All_value_meters = $scope.site_meters;
  }
   
   	$scope.getFeederDesc= function(){
		 
 	    if(($scope.description).length==0){
	    	$scope.site_meters = $rootScope.site_value_meters ;
	   	}else{
		    var meter_array=$rootScope.meter_id; 
		    
		    var siteData= $.grep(meter_array, function(n) {
		    	return ($scope.site_id).indexOf(n.site_id) != -1;
			});
		    
			var feederType= $.grep(siteData, function(n) {
		    	return ($scope.feeder_type).indexOf(n.feeder_type) != -1;
			});
			
		    var meterData= $.grep(feederType, function(n) {
		    	return ($scope.description).indexOf(n.description) != -1;
		    	
			});
		    $scope.site_meters = meterData;
	   	}
 	   $rootScope.All_value_meters = $scope.site_meters;
		
 }
   $http.get(baseUrl + "api/Consumers/get-all-consumer-types")
   .success(function(response) {
       $scope.consumerTypes = response.data;
       $rootScope.AllConsumerTypes = response.data;
   });
    
   $scope.filterSites = function(e) {
	   var siteswise=$rootScope.Allpackages;
	   if($scope.sites_id!=undefined){
	   var siteswise=$.grep(siteswise,function(n){
	   		return $scope.sites_id==n.site_id;
	   	});
	   }
	   if($scope.is_postpaids!=undefined){
		   var siteswise=$.grep(siteswise,function(n){
		   		return $scope.is_postpaids==n.is_postpaid_value;
		   	});
	   }
	   if($scope.statuss!=undefined){
		   var siteswise=$.grep(siteswise,function(n){
		   		return $scope.statuss==n.status;
		   	});
	   }
	   $scope.packages=siteswise;
	   
   }

   
    $('.nav-pills').tabdrop();
    
    $scope.addPackages = function(e) {
    	
        Command: toastr["info"]("Packages Adding Please Wait");
        
    var packageData = "package_cost/" + $scope.package_cost + "/";
    	packageData += "package_name/" + $scope.package_name + "/";
        packageData += "package_details/" + $scope.package_details + "/";
        packageData += "is_postpaid/" + $scope.is_postpaid + "/";
        packageData += "watt_id/" + $scope.wattage + "/";
        packageData += "unit/" + $scope.unit + "/";
        packageData += "extra_charges/" + $scope.extra_charges + "/";
        packageData += "state_id/" + $scope.state_id + "/";
        packageData += "site_id/" + $scope.site_id + "/";
        packageData += "feeder_id/" + $scope.site_meter_id + "/";
        packageData += "consumer_type/" + $scope.consumer_type + "/";
        packageData += "light_load/" + $scope.light_load + "/";
		packageData += "security_deposit/" + $scope.security_deposit + "/"; 
         
       /* $scope.package_cost="";  $scope.package_details=""; $scope.is_postpaid="";$scope.wattage="";
		$scope.unit="";$scope.extra_charges=""; $scope.consumer_type="";$scope.light_load="";$scope.package_name="";
		$("#Multiple_State").select2("val", "");
		$("#Multiple_Site").select2("val", "");
		$("#Multiple_feeder").select2('val', '');
		$("#Multiple_feeder_type").select2('val', '');
		$("#Multiple_description").select2('val', '');*/
        $http.post(baseUrl + "api/packages/add-Package/" + packageData)
                .success(function(response) {
				  
                    if (response.meta.code == "200") {
                        Command: toastr["success"]("Packages have been Added");
                        /*$http.get(baseUrl + "api/packages/get-all-packages/status/true")
                        .success(function(response) {
                            $scope.packages = response.data;
                        });*/
                    } else {
                        Command: toastr["error"]("Duplicate package Name, Please try again.");
                    }
                });
    }
    
    $scope.deletePackage = function(package) {
        Command: toastr["info"]("Package Deleting Please Wait");
        var modalInstance = $modal.open({
            templateUrl: 'deletePackage.html',
            controller: 'ModalPackagesCtrl',
            resolve: {
                selectedPackage: function() {
                    return package;
                }
            }
        });

        modalInstance.result.then(function(selectedPackage) {

            $scope.selectedPackage = selectedPackage;
            $scope.confirmDelete(selectedPackage);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }
    
    $scope.confirmDelete = function(package) {
        $http.post(baseUrl + "api/packages/delete-package-by-id/id/" + package.package_id)
                .success(function(response) {
                	if (response.meta.code == "200") {
                    	Command: toastr["success"]("Package has been Disabled");
	                    $http.get(baseUrl + "api/packages/get-all-packages/status/true")
	                    .success(function(response) {
	                        $scope.packages = response.data;
							$rootScope.Allpackages = response.data;
							$scope.filterSites();
	                    });
                    } else {
                        Command: toastr["error"]("Error occured while updating. Please try again.");
                    }
       });

    }

    $scope.editPackage = function(package) {

        var modalInstance = $modal.open({
            templateUrl: 'editPackage.html',
            controller: 'ModalPackagesCtrl',
            resolve: {
                selectedPackage: function() {
                $packages=angular.copy(package);
               	 	return $packages;
                }
            }
        });
        modalInstance.result.then(function(selectedPackage) {
            $scope.selectedPackage = selectedPackage;
            $scope.updatePackage(selectedPackage);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
        setTimeout(function () {
        	$('#editState').select2({
	            placeholder: "Select States",
	            allowClear: true
	        });
        	$('#editSite').select2({
	            placeholder: "Select Sites",
	            allowClear: true,
				closeOnSelect:false
	        });
        	$('#editFeeder').select2({
	            placeholder: "Select Feeder",
	            allowClear: true,
	            closeOnSelect:false
	        });
        },200);
    };
    $scope.submitForm = function(isValid) {
        if (isValid) 
        {
            $scope.addPackages();
        }
        else
        {
            Command: toastr["error"]("Error while validating form");
        }

    };
     
     
    $scope.updatePackage = function(package) { 
        Command: toastr["info"]("Package has been updating Please Wait");
        var editState= $('#editState').select2('val');
	    var editSite= $('#editSite').select2('val');
	    var editFeeder= $('#editFeeder').select2('val') ;
        var packageData = "package_cost/" + package.package_cost + "/";
	    	packageData += "package_name/" + package.package_shot + "/";
	        packageData += "package_details/" + package.package_details + "/";
	        packageData += "is_postpaid/" + package.is_postpaid_value + "/";
	        packageData += "watt_id/" + package.wattage + "/";
	        packageData += "unit/" + package.unit + "/";
	        packageData += "extra_charges/" + package.extra_charges + "/";
	        packageData += "state_id/" + editState + "/"; 
	        packageData += "site_id/" + editSite + "/";
	        packageData += "feeder_id/" + editFeeder + "/";
	        packageData += "consumer_type/" + package.connection_type + "/";
	        packageData += "light_load/" + package.light_load + "/";
	        packageData += "status/" + package.status+ "/";
			packageData += "security_deposit/" + package.security_deposit + "/";
			   
	    $http.post(baseUrl + "api/packages/update-package-by-id/id/" + package.package_id + "/" + packageData).success(function(response) {
            if (response.meta.code == "200") {
                Command: toastr["success"]("Package has been updated");
            	
             $http.get(baseUrl + "api/packages/get-all-packages/status/true")
            		.success(function(response) {
            			$scope.packages = response.data;
						$rootScope.Allpackages = response.data;
						$scope.filterSites();
            		}); 
            }
            else{
                Command: toastr["error"]("Something went wrong, Please try again.");
            }
	    });
    }
    
    $scope.Equal = function(prop, val){
        return function(item){
          if (item[prop] == val) return true;
        }
      }
    
});

huskApp.controller('ModalPackagesCtrl', function($scope,$http, $modalInstance, selectedPackage, $rootScope) {
    $scope.selectedPackage = selectedPackage;
    $scope.sites=$rootScope.selectedSite;
    $scope.states=$rootScope.AllStates;
    $scope.site_meters=$rootScope.meter_id;
    $scope.consumerTypes=$rootScope.AllConsumerTypes;
    var splitState=$scope.selectedPackage.state_id.split(",");
    var splitSite=$scope.selectedPackage.site_id.split(",");
    var splitFeeder=$scope.selectedPackage.site_meter_id.split(",");
    setTimeout(function () {
	    $("#editState").select2("val", splitState);
	    $("#editSite").select2("val", splitSite);
	    $("#editFeeder").select2("val", splitFeeder);
	 },200);
    $http.post(baseUrl + "api/wattage/get-wattage-by-multiple-site-id/site_id/" + $scope.selectedPackage.site_id)
		.success(function(response) {
			$scope.wattages = response.Allwatt;
		});
    
    $scope.ok = function() {
        $modalInstance.close($scope.selectedPackage);
    };
    
    $scope.updateSites=function(){
	   	if(($scope.state_id).length==0){
	   		$scope.sites = $rootScope.allSites;
	   	}else{
		    var site_array=$rootScope.allSites; 
		    var siteData= $.grep(site_array, function(n) {
		    	return ($scope.state_id).indexOf(n.state_id) != -1;
			});
		    $scope.sites = siteData;
	   	}
   }
    
   $scope.getFeederWattage= function(){
   		$http.post(baseUrl + "api/wattage/get-wattage-by-multiple-site-id/site_id/" + $scope.site_id)
   			.success(function(response) {
   				$scope.wattages = response.Allwatt;
		});

   	    if(($scope.site_id).length==0){
	    	Command: toastr["error"]("There is no master meter on this site.");
	   	}else{
		    var meter_array=$rootScope.meter_id; 
		    var meterData= $.grep(meter_array, function(n) {
		    	return ($scope.site_id).indexOf(n.site_id) != -1;
			});
		    $scope.site_meters = meterData;
	   	}
	 }
   
      
   	$scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.confirm = function() {
        $modalInstance.close($scope.selectedPackage);
    };
});