var huskApp = angular.module('huskApp');

huskApp.controller("CountryController", function($scope, $http , $log) {

    $http.get(baseUrl + "api/state/get-all-states")
            .success(function(response) {
                $scope.states = response.data;
            });

    $scope.addState = function(e) {
        Command: toastr["info"]("State Adding Please Wait");
        var stateData = "state_name/" + $scope.state_name + "/";
        $http.post(baseUrl + "api/state/add-state/" + stateData)
                .success(function(response) {
                    if (response.meta.code == "200") {
                        var new_state = {
                        		state_id: response.data.state_id,
                        		state_name:response.data.state_name,
                        };
                        Command: toastr["success"]("State has been Added");
                        $scope.states.unshift(new_state);
                    } else {
                        Command: toastr["danger"]("Opps Somthing Went Worng");
                    }
                });
    }

  
    $scope.ValidateForm = function() {
		
		var country_name=document.getElementById("country_name").value; 
		if( country_name==undefined || country_name=="" || country_name=='NULL' ){
			document.getElementById("country_validation").innerHTML = "This field is required";
			document.getElementById("country_name").focus();
				return false;
		}else{
			document.getElementById("country_validation").innerHTML = "";
		}
		$scope.addState();
   };
    

    $scope.editState = function(state) {
        var modalInstance = $modal.open({
            templateUrl: 'editState.html',
            controller: 'ModalCountryCtrl',
            resolve: {
                selectedState: function() {
                    $states=angular.copy(state);
                    return $states;

                }
            }
        });
        modalInstance.result.then(function(selectedState) {
            $scope.selectedState = selectedState;
            $scope.updateState(selectedState);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.updateState = function(state) {
        Command: toastr["info"]("State has been updating Please Wait");
        var statedata = "state_name/" + state.state_name + "/";
     
        $http.post(baseUrl + "api/state/update-state-by-id/id/" + state.state_id + "/" + statedata).success(function(response) {
        	
            if (response.meta.code == "200") {
            	 $http.get(baseUrl + "api/state/get-all-states").success(function(response) {
            		 $scope.states = response.data; 
                 });
                Command: toastr["success"]("State has been updated");
            }
            else
            {
                Command: toastr["error"]("Error occured while updating. Please try again.");
            }

        });

    }
    
    $scope.deleteState = function(state) {
        Command: toastr["info"]("State Deleting Please Wait");
        var modalInstance = $modal.open({
            templateUrl: 'deleteState.html',
            controller: 'ModalCountryCtrl',
            resolve: {
            	selectedState: function() {
                    return state;
                }
            }
        });

        modalInstance.result.then(function(selectedState) {

            $scope.selectedState = selectedState;
            $scope.confirmDelete(selectedState);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }
    $scope.confirmDelete = function(state) {
        $http.post(baseUrl + "api/state/delete-state-by-id/id/" + state.state_id)
                .success(function(response) {

                    if (response.meta.code == "200") {
                        var position = $scope.states.indexOf(state);

                        if (position > -1) {
                            $scope.states.splice(position, 1);
                        }
                        Command: toastr["success"]("State has been deleted");
                    } else {
                        Command: toastr["error"]("Error occured while deleting. Please try again.");
                    }

                });
    }
});


huskApp.controller('ModalCountryCtrl', function($scope, $modalInstance, selectedState) {
    $scope.selectedState = selectedState;
    $scope.ok = function() {
        $modalInstance.close($scope.selectedState);
    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.confirm = function() {
        $modalInstance.close($scope.selectedState);
    };
});
