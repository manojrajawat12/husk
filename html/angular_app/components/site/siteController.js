var huskApp = angular.module('huskApp');


huskApp.controller("siteController", function($scope, $http, $log,$rootScope) {
    $scope.validate_site_name = false;
document.title = "Sites";
   $http.get(baseUrl + "api/sites/get-all-sites-for-site-page")
            .success(function(response) {
                $scope.sites = response.data;
                $rootScope.allsites= response.data;
   });
   
	$scope.init=function(){
		$http.get(baseUrl + "default/auth/auth-data")
		 .success(function(response) {
			$scope.userRole = response.data.userRole;
			$scope.state_id=response.data.state_id;
			//$scope.clusters=response.data.clusters;
			//$scope.sites=response.data.sites;
			$scope.subMenus= response.data.subMenu_id;
	});
}

$scope.init();
  
    var allClusters=[];
    $http.get(baseUrl + "api/state/get-all-states")
    .success(function(response) {
        $scope.states = response.data;
    });
    $http.get(baseUrl + "api/clusters/get-all-clusters")
            .success(function(response) {
            	allClusters=response.data;
                $scope.clusters = response.data;
          });
    
    
    $http.get(baseUrl + "api/sites/get-all-charges")
    .success(function(response) {
    		$scope.otpCharges = response.data;
    		$rootScope.otp_Charges = response.data;
    });
    
    $scope.getClusters=function(e){
    	var cluster_array=allClusters; 
    	var clusterData= $.grep(cluster_array, function(n) {
	      	return n.state_id == $scope.state_id;
	    });
    	$scope.clusters = clusterData;
        Command: toastr["success"]("Clusters have been loaded");
    }
    
    
    $scope.addSite = function() {
        if(parseInt($scope.x) < parseInt($scope.y)){
            Command: toastr["error"]("Value of Y can't be greater than X.");
            return false;
        }
        var siteData = "id/" + $scope.cluster_name + "/";
        siteData += "num_of_poles/" + $scope.num_of_poles + "/";
        siteData += "site_code/" + $scope.site_code + "/";
        siteData += "name/" + $scope.site_name+ "/";
        siteData += "state_id/" + $scope.state_id+ "/";
        siteData += "x/" + $scope.x+ "/";
        siteData += "y/" + $scope.y+ "/";
        siteData += "size_of_plant/" + $scope.size_of_plant+ "/";
        siteData += "Solar_panel_size/" + $scope.Solar_panel_size+ "/";
        siteData += "Solar_panel_no/" + $scope.Solar_panel_no+ "/";
        siteData += "Charges_controller_size/" + $scope.Charges_controller_size+ "/";
        siteData += "Charges_controller_no/" + $scope.Charges_controller_no+ "/";
        siteData += "Inverter_size/" + $scope.Inverter_size+ "/";
        siteData += "Inverter_no/" + $scope.Inverter_no+ "/";
        siteData += "Battery_size/" + $scope.Battery_size+ "/";
        siteData += "Battery_no/" + $scope.Battery_no+ "/";
        siteData += "otp_act_id/" + $scope.act_charges+ "/";
        siteData += "otp_mtr_id/" + $scope.mtr_charges+ "/";

        $http.post(baseUrl + "api/sites/add-site/" + siteData)
                .success(function(response) {
                    if (response.meta.code == "200") {
                        $('#addSiteForm')[0].reset();
                        var new_site = {
                            cluster_name: response.data.cluster_name,
                            cluster_id: response.data.cluster_id,
                            site_name: response.data.site_name,
                            site_id: response.data.site_id,
                            site_code: response.data.site_code,
                            num_of_poles: response.data.num_of_poles,
                            state_id: response.data.state_id,
                            state_name: response.data.state_name,
                            site_status: response.data.site_status,
                            x: response.data.x,
                            y: response.data.y,
                            size_of_plant: response.data.size_of_plant,
                            Solar_panel_size: response.data.Solar_panel_size,
                            Solar_panel_no: response.data.Solar_panel_no,
                            Charges_controller_size: response.data.Charges_controller_size,
                            Charges_controller_no: response.data.Charges_controller_no,
                            Inverter_size: response.data.Inverter_size,
                            Inverter_no: response.data.Inverter_no,
                            Battery_size: response.data.Battery_size,
                            Battery_no: response.data.Battery_no,
                            act_charges: response.data.act_charges,
                            mtr_charges: response.data.mtr_charges,
                        };
                         Command: toastr["success"]("Sites have been Added");

                        $scope.sites.unshift(new_site);
                    } else {
                    	Command: toastr["error"]("Opps Somthing Went Worng");
                    }
                });
    }
    $scope.deleteSite = function(site) {
    	
        var modalInstance = $modal.open({
            templateUrl: 'deleteSite.html',
            controller: 'ModalSiteCtrl',
            resolve: {
                selectedSite: function() {
                    return site;
                }
            }
        });
        modalInstance.result.then(function(site) {

            $scope.selectedSite = site;
            $scope.confirmDelete(site);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }
    $scope.confirmDelete = function(site) {
    	 Command: toastr["info"]("Site deleting please Wait");
        $http.post(baseUrl + "api/sites/delete-site-by-id/id/" + site.site_id)
                .success(function(response) {

                    if (response.meta.code == "200") {

                       

                        var position = $scope.sites.indexOf(site);

                        if (position > -1) {

                            $scope.sites.splice(position, 1);
                        }

                        Command: toastr["success"]("Site has been deleted");
                    } else {
                        Command: toastr["error"]("Site has been updated");
                    }

                });

    }

    $scope.editSite = function(site) {
        
       
        var modalInstance = $modal.open({
            templateUrl: 'editSite.html',
            controller: 'ModalSiteCtrl',
            resolve: {
                selectedSite: function() {
                    return site;
                }
            }
        });

        modalInstance.result.then(function(selectedSite) {
            $scope.selectedSite = selectedSite;
            $scope.updateSite(selectedSite);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });



    };
    $scope.submitForm = function(isValid) {
        if (isValid) 
        {    
            $scope.addSite();
        }
        else
        {
            Command: toastr["error"]("Error while validating form");
        }

    };
    $scope.updateSite = function(site) {
        var sitedata = "name/" + site.site_name + "/";
        sitedata += "num_of_poles/" + site.num_of_poles + "/";
        sitedata += "site_code/" + site.site_code + "/";
        sitedata += "cluster_id/" + site.cluster_id+ "/";
        sitedata += "state_id/" + site.state_id+ "/";
        sitedata += "site_status/" + site.site_status+ "/";
        sitedata += "site_order/" + site.site_order+ "/";
        sitedata += "x/" + site.x+ "/";
        sitedata += "y/" + site.y+ "/";
        sitedata += "size_of_plant/" + site.size_of_plant+ "/";
        sitedata += "Solar_panel_size/" + site.Solar_panel_size+ "/";
        sitedata += "Solar_panel_no/" + site.Solar_panel_no+ "/";
		sitedata += "Solar_panel_kw/" + site.Solar_panel_kw+ "/";
        sitedata += "Charges_controller_size/" + site.Charges_controller_size+ "/";
        sitedata += "Charges_controller_no/" + site.Charges_controller_no+ "/";
        sitedata += "Inverter_size/" + site.Inverter_size+ "/";
        sitedata += "Inverter_no/" + site.Inverter_no+ "/";
        sitedata += "Battery_size/" + site.Battery_size+ "/";
        sitedata += "Battery_no/" + site.Battery_no+ "/";
		sitedata += "Battery_no_v/" + site.Battery_no_v+ "/";
        sitedata += "otp_act_id/" + site.act_charges+ "/";
        sitedata += "otp_mtr_id/" + site.mtr_charges+ "/";
		 
		sitedata += "dg_capacity/" + site.dg_capacity+ "/";
        sitedata += "dg_no/" + site.dg_no+ "/";
        sitedata += "bio_capacity/" + site.bio_capacity+ "/";
        sitedata += "bio_no/" + site.bio_no+ "/";
        sitedata += "radiation/" + site.radiation+ "/";
		sitedata += "site_target/" + site.site_target+ "/"; 
        sitedata += "plant_duration/" + site.plant_duration+ "/";
        sitedata += "plant_target/" + site.plant_target+ "/";
		sitedata += "array_no/" + site.array_no+ "/";
        sitedata += "array_capacity/" + site.array_capacity+ "/";
        sitedata += "day_factor/" + site.day_factor+ "/";
		sitedata += "night_factor/" + site.night_factor+ "/";
		
        Command: toastr["info"]("Site Updating Please Wait");
        $http.post(baseUrl + "api/sites/update-site-by-id/id/" + site.site_id + "/" + sitedata).success(function(response) {
            if (response.meta.code == "200") {
	           	$http.get(baseUrl + "default/auth/change-data")
	      	   	  .success(function(response) {
	      	   	  });
      	   	  
              	$http.get(baseUrl + "api/sites/get-all-sites-for-site-page")
                    .success(function(response) {
                        $scope.sites = response.data;
                });
            	Command: toastr["success"]("Site has been updated");
            }
            else
            {
                Command: toastr["error"]("Error occured while updating. Please try again.");
            }
        });
    }
});

huskApp.controller('ModalSiteCtrl', function($scope, $http, $modalInstance, selectedSite,$rootScope) {
    $scope.selectedSite = selectedSite;
    $scope.sites=$rootScope.allsites;
    $scope.otpCharges=$rootScope.otp_Charges;
    $http.get(baseUrl + "api/state/get-all-states")
    .success(function(response) {
        $scope.states = response.data;
    });
    
    var allClusters=[];
    $http.get(baseUrl + "api/clusters/get-all-clusters")
            .success(function(response) {
            	allClusters=response.data;
            	$scope.getClusters();
            	/*$scope.clusters = response.data;
                $scope.setCluster();*/
    });
    $scope.getClusters=function(e){
    	var cluster_array=allClusters; 
    	var clusterData= $.grep(cluster_array, function(n) {
	      	return n.state_id == selectedSite.state_id;
	    });
    	$scope.clusters = clusterData;
     }
   /* $scope.setCluster = function()
    {
        var result = $.grep($scope.clusters, function(e){ return e.cluster_id == $scope.selectedSite.cluster_id; });
        var position = $scope.clusters.indexOf(result[0]);
        $scope.selectedCluster = $scope.clusters[position];
        console.log(position);
    };*/
    $scope.ok = function() {
        if(parseInt($scope.selectedSite.x) < parseInt($scope.selectedSite.y)){
            Command: toastr["error"]("Value of Y can't be greater than X.");
            return false;
        }
        $modalInstance.close($scope.selectedSite);
    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.confirm = function() {
        $modalInstance.close($scope.selectedSite);
    };
   /* $scope.changeCluster = function(){
        console.log($scope.selectedCluster.cluster_name);
        $scope.selectedSite.cluster_name = $scope.selectedCluster.cluster_name;
        $scope.selectedSite.cluster_id = $scope.selectedCluster.cluster_id;
    }*/
});

