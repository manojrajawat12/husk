var huskApp = angular.module('huskApp');


huskApp.controller("userRoleController", function($scope,$http,$rootScope, $modal, $log) {
    $http.get(baseUrl + "api/user-role/get-all-user-roles")
            .success(function(response) {
                $scope.userRoles = response.data;
            });
   
    $http.get(baseUrl + "api/sites/get-all-sites")
    .success(function(response) {
        $rootScope.allSites=response.data;
    });
    
    $http.get(baseUrl + "api/state/get-all-states")
    .success(function(response) {
    	$scope.states = response.data;
        $rootScope.allState=response.data;
    });
   
    $http.get(baseUrl + "api/clusters/get-all-clusters/allCluster/"+true)
            .success(function(response) {
                $rootScope.allClusters=response.data;
          });
   
    $http.get(baseUrl + "api/user-role/get-all-menu-by-type/menu_type/menu")
    	.success(function(response) {
    		$scope.menus = response.data;
    		$rootScope.allMenus=$scope.menus;
    });
   
    $http.get(baseUrl + "api/user-role/get-all-menu-by-type/menu_type/sub_menu" )
    	.success(function(response) {
    		$rootScope.allSubMenu=response.data;
    });
 
    $rootScope.menu_id_val=[];
    $scope.getMenues=function(e){
    	var menu_array=$rootScope.allSubMenu; 
		if($scope.menu_id==undefined)
    		{
				$scope.subMenus="";
				setTimeout(function(){
					$("#Multiple_subMenu").select2("val", $scope.subMenus);
		    	}, 100);
				$scope.getsubMenues($scope.subMenus);
    		}
    	else{
		    	var menuData= $.grep(menu_array, function(n) {
		    		return ($scope.menu_id).indexOf(n.parent_menu) != -1;
		    	});
		    	$scope.subMenus = menuData;
		    	 
		    	var total=menuData.length;
		    	$rootScope.sel_subMenu_id=[];
		    	for(var i=0;i<total;i++){
		    		$rootScope.sel_subMenu_id[i]=menuData[i].menu_id;
		    	}
		    	var index;
	 	    	if(($rootScope.menu_id_val).length>0){
	 	    	for(var i=0;i<($rootScope.menu_id_val).length;i++){
	 		        index = $rootScope.sel_subMenu_id.indexOf($rootScope.menu_id_val[i]);
	 	    		if (index > -1) {
	 	    			$rootScope.sel_subMenu_id.splice(index, 1);
	 	    		}
	 			  }
	 	    	}
		    	// var checkclusters=($scope.menu_id).indexOf(menu_val); 	
			 	    if(menu_val!=0){
			     	var clusterData= $.grep(menu_array, function(n) {
			 		    		return (menu_val).indexOf(n.parent_menu) != -1;
			 		    	});
			     	
			 	    	var checkclusterData= $.grep(clusterData, function(n) {
			 	    		return ($rootScope.menu_id_val).indexOf(n.menu_id) != -1;
			 	    	});
			 	    	//menu_val=0;
			 	    	if(checkclusterData.length>0){
			 	    		var total=checkclusterData.length;
					    	$rootScope.select_subMenu_id;
					    	for(var k=0;k<total;k++){
					    		$rootScope.select_subMenu_id=checkclusterData[k].menu_id;
					    		$rootScope.sel_subMenu_id.push($rootScope.select_subMenu_id);
					    		index = $rootScope.menu_id_val.indexOf($rootScope.select_subMenu_id);
				 	    		if (index > -1) {
				 	    			$rootScope.menu_id_val.splice(index, 1);
				 	    		}
					    	}
			 	    	}else{var index;
			 		    	if(($rootScope.menu_id_val).length>0){
			 		    	for(var i=0;i<($rootScope.menu_id_val).length;i++){
			 			        index = $rootScope.sel_subMenu_id.indexOf($rootScope.menu_id_val[i]);
			 		    		if (index > -1) {
			 		    			$rootScope.sel_subMenu_id.splice(index, 1);
			 		    		}
			     			  }
			 		    	}}
			 		    
			 		}else{
			 			var index;
			 	    	if(($rootScope.menu_id_val).length>0){
			 	    	for(var i=0;i<($rootScope.menu_id_val).length;i++){
			 		        index = $rootScope.sel_subMenu_id.indexOf($rootScope.menu_id_val[i]);
			 	    		if (index > -1) {
			 	    			$rootScope.sel_subMenu_id.splice(index, 1);
			 	    		}
			 			  }
			 	    	}
			 		}
		    	setTimeout(function(){
					$("#Multiple_subMenu").select2("val", $rootScope.sel_subMenu_id);
		    	}, 100);
		    	$scope.getsubMenues($rootScope.sel_subMenu_id);
    	   }
    }
    
    $scope.getsubMenues=function(subMenu_id){

    	var menu_array=$rootScope.allSubMenu;
    	$scope.childMenus =$rootScope.allSubMenu;
    	var j=0;$rootScope.sel_childMenu_id=[];
    	if(subMenu_id.length>0){
	    		var menuData= $.grep(menu_array, function(n) {
		    		return (subMenu_id).indexOf(n.parent_menu) != -1;
			    });
		    	
	    		var total=menuData.length;
		    	for(var k=0;k<total;k++){
		    		$rootScope.sel_childMenu_id[k]=menuData[k].menu_id;
		    		j++;
		    	}
		    	var index;
	 	    	if(($rootScope.menu_child_id_val).length>0){
	 	    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
	 		        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
	 	    		if (index > -1) {
	 	    			$rootScope.sel_childMenu_id.splice(index, 1);
	 	    		}
	 			  }
	 	    	}
		    	
		    if(menu_val!=0){
			     	var clusterData= $.grep(menu_array, function(n) {
			 		    		return (subMenu_id).indexOf(n.parent_menu) != -1;
			 		    	});
			     	
			 	    	var checkclusterData= $.grep(clusterData, function(n) {
			 	    		return ($rootScope.menu_child_id_val).indexOf(n.menu_id) != -1;
			 	    	});

			 	    	if(checkclusterData.length>0){
			 	    		var total=checkclusterData.length;
					    	$rootScope.select_subMenu_id;
					    	for(var k=0;k<total;k++){
					    		$rootScope.select_subMenu_id=checkclusterData[k].menu_id;
					    		$rootScope.sel_childMenu_id.push($rootScope.select_subMenu_id);
					    		index = $rootScope.menu_child_id_val.indexOf($rootScope.select_subMenu_id);
				 	    		if (index > -1) {
				 	    			$rootScope.menu_child_id_val.splice(index, 1);
				 	    		}
					    	}
			 	    	}else{var index;
			 		    	if(($rootScope.menu_child_id_val).length>0){
			 		    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
			 			        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
			 		    		if (index > -1) {
			 		    			$rootScope.sel_childMenu_id.splice(index, 1);
			 		    		}
			     			  }
			 		    	}}
			 		    
			 		}else{
			 			var index;
			 	    	if(($rootScope.menu_child_id_val).length>0){
			 	    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
			 		        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
			 	    		if (index > -1) {
			 	    			$rootScope.sel_childMenu_id.splice(index, 1);
			 	    		}
			 			  }
			 	    	}
			 		}

	        menu_val=0;
	    	setTimeout(function(){
				$("#Multiple_childMenu").select2("val", $rootScope.sel_childMenu_id);
	    	}, 300);
	    	
    	}else{
    		$scope.childMenus="";
			setTimeout(function(){
				$("#Multiple_childMenu").select2("val", $scope.childMenus);
	    	}, 100)
    	}
    }
    
    $scope.checkSubMenue= function(){
    	var menu_array=$rootScope.allSubMenu; 
    	var sub_id=[];
    	 $("#Multiple_subMenu option").each(function()
    	   {	sub_id.push($(this).val());	});
    	    var diff = $(sub_id).not($scope.subMenu_id).get();
    	    $rootScope.menu_id_val=diff;
    	    
			var menuData= $.grep(menu_array, function(n) {
		    		return ($scope.subMenu_id).indexOf(n.parent_menu) != -1;
		    	});
		    	$scope.childMenus = menuData;
		    	
		    	var total=menuData.length;
		    	$rootScope.sel_childMenu_id=[];
		    	for(var i=0;i<total;i++){
		    		$rootScope.sel_childMenu_id[i]=menuData[i].menu_id;
		    	}
		    	var index;
 		    	if(($rootScope.menu_child_id_val).length>0){
 		    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
 			        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
 		    		if (index > -1) {
 		    			$rootScope.sel_childMenu_id.splice(index, 1);
 		    		}
     			  }
 		    	}
		    	if(subMenu_val!=0){
		     	var subMenu_Data= $.grep(menu_array, function(n) {
		 		    		return (subMenu_val).indexOf(n.parent_menu) != -1;
		 		    	});
		     	
		 	    	var checksubMenu_Data= $.grep(subMenu_Data, function(n) {
		 	    		return ($rootScope.menu_child_id_val).indexOf(n.menu_id) != -1;
		 	    	});

		 	    	subMenu_val=0;
		 	    	if(checksubMenu_Data.length>0){
		 	    		var total=checksubMenu_Data.length;
				    	$rootScope.select_subMenu_id;
				    	for(var k=0;k<total;k++){
				    		$rootScope.select_subMenu_id=checksubMenu_Data[k].menu_id;
				    		$rootScope.sel_childMenu_id.push($rootScope.select_subMenu_id);
				    		index = $rootScope.menu_child_id_val.indexOf($rootScope.select_subMenu_id);
			 	    		if (index > -1) {
			 	    			$rootScope.menu_child_id_val.splice(index, 1);
			 	    		}
				    	}
		 	    	}else{var index;
		 		    	if(($rootScope.menu_child_id_val).length>0){
		 		    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
		 			        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
		 		    		if (index > -1) {
		 		    			$rootScope.sel_childMenu_id.splice(index, 1);
		 		    		}
		     			  }
		 		    	}}
		 		    
		 		}else{
		 			var index;
		 	    	if(($rootScope.menu_child_id_val).length>0){
		 	    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
		 		        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
		 	    		if (index > -1) {
		 	    			$rootScope.sel_childMenu_id.splice(index, 1);
		 	    		}
		 			  }
		 	    	}
		 		}
		    	
		    	setTimeout(function(){
					$("#Multiple_childMenu").select2("val", $rootScope.sel_childMenu_id);
				}, 100);
		    	$scope.checkMenu();
    }
    $rootScope.menu_child_id_val=[];
    
	$scope.checkMenu=function(){
		    	var menu_array=$rootScope.allSubMenu; 
		    	var child_id=[];
		    	/* $("#Multiple_childMenu option").each(function()
		    	    	   {	child_id.push($(this).val());	});
		    	    	    var diff = $(child_id).not($scope.childMenu_id).get();
		    	    	    $rootScope.menu_child_id_val=diff;*/
		    	    	    
				var menuData= $.grep(menu_array, function(n) {
			    		return ($scope.subMenu_id).indexOf(n.menu_id) != -1;
			    	});
			    	
			    	var total=menuData.length;
			    	$rootScope.sel_main_menu_id=[];
			    	for(var i=0;i<total;i++){
			    		$rootScope.sel_main_menu_id[i]=menuData[i].parent_menu;
			    	}
			    	setTimeout(function(){
						$("#Multiple_Menu").select2("val", $rootScope.sel_main_menu_id);
			    	}, 100);
	 }

  $scope.checkMainMenu=function(){
     var menu_array=$rootScope.allSubMenu;
     var child_id=[];
     var childs_id=$("#Multiple_childMenu").select2('val');
	   	 $("#Multiple_childMenu option").each(function()
	   	    {	child_id.push($(this).val());	});
	   	      var diff = $(child_id).not($scope.childMenu_id).get();
	   	      $rootScope.menu_child_id_val=diff;
  }     
	  /*   var sub_id=$("#Multiple_subMenu").select2('val');
	  	 $("#Multiple_subMenu option").each(function()
	  	   {	sub_id.push($(this).val());	});
	  	    var diff = $(sub_id).not($scope.subMenu_id).get();
	  	    $rootScope.menu_id_val=diff;
  	    

	  	    var menuData= $.grep(menu_array, function(n) {
		  		return ($scope.childMenu_id).indexOf(n.menu_id) != -1;
		  	});
	  	  
	  	$scope.childMenus = menuData;
	  	
	  	var total=menuData.length;
	  	$rootScope.sel_childMenu_id=[];
	  	for(var i=0;i<total;i++){
	  		$rootScope.sel_childMenu_id[i]=menuData[i].parent_menu;
	  	}
	  	var index;
	   	if(($rootScope.menu_child_id_val).length>0){
	   	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
		        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
	   		if (index > -1) {
	   			$rootScope.sel_childMenu_id.splice(index, 1);
	   		}
			  }
	   	}
	  	if(child_val!=0){
	   	var subMenu_Data= $.grep(menu_array, function(n) {
			    		return (child_val).indexOf(n.parent_menu) != -1;
			    	});
	   	
		    	var checksubMenu_Data= $.grep(subMenu_Data, function(n) {
		    		return ($rootScope.menu_child_id_val).indexOf(n.menu_id) != -1;
		    	});
		    		
		    	child_val=0;
		    	if(checksubMenu_Data.length>0){
		    		var total=checksubMenu_Data.length;
			    	$rootScope.select_subMenu_id;
			    	for(var k=0;k<total;k++){
			    		$rootScope.select_subMenu_id=checksubMenu_Data[k].menu_id;
			    		$rootScope.sel_childMenu_id.push($rootScope.select_subMenu_id);
			    		index = $rootScope.menu_child_id_val.indexOf($rootScope.select_subMenu_id);
		 	    		if (index > -1) {
		 	    			$rootScope.menu_child_id_val.splice(index, 1);
		 	    		}
			    	}
		    	}else{var index;
			    	if(($rootScope.menu_child_id_val).length>0){
			    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
				        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
			    		if (index > -1) {
			    			$rootScope.sel_childMenu_id.splice(index, 1);
			    		}
	   			  }
			    	}}
			    
			}else{

				$rootScope.sel_childMenu_id.push(diff);
				var index;
		    	if(($rootScope.menu_child_id_val).length>0){
		    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
			        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
		    		if (index > -1) {
		    			$rootScope.sel_childMenu_id.splice(index, 1);
		    		}
				  }
		    	}
			}
  	
  	setTimeout(function(){
			$("#Multiple_subMenu").select2("val", $rootScope.sel_childMenu_id);
		}, 100);
  		$scope.checkMenubyChild($rootScope.sel_childMenu_id);
}
    $scope.checkMenubyChild = function(uniqueNames){
    	var menu_array=$rootScope.allSubMenu; 
		var menuData= $.grep(menu_array, function(n) {
	    		return (uniqueNames).indexOf(n.menu_id) != -1;
	    	});
	    	
	    	var total=menuData.length;
	    	$rootScope.sel_main_menu_id=[];
	    	for(var i=0;i<total;i++){
	    		$rootScope.sel_main_menu_id[i]=menuData[i].parent_menu;
	    	}
	    	setTimeout(function(){
				$("#Multiple_Menu").select2("val", $rootScope.sel_main_menu_id);
	    	}, 100);
    }*/
   /* $scope.getClusters=function(e){
    	var cluster_array=$rootScope.allClusters; 
		if($scope.state_id==undefined)
    		{$scope.clusters="";
    		}
    	else{
    	var clusterData= $.grep(cluster_array, function(n) {
    		return ($scope.state_id).indexOf(n.state_id) != -1;
	    });
    	$scope.clusters = clusterData;
		}
    }*/
    //$scope.state_val=0;
	
	
	$rootScope.site_id_val=[];
	$rootScope.cluster_id_val=[];
    
	$scope.getClusters=function(e){
    	var cluster_array=$rootScope.allClusters;
    	var states_id=[];

    	if($scope.state_id==undefined)
    		{
				$scope.clusters="";
				setTimeout(function(){
					$("#Multiple_Cluster").select2("val", $scope.clusters);
		    	}, 100);
				$scope.getAllSites($scope.clusters);
    		}
    	else{
    		var clusterData= $.grep(cluster_array, function(n) {
        		return ($scope.state_id).indexOf(n.state_id) != -1;
    	    });
        	$scope.clusters = clusterData;
		    	 
		    	var total=clusterData.length;
		    	$rootScope.sel_cluster_id=[];
		    	for(var i=0;i<total;i++){
		    		$rootScope.sel_cluster_id[i]=clusterData[i].cluster_id;
		    	}
		    	var index;
 		    	if(($rootScope.cluster_id_val).length>0){
 		    	for(var i=0;i<($rootScope.cluster_id_val).length;i++){
 			        index = $rootScope.sel_cluster_id.indexOf($rootScope.cluster_id_val[i]);
 		    		if (index > -1) {
 		    			$rootScope.sel_cluster_id.splice(index, 1);
 		    		}
     			  }
 		      }
		    	 if(state_val!=0){
		     	    var clusterData= $.grep(cluster_array, function(n) {
		 		    		return (state_val).indexOf(n.state_id) != -1;
		 		    	});
		     	
		 	    	var checkclusterData= $.grep(clusterData, function(n) {
		 	    		return ($rootScope.cluster_id_val).indexOf(n.cluster_id) != -1;
		 	    	});
		 	    	
		 	    	if(checkclusterData.length>0){
		 	    		var total=checkclusterData.length;
		 		    	$rootScope.select_cluster_id;
		 		    	for(var k=0;k<total;k++){
		 		    		$rootScope.select_cluster_id=checkclusterData[k].cluster_id;
		 		    		$rootScope.sel_cluster_id.push($rootScope.select_cluster_id);
		 		    		index = $rootScope.cluster_id_val.indexOf($rootScope.select_cluster_id);
		 	 	    		if (index > -1) {
		 	 	    			$rootScope.cluster_id_val.splice(index, 1);
		 	 	    		}
		 		    	}
		 	    	}
		 	    	else{
		 	    		var index;
		 		    	if(($rootScope.cluster_id_val).length>0){
		 		    	for(var i=0;i<($rootScope.cluster_id_val).length;i++){
		 			        index = $rootScope.sel_cluster_id.indexOf($rootScope.cluster_id_val[i]);
		 		    		if (index > -1) {
		 		    			$rootScope.sel_cluster_id.splice(index, 1);
		 		    		}
		     			  }
		 		      }
		 		   }
		 		  }else{
		 			  
		 			var index;
		 	    	if(($rootScope.cluster_id_val).length>0){
		 	    	for(var i=0;i<($rootScope.cluster_id_val).length;i++){
		 		        index = $rootScope.sel_cluster_id.indexOf($rootScope.cluster_id_val[i]);
		 	    		if (index > -1) {
		 	    			$rootScope.sel_cluster_id.splice(index, 1);
		 	    		}
		 			  }
		 	    	}
		 		}
		    	setTimeout(function(){
					$("#Multiple_Cluster").select2("val", $rootScope.sel_cluster_id);
		    	}, 100);
		    	$scope.getAllSites($rootScope.sel_cluster_id);
    	   }
    }
  
	  $scope.getAllSites =function(cluster_id){
	    	var site_array=$rootScope.allSites;
	    	$scope.sites =$rootScope.allSites;
	    	var j=0;$rootScope.sel_site_id=[];
	    	/*var site_value= $("#Multiple_Site").select2('val');
	    	var sites_id=[];
		   	 $("#Multiple_Site option").each(function()
		   	{	sites_id.push($(this).val());	});
		   	
		   	var diff = $(sites_id).not(site_value).get();
		   	$rootScope.site_id_val=diff;*/
		   	
	    	if(cluster_id.length>0){
	    		var siteData= $.grep(site_array, function(n) {
			    		return (cluster_id).indexOf(n.cluster_id) != -1;
				    });
			    	
		    		var total=siteData.length;
			    	for(var k=0;k<total;k++){
			    		$rootScope.sel_site_id[k]=siteData[k].site_id;
			    		
			    	}
			    	
			    	var index;
			    	if(($rootScope.site_id_val).length>0){
			    	for(var m=0;m<($rootScope.site_id_val).length;m++){
				        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[m]);
			    		if (index > -1) {
			    			$rootScope.sel_site_id.splice(index, 1);
			    		}
					  }
			    	
				  }
			    	
			    
		        if(state_val!=0){
			    	
		        	var chclusterData= $.grep(site_array, function(n) {
			    		return (state_val).indexOf(n.state_id) != -1;
			    	});
		    	 	var checkclusterData= $.grep(chclusterData, function(n) {
			    		return ($rootScope.site_id_val).indexOf(n.site_id) != -1;
			    	});
			    	
			    	   if(checkclusterData.length>0){
				    		var total=checkclusterData.length;
					    	$rootScope.select_site_id;
					    	for(var m=0;m<total;m++){
					    		$rootScope.select_site_id=checkclusterData[m].site_id;
					    		$rootScope.sel_site_id.push($rootScope.select_site_id);
					    		index = $rootScope.site_id_val.indexOf($rootScope.select_site_id);
				 	    		if (index > -1) {
				 	    			$rootScope.site_id_val.splice(index, 1);
				 	    		}
					    	}
				    	}
				    	else{
				    		var index;
					    	if(($rootScope.site_id_val).length>0){
					    	for(var m=0;m<($rootScope.site_id_val).length;m++){
						        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[m]);
					    		if (index > -1) {
					    			$rootScope.sel_site_id.splice(index, 1);
					    		}
							  }
					    	
						  }
					    }
			        
				  }
		    	else{
						var index;
				    	if(($rootScope.site_id_val).length>0){
				    	for(var m=0;m<($rootScope.site_id_val).length;m++){
					        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[m]);
				    		if (index > -1) {
				    			$rootScope.sel_site_id.splice(index, 1);
				    		}
						  }
				        }
				  }

		        state_val=0;
		    	setTimeout(function(){
					$("#Multiple_Site").select2("val", $rootScope.sel_site_id);
		    	}, 300);
		    	
	    	}else{
	    		$scope.sites="";
				setTimeout(function(){
					$("#Multiple_Site").select2("val", $scope.sites);
		    	}, 100)
	    	}
	    }

    
    $scope.getSites= function(){
    	var sites_array=$rootScope.allSites; 
    	var sites_id=[];
    	 $("#Multiple_Cluster option").each(function()
	   	{	sites_id.push($(this).val());	});
	   	
	   	var diff = $(sites_id).not($scope.cluster_id).get();
	   	$rootScope.cluster_id_val=diff;
    	var clusters_id=$scope.cluster_id;
    	
		
			var siteData= $.grep(sites_array, function(n) {
	    		return ($scope.cluster_id).indexOf(n.cluster_id) != -1;
	    	});
			//$scope.sites=siteData;
			
	    	var total=siteData.length;
	    	$rootScope.sel_site_id=[];
	    	$rootScope.sel_state_id=[];
	    	for(var i=0;i<total;i++){
	    		$rootScope.sel_site_id[i]=siteData[i].site_id;
	    		$rootScope.sel_state_id[i]=siteData[i].state_id;
	    	}
	    	var index;
	    	if(($rootScope.site_id_val).length>0){
	    	for(var i=0;i<($rootScope.site_id_val).length;i++){
		        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[i]);
	    		if (index > -1) {
	    			$rootScope.sel_site_id.splice(index, 1);
	    		}
			  }
	    	}
	    
    if(cluster_val!=0){
    	    var clusterData= $.grep(sites_array, function(n) {
		    		return (cluster_val).indexOf(n.cluster_id) != -1;
		    	});
    	
	    	var checkclusterData= $.grep(clusterData, function(n) {
	    		return ($rootScope.site_id_val).indexOf(n.site_id) != -1;
	    	});
	    	cluster_val=0;
	    	if(checkclusterData.length>0){
	    		var total=checkclusterData.length;
		    	$rootScope.select_site_id;
		    	for(var k=0;k<total;k++){
		    		$rootScope.select_site_id=checkclusterData[k].site_id;
		    		$rootScope.sel_site_id.push($rootScope.select_site_id);
		    		index = $rootScope.site_id_val.indexOf($rootScope.select_site_id);
	 	    		if (index > -1) {
	 	    			$rootScope.site_id_val.splice(index, 1);
	 	    		}
		    	}
	    	}
	    	else{
	    		var index;
		    	if(($rootScope.site_id_val).length>0){
		    	for(var i=0;i<($rootScope.site_id_val).length;i++){
			        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[i]);
		    		if (index > -1) {
		    			$rootScope.sel_site_id.splice(index, 1);
		    		}
    			  }
		      }
		   }
		  }else{
			  
			var index;
	    	if(($rootScope.site_id_val).length>0){
	    	for(var i=0;i<($rootScope.site_id_val).length;i++){
		        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[i]);
	    		if (index > -1) {
	    			$rootScope.sel_site_id.splice(index, 1);
	    		}
			  }
	    	}
		}
	   	setTimeout(function(){
			$("#Multiple_Site").select2("val", $rootScope.sel_site_id);
			$("#Multiple_State").select2("val", $rootScope.sel_state_id);
		}, 200);
	}
   
    $scope.checkCluster=function(){
    	
    	var sites_array=$rootScope.allSites; 
    	var sites_id=[];
    	 $("#Multiple_Site option").each(function()
    	{	sites_id.push($(this).val());	});
    	
    	var diff = $(sites_id).not($scope.site_id).get();
    	$rootScope.site_id_val=diff;
    	
    		var siteData= $.grep(sites_array, function(n) {
		    		return ($scope.site_id).indexOf(n.site_id) != -1;
		    	});
    		
    			//$scope.sites=siteData;
    			
    		 	var total=siteData.length;
		    	$rootScope.sel_site_id=[];$rootScope.sel_state_id=[];
		    	for(var i=0;i<total;i++){
		    		$rootScope.sel_site_id[i]=siteData[i].cluster_id;
		    		$rootScope.sel_state_id[i]=siteData[i].state_id;
		    	}
		    	setTimeout(function(){
					$("#Multiple_Cluster").select2("val", $rootScope.sel_site_id);
					$("#Multiple_State").select2("val", $rootScope.sel_state_id);
		    	}, 100);
		}


    $scope.addUserRole = function(e) {
        Command: toastr["info"]("UserRole Adding Please Wait");
    	var siteId=$scope.site_id;
    	$rootScope.menues=$scope.menu_id;
    /*	var child=$scope.childMenu_id;
    	if(child==""){child=null;}*/
    	var menu_id= $('#Multiple_Menu').select2('val');
	    var sub_menu_id= $('#Multiple_subMenu').select2('val');
	    var child_id= $('#Multiple_childMenu').select2('val') ;
	    var site_id= $('#Multiple_Site').select2('val') ;
	    if(menu_id==""){ menu_id=null;}
	    if(sub_menu_id==""){ sub_menu_id=null;}
	    if(child_id==""){ child_id=null;}
    	var userRoleData = "user_role/" + $scope.user_role + "/";
        userRoleData += "site_id/" + site_id + "/";
        userRoleData += "menu_id/" + menu_id + "/";
        userRoleData += "submenu_id/" + sub_menu_id + "/";
        userRoleData += "child_menu_id/" + child_id + "/";

        $http.post(baseUrl + "api/user-role/add-user-role/" + userRoleData)
                .success(function(response) {
                    if (response.meta.code == "200") {
                    	$('#userRole').val("");
                    	$("#Multiple_State").select2('val', 'All');
                    	$("#Multiple_Cluster").select2('val', 'All');
                    	$("#Multiple_Site").select2('val', 'All');
                    	$("#Multiple_Menu").select2('val', 'All');
                    	$("#Multiple_subMenu").select2('val', 'All');
                    	$("#Multiple_childMenu").select2('val', 'All');
                    	
                    	var new_Roles = {
                        		user_role_id: response.data.user_role_id,
                        		user_role:response.data.user_role,
                        };
                        Command: toastr["success"]("User Role has been Added");
                        $scope.userRoles.unshift(new_Roles);
                        
                    } else {
                        Command: toastr["danger"]("Opps Somthing Went Worng");
                    }
                });
    }

  
    $scope.submitForm = function(isValid) {
        if (isValid) 
        {
            $scope.addUserRole();
        }
        else
        {
            Command: toastr["error"]("Error while validating form");
        }

    };
    
    
    $scope.editUserRole = function(userRole) {
    	 
    	var modalInstance = $modal.open({
            templateUrl: 'editUserRole.html',
            controller: 'ModalUserRoleCtrl',
            
            resolve: {
                selectedUserRole: function() {
                    $UserRoles=angular.copy(userRole);
                    return $UserRoles;

                }
            }
        });
    	
        modalInstance.result.then(function(selectedUserRole) {
        	$scope.selectedUserRole = selectedUserRole;
        	$scope.updateUserRole(selectedUserRole);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
       
        setTimeout(function () {
        	 
        	$(document).ready(function() {
        	$('#editState').select2({
	            placeholder: "Select States",
	            allowClear: true
	        }).on("select2-selecting", function( e ) {
	        	$rootScope.edit_state_val=e.val;
	      	 });
        	$('#editCluster').select2({
	            placeholder: "Select Clusters",
	            allowClear: true
	        }).on("select2-selecting", function( e ) {
	        	$rootScope.edit_cluster_val=e.val;
	      	 });
        	$('#editSite').select2({
	            placeholder: "Select Sites",
	            allowClear: true,
	            closeOnSelect:false
	        }).on("select2-selecting", function( e ) {
	        $rootScope.edit_site_val=e.val;
	      	 });
        	$('#editMenu').select2({
	            placeholder: "Select Menu",
	            allowClear: true
	        }).on("select2-selecting", function( e ) {
	        	$rootScope.edit_menu_val=e.val;
	      	 });
        	$('#editSubMenu').select2({
	            placeholder: "Select SubMenu",
	            allowClear: true,
	            closeOnSelect:false
	        }).on("select2-selecting", function( e ) {
	        	$rootScope.edit_sub_val=e.val;
	      	 });
        	$('#editchildMenu').select2({
	            placeholder: "Select ChildMenu",
	            allowClear: true,
	            closeOnSelect:false
	        }).on("select2-selecting", function( e ) {
	        	$rootScope.edit_child_val=e.val;
	      	 });
        	});	
        },200);
     
    };
  
    $scope.updateUserRole = function(userRole) {
    	
    	
        Command: toastr["info"]("userRole has been updating Please Wait");
	    var child_menu= $('#editchildMenu').select2('val');
	    var sub_menu= $('#editSubMenu').select2('val');
	    var menu_id= $('#editMenu').select2('val') ;
	    var site_id= $('#editSite').select2('val') ;
	    if(child_menu==""){ child_menu=null;}
	    if(sub_menu==""){ sub_menu=null;}
	    if(menu_id==""){ menu_id=null;}
	    if(site_id==""){ site_id=null;}
        var userRoleData = "user_role/" + userRole.user_role + "/";
        	userRoleData += "user_site_id/" + site_id + "/";
        	userRoleData += "menu_id/" + menu_id + "/";
            userRoleData += "submenu_id/" + sub_menu + "/";
            userRoleData += "child_menu_id/" + child_menu;
        	
        $http.post(baseUrl + "api/user-role/update-user-role-by-id/id/" + userRole.user_role_id + "/" + userRoleData).success(function(response) {
        	
            if (response.meta.code == "200") {
            	 $http.get(baseUrl + "api/user-role/get-all-user-roles").success(function(response) {
            		 $scope.userRoles = response.data; 
                 });
                Command: toastr["success"]("UserRole has been updated");
            }
            else
            {
                Command: toastr["error"]("Error occured while updating. Please try again.");
            }

        });
    
  }
    
    $scope.deleteUserRole = function(userRole) {
        Command: toastr["info"]("userRole Deleting Please Wait");
        var modalInstance = $modal.open({
            templateUrl: 'deleteUserRole.html',
            controller: 'ModalUserRoleCtrl',
            resolve: {
            	selectedUserRole: function() {
                    return userRole;
                }
            }
        });

        modalInstance.result.then(function(selectedUserRole) {

            $scope.selectedUserRole = selectedUserRole;
            $scope.confirmDelete(selectedUserRole);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }
    $scope.confirmDelete = function(userRole) {
    	
        $http.post(baseUrl + "api/user-role/delete-user-role-by-id/id/" + userRole.user_role_id)
                .success(function(response) {

                    if (response.meta.code == "200") {
                        var position = $scope.userRoles.indexOf(userRole);

                        if (position > -1) {
                            $scope.userRoles.splice(position, 1);
                        }
                        Command: toastr["success"]("UserRole has been deleted");
                    } else {
                        Command: toastr["error"]("Error occured while deleting. Please try again.");
                    }

                });
    }
    
});


huskApp.controller('ModalUserRoleCtrl', function($scope,$http,$rootScope, $modalInstance, selectedUserRole) {
    $scope.selectedUserRole = selectedUserRole;
 
    /*$scope.getClusters=function(e){
    	var cluster_array=$rootScope.allClusters; 
    	var clusterData= $.grep(cluster_array, function(n) {
    		return (selectedUserRole.state_id).indexOf(n.state_id) != -1;
	    });
    	$scope.clusters = clusterData;
    }
   
    $scope.getSites=function(e){
    	var site_array=$rootScope.allSites; 
    	var siteData= $.grep(site_array, function(n) {
    		return (selectedUserRole.cluster_id).indexOf(n.cluster_id) != -1;
	    });
    	$scope.sites = siteData;
    };*/
    
    $rootScope.site_id_val=[];
	$rootScope.cluster_id_val=[];
    
	$scope.getClusters=function(e){
    	var cluster_array=$rootScope.allClusters;
    	var states_id=[];
    	var sites_id=[];
    	var cluster_value=$("#editCluster").select2('val');
   	   $("#editCluster option").each(function()
	   	{	sites_id.push($(this).val());	});
	   	
	   	var diff = $(sites_id).not(cluster_value).get();
	   	$rootScope.cluster_id_val=diff;
	   	
    	if(selectedUserRole.state_id==undefined)
    		{
				$scope.clusters="";
				setTimeout(function(){
					$("#editCluster").select2("val", $scope.clusters);
		    	}, 100);
				$scope.getAllSites($scope.clusters);
    		}
    	else{
    		var clusterData= $.grep(cluster_array, function(n) {
        		return (selectedUserRole.state_id).indexOf(n.state_id) != -1;
    	    });
        	$scope.clusters = clusterData;
		    	 
		    	var total=clusterData.length;
		    	$rootScope.sel_cluster_id=[];
		    	for(var i=0;i<total;i++){
		    		$rootScope.sel_cluster_id[i]=clusterData[i].cluster_id;
		    	}
		    	var index;
 		    	if(($rootScope.cluster_id_val).length>0){
 		    	for(var i=0;i<($rootScope.cluster_id_val).length;i++){
 			        index = $rootScope.sel_cluster_id.indexOf($rootScope.cluster_id_val[i]);
 		    		if (index > -1) {
 		    			$rootScope.sel_cluster_id.splice(index, 1);
 		    		}
     			  }
 		      }
		    	 if($rootScope.edit_state_val!=undefined){
		     	    var clusterData= $.grep(cluster_array, function(n) {
		 		    		return ($rootScope.edit_state_val).indexOf(n.state_id) != -1;
		 		    	});
		     	
		 	    	var checkclusterData= $.grep(clusterData, function(n) {
		 	    		return ($rootScope.cluster_id_val).indexOf(n.cluster_id) != -1;
		 	    	});
		 	    	
		 	    	if(checkclusterData.length>0){
		 	    		var total=checkclusterData.length;
		 		    	$rootScope.select_cluster_id;
		 		    	for(var k=0;k<total;k++){
		 		    		$rootScope.select_cluster_id=checkclusterData[k].cluster_id;
		 		    		$rootScope.sel_cluster_id.push($rootScope.select_cluster_id);
		 		    		index = $rootScope.cluster_id_val.indexOf($rootScope.select_cluster_id);
		 	 	    		if (index > -1) {
		 	 	    			$rootScope.cluster_id_val.splice(index, 1);
		 	 	    		}
		 		    	}
		 	    	}
		 	    	else{
		 	    		var index;
		 		    	if(($rootScope.cluster_id_val).length>0){
		 		    	for(var i=0;i<($rootScope.cluster_id_val).length;i++){
		 			        index = $rootScope.sel_cluster_id.indexOf($rootScope.cluster_id_val[i]);
		 		    		if (index > -1) {
		 		    			$rootScope.sel_cluster_id.splice(index, 1);
		 		    		}
		     			  }
		 		      }
		 		   }
		 		  }else{
		 			  
		 			var index;
		 	    	if(($rootScope.cluster_id_val).length>0){
		 	    	for(var i=0;i<($rootScope.cluster_id_val).length;i++){
		 		        index = $rootScope.sel_cluster_id.indexOf($rootScope.cluster_id_val[i]);
		 	    		if (index > -1) {
		 	    			$rootScope.sel_cluster_id.splice(index, 1);
		 	    		}
		 			  }
		 	    	}
		 		}
		    	setTimeout(function(){
					$("#editCluster").select2("val", $rootScope.sel_cluster_id);
		    	}, 100);
		    	$scope.getAllSites($rootScope.sel_cluster_id);
    	   }
    }
  
    $scope.getAllSites =function(cluster_id){
    	var site_array=$rootScope.allSites;
    	$scope.sites =$rootScope.allSites;
    	var j=0;$rootScope.sel_site_id=[];

    	var site_value= $("#editSite").select2('val');
    	var sites_id=[];
	   	 $("#editSite option").each(function()
	   	{	sites_id.push($(this).val());	});
	   	
	   	var diff = $(sites_id).not(site_value).get();
	   	$rootScope.site_id_val=diff;
	   	
    	if(cluster_id.length>0){
	    	for(var i=0;i<cluster_id.length;i++){
	    		var siteData= $.grep(site_array, function(n) {
		    		return (cluster_id[i]).indexOf(n.cluster_id) != -1;
			    });
		    	
	    		var total=siteData.length;
		    	for(var k=0;k<total;k++){
		    		$rootScope.sel_site_id[j]=siteData[k].site_id;
		    		j++;
		    	}
		    	
		    	var index;
		    	if(($rootScope.site_id_val).length>0){
		    	for(var m=0;m<($rootScope.site_id_val).length;m++){
			        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[m]);
		    		if (index > -1) {
		    			$rootScope.sel_site_id.splice(index, 1);
		    		}
				  }
		    	
			  }
		    	
		    
	        if($rootScope.edit_state_val!=undefined){
		    	
	        	var chclusterData= $.grep(site_array, function(n) {
		    		return ($rootScope.edit_state_val).indexOf(n.state_id) != -1;
		    	});
	    	 	var checkclusterData= $.grep(chclusterData, function(n) {
		    		return ($rootScope.site_id_val).indexOf(n.site_id) != -1;
		    	});
		    	
		    	   if(checkclusterData.length>0){
			    		var total=checkclusterData.length;
				    	$rootScope.select_site_id;
				    	for(var m=0;m<total;m++){
				    		$rootScope.select_site_id=checkclusterData[m].site_id;
				    		$rootScope.sel_site_id.push($rootScope.select_site_id);
				    		index = $rootScope.site_id_val.indexOf($rootScope.select_site_id);
			 	    		if (index > -1) {
			 	    			$rootScope.site_id_val.splice(index, 1);
			 	    		}
				    	}
			    	}
			    	else{
			    		var index;
				    	if(($rootScope.site_id_val).length>0){
				    	for(var m=0;m<($rootScope.site_id_val).length;m++){
					        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[m]);
				    		if (index > -1) {
				    			$rootScope.sel_site_id.splice(index, 1);
				    		}
						  }
				    	
					  }
				    }
		        
			  }
	    	else{
					var index;
			    	if(($rootScope.site_id_val).length>0){
			    	for(var m=0;m<($rootScope.site_id_val).length;m++){
				        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[m]);
			    		if (index > -1) {
			    			$rootScope.sel_site_id.splice(index, 1);
			    		}
					  }
			        }
			  }
		   }
	    	/*if(($rootScope.site_id_val).length>0){
		    	for(var i=0;i<($rootScope.site_id_val).length;i++){
			        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[i]);
		    		if (index > -1) {
		    			$rootScope.sel_site_id.splice(index, 1);
		    		}
				  }
		    	}*/
	    	$rootScope.edit_state_val=undefined;
	    	setTimeout(function(){
				$("#editSite").select2("val", $rootScope.sel_site_id);
	    	}, 300);
	    	
    	}else{
    		$scope.sites="";
			setTimeout(function(){
				$("#editSite").select2("val", $scope.sites);
	    	}, 100)
    	}
    }
    
    $scope.getSites= function(){
    	var sites_array=$rootScope.allSites; 
    	var sites_id=[];
    	 $("#editCluster option").each(function()
	   	{	sites_id.push($(this).val());	});
	   	
	   	var diff = $(sites_id).not(selectedUserRole.cluster_id).get();
	   	$rootScope.cluster_id_val=diff;
    	var clusters_id=selectedUserRole.cluster_id;
    	
		
			var siteData= $.grep(sites_array, function(n) {
	    		return (selectedUserRole.cluster_id).indexOf(n.cluster_id) != -1;
	    	});
			//$scope.sites=siteData;
			
	    	var total=siteData.length;
	    	$rootScope.sel_site_id=[];
	    	$rootScope.sel_state_id=[];
	    	for(var i=0;i<total;i++){
	    		$rootScope.sel_site_id[i]=siteData[i].site_id;
	    		$rootScope.sel_state_id[i]=siteData[i].state_id;
	    	}
	    	var index;
	    	if(($rootScope.site_id_val).length>0){
	    	for(var i=0;i<($rootScope.site_id_val).length;i++){
		        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[i]);
	    		if (index > -1) {
	    			$rootScope.sel_site_id.splice(index, 1);
	    		}
			  }
	    	}
	    
    if($rootScope.edit_cluster_val!=undefined){
    	    var clusterData= $.grep(sites_array, function(n) {
		    		return ($rootScope.edit_cluster_val).indexOf(n.cluster_id) != -1;
		    	});
    	
	    	var checkclusterData= $.grep(clusterData, function(n) {
	    		return ($rootScope.site_id_val).indexOf(n.site_id) != -1;
	    	});
	    	$rootScope.edit_cluster_val=undefined;
	    	if(checkclusterData.length>0){
	    		var total=checkclusterData.length;
		    	$rootScope.select_site_id;
		    	for(var k=0;k<total;k++){
		    		$rootScope.select_site_id=checkclusterData[k].site_id;
		    		$rootScope.sel_site_id.push($rootScope.select_site_id);
		    		index = $rootScope.site_id_val.indexOf($rootScope.select_site_id);
	 	    		if (index > -1) {
	 	    			$rootScope.site_id_val.splice(index, 1);
	 	    		}
		    	}
	    	}
	    	else{
	    		var index;
		    	if(($rootScope.site_id_val).length>0){
		    	for(var i=0;i<($rootScope.site_id_val).length;i++){
			        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[i]);
		    		if (index > -1) {
		    			$rootScope.sel_site_id.splice(index, 1);
		    		}
    			  }
		      }
		   }
		  }else{
			  
			var index;
	    	if(($rootScope.site_id_val).length>0){
	    	for(var i=0;i<($rootScope.site_id_val).length;i++){
		        index = $rootScope.sel_site_id.indexOf($rootScope.site_id_val[i]);
	    		if (index > -1) {
	    			$rootScope.sel_site_id.splice(index, 1);
	    		}
			  }
	    	}
		}
	   	setTimeout(function(){
			$("#editSite").select2("val", $rootScope.sel_site_id);
			$("#editState").select2("val", $rootScope.sel_state_id);
		}, 200);
	}
   
    $scope.checkCluster=function(){
    	
    	var sites_array=$rootScope.allSites; 
    	var sites_id=[];
    	 $("#editSite option").each(function()
    	{	sites_id.push($(this).val());	});
    	
    	var diff = $(sites_id).not(selectedUserRole.site_id).get();
    	$rootScope.site_id_val=diff;
    	
    		var siteData= $.grep(sites_array, function(n) {
		    		return (selectedUserRole.site_id).indexOf(n.site_id) != -1;
		    	});
    		
    			//$scope.sites=siteData;
    			
    		 	var total=siteData.length;
		    	$rootScope.sel_site_id=[];$rootScope.sel_state_id=[];
		    	for(var i=0;i<total;i++){
		    		$rootScope.sel_site_id[i]=siteData[i].cluster_id;
		    		$rootScope.sel_state_id[i]=siteData[i].state_id;
		    	}
		    	setTimeout(function(){
					$("#editCluster").select2("val", $rootScope.sel_site_id);
					$("#editState").select2("val", $rootScope.sel_state_id);
		    	}, 100);
		}
  
    $rootScope.menu_id_val=[];
    $scope.getMenues=function(e){
    	
    	var menu_array=$rootScope.allSubMenu; 
    	
    		if(selectedUserRole.menu_id==undefined)
    		{
				$scope.subMenus="";
				setTimeout(function(){
					$("#editSubMenu").select2("val", $scope.subMenus);
		    	}, 100);
				$scope.getsubMenues($scope.subMenus);
    		}
    	else{
		    	var menuData= $.grep(menu_array, function(n) {
		    		return (selectedUserRole.menu_id).indexOf(n.parent_menu) != -1;
		    	});
		    	$scope.subMenus = menuData;
		    	 
		    	var total=menuData.length;
		    	$rootScope.sel_subMenu_id=[];
		    	for(var i=0;i<total;i++){
		    		$rootScope.sel_subMenu_id[i]=menuData[i].menu_id;
		    	}
		    	var index;
	 	    	if(($rootScope.menu_id_val).length>0){
	 	    	for(var i=0;i<($rootScope.menu_id_val).length;i++){
	 		        index = $rootScope.sel_subMenu_id.indexOf($rootScope.menu_id_val[i]);
	 	    		if (index > -1) {
	 	    			$rootScope.sel_subMenu_id.splice(index, 1);
	 	    		}
	 			  }
	 	    	}
		    		
			 	    if($rootScope.edit_menu_val!=undefined){
			     	var clusterData= $.grep(menu_array, function(n) {
			 		    		return ($rootScope.edit_menu_val).indexOf(n.parent_menu) != -1;
			 		    	});
			     	
			 	    	var checkclusterData= $.grep(clusterData, function(n) {
			 	    		return ($rootScope.menu_id_val).indexOf(n.menu_id) != -1;
			 	    	});

			 	    	if(checkclusterData.length>0){
			 	    		var total=checkclusterData.length;
					    	$rootScope.select_subMenu_id;
					    	for(var k=0;k<total;k++){
					    		$rootScope.select_subMenu_id=checkclusterData[k].menu_id;
					    		$rootScope.sel_subMenu_id.push($rootScope.select_subMenu_id);
					    		index = $rootScope.menu_id_val.indexOf($rootScope.select_subMenu_id);
				 	    		if (index > -1) {
				 	    			$rootScope.menu_id_val.splice(index, 1);
				 	    		}
					    	}
			 	    	}else{var index;
			 		    	if(($rootScope.menu_id_val).length>0){
			 		    	for(var i=0;i<($rootScope.menu_id_val).length;i++){
			 			        index = $rootScope.sel_subMenu_id.indexOf($rootScope.menu_id_val[i]);
			 		    		if (index > -1) {
			 		    			$rootScope.sel_subMenu_id.splice(index, 1);
			 		    		}
			     			  }
			 		    	}}
			 		    
			 		}else{
			 			var index;
			 	    	if(($rootScope.menu_id_val).length>0){
			 	    	for(var i=0;i<($rootScope.menu_id_val).length;i++){
			 		        index = $rootScope.sel_subMenu_id.indexOf($rootScope.menu_id_val[i]);
			 	    		if (index > -1) {
			 	    			$rootScope.sel_subMenu_id.splice(index, 1);
			 	    		}
			 			  }
			 	    	}
			 		}
		    	setTimeout(function(){
					$("#editSubMenu").select2("val", $rootScope.sel_subMenu_id);
		    	}, 100);
		    	$scope.getsubMenues($rootScope.sel_subMenu_id);
    	   }
    }
    
    $scope.getsubMenues=function(subMenu_id){

    	var menu_array=$rootScope.allSubMenu;
    	$scope.childMenus =$rootScope.allSubMenu;
    	var j=0;$rootScope.sel_childMenu_id=[];
    	if(subMenu_id.length>0){
	    		var menuData= $.grep(menu_array, function(n) {
		    		return (subMenu_id).indexOf(n.parent_menu) != -1;
			    });
		    	
	    		var total=menuData.length;
		    	for(var k=0;k<total;k++){
		    		$rootScope.sel_childMenu_id[k]=menuData[k].menu_id;
		    		j++;
		    	}
		    	var index;
	 	    	if(($rootScope.menu_child_id_val).length>0){
	 	    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
	 		        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
	 	    		if (index > -1) {
	 	    			$rootScope.sel_childMenu_id.splice(index, 1);
	 	    		}
	 			  }
	 	    	}
		    	
		    if($rootScope.edit_menu_val!=undefined){
			     	var clusterData= $.grep(menu_array, function(n) {
			 		    		return (subMenu_id).indexOf(n.parent_menu) != -1;
			 		    	});
			     	
			 	    	var checkclusterData= $.grep(clusterData, function(n) {
			 	    		return ($rootScope.menu_child_id_val).indexOf(n.menu_id) != -1;
			 	    	});

			 	    	if(checkclusterData.length>0){
			 	    		var total=checkclusterData.length;
					    	$rootScope.select_subMenu_id;
					    	for(var k=0;k<total;k++){
					    		$rootScope.select_subMenu_id=checkclusterData[k].menu_id;
					    		$rootScope.sel_childMenu_id.push($rootScope.select_subMenu_id);
					    		index = $rootScope.menu_child_id_val.indexOf($rootScope.select_subMenu_id);
				 	    		if (index > -1) {
				 	    			$rootScope.menu_child_id_val.splice(index, 1);
				 	    		}
					    	}
			 	    	}else{var index;
			 		    	if(($rootScope.menu_child_id_val).length>0){
			 		    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
			 			        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
			 		    		if (index > -1) {
			 		    			$rootScope.sel_childMenu_id.splice(index, 1);
			 		    		}
			     			  }
			 		    	}}
			 		    
			 		}else{
			 			var index;
			 	    	if(($rootScope.menu_child_id_val).length>0){
			 	    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
			 		        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
			 	    		if (index > -1) {
			 	    			$rootScope.sel_childMenu_id.splice(index, 1);
			 	    		}
			 			  }
			 	    	}
			 		}

		    $rootScope.edit_menu_val=undefined;
	    	setTimeout(function(){
				$("#editchildMenu").select2("val", $rootScope.sel_childMenu_id);
	    	}, 300);
	    	
    	}else{
    		$scope.childMenus="";
			setTimeout(function(){
				$("#editchildMenu").select2("val", $scope.childMenus);
	    	}, 100)
    	}
    }
    
    $scope.checkSubMenue= function(){
    	var menu_array=$rootScope.allSubMenu; 
    	var sub_id=[];
    	 $("#editSubMenu option").each(function()
    	   {	sub_id.push($(this).val());	});
    	    var diff = $(sub_id).not(selectedUserRole.subMenu_id).get();
    	    $rootScope.menu_id_val=diff;
    	    
			var menuData= $.grep(menu_array, function(n) {
		    		return (selectedUserRole.subMenu_id).indexOf(n.parent_menu) != -1;
		    	});
		    	$scope.childMenus = menuData;
		    	
		    	var total=menuData.length;
		    	$rootScope.sel_childMenu_id=[];
		    	for(var i=0;i<total;i++){
		    		$rootScope.sel_childMenu_id[i]=menuData[i].menu_id;
		    	}
		    	var index;
 		    	if(($rootScope.menu_child_id_val).length>0){
 		    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
 			        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
 		    		if (index > -1) {
 		    			$rootScope.sel_childMenu_id.splice(index, 1);
 		    		}
     			  }
 		    	}
		    	if($rootScope.edit_sub_val!=undefined){
		     	var subMenu_Data= $.grep(menu_array, function(n) {
		 		    		return ($rootScope.edit_sub_val).indexOf(n.parent_menu) != -1;
		 		    	});
		     	
		 	    	var checksubMenu_Data= $.grep(subMenu_Data, function(n) {
		 	    		return ($rootScope.menu_child_id_val).indexOf(n.menu_id) != -1;
		 	    	});

		 	    	$rootScope.edit_sub_val=undefined;
		 	    	if(checksubMenu_Data.length>0){
		 	    		var total=checksubMenu_Data.length;
				    	$rootScope.select_subMenu_id;
				    	for(var k=0;k<total;k++){
				    		$rootScope.select_subMenu_id=checksubMenu_Data[k].menu_id;
				    		$rootScope.sel_childMenu_id.push($rootScope.select_subMenu_id);
				    		index = $rootScope.menu_child_id_val.indexOf($rootScope.select_subMenu_id);
			 	    		if (index > -1) {
			 	    			$rootScope.menu_child_id_val.splice(index, 1);
			 	    		}
				    	}
		 	    	}else{var index;
		 		    	if(($rootScope.menu_child_id_val).length>0){
		 		    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
		 			        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
		 		    		if (index > -1) {
		 		    			$rootScope.sel_childMenu_id.splice(index, 1);
		 		    		}
		     			  }
		 		    	}}
		 		    
		 		}else{
		 			var index;
		 	    	if(($rootScope.menu_child_id_val).length>0){
		 	    	for(var i=0;i<($rootScope.menu_child_id_val).length;i++){
		 		        index = $rootScope.sel_childMenu_id.indexOf($rootScope.menu_child_id_val[i]);
		 	    		if (index > -1) {
		 	    			$rootScope.sel_childMenu_id.splice(index, 1);
		 	    		}
		 			  }
		 	    	}
		 		}
		    	
		    	setTimeout(function(){
					$("#editchildMenu").select2("val", $rootScope.sel_childMenu_id);
				}, 100);
		    	$scope.checkMenu();
    }
    $rootScope.menu_child_id_val=[];
    
	$scope.checkMenu=function(){
		    	var menu_array=$rootScope.allSubMenu; 
		    	var child_id=[];
		    	/* $("#Multiple_childMenu option").each(function()
		    	    	   {	child_id.push($(this).val());	});
		    	    	    var diff = $(child_id).not($scope.childMenu_id).get();
		    	    	    $rootScope.menu_child_id_val=diff;*/
		    	    	    
				var menuData= $.grep(menu_array, function(n) {
			    		return (selectedUserRole.subMenu_id).indexOf(n.menu_id) != -1;
			    	});
			    	
			    	var total=menuData.length;
			    	$rootScope.sel_main_menu_id=[];
			    	for(var i=0;i<total;i++){
			    		$rootScope.sel_main_menu_id[i]=menuData[i].parent_menu;
			    	}
			    	setTimeout(function(){
						$("#editMenu").select2("val", $rootScope.sel_main_menu_id);
			    	}, 100);
	 }

  $scope.checkMainMenu=function(){
     var menu_array=$rootScope.allSubMenu;
     var child_id=[];
     var childs_id=$("#editchildMenu").select2('val');
	   	 $("#editchildMenu option").each(function()
	   	    {	child_id.push($(this).val());	});
	   	      var diff = $(child_id).not(selectedUserRole.childMenu_id).get();
	   	      $rootScope.menu_child_id_val=diff;
  }   
    $scope.getRoleSite = function()
    {
    	$http.get(baseUrl + "api/user-role/get-role-site-by-id/id/" +selectedUserRole.user_role_id)
    	.success(function(response) {
    		var roleSite=response.data;
    		$scope.setRoleSite(roleSite);
    	});
    	$http.get(baseUrl + "api/user-role/get-all-menu-id-by-role-id/id/" +selectedUserRole.user_role_id)
    	.success(function(response) {
    		var menu=response.data;
    		$scope.setMenu(menu);
    	});
    	
    }
    
    
    $scope.setRoleSite = function(roleSites)
    {
    	var total=roleSites.length;
    	$rootScope.states_id=[];
    	$rootScope.clusters_id=[];
    	$rootScope.sites_id=[];
    	for(var i=0;i<total;i++){
    		$rootScope.states_id[i]=roleSites[i].state_id;
    		$rootScope.clusters_id[i]=roleSites[i].cluster_id;
    		$rootScope.sites_id[i]=roleSites[i].site_id;
    	}
    	$("#editState").select2("val", $rootScope.states_id);
		$("#editCluster").select2("val", $rootScope.clusters_id);
		$("#editSite").select2("val", $rootScope.sites_id);
	};
    $scope.setMenu = function(menu)
    {
    	var total=menu.length;
    	$rootScope.sel_menu=[];
    	$rootScope.sel_subMenu=[];
    	for(var i=0;i<total;i++){
    		$rootScope.sel_menu[i]=menu[i].parent_menu;
    		$rootScope.sel_subMenu[i]=menu[i].menu_id;
    	}
    	
    	$("#editMenu").select2("val", $rootScope.sel_menu);
    	$("#editSubMenu").select2("val", $rootScope.sel_subMenu);
    	$("#editchildMenu").select2("val", $rootScope.sel_subMenu); 
		
    	$("#saveButton").prop( "disabled", false );
     };
     $scope.init=function(){
     $scope.states = $rootScope.allState;
     $scope.clusters = $rootScope.allClusters;
     $scope.sites = $rootScope.allSites;
     $scope.menus = $rootScope.allMenus;
     $scope.subMenus = $rootScope.allSubMenu;
     $scope.childMenus = $rootScope.allSubMenu;
    	setTimeout(function () {
    		$scope.getRoleSite();
    	},200);
     }
     $scope.init();
    
    $scope.ok = function() {
        $modalInstance.close($scope.selectedUserRole);
    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.confirm = function() {
        $modalInstance.close($scope.selectedUserRole);
    };
   
});
