var huskApp = angular.module('huskApp');


huskApp.controller("userRoleController", function($scope,$http,$rootScope, $log) {
	
	//Command: toastr["info"]("Loading data, Please wait...");
    $http.get(baseUrl + "api/user-role/get-all-user-roles")
            .success(function(response) {
                $scope.userRoles = response.data;
				
    //            Command: toastr["success"]("UserRole have been loaded");
            });
   
    /*$http.get(baseUrl + "api/sites/get-all-sites")
    .success(function(response) {
       // $scope.sites = response.data;
        $rootScope.allSites=response.data;
    //    	Command: toastr["success"]("Sites have been loaded");
    });*/
$http.get(baseUrl + "api/sites/get-all-sites-for-site-page")
    .success(function(response) {
        $rootScope.allSites=response.data;
    });
    $http.get(baseUrl + "api/state/get-all-states")
    .success(function(response) {
    	$scope.states = response.data;
        $rootScope.allState=response.data;
   //     Command: toastr["success"]("States have been loaded");
    });
    //var allClusters=[];
    $http.get(baseUrl + "api/clusters/get-all-clusters/allCluster/"+true)
            .success(function(response) {
            	//$scope.clusters = response.data;
                $rootScope.allClusters=response.data;
          });
   
    $http.get(baseUrl + "api/user-role/get-all-menu-by-type/menu_type/menu")
    	.success(function(response) {
    		$scope.menus = response.data;
    		$rootScope.allMenus=$scope.menus;
    });
   
    $http.get(baseUrl + "api/user-role/get-all-menu-by-type/menu_type/sub_menu" )
    	.success(function(response) {
    		//$scope.subMenus = response.data;
    		$rootScope.allSubMenu=response.data;
    });
   /* $http.get(baseUrl + "api/user-role/get-all-menu-by-type/menu_type/page_item" )
	.success(function(response) {
		$rootScope.allchildMenu=response.data;
	});*/
    $scope.getMenues=function(e){
    	var menu_array=$rootScope.allSubMenu; 
		if($scope.menu_id==undefined)
    		{$scope.subMenus="";
    		}
    	else{
    	var menuData= $.grep(menu_array, function(n) {
    		return ($scope.menu_id).indexOf(n.parent_menu) != -1;
	    });
    	$scope.subMenus = menuData;
		}
    //    Command: toastr["success"]("SubMenues have been loaded");
    }
    
    $scope.getsubMenues=function(e){
    	var menu_array=$rootScope.allSubMenu; 
		if($scope.subMenu_id==undefined)
    		{$scope.childMenus="";
    		}
    	else{
    	var menuData= $.grep(menu_array, function(n) {
    		return ($scope.subMenu_id).indexOf(n.parent_menu) != -1;
	    });
    	$scope.childMenus = menuData;
		}
    //    Command: toastr["success"]("childMenues have been loaded");
    }
   /* $scope.getSubMenues=function(e){
    	var subMenu_array=$rootScope.allSubMenu; 
    	var subMenuData= $.grep(subMenu_array, function(n) {
    		return ($scope.subMenu_id).indexOf(n.parent_menu) != -1;
	    });
    	$scope.subMenus.concat(subMenuData);
    	//$scope.subMenus = subMenuData;
        Command: toastr["success"]("SubMenues have been loaded");
    }*/
   
    
    $scope.getClusters=function(e){
    	var cluster_array=$rootScope.allClusters; 
		if($scope.state_id==undefined)
    		{$scope.clusters="";
    		}
    	else{
    	var clusterData= $.grep(cluster_array, function(n) {
    		return ($scope.state_id).indexOf(n.state_id) != -1;
	      	//return n.state_id == $scope.state_id;
	    });
    	$scope.clusters = clusterData;
		}
    //    Command: toastr["success"]("Clusters have been loaded");
    }
   $scope.getSites=function(e){
    	var site_array=$rootScope.allSites; 
		if($scope.cluster_id==undefined)
    		{$scope.sites="";
    		}
    	else{
    	var siteData= $.grep(site_array, function(n) {
    		return ($scope.cluster_id).indexOf(n.cluster_id) != -1;
	      	//return n.cluster_id == $scope.cluster_id;
	    });
    	$scope.sites = siteData;
		}
     //   Command: toastr["success"]("Sites have been loaded");
    }
    $scope.addUserRole = function(e) {
        Command: toastr["info"]("UserRole Adding Please Wait");
    	var siteId=$scope.site_id;
    	$rootScope.menues=$scope.menu_id;
	var child=$scope.childMenu_id;
    	if(child==""){child=null;}
    	var userRoleData = "user_role/" + $scope.user_role + "/";
        userRoleData += "site_id/" + $scope.site_id + "/";
        userRoleData += "menu_id/" + $scope.menu_id + "/";
        userRoleData += "submenu_id/" + $scope.subMenu_id + "/";
        userRoleData += "child_menu_id/" + child + "/";

        $http.post(baseUrl + "api/user-role/add-user-role/" + userRoleData)
                .success(function(response) {
                    if (response.meta.code == "200") {
                    	$('#userRole').val("");
                    	$("#Multiple_State").select2('val', 'All');
                    	$("#Multiple_Cluster").select2('val', 'All');
                    	$("#Multiple_Site").select2('val', 'All');
                    	$("#Multiple_Menu").select2('val', 'All');
                    	$("#Multiple_subMenu").select2('val', 'All');
                    	$("#Multiple_childMenu").select2('val', 'All');
                    	
                    	var new_Roles = {
                        		user_role_id: response.data.user_role_id,
                        		user_role:response.data.user_role,
                        };
                        Command: toastr["success"]("User Role has been Added");
                        $scope.userRoles.unshift(new_Roles);
                        
                    } else {
                        Command: toastr["danger"]("Opps Somthing Went Worng");
                    }
                });
    }

  
    $scope.submitForm = function(isValid) {
        if (isValid) 
        {
            $scope.addUserRole();
        }
        else
        {
            Command: toastr["error"]("Error while validating form");
        }

    };
    

    $scope.editUserRole = function(userRole) {
    	 
    	var modalInstance = $modal.open({
            templateUrl: 'editUserRole.html',
            controller: 'ModalUserRoleCtrl',
            
            resolve: {
                selectedUserRole: function() {
                    $UserRoles=angular.copy(userRole);
                    return $UserRoles;

                }
            }
        });
    	
        modalInstance.result.then(function(selectedUserRole) {
        	$scope.selectedUserRole = selectedUserRole;
        	$scope.updateUserRole(selectedUserRole);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
        
        setTimeout(function () {
        	$('#editState').select2({
	            placeholder: "Select States",
	            allowClear: true
	        });
        	$('#editCluster').select2({
	            placeholder: "Select Clusters",
	            allowClear: true
	        });
        	$('#editSite').select2({
	            placeholder: "Select Sites",
	            allowClear: true,
	            closeOnSelect:false
	        });
        	$('#editMenu').select2({
	            placeholder: "Select Menu",
	            allowClear: true
	        });
        	$('#editSubMenu').select2({
	            placeholder: "Select SubMenu",
	            allowClear: true,
	            closeOnSelect:false
	        });
        	$('#editchildMenu').select2({
	            placeholder: "Select ChildMenu",
	            allowClear: true,
	            closeOnSelect:false
	        });
        	
        },200);
     
    };
  
    $scope.updateUserRole = function(userRole) {
    	
    	
        Command: toastr["info"]("userRole has been updating Please Wait");
    
			var child_menu=$('#editchildMenu').select2('val');
			var sub_menu= $('#editSubMenu').select2('val');
			var menu_id= $('#editMenu').select2('val') ;
			if(child_menu==""){ child_menu=null;}
			if(sub_menu==""){ sub_menu=null;}
			if(menu_id==""){ menu_id=null;}
        	var userRoleData = "user_role/" + userRole.user_role + "/";
        	  userRoleData += "user_site_id/" + userRole.site_id + "/";
        	  userRoleData += "menu_id/" + menu_id + "/";
              userRoleData += "submenu_id/" + sub_menu + "/";
              userRoleData += "child_menu_id/" + child_menu;     
        $http.post(baseUrl + "api/user-role/update-user-role-by-id/id/" + userRole.user_role_id + "/" + userRoleData).success(function(response) {
        	
            if (response.meta.code == "200") {
			 
				$http.get(baseUrl + "default/auth/change-data")
	      	   	  .success(function(response) {
	      	   	  });
				  
            	 $http.get(baseUrl + "api/user-role/get-all-user-roles").success(function(response) {
            		 $scope.userRoles = response.data; 
                 });
                Command: toastr["success"]("UserRole has been updated");
            }
            else
            {
                Command: toastr["error"]("Error occured while updating. Please try again.");
            }

        });
    
  }
    
    $scope.deleteUserRole = function(userRole) {
        Command: toastr["info"]("userRole Deleting Please Wait");
        var modalInstance = $modal.open({
            templateUrl: 'deleteUserRole.html',
            controller: 'ModalUserRoleCtrl',
            resolve: {
            	selectedUserRole: function() {
                    return userRole;
                }
            }
        });

        modalInstance.result.then(function(selectedUserRole) {

            $scope.selectedUserRole = selectedUserRole;
            $scope.confirmDelete(selectedUserRole);
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }
    $scope.confirmDelete = function(userRole) {
    	
        $http.post(baseUrl + "api/user-role/delete-user-role-by-id/id/" + userRole.user_role_id)
                .success(function(response) {

                    if (response.meta.code == "200") {
                        var position = $scope.userRoles.indexOf(userRole);

                        if (position > -1) {
                            $scope.userRoles.splice(position, 1);
                        }
                        Command: toastr["success"]("UserRole has been deleted");
                    } else {
                        Command: toastr["error"]("Error occured while deleting. Please try again.");
                    }

                });
    }
    
});


huskApp.controller('ModalUserRoleCtrl', function($scope,$http,$rootScope, $modalInstance, selectedUserRole) {
    $scope.selectedUserRole = selectedUserRole;
 
    $scope.getClusters=function(e){
    	var cluster_array=$rootScope.allClusters; 
    	var clusterData= $.grep(cluster_array, function(n) {
    		return (selectedUserRole.state_id).indexOf(n.state_id) != -1;
	    });
    	$scope.clusters = clusterData;
    }
   
    $scope.getSites=function(e){
    	var site_array=$rootScope.allSites; 
    	var siteData= $.grep(site_array, function(n) {
    		return (selectedUserRole.cluster_id).indexOf(n.cluster_id) != -1;
	    });
    	$scope.sites = siteData;
    };
    
    $scope.getMenues=function(e){
    	var menu_array=$rootScope.allSubMenu; 
    	var menuData= $.grep(menu_array, function(n) {
    		return (selectedUserRole.menu_id).indexOf(n.parent_menu) != -1;
	    });
		  $scope.subMenus = menuData;
     }
    $scope.getChildMenues=function(e){
    	var menu_array=$rootScope.allSubMenu;
    	var menuData= $.grep(menu_array, function(n) {
    		return (selectedUserRole.subMenu_id).indexOf(n.parent_menu) != -1;
	    });
		var child_menu=$('#editchildMenu').select2('val');
		$scope.childMenus = menuData;
		
		  setTimeout(function () {
    		$("#editchildMenu").select2("val", child_menu); 
    	 },200);  
		 
    	
    }
    $scope.getRoleSite = function()
    {
    	$http.get(baseUrl + "api/user-role/get-role-site-by-id/id/" +selectedUserRole.user_role_id)
    	.success(function(response) {
    		var roleSite=response.data;
    		$scope.setRoleSite(roleSite);
    	});
    	$http.get(baseUrl + "api/user-role/get-all-menu-id-by-role-id/id/" +selectedUserRole.user_role_id)
    	.success(function(response) {
    		var menu=response.data;
    		$scope.setMenu(menu);
    	});
    	
    }
    
    
    $scope.setRoleSite = function(roleSites)
    {
    	var total=roleSites.length;
    	$rootScope.states_id=[];
    	$rootScope.clusters_id=[];
    	$rootScope.sites_id=[];
    	for(var i=0;i<total;i++){
    		$rootScope.states_id[i]=roleSites[i].state_id;
    		$rootScope.clusters_id[i]=roleSites[i].cluster_id;
    		$rootScope.sites_id[i]=roleSites[i].site_id;
    	}
    	$("#editState").select2("val", $rootScope.states_id);
		$("#editCluster").select2("val", $rootScope.clusters_id);
		$("#editSite").select2("val", $rootScope.sites_id);
	};
    $scope.setMenu = function(menu)
    {
    	var total=menu.length;
    	$rootScope.sel_menu=[];
    	$rootScope.sel_subMenu=[];
    	for(var i=0;i<total;i++){
    		$rootScope.sel_menu[i]=menu[i].parent_menu;
    		$rootScope.sel_subMenu[i]=menu[i].menu_id;
    	}
    	
    	$("#editMenu").select2("val", $rootScope.sel_menu);
    	$("#editSubMenu").select2("val", $rootScope.sel_subMenu);
    	$("#editchildMenu").select2("val", $rootScope.sel_subMenu); 
		
    	$("#saveButton").prop( "disabled", false );
     };
     $scope.init=function(){
     $scope.states = $rootScope.allState;
     $scope.clusters = $rootScope.allClusters;
     $scope.sites = $rootScope.allSites;
     $scope.menus = $rootScope.allMenus;
     $scope.subMenus = $rootScope.allSubMenu;
     $scope.childMenus = $rootScope.allSubMenu;
    	setTimeout(function () {
    		$scope.getRoleSite();
    	},200);
     }
     $scope.init();
    
    $scope.ok = function() {
        $modalInstance.close($scope.selectedUserRole);
    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.confirm = function() {
        $modalInstance.close($scope.selectedUserRole);
    };
   
});
