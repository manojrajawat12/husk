var Flotcharts = function() {

    return {
     
    interactiveCharts: function(control,data,showArea) {
 
    	 if (!jQuery.plot) {
             return;
        }
        
        if ($('#'+control).size() != 1) {
             return;
        }
              
               var option={
                        series: {
                            lines: {
                                show: true,
                                lineWidth: 2,
                                fill: showArea,
                                fillColor: {
                                    colors: [{
                                        opacity: 0.10
                                    }, {
                                        opacity: 0.10
                                    }]
                                }
                            },
                            points: {
                                show: true,
                                radius: 3,
                                lineWidth: 1
                            },
                            shadowSize: 2
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        },
                        colors: ["#d12610", "#37b7f3", "#52e136"],
                        xaxis: {
                        	mode: "categories",
            				tickLength: 0,
                           /* ticks: 11,
                            tickDecimals: 0,*/
                            tickColor: "#eee",
                        },
                        yaxis: {
                        	//mode: "categories",
            				//tickLength: 0,
                            ticks: 11,
                            tickDecimals: 0,
                            tickColor: "#eee",
                        }
                };

                var plot = jQuery.plot($('#'+control),data , option);

                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y + 5,
                        left: x + 15,
                        border: '1px solid #333',
                        padding: '4px',
                        color: '#fff',
                        'border-radius': '3px',
                        'background-color': '#333',
                        opacity: 0.80
                    }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
                jQuery('#'+control).bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));

                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            
                            switch(control)
                            {
                            case "chart_consumer" :
                            	showTooltip(item.pageX, item.pageY, item.series.data[item.dataIndex][0]+" "+item.series.label + " is " + item.datapoint[1]);
                            	break;
                            case "chart_bihar" :
                            	showTooltip(item.pageX, item.pageY, item.series.data[item.dataIndex][0]+" "+item.series.label + " Amount is Rs." + item.datapoint[1]);
                            	break;	
                            case "chart_up" :
                            	showTooltip(item.pageX, item.pageY, item.series.data[item.dataIndex][0]+" "+item.series.label + " Amount is Rs." + item.datapoint[1]);
                            	break;
                            case "chart_monthlyBilling" :
                            	showTooltip(item.pageX, item.pageY, item.series.data[item.dataIndex][0]+" "+item.series.label + " Amount is Rs." + item.datapoint[1]);
                            	break;
                            }
                          
                            
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
             
        },
		
		initBarCharts: function(control,data1) {
        	 var data = [];
        	 var ticks = [];
             for (i = 0; i < 15; i++) {
                data.push([(i),data1[i]["c"]]);
                ticks.push([(i),data1[i]["p"]]);
            }
        	var dataset = [{data: data, color: "#F7CA18" }];
             
              var options = {
                  series: {
                      bars: {
                          show: true
                      }
                  },
                  bars: {
                      align: "center",
                      barWidth: 0.5,
                      lineWidth: 0, // in pixels
                      shadowSize: 5,
                      fill:1
                  },
                  xaxis: {
                      axisLabel: "Package",
                      axisLabelUseCanvas: true,
                      axisLabelFontSizePixels: 12,
                      axisLabelFontFamily: 'Verdana, Arial',
                      axisLabelPadding: 10,
                      tickColor: "#eee",
                      ticks: ticks
                  },
                  yaxis: {
                      axisLabel: "Consumer Count",
                      axisLabelUseCanvas: true,
                      axisLabelFontSizePixels: 12,
                      axisLabelFontFamily: 'Verdana, Arial',
                      axisLabelPadding: 3,
                      tickColor: "#eee",
                      tickFormatter: function (v, axis) {
                          return v ;
                      }
                  },
                  legend: {
                      noColumns: 0,
                      //labelBoxBorderColor: "#f4d03f",
                      position: "nw"
                  },
                  grid: {
                      hoverable: true,
                      borderWidth: 0,
                      //tickColor: "#eee",
                      //borderColor: "#eee",
                      //backgroundColor: { colors: ["#eee", "#EDF5FF"] }
                  }
              };

             
//              $(document).ready(function () {
            	var plot=  jQuery.plot($('#'+control), dataset, options);
            	  
            	  function showTooltip(x, y,contents) {
                      $('<div id="tooltip">' + contents + '</div>').css({
                          position: 'absolute',
                          display: 'none',
                          top: y + 5,
                          left: x + 15,
                          border: '1px solid #333',
                          padding: '4px',
                          color: '#fff',
                          //'border-radius': '3px',
                          'background-color': '#333',
                          opacity: 0.80
                      }).appendTo("body").fadeIn(200);
                  }
//                  function UseTooltip () {
                	  jQuery('#'+control).bind("plothover", function (event, pos, item) {
                          if (item) {
                              if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                                  previousPoint = item.dataIndex;
                                  previousLabel = item.series.label;
                                  $("#tooltip").remove();
           
                                  var x = item.datapoint[0];
                                  var y = item.datapoint[1];
           
                                  showTooltip(item.pageX,item.pageY,
                                  "<strong> Consumer : " + y + "</strong><br><strong> Package : "  + item.series.xaxis.ticks[x].label +"</strong>");
                              }
                          } else {
                              $("#tooltip").remove();
                              previousPoint = null;
                          }
                      });
//                  }
//                 UseTooltip();
        //      });
       
             /* function gd(year, month, day) {
                  return new Date(year, month, day).getTime();
              }*/
       
              var previousPoint = null, previousLabel = null;
        },
        
  stackCharts: function(control,receive_data,pending_data,month,year,consumers,site_id,state_id) {
        	
          
	  if (!jQuery.plot) {
           return;
      }
      
      if ($('#'+control).size() != 1) {
           return;
      }
	 var myPlot;
	  
	  var d1 = [];
      var ticks = [];
      for (var i = 0; i < month.length; i++){
         ticks.push([(i),month[i]["m"]]);
      }
      for (var i = 0; i < month.length; i++)
    	  {
          d1.push([i,receive_data[i]["r"]]);
    	  }
      var d2 = [];
      for (var i = 0; i < month.length; i++){
          d2.push([i, pending_data[i]["p"]]);
      }
	      var data = [ 
	      {
	          label: "Received OTP",
	          data: d1,
	          url:""
	      },{
	          label: "Pending OTP",
	          data: d2,
	         url:"http://localhost:1234/tara/html/#/Pending-Otp?year="+year+"&"
	      }];

	      myPlot = jQuery.plot($('#'+control), data, {
	          series: {
	        	  stack: true,
	        	  bars: {
                      show: true,
                 }
	          },
	          bars: {
                  align: "center",
                  barWidth: 0.5,
                  lineWidth: 0, // in pixels
                  fill:1
              },
              
	          xaxis: {
	              ticks:ticks
	              //tickLength: 0
	          },
	          colors:["#44B6AE","#F36A5A"],
	          grid: {
	              hoverable: true,
	              tickColor: "#eee",
                  borderColor: "#eee",
                  borderWidth: 1,
                  clickable: true
	          }
	      });
	     
		  if(control=='stack_Chart')
		  {
	    var sumaArr = []; xPos=[];
		for (var u = 0; u < myPlot.getData().length; u++) {
		    jQuery.each(myPlot.getData()[u].data, function (i, el) {
		        sumaArr[i] > 0 ? sumaArr[i] = parseInt(sumaArr[i]) + parseInt(el[1]) : sumaArr[i] = el[1];
		        xPos[i]=el[0];
		    });
		}
		for (var i = 0; i < sumaArr.length; i++) {
		    var text = sumaArr[i];
		    var newConsumer= parseInt(consumers[i]);
		    var o = myPlot.pointOffset({x: xPos[i], y: text});
		    if(text >0)
		    {
			  $('<div class="data-point-label" style="color:green" title="New Consumers"><strong>' + newConsumer + '</strong></div>').css( {
			    position: 'absolute',
			    left: o.left -10,
			    top: o.top - 20,
			    display: 'none'
			  }).appendTo(myPlot.getPlaceholder()).fadeIn('slow');
			}
		}
		}

		  function showTooltip(x, y, color, contents) {
		      $('<div id="tooltip">' + contents + '</div>').css({
		          position: 'absolute',
		          display: 'none',
		          top: y + 5,
		          left: x + 15,
		          border: '1px solid #333',
		          padding: '4px',
		          color: '#fff',
	              'background-color': '#333',   
		          opacity: 0.80
		      }).appendTo("body").fadeIn(200);
		  }
		  
	jQuery('#'+control).unbind("plotclick");
	jQuery('#'+control).bind("plotclick", function (event, pos, item) {
		       if (item) {
		       
		        	if(item.series.label =="Pending OTP")
		        	{
		        		var x = item.datapoint[0];
		        		var data = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			        	var month=item.datapoint[0]+1;
			        	var url=item.series.url+"month="+month;
			        	var url=url+"&site_id="+site_id+"&state_id="+state_id;
			        	if(jQuery.inArray( item.series.xaxis.ticks[x].label, data )<0)
			        	{
			        		var month=item.series.xaxis.ticks[x].label;
			        		var url=item.series.url+"site="+month;
			        	}
			        	
			            var win = window.open(url, '_blank');
	  					win.focus();
		        	}
		        	
		        
		      }else {
		          $("#tooltip").remove();
		          prevLabel = null;
		      }
   		 });   
	var prevPoint = null, prevLabel = null;
	
	  
	  /*function showTooltip(x, y, color, contents) {
	      $('<div id="tooltip">' + contents + '</div>').css({
	          position: 'absolute',
	          display: 'none',
	          top: y + 5,
	          left: x + 15,
	          border: '1px solid #333',
	          padding: '4px',
	          color: '#fff',
              'background-color': '#333',   
	          opacity: 0.80
	      }).appendTo("body").fadeIn(200);
	  }*/


	  jQuery('#'+control).on("plothover", function (event, pos, item) {
	      if (item) {
	          if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
	              previousPoint = item.dataIndex;
	              previousLabel = item.series.label;
	              $("#tooltip").remove();

	              var x = item.datapoint[0];
	              var y = item.datapoint[1]- item.datapoint[2];


	              var color = item.series.color;

	              showTooltip(item.pageX,
	              item.pageY,
	              color,
	                  "<strong>" + item.series.label + "</strong><br>" + item.series.xaxis.ticks[x].label + " : <strong>" + y + "</strong> ");
	          }
	      } else {
	          $("#tooltip").remove();
	          previousPoint = null;
	      }
	  }); 
	  var previousPoint = null,
      previousLabel = null;

	   }  
  };

}();