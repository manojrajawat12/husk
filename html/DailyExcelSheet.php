<?php

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** PHPExcel_IOFactory */
require_once '../PHPExcel/PHPExcel/IOFactory.php';

class ReadFilter implements PHPExcel_Reader_IReadFilter {

	public function __construct($fromColumn, $toColumn) {
		$this->columns = array();
		$toColumn++;
		while ($fromColumn !== $toColumn) {
			$this->columns[] = $fromColumn++;
		}
	}

	public function readCell($column, $row, $worksheetName = '') {
		if (in_array($column, $this->columns)) {
			return true;
		}
		return false;
	}
}

$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
$cacheSettings = array( 'memoryCacheSize' => '4096MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

$objReader = PHPExcel_IOFactory::createReader('Excel2007');

$inputFileType = 'Excel2007';
$inputFileName = 'Daily SQL conversion - BK example filled v0.6.xlsm';//'Daily SQL conversion 20150805(new def for Server injection)(1).xlsx';//'template.xlsx' ;//'Daily SQL conversion.xlsx';
$sheetIndex = 0;
$filterSubset = new ReadFilter('A','H');


$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$sheetnames = $objReader->listWorksheetNames($inputFileName);
//$objReader->setLoadSheetsOnly($sheetnames[$sheetIndex]);
//$objReader->setReadFilter($filterSubset);
$objPHPExcel = $objReader->load($inputFileName);
$objPHPExcel->setActiveSheetIndex(0);
$sheetData = $objPHPExcel->getActiveSheet();//->toArray(null, true, true, false);
 $rowCount = 2;
while($rowCount <=5)
{
	$objPHPExcel->getActiveSheet() ->setCellValue('A'.$rowCount, 1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, 2);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, 3);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, 4);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, 5);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, 6);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, 7);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowCount, 8);
	
	$rowCount++;
}
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('covertedXml2Xlsx.xlsx'); 

echo "<pre>";
  //  print_r($sheetData);
echo "</pre>";

print_r($sheetnames); 

exit;

/* $cashRegisterMapper = new Application_Model_CashRegisterMapper();
$cashRegister=$cashRegisterMapper->getAllCashRegister();
$rowCount = 2;
foreach($cashRegisters as $cashRegister)
{
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("cr_id"));
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_id"));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("user_id"));
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("receipt_number"));
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("transaction_id"));
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("cr_entry_type"));
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $consumer->__get("cr_amount"));
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowCount, $consumer->__get("timestamp"));
	$rowCount++;
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('DailyExcelSheet'.date('ddMMYYY').'.xlsx');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
header('Content-type: application/vnd.ms-excel');

// It will be called file.xls
header('Content-Disposition: attachment; filename="file.xlsx"');

// Write file to the browser
$objWriter->save('php://output'); */
?>