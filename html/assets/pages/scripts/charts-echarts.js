var echarts = function() {

    return {
     
    ebarCharts: function(control,billingdata,collectiondata,revenueTotal) {
    	
jQuery(document).ready(function() {
    // ECHARTS
    require.config({
        paths: {
            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
        }
    });

    // DEMOS
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/chord',
            'echarts/chart/eventRiver',
            'echarts/chart/force',
            'echarts/chart/funnel',
            'echarts/chart/gauge',
            'echarts/chart/heatmap',
            'echarts/chart/k',
            'echarts/chart/line',
            'echarts/chart/map',
            'echarts/chart/pie',
            'echarts/chart/radar',
            'echarts/chart/scatter',
            'echarts/chart/tree',
            'echarts/chart/treemap',
            'echarts/chart/venn',
            'echarts/chart/wordCloud'
        ],
        function(ec) {
            //--- BAR ---
        	
        	 var billingdatas = [];
        	 var month_detail=[];
             for (var i = 0; i < billingdata.length; i++){
            	 billingdatas.push(billingdata[i][1]);
            	 month_detail.push(billingdata[i][0]);
             }
              //console.log(month_detail);
             var collectiondatas = [];
             for (var i = 0; i < collectiondata.length; i++){
            	 collectiondatas.push(collectiondata[i][1]);
             }
             var revenueTotals = [];
             for (var j = 0; j < revenueTotal.length; j++){
            	 revenueTotals.push(revenueTotal[j][1]);
             }
 
            var myChart = ec.init(control);
            myChart.setOption({
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['Billing(Debit)', 'Collection(Credit)','Target'],
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {
                            show: true
                        },
                        dataView: {
                            show: true,
                            readOnly: false
                        },
//                        magicType: {
//                            show: true,
//                            type: ['line', 'bar']
//                        },
                        restore: {
                            show: true
                        },
                        saveAsImage: {
                            show: true
                        }
                    }
                },
               calculable: true,
                xAxis: [{
                    type: 'category',
                    data: month_detail,
			axisLabel : {
                	show:true,
                	interval: 'auto',    
               		 rotate: 15,
                	margin: 5,
               		 textStyle: {
                   		fontSize: 12,
                 		}
           		 },

                    splitArea : {
                        show: true,
                        areaStyle:{
                            color:['white','white']
                        }
                    }
                    
                }],
                yAxis: [{
                    type: 'value',
                    splitArea: {
                        show: true,
                    },
                    splitArea : {
                        show: true,
                        areaStyle:{
                            color:['white','white']
                        }
                    }
                    
                }],
                series: [{
                    name: 'Billing(Debit)',
                    type: 'bar',
                    data: billingdatas
                }, {
                    name: 'Collection(Credit)',
                    type: 'bar',
                    data: collectiondatas
                },
                {
                    name:'Target',
                    type: 'line',
                    data: revenueTotals
                }],
            });
        	//}
        }
    );
});
},

ebarChartforMeters: function(control,reading,months) {
	
	jQuery(document).ready(function() {
	    // ECHARTS
	    require.config({
	        paths: {
	            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
	        }
	    });

	   
	    require(
	        [
	            'echarts',
	            'echarts/chart/bar',
	            'echarts/chart/chord',
	            'echarts/chart/eventRiver',
	            'echarts/chart/force',
	            'echarts/chart/funnel',
	            'echarts/chart/gauge',
	            'echarts/chart/heatmap',
	            'echarts/chart/k',
	            'echarts/chart/line',
	            'echarts/chart/map',
	            'echarts/chart/pie',
	            'echarts/chart/radar',
	            'echarts/chart/scatter',
	            'echarts/chart/tree',
	            'echarts/chart/treemap',
	            'echarts/chart/venn',
	            'echarts/chart/wordCloud'
	        ],
	        function(ec) {
	            //--- BAR ---
	        	
	      
	        //console.log(months);
	            var myChart = ec.init(control);
	            myChart.setOption({
	                tooltip: {
	                    trigger: 'axis'
	                },
	                legend: {
	                    data: ['Energy Consumed'],
	                },
	               
	                toolbox: {
	                    show: true,
	                    feature: {
	                        mark: {
	                            show: true
	                        },
	                        dataView: {
	                            show: true,
	                            readOnly: false
	                        },
	                        restore: {
	                            show: true
	                        },
	                        saveAsImage: {
	                            show: true
	                        }
	                    }
	                },
	               calculable: true,
	              
	                xAxis: [{
	                    type: 'category',
	                    data: months,
	                    splitArea : {
	                        show: true,
	                        areaStyle:{
	                            color:['white','white']
	                        }
	                    },
	                    
	                }],
	                yAxis: [{
	                    type: 'value',
	                    splitArea: {
	                        show: true,
	                    },
	                    splitArea : {
	                        show: true,
	                        areaStyle:{
	                            color:['white','white']
	                        }
	                    },
	                   
	                    
	                    
	                    
	                }],
	                series: [{
	                    name: 'Energy Consumed',
	                    type: 'bar',
	                    data: reading
	                }],
	            });
	        	 
	        }
	    );
	});
	},

	ebarChartforMetersSiteWise: function(control,reading,months) {
		
		jQuery(document).ready(function() {
		    // ECHARTS
		    require.config({
		        paths: {
		            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
		        }
		    });

		   
		    require(
		        [
		            'echarts',
		            'echarts/chart/bar',
		            'echarts/chart/chord',
		            'echarts/chart/eventRiver',
		            'echarts/chart/force',
		            'echarts/chart/funnel',
		            'echarts/chart/gauge',
		            'echarts/chart/heatmap',
		            'echarts/chart/k',
		            'echarts/chart/line',
		            'echarts/chart/map',
		            'echarts/chart/pie',
		            'echarts/chart/radar',
		            'echarts/chart/scatter',
		            'echarts/chart/tree',
		            'echarts/chart/treemap',
		            'echarts/chart/venn',
		            'echarts/chart/wordCloud'
		        ],
		        function(ec) {
		            //--- BAR ---
		        	var meter_id=[];
		        	 var readingdatas = [];
		        	 var month_detail=[];
		        	 var totalSession=[];
		             for (var i = 0; i < reading.length; i++){
		            	 meter_id.push(reading[i]["meter_id"]); 
		            	 readingdatas.push(reading[i]["reading"]);
		            	 
		             }	
		             for(var j=0;j<meter_id.length;j++){
		            	 var session = {
		            			 name:meter_id[j], 
		            			 type:"bar", 
		            			 data:readingdatas[j]
		            	};
		            	 
		            	 totalSession.push(session);
		             }
 			if(meter_id.length<=0){
		        	meter_id.push("NULL");
	        		totalSession=[{
		        		 name:"NULL", 
	            			 type:"bar", 
	            			 data:[0,0,0,0,0,0,0,0,0,0,0,0]	
		        	}];
		        }     
		       
		            var myChart = ec.init(control);
		            myChart.setOption({
		                tooltip: {
		                    trigger: 'axis'
		                },
		                legend: {
		                    data: meter_id,
		                },
		               
		                toolbox: {
		                    show: true,
		                    feature: {
		                        mark: {
		                            show: true
		                        },
		                        dataView: {
		                            show: true,
		                            readOnly: false
		                        },
		                        restore: {
		                            show: true
		                        },
		                        saveAsImage: {
		                            show: true
		                        }
		                    }
		                },
		               calculable: true,
		              
		                xAxis: [{
		                    type: 'category',
		                    data: months,
		                    splitArea : {
		                        show: true,
		                        areaStyle:{
		                            color:['white','white']
		                        }
		                    },
		                    
		                }],
		                yAxis: [{
		                	type: 'value',
		                    splitArea: {
		                        show: true,
		                    },
		                    splitArea : {
		                        show: true,
		                        areaStyle:{
		                            color:['white','white']
		                        }
		                    },
		                   
		                    
		                    
		                    
		                }],
		                series: totalSession,
		            });
		        	 
		        }
		    );
		});
		},
		
  	ePieChart: function(control,data_val,totalDebit,total,TypeOfConsumer) {
			
			jQuery(document).ready(function() {
			    // ECHARTS
			    require.config({
			        paths: {
			            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
			        }
			    });

			   
			    require(
			        [
			            'echarts',
			            'echarts/chart/bar',
			            'echarts/chart/chord',
			            'echarts/chart/eventRiver',
			            'echarts/chart/force',
			            'echarts/chart/funnel',
			            'echarts/chart/gauge',
			            'echarts/chart/heatmap',
			            'echarts/chart/k',
			            'echarts/chart/line',
			            'echarts/chart/map',
			            'echarts/chart/pie',
			            'echarts/chart/radar',
			            'echarts/chart/scatter',
			            'echarts/chart/tree',
			            'echarts/chart/treemap',
			            'echarts/chart/venn',
			            'echarts/chart/wordCloud'
			        ],
			        function(ec) {
			        	 
			        	 var TypeOfConsumers=[];
			        	 var TotalConsumer=[];
			             for (var i = 0; i < parseInt(total); i++){
			            	 TypeOfConsumers.push(data_val[i]["name"]); 
			            	 TotalConsumer.push(data_val[i]["value"]);
			             }	
			           
			            var myChart5 = ec.init(control);
			            myChart5.setOption({
			                tooltip: {
			                    show: true,
			                    formatter: "{a} <br/>{b} : {c} ({d}%)"
			                },
			                legend: {
			                    orient: 'vertical',
			                    x: 'left',
			                    data: TypeOfConsumer
			                },
			                toolbox: {
			                    show: true,
			                    feature: {
			                        mark: {
			                            show: true
			                        },
			                        dataView: {
			                            show: true,
			                            readOnly: false
			                        },
			                        restore: {
			                            show: true
			                        },
			                        saveAsImage: {
			                            show: true
			                        }
			                    }
			                },
			                calculable: true,
			                series: [{
			                    name: 'Type Of Consumer',
			                    type: 'pie',
			                    center: ['50%', 200],
			                    radius: 80,
			                    itemStyle: {
			                        normal: {
			                            label: {
			                                position: 'inner',
			                                formatter: function(params) {
			                                    return (params.percent - 0).toFixed(0) + '%'
			                                }
			                            },
			                            labelLine: {
			                                show: false
			                            }
			                        },
			                        emphasis: {
			                            label: {
			                                show: true,
			                                formatter: "{d}%"
			                            }
			                        }

			                    },
			                    data: data_val
			                }, {
			                    name: 'Revenue',
			                    type: 'pie',
			                    center: ['50%', 200],
			                    radius: [110, 140],
			                    data: totalDebit
			                }]
			            });
			        	 
			        }
			    );
			});
			},
ebarChartsConsumer: function(control,timers,vols,convol) {
		 
			jQuery(document).ready(function() {
		    // ECHARTS
		    require.config({
		        paths: {
		            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
		        }
		    });
   
				// DEMOS
				require(
					[
						'echarts',
						'echarts/chart/bar',
						'echarts/chart/chord',
						'echarts/chart/eventRiver',
						'echarts/chart/force',
						'echarts/chart/funnel',
						'echarts/chart/gauge',
						'echarts/chart/heatmap',
						'echarts/chart/k',
						'echarts/chart/line',
						'echarts/chart/map',
						'echarts/chart/pie',
						'echarts/chart/radar',
						'echarts/chart/scatter',
						'echarts/chart/tree',
						'echarts/chart/treemap',
						'echarts/chart/venn',
						'echarts/chart/wordCloud'
					],
				 
					function(ec) {
					var vol=[];
						 setTimeout(function(){ 
							
							for(var i=0;i<vols.length;i++){
								vol[i]= vols[i].toFixed(2) ;
							}		
							console.log(vol);							
						var myChart = ec.init(control);
						myChart.setOption({
							tooltip: {
								trigger: 'axis'
							},
							legend: {
								data: ['VA','Limit'],
							},
							toolbox: {
								show: true,
								feature: {
									mark: {
										show: true
									},
									dataView: {
										show: true,
										readOnly: false
									},

									restore: {
										show: true
									},
									saveAsImage: {
										show: true
									}
								}
							},
							calculable: true,
							xAxis: [{
								type: 'category',
								data: timers,
								axisLabel : {
									show:true,
									interval: 'auto',
									rotate: 15,
									margin: 5,
									textStyle: {
										fontSize: 12,
									}
								},

								splitArea : {
									show: true,
									areaStyle:{
										color:['white','white']
									}
								}

							}],
							yAxis: [{
								type: 'value',
								splitArea: {
									show: true,
								},
								splitArea : {
									show: true,
									areaStyle:{
										color:['white','white']
									}
								}

							}],
							series: [{
								name:'Limit',
								type: 'line',
								data: convol,
								colors:['#b9c246']
							},{
								name: 'VA',
								type: 'bar',
								data: vol,
							 
							}],

						});
						//}
					
					}, 1500);
					}
				);
			});
		},
		ebarcharts_energy: function(control,energy,names,max) {
			
			jQuery(document).ready(function() {
			    // ECHARTS
			    require.config({
			        paths: {
			            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
			        }
			    });

			   
			    require(
			        [
			            'echarts',
			            'echarts/chart/bar',
			            'echarts/chart/chord',
			            'echarts/chart/eventRiver',
			            'echarts/chart/force',
			            'echarts/chart/funnel',
			            'echarts/chart/gauge',
			            'echarts/chart/heatmap',
			            'echarts/chart/k',
			            'echarts/chart/line',
			            'echarts/chart/map',
			            'echarts/chart/pie',
			            'echarts/chart/radar',
			            'echarts/chart/scatter',
			            'echarts/chart/tree',
			            'echarts/chart/treemap',
			            'echarts/chart/venn',
			            'echarts/chart/wordCloud'
			        ],
			        function(ec) {
			 
            var myChart6 = ec.init(control);
            myChart6.setOption({
                tooltip: {
                    show: true,
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
					 
                    data: names
					
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {
                            show: true
                        },
                        dataView: {
                            show: true,
                            readOnly: false
                        },
                        restore: {
                            show: true
                        },
                        saveAsImage: {
                            show: true
                        }
                    }
                },
                calculable: true,
                series: [ {
                    name: 'Energy',
                    type: 'pie',
                    clockWise: true,
                    startAngle: 135,
                    center: ['50%', 200],
                    radius: [80, 120],
                    itemStyle:  {
                        normal: {
                            label: {
                                show: false
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            color: (function() {
                                var zrColor = require('zrender/tool/color');
                                return zrColor.getRadialGradient(
                                    650, 200, 80, 650, 200, 120, [
                                        [0, 'rgba(255,255,0,1)'],
                                        [1, 'rgba(255,0,0,1)']
                                    ]
                                )
                            })(),
                            label: {
                                show: true,
                                position: 'center',
                                formatter: "{d}%",
                                textStyle: {
                                    color: 'red',
                                    fontSize: '30',
                                    fontFamily: 'Open Sans',
                                    fontWeight: 'bold'
                                }
                            }
                        }
                    },
                    data: energy,
                    markPoint: {
                        symbol: 'star',
                        data: [{
                            name: max[0],
                            value: max[1],
                            x: '55%',
                            y: 50,
                            symbolSize: 32
                        }]
                    }
                }]
            });
			}
		
    );
			});
		},
		
		SpiebarCharts: function(control,Maindata,TagName) {
	    	
	    	jQuery(document).ready(function() {
	    	    // ECHARTS
	    	    require.config({
	    	        paths: {
	    	            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
	    	        }
	    	    });

	    	    // DEMOS
	    	    require(
	    	        [
	    	            'echarts',
	    	            'echarts/chart/bar',
	    	            'echarts/chart/chord',
	    	            'echarts/chart/eventRiver',
	    	            'echarts/chart/force',
	    	            'echarts/chart/funnel',
	    	            'echarts/chart/gauge',
	    	            'echarts/chart/heatmap',
	    	            'echarts/chart/k',
	    	            'echarts/chart/line',
	    	            'echarts/chart/map',
	    	            'echarts/chart/pie',
	    	            'echarts/chart/radar',
	    	            'echarts/chart/scatter',
	    	            'echarts/chart/tree',
	    	            'echarts/chart/treemap',
	    	            'echarts/chart/venn',
	    	            'echarts/chart/wordCloud'
	    	        ],
	    	        function(ec) {
	    	            //--- BAR ---
	    	        	
	    	        	 var Main_datas = [];
	    	        	 var month_detail=[];
	    	             for (var i = 0; i < Maindata.length; i++){
	    	            	 Main_datas.push(Maindata[i][1]);
	    	            	 month_detail.push(Maindata[i][0]);
	    	             }
	    	             
	    	            var myChart = ec.init(control);
	    	             
	    	            myChart.setOption({
	    	            	color: [ '#4cabce'],
	    	                tooltip: {
	    	                    trigger: 'axis'
	    	                },
	    	                legend: {
	    	                    data: [TagName],
	    	                },
	    	                toolbox: {
	    	                    show: true,
	    	                    feature: {
	    	                        mark: {
	    	                            show: true
	    	                        },
	    	                        dataView: {
	    	                            show: true,
	    	                            readOnly: false
	    	                        },
	    	                        restore: {
	    	                            show: true
	    	                        },
	    	                        saveAsImage: {
	    	                            show: true
	    	                        }
	    	                    }
	    	                },
	    	               calculable: true,
	    	                xAxis: [{
	    	                    type: 'category',
	    	                    data: month_detail,
	    	                    axisLabel : {
	    	                    	show:true,
	    	                    	interval: 'auto',    
	    	               		 	rotate: 15,
	    	               		 	margin: 5,
	    	               		 	textStyle: {
	    	               		 		fontSize: 12,
	    	                 		}
	    	           		 	},

	    	                    splitArea : {
	    	                        show: true,
	    	                        areaStyle:{
	    	                            color:['white','white']
	    	                        }
	    	                    }
	    	                    
	    	                }],
	    	                yAxis: [{
	    	                    type: 'value',
	    	                    splitArea: {
	    	                        show: true,
	    	                    },
	    	                    splitArea : {
	    	                        show: true,
	    	                        areaStyle:{
	    	                            color:['white','white']
	    	                        }
	    	                    }
	    	                    
	    	                }],
	    	                series: [{
	    	                    name: TagName,
	    	                    type: 'bar',
	    	                    data: Main_datas,
	    	                    label: {
	    	                        normal: {
	    	                            show: true,
	    	                        }
	    	                    }
	    	                }],
	    	            });
	    	         
	    	        }
	    	    );
	    	});
	    	},
	    	SpielineCharts: function(control,Maindata,TagName) {
		    	
		    	jQuery(document).ready(function() {
		    	    // ECHARTS
		    	    require.config({
		    	        paths: {
		    	            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
		    	        }
		    	    });

		    	    // DEMOS
		    	    require(
		    	        [
		    	            'echarts',
		    	            'echarts/chart/bar',
		    	            'echarts/chart/chord',
		    	            'echarts/chart/eventRiver',
		    	            'echarts/chart/force',
		    	            'echarts/chart/funnel',
		    	            'echarts/chart/gauge',
		    	            'echarts/chart/heatmap',
		    	            'echarts/chart/k',
		    	            'echarts/chart/line',
		    	            'echarts/chart/map',
		    	            'echarts/chart/pie',
		    	            'echarts/chart/radar',
		    	            'echarts/chart/scatter',
		    	            'echarts/chart/tree',
		    	            'echarts/chart/treemap',
		    	            'echarts/chart/venn',
		    	            'echarts/chart/wordCloud'
		    	        ],
		    	        function(ec) {
		    	            //--- BAR ---
		    	        	
		    	        	 var Main_datas = [];
		    	        	 var month_detail=[];
		    	             for (var i = 0; i < Maindata.length; i++){
		    	            	 Main_datas.push(Maindata[i][1]);
		    	            	 month_detail.push(Maindata[i][0]);
		    	             }
		    	             
		    	            var myChart = ec.init(control);
		    	             
		    	            myChart.setOption({
		    	            	color: [ '#4cabce'],
		    	                tooltip: {
		    	                    trigger: 'axis'
		    	                },
		    	                legend: {
		    	                    data: [TagName],
		    	                },
		    	                toolbox: {
		    	                    show: true,
		    	                    feature: {
		    	                        mark: {
		    	                            show: true
		    	                        },
		    	                        dataView: {
		    	                            show: true,
		    	                            readOnly: false
		    	                        },
		    	                        restore: {
		    	                            show: true
		    	                        },
		    	                        saveAsImage: {
		    	                            show: true
		    	                        }
		    	                    }
		    	                },
		    	               calculable: true,
		    	                xAxis: [{
		    	                    type: 'category',
		    	                    data: month_detail,
		    	                    axisLabel : {
		    	                    	show:true,
		    	                    	interval: 'auto',    
		    	               		 	rotate: 15,
		    	               		 	margin: 5,
		    	               		 	textStyle: {
		    	               		 		fontSize: 12,
		    	                 		}
		    	           		 	},

		    	                    splitArea : {
		    	                        show: true,
		    	                        areaStyle:{
		    	                            color:['white','white']
		    	                        }
		    	                    }
		    	                    
		    	                }],
		    	                yAxis: [{
		    	                    type: 'value',
		    	                    splitArea: {
		    	                        show: true,
		    	                    },
		    	                    splitArea : {
		    	                        show: true,
		    	                        areaStyle:{
		    	                            color:['white','white']
		    	                        }
		    	                    }
		    	                    
		    	                }],
		    	                series: [{
		    	                    name: TagName,
		    	                    type: 'line',
		    	                    data: Main_datas,
		    	                    label: {
		    	                        normal: {
		    	                            show: true,
		    	                        }
		    	                    }
		    	                }],
		    	            });
		    	         
		    	        }
		    	    );
		    	});
		    	},
	    	SpimultiebarCharts: function(control,Maindata,generation,sold) {
		    	
		    	jQuery(document).ready(function() {
		    	    // ECHARTS
		    	    require.config({
		    	        paths: {
		    	            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
		    	        }
		    	    });

		    	    // DEMOS
		    	    require(
		    	        [
		    	            'echarts',
		    	            'echarts/chart/bar',
		    	            'echarts/chart/chord',
		    	            'echarts/chart/eventRiver',
		    	            'echarts/chart/force',
		    	            'echarts/chart/funnel',
		    	            'echarts/chart/gauge',
		    	            'echarts/chart/heatmap',
		    	            'echarts/chart/k',
		    	            'echarts/chart/line',
		    	            'echarts/chart/map',
		    	            'echarts/chart/pie',
		    	            'echarts/chart/radar',
		    	            'echarts/chart/scatter',
		    	            'echarts/chart/tree',
		    	            'echarts/chart/treemap',
		    	            'echarts/chart/venn',
		    	            'echarts/chart/wordCloud'
		    	        ],
		    	        function(ec) {
		    	            //--- BAR ---
		    	        	
		    	        	 var Main_datas = [];
		    	        	 var month_detail=[];
		    	        	 var generation_arr=[];
		    	        	 var sold_arr=[];
		    	             for (var i = 0; i < Maindata.length; i++){
		    	            	 Main_datas.push(Maindata[i][1]);
		    	            	 generation_arr.push(generation[i][1]);
		    	            	 sold_arr.push(sold[i][1]);
		    	            	 month_detail.push(Maindata[i][0]);
		    	             }
		    	             
		    	            var myChart = ec.init(control);
		    	             
		    	            myChart.setOption({
		    	            	tooltip: {
		    	                    trigger: 'axis'
		    	                },
		    	                legend: {
		    	                    data: ["Avg. Units Generated","Avg. Units Consumed","Avg. Units Sold"],
		    	                },
		    	                toolbox: {
		    	                    show: true,
		    	                    feature: {
		    	                        mark: {
		    	                            show: true
		    	                        },
		    	                        dataView: {
		    	                            show: true,
		    	                            readOnly: false
		    	                        },
		    	                        restore: {
		    	                            show: true
		    	                        },
		    	                        saveAsImage: {
		    	                            show: true
		    	                        }
		    	                    }
		    	                },
		    	               calculable: true,
		    	                xAxis: [{
		    	                    type: 'category',
		    	                    data: month_detail,
		    	                    axisLabel : {
		    	                    	show:true,
		    	                    	interval: 'auto',    
		    	               		 	rotate: 15,
		    	               		 	margin: 5,
		    	               		 	textStyle: {
		    	               		 		fontSize: 12,
		    	                 		}
		    	           		 	},

		    	                    splitArea : {
		    	                        show: true,
		    	                        areaStyle:{
		    	                            color:['white','white']
		    	                        }
		    	                    }
		    	                    
		    	                }],
		    	                yAxis: [{
		    	                    type: 'value',
		    	                    splitArea: {
		    	                        show: true,
		    	                    },
		    	                    splitArea : {
		    	                        show: true,
		    	                        areaStyle:{
		    	                            color:['white','white']
		    	                        }
		    	                    }
		    	                    
		    	                }],
		    	                series: [{
		    	                    name: "Avg. Units Generated",
		    	                    type: 'bar',
		    	                    data: generation_arr,
		    	                    label: {
		    	                        normal: {
		    	                            show: true,
		    	                        }
		    	                    }
		    	                },
		    	                {
		    	                    name: "Avg. Units Consumed",
		    	                    type: 'bar',
		    	                    data: Main_datas,
		    	                    label: {
		    	                        normal: {
		    	                            show: true,
		    	                        }
		    	                    }
		    	                },
		    	                {
		    	                    name: "Avg. Units Sold",
		    	                    type: 'bar',
		    	                    data: sold_arr,
		    	                    label: {
		    	                        normal: {
		    	                            show: true,
		    	                        }
		    	                    }
		    	                }
		    	                ],
		    	            });
		    	         
		    	        }
		    	    );
		    	});
		    	},
		    	
		    	SpiStackCharts: function(control,household,shop,commercial,telecom,consumer) {
			    	
			    	jQuery(document).ready(function() {
			    	    // ECHARTS
			    	    require.config({
			    	        paths: {
			    	            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
			    	        }
			    	    });

			    	    // DEMOS
			    	    require(
			    	        [
			    	            'echarts',
			    	            'echarts/chart/bar',
			    	            'echarts/chart/chord',
			    	            'echarts/chart/eventRiver',
			    	            'echarts/chart/force',
			    	            'echarts/chart/funnel',
			    	            'echarts/chart/gauge',
			    	            'echarts/chart/heatmap',
			    	            'echarts/chart/k',
			    	            'echarts/chart/line',
			    	            'echarts/chart/map',
			    	            'echarts/chart/pie',
			    	            'echarts/chart/radar',
			    	            'echarts/chart/scatter',
			    	            'echarts/chart/tree',
			    	            'echarts/chart/treemap',
			    	            'echarts/chart/venn',
			    	            'echarts/chart/wordCloud'
			    	        ],
			    	        function(ec) {
			    	            //--- BAR ---
			    	        	//household,shop,commercial,telecom,comsumer
			    	        	 var hh = [];
			    	        	 var sh=[];
			    	        	 var comm=[];
			    	        	 var tele=[];
			    	        	 var con=[];
			    	        	 var month_detail=[];
			    	             for (var i = 0; i < household.length; i++){
			    	            	 hh.push(household[i][1]);
			    	            	 sh.push(shop[i][1]);
			    	            	 comm.push(commercial[i][1]);
			    	            	 tele.push(telecom[i][1]);
			    	            	 con.push(consumer[i][1]);
			    	            	 month_detail.push(household[i][0]);
			    	             }
			    	           
			    	            var myChart = ec.init(control);
			    	             
			    	            myChart.setOption({
			    	            	tooltip: {
			    	                    trigger: 'axis'
			    	                },
			    	                legend: {
			    	                    data: ["Household","Shops","Commercial",'Telecom',"Total Customers"],
			    	                },
			    	                toolbox: {
			    	                    show: true
			    	                },
			    	               calculable: true,
			    	                xAxis: [{
			    	                    type: 'category',
			    	                    data: month_detail,
			    	                    axisLabel : {
			    	                    	show:true,
			    	                    	interval: 'auto',    
			    	               		 	rotate: 15,
			    	               		 	margin: 5,
			    	               		 	textStyle: {
			    	               		 		fontSize: 12,
			    	                 		}
			    	           		 	},

			    	                    splitArea : {
			    	                        show: true,
			    	                        areaStyle:{
			    	                            color:['white','white']
			    	                        }
			    	                    }
			    	                    
			    	                }],
			    	                yAxis: [{
			    	                    type: 'value',
			    	                    splitArea: {
			    	                        show: true,
			    	                    },
			    	                    splitArea : {
			    	                        show: true,
			    	                        areaStyle:{
			    	                            color:['white','white']
			    	                        }
			    	                    }
			    	                    
			    	                }],
			    	                series: [{
			    	                    name: "Household",
			    	                    type: 'bar',
			    	                    data: hh,
			    	                    stack:true,
			    	                    label: {
			    	                        normal: {
			    	                            show: true,
			    	                        }
			    	                    }
			    	                },
			    	                {
			    	                    name: "Shops",
			    	                    type: 'bar',
			    	                    data: sh,
			    	                    stack:true,
			    	                    label: {
			    	                        normal: {
			    	                            show: true,
			    	                        }
			    	                    }
			    	                },
			    	                {
			    	                    name: "Commercial",
			    	                    type: 'bar',
			    	                    data: comm,
			    	                    stack:true,
			    	                    label: {
			    	                        normal: {
			    	                            show: true,
			    	                        }
			    	                    }
			    	                },
			    	                {
			    	                    name: "Telecom",
			    	                    type: 'bar',
			    	                    data: tele,
			    	                    stack:true,
			    	                    label: {
			    	                        normal: {
			    	                            show: true,
			    	                        }
			    	                    }
			    	                },
			    	                {
			    	                    name: "Total Customers",
			    	                    type: 'line',
			    	                    data: con,
			    	                    stack:true,
			    	                    label: {
			    	                        normal: {
			    	                            show: true,
			    	                        }
			    	                    }
			    	                }
			    	                ],
			    	            });
			    	         
			    	        }
			    	    );
			    	});
			    	},
			    	
			   SpiStackRevenueCharts: function(control,household,shop,commercial,telecom) {
				    	
				    	jQuery(document).ready(function() {
				    	    // ECHARTS
				    	    require.config({
				    	        paths: {
				    	            echarts: 'http://localhost:8080/tara/html/assets/global/plugins/echarts/'
				    	        }
				    	    });

				    	    // DEMOS
				    	    require(
				    	        [
				    	            'echarts',
				    	            'echarts/chart/bar',
				    	            'echarts/chart/chord',
				    	            'echarts/chart/eventRiver',
				    	            'echarts/chart/force',
				    	            'echarts/chart/funnel',
				    	            'echarts/chart/gauge',
				    	            'echarts/chart/heatmap',
				    	            'echarts/chart/k',
				    	            'echarts/chart/line',
				    	            'echarts/chart/map',
				    	            'echarts/chart/pie',
				    	            'echarts/chart/radar',
				    	            'echarts/chart/scatter',
				    	            'echarts/chart/tree',
				    	            'echarts/chart/treemap',
				    	            'echarts/chart/venn',
				    	            'echarts/chart/wordCloud'
				    	        ],
				    	        function(ec) {
				    	            //--- BAR ---
				    	        	//household,shop,commercial,telecom,comsumer
				    	        	 var hh = [];
				    	        	 var sh=[];
				    	        	 var comm=[];
				    	        	 var tele=[];
				    	        	 var month_detail=[];
				    	        	 for (var i = 0; i < household.length; i++){
				    	            	 hh.push(household[i][1]);
				    	            	 sh.push(shop[i][1]);
				    	            	 comm.push(commercial[i][1]);
				    	            	 tele.push(telecom[i][1]);
				    	            	 month_detail.push(household[i][0]);
				    	            }
				    	             
				    	            var myChart = ec.init(control);
				    	             
				    	            myChart.setOption({
				    	            	tooltip: {
				    	                    trigger: 'axis'
				    	                },
				    	                legend: {
				    	                    data: ["Household","Shops","Commercial",'Telecom'],
				    	                },
				    	                toolbox: {
				    	                    show: true
				    	                },
				    	               calculable: true,
				    	                xAxis: [{
				    	                    type: 'category',
				    	                    data: month_detail,
				    	                    axisLabel : {
				    	                    	show:true,
				    	                    	interval: 'auto',    
				    	               		 	rotate: 15,
				    	               		 	margin: 5,
				    	               		 	textStyle: {
				    	               		 		fontSize: 12,
				    	                 		}
				    	           		 	},

				    	                    splitArea : {
				    	                        show: true,
				    	                        areaStyle:{
				    	                            color:['white','white']
				    	                        }
				    	                    }
				    	                    
				    	                }],
				    	                yAxis: [{
				    	                    type: 'value',
				    	                    splitArea: {
				    	                        show: true,
				    	                    },
				    	                    splitArea : {
				    	                        show: true,
				    	                        areaStyle:{
				    	                            color:['white','white']
				    	                        }
				    	                    }
				    	                    
				    	                }],
				    	                series: [{
				    	                    name: "Household",
				    	                    type: 'bar',
				    	                    data: hh,
				    	                    stack:true,
				    	                    label: {
				    	                        normal: {
				    	                            show: true,
				    	                        }
				    	                    }
				    	                },
				    	                {
				    	                    name: "Shops",
				    	                    type: 'bar',
				    	                    data: sh,
				    	                    stack:true,
				    	                    label: {
				    	                        normal: {
				    	                            show: true,
				    	                        }
				    	                    }
				    	                },
				    	                {
				    	                    name: "Commercial",
				    	                    type: 'bar',
				    	                    data: comm,
				    	                    stack:true,
				    	                    label: {
				    	                        normal: {
				    	                            show: true,
				    	                        }
				    	                    }
				    	                },
				    	                {
				    	                    name: "Telecom",
				    	                    type: 'bar',
				    	                    data: tele,
				    	                    stack:true,
				    	                    label: {
				    	                        normal: {
				    	                            show: true,
				    	                        }
				    	                    }
				    	                }
				    	                ],
				    	            });
				    	         
				    	        }
				    	    );
				    	});
				    	}
		
		
		};
}();