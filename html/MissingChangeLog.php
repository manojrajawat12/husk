
<?php

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** PHPExcel_IOFactory */
require_once '../PHPExcel/PHPExcel/IOFactory.php';

class ReadFilter implements PHPExcel_Reader_IReadFilter {

	public function __construct($fromColumn, $toColumn) {
		$this->columns = array();
		$toColumn++;
		while ($fromColumn !== $toColumn) {
			$this->columns[] = $fromColumn++;
		}
	}

	public function readCell($column, $row, $worksheetName = '') {
		if (in_array($column, $this->columns)) {
			return true;
		}
		return false;
	}
}

$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
$cacheSettings = array( 'memoryCacheSize' => '4096MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

$objReader = PHPExcel_IOFactory::createReader('Excel2007');

$inputFileType = 'Excel2007';
$inputFileName = 'Missing Change Log.xlsx';//'Daily SQL conversion 20150805(new def for Server injection)(1).xlsx';//'template.xlsx' ;//'Daily SQL conversion.xlsx';
$sheetIndex = 0;

$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$sheetnames = $objReader->listWorksheetNames($inputFileName);
$objPHPExcel = $objReader->load($inputFileName);
$objPHPExcel->setActiveSheetIndex(0);
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);

$highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();
$highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
$array=[];
for ($row = 2; $row <= $highestRow; $row++){
	//  Read a row of data into an array
	$rowData = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $row . ':' . $highestColumn . $row,
			NULL,
			TRUE,
			FALSE);
	//  Insert row data array into your database of choice here
	//print_r($rowData);
	//echo $rowData[0][0];
	 $who= $rowData[0][1];
	 $what=$rowData[0][2];
	 $rowData[0][3];
	 $array_data=array ($who,$what,$rowData[0][3]);
	//header("Location: http://localhost:8080/tara/html/admin/ajax/long-code?who=$who&what=$what");
	array_push($array, $array_data);
	
}
//print_r($sheetnames);
//print_r($array);
$json = json_encode($array, JSON_PRETTY_PRINT);
echo $json;

?>