<?php

class Admin_AjaxController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction() {
        $consumersMapper = new Application_Model_ConsumersMapper();
        print_r($consumersMapper->getTotalConsumersBySite(17));
    }

    public function longCodeAction() {
		
        $request = $this->getRequest();
        $who = $request->getParam("who");
        $what = $request->getParam("what");
        $what_string = $what;
        $number = $who ;
	
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        $collectionAgent = $collectionAgentsMapper->getCollectionAgentByPhone($number);

        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
        $consumersMapper = new Application_Model_ConsumersMapper();
        $what = explode(" ", $what);
        $type = $what[1];
        $zendDate = new Zend_Date();
        $zendDate->setTimezone("Asia/Calcutta");
        if($type=="PICO" or $type=="AP1" or $type=="AP2" or $type=="AP3" or $type=="TMTS1"  or $type=="TMTS2")
        {
            $type = strtolower($type);
            $string = "Number - ".$who;
            $string .= "|";
            $string .= "Message - ".$what[2];
            $string .= "|";
            $zendDate = new Zend_Date();
            $zendDate->setTimezone("Asia/Calcutta");
            $string .= "Time - ".$zendDate->toString();
            $file = $type.'.txt';
            $current = file_get_contents($file);
            $current .= $string . "\r\n";
            file_put_contents($file, $current);
            exit;
        }
        if ($type == "BAL") {
            $result = $this->getConsumerBalance($number);
        } else {
            if ($collectionAgent) {
                $customer_code = $what[2];
                $con = $consumersMapper->getConsumersByColumnValue("consumer_connection_id", $customer_code);
                switch ($type) {
                    case "CID2PKG":
                        if($con)
                        {
                            $package_id = $con[0]->__get("package_id");
                            $result = "Package id of the customer is ".$package_id;
                        }
                        else
                        {
                            $result = "Connection id not found";
                        }
                        break;
                    case "CABAL":
                        $amount = abs($what[3]);
                        $b = $collectionAgent->__get("agent_balance");
                        $name = $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");;
                        $result = "Dear " . $name . ", your current Account Balance is Rs. " . $b . ".";
                        break;
                    case "ACT":
                        $amount = abs($what[3]);
                        $result = $this->rechargeConsumer($collectionAgent, $type, $amount, $customer_code);
                        break;
                    case "CHG":
                        $amount = abs($what[3]);
                        $result = $this->rechargeConsumer($collectionAgent, $type, $amount, $customer_code);
                        break;
                    case "CNC":
                        $amount = abs($what[3]);
                        $result = $this->addFollowup($collectionAgent, $type, $amount, $customer_code);
                        break;
                    case "MTR":
                        if(!$con)
                        {
                            $result = "Error. Customer ID ".$customer_code." not found. Please check.";
                        }
                        elseif($con[0]->__get("is_micro_enterprise")==0)
                        {
                            $packagesMapper = new Application_Model_PackagesMapper();
                            $package_id = $con[0]->__get("package_id");
                            $package = $packagesMapper->getPackageById($package_id);
                            $package_name = $package->__get("package_name");
                            $result = "Connection ID ".$customer_code." is not a Metered Connection. Please check Connection ID and try again. ".$con[0]->__get("consumer_name").", s/o ".$con[0]->__get("consumer_father_name")." with Package ".$package_name.".";
                        }
                        else
                        {
                            $latestReading = $meterReadingsMapper->getLatestReadingByConsumerId($con[0]->__get("consumer_id"));
                            $start_reading = 0;
                            if($latestReading)
                            {
                                $start_reading = $amount = abs($latestReading->__get("meter_reading"));
                            }
                            $end_reading = $amount = abs($what[3]);
                            if($end_reading>$start_reading)
                            {
							
                                $result = $this->addMTR($collectionAgent, $start_reading, $end_reading, $customer_code);
                            }
                            else
                            {
                                $connection_id = $con[0]->__get("consumer_connection_id");
                                $result = "Error. Previous Reading for ".$connection_id." was ".$start_reading.". New reading must be bigger. Please check and try again.";
                            }
                        }
                        break;
                    case "ENROL":
                        //TARA ENROL BHFAKT:ARJUN_DHAWAN:SANJAY_DHAWAN:9650408880:012:0600:0120
                        $data = explode(":",$customer_code);
                        if(count($data)!=11)
                        {
                            $result = "Invalid request";
                            break;
                        }
                        else
                        {
                            $site_code = $data[0];
                            $generated_connection_id = $data[1];
                            $consumer_name = ucwords(strtolower(str_replace("_", " ", $data[2])));
                            $consumer_father_name = ucwords(strtolower(str_replace("_", " ", $data[3])));
                            $consumer_code = $data[4];
                            $package_id = intval($data[5]);
                            $micro_price = intval($data[6]);
                            $meter_start_reading = intval($data[7]);
                            $activation_charge = intval($data[8]);
                            $activation_amount = intval($data[9]);
                            $collection_amount = intval($data[10]);
                            $sitesMapper = new Application_Model_SitesMapper();
                            $site = $sitesMapper->getSiteBySiteCode($site_code);
                            if($site->__get("site_name")=="---Deleted---")
                            {
                                $result = "Site not found";
                                break;
                            }
                            $zendDate = new Zend_Date();
                            $zendDate->setTimezone("Asia/Calcutta");
                            $activation_date = $zendDate->toString("yyyy-MM-dd HH:mm:ss");
                            $site_id = $site->__get("site_id");
                            $consumersMapper = new Application_Model_ConsumersMapper();
                            $consumer = new Application_Model_Consumers();
                            $packagesMapper = new Application_Model_PackagesMapper();
                            $package = $packagesMapper->getPackageById($package_id);
                            $package_name = "N/A";
                            if($package)
                            {
                                $package_name = $package->__get("package_name");
                            }
                            $consumer->__set("site_id",$site_id);
                            $consumer->__set("package_id",$package_id);
                            $consumer->__set("consumer_code",$consumer_code);
                            $consumer->__set("consumer_name",$consumer_name);
                            $consumer->__set("consumer_father_name",$consumer_father_name);
                            $consumer->__set("consumer_status","new");
                            $consumer->__set("consumer_act_charge",$activation_charge);
                            $consumer->__set("consumer_act_date",$activation_date);
                            $consumer->__set("micro_entrprise_price",$micro_price);
                            $consumer->__set("is_micro_enterprise","0");
                            $consumer->__set("sms_opt_in","1");
                            if($micro_price!=0)
                            {
                                $consumer->__set("is_micro_enterprise","1");
                                $consumer->__set("meter_start_reading",$meter_start_reading);
                            }
                            
                            $consumer_id = $consumersMapper->addNewConsumer($consumer);
                            $connection_id = $site_code.$consumer_id;
                            if($generated_connection_id!="00000")
                            {
                                $new_connection_id = $site_code.$generated_connection_id;
                                $getConsumer = $consumersMapper->getConsumerByConnectionID($new_connection_id);
                                if($getConsumer->__get("consumer_name")=="")
                                {
                                    $connection_id=$new_connection_id;
                                    $duplicate = false;
                                }
                                else
                                {
                                    $result = "Duplicate Connection ID found (".$getConsumer->__get("consumer_name")." s/o ".$getConsumer->__get("consumer_father_name").", ID - ".$new_connection_id."). Generating Temporary ID ".$connection_id.". Please update correct ID with HQ as soon as possible.";
                                    $this->_smsNotification($number, $result);
                                    $duplicate = true;
                                }
                            }
                            
                            
                            
                            $consumer->__set("consumer_connection_id",$connection_id);
                            $consumer->__set("consumer_id",$consumer_id);
                            $consumersMapper->updateConsumer($consumer);
                            $result = "Consumer added successfully";
                            if($consumer_id)
                            {
                                $result = $this->rechargeConsumer($collectionAgent, "ACT", $activation_amount, $connection_id,false);
                                $result = $this->rechargeConsumer($collectionAgent, "CHG", $collection_amount, $connection_id,false);
                                $meterReading = new Application_Model_MeterReadings();
                                $meterReading->__set("consumer_id",$consumer_id);
                                $meterReading->__set("meter_reading",$meter_start_reading);
                                $meterReadingsMapper->addNewMeterReading($meterReading);
                                if($package_id==5)
                                {
                                    $sms = "
    Customer ".$consumer_name." s/o ".$consumer_father_name.". Mobile No. ".$consumer_code.".  Connection ID ".$connection_id.". Metered Connection. Total OTP Due ".$activation_charge.". OTP In ".$activation_amount.". Advance Amt. ".$collection_amount.". Meter Start Reading ".$meter_start_reading.". Rate Rs.".$micro_price." per unit.";
                                    $this->_smsNotification($number, $sms);
                                }
                                else
                                {
                                    if($duplicate)
                                    {
                                        $result = "Customer ".$consumer_name." s/o ".$consumer_father_name.". Mobile No. ".$consumer_code.". Temp. Connection ID ".$connection_id.". Package ".$package_name.". Total OTP ".$activation_charge.". OTP In ".$activation_amount.". Advance Amt. ".$collection_amount.".";
                                    }
                                    else
                                    {
                                        $result = "Customer ".$consumer_name." s/o ".$consumer_father_name.". Mobile No. ".$consumer_code.". Connection ID ".$connection_id.". Package ".$package_name.". Total OTP ".$activation_charge.". OTP In ".$activation_amount.". Advance Amt. ".$collection_amount.".";
                                    }
                                }
                            }
                            break;
                        }
					case "CHGPKG":
                		$package_id = $what[3];
                		$result=$this->changePackage($collectionAgent, $package_id, $customer_code);
                		
                	break;
                	case "CHKPKG":
                		$result=$this->checkPackage($collectionAgent,$customer_code);
                		break; 
                    default:
                        //$result = "Invalid request";
                        $result = "Error. Kripya kuch der baad try karein.";
                        break;
                }
            } else {
                //$result = "Error, you are not a valid POS! Please call up your cluster manager to resolve this issue.";
                $result = "Error. You are not an Agent.";
            }
        }
       $text= $this->_smsNotification($number, $result);
        $file = 'people.txt';
        $current = file_get_contents($file);
        $current .= "IN(".$who."):".$what_string." ---- Out:".$result . " ---- Time - ".$zendDate->toString().$text."\r\n";
        //echo $current;exit;
        file_put_contents($file, $current);
		
		$file = 'test.txt';
        $current = file_get_contents($file);
        $current .= $result."".$text."-".$zendDate->toString() . "\r\n";
        file_put_contents($file, $current);
    }

    
    public function sendWarningSmsAction(){
        
        $connections_arr = array("AMANTEST","ARJUN","GCV","SID","KVL","VIC","PAT","ZEN","FNG","SDA","KRITI","KAN");
        
        $consumerMapper = new Application_Model_ConsumersMapper();
        foreach($connections_arr as $connection_id){
                $consumers = $consumerMapper->getConsumersByConnectionId($connection_id);
               
                foreach($consumers as $consumer){
                    print_r($consumer);
                    $number = $consumer->__get("consumer_code");
                    $sms = "Dear ".$consumer->__get("consumer_name").", your payment is overdue. Please pay your local TARAoorja agent immediately to enjoy uninterrupted services. ";
                    
                    $result = $this->_smsNotification($number, $sms);
                   
                       echo $result;
                   
                }
        }
    }
    private function addMTR($collectionAgent, $start_reading, $end_reading, $customer_code) {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
	   	$enterprisePriceMappper = new Application_Model_EnterprisePriceMapper();
    	$consumerSchemeMapper=new Application_Model_ConsumerSchemeMapper();
        $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
		  
        if ($consumers) {
            $consumer = $consumers[0];
            $reading = $end_reading-$start_reading;
			$package_id=$consumer->__get("package_id");
    		$consumer_id=$consumer->__get("consumer_id");
    		$consumer_number=$consumer->__get("consumer_code");
    		$unit_price = $consumer->__get("micro_entrprise_price");
			$previousOutstanding = $consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
    		$amount = $reading*$unit_price;
			$micro_price=$unit_price;
			$enterprise_type=$consumer->__get("enterprise_type");
    		if($package_id ==5 && $enterprise_type =="Scheme")
    		{
    		
    			$unit_price="";
    			$tot_amount=0;
				 
    			$consumerSchemes=$consumerSchemeMapper->getConsumerschemeByConsumerId($consumer_id);
				$scheme_id=1;
    			if($consumerSchemes)
    			{
    				$scheme_id=$consumerSchemes->__get("consumer_scheme");
    			}
    			  			
    			$schemeUnitPrices=$enterprisePriceMappper->getEnterprisePriceBySchemeId($scheme_id);
    			$cal_amount=0;$last_unit_price=0;
    			$cal_reading=$reading;
    			$amount=$tot_amount;
    			$unit_price_val = array();
    			foreach ($schemeUnitPrices as $schemeUnitPrice) {
    				$unit_from = $schemeUnitPrice->__get("unit_from");
    				$unit_to = $schemeUnitPrice->__get("unit_to");
    				$enterprise_price =$schemeUnitPrice->__get("enterprise_price");
    
    				$unit_between=$unit_to-($unit_from-1);
    				if($cal_reading>0){
    				$unit_between=($cal_reading>=$unit_between)?$unit_between:$cal_reading;
    				$cal_reading=$cal_reading-$unit_between;
    				$cal_amount=$unit_between*$enterprise_price;
    				$tot_amount=$tot_amount+$cal_amount;
    				$last_unit_price=$enterprise_price;
    				$unit_price.=($unit_price== "")?$unit_from."-".$unit_to." Rs.".$enterprise_price."/unit": " ".$unit_from."-".$unit_to." Rs.".$enterprise_price."/unit";
    				}
    				
    				}

    			if($cal_reading >0)
    			{
    				$cal_amount=$cal_reading*$last_unit_price;
    				$tot_amount=$tot_amount+$cal_amount;
    			}
    			 $amount=$tot_amount;
				
    		}
            if ($collectionAgent->__get("agent_balance") >= $amount) {
                
                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $recentCR = $cashRegisterMapper->getLastestEntryByConsumerId($consumer->__get("consumer_id"));
                $date = new Zend_Date();
                $date->setTimezone("Asia/Kolkata");
                
                $timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));

                $tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
                $transaction_id = $timestamp . "-" . $tcr_amount;

                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $cashRegister = new Application_Model_CashRegister();
                $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
                $cashRegister->__set("cr_entry_type", "DEBIT");
                $cashRegister->__set("cr_amount", $amount);
                $cashRegister->__set("transaction_id", $transaction_id);
                $cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
                $cashRegister->__set("receipt_number", "CA-" . $collectionAgent->__get("collection_agent_id"));
				$cashRegister->__set("mtr_entry", '1');
				
                if ($cashRegisterMapper->addNewCashRegister($cashRegister)) {
                    $currentOutstanding = $consumersMapper->getConsumerOutstanding($consumer->__get("consumer_id"));
                    $caTransactionsMapper = new Application_Model_CaTransactionsMapper();
                    $caTransactions = new Application_Model_CaTransactions();
                    $caTransactions->__set("collection_agent_id", $collectionAgent->__get("collection_agent_id"));
                    $caTransactions->__set("transaction_type", "MINUS");
                    $caTransactions->__set("transaction_amount", $amount);
                    $caTransactions->__set("remarks", "Consumer recharge for Consumer Code : " . $consumer->__get("consumer_code"));
                    $ca_amount = $collectionAgent->__get("agent_balance");
                    $balance = $ca_amount - $amount;
                    $collectionAgent->__set("agent_balance", $balance);
                    $collectionAgentsMapper->updateCollectionAgent($collectionAgent);
                    if ($caTransactionsMapper->addNewCaTransaction($caTransactions)) {
                        $meterReadingsMapper = new Application_Model_MeterReadingsMapper();
                        $meterReading = new Application_Model_MeterReadings();
                        $meterReading->__set("meter_reading",$end_reading);
                        $meterReading->__set("consumer_id",$consumer->__get("consumer_id"));
                        $meterReadingsMapper->addNewMeterReading($meterReading);
                      
					  if($package_id==5 && $package_id ==5 && $enterprise_type =="Scheme"){
                        	$result = "Connection ID ".$consumer->__get("consumer_connection_id")." ka bill Rs.".$amount." hai. Old Meter Reading ".$start_reading.". New Meter Reading ".$end_reading.". Total Unit ".$reading.". Rate for ".$unit_price." hai.";
                        }
                        else{
 							$result = "Connection ID ".$consumer->__get("consumer_connection_id")." ka abhi ka bill amount Rs.".$amount." hai. Pichli Meter Reading ".$start_reading." . Abhi ki Meter Reading ".$end_reading." . Total Unit ".$reading." . Rate Rs".$unit_price."/unit. Customer ke pichley Rs.".$previousOutstanding." abhi bhi bakaya hain. Customer se total Rs.".$currentOutstanding." collect kariye.";
                        }
                        
                      /*  $result = "Connection ID ".$consumer->__get("consumer_connection_id")." ka abhi ka bill amount Rs.".$amount." hai. Pichli Meter Reading ".$start_reading." . Abhi ki Meter Reading ".$end_reading." . Total Unit ".$reading." . Rate Rs".$unit_price."/unit. Customer ke pichley Rs.".$previousOutstanding." abhi bhi bakaya hain. Customer se total Rs.".$currentOutstanding." collect kariye.";*/
                        $name = $consumer->__get("consumer_name");
                        $sms = "Namastey ".$name."! Aapkey TARAurja Connection ".$consumer->__get("consumer_connection_id")." ke liye hamein Rs.".$amount." prapt ho gaye hain. ".$transaction_id.".";
                        $sms1 = "Namastey ".$name."! Aapkey TARAurja Connection ".$consumer->__get("consumer_connection_id")." ka bill Rs.".$amount." hai. Yeh bill pichli reading ".$start_reading." aur ab ki reading ".$end_reading." ke hisaab se ".$reading." unit ka bill hai. Aapki bijli ka daam Rs.".$unit_price."/unit hai.";
                        $sms2 = "Namastey ".$name."! Aapkey TARAurja Connection ".$consumer->__get("consumer_connection_id")." ka bill Rs.".$amount." hai. Yeh bill pichli reading ".$start_reading." aur ab ki reading ".$end_reading." ke hisaab se ".$reading." unit ka bill hai. Aapkey pichley bill ke anusaar, Rs.".$previousOutstanding." bakaaya hai. Kripya TARAurja Agent ko total Rs.".$currentOutstanding." ki payment karein.";
                        //$sms = "Dear " . $name . ", Rs." . $amount . " credited to your account " . $consumer->__get("consumer_connection_id") . ". Transaction ID " . $transaction_id . ".";
                        if(true)//$consumer->__get("sms_opt_in")==1)
                        {
                            //$this->_smsNotification($consumer->__get("consumer_code"), $sms);
                            $this->_smsNotification($consumer->__get("consumer_code"), $sms2);
                        }
                        else
                        {
                            //$result = "Consumer has opted out of SMS Facility";
                        }
                    } else {
                        $result = "Technical Error, Could not deduct balance from CA Account";
                        //$result = "Error. Kripya kuch der baad try karein.";
                    }
                } else {
                    $result = "Technical Error, Could not add transaction. Please try again";
                    //$result = "Error. Kripya kuch der baad try karein.";
                }
            } else {
                //$result = "Customer not found. Please check the Customer ID and try again.";
                $result = "Error. Customer ID ".$customer_code." not found. Please check.";
            }
        } else {
            //$result = "Error, not enough cash balance, please recharge your CA Account to continue.";
            $result = "Error. Not enough balance.";
        }
        return $result;
    }
    private function addFollowup($collectionAgent, $t_type, $amount, $customer_code)
    {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
        if ($consumers) {
            $consumer = $consumers[0];
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $date = new Zend_Date();
            $date->setTimezone("Asia/Kolkata");
            $timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));

            $tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
            $transaction_id = $timestamp . "-" . $tcr_amount;

            $cashRegister = new Application_Model_CashRegister();
            $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
            $cashRegister->__set("cr_entry_type", "FOLLOWUP");
            $cashRegister->__set("cr_amount", $amount);
            $cashRegister->__set("transaction_id", $transaction_id);
            $cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
            $cashRegister->__set("receipt_number", "CA-" . $collectionAgent->__get("collection_agent_id"));
            $id = $cashRegisterMapper->addNewCashRegister($cashRegister);
            echo $id;exit;
            if ($id) {
                $result = "Follow up added";
            } else {
                $result = "Technical Error, Could not add transaction. Please try again";
                //$result = "Error. Kripya kuch der baad try karein.";
            }
        } else {
            //$result = "Customer not found. Please check the Customer ID and try again.";
            $result = "Error. Customer ID ".$customer_code." not found. Please check.";
        }
        
        return $result;
    }
    private function rechargeConsumer($collectionAgent, $t_type, $amount, $customer_code,$send_sms=true) {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
        if ($collectionAgent->__get("agent_balance") >= $amount) {
            $consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
            if ($consumers) {
                $consumer = $consumers[0];
                if ($t_type == "ACT") {
                    $transaction_type = "ACTIVATION";
                }
                elseif($t_type == "CNC"){
                    $transaction_type = "FOLLOWUP";
                }
                else {
                    $transaction_type = "CREDIT";
                }
                
                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $recentCR = $cashRegisterMapper->getLastestEntryByConsumerId($consumer->__get("consumer_id"));
                $date = new Zend_Date();
                $date->setTimezone("Asia/Kolkata");
                if(false)//$recentCR)
                {
                    if($recentCR->__get("cr_entry_type")==$transaction_type)
                    {
                        $entryDate = new Zend_Date($recentCR->__get("timestamp"),"yyyy-MM-dd HH:mm:ss");
                        $entryDate->addDay(1);
                        if($entryDate->isLater($date))
                        {
                            return "Error. Duplicate Entry. Please try again.";
                        }
                    }
                }
                
                $timestamp = $date->toString("ddMMYYHHmmss".rand(10,99));

                $tcr_amount = str_pad($amount, 4, "0", STR_PAD_LEFT);
                $transaction_id = $timestamp . "-" . $tcr_amount;

                $cashRegisterMapper = new Application_Model_CashRegisterMapper();
                $cashRegister = new Application_Model_CashRegister();
                $cashRegister->__set("consumer_id", $consumer->__get("consumer_id"));
                $cashRegister->__set("cr_entry_type", $transaction_type);
                $cashRegister->__set("cr_amount", $amount);
                $cashRegister->__set("transaction_id", $transaction_id);
                $cashRegister->__set("user_id", $collectionAgent->__get("collection_agent_id"));
                $cashRegister->__set("receipt_number", "CA-" . $collectionAgent->__get("collection_agent_id"));
                if ($cashRegisterMapper->addNewCashRegister($cashRegister)) {
                    $caTransactionsMapper = new Application_Model_CaTransactionsMapper();
                    $caTransactions = new Application_Model_CaTransactions();
                    $caTransactions->__set("collection_agent_id", $collectionAgent->__get("collection_agent_id"));
                    $caTransactions->__set("transaction_type", "MINUS");
                    $caTransactions->__set("transaction_amount", $amount);
                    $caTransactions->__set("remarks", "Consumer recharge for Consumer Code : " . $consumer->__get("consumer_code"));
                    $ca_amount = $collectionAgent->__get("agent_balance");
                    $balance = $ca_amount - $amount;
                    $collectionAgent->__set("agent_balance", $balance);
                    $collectionAgentsMapper->updateCollectionAgent($collectionAgent);
                    if ($caTransactionsMapper->addNewCaTransaction($caTransactions)) {
                        //$result = "Connection ID ".$consumer->__get("consumer_connection_id")." charged successfully with Rs.".$amount.". Your balance is ".$balance.". Transaction ID ".$transaction_id.".";
                        //$result = "Connection ID ".$consumer->__get("consumer_connection_id")." - Rs.".$amount.". ".$transaction_id.".";
                        $result = "Connection ID ".$consumer->__get("consumer_connection_id")." - Rs.".$amount.". ".$transaction_id.". ".$consumer->__get("consumer_name")." s/o ".$consumer->__get("consumer_father_name").".";
                        //$result = "Customer account has been recharged successfully. Transaction id " . $transaction_id . ". Your balance is Rs. " . $balance;
                        $name = $consumer->__get("consumer_name");
                        $sms = "Namastey ".$name."! Aapkey TARAurja Connection ".$consumer->__get("consumer_connection_id")." ke liye hamein Rs.".$amount." prapt ho gaye hain. ".$transaction_id.".";
                        //$sms = "Dear " . $name . ", Rs." . $amount . " credited to your account " . $consumer->__get("consumer_connection_id") . ". Transaction ID " . $transaction_id . ".";
                        if($consumer->__get("sms_opt_in")==1 and $send_sms)
                        {
                            $this->_smsNotification($consumer->__get("consumer_code"), $sms);
                        }
                        else
                        {
                            //$result = "Consumer has opted out of SMS Facility";
                        }
                    } else {
                        $result = "Technical Error, Could not deduct balance from CA Account";
                        //$result = "Error. Kripya kuch der baad try karein.";
                    }
                } else {
                    $result = "Technical Error, Could not add transaction. Please try again";
                    //$result = "Error. Kripya kuch der baad try karein.";
                }
            } else {
                //$result = "Customer not found. Please check the Customer ID and try again.";
                $result = "Error. Customer ID ".$customer_code." not found. Please check.";
            }
        } else {
            //$result = "Error, not enough cash balance, please recharge your CA Account to continue.";
            $result = "Error. Not enough balance.";
        }
        return $result;
    }

    private function getConsumerBalance($number) {
        $consumersMapper = new Application_Model_ConsumersMapper();
        $currentConsumer = $consumersMapper->searchByColumnValue("consumer_code", $number);
        if (!$currentConsumer) {
            //$result = "Invalid request";
            $result = "Error. Kripya kuch der baad try karein.";
        } else {
            $currentConsumer = $currentConsumer[0];
            $consumer_id = $currentConsumer->__get("consumer_id");
            $outstanding = $consumersMapper->getConsumerOutstanding($consumer_id);
            $activation = $consumersMapper->getRemainingActivation($consumer_id);
            $total = $outstanding + $activation;
            $result = "Dear " . $currentConsumer->__get("consumer_name") . ", your TOTAL outstanding amount is Rs." . $total . " including Rs." . $activation . " of connection charges.";
        }
        return $result;
    }

    public function getSiteByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $site_id = $request->getParam("site_id");
            $sitesMapper = new Application_Model_SitesMapper();
            $site = $sitesMapper->getSiteById($site_id);
            if ($site) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "site_id" => $site->__get("site_id"),
                    "site_name" => $site->__get("site_name"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getMappingClusterByIdAction() {
        $request = $this->getRequest();
        
            $id = $request->getParam("id");
            $mappingMapper = new Application_Model_MappingClusterSitesMapper();
            $mapping = $mappingMapper->getMappingClusterSiteById($id);
            if ($mapping) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "id" => $mapping->__get("mapping_cluster_site_id"),
                    "site_id" => $mapping->__get("site_id"),
                    "cluster_manager_id" => $mapping->__get("cluster_manager_id"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
       
    }

    public function getPackageByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $packagesMapper = new Application_Model_PackagesMapper();
            $package = $packagesMapper->getPackageById($id);
            if ($package) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "package_id" => $package->__get("package_id"),
                    "package_name" => $package->__get("package_name"),
                    "package_cost" => $package->__get("package_cost"),
                    "package_details" => $package->__get("package_details"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getConsumerByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumer = $consumersMapper->getConsumerById($id);
            if ($consumer) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $actDate = $consumer->__get("consumer_act_date");
                if (strlen($actDate) > 2) {
                    $zendDate = new Zend_Date($actDate, "yyyy-MM-dd HH:mm:ss");
                    $date = $zendDate->toString("dd/MM/yyyy");
                } else {
                    $date = "";
                }
                $data = array(
                    "consumer_id" => $consumer->__get("consumer_id"),
                    "consumer_code" => $consumer->__get("consumer_code"),
                    "consumer_name" => $consumer->__get("consumer_name"),
                    "consumer_father_name" => $consumer->__get("consumer_father_name"),
                    "site_id" => $consumer->__get("site_id"),
                    "package_id" => $consumer->__get("package_id"),
                    "consumer_status" => $consumer->__get("consumer_status"),
                    "consumer_act_date" => $date,
                    "consumer_act_charge" => $consumer->__get("consumer_act_charge"),
                    "consumer_connection_id" => $consumer->__get("consumer_connection_id"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getAdminByIdAction() {
        $request = $this->getRequest();
        
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $adminsMapper = new Application_Model_AdminsMapper();
            $admin = $adminsMapper->getAdminById($id);
            if ($admin) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "admin_id" => $admin->__get("admin_id"),
                    "username" => $admin->__get("username"),
                    "admin_fname" => $admin->__get("admin_fname"),
                    "admin_lname" => $admin->__get("admin_lname"),
                    "admin_email" => $admin->__get("admin_email"),
                    "admin_role" => $admin->__get("admin_role"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getClusterManagerByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $clusterMapper = new Application_Model_ClusterManagersMapper();
            $clusterManager = $clusterMapper->getClusterManagerById($id);
            if ($clusterManager) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "id" => $clusterManager->__get("cluster_manager_id"),
                    "name" => $clusterManager->__get("cluster_manager_name"),
                    "phone" => $clusterManager->__get("cluster_manager_phone"),
                    "email" => $clusterManager->__get("cluster_manager_email")
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getUserByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $usersMapper = new Application_Model_UsersMapper();
            $user = $usersMapper->getUserById($id);
            if ($user) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "user_id" => $user->__get("user_id"),
                    "username" => $user->__get("username"),
                    "user_fname" => $user->__get("user_fname"),
                    "user_lname" => $user->__get("user_lname"),
                    "user_email" => $user->__get("user_email"),
                    "phone" => $user->__get("phone"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getEntryTypeByIdAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $cashRegisterMapper = new Application_Model_CashRegisterMapper();
            $cashRegister = $cashRegisterMapper->getCashRegisterById($id);
            if ($cashRegister) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array(
                    "cr_id" => $cashRegister->__get("cr_id"),
                    "consumer_id" => $cashRegister->__get("consumer_id"),
                    "user_id" => $cashRegister->__get("user_id"),
                    "receipt_number" => $cashRegister->__get("receipt_number"),
                    "cr_entry_type" => $cashRegister->__get("cr_entry_type"),
                    "cr_amount" => $cashRegister->__get("cr_amount"),
                );
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function generateBackupAction() {
      
        $db = My_Database::getDetails();
        $host = $db["host"];
        $user = $db["username"];
        $pass = $db["password"];
        $name = $db["dbname"];
        $tables = '*';
        $handle = $this->generateSQLFile($host, $user, $pass, $name, $tables);
        print_r($handle); die;
        if ($handle) {
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $data = array();
        } else {
            $meta = array(
                "code" => 500,
                "message" => "ERROR"
            );
            $data = array();
        }
        $array = array(
            "meta" => $meta,
        );
        $json = json_encode($array);
        echo $json;
    }

    public function importBackupAction() {
        $db = My_Database::getDetails();
        $filename = 'db-backup.sql';
        $mysql_host = $db["host"];
        $mysql_username = $db["username"];
        $mysql_password = $db["password"];
        $mysql_database = $db["dbname"];
        mysql_connect($mysql_host, $mysql_username, $mysql_password) or die('Error connecting to MySQL server: ' . mysql_error());
        mysql_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysql_error());
        $templine = '';
        $lines = file($filename);
        foreach ($lines as $line) {
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;
            $templine .= $line;
            if (substr(trim($line), -1, 1) == ';') {
                mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
                $templine = '';
                $flag = false;
            }
        }
        if ($flag) {
            $meta = array(
                "code" => 200,
                "message" => "SUCCESS"
            );
            $data = array();
        } else {
            $meta = array(
                "code" => 500,
                "message" => "ERROR"
            );
            $data = array();
        }
        $array = array(
            "meta" => $meta,
        );
        $json = json_encode($array);
        echo $json;
    }

    public function generateSQLFile($host, $user, $pass, $name, $tables) {
        $link = mysqli_connect($host, $user, $pass);
        mysql_select_db($name, $link);

        //get all of the tables
        if ($tables == '*') {
            $tables = array();
            $result = mysql_query('SHOW TABLES');
            while ($row = mysql_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }
        $return = "";
        //cycle through
        foreach ($tables as $table) {
            $result = mysql_query('SELECT * FROM ' . $table);
            $num_fields = mysql_num_fields($result);

            $return.= 'DROP TABLE ' . $table . ';';
            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
       
            $return.= "\n\n" . $row2[1] . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysql_fetch_row($result)) {
                    $return.= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $return.= '"' . $row[$j] . '"';
                        } else {
                            $return.= '""';
                        }
                        if ($j < ($num_fields - 1)) {
                            $return.= ',';
                        }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }

        //save file
        $handle = fopen('db-backup.sql', 'w+');
        fwrite($handle, $return);
        fclose($handle);
        return $handle;
    }

    public function getAllConsumersAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumers = $consumersMapper->getAllConsumers();

            if ($consumers) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );

                foreach ($consumers as $consumer) {
                    $data1 = array(
                        "consumer_id" => $consumer->__get("consumer_id"),
                        "consumer_name" => $consumer->__get("consumer_name"),
                    );
                    $data[] = $data1;
                }
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }

            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getAllUsersAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $usersMapper = new Application_Model_UsersMapper();
            $users = $usersMapper->getAllUsers();

            if ($users) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );

                foreach ($users as $user) {
                    $data1 = array(
                        "user_id" => $user->__get("user_id"),
                        "user_fname" => $user->__get("user_fname"),
                        "user_lname" => $user->__get("user_lname"),
                    );
                    $data[] = $data1;
                }
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }

            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function searchConsumerByCodeAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $code = $request->getParam("code");
            $consumersMapper = new Application_Model_ConsumersMapper();
            $consumers = $consumersMapper->searchByColumnValue("consumer_code", $code);
            if ($consumers) {
                $meta = array(
                    "code" => 200,
                    "message" => "SUCCESS"
                );
                $data = array();
                foreach ($consumers as $consumer) {
                    $data[] = array(
                        "consumer_id" => $consumer->__get("consumer_id"),
                        "consumer_code" => $consumer->__get("consumer_code"),
                        "consumer_name" => $consumer->__get("consumer_name"),
                        "consumer_father_name" => $consumer->__get("consumer_father_name"),
                    );
                }
            } else {
                $meta = array(
                    "code" => 404,
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    protected function _smsNotification($number, $sms) {
        $number = substr($number, 0, 10);
        $sms = urlencode($sms);
        //$url = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=TARA&pwd=921118&to=91" . $number . "&sid=TARAMU&msg=" . $sms . "&fl=0&gwid=2";
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91".$number."&msg=".$sms."&msg_type=TEXT&userid=2000140530&auth_scheme=plain&password=wlVYuapVg&v=1.1&format=text";
        //echo $url;exit;
        $text = file_get_contents($url);
        //echo $text;
        //$text = "";
        return $text;
    }

    public function getCollectionAgentAction() {

        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $collectionagentsMapper = new Application_Model_CollectionAgentsMapper();
            $collectionagent = $collectionagentsMapper->getCollectionAgentById($id);

            if ($collectionagent) {
                $meta = array(
                    "code" => "200",
                    "message" => "SUCCESS"
                );
                $data = array(
                    'agent_mobile' => $collectionagent->__get("agent_mobile"),
                    'agent_fname' => $collectionagent->__get("agent_fname"),
                    'agent_lname' => $collectionagent->__get("agent_lname"),
                    'agent_status' => $collectionagent->__get("agent_status"),
                    'agent_balance' => $collectionagent->__get("agent_balance"),
                    'id' => $id
                );
            } else {
                $meta = array(
                    "code" => "404",
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }

    public function getCaTransactionAction() {

        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $id = $request->getParam("id");
            $catransactionsMapper = new Application_Model_CaTransactionsMapper();
            $catransaction = $catransactionsMapper->getCaTransactionById($id);
            $collectionagentsMapper = new Application_Model_CollectionAgentsMapper();
            $collectionagent = $collectionagentsMapper->getCollectionAgentById($catransaction->__get("collection_agent_id"));

            if ($catransaction) {
                $meta = array(
                    "code" => "200",
                    "message" => "SUCCESS"
                );
                $data = array(
                    'collection_agent_id' => $collectionagent->__get("agent_fname"),
                    'transaction_type' => $catransaction->__get("transaction_type"),
                    'transaction_amount' => $catransaction->__get("transaction_amount"),
                    'timestamp' => $catransaction->__get("timestamp"),
                    'remarks' => $catransaction->__get("remarks"),
                    'id' => $id
                );
            } else {
                $meta = array(
                    "code" => "404",
                    "message" => "NOTFOUND"
                );
                $data = array();
            }
            $array = array(
                "meta" => $meta,
                "data" => $data
            );
            $json = json_encode($array);
            echo $json;
        }
    }
  
    public function  changePackage($collectionAgent, $package_id, $customer_code){

    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
    	$enterprisePriceMappper = new Application_Model_EnterprisePriceMapper();
    	$packageHistoryMapper= new Application_Model_PackageHistoryMapper();
		
    	$packageMapper= new Application_Model_PackagesMapper();
    	//$packageHistory=new Application_Model_PackageHistory();

    	$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
    	if ($consumers) {
    		$consumer = $consumers[0];
    		$agent_id=$collectionAgent->__get("collection_agent_id");
    		$consumer_id=$consumer->__get("consumer_id");
    		$package_Id=$consumer->__get("package_id");
    		$consumer_number=$consumer->__get("consumer_code");
    		$consumer_name=$consumer->__get("consumer_name");
    
    		$packages= $packageMapper->getPackageById($package_id);
    		if($packages)
    		{
    			$package_Names=$packages->__get("package_name");
    			$package_Costs=$packages->__get("package_cost");
				
    			if($package_Id!=$package_id){
					$updateConsumer=$consumersMapper->updateConsumerPackage($package_id, $consumer_id);
    				
    				if($updateConsumer){
									
    					if($packagehistorys=$packageHistoryMapper->getpackageHistoryByConsumerId($consumer_id)){
								
    						$packages_history=$packagehistorys[0];
    						$packageId=$packages_history->__get('package_id');
    						$consumerId=$packages_history->__get('consumer_id');
    						$changeBy=$packages_history->__get('change_by');
    						$packageCost=$packages_history->__get('package_cost');
    						$packageChangeDate=$packages_history->__get('package_change_date');
    						$changeDescription=$packages_history->__get('change_description');
							$file = 'test.txt';
							$current = file_get_contents($file);
							$current .= $packageId."&& ".$packageCost . "\r\n";
							file_put_contents($file, $current);
    						$packages_Name=$packageMapper->getPackageById($packageId);
    						$packageName=$packages_Name->__get("package_name");
    							
    						$packages_history->__set("package_id",$package_id);
    						$packages_history->__set("consumer_id",$consumer_id);
    						$packages_history->__set("change_by",$agent_id);
    						$packages_history->__set("package_cost",$package_Costs);
    						if($packageCost<$package_Costs)
    						{
    							$packages_history->__set("change_description","Package upgraded from ".$packageName."(Rs.".$packageCost.") to " .$package_Names."(Rs.".$package_Costs.")");
    						}else{
    							$packages_history->__set("change_description","Package downgraded from ".$packageName."(Rs.".$packageCost.") to " .$package_Names."(Rs.".$package_Costs.")");
    
    						}
    						$file = 'test.txt';
    						$current = file_get_contents($file);
    						$current .= $package_Names."## ".$package_Costs . "\r\n";
    						file_put_contents($file, $current);
    						$packageHistoryMapper->addNewPackageHistory($packages_history);
    							
    						$date=new Zend_Date();
    						$date->setTimezone("Asia/Kolkata");
    						$currentdate=$date->toString("yyyy-MM-dd");
    						$day=intval($date->toString("dd"));
    						$year=intval($date->toString("yyyy"));
    						$month=intval($date->toString("MM"));
    						$nextDate=$date->add(1,Zend_Date::MONTH);
    						$nextYear=intval($nextDate->toString("yyyy"));
    						$nextMonth=intval($nextDate->toString("MM"));
    						$liveDate=($day <8)?$year."-".$month."-08":(($day >=8 && $day<15)?$year."-".$month."-15":(($day >=15 && $day<22)?$year."-".$month."-22":$nextYear."-".$nextMonth."-01"));
    						/* $result="Package ID ".$package_id." has been updated for Customer ID ".$customer_code."."; */
    
    						$sms= "Namastey ".$consumer_name."! Package change request darj kar li hai. Aapka package ".$package_Names." ko change kar diya jayega";
    						$result="Package change request received. Connection ID ".$customer_code.". Old Package ". $packageName.". New Package ".$package_Names." will be live from ".$liveDate.".";
    						//$this->_smsNotification($consumer_number, $sms);
    
    
    					}
    				}
    				else{
    					$result="Error. Kripya kuch der baad try karein.";
    				}
    			}
    			else{
    				$result="Error. ".$customer_code." already has ". $packages->__get("package_name")." assigned. Check and try again.";
    				//"Package ".$package_id." has already assigned. Please check.";
    			}
    		}
    		else{
    			$result ="Error. Package ".$package_id." not found. Please check.";
    		}
    	}else {
    		$result = "Error. Customer ID ".$customer_code." not found. Please check.";
    	}
    	return $result;
    }
    
    
     public function  checkPackage($collectionAgent, $customer_code){
    	$consumersMapper = new Application_Model_ConsumersMapper();
    	$packageMapper= new Application_Model_PackagesMapper();
    	$consumers = $consumersMapper->searchByColumnValue("consumer_connection_id", $customer_code, TRUE);
    	if ($consumers) {
    		$consumer=$consumers[0];
    		
    		$package_id=$consumer->__get('package_id');
    		$packages= $packageMapper->getPackageById($package_id);
    		if($packages)
    		{
    			$package_name=$packages->__get("package_name");
    			$result="Customer ID ".$customer_code." ka Package Name ".$package_name." aur Package Id ".$package_id." hai.";
    		}
    	}else {
    		$result = "Error. Customer ID ".$customer_code." not found. Please check.";
    	}
    	return $result;
    }
    
}
