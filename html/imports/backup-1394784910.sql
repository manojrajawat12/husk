DROP TABLE activation_payments;

CREATE TABLE `activation_payments` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE admins;

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hashed_password` varchar(255) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `admin_role` varchar(20) NOT NULL,
  `reset_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO admins VALUES("4","arungoyal1992","6911d73b74e4555b6fe8ec7f122230a16e610f0a","arun","goyal","arungoyal014@gmail.com","admin-1392875652.jpg","superadmin","");
INSERT INTO admins VALUES("6","chk","288a1c51d8040dcdbe56a66dcb88fa476a452419","chk","qchk","chk@chk.in","admin-1393571698.jpg","admin","");
INSERT INTO admins VALUES("8","amanarora","41eecfc4a77ea273c55e30e57a357713b641ceee","Aman","Arora","aroraaman2709@gmail.com","admin-1393823985.PNG","admin","");



DROP TABLE cash_register;

CREATE TABLE `cash_register` (
  `cr_id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `receipt_number` varchar(255) NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `cr_entry_type` enum('CREDIT','DEBIT','REFUND','PENALTY','RECEIVED','ACTIVATION','DISCOUNT') NOT NULL,
  `cr_amount` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=latin1;

INSERT INTO cash_register VALUES("11","8","4","test12","","CREDIT","1000","2014-02-19 15:05:54");
INSERT INTO cash_register VALUES("12","8","4","test12","","REFUND","500","2014-02-19 15:38:34");
INSERT INTO cash_register VALUES("13","8","4","test12","","REFUND","100","2014-02-19 15:41:06");
INSERT INTO cash_register VALUES("14","8","4","act1","","ACTIVATION","2500","2014-02-21 14:10:51");
INSERT INTO cash_register VALUES("15","8","4","disc","","CREDIT","500","2014-02-21 15:46:23");
INSERT INTO cash_register VALUES("16","8","4","discount","","CREDIT","100","2014-02-21 15:48:17");
INSERT INTO cash_register VALUES("17","8","4","1","","CREDIT","100","2014-02-21 15:50:37");
INSERT INTO cash_register VALUES("18","8","4","2","","CREDIT","100","2014-02-21 15:51:58");
INSERT INTO cash_register VALUES("19","8","4","3","","CREDIT","12","2014-02-21 15:52:25");
INSERT INTO cash_register VALUES("20","8","4","4","","CREDIT","10","2014-02-21 15:53:02");
INSERT INTO cash_register VALUES("21","8","4","5","","CREDIT","5","2014-02-21 15:53:37");
INSERT INTO cash_register VALUES("22","8","4","6","","CREDIT","6","2014-02-21 15:55:48");
INSERT INTO cash_register VALUES("23","8","4","7","","CREDIT","7","2014-02-21 16:00:46");
INSERT INTO cash_register VALUES("24","8","4","0","","DISCOUNT","200","2014-02-21 16:00:46");
INSERT INTO cash_register VALUES("25","8","4","10","","CREDIT","10","2014-02-21 16:08:11");
INSERT INTO cash_register VALUES("26","8","4","11","","CREDIT","11","2014-02-21 16:09:32");
INSERT INTO cash_register VALUES("27","8","4","123","","CREDIT","123","2014-02-21 16:11:30");
INSERT INTO cash_register VALUES("28","8","4","0","","DISCOUNT","3000","2014-02-21 16:11:30");
INSERT INTO cash_register VALUES("29","8","4","","","ACTIVATION","","2014-02-28 12:09:47");
INSERT INTO cash_register VALUES("30","8","4","abc1234","","ACTIVATION","2500","2014-02-28 12:25:14");
INSERT INTO cash_register VALUES("31","8","4","abc1234","","ACTIVATION","2500","2014-02-28 12:25:22");
INSERT INTO cash_register VALUES("32","8","4","abc1234","","ACTIVATION","2500","2014-02-28 12:25:29");
INSERT INTO cash_register VALUES("33","8","4","ar","","ACTIVATION","2500","2014-02-28 12:33:23");
INSERT INTO cash_register VALUES("34","7","4","56","","ACTIVATION","500","2014-02-28 13:44:02");
INSERT INTO cash_register VALUES("35","7","4","234","","ACTIVATION","500","2014-02-28 13:44:21");
INSERT INTO cash_register VALUES("36","10","4","abc","","CREDIT","2000","2014-02-28 16:08:23");
INSERT INTO cash_register VALUES("136","14","4","25256","03032014174322-0200","CREDIT","200","2014-03-03 17:43:22");
INSERT INTO cash_register VALUES("137","14","4","0","03032014174323-0","CREDIT","0","2014-03-03 17:43:23");
INSERT INTO cash_register VALUES("138","14","4","8978","03032014174329-0400","CREDIT","400","2014-03-03 17:43:29");
INSERT INTO cash_register VALUES("139","14","4","0","03032014174330-0","CREDIT","0","2014-03-03 17:43:30");
INSERT INTO cash_register VALUES("140","10","4","855","03032014174345-0100","CREDIT","100","2014-03-03 17:43:46");
INSERT INTO cash_register VALUES("141","10","4","0","03032014174346-0","CREDIT","0","2014-03-03 17:43:46");
INSERT INTO cash_register VALUES("142","16","4","787","03032014174406-0080","CREDIT","80","2014-03-03 17:44:06");
INSERT INTO cash_register VALUES("143","16","4","0","03032014174407-0","CREDIT","0","2014-03-03 17:44:07");
INSERT INTO cash_register VALUES("144","16","4","74589","03032014174413-0150","CREDIT","150","2014-03-03 17:44:13");
INSERT INTO cash_register VALUES("145","16","4","0","03032014174413-0","CREDIT","0","2014-03-03 17:44:13");
INSERT INTO cash_register VALUES("146","8","4","4587","03032014174523-0400","CREDIT","400","2014-03-03 17:45:23");
INSERT INTO cash_register VALUES("147","8","4","0","03032014174523-0","CREDIT","0","2014-03-03 17:45:23");
INSERT INTO cash_register VALUES("148","8","4","78945","03032014174530-0010","CREDIT","10","2014-03-03 17:45:30");
INSERT INTO cash_register VALUES("149","8","4","0","03032014174531-0","CREDIT","0","2014-03-03 17:45:31");
INSERT INTO cash_register VALUES("150","16","4","253","04032014104130-0030","CREDIT","30","2014-03-04 10:41:30");
INSERT INTO cash_register VALUES("151","16","4","0","04032014104131-0","CREDIT","0","2014-03-04 10:41:31");
INSERT INTO cash_register VALUES("152","0","4","0","04032014104204-54","RECEIVED","54","2014-03-04 10:42:04");
INSERT INTO cash_register VALUES("153","16","4","24586","04032014104945--260","CREDIT","-260","2014-03-04 10:49:45");
INSERT INTO cash_register VALUES("154","16","4","0","04032014104945-0","DEBIT","40000","2014-03-04 10:49:45");
INSERT INTO cash_register VALUES("155","17","4","78945","04032014123007-0500","ACTIVATION","500","2014-03-04 12:30:07");
INSERT INTO cash_register VALUES("156","17","4","14562","04032014123035-5000","ACTIVATION","5000","2014-03-04 12:30:35");
INSERT INTO cash_register VALUES("157","18","4","654134","04032014132444-2350","ACTIVATION","2350","2014-03-04 13:24:44");



DROP TABLE consumers;

CREATE TABLE `consumers` (
  `consumer_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `consumer_code` varchar(12) NOT NULL,
  `consumer_name` varchar(255) NOT NULL,
  `consumer_father_name` varchar(255) NOT NULL,
  `consumer_status` varchar(255) NOT NULL,
  `consumer_act_date` varchar(255) NOT NULL,
  `consumer_act_charge` varchar(255) NOT NULL,
  PRIMARY KEY (`consumer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO consumers VALUES("8","2","4","9716802504","Arun","arun","new","","2500");
INSERT INTO consumers VALUES("10","2","3","9999926046","mayank","mayank","new","","2500");
INSERT INTO consumers VALUES("14","2","3","9958800584","Chander","Chander","new","","2500");
INSERT INTO consumers VALUES("15","2","1","9650408880","Arjun Dhawan","Arjun","new","","2500");
INSERT INTO consumers VALUES("16","2","1","8860863565","Aman Arora","Vindo Arora","active","2014-03-03 00:00:00","500");
INSERT INTO consumers VALUES("17","2","5","8800970065","Aman Arora","Vinod Arora","active","2014-01-12 00:00:00","2500");
INSERT INTO consumers VALUES("18","2","6","9899543112","Ashish Yadav","Satyaveer Yadav","active","2014-03-14 00:00:00","5000");



DROP TABLE packages;

CREATE TABLE `packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `package_cost` varchar(255) NOT NULL,
  `package_details` text NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO packages VALUES("2","Shop @ 100","100","Shop Package @ Rs.100 for 1 LED Bulb");
INSERT INTO packages VALUES("3","Shop @ 150","150","Shop Package @ Rs.150 for 2 LED Bulbs");
INSERT INTO packages VALUES("4","HH @ 150","150","Household Package @ Rs.150 for 2 LED Bulbs");



DROP TABLE settings;

CREATE TABLE `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO settings VALUES("1","system_email","arun@trion.com");
INSERT INTO settings VALUES("2","backup_email","aroraaman2709@gmail.com");
INSERT INTO settings VALUES("3","backup_email1","");
INSERT INTO settings VALUES("4","backup_email2","");
INSERT INTO settings VALUES("5","penalty_amount","0");
INSERT INTO settings VALUES("6","discount_amount","0");
INSERT INTO settings VALUES("7","backup_email","aman@triontechnologies.org");



DROP TABLE sites;

CREATE TABLE `sites` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO sites VALUES("1","Test Site 4");
INSERT INTO sites VALUES("3","Test Site 3");
INSERT INTO sites VALUES("4","Test Site 2");
INSERT INTO sites VALUES("5","Gurgaon");
INSERT INTO sites VALUES("6","Faridabaad");
INSERT INTO sites VALUES("7","Katsa");
INSERT INTO sites VALUES("8","Tethi");
INSERT INTO sites VALUES("9","Test Site 1");
INSERT INTO sites VALUES("10","Piparpatti");
INSERT INTO sites VALUES("11","Marhowda");
INSERT INTO sites VALUES("12","Kishanchand Bhagat Ka Tola");



DROP TABLE users;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hashed_password` varchar(255) NOT NULL,
  `user_fname` varchar(255) NOT NULL,
  `user_lname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `reset_code` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) NOT NULL,
  `phone` varchar(12) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO users VALUES("4","arungoyal1992","6911d73b74e4555b6fe8ec7f122230a16e610f0a","arun","goyal","arungoyal014@gmail.com","","","");
INSERT INTO users VALUES("7","amanarora","","Aman","Arora","aroraaman2709@gmail.com","","","");
INSERT INTO users VALUES("9","amanarora2709","41eecfc4a77ea273c55e30e57a357713b641ceee","Aman","Arora","aman@triontechnologies.org","","user-1394784520.jpg","8860863565");



