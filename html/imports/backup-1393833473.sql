DROP TABLE activation_payments;

CREATE TABLE `activation_payments` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE admins;

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hashed_password` varchar(255) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `admin_role` varchar(20) NOT NULL,
  `reset_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO admins VALUES("4","arungoyal1992","6911d73b74e4555b6fe8ec7f122230a16e610f0a","arun","goyal","arungoyal014@gmail.com","admin-1392875652.jpg","superadmin","");
INSERT INTO admins VALUES("6","chk","288a1c51d8040dcdbe56a66dcb88fa476a452419","chk","qchk","chk@chk.in","admin-1393571698.jpg","admin","");
INSERT INTO admins VALUES("8","amanarora","41eecfc4a77ea273c55e30e57a357713b641ceee","Aman","Arora","aroraaman2709@gmail.com","admin-1393823985.PNG","admin","");



DROP TABLE cash_register;

CREATE TABLE `cash_register` (
  `cr_id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `receipt_number` varchar(255) NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `cr_entry_type` enum('CREDIT','DEBIT','REFUND','PENALTY','RECEIVED','ACTIVATION','DISCOUNT') NOT NULL,
  `cr_amount` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;

INSERT INTO cash_register VALUES("11","8","4","test12","","CREDIT","1000","2014-02-19 15:05:54");
INSERT INTO cash_register VALUES("12","8","4","test12","","REFUND","500","2014-02-19 15:38:34");
INSERT INTO cash_register VALUES("13","8","4","test12","","REFUND","100","2014-02-19 15:41:06");
INSERT INTO cash_register VALUES("14","8","4","act1","","ACTIVATION","2500","2014-02-21 14:10:51");
INSERT INTO cash_register VALUES("15","8","4","disc","","CREDIT","500","2014-02-21 15:46:23");
INSERT INTO cash_register VALUES("16","8","4","discount","","CREDIT","100","2014-02-21 15:48:17");
INSERT INTO cash_register VALUES("17","8","4","1","","CREDIT","100","2014-02-21 15:50:37");
INSERT INTO cash_register VALUES("18","8","4","2","","CREDIT","100","2014-02-21 15:51:58");
INSERT INTO cash_register VALUES("19","8","4","3","","CREDIT","12","2014-02-21 15:52:25");
INSERT INTO cash_register VALUES("20","8","4","4","","CREDIT","10","2014-02-21 15:53:02");
INSERT INTO cash_register VALUES("21","8","4","5","","CREDIT","5","2014-02-21 15:53:37");
INSERT INTO cash_register VALUES("22","8","4","6","","CREDIT","6","2014-02-21 15:55:48");
INSERT INTO cash_register VALUES("23","8","4","7","","CREDIT","7","2014-02-21 16:00:46");
INSERT INTO cash_register VALUES("24","8","4","0","","DISCOUNT","200","2014-02-21 16:00:46");
INSERT INTO cash_register VALUES("25","8","4","10","","CREDIT","10","2014-02-21 16:08:11");
INSERT INTO cash_register VALUES("26","8","4","11","","CREDIT","11","2014-02-21 16:09:32");
INSERT INTO cash_register VALUES("27","8","4","123","","CREDIT","123","2014-02-21 16:11:30");
INSERT INTO cash_register VALUES("28","8","4","0","","DISCOUNT","3000","2014-02-21 16:11:30");
INSERT INTO cash_register VALUES("29","8","4","","","ACTIVATION","","2014-02-28 12:09:47");
INSERT INTO cash_register VALUES("30","8","4","abc1234","","ACTIVATION","2500","2014-02-28 12:25:14");
INSERT INTO cash_register VALUES("31","8","4","abc1234","","ACTIVATION","2500","2014-02-28 12:25:22");
INSERT INTO cash_register VALUES("32","8","4","abc1234","","ACTIVATION","2500","2014-02-28 12:25:29");
INSERT INTO cash_register VALUES("33","8","4","ar","","ACTIVATION","2500","2014-02-28 12:33:23");
INSERT INTO cash_register VALUES("34","7","4","56","","ACTIVATION","500","2014-02-28 13:44:02");
INSERT INTO cash_register VALUES("35","7","4","234","","ACTIVATION","500","2014-02-28 13:44:21");
INSERT INTO cash_register VALUES("36","10","4","abc","","CREDIT","2000","2014-02-28 16:08:23");
INSERT INTO cash_register VALUES("39","8","4","0","20140301124844-3000","CREDIT","3000","2014-03-01 12:48:44");
INSERT INTO cash_register VALUES("40","14","4","12","20140301130729-2500","CREDIT","2500","2014-03-01 13:07:29");
INSERT INTO cash_register VALUES("41","14","4","0","20140301130729-3000","CREDIT","3000","2014-03-01 13:07:29");
INSERT INTO cash_register VALUES("42","14","4","123","20140301130858-123","CREDIT","123","2014-03-01 13:08:58");
INSERT INTO cash_register VALUES("43","14","4","0","20140301130858-3000","CREDIT","3000","2014-03-01 13:08:58");
INSERT INTO cash_register VALUES("44","14","4","12\\","20140301134731-123456","CREDIT","123456","2014-03-01 13:47:31");
INSERT INTO cash_register VALUES("45","14","4","0","20140301134731-3000","CREDIT","3000","2014-03-01 13:47:31");
INSERT INTO cash_register VALUES("46","14","4","12","20140301134910-2500","CREDIT","2500","2014-03-01 13:49:10");
INSERT INTO cash_register VALUES("47","14","4","0","20140301134910-3000","CREDIT","3000","2014-03-01 13:49:10");
INSERT INTO cash_register VALUES("48","14","4","98","20140301140512-100","CREDIT","100","2014-03-01 14:05:12");
INSERT INTO cash_register VALUES("49","14","4","0","20140301140512-3000","CREDIT","3000","2014-03-01 14:05:12");
INSERT INTO cash_register VALUES("50","14","4","87","20140301140705-500","CREDIT","500","2014-03-01 14:07:05");
INSERT INTO cash_register VALUES("51","14","4","0","20140301140705-3000","CREDIT","3000","2014-03-01 14:07:05");
INSERT INTO cash_register VALUES("52","14","4","76","20140301150057-987","CREDIT","987","2014-03-01 15:00:57");
INSERT INTO cash_register VALUES("53","14","4","0","20140301150057-3000","CREDIT","3000","2014-03-01 15:00:57");
INSERT INTO cash_register VALUES("54","14","4","678","20140301150135-1234","CREDIT","1234","2014-03-01 15:01:35");
INSERT INTO cash_register VALUES("55","14","4","0","20140301150135-3000","CREDIT","3000","2014-03-01 15:01:35");
INSERT INTO cash_register VALUES("56","14","4","45","20140301150344-234","CREDIT","234","2014-03-01 15:03:44");
INSERT INTO cash_register VALUES("57","14","4","0","20140301150345-3000","CREDIT","3000","2014-03-01 15:03:45");
INSERT INTO cash_register VALUES("58","14","4","45","20140301150434-445","CREDIT","445","2014-03-01 15:04:34");
INSERT INTO cash_register VALUES("59","14","4","0","20140301150435-3000","CREDIT","3000","2014-03-01 15:04:35");
INSERT INTO cash_register VALUES("60","14","4","45","20140301150510-445","CREDIT","445","2014-03-01 15:05:10");
INSERT INTO cash_register VALUES("61","14","4","0","20140301150510-3000","CREDIT","3000","2014-03-01 15:05:10");
INSERT INTO cash_register VALUES("62","14","4","45","20140301150544-445","CREDIT","445","2014-03-01 15:05:44");
INSERT INTO cash_register VALUES("63","14","4","0","20140301150544-3000","CREDIT","3000","2014-03-01 15:05:44");
INSERT INTO cash_register VALUES("64","14","4","45","20140301150553-445","CREDIT","445","2014-03-01 15:05:53");
INSERT INTO cash_register VALUES("65","14","4","0","20140301150554-3000","CREDIT","3000","2014-03-01 15:05:54");
INSERT INTO cash_register VALUES("66","14","4","45","20140301150646-445","CREDIT","445","2014-03-01 15:06:46");
INSERT INTO cash_register VALUES("67","14","4","0","20140301150646-3000","CREDIT","3000","2014-03-01 15:06:46");
INSERT INTO cash_register VALUES("68","14","4","45","20140301150720-445","CREDIT","445","2014-03-01 15:07:20");
INSERT INTO cash_register VALUES("69","14","4","0","20140301150721-3000","CREDIT","3000","2014-03-01 15:07:21");
INSERT INTO cash_register VALUES("70","14","4","45","20140301150841-445","CREDIT","445","2014-03-01 15:08:42");
INSERT INTO cash_register VALUES("71","14","4","0","20140301150842-3000","CREDIT","3000","2014-03-01 15:08:42");
INSERT INTO cash_register VALUES("72","14","4","45","20140301151017-445","CREDIT","445","2014-03-01 15:10:17");
INSERT INTO cash_register VALUES("73","14","4","0","20140301151018-3000","CREDIT","3000","2014-03-01 15:10:18");
INSERT INTO cash_register VALUES("74","14","4","45","20140301151128-445","CREDIT","445","2014-03-01 15:11:28");
INSERT INTO cash_register VALUES("75","14","4","0","20140301151128-3000","CREDIT","3000","2014-03-01 15:11:28");
INSERT INTO cash_register VALUES("76","14","4","45","20140301151224-445","CREDIT","445","2014-03-01 15:12:24");
INSERT INTO cash_register VALUES("77","14","4","0","20140301151224-3000","CREDIT","3000","2014-03-01 15:12:24");
INSERT INTO cash_register VALUES("78","14","4","45","20140301151304-445","CREDIT","445","2014-03-01 15:13:04");
INSERT INTO cash_register VALUES("79","14","4","0","20140301151304-3000","CREDIT","3000","2014-03-01 15:13:04");
INSERT INTO cash_register VALUES("80","14","4","45","20140301151322-445","CREDIT","445","2014-03-01 15:13:22");
INSERT INTO cash_register VALUES("81","14","4","0","20140301151322-3000","CREDIT","3000","2014-03-01 15:13:22");
INSERT INTO cash_register VALUES("82","14","4","45","20140301151350-445","CREDIT","445","2014-03-01 15:13:50");
INSERT INTO cash_register VALUES("83","14","4","0","20140301151350-3000","CREDIT","3000","2014-03-01 15:13:50");
INSERT INTO cash_register VALUES("84","14","4","45","20140301151415-445","CREDIT","445","2014-03-01 15:14:15");
INSERT INTO cash_register VALUES("85","14","4","0","20140301151415-3000","CREDIT","3000","2014-03-01 15:14:15");
INSERT INTO cash_register VALUES("86","14","4","45","20140301151444-445","CREDIT","445","2014-03-01 15:14:44");
INSERT INTO cash_register VALUES("87","14","4","0","20140301151444-3000","CREDIT","3000","2014-03-01 15:14:44");
INSERT INTO cash_register VALUES("88","15","4","abc123","20140301151659-2500","CREDIT","2500","2014-03-01 15:16:59");
INSERT INTO cash_register VALUES("89","15","4","0","20140301151659-3000","CREDIT","3000","2014-03-01 15:16:59");
INSERT INTO cash_register VALUES("90","15","4","asdf","20140301161205-1234","CREDIT","1234","2014-03-01 16:12:05");
INSERT INTO cash_register VALUES("91","15","4","0","20140301161206-3000","CREDIT","3000","2014-03-01 16:12:06");
INSERT INTO cash_register VALUES("92","0","4","0","","RECEIVED","500","2014-03-01 16:19:57");
INSERT INTO cash_register VALUES("93","0","4","0","","RECEIVED","240761","2014-03-01 16:20:05");
INSERT INTO cash_register VALUES("94","15","4","762732","01032014162112--9734","CREDIT","-9734","2014-03-01 16:21:12");
INSERT INTO cash_register VALUES("95","15","4","0","01032014162113-3000","CREDIT","3000","2014-03-01 16:21:13");
INSERT INTO cash_register VALUES("97","15","4","0","01032014162734-3000","CREDIT","3000","2014-03-01 16:27:34");
INSERT INTO cash_register VALUES("98","15","4","jflug","01032014162825--6000","CREDIT","-6000","2014-03-01 16:28:25");
INSERT INTO cash_register VALUES("99","15","4","0","01032014162825-3000","CREDIT","3000","2014-03-01 16:28:25");
INSERT INTO cash_register VALUES("100","15","4","64654654","01032014162849-20","ACTIVATION","20","2014-03-01 16:28:49");
INSERT INTO cash_register VALUES("101","15","4","0","01032014162850-3000","CREDIT","3000","2014-03-01 16:28:50");
INSERT INTO cash_register VALUES("102","0","4","0","","RECEIVED","-1214","2014-03-01 16:35:38");
INSERT INTO cash_register VALUES("103","14","4","sdfvf","01032014164416-1200","CREDIT","1200","2014-03-01 16:44:16");
INSERT INTO cash_register VALUES("104","14","4","0","01032014164417-3000","CREDIT","3000","2014-03-01 16:44:17");
INSERT INTO cash_register VALUES("105","15","4","1234","02032014220758-200","CREDIT","200","2014-03-02 22:07:58");
INSERT INTO cash_register VALUES("106","15","4","0","02032014220801-3000","CREDIT","3000","2014-03-02 22:08:01");
INSERT INTO cash_register VALUES("107","15","4","1234","02032014220821--20","ACTIVATION","-20","2014-03-02 22:08:21");
INSERT INTO cash_register VALUES("108","15","4","0","02032014220823-3000","CREDIT","3000","2014-03-02 22:08:23");
INSERT INTO cash_register VALUES("109","0","4","0","02032014222905-2780","RECEIVED","2780","2014-03-02 22:29:05");
INSERT INTO cash_register VALUES("110","15","4","1234","02032014230155-200","ACTIVATION","200","2014-03-02 23:01:55");
INSERT INTO cash_register VALUES("111","15","4","1234","02032014230329-200","ACTIVATION","200","2014-03-02 23:03:29");
INSERT INTO cash_register VALUES("112","15","4","1234","02032014230351-300","CREDIT","300","2014-03-02 23:03:51");
INSERT INTO cash_register VALUES("113","15","4","0","02032014230354-3000","CREDIT","3000","2014-03-02 23:03:54");
INSERT INTO cash_register VALUES("114","13","4","4567","03032014082236-200","ACTIVATION","200","2014-03-03 08:22:36");
INSERT INTO cash_register VALUES("115","13","4","4567","03032014082318-200","ACTIVATION","200","2014-03-03 08:23:18");
INSERT INTO cash_register VALUES("116","13","4","347856","03032014082507-249600","ACTIVATION","249600","2014-03-03 08:25:07");
INSERT INTO cash_register VALUES("117","16","4","25893","03032014082716-1000","CREDIT","1000","2014-03-03 08:27:16");
INSERT INTO cash_register VALUES("118","16","4","0","03032014082717-3000","DEBIT","3000","2014-03-03 08:27:17");
INSERT INTO cash_register VALUES("121","16","4","5860","03032014083059-20","CREDIT","20","2014-03-03 08:30:59");
INSERT INTO cash_register VALUES("122","16","4","0","03032014083059-3000","CREDIT","3000","2014-03-03 08:30:59");
INSERT INTO cash_register VALUES("123","16","4","5687","03032014085651-60","CREDIT","60","2014-03-03 08:56:51");
INSERT INTO cash_register VALUES("124","16","4","0","03032014085651-0","CREDIT","0","2014-03-03 08:56:51");
INSERT INTO cash_register VALUES("125","16","4","5869","03032014090048-0010","CREDIT","0010","2014-03-03 09:00:48");
INSERT INTO cash_register VALUES("126","16","4","0","03032014090049-0","CREDIT","0","2014-03-03 09:00:49");
INSERT INTO cash_register VALUES("127","16","4","7895","03032014090126-0005","CREDIT","5","2014-03-03 09:01:26");
INSERT INTO cash_register VALUES("128","16","4","0","03032014090127-0","CREDIT","0","2014-03-03 09:01:27");
INSERT INTO cash_register VALUES("129","16","4","7895","03032014090717-0005","CREDIT","5","2014-03-03 09:07:17");
INSERT INTO cash_register VALUES("130","16","4","0","03032014090717-0","CREDIT","0","2014-03-03 09:07:17");
INSERT INTO cash_register VALUES("131","16","4","25893","03032014092208-0080","CREDIT","80","2014-03-03 09:22:08");
INSERT INTO cash_register VALUES("132","16","4","0","03032014092208-0","CREDIT","0","2014-03-03 09:22:08");
INSERT INTO cash_register VALUES("133","16","4","897455","03032014095144-0087","CREDIT","87","2014-03-03 09:51:44");
INSERT INTO cash_register VALUES("134","16","4","0","03032014095148-0","CREDIT","0","2014-03-03 09:51:48");
INSERT INTO cash_register VALUES("135","14","4","12","","REFUND","500","2014-03-03 10:10:33");



DROP TABLE consumers;

CREATE TABLE `consumers` (
  `consumer_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `consumer_code` varchar(12) NOT NULL,
  `consumer_name` varchar(255) NOT NULL,
  `consumer_father_name` varchar(255) NOT NULL,
  `consumer_status` varchar(255) NOT NULL,
  `consumer_act_date` varchar(255) NOT NULL,
  `consumer_act_charge` varchar(255) NOT NULL,
  PRIMARY KEY (`consumer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO consumers VALUES("8","2","2","9716802504","Arun","arun","new","","2500");
INSERT INTO consumers VALUES("10","2","2","9999926046","mayank","mayank","new","0","2500");
INSERT INTO consumers VALUES("14","2","2","9958800584","Chander","Chander","new","0","2500");
INSERT INTO consumers VALUES("15","2","1","9650408880","Arjun Dhawan","Arjun","new","","2500");
INSERT INTO consumers VALUES("16","2","1","8860863565","Aman Arora","Vindo Arora","active","03/03/2014","500");



DROP TABLE packages;

CREATE TABLE `packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `package_cost` varchar(255) NOT NULL,
  `package_details` text NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO packages VALUES("2","apackge 2","2000","arun goyAL");



DROP TABLE settings;

CREATE TABLE `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO settings VALUES("1","system_email","arun@trion.com");
INSERT INTO settings VALUES("2","backup_email","aroraaman2709@gmail.com");
INSERT INTO settings VALUES("3","backup_email1","");
INSERT INTO settings VALUES("4","backup_email2","");
INSERT INTO settings VALUES("5","penalty_amount","0");
INSERT INTO settings VALUES("6","discount_amount","0");
INSERT INTO settings VALUES("7","backup_email","aman@triontechnologies.org");



DROP TABLE sites;

CREATE TABLE `sites` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO sites VALUES("1","Gurgaon");
INSERT INTO sites VALUES("3","Delhi");



DROP TABLE users;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hashed_password` varchar(255) NOT NULL,
  `user_fname` varchar(255) NOT NULL,
  `user_lname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `reset_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO users VALUES("4","arungoyal1992","6911d73b74e4555b6fe8ec7f122230a16e610f0a","arun","goyal","arungoyal014@gmail.com","");
INSERT INTO users VALUES("7","amanarora","","Aman","Arora","aroraaman2709@gmail.com","");



