DROP TABLE activation_payments;

CREATE TABLE `activation_payments` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE admins;

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hashed_password` varchar(255) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_role` varchar(20) NOT NULL,
  `reset_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO admins VALUES("4","arungoyal1992","6911d73b74e4555b6fe8ec7f122230a16e610f0a","arun","goyal","arungoyal014@gmail.com","superadmin","");



DROP TABLE cash_register;

CREATE TABLE `cash_register` (
  `cr_id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `receipt_number` varchar(255) NOT NULL,
  `cr_entry_type` enum('CREDIT','DEBIT','REFUND','PENALTY','RECEIVED','ACTIVATION') NOT NULL,
  `cr_amount` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

INSERT INTO cash_register VALUES("2","7","4","Abc123","RECEIVED","2400","2014-02-10 16:56:58");
INSERT INTO cash_register VALUES("3","7","4","def125","CREDIT","122","2014-02-11 12:07:37");
INSERT INTO cash_register VALUES("4","7","4","abcd12","DEBIT","234","2014-02-11 12:07:37");
INSERT INTO cash_register VALUES("6","7","4","1234","REFUND","50","2014-02-18 17:00:08");
INSERT INTO cash_register VALUES("10","7","1","1234","ACTIVATION","500","2014-02-18 17:11:07");
INSERT INTO cash_register VALUES("11","7","5","1111","CREDIT","62","2014-02-19 00:08:47");
INSERT INTO cash_register VALUES("12","7","5","2222","ACTIVATION","500","2014-02-19 00:08:55");
INSERT INTO cash_register VALUES("13","0","5","0","RECEIVED","500","2014-02-19 00:15:53");
INSERT INTO cash_register VALUES("14","0","5","0","RECEIVED","12","2014-02-19 00:15:59");
INSERT INTO cash_register VALUES("15","0","4","0","RECEIVED","-2000","2014-02-19 00:16:03");
INSERT INTO cash_register VALUES("16","0","4","0","RECEIVED","-328","2014-02-19 00:16:08");
INSERT INTO cash_register VALUES("17","0","5","0","RECEIVED","50","2014-02-19 00:16:19");
INSERT INTO cash_register VALUES("18","7","5","55555","CREDIT","50","2014-02-19 00:16:49");
INSERT INTO cash_register VALUES("19","0","5","0","RECEIVED","59","2014-02-19 00:16:57");
INSERT INTO cash_register VALUES("20","7","5","555","ACTIVATION","1000","2014-02-19 00:17:53");
INSERT INTO cash_register VALUES("21","0","5","0","RECEIVED","91","2014-02-19 00:18:40");
INSERT INTO cash_register VALUES("22","0","5","0","RECEIVED","900","2014-02-19 00:18:44");
INSERT INTO cash_register VALUES("23","11","4","12345","DEBIT","150","2014-02-19 16:02:11");
INSERT INTO cash_register VALUES("24","11","0","","PENALTY","20","2014-02-19 16:53:10");
INSERT INTO cash_register VALUES("25","11","4","","REFUND","20","2014-02-19 16:58:06");
INSERT INTO cash_register VALUES("26","11","0","","PENALTY","20","2014-02-19 17:02:32");
INSERT INTO cash_register VALUES("27","0","4","0","RECEIVED","-200","2014-02-19 17:18:59");
INSERT INTO cash_register VALUES("28","0","4","0","RECEIVED","-600","2014-02-19 17:19:14");
INSERT INTO cash_register VALUES("29","0","4","0","RECEIVED","600","2014-02-19 17:19:18");



DROP TABLE consumers;

CREATE TABLE `consumers` (
  `consumer_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `consumer_code` varchar(12) NOT NULL,
  `consumer_name` varchar(255) NOT NULL,
  `consumer_father_name` varchar(255) NOT NULL,
  `consumer_status` varchar(255) NOT NULL,
  `consumer_act_date` varchar(255) NOT NULL,
  `consumer_act_charge` varchar(255) NOT NULL,
  PRIMARY KEY (`consumer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO consumers VALUES("7","2","2","9876543210","test","Test Father","new","0","2500");
INSERT INTO consumers VALUES("8","2","1","9716802504","arun ","arun","new","0","2500");
INSERT INTO consumers VALUES("10","2","2","9999926046","mayank","mayank","new","0","2500");
INSERT INTO consumers VALUES("11","2","1","8860863565","Aman Arora","Vinod Arora","active","0","1500");
INSERT INTO consumers VALUES("13","2","1","9650408880","Arjun Dhawan","Sanjay Dhawan","new","0","1200");



DROP TABLE packages;

CREATE TABLE `packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `package_cost` varchar(255) NOT NULL,
  `package_details` text NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO packages VALUES("2","apackge 2","150","arun goyAL");



DROP TABLE settings;

CREATE TABLE `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO settings VALUES("1","system_email","system@trionprojects.com");
INSERT INTO settings VALUES("2","backup_email","arjundhawan.21@gmail.com");
INSERT INTO settings VALUES("3","backup_email","ashish@triontechnologies.org");
INSERT INTO settings VALUES("4","backup_email","ashutosh@triontechnologies.org");
INSERT INTO settings VALUES("5","penalty_amount","20");
INSERT INTO settings VALUES("6","discount_amount","30");
INSERT INTO settings VALUES("7","backup_email","chander@triontechnologies.org");
INSERT INTO settings VALUES("8","backup_email","saleem@triontechnologies.org");



DROP TABLE sites;

CREATE TABLE `sites` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO sites VALUES("1","gurgaon");
INSERT INTO sites VALUES("2","Kishanchan Bhagat Ka Tola");
INSERT INTO sites VALUES("3","Tehti");
INSERT INTO sites VALUES("4","Katsa");



DROP TABLE users;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hashed_password` varchar(255) NOT NULL,
  `user_fname` varchar(255) NOT NULL,
  `user_lname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `reset_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO users VALUES("4","arungoyal1992","6911d73b74e4555b6fe8ec7f122230a16e610f0a","arun","goyal","arungoyal014@gmail.com","");
INSERT INTO users VALUES("5","amanarora","41eecfc4a77ea273c55e30e57a357713b641ceee","Aman","Arora","aroraaman2709@gmail.com","");



