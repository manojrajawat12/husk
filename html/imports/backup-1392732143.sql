-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2014 at 12:41 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `tara`
--

-- --------------------------------------------------------

--
-- Table structure for table `activation_payments`
--

CREATE TABLE `activation_payments` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hashed_password` varchar(255) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_role` varchar(20) NOT NULL,
  `reset_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `username`, `hashed_password`, `admin_fname`, `admin_lname`, `admin_email`, `admin_role`, `reset_code`) VALUES
(4, 'arungoyal1992', '6911d73b74e4555b6fe8ec7f122230a16e610f0a', 'arun', 'goyal', 'arungoyal014@gmail.com', 'superadmin', '');

-- --------------------------------------------------------

--
-- Table structure for table `cash_register`
--

CREATE TABLE `cash_register` (
  `cr_id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `receipt_number` varchar(255) NOT NULL,
  `cr_entry_type` enum('CREDIT','DEBIT','REFUND','PENALTY','RECEIVED','ACTIVATION') NOT NULL,
  `cr_amount` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `cash_register`
--

INSERT INTO `cash_register` (`cr_id`, `consumer_id`, `user_id`, `receipt_number`, `cr_entry_type`, `cr_amount`, `timestamp`) VALUES
(2, 7, 4, 'Abc123', 'RECEIVED', '2400', '2014-02-10 11:26:58'),
(3, 7, 4, 'def125', 'CREDIT', '122', '2014-02-11 06:37:37'),
(4, 7, 4, 'abcd12', 'DEBIT', '234', '2014-02-11 06:37:37'),
(6, 7, 1, '1234', 'CREDIT', '50', '2014-02-18 11:30:08'),
(10, 7, 1, '1234', 'ACTIVATION', '500', '2014-02-18 11:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `consumers`
--

CREATE TABLE `consumers` (
  `consumer_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `consumer_code` varchar(12) NOT NULL,
  `consumer_name` varchar(255) NOT NULL,
  `consumer_father_name` varchar(255) NOT NULL,
  `consumer_status` varchar(255) NOT NULL,
  `consumer_act_date` varchar(255) NOT NULL,
  `consumer_act_charge` varchar(255) NOT NULL,
  PRIMARY KEY (`consumer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `consumers`
--

INSERT INTO `consumers` (`consumer_id`, `package_id`, `site_id`, `consumer_code`, `consumer_name`, `consumer_father_name`, `consumer_status`, `consumer_act_date`, `consumer_act_charge`) VALUES
(7, 2, 2, '9876543210', 'test', 'Test Father', 'new', '0', '2500'),
(8, 2, 1, '9716802504', 'arun ', 'arun', 'new', '0', '2500'),
(10, 2, 2, '9999926046', 'mayank', 'mayank', 'new', '0', '2500');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `package_cost` varchar(255) NOT NULL,
  `package_details` text NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`package_id`, `package_name`, `package_cost`, `package_details`) VALUES
(2, 'apackge 2', '2000', 'arun goyAL');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `setting_name`, `setting_value`) VALUES
(1, 'system_email', 'arun@trion.com'),
(2, 'backup_email', 'kdfnkvb'),
(3, 'backup_email1', ''),
(4, 'backup_email2', ''),
(5, 'penalty_amount', '2000'),
(6, 'discount_amount', '3000');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`site_id`, `site_name`) VALUES
(1, 'gurgaon'),
(2, 'faridabad');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `hashed_password` varchar(255) NOT NULL,
  `user_fname` varchar(255) NOT NULL,
  `user_lname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `reset_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `hashed_password`, `user_fname`, `user_lname`, `user_email`, `reset_code`) VALUES
(4, 'arungoyal1992', '6911d73b74e4555b6fe8ec7f122230a16e610f0a', 'arun', 'goyal', 'arungoyal014@gmail.com', '');
