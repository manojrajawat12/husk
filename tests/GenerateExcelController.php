
<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('memory_limit', '5000M');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

include('MPDF/mpdf.php'); 
require('html2fpdf.php');
require('font/makefont/makefont.php');

/** PHPExcel_IOFactory */

require_once '../PHPExcel/PHPExcel/IOFactory.php';
gc_enable() ;gc_collect_cycles();

class Admin_GenerateExcelController extends Zend_Controller_Action
{
	public function init()
	{
		 $this->_helper->layout()->disableLayout();
         	 $this->_helper->viewRenderer->setNoRender(true);
		
	}
	
	public function indexAction()
	{
	
		$request = $this->getRequest();
		$type = $request->getParam("type");
		$from = $request->getParam("cron");
		$daily = $request->getParam("daily");
		$year = $request->getParam("year");
		$month_val = $request->getParam("month");
		$month = date("m", strtotime($month_val));
		$consumerPackageMapper=new Application_Model_ConsumersPackageMapper();
		if($from !=null && $from!=="")
		{
			//$date = date(); 
			$next_date = date('Y-m-d', strtotime($date .' +1 day'));
			$day=date('d', strtotime($next_date));
			if($daily=='yes'){
				$day=1;
			}
			if($day!=1){   
				exit;
			} 
		}

		$newfile='';
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '8192MB'); 
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		$objPHPExcel=null;
		date_default_timezone_set('Asia/Kolkata');
		switch ($type)
		{
			case 'BKS_Defaulter':
				$newfile = 'BKS - Defaulters'.date('d M,Y h-i A').'.xlsm';
				copy('ExcelFiles/BKS - Defaulters - AR ON (txnGroup).xlsm', $newfile);
				$inputFileType = 'Excel2007';
				$inputFileName = $newfile;
				
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				$objPHPExcel->setActiveSheetIndex(0);
				
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
				$rowCount = 2;
				
				foreach(array_reverse($cashRegisters) as $cashRegister)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
					$rowCount++;
				}
				$objPHPExcel->setActiveSheetIndex(1);
				$consumerMapper = new Application_Model_ConsumersMapper();
				$consumers = $consumerMapper->getAllConsumers(false);
				$rowCount = 2;
				
				foreach(array_reverse($consumers) as $consumer)
				{
						
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
					$rowCount++;
				}	
			break;	
			
			case 'BKS_Monthly':
				
				$newfile = 'BKS - Monthly Village Revenue'.date('d M,Y h-i A').'.xlsm';
				copy('ExcelFiles/BKS - Monthly Village Revenue - AR ON.xlsm', $newfile);
				$inputFileType = 'Excel2007';
				$inputFileName = $newfile;
			
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				$objPHPExcel->setActiveSheetIndex(0);
			
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
				$rowCount = 2;
			
				foreach(array_reverse($cashRegisters) as $cashRegister)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
					$rowCount++;
				}
				$objPHPExcel->setActiveSheetIndex(1);
				$consumerMapper = new Application_Model_ConsumersMapper();
				$consumers = $consumerMapper->getAllConsumers(false);
				$rowCount = 2;
			
				foreach(array_reverse($consumers) as $consumer)
				{
			
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
					$rowCount++;
				}
				break;
				
				case 'CM_Current':
					$newfile = 'CM - Current Month Defaulters'.date('d M,Y h-i A').'.xlsm';
					copy('ExcelFiles/CM - Current Month Defaulters - AR ON (TxnGroup).xlsm', $newfile);
					$inputFileType = 'Excel2007';
					$inputFileName = $newfile;
						
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					$objPHPExcel->setActiveSheetIndex(0);
						
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
					$rowCount = 2;
						
					foreach(array_reverse($cashRegisters) as $cashRegister)
					{
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
						$rowCount++;
					}
					$objPHPExcel->setActiveSheetIndex(1);
					$consumerMapper = new Application_Model_ConsumersMapper();
					$consumers = $consumerMapper->getAllConsumers(false);
					$rowCount = 2;
						
					foreach(array_reverse($consumers) as $consumer)
					{
							
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
						$rowCount++;
					}
					break;
					
					case 'CM_Next':
						$newfile = 'CM - Next Month Collection Report'.date('d M,Y h-i A').'.xlsm';
						copy('ExcelFiles/CM - Next Month Collection Report - AR ON.xlsm', $newfile);
						$inputFileType = 'Excel2007';
						$inputFileName = $newfile;
					
						$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
						$objPHPExcel->setActiveSheetIndex(0);
					
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
						$rowCount = 2;
					
						foreach(array_reverse($cashRegisters) as $cashRegister)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("cr_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("consumer_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("transaction_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_entry_type"));
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("cr_amount"));
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $cashRegister->__get("timestamp"));
							$rowCount++;
						}
						$objPHPExcel->setActiveSheetIndex(1);
						$consumerMapper = new Application_Model_ConsumersMapper();
						$consumers = $consumerMapper->getAllConsumers(false);
						$rowCount = 2;
					
						foreach(array_reverse($consumers) as $consumer)
						{
							$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
							foreach ($consumerPackages as $consumerPackage){
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumerPackage["package_id"]);
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("site_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_name"));
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("consumer_status"));
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("consumer_act_charge"));
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $consumer->__get("consumer_connection_id"));
								$rowCount++;
							}
						}
						$objPHPExcel->setActiveSheetIndex(2);
						$packagesMapper = new Application_Model_PackagesMapper();
						$packages = $packagesMapper->getAllPackages(true);
						$rowCount = 2;
							
						foreach(array_reverse($packages) as $package)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_cost"));
							$rowCount++;
						}
				break;
				
					case 'SD_Kwh':
					$newfile = '2SD - KwH daily'.date('d M,Y h-i A').'.xlsm';
					copy('ExcelFiles/2 SD - KwH daily - AR ON.xlsm', $newfile);
					$inputFileType = 'Excel2007';
					$inputFileName = $newfile;
						
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					$objPHPExcel->setActiveSheetIndex(0);
						
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
					$rowCount = 2;
						
					foreach(array_reverse($cashRegisters) as $cashRegister)
					{
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
						$rowCount++;
					}
					$objPHPExcel->setActiveSheetIndex(1);
					$consumerMapper = new Application_Model_ConsumersMapper();
					$consumers = $consumerMapper->getAllConsumers(false);
					$rowCount = 2;
						
					foreach(array_reverse($consumers) as $consumer)
					{
						$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
						foreach ($consumerPackages as $consumerPackage){
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumerPackage["package_id"]);
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_name"));
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_status"));
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("wattage"));
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("micro_entrprise_price"));
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $consumer->__get("consumer_connection_id"));
							$rowCount++;
						}
					}
					$objPHPExcel->setActiveSheetIndex(2);
					$packagesMapper = new Application_Model_PackagesMapper();
					$packages = $packagesMapper->getAllPackages(true);
					$rowCount = 2;
						
					foreach(array_reverse($packages) as $package)
					{
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_name"));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $package->__get("package_cost"));
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $package->__get("package_details"));
						$rowCount++;
					}
					break;
					
					case 'Server_Collection':
						$newfile = '2 Server Collection List'.date('d M,Y h-i A').'.xlsm';
						copy('ExcelFiles/2 Server Collection List- AR ON.xlsm', $newfile);
						$inputFileType = 'Excel2007';
						$inputFileName = $newfile;
					
						$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
						$objPHPExcel->setActiveSheetIndex(0);
					
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
						$rowCount = 2;
					
						foreach(array_reverse($cashRegisters) as $cashRegister)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("user_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_amount"));
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("transaction_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("cr_entry_type"));
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $cashRegister->__get("timestamp"));
							$rowCount++;
						}
						$objPHPExcel->setActiveSheetIndex(2);
						$consumerMapper = new Application_Model_ConsumersMapper();
						$consumers = $consumerMapper->getAllConsumers(false);
						$rowCount = 2;
					
						foreach(array_reverse($consumers) as $consumer)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_connection_id"));
							$rowCount++;
						}
						 $objPHPExcel->setActiveSheetIndex(3);
						$agentsMapper = new Application_Model_CollectionAgentsMapper();
						$agents = $agentsMapper->getAllCollectionAgents();
						$rowCount = 2;
					
						foreach(array_reverse($agents) as $agent)
						{
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $agent->__get("collection_agent_id"));
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $agent->__get("agent_fname"));
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $agent->__get("agent_lname"));
							$rowCount++;
						} 
						break;
						
						case 'VN_Monthly_Revenue':
							$newfile = '2 VN - Customer Monthly Revenue'.date('d M,Y h-i A').'.xlsm';
							copy('ExcelFiles/2 VN - Customer Monthly Revenue - AR ON.xlsm', $newfile);
							$inputFileType = 'Excel2007';
							$inputFileName = $newfile;
								
							$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
							$objPHPExcel->setActiveSheetIndex(0);
								
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
							$rowCount = 2;
								
							foreach(array_reverse($cashRegisters) as $cashRegister)
							{
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
								$rowCount++;
							}
							$objPHPExcel->setActiveSheetIndex(1);
							$consumerMapper = new Application_Model_ConsumersMapper();
							$consumers = $consumerMapper->getAllConsumers(false);
							$rowCount = 2;
								
							foreach(array_reverse($consumers) as $consumer)
							{
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_act_charge"));
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("consumer_connection_id"));
								$rowCount++;
							}
							
					break;
					case "RF_Monthly" :
							$newfile = 'RF - Monthly report'.date('d M,Y h-i A').'.xlsm';
							copy('ExcelFiles/RF - Monthly report.xlsm', $newfile);
							$inputFileType = 'Excel2007';
							$inputFileName = $newfile;
							
							$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
							$objPHPExcel->setActiveSheetIndex(0);
							
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
							$rowCount = 2;
							$sheet=$objPHPExcel->getActiveSheet();
							$num_rows = count($cashRegisters);
							$row = 2;
							$sheet->insertNewRowBefore($row, $num_rows);
							
							foreach(array_reverse($cashRegisters) as $cashRegister)
							{
								$sheet->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
								$sheet->setCellValue('B'.$rowCount, $cashRegister->__get("cr_entry_type"));
								$sheet->setCellValue('C'.$rowCount, $cashRegister->__get("cr_amount"));
								$sheet->setCellValue('D'.$rowCount, $cashRegister->__get("timestamp"));
								$rowCount++;
							}
							$objPHPExcel->setActiveSheetIndex(1);
							$consumerMapper = new Application_Model_ConsumersMapper();
							if($from !=null && $from!=="")
								{
									$consumers = $consumerMapper->getAllConsumers(false,null,null,null,'true');
								}else{
									$consumers = $consumerMapper->getAllConsumers(false);
								}
							
							$rowCount = 2;
							$sheet=$objPHPExcel->getActiveSheet();
							$num_rows = count($consumers);
							$row = 2;
							$sheet->insertNewRowBefore($row, $num_rows);
							foreach(array_reverse($consumers) as $consumer)
							{
								$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
								foreach ($consumerPackages as $consumerPackage){
								$sheet->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
								$sheet->setCellValue('B'.$rowCount, $consumerPackage["package_id"]);
								//$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_name"));
								$sheet->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
								$sheet->setCellValue('D'.$rowCount, $consumer->__get("wattage"));
								$sheet->setCellValue('E'.$rowCount, $consumer->__get("consumer_act_date"));
								$sheet->setCellValue('F'.$rowCount, $consumer->__get("consumer_connection_id"));
								$sheet->setCellValue('G'.$rowCount, $consumer->__get("type_of_me"));
								$rowCount++;
								}
							}
							$objPHPExcel->setActiveSheetIndex(2);
							$packagesMapper = new Application_Model_PackagesMapper();
							$packages = $packagesMapper->getAllPackages(true);
							$rowCount = 2;
							$sheet=$objPHPExcel->getActiveSheet();
							$num_rows = count($packages);
							$row = 2;
							$sheet->insertNewRowBefore($row, $num_rows);
							foreach(array_reverse($packages) as $package)
							{
								$sheet->setCellValue('A'.$rowCount, $package->__get("package_id"));
								$sheet->setCellValue('B'.$rowCount, $package->__get("package_name"));
								$sheet->setCellValue('C'.$rowCount, $package->__get("package_cost"));
								$sheet->setCellValue('D'.$rowCount, $package->__get("package_details"));
								$rowCount++;
							}
							break;
				
							case 'CM_XL07':
								$newfile = 'CM XL07 Defaulters'.date('d M,Y h-i A').'.xlsm';
								copy('ExcelFiles/CM XL07 Defaulters.xlsm', $newfile);
								$inputFileType = 'Excel2007';
								$inputFileName = $newfile;
							
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								$objPHPExcel->setActiveSheetIndex(0);
							
								$cashRegisterMapper = new Application_Model_CashRegisterMapper();
								$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
								$rowCount = 2;
							
								foreach(array_reverse($cashRegisters) as $cashRegister)
								{
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
									$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
								
									$rowCount++;
								}
								$objPHPExcel->setActiveSheetIndex(1);
								$consumerMapper = new Application_Model_ConsumersMapper();
								$consumers = $consumerMapper->getAllConsumers(false);
								$rowCount = 2;
							
								foreach(array_reverse($consumers) as $consumer)
								{
										
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_name"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_status"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
									$rowCount++;
								}
								break;
					case 'SD_KwhEnroll':
									$newfile = 'SD - KwH & Enrollment'.date('d M,Y h-i A').'.xlsm';
									copy('ExcelFiles/SD - KwH & Enrollment.xlsm', $newfile);
									$inputFileType = 'Excel2007';
									$inputFileName = $newfile;
								
									$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
									$objPHPExcel->setActiveSheetIndex(0);
								
									$cashRegisterMapper = new Application_Model_CashRegisterMapper();
									$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
									$rowCount = 2;
								
									foreach(array_reverse($cashRegisters) as $cashRegister)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("transaction_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_entry_type"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("cr_amount"));
										$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("timestamp"));
										$rowCount++;
									}
									$objPHPExcel->setActiveSheetIndex(1);
									$consumerMapper = new Application_Model_ConsumersMapper();
									$consumers = $consumerMapper->getAllConsumers(false);
									$rowCount = 2;
								
									foreach(array_reverse($consumers) as $consumer)
									{
										$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
										foreach ($consumerPackages as $consumerPackage){
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumerPackage["package_id"]);
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_name"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_status"));
										$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("wattage"));
										$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("micro_entrprise_price"));
										$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowCount, $consumer->__get("consumer_act_date"));
										$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowCount, $consumer->__get("consumer_connection_id"));
										$rowCount++;
										}
									}
									$objPHPExcel->setActiveSheetIndex(2);
									$meterReadingMapper = new Application_Model_MeterReadingsMapper();
									$meterReading = $meterReadingMapper->getAllMeterReadings();
									$rowCount = 2;
									
									foreach(array_reverse($meterReading) as $meter)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $meter->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $meter->__get("meter_reading"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $meter->__get("timestamp"));
								
										$rowCount++;
									}
									$objPHPExcel->setActiveSheetIndex(5);
									$packagesMapper = new Application_Model_PackagesMapper();
									$packages = $packagesMapper->getAllPackages(true);
									$rowCount = 2;
								
									foreach(array_reverse($packages) as $package)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_name"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $package->__get("package_cost"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $package->__get("package_details"));
										$rowCount++;
									}
						break;
				case 'RF_WeeklyEnroll':
										$newfile = 'RF - Weekly Enrollment'.date('d M,Y h-i A').'.xlsm';
										copy('ExcelFiles/RF - Weekly Enrollment.xlsm', $newfile);
										$inputFileType = 'Excel2007';
										$inputFileName = $newfile;
									
										$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
										$objPHPExcel->setActiveSheetIndex(0);
									
										$consumerMapper = new Application_Model_ConsumersMapper();
										$consumers = $consumerMapper->getAllConsumers(false);
										$rowCount = 2;
									
										foreach(array_reverse($consumers) as $consumer)
										{
											
											$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
											foreach ($consumerPackages as $consumerPackage){
											$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumerPackage["package_id"]);
											$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_status"));
											$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_act_date"));
											$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
											$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("suspension_date"));
											$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("type_of_me"));

											$rowCount++;
											}
										}
										$objPHPExcel->setActiveSheetIndex(1);
										$packagesMapper = new Application_Model_PackagesMapper();
										$packages = $packagesMapper->getAllPackages(true);
										$rowCount = 2;
									
										foreach(array_reverse($packages) as $package)
										{
											$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
											$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_name"));
											$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $package->__get("package_cost"));
											$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $package->__get("package_details"));
											$rowCount++;
										}
										break;
					case "SPI_Monthly" :
								$newfile = 'SPI Monthly Report'.date('d M,Y h-i A').'.xlsm';
								copy('ExcelFiles/SPI Monthly Report.xlsm', $newfile);
								$inputFileType = 'Excel2007';
								$inputFileName = $newfile;
									
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								$objPHPExcel->setActiveSheetIndex(0);
								$rowCount = 2;

								$consumerMapper = new Application_Model_ConsumersMapper();
								if($from !=null && $from!=="")
								{
									$consumers = $consumerMapper->getAllConsumers(false,null,null,null,'true');
								}else{
									$consumers = $consumerMapper->getAllConsumers(false);
								}
								$rowCount = 2;
							  
								foreach(array_reverse($consumers) as $consumer)
								{
									$consumerPackages=$consumerPackageMapper->getPackageByConsumerId($consumer->__get("consumer_id"));
									foreach ($consumerPackages as $consumerPackage){
									//$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumerPackage["package_id"]);
									//$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_name"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_status"));
									//$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("wattage"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_act_date"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_connection_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("suspension_date"));
									$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("type_of_me"));
							        $rowCount++;
									
									}
								}
								$objPHPExcel->setActiveSheetIndex(1);
								$packagesMapper = new Application_Model_PackagesMapper();
								$packages = $packagesMapper->getAllPackages(true);
								$rowCount = 2;
									
								foreach(array_reverse($packages) as $package)
								{
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $package->__get("package_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $package->__get("package_name"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $package->__get("package_cost"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $package->__get("package_details"));
									
									$rowCount++;
								}
								break;
								
		case "Debtor_Aging" :
								$newfile = 'Debtor Aging Sheet'.date('d M,Y h-i A').'.xlsm';
								copy('ExcelFiles/Debtor Aging Sheet.xlsm', $newfile);
								$inputFileType = 'Excel2007';
								$inputFileName = $newfile;
									
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								$objPHPExcel->setActiveSheetIndex(0);
								$rowCount = 2;

								$consumerMapper = new Application_Model_ConsumersMapper();
								$consumers = $consumerMapper->getAllConsumers(false);
								$rowCount = 2;
									
								foreach(array_reverse($consumers) as $consumer)
								{
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_connection_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("site_id"));
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_status"));
									$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $consumer->__get("consumer_act_date"));
									$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowCount, $consumer->__get("consumer_act_charge"));

							
									$rowCount++;
								}
									$objPHPExcel->setActiveSheetIndex(1);
									$cashRegisterMapper = new Application_Model_CashRegisterMapper();
									$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
									$rowCount = 2;
								
									foreach(array_reverse($cashRegisters) as $cashRegister)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("cr_entry_type"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_amount"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("timestamp"));
									//	$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowCount, $cashRegister->__get("cr_status"));
										$rowCount++;
									}
									
								/*	$objPHPExcel->setActiveSheetIndex(2);
									$siteMapper = new Application_Model_SitesMapper();
									$sites=$siteMapper->getAllSites();
									$rowCount = 2;
								
									foreach(array_reverse($sites) as $site)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $site->__get("site_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $site->__get("site_name"));
										$rowCount++;
									}*/
								break;	
								case "Socket8001" : 		
								$newfile = 'Report template '.date('d M,Y h-i A').'.xlsm';
								copy('ExcelFiles/Report_template.xlsm', $newfile);
								$inputFileType = 'Excel2007';
								$inputFileName = $newfile;
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								break; 

case "MED_Revenue" :
									$newfile = 'MED Revenue data.xlsm'; 
									copy('ExcelFiles/MED Revenue data.xlsm', $newfile);
									$inputFileType = 'Excel2007';
									$inputFileName = $newfile;
										
									$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
									$objPHPExcel->setActiveSheetIndex(0);
									$rowCount = 2;
								
									$consumerMapper = new Application_Model_ConsumersMapper();
									$consumers = $consumerMapper->getAllConsumers(false);
									$rowCount = 2;
										
									foreach(array_reverse($consumers) as $consumer)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $consumer->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $consumer->__get("consumer_connection_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $consumer->__get("consumer_act_date"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $consumer->__get("consumer_status"));
										 
								
											
										$rowCount++;
									}
									$objPHPExcel->setActiveSheetIndex(1);
									$cashRegisterMapper = new Application_Model_CashRegisterMapper();
									$cashRegisters=$cashRegisterMapper->getAllCashRegisterByStatus();
									$rowCount = 2;
								
									foreach(array_reverse($cashRegisters) as $cashRegister)
									{
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $cashRegister->__get("consumer_id"));
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $cashRegister->__get("cr_entry_type"));
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowCount, $cashRegister->__get("cr_amount"));
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowCount, $cashRegister->__get("timestamp"));
										 
										$rowCount++;
									}
										
									$objPHPExcel->setActiveSheetIndex(2);
									 $siteMapper = new Application_Model_SitesMapper();
									 $sites=$siteMapper->getAllSites();
									 $rowCount = 2;
								
									 foreach(array_reverse($sites) as $site)
									 {
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $site->__get("site_id"));
									 	$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $site->__get("site_name"));
									 	
									 	$rowCount++;
									 }
									 $objPHPExcel->setActiveSheetIndex(3);
									 $CelamedMapper = new Application_Model_CelamedMapper();
									 $Celameds=$CelamedMapper->getAllCelameds();
									 $rowCount = 2;
									 
									 foreach(array_reverse($Celameds) as $Celamed)
									 {
									 	$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowCount, $Celamed->__get("consumer_id"));
									 	$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowCount, $Celamed->__get("intervention_type"));
									 		
									 	$rowCount++;
									 }
									break;							
			case "YTD_Sheet_exl" :
		 		$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
				$trans_date=date_parse_from_format("Y-m-d", $timestamp);
				$day=$trans_date["day"];
			 	$month= $trans_date["month"];
				 
				$year= $trans_date["year"];
				$stateMapper=new Application_Model_StatesMapper();
				$clusterMapper=new Application_Model_ClustersMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$siteMapper=new Application_Model_SitesMapper();
				$consumerMapper=new Application_Model_ConsumersMapper();
				$packageMapper=new Application_Model_PackagesMapper();
				
		 		$StateName_Array=array();
			  	$sites=$siteMapper->getAllSites(); 
				$counters=0;
				$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
				$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
			  $consumerdata="";
			 	
			  foreach ($sites as $site)
			  {
			  	$consumerdata.="";
			  	if($site->__get("site_id")!=1){
			  		 
					 $consumerdata.= " <h4 align='center'>Collection Sheet for ".date('F, Y')."</h4>";
			  
			  $stateName=$stateMapper->getStateById($site->__get("state_id"));
			  $consumerdata.= "<div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>
	                <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
                        <tbody>
                         <tr>
						  <th colspan='29' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			 
			  $consumerdata.= "<tr>
							<th></th> 
							<th></th> 
							<th></th>
							<th></th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
								
									$year_vals=$year;
									if($month >=4)
										$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
										$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th width='120px' colspan='5'>".$months[$m].",".$year_vals."</th>
											<th></th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
                                <th width='50px'>S.No</th>
                                <th width='150px'>Connection ID</th>
				 				<th width='130px'>Consumers Name</th> 
							  	<th width='130px'>Mobile No.</th>";
								
							for($m=0 ; $m<sizeof($months);$m++)
							{
								 
									$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "
                                <th  width='80px'>Balance</th>
							 </tr>";

			  				$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"),true);
			  				 
			  				if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			  				foreach ($consumers as $consumer){
			  					$year_balance=0;
			  					$totalDebit=0;$totalCredit=0;
			  					$consumer_id=$consumer->__get("consumer_id");	
				  				$count= $count+1;
				  				$sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM)</b>";
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				}}
								$consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$consumer->__get("consumer_connection_id").$metered."</td>
										    <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
											<td>".$consumer->__get("consumer_code")."</td>";
								for($n=0 ; $n<sizeof($month_no);$n++)
								{
									 
									$year_val=$year;
									if($month >=4)
										$year_val=($month_no[$n] >=4)?$year:$year+1;
									else {
										$year_val=($month_no[$n] >=4)?$year-1:$year;
									}
									$debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$n],$year_val);
				  					$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
									$consumerdata.= "<td>".$debit."</td>
										    <td>".$credit."</td>";
									$totalDebit=$totalDebit+$debit;
									$totalCredit=$totalCredit+$credit;
									if($month_no[$n]==$month){
										break;
									}
								}		    
								$year_balance=$totalDebit - $totalCredit;
			                     $consumerdata.= "<td align='center'>". $year_balance."</td>
				                </tr> ";
			  				}
				  		 }
			  		 
			 	 
			  $consumerdata.=  "</tbody> </table> </div></div></div></div>
			  <h4 align='center'>Banned Consumers</h4>
			  <div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>		
			  		<table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
			  			<tbody>
			  			<tr>
			  			<th colspan='29' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			  
						$consumerdata.= "<tr>
							<th></th> 
							<th></th> 
							<th></th>
							<th></th>
								<th></th>";
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
							  	  $year_vals=$year;
								  if($month >=4)
								  	$year_vals=($month_no[$m] >=4)?$year:$year+1;
								  else {
								  	$year_vals=($month_no[$m] >=4)?$year-1:$year;
								  }
								  $consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>
								  		<th></th>";
								  if($month_no[$m]==$month){
								  	break;
								  }
							  }
							  $consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
							  		<th width='50px'>S.No</th>
							  		<th width='150px'>Connection ID</th>
							  		<th width='130px'>Consumers Name</th>
							  		<th width='130px'>Mobile No.</th>
							  		<th width='130px'>Banned Date</th>";
							  
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
							  		$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
							  		if($month_no[$m]==$month){
							  			break;
							 		}
							  }
							  $consumerdata.= "
							  	<th  width='80px'>Balance</th>
							  </tr>";
							  
							  $consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"));
							  
							  if($consumers){
							  $count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
							  foreach ($consumers as $consumer){
							  $year_balance=0;
							  $totalDebit=0;$totalCredit=0;
							  $consumer_id=$consumer->__get("consumer_id");
							  $count= $count+1;
							  $sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
							  $DODate=$consumerMapper->getConsumerDODatebyConnectionId($consumer->__get("consumer_connection_id"));
							   

				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM)</b>";
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				}}
							  $consumerdata.= "  <tr>
							  <td>".$count."</td>
							  <td>".$consumer->__get("consumer_connection_id").$metered."</td>
							  <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
							  		<td>".$consumer->__get("consumer_code")."</td>
							  				<td>".$DODate["timestamp"]."</td>";
							  		for($n=0 ; $n<sizeof($month_no);$n++)
							  		{
							  			$year_val=$year;
							  			if($month >=4)
							  				$year_val=($month_no[$n] >=4)?$year:$year+1;
							  			else {
							  				$year_val=($month_no[$n] >=4)?$year-1:$year;
							  			}
							  			$debit=$cashRegisterMapper->getMonthlyDueByConsumerId($consumer_id,$month_no[$n],$year_val);
							  			$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
							  			$consumerdata.= "<td>".$debit."</td>
							  			<td>".$credit."</td>";
							  			$totalDebit=$totalDebit+$debit;
							  			$totalCredit=$totalCredit+$credit;
							  			if($month_no[$n]==$month){
							  					break;
							  		}
							  }
							  		$year_balance=$totalDebit - $totalCredit;
							  		$consumerdata.= "<td align='center'>". $year_balance."</td>
							  		</tr> ";
 
							  }
							}
					$consumerdata.=  "</tbody> </table>
				                		</div>
				           			</div>
						   		</div>
				         	</div> 
				<h5 align='right'><i>File created on ".date('jS F, Y')." at ".date('h:i A')." </i></h5>";
				 
         	 				
				}
			}
							$newfile = 'Collection Sheet '.date('d M,Y h-i A').'.xlsx';
							$inputFileType = 'Excel2007';
							$tmpfile = tempnam(sys_get_temp_dir(), 'html');
							file_put_contents($tmpfile, $consumerdata);
							 
							$objPHPExcel = new PHPExcel();
							$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
							$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
							$objPHPExcel->getActiveSheet()->setTitle('Collection Sheet'); 

							unlink($tmpfile);

							break;

		case "YTD_Sheet_old_exl" :
				$date = new Zend_Date();
				$date->setTimezone("Asia/Calcutta");
				$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
				$trans_date=date_parse_from_format("Y-m-d", $timestamp);
				$day=$trans_date["day"];
			 	$month= $trans_date["month"];
				//$month= 8;$year=2016;
				$year= $trans_date["year"];
				$stateMapper=new Application_Model_StatesMapper();
				$clusterMapper=new Application_Model_ClustersMapper();
				$cashRegisterMapper = new Application_Model_CashRegisterMapper();
				$siteMapper=new Application_Model_SitesMapper();
				$consumerMapper=new Application_Model_ConsumersMapper();
				$packageMapper=new Application_Model_PackagesMapper();
				
		 		$StateName_Array=array(); 
			  	$sites=$siteMapper->getAllSites(); 
				$counters=0;
				$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
				$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
				$consumerdata="";
			  foreach ($sites as $site)
			  {
			  	$consumerdata.="";
			  	if($site->__get("site_id")!=1){
			  		 
					 $consumerdata.= " <h4 align='center'>Collection Sheet (Old Cycle) for ".date('F, Y')."</h4>";
			  
			  $stateName=$stateMapper->getStateById($site->__get("state_id"));
			  $consumerdata.= "<div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>
	                <table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
                        <tbody>
                         <tr>
						  <th colspan='29' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			 
			  $consumerdata.= "<tr>
                                <th></th>
						  		<th></th>
						  		<th></th>  
						  		<th></th>";

							for($m=0 ; $m<sizeof($months);$m++)
							{
								
									$year_vals=$year;
									if($month >=4)
										$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
										$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>
											<th></th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
                                <th width='50px'>S.No</th>
                                <th width='150px'>Connection ID</th>
				 				<th width='130px'>Consumers Name</th> 
							  	<th width='130px'>Mobile No.</th>";
								
							for($m=0 ; $m<sizeof($months);$m++)
							{
								 
									$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>
											<th></th>";
									if($month_no[$m]==$month){
										break;
									}
							}
				$consumerdata.= "
                                <th  width='80px'>Balance</th>
							 </tr>";

			  				$consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"),true);
			  				 
			  				if($consumers){
			  					$count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
			  				foreach ($consumers as $consumer){
			  					$year_balance=0;
			  					$totalDebit=0;$totalCredit=0;
			  					$consumer_id=$consumer->__get("consumer_id");	
				  				$count= $count+1;
				  				$sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM)</b>";
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				}
								}
								$consumerdata.= "  <tr>
		                                    <td>".$count."</td>
		                                    <td>".$consumer->__get("consumer_connection_id").$metered."</td>
										    <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
											<td>".$consumer->__get("consumer_code")."</td>";
								for($n=0 ; $n<sizeof($month_no);$n++)
								{
									 
									$year_val=$year;
									if($month >=4)
										$year_val=($month_no[$n] >=4)?$year:$year+1;
									else {
										$year_val=($month_no[$n] >=4)?$year-1:$year;
									}
									$debit=$cashRegisterMapper->getMonthlyDebitByConsumerId($consumer_id,$month_no[$n],$year_val);
				  					$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
									$consumerdata.= "<td>".$debit."</td>
										    <td>".$credit."</td>";
									$totalDebit=$totalDebit+$debit;
									$totalCredit=$totalCredit+$credit;
									if($month_no[$n]==$month){
										break;
									}
								}		    
								$year_balance=$totalDebit - $totalCredit;
			                     $consumerdata.= "<td align='center'>". $year_balance."</td>
				                </tr> ";
			  				}
				  	 }
			  		  
			  		 
				 
			  $consumerdata.=  "</tbody> </table> </div></div></div></div>
			  <h4 align='center'>Banned Consumers</h4>
			  <div>
			   <div class='panel-body'>
            	<div class='table-scrollable'>
             	<div class='adv-table'>		
			  		<table border=1 align='center' class='display table table-bordered' id='dynamic-table'>
			  			<tbody>
			  			<tr>
			  			<th colspan='29' align='left'>".$site->__get("site_name").", ".$stateName->__get("state_name")."<br></th> ";
			  
						$consumerdata.= "<tr>
							<th></th>
						  		<th></th>
						  		<th></th>
						  		<th></th>
								<th></th>";
							  
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
							  	  $year_vals=$year;
								  if($month >=4)
								  	$year_vals=($month_no[$m] >=4)?$year:$year+1;
								  else {
								  	$year_vals=($month_no[$m] >=4)?$year-1:$year;
								  }
								  $consumerdata.= "<th width='120px' colspan='2'>".$months[$m].",".$year_vals."</th>
								  		<th></th>";
								  if($month_no[$m]==$month){
								  	break;
								  }
							  }
							  $consumerdata.= "</tr>";
							  $consumerdata.= "<tr>
							  		<th width='50px'>S.No</th>
							  		<th width='150px'>Connection ID</th>
							  		<th width='130px'>Consumers Name</th>
							  		<th width='130px'>Mobile No.</th>
							  		<th width='130px'>Banned Date</th>";
							  
							  for($m=0 ; $m<sizeof($months);$m++)
							  {
							  		$consumerdata.= "<th width='60px' >D</th><th width='60px' >C</th>";
							  		if($month_no[$m]==$month){
							  			break;
							 		}
							  }
							  $consumerdata.= "
							  	<th  width='80px'>Balance</th>
							  </tr>";
							  
							  $consumers=$consumerMapper->getConsumersBySiteId($site->__get("site_id"));
							  
							  if($consumers){
							  $count=0;$s_otpOs=0;$s_cacti=0;$s_totalOutStanding=0;$s_debit=0;$s_credit=0;$s_total=0;
							  foreach ($consumers as $consumer){
							  $year_balance=0;
							  $totalDebit=0;$totalCredit=0;
							  $consumer_id=$consumer->__get("consumer_id");
							  $count= $count+1;
							  $sites_val=$packageMapper->getPackageById($consumer->__get("package_id"));
							  $DODate=$consumerMapper->getConsumerDODatebyConnectionId($consumer->__get("consumer_connection_id"));
							  
				  				$metered="";
								if($sites_val){
				  				if($sites_val->__get("is_postpaid")==2){
				  					$metered="<b> (MM)</b>";
				  				}elseif ($sites_val->__get("is_postpaid")==1){
				  					$metered="<b> (ME)</b>";
				  				}}
							  $consumerdata.= "  <tr>
							  <td>".$count."</td>
							  <td>".$consumer->__get("consumer_connection_id").$metered."</td>
							  <td>".ucwords(strtolower($consumer->__get("consumer_name")))."</td>
							  		<td>".$consumer->__get("consumer_code")."</td>
							  				<td>".$DODate["timestamp"]."</td>";
							  		for($n=0 ; $n<sizeof($month_no);$n++)
							  		{
							  			$year_val=$year;
							  			if($month >=4)
							  				$year_val=($month_no[$n] >=4)?$year:$year+1;
							  			else {
							  				$year_val=($month_no[$n] >=4)?$year-1:$year;
							  			}
							  			$debit=$cashRegisterMapper->getMonthlyDebitByConsumerId($consumer_id,$month_no[$n],$year_val);
							  			$credit=$cashRegisterMapper->getMonthlyInByConsumerId($consumer_id,$month_no[$n],$year_val);
							  			$consumerdata.= "<td>".$debit."</td>
							  			<td>".$credit."</td>";
							  			$totalDebit=$totalDebit+$debit;
							  			$totalCredit=$totalCredit+$credit;
							  			if($month_no[$n]==$month){
							  					break;
							  		}
							  }
							  		$year_balance=$totalDebit - $totalCredit;
							  		$consumerdata.= "<td align='center'>". $year_balance."</td>
							  		</tr> ";
							  }
							}
					$consumerdata.=  "</tbody> </table>
				                		</div>
				           			</div>
						   		</div>
				         	</div> 
						<h5 align='right'><i>File created on ".date('jS F, Y')." at ".date('h:i A')." </i></h5>";
					
         	 		 
         	 		
				}
			  }
						$newfile = 'Collection Sheet (Old Cycle) '.date('d M,Y h-i A').'.xlsx';
						$inputFileType = 'Excel2007';
						$tmpfile = tempnam(sys_get_temp_dir(), 'html');
						file_put_contents($tmpfile, $consumerdata);
						
						$objPHPExcel = new PHPExcel();
						$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
						$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('Collection Sheet (Old Cycle)');
							
				break;
		case "Monthly_revenue_report" :
					$date = new Zend_Date();
					$date->setTimezone("Asia/Calcutta");
					$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
				
					$trans_date=date_parse_from_format("Y-m-d", $timestamp);
					$day=$trans_date["day"];
					$month= $trans_date["month"];
					$years= $trans_date["year"];
					 
					$stateMapper=new Application_Model_StatesMapper();
					$clusterMapper=new Application_Model_ClustersMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
				
					$StateName_Array=array();
					$sites=$siteMapper->getAllSites();
					$counters=0;
					$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
					$month_no=[4,5,6,7,8,9,10,11,12,1,2,3];
					$consumerdata="";
					$states=$stateMapper->getAllStates();
					$lastyear=$year-1;
					
					$consumerdata.= " <h4><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>Monthly Revenue Report for Year ".$lastyear."-".$year."</h4>";
					
							
								
							 
			 $consumerdata.= "<div>
			  
	                <table border=1 align='center'>
                        <tbody>";
				
		     	$consumerdata.= "<tr><th></th>";
				
							for($m=0 ; $m<sizeof($months);$m++)
							{
				
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th></th>
											<th>".$months[$m].",".$year_vals."</th>
											<th></th>";
									if($year==$years){
										if($month_no[$m]==$month){
											break;
											}
									}
							}
							$consumerdata.= "</tr><tr><th>Site Name</th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
							
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th> Billed Amt.</th>
											<th>Collected Amt.</th>
											<th>Outstanding Amt.</th>";
									if($year==$years){
										if($month_no[$m]==$month){
											break;
											}
									}
							}
							$consumerdata.= "</tr>";
								 
							foreach ($states as $state){
							 
							 $consumerdata.= "<tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th><h4>".$state->__get("state_name")."</h4></th>
								</tr>";
					$sites=$siteMapper->getSiteByStateId($state->__get("state_id"));
					foreach ($sites as $site)
					 {
							 		
						if($site->__get("site_id")!=1){
							 
					 $consumerdata.= "<tr><td>".$site->__get("site_name")."</td>";
					 
					 for($m=0 ; $m<sizeof($months);$m++)
					 {
					 	
					 $year_vals=$year;
					 	if($month >=4)
					 		$year_vals=($month_no[$m] >=4)?$year:$year+1;
					 		else {
					 		$year_vals=($month_no[$m] >=4)?$year-1:$year;
					 		}
					 		//$balance_debit=$cashRegisterMapper->getBalanceBySite($site->__get("site_id"),$month_no[$m],$year,"DEBIT");
					 		//$balance_credit=$cashRegisterMapper->getBalanceBySite($site->__get("site_id"),$month_no[$m],$year,"CREDIT");
					 		$balance_debit = $cashRegisterMapper->getTotalCollectedByMonth($month_no[$m],$site->__get("site_id"),"DEBIT",$year_vals);
					 		$balance_credit=$cashRegisterMapper->getTotalCollectedByMonth($month_no[$m],$site->__get("site_id"),"CREDIT",$year_vals);
					 		

									$consumerdata.= "<td> ".$balance_debit."</td>
									 		<td>".$balance_credit."</td>
									 		<td>".($balance_debit-$balance_credit)."</td>";
					 				if($year==$years){
										if($month_no[$m]==$month){
											break;
											}
									}
					 }
					 	$consumerdata.= "</tr> ";
					 
			  			 	}
					 }
					 
					 $consumerdata.= "</tr><tr><th><b>Total</b></th>";
					 for($m=0 ; $m<sizeof($months);$m++)
					 {
					 	
					 $year_vals=$year;
					 	if($month >=4)
					 		$year_vals=($month_no[$m] >=4)?$year:$year+1; 
					 		else {
					 		$year_vals=($month_no[$m] >=4)?$year-1:$year;
					 		}
					 		//$Total_balance_debit=$cashRegisterMapper->getBalanceByState($state->__get("state_id"),$month_no[$m],$year,"DEBIT");
					 		//$Total_balance_credit=$cashRegisterMapper->getBalanceByState($state->__get("state_id"),$month_no[$m],$year,"CREDIT");
					 		$Total_balance_debit = $cashRegisterMapper->getTotalCollectedByMonth($month_no[$m],null,"DEBIT",$year_vals,$state->__get("state_id"));
					 		$Total_balance_credit=$cashRegisterMapper->getTotalCollectedByMonth($month_no[$m],null,"CREDIT",$year_vals,$state->__get("state_id"));
					 		
					 			$consumerdata.= "<td> ".$Total_balance_debit."</td>
									 		<td>".$Total_balance_credit."</td>
									 		<td>".($Total_balance_debit-$Total_balance_credit)."</td>";
					 				if($year==$years){
										if($month_no[$m]==$month){
											break;
											}
									}
							}
					  $consumerdata.= "</tr>";
					 
							}
										$consumerdata.=  "</tbody> </table>
						
										</div>";
									
										$newfile = 'Monthly Revenue Report for '.$lastyear.'-'.$year.'.xlsx';
										$inputFileType = 'Excel2007';
												$tmpfile = tempnam(sys_get_temp_dir(), 'html');
												file_put_contents($tmpfile, $consumerdata);
				
												$objPHPExcel = new PHPExcel();
												$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
												$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('Monthly Revenue Report '); 
													
												break;
			case "Monthly_active_consumer" :
													 
						$stateMapper=new Application_Model_StatesMapper();
						$cashRegisterMapper = new Application_Model_CashRegisterMapper();
						$siteMapper=new Application_Model_SitesMapper();
						$consumerMapper=new Application_Model_ConsumersMapper();
						$packageMapper=new Application_Model_PackagesMapper();
						$consumerdata="";
						$StateName_Array=array();
													 	
						$consumerdata.= "<div>
			
	                <table border=1 align='center'>
                        <tbody>";
												
				$consumerdata.= "<tr>
								<th>S.NO.</th>
								<th>Consumer Connection ID</th> 
								<th>Consumer Name</th> 
								<th>Status</th> 
								<th>Type of ME</th> 
								<th>Activation Date</th> 
								<th>Package Type</th> 
								<th>Pkg. Cost (Old)</th> 
								<th>Package Cost</th> 
								<th>Site Name</th> 
								<th>State Name</th> 
								<th>Upgradation Date</th> 							 
						 </tr>";
					$consumers=$consumerMapper->getConsumersWithActDate($month,$year);	 
					$Total_cost=0;$Total_cost_old=0;
					$main_summary_array=array();
					  	foreach ($consumers as $consumer)
					 	{
					 		$sites=$siteMapper->getSiteById($consumer->__get("site_id"));
					 		$state=$stateMapper->getStateById($sites->__get("state_id"));
					 		$package=$packageMapper->getPackageById($consumer->__get("package_id"));
					 		$Package_his=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,$consumer->__get("consumer_id"),"active");
					 		$upgradation="";$old_packageCost="";
							if($Package_his){
					 		if($Package_his[0]->__get("old_package_id")!=NULL){
					 			$oldPackage=$packageMapper->getPackageById($Package_his[0]->__get("old_package_id"));
					 			if($oldPackage){
					 				$packageCost=$Package_his[0]->__get("package_cost");
					 				$old_packageCost=$oldPackage->__get("package_cost");
					 				if($old_packageCost<$packageCost)
					 				{
										$Total_cost_old=$Total_cost_old+$old_packageCost;
					 					$upgradation=$Package_his[0]->__get("package_change_date");
					 				}
					 			}
					 		}}
							$Total_cost=$Total_cost+$package->__get("package_cost");
							if($package->__get("is_postpaid")==2){
								$value='MixMode' ;
							}elseif($package->__get("is_postpaid") ==1){
								$value= 'Postpaid';
							}else{
								$value= 'Prepaid' ;
							}
							$datas=array(
									"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
									"consumer_name"=>$consumer->__get("consumer_name"),
									"consumer_status"=>$consumer->__get("consumer_status"),
									"type_of_me"=>$consumer->__get("type_of_me"),
									"consumer_act_date"=>$consumer->__get("consumer_act_date"),
									"ispostpaid"=>$value,
									"oldPackge"=>$old_packageCost,
									"package_cost"=>$package->__get("package_cost"),
									"site_name"=>$sites->__get("site_name"),
									"state_name"=>$state->__get("state_name"),
									"upgradation"=>$upgradation
							);
							$main_summary_array[]=$datas;
						}
						$Package_changes=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,null,"active");
						foreach ($Package_changes as $Package_change){
							if($Package_change->__get("old_package_id")!=NULL){
								$oldPackage=$packageMapper->getPackageById($Package_change->__get("old_package_id"));
								if($oldPackage){
									$packageCost=$Package_change->__get("package_cost");
									$old_packageCost=$oldPackage->__get("package_cost");
									if($old_packageCost<$packageCost)
									{
										$consumers_detail=$consumerMapper->getConsumerById($Package_change->__get("consumer_id"));
										$sites=$siteMapper->getSiteById($consumers_detail->__get("site_id"));
										$state=$stateMapper->getStateById($sites->__get("state_id"));
						
										$Total_cost=$Total_cost+$packageCost;
										$Total_cost_old=$Total_cost_old+$old_packageCost;
										if($Package_change->__get("is_postpaid")==2){
											$value='MixMode' ;
										}elseif($Package_change->__get("is_postpaid") ==1){
											$value= 'Postpaid';
										}else{
											$value= 'Prepaid' ;
										}
										$datas=array(
												"consumer_connection_id"=>$consumers_detail->__get("consumer_connection_id"),
												"consumer_name"=>$consumers_detail->__get("consumer_name"),
												"consumer_status"=>$consumers_detail->__get("consumer_status"),
												"type_of_me"=>$consumers_detail->__get("type_of_me"),
												"consumer_act_date"=>$consumers_detail->__get("consumer_act_date"),
												"ispostpaid"=>$value,
												"oldPackge"=>$old_packageCost,
												"package_cost"=>$packageCost,
												"site_name"=>$sites->__get("site_name"),
												"state_name"=>$state->__get("state_name"),
												"upgradation"=>$Package_change->__get("package_change_date")
										);
										$main_summary_array[]=$datas;
									}
								}
							}
						}
						$sort_arr = array();
						foreach ($main_summary_array as $key => $row)
						{
							$sort_arr[$key] = $row['state_name'];
						}
						array_multisort($sort_arr, SORT_ASC ,$main_summary_array);
						$sort_arr = array();
						foreach ($main_summary_array as $key => $row)
						{
							$sort_arr[$key] = $row['consumer_act_date'];
						}
						array_multisort($sort_arr, SORT_DESC ,$main_summary_array);
						foreach ($main_summary_array as $main){
							$consumerdata.= "<tr>
											<td>".++$counter."</td>
											<td>".$main["consumer_connection_id"]."</td>
											<td>".$main["consumer_name"]."</td>
											<td>".$main["consumer_status"]."</td>
											<td>".$main["type_of_me"]."</td>
											<td>".$main["consumer_act_date"]."</td>
											<td>".$main["ispostpaid"]."</td>
											<td>".$main["oldPackge"]."</td>
											<td>".$main["package_cost"]."</td>
											<td>".$main["site_name"]."</td>
											<td>".$main["state_name"]."</td>
											<td>".$main["upgradation"]."</td>
									 </tr> ";
						}
						
							$consumerdata.= "<tr>
									<th></th><th></th><th></th><th></th><th></th><th></th><th>Total</th>
									<th>".$Total_cost_old."</th>
									<th>".$Total_cost."</th>";
							$consumerdata.= "</tr>";
						 
							$consumerdata.=  "</tbody> </table>
						</div>";
					 																		
					$newfile = 'Increment of Load in Month '.$month_val.','.$year.'.xlsx';
					$inputFileType = 'Excel2007';
					$tmpfile = tempnam(sys_get_temp_dir(), 'html');
					file_put_contents($tmpfile, $consumerdata);
												
					$objPHPExcel = new PHPExcel();
					$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
					$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
					$objPHPExcel->getActiveSheet()->setTitle('Monthly Active Consumer ');
																										
				break;
				
				case "Monthly_banned_consumer" :
				
					$stateMapper=new Application_Model_StatesMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
					$consumerdata="";
					$StateName_Array=array();
						
					$consumerdata.= "<div>
		
	                <table border=1 align='center'>
                        <tbody>";
				
					$consumerdata.= "<tr>
								<th>S.NO.</th>
								<th>Consumer Connection ID</th>
								<th>Consumer Name</th>
								<th>Status</th>
								<th>Type of ME</th>
								<th>suspension_date</th>
								<th>Package Type</th> 
								<th>Pkg. Cost (Old)</th> 
								<th>Package Cost</th>
								<th>Site Name</th>
								<th>State Name</th>
								<th>Downgrade Date</th>
						 </tr>";
					$consumers=$consumerMapper->getBannedConsumersWithActDate($month,$year);
					$Total_cost=0;$Total_cost_old=0;
					foreach ($consumers as $consumer)
					{
							$sites=$siteMapper->getSiteById($consumer->__get("site_id"));
					 		$state=$stateMapper->getStateById($sites->__get("state_id"));
					 		$package=$packageMapper->getPackageById($consumer->__get("package_id"));
					 		$Package_his=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,$consumer->__get("consumer_id"),"banned");
					 		
					 		$downdation="";$old_packageCost="";
							if($Package_his){
					 		if($Package_his[0]->__get("old_package_id")!=NULL){
					 			$oldPackage=$packageMapper->getPackageById($Package_his[0]->__get("old_package_id"));
					 			if($oldPackage){
					 				$packageCost=$Package_his[0]->__get("package_cost");
					 				$old_packageCost=$oldPackage->__get("package_cost");
					 				if($old_packageCost>$packageCost)
					 				{
										$Total_cost_old=$Total_cost_old+$old_packageCost;
					 					$downdation=$Package_his[0]->__get("package_change_date");
					 				}
					 			}
					 		} }
							$Total_cost=$Total_cost+$package->__get("package_cost");
							
							if($package->__get("is_postpaid")==2){
								$value='MixMode' ;
							}elseif($package->__get("is_postpaid") ==1){
								$value= 'Postpaid';
							}else{
								$value= 'Prepaid' ;
							}
							if($consumer->__get("consumer_status")=='active'){
								$suspension="";
							}else{
								$suspension=$consumer->__get("suspension_date");
							}
							$datas=array(
									"consumer_connection_id"=>$consumer->__get("consumer_connection_id"),
									"consumer_name"=>$consumer->__get("consumer_name"),
									"consumer_status"=>$consumer->__get("consumer_status"),
									"type_of_me"=>$consumer->__get("type_of_me"),
									"consumer_act_date"=>$suspension,
									"ispostpaid"=>$value,
									"oldPackge"=>$old_packageCost,
									"package_cost"=>$package->__get("package_cost"),
									"site_name"=>$sites->__get("site_name"),
									"state_name"=>$state->__get("state_name"),
									"upgradation"=>$downdation
							);
							$main_summary_array[]=$datas;
					}
					$Package_changes=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,null,null);
					 if($Package_changes){

					foreach ($Package_changes as $Package_change){
					if($Package_change->__get("old_package_id")!=NULL){
						$oldPackage=$packageMapper->getPackageById($Package_change->__get("old_package_id"));
						if($oldPackage){
							$packageCost=$Package_change->__get("package_cost");
							$old_packageCost=$oldPackage->__get("package_cost");
							if($old_packageCost>$packageCost)
							{
								$consumers_detail=$consumerMapper->getConsumerById($Package_change->__get("consumer_id"));
								$sites=$siteMapper->getSiteById($consumers_detail->__get("site_id"));
								$state=$stateMapper->getStateById($sites->__get("state_id"));
								$Total_cost_old=$Total_cost_old+$old_packageCost;
								$Total_cost=$Total_cost+$packageCost;
						
								if($Package_change->__get("is_postpaid")==2){
									$value='MixMode' ;
								}elseif($Package_change->__get("is_postpaid") ==1){
									$value= 'Postpaid';
								}else{
									$value= 'Prepaid' ;
								}
								if($consumers_detail->__get("consumer_status")=='active'){
									$suspension="";
								}else{
									$suspension=$consumers_detail->__get("suspension_date");
								}
								$datas=array(
										"consumer_connection_id"=>$consumers_detail->__get("consumer_connection_id"),
										"consumer_name"=>$consumers_detail->__get("consumer_name"),
										"consumer_status"=>$consumers_detail->__get("consumer_status"),
										"type_of_me"=>$consumers_detail->__get("type_of_me"),
										"consumer_act_date"=>$suspension,
										"ispostpaid"=>$value,
										"oldPackge"=>$old_packageCost,
										"package_cost"=>$packageCost,
										"site_name"=>$sites->__get("site_name"),
										"state_name"=>$state->__get("state_name"),
										"upgradation"=>$Package_change->__get("package_change_date")
								);
								$main_summary_array[]=$datas;
							}
						}
					}
					
					}
					}
					$sort_arr = array();
					foreach ($main_summary_array as $key => $row)
					{
						$sort_arr[$key] = $row['state_name'];
					}
					array_multisort($sort_arr, SORT_ASC ,$main_summary_array);
					$sort_arr = array();
					foreach ($main_summary_array as $key => $row)
					{
						$sort_arr[$key] = $row['consumer_act_date'];
					}
					array_multisort($sort_arr, SORT_DESC ,$main_summary_array);
					$counter=0;
						foreach ($main_summary_array as $main){
							$consumerdata.= "<tr>
											<td>".++$counter."</td>
											<td>".$main["consumer_connection_id"]."</td>
											<td>".$main["consumer_name"]."</td>
											<td>".$main["consumer_status"]."</td>
											<td>".$main["type_of_me"]."</td>
											<td>".$main["consumer_act_date"]."</td>
											<td>".$main["ispostpaid"]."</td>
											<td>".$main["oldPackge"]."</td>
											<td>".$main["package_cost"]."</td>
											<td>".$main["site_name"]."</td>
											<td>".$main["state_name"]."</td>
											<td>".$main["upgradation"]."</td>
									 </tr> ";
					}
					$consumerdata.= "<tr>
									<th></th><th></th><th></th><th></th><th></th><th></th><th>Total</th>
									<th>".$Total_cost_old."</th>
									<th>".$Total_cost."</th>";
					$consumerdata.= "</tr>";
						
					$consumerdata.=  "</tbody> </table>
						</div>";
					 
					$newfile = 'Short Falls of Load in Month '.$month_val.','.$year.'.xlsx';
					$inputFileType = 'Excel2007';
					$tmpfile = tempnam(sys_get_temp_dir(), 'html');
					file_put_contents($tmpfile, $consumerdata);
				
					$objPHPExcel = new PHPExcel();
					$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
					$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
					$objPHPExcel->getActiveSheet()->setTitle('Monthly Banned Consumer ');
				
					break;

case "new_consumer_statics" :
			$date = new Zend_Date();
			$date->setTimezone("Asia/Calcutta");
			$timestamp = $date->toString("yyyy-MM-dd HH:mm:ss");
					
			$trans_date=date_parse_from_format("Y-m-d", $timestamp);
			$day=$trans_date["day"];
			$month= $trans_date["month"];
			$years= $trans_date["year"];
					
			$stateMapper=new Application_Model_StatesMapper();
			$clusterMapper=new Application_Model_ClustersMapper();
			$cashRegisterMapper = new Application_Model_CashRegisterMapper();
			$siteMapper=new Application_Model_SitesMapper();
			$consumerMapper=new Application_Model_ConsumersMapper();
			$packageMapper=new Application_Model_PackagesMapper();
					
			$StateName_Array=array();
			 
			$counters=0;
			$months= [ 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb', 'Mar'];
			$month_no=['04','05','06','07','08','09','10','11','12','01','02','03'];
			$consumerdata="";
			$states=$stateMapper->getAllStates();
			$lastyear=$year-1;					
			$consumerdata.= " <h4><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>Financial Year ".$lastyear."-".$year."</h4>";
								
			$consumerdata.= "<div>
			
	                <table border=1 align='center'> 
                        <tbody>";
					
						$consumerdata.= "<tr><th>Sites</th>
											 <th>Months</th>";
					
						for($m=0 ; $m<sizeof($months);$m++)
						{
							$year_vals=$year;
							if($month >=4)
								$year_vals=($month_no[$m] >=4)?$year:$year+1;
							else {
								$year_vals=($month_no[$m] >=4)?$year-1:$year;
							}
							$consumerdata.= " 
									<th>".$months[$m].",".$year_vals."</th>
									<th></th>";
									if($year==$years){
										if($month_no[$m]==$month){
												break;
										}
									}
						}
						$consumerdata.= "</tr><tr><th></th><th></th>";
						for($m=0 ; $m<sizeof($months);$m++)
						{
						
						$year_vals=$year;
							if($month >=4)
								$year_vals=($month_no[$m] >=4)?$year:$year+1;
								else {
								$year_vals=($month_no[$m] >=4)?$year-1:$year;
								}
									$consumerdata.= "<th> Pre.p</th>
													 <th>Post.p</th>";
								if($year==$years){
									 if($month_no[$m]==$month){
											break;
									  }
								}
						}
						$consumerdata.= "</tr>";
		foreach ($states as $state){
			$consumerdata.= "</tr><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>".$state->__get("state_name")."</th></tr>";
							$consumerdata.= "<tr><th>Opening Customer</th><th></th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
							
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									 
						$preConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),true,null,'0',null,true);
						$postConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),true,null,'1',null,true);
						
						$prebannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'0',true);
					   	$postbannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'1',true);
					   		
							$totalpre=$preConsumer+$prebannedConsumer;
							$totalpost=$postConsumer+$postbannedConsumer;
								$consumerdata.= "<th>".$totalpre."</th>
									<th>".$totalpost."</th>";
									if($year==$years){
									 if($month_no[$m]==$month){
									break;
									}
								}
							}
							$consumerdata.= "</tr><tr><th>Target for the month</th><th></th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
							
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
									$consumerdata.= "<th> </th>
									<th> </th>";
									if($year==$years){
									 if($month_no[$m]==$month){
										break;
									 }
									}
							}
							$consumerdata.= "</tr><tr><th>New Customer Added</th><th></th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
								
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
									}
							$preConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),null,null,'0');
							$postConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),null,null,'1');
							
							$prebannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'0',null,true);
							$postbannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'1',null,true);
					   		
							$totalpre=$preConsumer+$prebannedConsumer;
							$totalpost=$postConsumer+$postbannedConsumer;
								$consumerdata.= "<th>".$totalpre."</th>
									<th>".$totalpost."</th>";
									if($year==$years){
									if($month_no[$m]==$month){
										break;
							}
							}
							}
						$sites=$siteMapper->getSiteByStateId($state->__get("state_id"));
						
						foreach ($sites as $site)
						{
								
							if($site->__get("site_id")!=1){
						
						$consumerdata.= "<tr><th>".$site->__get("site_name")."</th><th></th>";
							for($m=0 ; $m<sizeof($months);$m++)
							{
								
							$year_vals=$year;
							if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
							else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year;
							}
								$preConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,$site->__get("site_id"),null,null,null,'0');
								$postConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,$site->__get("site_id"),null,null,null,'1');
								
								$prebannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,$site->__get("site_id"),null,'0',null,true);
								$postbannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,$site->__get("site_id"),null,'1',null,true);
									
									$preConsumer1=$preConsumer+$prebannedConsumer;
									$postConsumer1=$postConsumer+$postbannedConsumer;
									
							$consumerdata.= "<th>".$preConsumer1."  </th>
											<th> ".$postConsumer1."</th>";
															 
								if($year==$years){
									if($month_no[$m]==$month){
											break;
									}
								}
							}
							 $consumerdata.= "</tr>";
							 
					 	  }
					   }
						   $consumerdata.= "<tr><th>Disconnections in Month</th><th></th>";
						   for($m=0 ; $m<sizeof($months);$m++)
						   {
					   
					   		$year_vals=$year;
					   		if($month >=4)
					   			$year_vals=($month_no[$m] >=4)?$year:$year+1;
					   		else {
					   			$year_vals=($month_no[$m] >=4)?$year-1:$year;
					   		}
					   		$preConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),null,true,'0',true);
					   		$postConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),null,true,'1',true);
					   		
							
					   		$consumerdata.= "<th>".$preConsumer."  </th>
											<th> ".$postConsumer."</th>";
							
					   		if($year==$years){
									if($month_no[$m]==$month){
					   					break;
					   				}
					   			}
					   		}
					   		$consumerdata.= "</tr>";

					   		$consumerdata.= "<tr><th>Net Add/Omition in Month</th><th></th>";
					   			for($m=0 ; $m<sizeof($months);$m++)
					   			{
					   				$year_vals=$year;
					   				if($month >=4)
					   						$year_vals=($month_no[$m] >=4)?$year:$year+1;
					   				else {
					   						$year_vals=($month_no[$m] >=4)?$year-1:$year;
					   				}
					   				
					   				$preConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),null,null,'0');
					   				$postConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),null,null,'1');
									$prebannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'0',null,true);
									$postbannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'1',null,true);
									
									$preConsumer1=$preConsumer+$prebannedConsumer;
									$postConsumer1=$postConsumer+$postbannedConsumer;
									
					   				
					   				$preConsumer2=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),null,true,'0',true);
					   				$postConsumer2=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),null,true,'1',true);
					   		
					   					$consumerdata.= "<th>".($preConsumer1-$preConsumer2)." </th>
					   									<th>".($postConsumer1-$postConsumer2)."</th>";
					   					if($year==$years){
											if($month_no[$m]==$month){
					   							break;
					   						}
					   					}
					   			}
					   			$consumerdata.= "</tr>";
								$consumerdata.= "<tr><th>Closing Customer</th><th></th>";
					   			for($m=0 ; $m<sizeof($months);$m++)
							{
							
							$year_vals=$year;
								if($month >=4)
									$year_vals=($month_no[$m] >=4)?$year:$year+1;
									else {
									$year_vals=($month_no[$m] >=4)?$year-1:$year; 
									}
									 
						$preConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),true,null,'0');
						$postConsumer=$consumerMapper->getNewConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),true,null,'1');
					 
						$prebannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'0');
					   	$postbannedConsumer=$consumerMapper->getBannedLastConsumerDetails($month_no[$m],$year_vals,null,$state->__get("state_id"),'1');
					   		
							$totalpre=$preConsumer+$prebannedConsumer;
							$totalpost=$postConsumer+$postbannedConsumer;
							$consumerdata.= "<th>".$totalpre."</th>
									<th>".$totalpost."</th>";
									if($year==$years){
									 if($month_no[$m]==$month){
									break;
									}
								}
							}
							$consumerdata.= "</tr>";
				}
				$consumerdata.=  "</tbody> </table></div>";
				 	 											
						$newfile = 'New Consumer Statistics for '.$year.'.xlsx';
						$inputFileType = 'Excel2007';
						$tmpfile = tempnam(sys_get_temp_dir(), 'html');
						file_put_contents($tmpfile, $consumerdata);
					
						$objPHPExcel = new PHPExcel();
						$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
						$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('New Consumer Statics');
						break;
						
			case "mixed_mode_report" :
				
					$stateMapper=new Application_Model_StatesMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
					$consumerdata="";
					$StateName_Array=array();
					$zendDate = new Zend_Date(); 
					//$zendDate =  new Zend_Date('2017-02-28', 'yyyy-MM-dd'); 
		   			$zendDate->setTimezone("Asia/Calcutta");
					 		
		   				$day= intval($zendDate->toString("dd"));
						   $month=intval( $zendDate->toString("MM"));
		   				$year= intval($zendDate->toString("yyyy"));	
					$consumerdata.= "<div>
		
	                <table border=1 align='center'>
                        <tbody>";
				
					$consumerdata.= "<tr>
										<th>Consumer ID</th>
										<th>Consumer Connection ID</th>
										<th>MM Debit Amt</th>
										<th>Last package Amt</th>
						           </tr>";
					$consumers=$cashRegisterMapper->getMixedModeEntry("mm",$day,$month,$year);
					echo count($consumers);
					$Total_cost_mm=0; $Total_cost_old=0;
					if($consumers){
						foreach ($consumers as $consumer)
						{
								$con=$consumerMapper->getConsumerById($consumer["consumer_id"]);
								$consumers_val=$consumerMapper->getConsumersPackageStatus($month,$year,null,null,null,null,null,null,$consumer["consumer_id"]);
								if($consumers_val){
	                  				if($consumers_val[0]->__get("old_package_id")!=NULL){
											$oldPackage=$packageMapper->getPackageById($consumers_val[0]->__get("old_package_id"));
											$oldPack=$oldPackage->__get("package_cost");
									}else{
										$oldPack=null;
									}
								}else{
										$oldPack=null;
									}	
								$datas=array(
										"consumer_id"=>$consumer["consumer_id"],
										"consumer_connection_id"=>$con->__get("consumer_connection_id"),
										"cr_amount"=>$consumer["cr_amount"],
										"oldpack"=>$oldPack
								);
								$Total_cost_mm=$Total_cost_mm+$consumer["cr_amount"];
								$Total_cost_old=$Total_cost_old+$oldPack;
								$main_summary_array[]=$datas;
						}
						$sort_arr = array();
						foreach ($main_summary_array as $key => $row)
						{
							$sort_arr[$key] = $row['cr_amount'];
						}
						array_multisort($sort_arr, SORT_DESC ,$main_summary_array); 
						
						foreach ($main_summary_array as $main){
							$consumerdata.= "<tr><td>".$main["consumer_id"]."</td>
												 <td>".$main["consumer_connection_id"]."</td>
												 <td>".$main["cr_amount"]."</td>
												 <td>".$main["oldpack"]."</td>
											</tr> ";
						}
						$consumerdata.= "<tr>
										<th>Total</th>
										<th>".count($main_summary_array)."</th>
										<th>".$Total_cost_mm."</th>
										<th>".$Total_cost_old."</th>";
						$consumerdata.= "</tr>";
					}
					$consumerdata.=  "</tbody> </table>
						</div>";
					//echo $consumerdata;exit; 
					$newfile = 'Mixed Mode Consumers Report for '.date('d M,Y h-i A').'.xlsx';
					$inputFileType = 'Excel2007'; 
					$tmpfile = tempnam(sys_get_temp_dir(), 'html');
					file_put_contents($tmpfile, $consumerdata);
				
					$objPHPExcel = new PHPExcel();
					$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
					$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
					$objPHPExcel->getActiveSheet()->setTitle('Mixed Mode Consumers');
				
					break; 

		case "bill_reduce_report" :
				
					$stateMapper=new Application_Model_StatesMapper();
					$cashRegisterMapper = new Application_Model_CashRegisterMapper();
					$siteMapper=new Application_Model_SitesMapper();
					$consumerMapper=new Application_Model_ConsumersMapper();
					$packageMapper=new Application_Model_PackagesMapper();
					$consumerdata="";
					$StateName_Array=array();
					$zendDate = new Zend_Date(); 
					//$zendDate =  /*new Zend_Date();//*/ new Zend_Date('2017-02-28', 'yyyy-MM-dd');
		   			$zendDate->setTimezone("Asia/Calcutta");
					 		
		   				$day= intval($zendDate->toString("dd"));
						   $month=intval( $zendDate->toString("MM"));
		   				$year= intval($zendDate->toString("yyyy"));	
					$consumerdata.= "<div>
		
	                <table border=1 align='center'>
                        <tbody>";
				
					$consumerdata.= "<tr>
										<th>Consumer ID</th>
										<th>Consumer Connection ID</th>
										<th>Old debit amt</th>
										<th>Update amt</th>
						           </tr>";
					$consumers=$cashRegisterMapper->getMixedModeEntry("bill_reduce",$day,$month,$year);
					$Total_cost_mm=0; $Total_cost_bil=0;
					if($consumers){
						foreach ($consumers as $consumer)
						{
								$con=$consumerMapper->getConsumerById($consumer["consumer_id"]);
								
								$datas=array(
										"consumer_id"=>$consumer["consumer_id"],
										"consumer_connection_id"=>$con->__get("consumer_connection_id"),
										"amt_prompt"=>$consumer["amt_prompt"],
										"cr_amount"=>$consumer["cr_amount"],
										
								);
								$Total_cost_mm=$Total_cost_mm+$consumer["amt_prompt"];
								$Total_cost_bil=$Total_cost_bil+$consumer["cr_amount"];
								$main_summary_array[]=$datas;
						}
						$sort_arr = array();
						foreach ($main_summary_array as $key => $row)
						{
							$sort_arr[$key] = $row['cr_amount'];
						}
						array_multisort($sort_arr, SORT_DESC ,$main_summary_array); 
						
						foreach ($main_summary_array as $main){
							$consumerdata.= "<tr><td>".$main["consumer_id"]."</td>
												 <td>".$main["consumer_connection_id"]."</td>
												 <td>".$main["amt_prompt"]."</td>
												 <td>".$main["cr_amount"]."</td>
												 
											</tr> ";
						}
						$consumerdata.= "<tr>
										<th>Total</th>
										<th>".count($main_summary_array)."</th>
										<th>".$Total_cost_mm."</th>
										<th>".$Total_cost_bil."</th>";
						$consumerdata.= "</tr>";
					}	
					$consumerdata.=  "</tbody> </table>
						</div>";
					 //echo $consumerdata;exit;
					$newfile = 'Bill Reduction Report for '.date('d M,Y h-i A').'.xlsx';
					$inputFileType = 'Excel2007';
					$tmpfile = tempnam(sys_get_temp_dir(), 'html');
					file_put_contents($tmpfile, $consumerdata);    
				
					$objPHPExcel = new PHPExcel();
					$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
					$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
					$objPHPExcel->getActiveSheet()->setTitle('Bill Reduction Consumers');
				
					break;
					
			case "consumer_status_report" :
					
						$state_id = $request->getParam("state_id");
						$site_id = $request->getParam("site_id");
						$package_id = $request->getParam("package_id");
						$pdfFile= $request->getParam("PDF");
						
						$state_id=($state_id=="ALL")?NULL:$state_id;
						$site_id=($site_id=="ALL")?NULL:$site_id;
						$package_id=($package_id=="ALL")?NULL:$package_id;
						$pdf=($pdfFile=="PDF")?true:false;
						
						$logMapper = new Application_Model_LogsMapper();
						$consumerMapper=new Application_Model_ConsumersMapper();
						$consumerdata="";
						$Logs_conn=$logMapper->getAllLogsbyConnectionID(null,$month,$year);
						if($Logs_conn){
						foreach ($Logs_conn as $logCon){
							$connection_id=explode("Consumer ID: ", $logCon["message"]);
							$connectionIDs=explode(" ", $connection_id[1]);
							$Conn_id[]=$connectionIDs[0];
						} 
						$Conn_ids = array_unique($Conn_id);
						$ConnIDs = array_values($Conn_ids);
						}else{ $ConnIDs=array();}
						$consumerdata.= "<div>
					
	                <table border=1 align='center'>
                        <tbody>";
					
						$consumerdata.= "<tr>
										<th>Consumer Name</th>
										<th>Consumer Connection ID</th>
										<th>Timestamp</th>
										<th>Summary</th>
						           </tr>";
						 
						$Total_cost_mm=0; $Total_cost_old=0;
						if(count($ConnIDs)>0){
							for($j=0;$j<count($ConnIDs);$j++)
							{
								$Logs=$logMapper->getAllLogsbyConnectionID(true,$month,$year,$ConnIDs[$j]);
								if($Logs) {
									$consumer=$consumerMapper->geAllConsumerStatusDetails($state_id,$site_id,$ConnIDs[$j],$package_id);
									if($consumer){
										$consumerdata.= "<tr>
												<th>".$consumer["consumer_name"]."</th>
												<th>".$consumer["consumer_connection_id"]."</th>";
										$i=0;
										foreach ($Logs as $log){
										$i++;
										if($i==1){
											$consumerdata.="<th>".$log["timestamp"]."</th>
															<th>".$log["message"]."</th></tr>";
										}else{
											$consumerdata.="<tr><th> </th>
															<th> </th>
															<th>".$log["timestamp"]."</th>
															<th>".$log["message"]."</th></tr>";
											}
										}
									}
							   } 
							}
						}
						 
						$consumerdata.=  "</tbody> </table>
						</div>";
					 
						$newfile = 'Consumers Status Report for '.$month.','.$year.'.xlsx';
						if($pdf){
							$newfile = 'Consumers Status Report for '.$month.','.$year.'.pdf';
							$files=(string)$newfile;
							$checkPdf=$this->pdfGenerateAction($consumerdata,$files);
							$this->sendMailAction($files);
						Exit;
						}
						$inputFileType = 'Excel2007';
						$tmpfile = tempnam(sys_get_temp_dir(), 'html');
						file_put_contents($tmpfile, $consumerdata);
					
						$objPHPExcel = new PHPExcel();
						$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
						$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
						$objPHPExcel->getActiveSheet()->setTitle('Consumers Status Report');
					
						break;
						
			case "daily_transaction_report" :
						
							$stateMapper=new Application_Model_StatesMapper();
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							
							$consumerdata="";
							$StateName_Array=array();
							$zendDate = new Zend_Date();
							$zendDate->setTimezone("Asia/Calcutta");
						
							$day= intval($zendDate->toString("dd"));
							$month=intval( $zendDate->toString("MM"));
							$year= intval($zendDate->toString("yyyy"));
							$states=$stateMapper->getAllStates();
							
							$consumerdata.= "<div>
						
	                <table border=1 align='center'>
                        <tbody>";
						
							$consumerdata.= "<tr>
										<th>State Name</th>
										<th>Site Name</th>
										<th>Consumer Connection ID</th>
										<th>Transaction Id</th>
										<th>Entry Type</th>
										<th>Amount</th>
										<th>Transaction By</th>
										<th>Timestamp</th>
										 
						           </tr>";
							
							if($states){
								foreach ($states as $state)
								{
									$cashRegisters=$cashRegisterMapper->getAllTransactionType($state->__get("state_id"));
									if($cashRegisters){
										foreach ($cashRegisters as $cashRegisters){
											if($cashRegisters['transaction_type']=="(M)"){
												$type="Mobile";
											}else{
												$type="SMS";
											}
										$consumerdata.= "<tr>
											<td>".$state->__get("state_name")."</td>
											<td>".$cashRegisters['site_name']."</td>
											<td>".$cashRegisters['consumer_connection_id']."</td>
											<td>".$cashRegisters['transaction_id']."</td>
											<td>".$cashRegisters['cr_entry_type']."</td>
											<td>".$cashRegisters['cr_amount']."</td>
											<td>".$type."</td>
											<td>".$cashRegisters['timestamp']."</td>";
										$consumerdata.= "</tr>";
										}
									}
								}
								 
							}
							$consumerdata.=  "</tbody> </table>
						</div>";
							 
							$newfile = 'Daily transaction Report for '.$month.','.$year.'.xlsx';
							$inputFileType = 'Excel2007';
							$tmpfile = tempnam(sys_get_temp_dir(), 'html');
							file_put_contents($tmpfile, $consumerdata);
						
							$objPHPExcel = new PHPExcel();
							$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
							$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
							$objPHPExcel->getActiveSheet()->setTitle('Daily transaction Report');
						
							break;
							
			case "daily_consumption_report" :
							
								$stateMapper=new Application_Model_StatesMapper();
								$cashRegisterMapper = new Application_Model_CashRegisterMapper();
								$mpptMapper=new Application_Model_MpptsMapper();
								$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
								$sitesMapper=new Application_Model_SitesMapper();
								$consumerdata="";
								$StateName_Array=array();
								$zendDate = new Zend_Date();
								$zendDate->setTimezone("Asia/Calcutta");
							
								$day= intval($zendDate->toString("dd"));
								$month=intval( $zendDate->toString("MM"));
								$year= intval($zendDate->toString("yyyy"));
								$states=$stateMapper->getAllStates();
									
								$first=$year."-".$month."-01 00:00:00";
								$second=$year."-".$month."-".$day." 23:59:59";
								$consumerdata.= "<div>
							
	                <table border=1>
                        <tbody>";
							
								$consumerdata.= "<tr>
										<th>State Name</th>
										<th>Site Name</th>
										<th>MPPT1</th>
										<th>MPPT2</th>
										<th>MPPT3</th>
										<th>MPPT4</th>
										<th>MPPT5</th>
										<th>MPPT6</th>
										<th>MPPT7</th>
										<th>MPPT8</th>
										<th>Timestamp</th>
						
						           </tr>";
								if($states){
									foreach ($states as $state)
									{
										$sites=$sitesMapper->getSitesByStateId($state->__get("state_id"));
										if($sites){
											foreach ($sites as $site)
											{
												$mppts=$mpptMapper->getMpptsBySiteId($site->__get("site_id"));
												if($mppts){
													foreach ($mppts as $mppt){
														$site_id=$site->__get("site_id");
														$mppt_id=$mppt->__get("id");
														$mppt_name=$mppt->__get("mppt_name");
														$mpptReadings=$mpptReadingMapper->getMpptDataByMpptId($mppt_id,$site_id,$first,$second);
														if($mpptReadings){
															foreach ($mpptReadings as $mpptReading){
																$consumerdata.= "<tr>
																				 <td>".$state->__get("state_name")."</td>
																				 <td>".$site->__get("site_name")."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 1')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 2')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 3')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 4')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 5')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 6')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 7')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".(($mppt_name=='MPPT 8')?$mpptReading['enegry_battery_hr']:0)."</td>";
																$consumerdata.=	"<td>".$mpptReading['timestamp']."</td>";
																$consumerdata.= "</tr>";
															}
														}
													}
												}
											}
									}
									}
								}
								$consumerdata.=  "</tbody> </table>
						</div>";
								 
								$newfile = 'Daily Consumption Report for '.$month.','.$year.'.xlsx';
								$inputFileType = 'Excel2007';
								$tmpfile = tempnam(sys_get_temp_dir(), 'html');
								file_put_contents($tmpfile, $consumerdata);
							
								$objPHPExcel = new PHPExcel();
								$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
								$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
								$objPHPExcel->getActiveSheet()->setTitle('Daily Consumption Report');
							
								break;
								
				case "transaction_report" :
						
							$stateMapper=new Application_Model_StatesMapper();
							$cashRegisterMapper = new Application_Model_CashRegisterMapper();
							$logMapper=new Application_Model_LogsMapper();
							$packageMapper=new Application_Model_PackagesMapper();
							$siteMapper=new Application_Model_SitesMapper();
							$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
							$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
							$userMapper=new Application_Model_UsersMapper();
							$collectionAgentsMapper = new Application_Model_CollectionAgentsMapper();
							$currDate = $request->getParam("secondDate");
							$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
							$currDate = $zendDates->toString("yyyy-MM-dd");

							$lastDate = $request->getParam("firstDate");
							$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
							$lastDate = $zendDate->toString("yyyy-MM-dd");
											
							$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
							$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
							$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
							$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
							$status = ($request->getParam("status")=="" || $request->getParam("status")=='undefined' )?NULL: explode(",", $request->getParam("status"));
								
							$collection_agent = ($request->getParam("collection_agent")=="" || $request->getParam("collection_agent")=='undefined' )?NULL: explode(",", $request->getParam("collection_agent"));
							$type = ($request->getParam("types")=="" || $request->getParam("types")=='undefined' )?NULL: explode(",", $request->getParam("types"));

							$outstanding = ($request->getParam("outstanding")=="" || $request->getParam("outstanding")=='undefined' )?NULL:  $request->getParam("outstanding");
							$not_paid = ($request->getParam("not_paid")=="" || $request->getParam("type")=='not_paid' )?NULL: $request->getParam("not_paid");
							
							$consumerdata.= "<div>
						
	                <table border=1 align='center'>
                        <tbody>";
							$cashRegisters=$cashRegisterMapper->getAllTransactionType($state_id,$site_id,$package_type,$package_id,$status,$collection_agent,$type,$currDate,$lastDate);
							$cashRegisters_pending=FALSE;
							if($status!=NULL){
								if(in_array("'INACTIVE'", $status) || in_array("'PENDING'", $status)){
									$cashRegisters_pending=$cashRegisterMapper->getAllTransactionTypeForPending($state_id,$site_id,$package_type,$package_id,$status,$collection_agent,$type,$outstanding,$not_paid,$currDate,$lastDate);
								}
							}
							 
							$consumerdata.= "<tr>
										<th>State Name</th>
										<th>Site Name</th>
										<th>Consumer Name</th>
										<th>Consumer Connection ID</th>
									    <th>Transaction Id</th>
										<th>Entry Type</th>
										<th>Amount</th>
										<th>Transaction By</th>
										<th>Timestamp</th>
										<th>Status</th>
										<th>Type</th>
										<th>Collection Agent</th>
						           </tr>";
							 
							if($cashRegisters){
								foreach ($cashRegisters as $cashRegister)
								{
									$state=$stateMapper->getStateById($cashRegister["state_id"]);
									 
											if($cashRegister['transaction_type']=="(M)" || $cashRegister['transaction_type']=="(S)"){
												if($cashRegister['transaction_type']=="(M)"){
													$type="Mobile";
												}else{
													$type="SMS";
												}
												 
												$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cashRegister["user_id"]);
												$By = $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
											}else{
												$type="Website";
												$users = $userMapper->getUserById($cash["user_id"]);
                                                $By=$users->__get("user_fname") . " " . $users->__get("user_lname");
                                                               
											}
											 
                                                        
										$consumerdata.= "<tr>
											<td>".$state->__get("state_name")."</td>
											<td>".$cashRegister['site_name']."</td>
										    <td>".$cashRegister['consumer_name']."</td>
											<td>".$cashRegister['consumer_connection_id']."</td>
											<td>".$cashRegister['transaction_id']."</td>
											<td>".$cashRegister['cr_entry_type']."</td>
											<td>".$cashRegister['cr_amount']."</td>
											<td>".$type."</td>
											<td>".$cashRegister['timestamp']."</td>
											<td>".$cashRegister['cr_status']."</td>
											<td>".$cashRegister['entry_status']."</td>
											<td>".$By."</td>";
											
										$consumerdata.= "</tr>";
										 
								}
							}
							
							if($cashRegisters_pending){
								foreach ($cashRegisters_pending as $cashRegister)
								{
									$state=$stateMapper->getStateById($cashRegister["state_id"]);
									 
											if($cashRegister['transaction_type']=="(M)" || $cashRegister['transaction_type']=="(S)"){
												if($cashRegister['transaction_type']=="(M)"){
													$type="Mobile";
												}else{
													$type="SMS";
												}
												$collectionAgent = $collectionAgentsMapper->getCollectionAgentById($cashRegister["user_id"]);
												$By= $collectionAgent->__get("agent_fname") . " " . $collectionAgent->__get("agent_lname");
                                             
											}else{
												$type="Website";
												$users = $userMapper->getUserById($cash["user_id"]);
                                                $By=$users->__get("user_fname") . " " . $users->__get("user_lname");
                                                  
											}
											$status_val=($cashRegister['cr_status']=='ACTIVE'?'PENDING':'REJECT');
											      
											$consumerdata.= "<tr>
											<td>".$state->__get("state_name")."</td>
											<td>".$cashRegister['site_name']."</td>
										    <td>".$cashRegister['consumer_name']."</td>
											<td>".$cashRegister['consumer_connection_id']."</td>
											<td>".$cashRegister['transaction_id']."</td>
											<td>".$cashRegister['cr_entry_type']."</td>
											<td>".$cashRegister['cr_amount']."</td>
											<td>".$type."</td>
											<td>".$cashRegister['timestamp']."</td>
											<td>".$status_val."</td>
											<td>".$cashRegister['entry_status']."</td>
											<td>".$By."</td>";
											$consumerdata.= "</tr>";
										 
								}
							} 
							$consumerdata.=  "</tbody> </table>
						</div>";
							 
							$newfile = 'Revenue Report for '.date('Y-m-d H:i:s').'.xlsx';
							$inputFileType = 'Excel2007';
							$tmpfile = tempnam("/html/excelBackup/", 'html');
							file_put_contents($tmpfile, $consumerdata);
						
							$objPHPExcel = new PHPExcel();
							$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
							$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
							$objPHPExcel->getActiveSheet()->setTitle('Transaction Report');  
						
							break;
				
				case "transaction_report_for_out" :
							
								$stateMapper=new Application_Model_StatesMapper();
								$cashRegisterMapper = new Application_Model_CashRegisterMapper();
								$logMapper=new Application_Model_LogsMapper();
								$packageMapper=new Application_Model_PackagesMapper();
								$siteMapper=new Application_Model_SitesMapper();
								$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
								$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
								$userMapper=new Application_Model_UsersMapper();
								$consumerMapper=new Application_Model_ConsumersMapper();
								$currDate = $request->getParam("secondDate");
								$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
								$currDate = $zendDates->toString("yyyy-MM-dd");
								
								$lastDate = $request->getParam("firstDate");
								$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
								$lastDate = $zendDate->toString("yyyy-MM-dd");
								
								$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
								$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
								$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
								$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
								$status = ($request->getParam("status")=="" || $request->getParam("status")=='undefined' )?NULL: explode(",", $request->getParam("status"));
								
								$collection_agent = ($request->getParam("collection_agent")=="" || $request->getParam("collection_agent")=='undefined' )?NULL: explode(",", $request->getParam("collection_agent"));
								$type = ($request->getParam("types")=="" || $request->getParam("types")=='undefined' )?NULL: explode(",", $request->getParam("types"));
								
								$outstanding = ($request->getParam("outstanding")=="" || $request->getParam("outstanding")=='undefined' )?NULL:  $request->getParam("outstanding");
								//$not_paid = ($request->getParam("not_paid")=="" || $request->getParam("type")=='not_paid' )?NULL: $request->getParam("not_paid");
								
								
								$consumerdata.= "<div>
							
										
	                <table border=1 align='center'>
                        <tbody>";
								$consumerdata.= "<tr>
										<th>State Name</th>
										<th>Site Name</th>
										<th>Consumer Name</th>
										<th>Consumer Connection ID</th>
									    <th>DEBIT</th>
										<th>CREDIT</th>
										<th>Outstanding</th>
										<th>Status</th>
								</tr>";
								$consumers=$consumerMapper->getConsumerByFilter($state_id,$site_id,$package_type,$package_id);
								 
								if ($consumers){
									foreach ($consumers as $consumer){
									$cashRegisters=$cashRegisterMapper->getOutstandingByConsumers($consumer["consumer_id"],$currDate,$lastDate);
									 if($cashRegisters){
									    	$credit=intval($cashRegisters["CREDIT"]);
									    	$debit=intval($cashRegisters["DEBIT"]);
									    	$outstanding_val=intval($debit-$credit);
									    	if($outstanding_val==0){
									    		$status="ZERO";
									    	}elseif ($outstanding_val>0){
									    		$status="Increment";
									    	}else{
									    		$status="Decrement";
									    	}
									    	if(($outstanding==0 && $outstanding_val==0) || ($outstanding==1 && $outstanding_val>0) || ($outstanding==2 && $outstanding_val<0)){
												$state=$stateMapper->getStateById($consumer["state_id"]);
												
												$consumerdata.= "<tr>
														<td>".$state->__get("state_name")."</td>
														<td>".$consumer['site_name']."</td>
													    <td>".$consumer['consumer_name']."</td>
														<td>".$consumer['consumer_connection_id']."</td>
														<td>".$debit."</td>
														<td>".$credit."</td>
														<td>".$outstanding_val."</td>
														<td>".$status."</td>";
														 
												$consumerdata.= "</tr>";
											}
										}
									}
								}
								 
								$consumerdata.=  "</tbody> </table>
						</div>";
							 
								$newfile = 'Outstanding Report for '.date('Y-m-d H:i:s').'.xlsx';
								$inputFileType = 'Excel2007';
								$tmpfile = tempnam("/html/excelBackup/", 'html');
								file_put_contents($tmpfile, $consumerdata);
							
								$objPHPExcel = new PHPExcel();
								$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
								$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
								$objPHPExcel->getActiveSheet()->setTitle('Outstanding Report');
							
								break;
				
				case "package_report" :
									
										$stateMapper=new Application_Model_StatesMapper();
											$cashRegisterMapper = new Application_Model_CashRegisterMapper();
											$logMapper=new Application_Model_LogsMapper();
											$packageMapper=new Application_Model_PackagesMapper();
											$siteMapper=new Application_Model_SitesMapper();
											$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
											$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
											$userMapper=new Application_Model_UsersMapper();
											$currDate = $request->getParam("secondDate");
											$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
											$currDate = $zendDates->toString("yyyy-MM-dd");

											$lastDate = $request->getParam("firstDate");
											$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
											$lastDate = $zendDate->toString("yyyy-MM-dd");
											
											$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
											$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
											$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
											$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
												
											$status = ($request->getParam("status")=="" || $request->getParam("status")=='undefined' )?NULL: explode(",", $request->getParam("status"));
											$wattage = ($request->getParam("wattage")=="" || $request->getParam("wattage")=='undefined' )?NULL: explode(",", $request->getParam("wattage"));
											 
										$consumerdata.= "<div>
										
	                <table border=1 align='center'>
                        <tbody>";
									
										$consumerdata.= "<tr>
										<th>Site Name</th>
										<th>Package Name</th>
										<th>Package Cost</th>
										<th>is_postpaid</th>
										<th>wattage</th>
										<th>unit</th>
										<th>extra_charges</th>
										<th>status</th>
										<th>User Name</th>
										 
						
						           </tr>";
										$packages=$packageMapper->getPackagesByDate($state_id,$site_id,$package_type,$package_id,$status,$wattage,$currDate,$lastDate);
										  
										if($packages){
											foreach ($packages as $package)
											{
												$site=$siteMapper->getSiteById($package["site_id"]);
												$user=$userMapper->getUserById($package["user_id"]);
												
												if($package["is_postpaid"]==0){
													$is_post="Prepaid";
												}elseif ($package["is_postpaid"]==1){
													$is_post="Postpaid";
												}else{
													$is_post="MixMode";
												}
										 
												 		$consumerdata.= "<tr>
															<td>".$site->__get("site_name")."</td>
															<td>".$package["package_name"]."</td>
															<td>".$package["package_cost"]."</td>
															<td>".$is_post."</td>
															<td>".$package["wattage"]."</td>
															<td>".$package["unit"]."</td>
															<td>".$package["extra_charges"]."</td>
															<td>".$package["status"]."</td>
															<td>".$user->__get("user_fname")." ".$user->__get("user_lname")."</td>";
														$consumerdata.= "</tr>";
												 
											}
										}
										 
										$consumerdata.=  "</tbody> </table>
						</div>";
										 
										$newfile = 'Package Report for '.date('Y-m-d H:i:s').'.xlsx';
										$inputFileType = 'Excel2007';
										$tmpfile = tempnam("/html/excelBackup/", 'html');
										file_put_contents($tmpfile, $consumerdata);
									 
										$objPHPExcel = new PHPExcel();
										$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
										$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
										$objPHPExcel->getActiveSheet()->setTitle('Package Report');
									
										break;
										
				case "meter_reading_report" :
													
												$stateMapper=new Application_Model_StatesMapper();
												$cashRegisterMapper = new Application_Model_CashRegisterMapper();
												$logMapper=new Application_Model_LogsMapper();
												$packageMapper=new Application_Model_PackagesMapper();
												$siteMapper=new Application_Model_SitesMapper();
												$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
												$meterReadingMapper=new Application_Model_MeterReadingsMapper();
												$consumerMapper=new Application_Model_ConsumersMapper();
												
												$currDate = $request->getParam("secondDate");
												$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
												$currDate = $zendDates->toString("yyyy-MM-dd");
													
												$lastDate = $request->getParam("firstDate");
												$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
												$lastDate = $zendDate->toString("yyyy-MM-dd");
													
												$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
												$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
												$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
												$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
												$consumer_id = ($request->getParam("consumer_id")=="" || $request->getParam("consumer_id")=='undefined' )?NULL: explode(",", $request->getParam("consumer_id"));
												$listing = ($request->getParam("listing")=="" || $request->getParam("listing")=='undefined' )?NULL: $request->getParam("listing");
													
												
												$consumerdata.= "<div>
				
											
	                <table border=1 align='center'>
                        <tbody>";
												$consumerdata.= "<tr>
													<th>State Name</th>
													<th>Site Name</th>
													<th>Consumer Name</th>
													<th>Father Name</th>
													<th>Consumer Connection ID</th>
												    <th>Activation Date</th>
													<th>Package Name</th>
													<th>Month Reading</th>
													<th>Last Month reading</th>
												</tr>";
												$consumers=$consumerMapper->getConsumerByFilter($state_id,$site_id,$package_type,$package_id,$consumer_id,$type);		
												if ($consumers){
													foreach ($consumers as $consumer){
														if($listing=='unit_consumerWise' || $listing=='month_wise_Consumption'){
															$meterReadings=$meterReadingMapper->getMeterReadingsByFilters($consumer["consumer_id"],$currDate,$lastDate,$listing,1);
															if($meterReadings){
																foreach ($meterReadings as $meterReading)
																{
																	$consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
																	$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer['consumer_id']);
															
																	$packageName="";
																	if($consumerpackage){
															
																		foreach($consumerpackage as $package){
																			$packageName.=$package["package_name"]." , ";
																			$is_postpaid=$package["is_postpaid"];
																			$package_id[]=$package["package_id"];
																		}
																	}
																	$state=$stateMapper->getStateById($consumer["state_id"]);
																	$Readings=$meterReadingMapper->getMeterReadingsByFiltersSecond($consumer["consumer_id"],$currDate,$lastDate,$listing,0);
																	if($Readings){
																		$last=$Readings[0]['meter_reading'];
																	}else{
																		$last=0;
																	}
																	$unit_curr=$meterReading['meter_reading']-$last;
																	
																	$lastunit = date('Y-m-d H:i:s', strtotime($lastDate ."-1 month"));
																	
																	$Readings=$meterReadingMapper->getMeterReadingsByFiltersSecond($consumer["consumer_id"],$currDate,$lastunit,$listing,0);
																	if($Readings){
																		$last_unit=$Readings[0]['meter_reading'];
																	}else{
																		$last_unit=0;
																	}
																	$unit_last=$last-$last_unit;
																	$consumerdata.= "<tr>
																		<td>".$state->__get("state_name")."</td>
																		<td>".$consumer['site_name']."</td>
																	    <td>".$consumer['consumer_name']."</td>
																	    <td>".$consumer['consumer_father_name']."</td>
																		<td>".$consumer['consumer_connection_id']."</td>
																		<td>".$consumer['consumer_act_date']."</td>
																		<td>".chop($packageName,", ")."</td>
																		<td>".$unit_curr."</td>
																		<td>".$unit_last."</td>";
																	$consumerdata.= "</tr>";
																}
															}
														}else {
															$meterReadings=$meterReadingMapper->checkMonthMeterReadingsByConsumerId($consumer["consumer_id"]);
															if(!$meterReadings){
																 $consumerPackageMapper= new Application_Model_ConsumersPackageMapper();
																	$consumerpackage=$consumerPackageMapper->getPackageByConsumerId($consumer['consumer_id']);
																		
																	$packageName="";
																	if($consumerpackage){
																			
																		foreach($consumerpackage as $package){
																			$packageName.=$package["package_name"]." , ";
																			$is_postpaid=$package["is_postpaid"];
																			$package_id[]=$package["package_id"];
																		}
																	}
																	$state=$stateMapper->getStateById($consumer["state_id"]);
															
																	$consumerdata.= "<tr>
																		<td>".$state->__get("state_name")."</td>
																		<td>".$consumer['site_name']."</td>
																	    <td>".$consumer['consumer_name']."</td>
																	    <td>".$consumer['consumer_father_name']."</td>
																		<td>".$consumer['consumer_connection_id']."</td>
																		<td>".$consumer['consumer_act_date']."</td>
																		<td>".chop($packageName,", ")."</td>
																		<td>0</td>
																		<td>0</td>";
																	$consumerdata.= "</tr>";
																 
															}
														}
														
													}
												}
													
												$consumerdata.=  "</tbody> </table>
						</div>";
												 	
												$newfile = 'Meter Reading Report for '.date('Y-m-d H:i:s').'.xlsx';
												$inputFileType = 'Excel2007';
												$tmpfile = tempnam("/html/excelBackup/", 'html');
												file_put_contents($tmpfile, $consumerdata);
													
												$objPHPExcel = new PHPExcel();
												$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
												$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
												$objPHPExcel->getActiveSheet()->setTitle('Meter Reading Report');
													
												break;
												
							case "gen_con_report" :
											$stateMapper=new Application_Model_StatesMapper();
											$cashRegisterMapper = new Application_Model_CashRegisterMapper();
											$logMapper=new Application_Model_LogsMapper();
											$packageMapper=new Application_Model_PackagesMapper();
											$siteMapper=new Application_Model_SitesMapper();
											$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
											$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
											$feedarMapper=new Application_Model_SiteMeterReadingMapper();
											
											$currDate = $request->getParam("secondDate");
											$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
											$currDate = $zendDates->toString("yyyy-MM-dd");

											$lastDate = $request->getParam("firstDate");
											$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
											$lastDate = $zendDate->toString("yyyy-MM-dd");
											
											$state_ids = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
											$site_ids = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
											$mppt = ($request->getParam("mppt")=="" || $request->getParam("mppt")=='undefined' )?NULL:explode(",", $request->getParam("mppt"));
											$feeder = ($request->getParam("feeder")=="" || $request->getParam("feeder")=='undefined' )?NULL:explode(",", $request->getParam("feeder"));

											$mppt_range = ($request->getParam("mppt_range")=="" || $request->getParam("mppt_range")=='undefined' )?NULL: $request->getParam("mppt_range");
											$mppt_range_con = ($request->getParam("mppt_range_con")=="" || $request->getParam("mppt_range_con")=='undefined' )?NULL: $request->getParam("mppt_range_con");
											$feeder_range = ($request->getParam("feeder_range")=="" || $request->getParam("feeder_range")=='undefined' )?NULL: $request->getParam("feeder_range");
											$feeder_range_con = ($request->getParam("feeder_range_con")=="" || $request->getParam("feeder_range_con")=='undefined' )?NULL: $request->getParam("feeder_range_con");

											$days=intval((strtotime($currDate)-strtotime($lastDate)) / (60 * 60 * 24));
											 $roleSession = new Zend_Session_Namespace('roles');
											$role_sites_id=$roleSession->site_id;
											$consumerdata="";
											$StateName_Array=array();
											$zendDate = new Zend_Date();
											$zendDate->setTimezone("Asia/Calcutta");
										
											$day= intval($zendDate->toString("dd"));
											$month=intval( $zendDate->toString("MM"));
											$year= intval($zendDate->toString("yyyy"));
										
											$consumerdata.= "<div>
													<table border=1 align='center'>
			                        					<tbody><tr>
																<th>State Name</th>
																<th>Site Name</th>
																<th>Date</th>
																<th>MPPT1</th>
																<th>MPPT2</th>
																<th>MPPT3</th>
																<th>MPPT4</th>
																<th>MPPT5</th>
																<th>MPPT6</th>
																<th>MPPT7</th>
																<th>MPPT8</th>
																<th>MPPT9</th>
																<th>MPPT10</th>
																<th>Total</th>
																<th>FEEDAR1</th>
																<th>FEEDAR2</th>
																<th>FEEDAR3</th>
																<th>FEEDAR4</th>
																<th>FEEDAR5</th>
																<th>Total</th>";
							for($j=0;$j<=$days;$j++){
								$next="+".$j." day";
								$secondNext = date('Y-m-d H:i:s', strtotime($lastDate .$next));
							 
								$trans_date=date_parse_from_format("Y-m-d", $secondNext);
								$day=$trans_date["day"];
								$month= $trans_date["month"];
								$year= $trans_date["year"];
							$sites=$siteMapper->getSitesByStateAndSite($site_ids,$state_ids,$role_sites_id);
							 
							if($sites){
								foreach ($sites as $site){
									$site_id=$site["site_id"];
									$site_name=$site["site_name"];
									$state_name=$site["state_name"];
							//$feedar_data_new=$feedarMapper->getTotalFeedarReadingByDate($site_id,$day,$month,$year,$feeder_range,$feeder_range_con);
							//$mpptReading_data_new=$mpptReadingMapper->getTotalMpptReadingByDate($site_id,$day,$month,$year,$mppt_range,$mppt_range_con);
								//if($mpptReading_data_new || $feedar_data_new){
									//$temp=1;
									$mppts=$mpptReadingMapper->getTotalMpptReadingByMpptName($site_id,$day,$month,$year,$mppt_range,$mppt_range_con);
									 
									 
													if($mppts){
														$mpptTotal=floatval($mppts['MPPT1'])+floatval($mppts['MPPT2'])+floatval($mppts['MPPT3'])+floatval($mppts['MPPT4'])+floatval($mppts['MPPT5'])+floatval($mppts['MPPT6'])+floatval($mppts['MPPT7'])+floatval($mppts['MPPT8'])+floatval($mppts['MPPT9'])+floatval($mppts['MPPT10']);				
										 				$consumerdata.= "<tr>
																	<td>".$state_name."</td>
																	<td>".$site_name."</td>
																	<td>".$year."-".$month."-".$day."</td>
																	<td>".floatval($mppts['MPPT1'])."</td>
																	<td>".floatval($mppts['MPPT2'])."</td>
																	<td>".floatval($mppts['MPPT3'])."</td>
																	<td>".floatval($mppts['MPPT4'])."</td>
																	<td>".floatval($mppts['MPPT5'])."</td>
																	<td>".floatval($mppts['MPPT6'])."</td>
																	<td>".floatval($mppts['MPPT7'])."</td>
																	<td>".floatval($mppts['MPPT8'])."</td>
																	<td>".floatval($mppts['MPPT9'])."</td>
																	<td>".floatval($mppts['MPPT10'])."</td>
    																<td>".$mpptTotal."</td>";
										 	 
													}else{
														$consumerdata.= "<tr>
																	<td>".$state_name."</td>
																	<td>".$site_name."</td>
																	<td>".$year."-".$month."-".$day."</td>
																	<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
																	<td>0</td><td>0</td><td>0</td><td>0</td>";
													}
													$feedars=$feedarMapper->getTotalFeedarReadingByFeedarName($site_id,$day,$month,$year,$feeder_range,$feeder_range_con);
													if($feedars){
														$feedarTotal=floatval($feedars['FEEDER1'])+floatval($feedars['FEEDER2'])+floatval($feedars['FEEDER3'])+floatval($feedars['FEEDER4'])+floatval($feedars['FEEDER5']);
													 		$consumerdata.= " 
																					<td>".floatval($feedars['FEEDER1'])."</td>
																					<td>".floatval($feedars['FEEDER2'])."</td>
																					<td>".floatval($feedars['FEEDER3'])."</td>
																					<td>".floatval($feedars['FEEDER4'])."</td>
																					<td>".floatval($feedars['FEEDER5'])."</td>
																					<td>".$feedarTotal."</td></tr>";
													
													}else{
															$consumerdata.= " <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>";
													}
								//}

								}
								}
							} 
											
											
											$consumerdata.=  "</tbody> </table>
											</div>";
										 
											$newfile = 'Generation consumption report for '.$month.','.$year.'.xlsx';
											$inputFileType = 'Excel2007';
											$tmpfile = tempnam("/html/excelBackup/", 'html');
											file_put_contents($tmpfile, $consumerdata);
												
											$objPHPExcel = new PHPExcel();
											$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
											$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
											$objPHPExcel->getActiveSheet()->setTitle('Gen Con report');
												
											break;
											
						case "consumer_profile_report" :
												 
													$stateMapper=new Application_Model_StatesMapper();
													$cashRegisterMapper = new Application_Model_CashRegisterMapper();
													$logMapper=new Application_Model_LogsMapper();
													$packageMapper=new Application_Model_PackagesMapper();
													$siteMapper=new Application_Model_SitesMapper();
													$feedarReadingMapper=new Application_Model_SiteMeterReadingMapper();
													$mpptReadingMapper=new Application_Model_MpptsReadingMapper();
													$userMapper=new Application_Model_UsersMapper();
													$consumerMapper=new Application_Model_ConsumersMapper();	
													$currDate = $request->getParam("secondDate");
													$zendDates = new Zend_Date($currDate,"MMMM D, YYYY");
													$currDate = $zendDates->toString("yyyy-MM-dd");
													$conPackageMapper=new Application_Model_ConsumersPackageMapper();
													
													$lastDate = $request->getParam("firstDate");
													$zendDate = new Zend_Date($lastDate,"MMMM D, YYYY");
													$lastDate = $zendDate->toString("yyyy-MM-dd");
														
													$state_id = ($request->getParam("state_id")=="" || $request->getParam("state_id")=='undefined' )?NULL:explode(",", $request->getParam("state_id"));
													$site_id = ($request->getParam("site_id")=="" || $request->getParam("site_id")=='undefined' )?NULL:explode(",", $request->getParam("site_id"));
													$package_type = ($request->getParam("package_type")=="" || $request->getParam("package_type")=='undefined' )?NULL:explode(",", $request->getParam("package_type"));
													$package_id = ($request->getParam("package_id")=="" || $request->getParam("package_id")=='undefined' )?NULL:explode(",", $request->getParam("package_id"));
													$status = ($request->getParam("status")=="" || $request->getParam("status")=='undefined' )?NULL: explode(",", $request->getParam("status"));
													$wattage = ($request->getParam("wattage")=="" || $request->getParam("wattage")=='undefined' )?NULL: explode(",", $request->getParam("wattage"));
													$upgrade_downgrade = ($request->getParam("upgrade_downgrade")=="" || $request->getParam("upgrade_downgrade")=='undefined' )?NULL: explode(",", $request->getParam("upgrade_downgrade"));
													$gender = ($request->getParam("gender")=="" || $request->getParam("gender")=='undefined' )?NULL: explode(",", $request->getParam("gender"));
													$feeder = ($request->getParam("feeder")=="" || $request->getParam("feeder")=='undefined' )?NULL: explode(",", $request->getParam("feeder"));
													$equipment_type = ($request->getParam("equipment_type")=="" || $request->getParam("equipment_type")=='undefined' )?NULL: explode(",", $request->getParam("equipment_type"));
													$scheme = ($request->getParam("scheme")=="" || $request->getParam("scheme")=='undefined' )?NULL: explode(",", $request->getParam("scheme"));
													$consumer_type = ($request->getParam("consumer_type")=="" || $request->getParam("consumer_type")=='undefined' )?NULL: explode(",", $request->getParam("consumer_type"));
													
													$no_of_packages = ($request->getParam("no_of_packages")=="" || $request->getParam("no_of_packages")=='undefined' )?NULL:  $request->getParam("no_of_packages");
													
													$consumerdata.= "<div>
														<table border=1 align='center'><tbody>";
													
													$consumerdata.= "<tr>
															<th>State Name</th>
															<th>Site Name</th>
															<th>Consumer Name</th>
															<th>Consumer Connection ID</th>
														    <th>Package Id</th>
															<th>Activation Date</th>
															<th>Status</th>";
													
													
													if(count($upgrade_downgrade)>0){
														$consumerdata.="	<th>Package Status</th>";
													}		 
											           $consumerdata.="</tr>";
													$roleSession = new Zend_Session_Namespace('roles');
													$role_sites_id=$roleSession->site_id;
													$packageHistoryMapper=new Application_Model_PackageHistoryMapper();
													$consumers=$consumerMapper->getConsumerDetailsByFilters($state_id,$site_id,$package_type,$package_id,$status,$wattage,$gender,$feeder,$equipment_type,$scheme,$consumer_type,$currDate,$lastDate,$role_sites_id,$no_of_packages);	
													if($consumers){
														foreach ($consumers as $consumer)
														{
															$state=$stateMapper->getStateById($consumer["state_id"]);
															$package=$packageMapper->getPackageById($consumer["package_id"]);
															
														 if(count($upgrade_downgrade)>0){
															$packageHistorys=$packageHistoryMapper->getLastPackageHistoryDetailsByConumerId($consumer["consumer_id"],$currDate,$lastDate);
															if ($packageHistorys){
															 
																 	
																$ph_package_id=$packageMapper->getPackageById($packageHistorys["ph_package_id"]);
																$newPackage=0;$lastPackage=0;
																if($ph_package_id){
																	$newPackage=$ph_package_id->__get("package_cost");
																	$newpackage_name=$ph_package_id->__get("package_name");
																}
																$last_package=$packageMapper->getPackageById($packageHistorys["last_package"]);
																if($last_package){
																	$lastPackage=$last_package->__get("package_cost");
																	$lastpackage_name=$last_package->__get("package_name");
																}
																
																if(in_array("upgrade", $upgrade_downgrade)){
																if($newPackage>$lastPackage){
																$consumerdata.= "<tr>
																	<td>".$state->__get("state_name")."</td>
																	<td>".$consumer['site_name']."</td>
																    <td>".$consumer['consumer_name']."</td>
																	<td>".$consumer['consumer_connection_id']."</td>
																	<td>".$package->__get("package_name")."</td>
																	<td>".$consumer['consumer_act_date']."</td>
																	<td>".$consumer['consumer_status']."</td>
																	<td> UPGRADE </td>";
																		$consumerdata.= "</tr>";
																} 
																}
																if(in_array("downgrade", $upgrade_downgrade)){
																if($newPackage<$lastPackage){
																	$consumerdata.= "<tr>
																	<td>".$state->__get("state_name")."</td>
																	<td>".$consumer['site_name']."</td>
																    <td>".$consumer['consumer_name']."</td>
																	<td>".$consumer['consumer_connection_id']."</td>
																	<td>".$package->__get("package_name")."</td>
																	<td>".$consumer['consumer_act_date']."</td>
																	<td>".$consumer['consumer_status']."</td>
																	<td> DOWNGRADE </td>";
																	$consumerdata.= "</tr>";
																}
																}
															   
															  }
															}
															
															 
																$consumerdata.= "<tr>
																	<td>".$state->__get("state_name")."</td>
																	<td>".$consumer['site_name']."</td>
																    <td>".$consumer['consumer_name']."</td>
																	<td>".$consumer['consumer_connection_id']."</td>
																	<td>".$package->__get("package_name")."</td>
																	<td>".$consumer['consumer_act_date']."</td>
																	<td>".$consumer['consumer_status']."</td>";
																	 
																$consumerdata.= "</tr>";
															  
														}
													}
													 
													if(count($status)>0){
													if(in_array("'reactive'", $status)){
													$logs=$logMapper->getConsumerReactiveLogsBydate($currDate,$lastDate);
													if($logs){
														foreach ($logs as $log)
														{
															$log_msg=$log["message"];
															$msg=explode("banned Consumer ID: ", $log_msg);
															$conn_id=explode(" By", $msg[1]);
															$consumers=$consumerMapper->getConnectionIdDetails($conn_id[0]);
															if($consumers){
															 
																	$site=$siteMapper->getSiteById($consumers["site_id"]);
																	$state=$stateMapper->getStateById($site->__get("state_id"));
																	$ConPacks=$conPackageMapper->getPackageByConsumerId($consumers['consumer_id']);
																	$packageName=array();
																	if($ConPacks){
																		foreach ($ConPacks as $ConPack){
																			$packages=$packageMapper->getPackageById($ConPack["package_id"]);
																			$packageName[]=$packages->__get("package_name");
																		}
																	}
																	$zendDate = new Zend_Date($consumers['consumer_act_date'],"yyyy-MM-dd");
																	$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
																	$consumerdata.= "<tr>
																		<td>".$state->__get("state_name")."</td>
																		<td>".$site->__get("site_name")."</td>
																	    <td>".$consumers['consumer_connection_id']."</td>
																		<td>".$consumers['consumer_name']."</td>
																		<td>".implode(",", $packageName)."</td>
																		<td>".$consumer_act_date."</td>
																	    <td>".$consumers['consumer_status']."</td>
																		<td> CONSUMER RE-ACTIVE</td>";
																	$consumerdata.= "</tr>";
																}
															 
														}
														//}
													}
													}}
													if(count($status)>0){
													if(in_array("'disabled'", $status)){
												 	$DeletedCons=$consumerMapper->getDeletedConsumerDetails($currDate,$lastDate);
												 	if($DeletedCons){
												 		 
												 		foreach ($DeletedCons as $consumer)
												 		{
												 			$sites=$siteMapper->getSiteById($consumer["site_id"]);
												 			$state=$stateMapper->getStateById($site->__get("state_id"));
												 				
												 			$ConPacks=$conPackageMapper->getPackageByConsumerId($consumer['consumer_id']);
												 			$packageName=array();
												 			if($ConPacks){
												 				foreach ($ConPacks as $ConPack){
												 					$packages=$packageMapper->getPackageById($ConPack["package_id"]);
												 					$packageName[]=$packages->__get("package_name");
												 				}
												 			}
												 			$zendDate = new Zend_Date($consumers['consumer_act_date'],"yyyy-MM-dd");
												 			$consumer_act_date = $zendDate->toString("dd-MM-yyyy");
												 			$consumerdata.= "<tr>
												 				<td>".$state->__get("state_name")."</td>
																<td>".$sites->__get("site_name")."</td>
															    <td>".$consumer['consumer_connection_id']."</td>
																<td>".$consumer['consumer_name']."</td>
																<td>".implode(",", $packageName)."</td>
																<td>".$consumer_act_date."</td>
																<td>".$consumer['consumer_status']."</td>
																<td>".$consumer['suspension_date']."</td>
																<td>Deleted CONSUMER</td>";
												 			$consumerdata.= "</tr>";
												 		}
												 	}
													}}
													$consumerdata.=  "</tbody> </table>
						</div>";
												
													$newfile = 'Consumer Profile Report for '.date('Y-m-d H:i:s').'.xlsx';
													$inputFileType = 'Excel2007';
													$tmpfile = tempnam("/html/excelBackup/", 'html');
													file_put_contents($tmpfile, $consumerdata);
												
													$objPHPExcel = new PHPExcel();
													$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
													$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
													$objPHPExcel->getActiveSheet()->setTitle('Consumer Profile Report');
												
													break;
			
		}		
		
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		//$objWriter->setprecalculateformulas(false);
		$objWriter->save($newfile);
		//header("Location: http://taraurja.in/$newfile");
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);unset($cacheMethod);unset($cacheSettings);
		gc_enable() ;gc_collect_cycles();
		//echo "http://taraurja.in/".$newfile;
		//$objWriter->save('php://output'); 

	if($from !=null && $from!=="")
		{
		defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
		  $date = new Zend_Date();
			$mail = new Zend_Mail();
			 $config = array('auth' => 'login',
                'username' => 'postmaster@taraurja.in',
                'ssl' => 'ssl',
                'port' => 465,
                'password' => '98ebfcfd8eaa8f14b9258184fa3f26f5');
				
			$filename =PUBLIC_PATH . "/html/".$newfile;
			$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
            $at = $mail->createAttachment(file_get_contents($filename));
            $date = new Zend_Date();
            $at->filename = $newfile;
			
			$mail->setFrom("postmaster@taraurja.in", "TARAUrja.in CRON");
			 
			if(strpos($newfile, 'RF - Monthly report') !== false){
			
          		$email_user=$this->getEmailsAction('RF - Monthly report');
           		$mail->setSubject("RF Monthly Report ");
				
			}elseif (strpos($newfile, 'SPI Monthly Report') !== false){
			
				$email_user=$this->getEmailsAction('SPI Monthly Report');
				$mail->setSubject("SPI Monthly Report");
				
			}elseif(strpos($newfile, 'Mixed Mode Consumers') !== false){
				
				$email_user=$this->getEmailsAction('Mixed Mode Consumers');
				$mail->setSubject("Mixed Mode Consumers");
				
			}elseif(strpos($newfile, 'Bill Reduction Report') !== false){
				$email_user=$this->getEmailsAction('Bill Reduction Report');
				$mail->setSubject("Bill Reduction Report");
				
			}elseif(strpos($newfile, 'Daily transaction Report') !== false){
			
				$email_user=$this->getEmailsAction('Daily transaction Report');
				$mail->setSubject("Daily transaction Report");
				
			}elseif(strpos($newfile, 'Daily Consumption Report') !== false){
			
				$email_user=$this->getEmailsAction('Daily Consumption Report');
				$mail->setSubject("Daily Consumption Report");
				
			}else{
				$mail->setSubject("Monthly Report");
			}
			//print_r($email_user);exit;
			$mail->addTo($email_user, 'recipient');
			$mail->addCc('taraurja@gmail.com');
			
            $mail->setBodyHtml("PFA");
			
            if ($mail->send($transport)) {
                echo $msg= "SUCCESS";
            } else {
                echo $msg= "ERROR WHILE MAILING";
            }
	  }else{
		  	defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
		  	$date = new Zend_Date();
		  	$mail = new Zend_Mail();
		  	$config = array('auth' => 'login',
                'username' => 'postmaster@taraurja.in',
                'ssl' => 'ssl',
                'port' => 465,
                'password' => '98ebfcfd8eaa8f14b9258184fa3f26f5');
		  	
		  	$filename =PUBLIC_PATH . "/html/".$newfile;
		  	$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
		  	$at = $mail->createAttachment(file_get_contents($filename));
		  	$date = new Zend_Date();
		  	$at->filename = $newfile;
		  		
		   
		  	$mail->setFrom("postmaster@taraurja.in", "TARAUrja.in CRON");
		  	$auth=new My_Auth('user');
		  	$email_id=$auth->getIdentity()->user_email;
		  	 
		  	$mail->addTo($email_id, 'recipient');
			//$mail->addCc('taraurja@gmail.com'); 
		  	$mail->setSubject($newfile);
		   
		  	$mail->setBodyHtml("PFA");
		  	if ($mail->send($transport)) {
		  		echo $msg= "SUCCESS";
		  	} else {
		  		echo $msg= "ERROR WHILE MAILING";
		  	}
		  }	

	}


	public function getEmailsAction($filename){
	
		$userMapper=new Application_Model_UsersMapper();
		$reportPermissionMapper=new Application_Model_ReportPermissionMapper();
		$rps=$reportPermissionMapper->getReportsByReportName($filename);
		if($rps){
			$rpReport=explode(",", $rps["user_id"]);
			$user_email=array();
			for ($r=0;$r<count($rpReport);$r++){
				$users=$userMapper->getUserById($rpReport[$r]);
				$user_email[]=$users->__get("user_email");
			}
		}
		return $user_email;
	}
	
		public function pdfGenerateAction($data,$filename){
	
		date_default_timezone_set('Asia/Kolkata');
		$pdf=new HTML2FPDF('P','mm',"a2");
		$pdf->AddPage();
	
		$strContent=$data;
		$pdf->WriteHTML($strContent);
	
		$pdf->Output($filename);
		return $filename;
	}
	public function sendMailAction($filename)
	{
	 
		$date = new Zend_Date();
		$date->setTimezone("Asia/Calcutta");
	
		defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
	
		$mail = new Zend_Mail();
		 $config = array('auth' => 'login',
                'username' => 'postmaster@taraurja.in',
                'ssl' => 'ssl',
                'port' => 465,
                'password' => '98ebfcfd8eaa8f14b9258184fa3f26f5');
	
		$transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', $config);
	
	
		$filenames = PUBLIC_PATH . "/html/" . $filename;
		$content = file_get_contents($filenames);
		$attachment = new Zend_Mime_Part($content);
		$attachment->type = 'application/pdf';
		$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
		$attachment->encoding = Zend_Mime::ENCODING_BASE64;
		$attachment->filename = $filename;
	
		$mail->addAttachment($attachment);
	
		$auth=new My_Auth('user');
		$email_id=$auth->getIdentity()->user_email;
		
		$mail->setFrom("postmaster@taraurja.in", "TARAUrja.in CRON");
		$mail->addTo($email_id, 'recipient');
		 
		$mail->setSubject($filename);
		$mail->setBodyHtml("PFA");
	
		if ($mail->send($transport)) {
	
			echo "Mail sent successfully...";
		} else {
			echo "Mail could not be sent...";
		}
	}
	
}



class ReadFilter implements PHPExcel_Reader_IReadFilter {

	public function __construct($fromColumn, $toColumn) {
		$this->columns = array();
		$toColumn++;
		while ($fromColumn !== $toColumn) {
			$this->columns[] = $fromColumn++;
		}
	}

	public function readCell($column, $row, $worksheetName = '') {
		if (in_array($column, $this->columns)) {
			return true;
		}
		return false;
	}
}


